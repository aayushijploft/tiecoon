package com.twc.tiecoon;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import androidx.lifecycle.ProcessLifecycleOwner;

import com.quickblox.BuildConfig;
import com.quickblox.auth.session.QBSettings;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.UploadImageListData;
import com.twc.tiecoon.network.response.UploadVideoListData;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.twc.tiecoon.utils.ActivityLifecycle;
import com.twc.tiecoon.utils.BackgroundListener;

import java.util.ArrayList;

public class    ApplicationClass extends Application implements Application.ActivityLifecycleCallbacks  {
    public static final String TAG = ApplicationClass.class.getSimpleName();
    public static Context mAppContext;
    public static Activity mCurrActivity;
    public static AppPreferences appPreferences;
    public static ArrayList<UploadImageListData> uploadImageListDataArrayList;
    public static ArrayList<UploadImageListData> uploadImageListDataArrayList1;
    public static ArrayList<UploadImageListData> uploadImageListDataArrayList2;
    public static ArrayList<UploadVideoListData> uploadVideoListDataArrayList;
    public static ArrayList<String> ReasonList ;
    public static ArrayList<String> ServiceReasonList ;
    public static ArrayList<String> TalentReasonList ;
    public static HomeUserDetailData homeUserDetailData;
    public static HomeUserDetailsUploadTalent homeUserDetailsUploadTalent;
    private static ApplicationClass instance;
    //App credentials
    private static final String APPLICATION_ID = "88218";
    private static final String AUTH_KEY = "FP68OjmNrJ5ZLvy";
    private static final String AUTH_SECRET = "NPBCWrFNAOF9fkr";
    private static final String ACCOUNT_KEY = "SmQborhq9J6czH4B914o";

    //Chat settings range
    private static final int MAX_PORT_VALUE = 65535;
    private static final int MIN_PORT_VALUE = 1000;
    private static final int MIN_SOCKET_TIMEOUT = 300;
    private static final int MAX_SOCKET_TIMEOUT = 60000;

    //Chat settings
    public static final String USER_DEFAULT_PASSWORD = "quickblox";
    public static final int CHAT_PORT = 5223;
    public static final int SOCKET_TIMEOUT = 300;
    public static final boolean KEEP_ALIVE = true;
    public static final boolean USE_TLS = true;
    public static final boolean AUTO_JOIN = false;
    public static final boolean AUTO_MARK_DELIVERED = true;
    public static final boolean RECONNECTION_ALLOWED = true;
    public static final boolean ALLOW_LISTEN_NETWORK = true;

    public void onCreate() {
        super.onCreate();
        mAppContext = this;
        homeUserDetailData=null;
        homeUserDetailsUploadTalent=null;
        appPreferences = AppPreferences.getInstance();
        uploadImageListDataArrayList=new ArrayList<>();
        uploadImageListDataArrayList1=new ArrayList<>();
        uploadImageListDataArrayList2=new ArrayList<>();
        uploadVideoListDataArrayList=new ArrayList<>();
        ReasonList=new ArrayList<>();
        ServiceReasonList=new ArrayList<>();
        FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);
        initApplication();
        ActivityLifecycle.init(this);
        initFabric();
        checkAppCredentials();
        checkChatSettings();
        initCredentials();
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new BackgroundListener());


    }

    public static Activity getCurrActivity() {
        return mCurrActivity;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        mCurrActivity = activity;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        mCurrActivity = activity;
    }

    @Override
    public void onActivityStarted(Activity activity) {
        mCurrActivity = activity;

    }


    @Override
    public void onActivityPaused(Activity activity) {
        mCurrActivity = activity;
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    private void initFabric() {
        if (!BuildConfig.DEBUG) {
            //  Fabric.with(this, new Crashlytics());
        }
    }
    private void checkAppCredentials() {
        if (APPLICATION_ID.isEmpty() || AUTH_KEY.isEmpty() || AUTH_SECRET.isEmpty() || ACCOUNT_KEY.isEmpty()) {
            throw new AssertionError("error_qb_credentials_empty");
        }
    }

    private void checkChatSettings() {
        if (USER_DEFAULT_PASSWORD.isEmpty() || (CHAT_PORT < MIN_PORT_VALUE || CHAT_PORT > MAX_PORT_VALUE)
                || (SOCKET_TIMEOUT < MIN_SOCKET_TIMEOUT || SOCKET_TIMEOUT > MAX_SOCKET_TIMEOUT)) {
            throw new AssertionError("error_chat_credentails_empty");
        }
    }
    private void initCredentials() {
        QBSettings.getInstance().init(getApplicationContext(), APPLICATION_ID, AUTH_KEY, AUTH_SECRET);
        QBSettings.getInstance().setAccountKey(ACCOUNT_KEY);

        // Uncomment and put your Api and Chat servers endpoints if you want to point the sample
        // against your own server.
        //
        // QBSettings.getInstance().setEndpoints("https://your_api_endpoint.com", "your_chat_endpoint", ServiceZone.PRODUCTION);
        // QBSettings.getInstance().setZone(ServiceZone.PRODUCTION);
    }




    private void initApplication() {
        instance = this;
    }

    public static ApplicationClass getInstance() {
        return instance;
    }



}
