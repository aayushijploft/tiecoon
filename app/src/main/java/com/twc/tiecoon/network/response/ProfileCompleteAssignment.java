package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class ProfileCompleteAssignment implements Parcelable {

    @Expose
    @SerializedName("dialogs_user_id")
    private String dialogs_user_id;
    @Expose
    @SerializedName("xmpp_room_jid")
    private String xmpp_room_jid;
    @Expose
    @SerializedName("dialogs_id")
    private String dialogs_id;
    @Expose
    @SerializedName("currency")
    private String currency;
    @Expose
    @SerializedName("youtublink")
    private String youtublink;
    @Expose
    @SerializedName("city_id")
    private int city_id;
    @Expose
    @SerializedName("state_id")
    private int state_id;
    @Expose
    @SerializedName("country_id")
    private int country_id;
    @Expose
    @SerializedName("language_id")
    private int language_id;
    @Expose
    @SerializedName("age_id")
    private int age_id;
    @Expose
    @SerializedName("gender")
    private String gender;
    @Expose
    @SerializedName("user_id")
    private int user_id;
    @Expose
    @SerializedName("updated_at")
    private String updated_at;
    @Expose
    @SerializedName("created_at")
    private String created_at;
    @Expose
    @SerializedName("tresh")
    private String tresh;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("end_date")
    private String end_date;
    @Expose
    @SerializedName("start_date")
    private String start_date;
    @Expose
    @SerializedName("amount")
    private int amount;
    @Expose
    @SerializedName("text")
    private String text;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("id")
    private int id;

    protected ProfileCompleteAssignment(Parcel in) {
        dialogs_user_id = in.readString();
        xmpp_room_jid = in.readString();
        dialogs_id = in.readString();
        currency = in.readString();
        youtublink = in.readString();
        city_id = in.readInt();
        state_id = in.readInt();
        country_id = in.readInt();
        language_id = in.readInt();
        age_id = in.readInt();
        gender = in.readString();
        user_id = in.readInt();
        updated_at = in.readString();
        created_at = in.readString();
        tresh = in.readString();
        status = in.readString();
        end_date = in.readString();
        start_date = in.readString();
        amount = in.readInt();
        text = in.readString();
        name = in.readString();
        id = in.readInt();
    }

    public static final Creator<ProfileCompleteAssignment> CREATOR = new Creator<ProfileCompleteAssignment>() {
        @Override
        public ProfileCompleteAssignment createFromParcel(Parcel in) {
            return new ProfileCompleteAssignment(in);
        }

        @Override
        public ProfileCompleteAssignment[] newArray(int size) {
            return new ProfileCompleteAssignment[size];
        }
    };

    public String getDialogs_user_id() {
        return dialogs_user_id;
    }

    public void setDialogs_user_id(String dialogs_user_id) {
        this.dialogs_user_id = dialogs_user_id;
    }

    public String getXmpp_room_jid() {
        return xmpp_room_jid;
    }

    public void setXmpp_room_jid(String xmpp_room_jid) {
        this.xmpp_room_jid = xmpp_room_jid;
    }

    public String getDialogs_id() {
        return dialogs_id;
    }

    public void setDialogs_id(String dialogs_id) {
        this.dialogs_id = dialogs_id;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getYoutublink() {
        return youtublink;
    }

    public void setYoutublink(String youtublink) {
        this.youtublink = youtublink;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public int getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(int language_id) {
        this.language_id = language_id;
    }

    public int getAge_id() {
        return age_id;
    }

    public void setAge_id(int age_id) {
        this.age_id = age_id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTresh() {
        return tresh;
    }

    public void setTresh(String tresh) {
        this.tresh = tresh;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dialogs_user_id);
        dest.writeString(xmpp_room_jid);
        dest.writeString(dialogs_id);
        dest.writeString(currency);
        dest.writeString(youtublink);
        dest.writeInt(city_id);
        dest.writeInt(state_id);
        dest.writeInt(country_id);
        dest.writeInt(language_id);
        dest.writeInt(age_id);
        dest.writeString(gender);
        dest.writeInt(user_id);
        dest.writeString(updated_at);
        dest.writeString(created_at);
        dest.writeString(tresh);
        dest.writeString(status);
        dest.writeString(end_date);
        dest.writeString(start_date);
        dest.writeInt(amount);
        dest.writeString(text);
        dest.writeString(name);
        dest.writeInt(id);
    }
}
