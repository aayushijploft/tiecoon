package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class RequestMoneyModel {

    @Expose
    @SerializedName("data")
    public Data data;
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    @SerializedName("success")
    public boolean success;

    public static class Data {
        @Expose
        @SerializedName("id")
        public String id;
        @Expose
        @SerializedName("created_at")
        public String created_at;
        @Expose
        @SerializedName("updated_at")
        public String updated_at;
        @Expose
        @SerializedName("amount")
        public String amount;
        @Expose
        @SerializedName("userid")
        public String userid;
        @Expose
        @SerializedName("projectownerId")
        public String projectownerId;
        @Expose
        @SerializedName("projectId")
        public String projectId;
    }
}
