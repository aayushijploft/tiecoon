package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("firstname")
    @Expose
    private Object firstname;
    @SerializedName("lastname")
    @Expose
    private Object lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("profile")
    @Expose
    private String profile;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("talents")
    @Expose
    private List<ProfileTalent> talents = null;
    @SerializedName("user_detils")
    @Expose
    private UserProfileDetails userDetils;
    @SerializedName("create_assignments")
    @Expose
    private List<ProfileCreateAssignment> createAssignments = null;
    @SerializedName("complete_assignment")
    @Expose
    private List<ProfileCompleteAssignment> complete_assignment = null;
    @SerializedName("upload_talent")
    @Expose
    private List<HomeUserDetailsUploadTalent> uploadTalent = null;
    @SerializedName("user_talent")
    @Expose
    private List<ProfileUserTalent> profileUserTalents = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getFirstname() {
        return firstname;
    }

    public void setFirstname(Object firstname) {
        this.firstname = firstname;
    }

    public Object getLastname() {
        return lastname;
    }

    public void setLastname(Object lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ProfileTalent> getTalents() {
        return talents;
    }

    public void setTalents(List<ProfileTalent> talents) {
        this.talents = talents;
    }

    public UserProfileDetails getUserDetils() {
        return userDetils;
    }

    public void setUserDetils(UserProfileDetails userDetils) {
        this.userDetils = userDetils;
    }

    public List<ProfileCreateAssignment> getCreateAssignments() {
        return createAssignments;
    }

    public void setCreateAssignments(List<ProfileCreateAssignment> createAssignments) {
        this.createAssignments = createAssignments;
    }

    public List<ProfileCompleteAssignment> getComplete_assignment() {
        return complete_assignment;
    }

    public void setComplete_assignment(List<ProfileCompleteAssignment> complete_assignment) {
        this.complete_assignment = complete_assignment;
    }

    public List<HomeUserDetailsUploadTalent> getUploadTalent() {
        return uploadTalent;
    }

    public void setUploadTalent(List<HomeUserDetailsUploadTalent> uploadTalent) {
        this.uploadTalent = uploadTalent;
    }

    public List<ProfileUserTalent> getProfileUserTalents() {
        return profileUserTalents;
    }

    public void setProfileUserTalents(List<ProfileUserTalent> profileUserTalents) {
        this.profileUserTalents = profileUserTalents;
    }

    @Override
    public String toString() {
        return "ProfileData{" +
                "id=" + id +
                ", firstname=" + firstname +
                ", lastname=" + lastname +
                ", email='" + email + '\'' +
                ", profile='" + profile + '\'' +
                ", path='" + path + '\'' +
                ", phone='" + phone + '\'' +
                ", type='" + type + '\'' +
                ", talents=" + talents +
                ", userDetils=" + userDetils +
                ", createAssignments=" + createAssignments +
                ", uploadTalent=" + uploadTalent +
                ", profileUserTalents=" + profileUserTalents +
                '}';
    }
}
