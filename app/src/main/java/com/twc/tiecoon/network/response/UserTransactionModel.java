package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class UserTransactionModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    @SerializedName("success")
    public String success;

    public static class Data {
        @Expose
        @SerializedName("image")
        public String image;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("userid")
        public String userid;
        @Expose
        @SerializedName("id")
        public String id;
    }
}
