package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class AddressModel {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("updated_at")
        private String updated_at;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("bstate")
        private String bstate;
        @Expose
        @SerializedName("bcity")
        private String bcity;
        @Expose
        @SerializedName("btown")
        private String btown;
        @Expose
        @SerializedName("bdeliveryaddress")
        private String bdeliveryaddress;
        @Expose
        @SerializedName("bpincode")
        private String bpincode;
        @Expose
        @SerializedName("bmobile")
        private String bmobile;
        @Expose
        @SerializedName("bname")
        private String bname;
        @Expose
        @SerializedName("state")
        private String state;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("town")
        private String town;
        @Expose
        @SerializedName("deliveryaddress")
        private String deliveryaddress;
        @Expose
        @SerializedName("pincode")
        private String pincode;
        @Expose
        @SerializedName("mobile")
        private String mobile;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("id")
        private int id;

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getBstate() {
            return bstate;
        }

        public void setBstate(String bstate) {
            this.bstate = bstate;
        }

        public String getBcity() {
            return bcity;
        }

        public void setBcity(String bcity) {
            this.bcity = bcity;
        }

        public String getBtown() {
            return btown;
        }

        public void setBtown(String btown) {
            this.btown = btown;
        }

        public String getBdeliveryaddress() {
            return bdeliveryaddress;
        }

        public void setBdeliveryaddress(String bdeliveryaddress) {
            this.bdeliveryaddress = bdeliveryaddress;
        }

        public String getBpincode() {
            return bpincode;
        }

        public void setBpincode(String bpincode) {
            this.bpincode = bpincode;
        }

        public String getBmobile() {
            return bmobile;
        }

        public void setBmobile(String bmobile) {
            this.bmobile = bmobile;
        }

        public String getBname() {
            return bname;
        }

        public void setBname(String bname) {
            this.bname = bname;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getTown() {
            return town;
        }

        public void setTown(String town) {
            this.town = town;
        }

        public String getDeliveryaddress() {
            return deliveryaddress;
        }

        public void setDeliveryaddress(String deliveryaddress) {
            this.deliveryaddress = deliveryaddress;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
