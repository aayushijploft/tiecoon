package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeUserDetailsProduct implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("qbid")
    @Expose
    private String quickbloxuser_id;
    @SerializedName("watermark")
    @Expose
    private String watermark;
    @SerializedName("createimage")
    @Expose
    private String createimage;
    @SerializedName("fontsize")
    @Expose
    private String fontsize;
    @SerializedName("fontcolor")
    @Expose
    private String fontcolor;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private Integer subcategoryId;
    @SerializedName("pricecode")
    @Expose
    private String pricecode;
    @SerializedName("price")
    @Expose
    private Long price;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("outofstock")
    @Expose
    private String outofstock;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tresh")
    @Expose
    private String tresh;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("uploadtype")
    @Expose
    private Integer uploadtype;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("pincode")
    @Expose
    private String pincode;
     @SerializedName("address")
    @Expose
    private String address;
     @SerializedName("town")
    @Expose
    private String town;
     @SerializedName("city")
    @Expose
    private String city;
      @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("category")
    @Expose
        private AddProductCategoryData category=null;
    @SerializedName("sub_category")
    @Expose
    private AddProductSubCategoryData subCategory=null;

    @SerializedName("upload_image")
    @Expose
    private List<UploadImageListData> upload_image = null;


    public HomeUserDetailsProduct() {
    }
    protected HomeUserDetailsProduct(Parcel in) {
        category = in.readParcelable(getClass().getClassLoader());
        subCategory = in.readParcelable(getClass().getClassLoader());
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            userId = null;
        } else {
            userId = in.readInt();
        }
        image = in.readString();
        path = in.readString();
        watermark = in.readString();
        createimage = in.readString();
        quickbloxuser_id = in.readString();
        fontsize = in.readString();
        fontcolor = in.readString();
        if (in.readByte() == 0) {
            categoryId = null;
        } else {
            categoryId = in.readInt();
        }
        if (in.readByte() == 0) {
            subcategoryId = null;
        } else {
            subcategoryId = in.readInt();
        }
        pricecode = in.readString();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readLong();
        }
        description = in.readString();
        qty = in.readString();
        outofstock = in.readString();
        status = in.readString();
        tresh = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        if (in.readByte() == 0) {
            uploadtype = null;
        } else {
            uploadtype = in.readInt();
        }
        location = in.readString();
        name = in.readString();
        phone = in.readString();
        pincode = in.readString();
        address = in.readString();
        town = in.readString();
        city = in.readString();
        state = in.readString();
        category = in.readParcelable(AddProductCategoryData.class.getClassLoader());
        subCategory = in.readParcelable(AddProductSubCategoryData.class.getClassLoader());
        upload_image = in.createTypedArrayList(UploadImageListData.CREATOR);
    }

    public static final Creator<HomeUserDetailsProduct> CREATOR = new Creator<HomeUserDetailsProduct>() {
        @Override
        public HomeUserDetailsProduct createFromParcel(Parcel in) {
            return new HomeUserDetailsProduct(in);
        }

        @Override
        public HomeUserDetailsProduct[] newArray(int size) {
            return new HomeUserDetailsProduct[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getWatermark() {
        return watermark;
    }

    public void setWatermark(String watermark) {
        this.watermark = watermark;
    }

    public String getCreateimage() {
        return createimage;
    }

    public void setCreateimage(String createimage) {
        this.createimage = createimage;
    }

    public String getFontsize() {
        return fontsize;
    }

    public void setFontsize(String fontsize) {
        this.fontsize = fontsize;
    }

    public String getFontcolor() {
        return fontcolor;
    }

    public void setFontcolor(String fontcolor) {
        this.fontcolor = fontcolor;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(Integer subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getPricecode() {
        return pricecode;
    }

    public void setPricecode(String pricecode) {
        this.pricecode = pricecode;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getOutofstock() {
        return outofstock;
    }

    public void setOutofstock(String outofstock) {
        this.outofstock = outofstock;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTresh() {
        return tresh;
    }

    public void setTresh(String tresh) {
        this.tresh = tresh;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getUploadtype() {
        return uploadtype;
    }

    public void setUploadtype(Integer uploadtype) {
        this.uploadtype = uploadtype;
    }

    public String getQuickbloxuser_id() {
        return quickbloxuser_id;
    }

    public void setQuickbloxuser_id(String quickbloxuser_id) {
        this.quickbloxuser_id = quickbloxuser_id;
    }

    public AddProductCategoryData getCategory() {
        return category;
    }

    public void setCategory(AddProductCategoryData category) {
        this.category = category;
    }

    public AddProductSubCategoryData getSubCategory() {
        return subCategory;
    }


    public List<UploadImageListData> getUpload_image() {
        return upload_image;
    }

    public void setUpload_image(List<UploadImageListData> upload_image) {
        this.upload_image = upload_image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(category, i);
        parcel.writeParcelable(subCategory, i);
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        if (userId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(userId);
        }
        parcel.writeString(image);
        parcel.writeString(path);
        parcel.writeString(watermark);
        parcel.writeString(createimage);
        parcel.writeString(quickbloxuser_id);
        parcel.writeString(fontsize);
        parcel.writeString(fontcolor);
        if (categoryId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(categoryId);
        }
        if (subcategoryId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(subcategoryId);
        }
        parcel.writeString(pricecode);
        if (price == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(price);
        }
        parcel.writeString(description);
        parcel.writeString(qty);
        parcel.writeString(outofstock);
        parcel.writeString(status);
        parcel.writeString(tresh);
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
        if (uploadtype == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(uploadtype);
        }
        parcel.writeString(location);
        parcel.writeString(name);
        parcel.writeString(phone);
        parcel.writeString(pincode);
        parcel.writeString(address);
        parcel.writeString(town);
        parcel.writeString(city);
        parcel.writeString(state);
        parcel.writeParcelable(category, i);
        parcel.writeParcelable(subCategory, i);
        parcel.writeTypedList(upload_image);
    }

    @Override
    public String toString() {
        return "HomeUserDetailsProduct{" +
                "id=" + id +
                ", userId=" + userId +
                ", image='" + image + '\'' +
                ", path='" + path + '\'' +
                ", watermark='" + watermark + '\'' +
                ", createimage='" + createimage + '\'' +
                ", fontsize='" + fontsize + '\'' +
                ", fontcolor='" + fontcolor + '\'' +
                ", categoryId=" + categoryId +
                ", subcategoryId=" + subcategoryId +
                ", pricecode='" + pricecode + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", tresh='" + tresh + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", uploadtype=" + uploadtype +
                ", location='" + location + '\'' +
                ", category=" + category +
                ", subCategory=" + subCategory +
                ", upload_image=" + upload_image +
                '}';
    }
}
