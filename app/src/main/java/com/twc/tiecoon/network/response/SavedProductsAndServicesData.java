package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SavedProductsAndServicesData {

    @SerializedName("service")
    @Expose
    private List<SavedProductsAndServicesDataServices> service = null;
    @SerializedName("product")
    @Expose
    private List<SavedProductsAndServicesDataProducts> product = null;

    public List<SavedProductsAndServicesDataServices> getService() {
        return service;
    }

    public void setService(List<SavedProductsAndServicesDataServices> service) {
        this.service = service;
    }

    public List<SavedProductsAndServicesDataProducts> getProduct() {
        return product;
    }

    public void setProduct(List<SavedProductsAndServicesDataProducts> product) {
        this.product = product;
    }
}
