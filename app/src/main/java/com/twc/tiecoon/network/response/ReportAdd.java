package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportAdd {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("id")
        private int id;
        @Expose
        @SerializedName("report_id")
        private String report_id;
        @Expose
        @SerializedName("reportuser_id")
        private String reportuser_id;
        @Expose
        @SerializedName("loginuser_id")
        private int loginuser_id;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getReport_id() {
            return report_id;
        }

        public void setReport_id(String report_id) {
            this.report_id = report_id;
        }

        public String getReportuser_id() {
            return reportuser_id;
        }

        public void setReportuser_id(String reportuser_id) {
            this.reportuser_id = reportuser_id;
        }

        public int getLoginuser_id() {
            return loginuser_id;
        }

        public void setLoginuser_id(int loginuser_id) {
            this.loginuser_id = loginuser_id;
        }
    }
}
