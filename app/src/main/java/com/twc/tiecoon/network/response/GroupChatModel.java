package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class GroupChatModel {


    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("occupants_ids")
        private List<Occupants_ids> occupants_ids;
        @Expose
        @SerializedName("amount")
        private int amount;
        @Expose
        @SerializedName("dialogs_user_id")
        private String dialogs_user_id;
        @Expose
        @SerializedName("xmpp_room_jid")
        private String xmpp_room_jid;
        @Expose
        @SerializedName("quickblox_user_id")
        private String quickblox_user_id;
        @Expose
        @SerializedName("dialogs_id")
        private String dialogs_id;
        @Expose
        @SerializedName("massage")
        private String massage;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("paid")
        private String paid;
        @Expose
        @SerializedName("project_id")
        private int project_id;
        @Expose
        @SerializedName("projectuser_id")
        private int projectuser_id;
        @Expose
        @SerializedName("name")
        private String name;
       @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("id")
        private int id;
        @Expose
        @SerializedName("is_admin")
        private boolean is_admin;

        public List<Occupants_ids> getOccupants_ids() {
            return occupants_ids;
        }

        public void setOccupants_ids(List<Occupants_ids> occupants_ids) {
            this.occupants_ids = occupants_ids;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getDialogs_user_id() {
            return dialogs_user_id;
        }

        public void setDialogs_user_id(String dialogs_user_id) {
            this.dialogs_user_id = dialogs_user_id;
        }

        public String getXmpp_room_jid() {
            return xmpp_room_jid;
        }

        public void setXmpp_room_jid(String xmpp_room_jid) {
            this.xmpp_room_jid = xmpp_room_jid;
        }

        public String getQuickblox_user_id() {
            return quickblox_user_id;
        }

        public void setQuickblox_user_id(String quickblox_user_id) {
            this.quickblox_user_id = quickblox_user_id;
        }

        public String getDialogs_id() {
            return dialogs_id;
        }

        public void setDialogs_id(String dialogs_id) {
            this.dialogs_id = dialogs_id;
        }

        public String getMassage() {
            return massage;
        }

        public void setMassage(String massage) {
            this.massage = massage;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPaid() {
            return paid;
        }

        public void setPaid(String paid) {
            this.paid = paid;
        }

        public int getProject_id() {
            return project_id;
        }

        public void setProject_id(int project_id) {
            this.project_id = project_id;
        }

        public int getProjectuser_id() {
            return projectuser_id;
        }

        public void setProjectuser_id(int projectuser_id) {
            this.projectuser_id = projectuser_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public boolean isIs_admin() {
            return is_admin;
        }

        public void setIs_admin(boolean is_admin) {
            this.is_admin = is_admin;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }

    public static class Occupants_ids {
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("occupants_ids")
        private String occupants_ids;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOccupants_ids() {
            return occupants_ids;
        }

        public void setOccupants_ids(String occupants_ids) {
            this.occupants_ids = occupants_ids;
        }
    }
}
