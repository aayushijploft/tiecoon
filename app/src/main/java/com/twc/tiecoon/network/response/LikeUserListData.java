package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LikeUserListData implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("like_user_id")
    @Expose
    private Integer likeUserId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user")
    @Expose
    private LikeUserDetailData user = null;
    @SerializedName("user_detils")
    @Expose
    private HomeUserDetailsUserDetils userDetils;

    @SerializedName("brand")
    @Expose
    private List<AddBrandsData> brand = null;
    @SerializedName("user_talent")
    @Expose
    private List<HomeUserDetialsUserTalent> userTalent = null;
    @SerializedName("services")
    @Expose
    private List<HomeUserDetailsService> services = null;
    @SerializedName("products")
    @Expose
    private List<HomeUserDetailsProduct> products = null;
    @SerializedName("upload_talent")
    @Expose
    private List<HomeUserDetailsUploadTalent> uploadTalent = null;
    @SerializedName("create_assignments")
    @Expose
    private List<ProfileCreateAssignment> createAssignments = null;
    @SerializedName("testimonial")
    @Expose
    private List<HomeUserDetailsTestimonial> testimonial = null;
    @SerializedName("reorganization")
    @Expose
    private List<HomeUserDetailsReOrganisation> reorganization = null;
    @SerializedName("feedback")
    @Expose
    private List<HomeUserDetailsFeedback> feedback = null;

    protected LikeUserListData(Parcel in) {
        in.readTypedList(uploadTalent, HomeUserDetailsUploadTalent.CREATOR);
        in.readTypedList(products, HomeUserDetailsProduct.CREATOR);
        in.readTypedList(brand, AddBrandsData.CREATOR);
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            userId = null;
        } else {
            userId = in.readInt();
        }
        if (in.readByte() == 0) {
            likeUserId = null;
        } else {
            likeUserId = in.readInt();
        }
        createdAt = in.readString();
        updatedAt = in.readString();
        userDetils = in.readParcelable(HomeUserDetailsUserDetils.class.getClassLoader());

        brand = in.createTypedArrayList(AddBrandsData.CREATOR);
        userTalent = in.createTypedArrayList(HomeUserDetialsUserTalent.CREATOR);
        services = in.createTypedArrayList(HomeUserDetailsService.CREATOR);
        products = in.createTypedArrayList(HomeUserDetailsProduct.CREATOR);
        uploadTalent = in.createTypedArrayList(HomeUserDetailsUploadTalent.CREATOR);
        createAssignments = in.createTypedArrayList(ProfileCreateAssignment.CREATOR);
        testimonial = in.createTypedArrayList(HomeUserDetailsTestimonial.CREATOR);
        reorganization = in.createTypedArrayList(HomeUserDetailsReOrganisation.CREATOR);
        feedback = in.createTypedArrayList(HomeUserDetailsFeedback.CREATOR);
    }

    public static final Creator<LikeUserListData> CREATOR = new Creator<LikeUserListData>() {
        @Override
        public LikeUserListData createFromParcel(Parcel in) {
            return new LikeUserListData(in);
        }

        @Override
        public LikeUserListData[] newArray(int size) {
            return new LikeUserListData[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLikeUserId() {
        return likeUserId;
    }

    public void setLikeUserId(Integer likeUserId) {
        this.likeUserId = likeUserId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LikeUserDetailData getUser() {
        return user;
    }

    public void setUser(LikeUserDetailData user) {
        this.user = user;
    }

    public HomeUserDetailsUserDetils getUserDetils() {
        return userDetils;
    }

    public void setUserDetils(HomeUserDetailsUserDetils userDetils) {
        this.userDetils = userDetils;
    }



    public List<AddBrandsData> getBrand() {
        return brand;
    }

    public void setBrand(List<AddBrandsData> brand) {
        this.brand = brand;
    }

    public List<HomeUserDetialsUserTalent> getUserTalent() {
        return userTalent;
    }

    public void setUserTalent(List<HomeUserDetialsUserTalent> userTalent) {
        this.userTalent = userTalent;
    }

    public List<HomeUserDetailsService> getServices() {
        return services;
    }

    public void setServices(List<HomeUserDetailsService> services) {
        this.services = services;
    }

    public List<HomeUserDetailsProduct> getProducts() {
        return products;
    }

    public void setProducts(List<HomeUserDetailsProduct> products) {
        this.products = products;
    }

    public List<HomeUserDetailsUploadTalent> getUploadTalent() {
        return uploadTalent;
    }

    public void setUploadTalent(List<HomeUserDetailsUploadTalent> uploadTalent) {
        this.uploadTalent = uploadTalent;
    }

    public List<ProfileCreateAssignment> getCreateAssignments() {
        return createAssignments;
    }

    public void setCreateAssignments(List<ProfileCreateAssignment> createAssignments) {
        this.createAssignments = createAssignments;
    }

    public List<HomeUserDetailsTestimonial> getTestimonial() {
        return testimonial;
    }

    public void setTestimonial(List<HomeUserDetailsTestimonial> testimonial) {
        this.testimonial = testimonial;
    }

    public List<HomeUserDetailsReOrganisation> getReorganization() {
        return reorganization;
    }

    public void setReorganization(List<HomeUserDetailsReOrganisation> reorganization) {
        this.reorganization = reorganization;
    }

    public List<HomeUserDetailsFeedback> getFeedback() {
        return feedback;
    }

    public void setFeedback(List<HomeUserDetailsFeedback> feedback) {
        this.feedback = feedback;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(uploadTalent);
        parcel.writeTypedList(products);
        parcel.writeTypedList(brand);
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        if (userId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(userId);
        }
        if (likeUserId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(likeUserId);
        }
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
        parcel.writeParcelable(userDetils, i);
//        parcel.writeTypedList(preferredLanguage);
        parcel.writeTypedList(brand);
        parcel.writeTypedList(userTalent);
        parcel.writeTypedList(services);
        parcel.writeTypedList(products);
        parcel.writeTypedList(uploadTalent);
        parcel.writeTypedList(createAssignments);
        parcel.writeTypedList(testimonial);
        parcel.writeTypedList(reorganization);
        parcel.writeTypedList(feedback);
    }

    @Override
    public String toString() {
        return "LikeUserListData{" +
                "id=" + id +
                ", userId=" + userId +
                ", likeUserId=" + likeUserId +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", user=" + user +
                ", userDetils=" + userDetils +

                ", brand=" + brand +
                ", userTalent=" + userTalent +
                ", services=" + services +
                ", products=" + products +
                ", uploadTalent=" + uploadTalent +
                ", createAssignments=" + createAssignments +
                ", testimonial=" + testimonial +
                ", reorganization=" + reorganization +
                ", feedback=" + feedback +
                '}';
    }
}
