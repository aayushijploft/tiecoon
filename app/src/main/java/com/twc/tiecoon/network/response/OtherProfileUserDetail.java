package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OtherProfileUserDetail {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("notfication_count")
    @Expose
    private Integer notfication_count;
    @SerializedName("is_logout")
    @Expose
    private Integer is_logout;
     @SerializedName("collaboration_count ")
    @Expose
    private Integer collaboration_count;

    @SerializedName("data")
    @Expose
    private List<HomeUserDetailData> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getNotfication_count() {
        return notfication_count;
    }

    public void setNotfication_count(Integer notfication_count) {
        this.notfication_count = notfication_count;
    }

    public Integer getCollaboration_count() {
        return collaboration_count;
    }

    public void setCollaboration_count(Integer collaboration_count) {
        this.collaboration_count = collaboration_count;
    }

    public Integer getIs_logout() {
        return is_logout;
    }

    public void setIs_logout(Integer is_logout) {
        this.is_logout = is_logout;
    }

    public List<HomeUserDetailData> getData() {
        return data;
    }

    public void setData(List<HomeUserDetailData> data) {
        this.data = data;
    }
}
