package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatDataDetails {

    @SerializedName("user_id")
    @Expose
    private Integer user_id;

    @SerializedName("massage")
    @Expose
    private String massage;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("profile")
    @Expose
    private String profile;

    @SerializedName("device_token")
    @Expose
    private String device_token;

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }
}
