package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class GetBankDetails {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("total_earning")
    private Total_earning total_earning;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Total_earning getTotal_earning() {
        return total_earning;
    }

    public void setTotal_earning(Total_earning total_earning) {
        this.total_earning = total_earning;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("updated_at")
        private String updated_at;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("paypal_email")
        private String paypal_email;
         @Expose
        @SerializedName("upi")
        private String upi;
         @Expose
        @SerializedName("isverfy")
        private String isverfy ;
        @Expose
        @SerializedName("ifsc_code")
        private String ifsc_code;
        @Expose
        @SerializedName("account_number")
        private String account_number;
        @Expose
        @SerializedName("phone_number")
        private String phone_number;
        @Expose
        @SerializedName("bankname")
        private String bankname;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("id")
        private int id;

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPaypal_email() {
            return paypal_email;
        }

        public void setPaypal_email(String paypal_email) {
            this.paypal_email = paypal_email;
        }

        public String getUpi() {
            return upi;
        }

        public void setUpi(String upi) {
            this.upi = upi;
        }

        public String getIsverfy() {
            return isverfy;
        }

        public void setIsverfy(String isverfy) {
            this.isverfy = isverfy;
        }

        public String getIfsc_code() {
            return ifsc_code;
        }

        public void setIfsc_code(String ifsc_code) {
            this.ifsc_code = ifsc_code;
        }

        public String getAccount_number() {
            return account_number;
        }

        public void setAccount_number(String account_number) {
            this.account_number = account_number;
        }

        public String getPhone_number() {
            return phone_number;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public String getBankname() {
            return bankname;
        }

        public void setBankname(String bankname) {
            this.bankname = bankname;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Total_earning {
        @Expose
        @SerializedName("inr")
        private String inr;
        @Expose
        @SerializedName("dollor")
        private String dollor;

        public String getInr() {
            return inr;
        }

        public void setInr(String inr) {
            this.inr = inr;
        }

        public String getDollor() {
            return dollor;
        }

        public void setDollor(String dollor) {
            this.dollor = dollor;
        }
    }
}
