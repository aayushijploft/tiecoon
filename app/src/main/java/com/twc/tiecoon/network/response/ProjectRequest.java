package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class ProjectRequest {


    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("reject")
        private int reject;
        @Expose
        @SerializedName("accepct")
        private int accepct;
        @Expose
        @SerializedName("massage")
        private String massage;
        @Expose
        @SerializedName("ownername")
        private String ownername;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("request_id")
        private int request_id;
        @Expose
        @SerializedName("ownername_id")
        private int ownername_id;
        @Expose
        @SerializedName("project_id")
        private int project_id;
        @Expose
        @SerializedName("quickbloxId")
        private String quickblox_id;

        public int getReject() {
            return reject;
        }

        public void setReject(int reject) {
            this.reject = reject;
        }

        public int getAccepct() {
            return accepct;
        }

        public void setAccepct(int accepct) {
            this.accepct = accepct;
        }

        public String getMassage() {
            return massage;
        }

        public void setMassage(String massage) {
            this.massage = massage;
        }

        public String getOwnername() {
            return ownername;
        }

        public void setOwnername(String ownername) {
            this.ownername = ownername;
        }

        public String getQuickblox_id() {
            return quickblox_id;
        }

        public void setQuickblox_id(String quickblox_id) {
            this.quickblox_id = quickblox_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getRequest_id() {
            return request_id;
        }

        public void setRequest_id(int request_id) {
            this.request_id = request_id;
        }

        public int getOwnername_id() {
            return ownername_id;
        }

        public void setOwnername_id(int ownername_id) {
            this.ownername_id = ownername_id;
        }

        public int getProject_id() {
            return project_id;
        }

        public void setProject_id(int project_id) {
            this.project_id = project_id;
        }
    }
}
