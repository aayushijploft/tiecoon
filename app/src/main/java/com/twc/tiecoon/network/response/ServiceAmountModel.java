package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class ServiceAmountModel {

    @Expose
    @SerializedName("data")
    public Data data;
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    @SerializedName("success")
    public String success;

    public static class Data {
        @Expose
        @SerializedName("relese_amount")
        public String relese_amount;
        @Expose
        @SerializedName("price")
        public String price;
        @Expose
        @SerializedName("service_end")
        public String service_end;
      @Expose
        @SerializedName("end_service")
        public String end_service ;
    }
}
