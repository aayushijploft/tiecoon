package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeUserDetailsFeedback implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("talant_id")
    @Expose
    private Object talantId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("rating")
    @Expose
    private Integer rating;
    @SerializedName("massage")
    @Expose
    private String massage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tresh")
    @Expose
    private String tresh;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("user")
    @Expose
    private HomeUserDetailsUser user;
    @SerializedName("user_detils")
    @Expose
    private HomeUserDetailsUserDetils userDetils;

    protected HomeUserDetailsFeedback(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            userId = null;
        } else {
            userId = in.readInt();
        }
        if (in.readByte() == 0) {
            rating = null;
        } else {
            rating = in.readInt();
        }
        massage = in.readString();
        status = in.readString();
        tresh = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
    }

    public static final Creator<HomeUserDetailsFeedback> CREATOR = new Creator<HomeUserDetailsFeedback>() {
        @Override
        public HomeUserDetailsFeedback createFromParcel(Parcel in) {
            return new HomeUserDetailsFeedback(in);
        }

        @Override
        public HomeUserDetailsFeedback[] newArray(int size) {
            return new HomeUserDetailsFeedback[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getTalantId() {
        return talantId;
    }

    public void setTalantId(Object talantId) {
        this.talantId = talantId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTresh() {
        return tresh;
    }

    public void setTresh(String tresh) {
        this.tresh = tresh;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public HomeUserDetailsUser getUser() {
        return user;
    }

    public void setUser(HomeUserDetailsUser user) {
        this.user = user;
    }

    public HomeUserDetailsUserDetils getUserDetils() {
        return userDetils;
    }

    public void setUserDetils(HomeUserDetailsUserDetils userDetils) {
        this.userDetils = userDetils;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        if (userId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(userId);
        }
        if (rating == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(rating);
        }
        parcel.writeString(massage);
        parcel.writeString(status);
        parcel.writeString(tresh);
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
    }
}
