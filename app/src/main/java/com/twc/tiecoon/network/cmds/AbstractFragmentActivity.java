package com.twc.tiecoon.network.cmds;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Observer;

import androidx.appcompat.app.AppCompatActivity;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.Activities.BaseActivity;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;

public abstract class AbstractFragmentActivity extends AppCompatActivity implements Observer {
    protected BasicModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            model = getModel();
            if (model != null)
                model.addObserver(this);

            onCreatePost(savedInstanceState);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    prepareUser();
                }
            },20000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepareUser() {
        QBUser qbUser = new QBUser();
        qbUser.setLogin(ApplicationClass.appPreferences.getPhoneNumber());
//         qbUser.setFullName(usernameEt.getText().toString().trim());
        qbUser.setPassword(ApplicationClass.appPreferences.getPhoneNumber());
        //qbUser.setPassword(App.USER_DEFAULT_PASSWORD);
        signIn(qbUser);
    }

    private void signIn(final QBUser user) {

        ChatHelper.getInstance().login(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser userFromRest, Bundle bundle) {
                if (userFromRest.getId().equals(user.getId())) {
                    loginToChat(user);
//                    Toast.makeText(UserHomeActivity.this, "login", Toast.LENGTH_SHORT).show();
                } else {
                    //Need to set password NULL, because server will update user only with NULL password
                    user.setPassword(null);
                    Toast.makeText(AbstractFragmentActivity.this, "register", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    private void loginToChat(final QBUser user) {
        //Need to set password, because the server will not register to chat without password
        user.setPassword(ApplicationClass.appPreferences.getPhoneNumber());
        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                AppPreferences.getInstance().saveQbUser(user);
//                finish();
//                hideProgressDialog();
            }

            @Override
            public void onError(QBResponseException e) {
//                hideProgressDialog();
//                showErrorSnackbar(R.string.login_chat_login_error, e, null);
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int location[] = new int[2];
            w.getLocationOnScreen(location);
            float x = event.getRawX() + w.getLeft() - location[0];
            float y = event.getRawY() + w.getTop() - location[1];
            if (event.getAction() == MotionEvent.ACTION_DOWN
                    && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }

    protected abstract void onCreatePost(Bundle savedInstanceState);

    protected abstract BasicModel getModel();

}
