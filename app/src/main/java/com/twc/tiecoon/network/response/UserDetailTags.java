package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetailTags implements Parcelable {

    @Expose
    @SerializedName("talent_id")
    private int talent_id;
    @Expose
    @SerializedName("user_id")
    private int user_id;
    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("username")
    public String username;

    public int getTalent_id() {
        return talent_id;
    }

    public void setTalent_id(int talent_id) {
        this.talent_id = talent_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    protected UserDetailTags(Parcel in) {
        talent_id = in.readInt();
        user_id = in.readInt();
        id = in.readInt();
        username = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(talent_id);
        dest.writeInt(user_id);
        dest.writeInt(id);
        dest.writeString(username);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UserDetailTags> CREATOR = new Creator<UserDetailTags>() {
        @Override
        public UserDetailTags createFromParcel(Parcel in) {
            return new UserDetailTags(in);
        }

        @Override
        public UserDetailTags[] newArray(int size) {
            return new UserDetailTags[size];
        }
    };
}
