package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class ProjectTemplatesModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    @SerializedName("success")
    public String success;

    public static class Data {
        @Expose
        @SerializedName("updated_at")
        public String updated_at;
        @Expose
        @SerializedName("created_at")
        public String created_at;
        @Expose
        @SerializedName("description")
        public String description;
        @Expose
        @SerializedName("title")
        public String title;
        @Expose
        @SerializedName("id")
        public String id;
    }
}
