package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TransactionListModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    @SerializedName("success")
    public String success;

    public static class Data {
        @Expose
        @SerializedName("created_at")
        public String created_at;
        @Expose
        @SerializedName("time")
        public String time;
        @Expose
        @SerializedName("date")
        public String date;
        @Expose
        @SerializedName("download")
        public String download;
        @Expose
        @SerializedName("type_id")
        public String type_id;
        @Expose
        @SerializedName("type")
        public String type;
        @Expose
        @SerializedName("massage")
        public String massage;
        @Expose
        @SerializedName("digitid")
        public String digitid;
        @Expose
        @SerializedName("trangctionId")
        public String trangctionId;
        @Expose
        @SerializedName("id")
        public String id;
        @Expose
        @SerializedName("order_id")
        public String order_id;
        @Expose
        @SerializedName("productname")
        public String productname;
        @Expose
        @SerializedName("tracking_id")
        public String tracking_id;
        @Expose
        @SerializedName("tracking_link")
        public String tracking_link;
        @Expose
        @SerializedName("is_release")
        public String is_release;
        @Expose
        @SerializedName("price")
        public String price;
        @Expose
        @SerializedName("self")
        public String self;
        @Expose
        @SerializedName("collabId")
        public String collabId;
        @Expose
        @SerializedName("booking_id")
        public String booking_id;
        @Expose
        @SerializedName("currency")
        public String currency;
        @Expose
        @SerializedName("is_owner")
        public String is_owner;
        @Expose
        @SerializedName("service_id")
        public String service_id;
        @Expose
        @SerializedName("service_digitid")
        public String service_digitid;
        @Expose
        @SerializedName("service_owner_id")
        public String service_owner_id;
        @Expose
        @SerializedName("feedback")
        public String feedback;
        @Expose
        @SerializedName("is_feedback")
        public String is_feedback;
        @Expose
        @SerializedName("username")
        public String username;
    }
}
