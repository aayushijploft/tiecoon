package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class ProfileCreateAssignment implements Parcelable {

    @Expose
    @SerializedName("assignement_user")
    private List<Assignement_user> assignement_user;
    @Expose
    @SerializedName("language")
    private Language language;
    @Expose
    @SerializedName("country")
    private Country country;
    @Expose
    @SerializedName("state")
    private State state;
    @Expose
    @SerializedName("city")
    private City city;
    @Expose
    @SerializedName("age")
    private Age age;
    @Expose
    @SerializedName("talent")
    private String talent;
    @Expose
    @SerializedName("deleted_at")
    private String deleted_at;
    @Expose
    @SerializedName("currency")
    private String currency;
    @Expose
    @SerializedName("location")
    private String location;
    @Expose
    @SerializedName("youtublink")
    private String youtublink;
    @Expose
    @SerializedName("city_id")
    private String city_id;
    @Expose
    @SerializedName("state_id")
    private int state_id;
    @Expose
    @SerializedName("country_id")
    private int country_id;
    @Expose
    @SerializedName("language_id")
    private int language_id;
    @Expose
    @SerializedName("age_id")
    private int age_id;
    @Expose
    @SerializedName("gender")
    private String gender;
    @Expose
    @SerializedName("image")
    private String image;
    @Expose
    @SerializedName("path")
    private String path;
    @Expose
    @SerializedName("user_id")
    private int user_id;
    @Expose
    @SerializedName("updated_at")
    private String updated_at;
    @Expose
    @SerializedName("created_at")
    private String created_at;
    @Expose
    @SerializedName("tresh")
    private String tresh;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("end_date")
    private String end_date;
    @Expose
    @SerializedName("start_date")
    private String start_date;
    @Expose
    @SerializedName("amount")
    private int amount;
    @Expose
    @SerializedName("text")
    private String text;
    @Expose
    @SerializedName("talant_id")
    private String talant_id;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("id")
    private int id;

    protected ProfileCreateAssignment(Parcel in) {
        assignement_user = in.createTypedArrayList(Assignement_user.CREATOR);
        talent = in.readString();
        deleted_at = in.readString();
        currency = in.readString();
        location = in.readString();
        youtublink = in.readString();
        city_id = in.readString();
        state_id = in.readInt();
        country_id = in.readInt();
        language_id = in.readInt();
        age_id = in.readInt();
        gender = in.readString();
        image = in.readString();
        path = in.readString();
        user_id = in.readInt();
        updated_at = in.readString();
        created_at = in.readString();
        tresh = in.readString();
        status = in.readString();
        end_date = in.readString();
        start_date = in.readString();
        amount = in.readInt();
        text = in.readString();
        talant_id = in.readString();
        name = in.readString();
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(assignement_user);
        dest.writeString(talent);
        dest.writeString(deleted_at);
        dest.writeString(currency);
        dest.writeString(location);
        dest.writeString(youtublink);
        dest.writeString(city_id);
        dest.writeInt(state_id);
        dest.writeInt(country_id);
        dest.writeInt(language_id);
        dest.writeInt(age_id);
        dest.writeString(gender);
        dest.writeString(image);
        dest.writeString(path);
        dest.writeInt(user_id);
        dest.writeString(updated_at);
        dest.writeString(created_at);
        dest.writeString(tresh);
        dest.writeString(status);
        dest.writeString(end_date);
        dest.writeString(start_date);
        dest.writeInt(amount);
        dest.writeString(text);
        dest.writeString(talant_id);
        dest.writeString(name);
        dest.writeInt(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProfileCreateAssignment> CREATOR = new Creator<ProfileCreateAssignment>() {
        @Override
        public ProfileCreateAssignment createFromParcel(Parcel in) {
            return new ProfileCreateAssignment(in);
        }

        @Override
        public ProfileCreateAssignment[] newArray(int size) {
            return new ProfileCreateAssignment[size];
        }
    };

    public List<Assignement_user> getAssignement_user() {
        return assignement_user;
    }

    public void setAssignement_user(List<Assignement_user> assignement_user) {
        this.assignement_user = assignement_user;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }



    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Age getAge() {
        return age;
    }

    public void setAge(Age age) {
        this.age = age;
    }

    public String getTalent() {
        return talent;
    }

    public void setTalent(String talent) {
        this.talent = talent;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getYoutublink() {
        return youtublink;
    }

    public void setYoutublink(String youtublink) {
        this.youtublink = youtublink;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public int getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(int language_id) {
        this.language_id = language_id;
    }

    public int getAge_id() {
        return age_id;
    }

    public void setAge_id(int age_id) {
        this.age_id = age_id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTresh() {
        return tresh;
    }

    public void setTresh(String tresh) {
        this.tresh = tresh;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTalant_id() {
        return talant_id;
    }

    public void setTalant_id(String talant_id) {
        this.talant_id = talant_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static class Assignement_user implements Parcelable{
        @Expose
        @SerializedName("talent")
        private Talent talent;
        @Expose
        @SerializedName("talent_id")
        private int talent_id;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("id")
        private int id;

        protected Assignement_user(Parcel in) {
            talent_id = in.readInt();
            user_id = in.readInt();
            id = in.readInt();
        }



        public static final Creator<Assignement_user> CREATOR = new Creator<Assignement_user>() {
            @Override
            public Assignement_user createFromParcel(Parcel in) {
                return new Assignement_user(in);
            }

            @Override
            public Assignement_user[] newArray(int size) {
                return new Assignement_user[size];
            }
        };

        public Talent getTalent() {
            return talent;
        }

        public void setTalent(Talent talent) {
            this.talent = talent;
        }

        public int getTalent_id() {
            return talent_id;
        }

        public void setTalent_id(int talent_id) {
            this.talent_id = talent_id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {

        }
    }

    public static class Talent  implements Parcelable {
        @Expose
        @SerializedName("updated_at")
        private String updated_at;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("tresh")
        private String tresh;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("parent_id")
        private String parent_id;
        @Expose
        @SerializedName("slug")
        private String slug;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("id")
        private int id;

        protected Talent(Parcel in) {
            updated_at = in.readString();
            created_at = in.readString();
            tresh = in.readString();
            status = in.readString();
            description = in.readString();
            parent_id = in.readString();
            slug = in.readString();
            title = in.readString();
            id = in.readInt();
        }

        public static final Creator<Talent> CREATOR = new Creator<Talent>() {
            @Override
            public Talent createFromParcel(Parcel in) {
                return new Talent(in);
            }

            @Override
            public Talent[] newArray(int size) {
                return new Talent[size];
            }
        };

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getTresh() {
            return tresh;
        }

        public void setTresh(String tresh) {
            this.tresh = tresh;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getParent_id() {
            return parent_id;
        }

        public void setParent_id(String parent_id) {
            this.parent_id = parent_id;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(updated_at);
            dest.writeString(created_at);
            dest.writeString(tresh);
            dest.writeString(status);
            dest.writeString(description);
            dest.writeString(parent_id);
            dest.writeString(slug);
            dest.writeString(title);
            dest.writeInt(id);
        }
    }

    public static class Language  implements Parcelable  {
        @Expose
        @SerializedName("updated_at")
        private String updated_at;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("tresh")
        private String tresh;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("slug")
        private String slug;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        protected Language(Parcel in) {
            updated_at = in.readString();
            created_at = in.readString();
            tresh = in.readString();
            status = in.readString();
            slug = in.readString();
            name = in.readString();
            id = in.readInt();
        }

        public static final Creator<Language> CREATOR = new Creator<Language>() {
            @Override
            public Language createFromParcel(Parcel in) {
                return new Language(in);
            }

            @Override
            public Language[] newArray(int size) {
                return new Language[size];
            }
        };

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getTresh() {
            return tresh;
        }

        public void setTresh(String tresh) {
            this.tresh = tresh;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(updated_at);
            dest.writeString(created_at);
            dest.writeString(tresh);
            dest.writeString(status);
            dest.writeString(slug);
            dest.writeString(name);
            dest.writeInt(id);
        }
    }

    public static class Country   implements Parcelable {
        @Expose
        @SerializedName("updated_at")
        private String updated_at;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("deleted_at")
        private String deleted_at;
        @Expose
        @SerializedName("phonecode")
        private int phonecode;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("sortname")
        private String sortname;
        @Expose
        @SerializedName("id")
        private int id;

        protected Country(Parcel in) {
            updated_at = in.readString();
            created_at = in.readString();
            deleted_at = in.readString();
            phonecode = in.readInt();
            name = in.readString();
            sortname = in.readString();
            id = in.readInt();
        }

        public static final Creator<Country> CREATOR = new Creator<Country>() {
            @Override
            public Country createFromParcel(Parcel in) {
                return new Country(in);
            }

            @Override
            public Country[] newArray(int size) {
                return new Country[size];
            }
        };

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(String deleted_at) {
            this.deleted_at = deleted_at;
        }

        public int getPhonecode() {
            return phonecode;
        }

        public void setPhonecode(int phonecode) {
            this.phonecode = phonecode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSortname() {
            return sortname;
        }

        public void setSortname(String sortname) {
            this.sortname = sortname;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(updated_at);
            dest.writeString(created_at);
            dest.writeString(deleted_at);
            dest.writeInt(phonecode);
            dest.writeString(name);
            dest.writeString(sortname);
            dest.writeInt(id);
        }
    }

    public static class State  implements Parcelable  {
        @Expose
        @SerializedName("updated_at")
        private String updated_at;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("deleted_at")
        private String deleted_at;
        @Expose
        @SerializedName("country_id")
        private int country_id;
        @Expose
        @SerializedName("statecode")
        private String statecode;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        protected State(Parcel in) {
            updated_at = in.readString();
            created_at = in.readString();
            deleted_at = in.readString();
            country_id = in.readInt();
            statecode = in.readString();
            name = in.readString();
            id = in.readInt();
        }

        public static final Creator<State> CREATOR = new Creator<State>() {
            @Override
            public State createFromParcel(Parcel in) {
                return new State(in);
            }

            @Override
            public State[] newArray(int size) {
                return new State[size];
            }
        };

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(String deleted_at) {
            this.deleted_at = deleted_at;
        }

        public int getCountry_id() {
            return country_id;
        }

        public void setCountry_id(int country_id) {
            this.country_id = country_id;
        }

        public String getStatecode() {
            return statecode;
        }

        public void setStatecode(String statecode) {
            this.statecode = statecode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(updated_at);
            dest.writeString(created_at);
            dest.writeString(deleted_at);
            dest.writeInt(country_id);
            dest.writeString(statecode);
            dest.writeString(name);
            dest.writeInt(id);
        }
    }
    public static class City  implements Parcelable  {

        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;


        protected City(Parcel in) {
            name = in.readString();
            id = in.readInt();
        }

        public static final Creator<City> CREATOR = new Creator<City>() {
            @Override
            public City createFromParcel(Parcel in) {
                return new City(in);
            }

            @Override
            public City[] newArray(int size) {
                return new City[size];
            }
        };

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeInt(id);
        }
    }

    public static class Age  implements Parcelable  {
        @Expose
        @SerializedName("age")
        private String age;
        @Expose
        @SerializedName("id")
        private int id;

        protected Age(Parcel in) {
            age = in.readString();
            id = in.readInt();
        }

        public static final Creator<Age> CREATOR = new Creator<Age>() {
            @Override
            public Age createFromParcel(Parcel in) {
                return new Age(in);
            }

            @Override
            public Age[] newArray(int size) {
                return new Age[size];
            }
        };

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(age);
            dest.writeInt(id);
        }
    }

    @Override
    public String toString() {
        return "ProfileCreateAssignment{" +
                "assignement_user=" + assignement_user +
                ", language=" + language +
                ", country=" + country +
                ", state=" + state +
                ", city=" + city +
                ", age=" + age +
                ", talent='" + talent + '\'' +
                ", deleted_at='" + deleted_at + '\'' +
                ", currency='" + currency + '\'' +
                ", location='" + location + '\'' +
                ", youtublink='" + youtublink + '\'' +
                ", city_id='" + city_id + '\'' +
                ", state_id=" + state_id +
                ", country_id=" + country_id +
                ", language_id=" + language_id +
                ", age_id=" + age_id +
                ", gender='" + gender + '\'' +
                ", image='" + image + '\'' +
                ", path='" + path + '\'' +
                ", user_id=" + user_id +
                ", updated_at='" + updated_at + '\'' +
                ", created_at='" + created_at + '\'' +
                ", tresh='" + tresh + '\'' +
                ", status='" + status + '\'' +
                ", end_date='" + end_date + '\'' +
                ", start_date='" + start_date + '\'' +
                ", amount=" + amount +
                ", text='" + text + '\'' +
                ", talant_id='" + talant_id + '\'' +
                ", name='" + name + '\'' +
                ", id=" + id +
                '}';
    }
}
