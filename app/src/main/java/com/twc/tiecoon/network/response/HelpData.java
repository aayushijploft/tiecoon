package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HelpData {
    @SerializedName("name")
    @Expose
    private String  name;

    @SerializedName("email")
    @Expose
    private String  email;

    @SerializedName("description")
    @Expose
    private String  description;

    @SerializedName("updated_at")
    @Expose
    private String  updated_at;

    @SerializedName("created_at")
    @Expose
    private String  created_at;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
