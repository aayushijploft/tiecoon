package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class VerifyPhoneModel {

    @Expose
    @SerializedName("data")
    public Data data;
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    @SerializedName("success")
    public String success;

    public static class Data {
        @Expose
        @SerializedName("phone")
        public String phone;
        @Expose
        @SerializedName("email")
        public String email;
        @Expose
        @SerializedName("isverfy")
        public String isverfy;
        @Expose
        @SerializedName("userId")
        public String userid;
        @Expose
        @SerializedName("type")
        public String type;
    }
}
