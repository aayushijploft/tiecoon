package com.twc.tiecoon.network.cmds;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Observer;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;

public abstract class AbstractFragment extends Fragment implements Observer {
    protected BasicModel model;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        model = getModel();
        if (model != null)
            model.addObserver(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                prepareUser();
            }
        },20000);

        return onCreateViewPost(inflater, container, savedInstanceState);
    }

    protected abstract View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);

    protected abstract BasicModel getModel();

    private void prepareUser() {
        QBUser qbUser = new QBUser();
        qbUser.setLogin(ApplicationClass.appPreferences.getPhoneNumber());
        qbUser.setPassword(ApplicationClass.appPreferences.getPhoneNumber());
        signIn(qbUser);
    }

    private void signIn(final QBUser user) {

        ChatHelper.getInstance().login(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser userFromRest, Bundle bundle) {
                if (userFromRest.getId().equals(user.getId())) {
                    loginToChat(user);
//                    Toast.makeText(UserHomeActivity.this, "login", Toast.LENGTH_SHORT).show();
                } else {
                    //Need to set password NULL, because server will update user only with NULL password
                    user.setPassword(null);
                    Toast.makeText(getContext(), "register", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    private void loginToChat(final QBUser user) {
        //Need to set password, because the server will not register to chat without password
        user.setPassword(ApplicationClass.appPreferences.getPhoneNumber());
        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                AppPreferences.getInstance().saveQbUser(user);
//                if(getActivity()!=null){
//                    getActivity().finish();
//                }

//                hideProgressDialog();
            }

            @Override
            public void onError(QBResponseException e) {
//                hideProgressDialog();
//                showErrorSnackbar(R.string.login_chat_login_error, e, null);
            }
        });
    }

}