package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StoryViewData {


    @SerializedName("id")
    @Expose
    private Integer id;



    @SerializedName("profile_image")
    @Expose
    private String profile_image;

    @SerializedName("story")
    @Expose
    private List<StoryViewImagesList> story;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public List<StoryViewImagesList> getStory() {
        return story;
    }

    public void setStory(List<StoryViewImagesList> story) {
        this.story = story;
    }
}

