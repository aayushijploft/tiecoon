package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryCurrencyData {
    @SerializedName("category")
    @Expose
    private List<CategoryData> category = null;
    @SerializedName("price")
    @Expose
    private List<CurrencyData> price = null;

    public List<CategoryData> getCategory() {
        return category;
    }

    public void setCategory(List<CategoryData> category) {
        this.category = category;
    }

    public List<CurrencyData> getPrice() {
        return price;
    }

    public void setPrice(List<CurrencyData> price) {
        this.price = price;
    }
}
