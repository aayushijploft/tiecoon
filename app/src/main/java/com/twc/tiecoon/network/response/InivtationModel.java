package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class InivtationModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    @SerializedName("success")
    public String success;

    public static class Data {
        @Expose
        @SerializedName("reject")
        public int reject;
        @Expose
        @SerializedName("accepct")
        public int accepct;
        @Expose
        @SerializedName("massage")
        public String massage;
        @Expose
        @SerializedName("user_name")
        public String user_name;
        @Expose
        @SerializedName("user_id")
        public String user_id;
        @Expose
        @SerializedName("quickbloxId")
        public String quickbloxId;
        @Expose
        @SerializedName("ownername_id")
        public String ownername_id;
        @Expose
        @SerializedName("ownername")
        public String ownername;
        @Expose
        @SerializedName("project_id")
        public String project_id;
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("request_id")
        public String request_id;
    }
}
