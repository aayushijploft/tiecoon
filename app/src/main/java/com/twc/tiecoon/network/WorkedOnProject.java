package com.twc.tiecoon.network;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class WorkedOnProject implements Parcelable {

    @Expose
    @SerializedName("qbid")
    private String qbid;
    @Expose
    @SerializedName("amount")
    private int amount;
    @Expose
    @SerializedName("currency")
    private String currency;
    @Expose
    @SerializedName("text")
    private String text;
    @Expose
    @SerializedName("name")
    private String name;
    @Expose
    @SerializedName("project_id")
    private int project_id;
    @Expose
    @SerializedName("project_owner_name")
    private String project_owner_name;
    @Expose
    @SerializedName("user_id")
    private int user_id;
    @Expose
    @SerializedName("id")
    private int id;

    protected WorkedOnProject(Parcel in) {
        amount = in.readInt();
        currency = in.readString();
        text = in.readString();
        name = in.readString();
        qbid = in.readString();
        project_owner_name = in.readString();
        project_id = in.readInt();
        user_id = in.readInt();
        id = in.readInt();
    }

    public static final Creator<WorkedOnProject> CREATOR = new Creator<WorkedOnProject>() {
        @Override
        public WorkedOnProject createFromParcel(Parcel in) {
            return new WorkedOnProject(in);
        }

        @Override
        public WorkedOnProject[] newArray(int size) {
            return new WorkedOnProject[size];
        }
    };

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProject_owner_name() {
        return project_owner_name;
    }

    public void setProject_owner_name(String project_owner_name) {
        this.project_owner_name = project_owner_name;
    }

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQbid() {
        return qbid;
    }

    public void setQbid(String qbid) {
        this.qbid = qbid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(amount);
        dest.writeString(currency);
        dest.writeString(text);
        dest.writeString(name);
        dest.writeString(qbid);
        dest.writeString(project_owner_name);
        dest.writeInt(user_id);
        dest.writeInt(project_id);
        dest.writeInt(id);
    }
}
