package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeUserDetailsService implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("watermark")
    @Expose
    private String watermark;
    @SerializedName("createimage")
    @Expose
    private String createimage;
    @SerializedName("fontsize")
    @Expose
    private String fontsize;
    @SerializedName("fontcolor")
    @Expose
    private String fontcolor;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private Integer subcategoryId;
    @SerializedName("pricecode")
    @Expose
    private String pricecode;
    @SerializedName("day")
    @Expose
    private String day;
    @SerializedName("price")
    @Expose
    private Long price;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tresh")
    @Expose
    private String tresh;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("uploadtype")
    @Expose
    private Integer uploadtype;
    @SerializedName("qbid")
    @Expose
    private String quickbloxuser_id;
    @SerializedName("days")
    @Expose
    private String days;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("category")
    @Expose
    private AddProductCategoryData category=null;
    @SerializedName("sub_category")
    @Expose
    private AddProductSubCategoryData subCategory=null;
    @SerializedName("upload_image")
    @Expose
    private List<UploadImageListData> upload_image = null;

    protected HomeUserDetailsService(Parcel in) {
        category = in.readParcelable(getClass().getClassLoader());
        subCategory = in.readParcelable(getClass().getClassLoader());
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            userId = null;
        } else {
            userId = in.readInt();
        }
        image = in.readString();
        path = in.readString();
        watermark = in.readString();
        quickbloxuser_id = in.readString();
        createimage = in.readString();
        fontsize = in.readString();
        fontcolor = in.readString();
        color = in.readString();
        if (in.readByte() == 0) {
            categoryId = null;
        } else {
            categoryId = in.readInt();
        }
        if (in.readByte() == 0) {
            subcategoryId = null;
        } else {
            subcategoryId = in.readInt();
        }
        pricecode = in.readString();
        day = in.readString();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readLong();
        }
        description = in.readString();
        status = in.readString();
        tresh = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        if (in.readByte() == 0) {
            uploadtype = null;
        } else {
            uploadtype = in.readInt();
        }
        days = in.readString();
        location = in.readString();
        category = in.readParcelable(AddProductCategoryData.class.getClassLoader());
        subCategory = in.readParcelable(AddProductSubCategoryData.class.getClassLoader());
        upload_image = in.createTypedArrayList(UploadImageListData.CREATOR);
    }

    public static final Creator<HomeUserDetailsService> CREATOR = new Creator<HomeUserDetailsService>() {
        @Override
        public HomeUserDetailsService createFromParcel(Parcel in) {
            return new HomeUserDetailsService(in);
        }

        @Override
        public HomeUserDetailsService[] newArray(int size) {
            return new HomeUserDetailsService[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getImage() {
        return image;
    }

    public String getPath() {
        return path;
    }

    public String getWatermark() {
        return watermark;
    }

    public String getCreateimage() {
        return createimage;
    }

    public String getFontsize() {
        return fontsize;
    }

    public String getFontcolor() {
        return fontcolor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public String getPricecode() {
        return pricecode;
    }

    public Long getPrice() {
        return price;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDescription() {
        return description;
    }

    public String getStatus() {
        return status;
    }

    public String getTresh() {
        return tresh;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public Integer getUploadtype() {
        return uploadtype;
    }

    public String getDays() {
        return days;
    }

    public String getLocation() {
        return location;
    }

    public AddProductCategoryData getCategory() {
        return category;
    }

    public AddProductSubCategoryData getSubCategory() {
        return subCategory;
    }

    public List<UploadImageListData> getUpload_image() {
        return upload_image;
    }

    public String getQuickbloxuser_id() {
        return quickbloxuser_id;
    }

    public void setQuickbloxuser_id(String quickbloxuser_id) {
        this.quickbloxuser_id = quickbloxuser_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(category, i);
        parcel.writeParcelable(subCategory, i);
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        if (userId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(userId);
        }
        parcel.writeString(image);
        parcel.writeString(path);
        parcel.writeString(watermark);
        parcel.writeString(quickbloxuser_id);
        parcel.writeString(createimage);
        parcel.writeString(fontsize);
        parcel.writeString(fontcolor);
        parcel.writeString(color);
        if (categoryId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(categoryId);
        }
        if (subcategoryId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(subcategoryId);
        }
        parcel.writeString(pricecode);
        parcel.writeString(day);
        if (price == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(price);
        }
        parcel.writeString(description);
        parcel.writeString(status);
        parcel.writeString(tresh);
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
        if (uploadtype == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(uploadtype);
        }
        parcel.writeString(days);
        parcel.writeString(location);
        parcel.writeParcelable(category, i);
        parcel.writeParcelable(subCategory, i);
        parcel.writeTypedList(upload_image);
    }

    @Override
    public String toString() {
        return "HomeUserDetailsService{" +
                "id=" + id +
                ", userId=" + userId +
                ", image='" + image + '\'' +
                ", path='" + path + '\'' +
                ", watermark='" + watermark + '\'' +
                ", createimage='" + createimage + '\'' +
                ", fontsize='" + fontsize + '\'' +
                ", fontcolor='" + fontcolor + '\'' +
                ", categoryId=" + categoryId +
                ", subcategoryId=" + subcategoryId +
                ", pricecode='" + pricecode + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", tresh='" + tresh + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", uploadtype=" + uploadtype +
                ", days='" + days + '\'' +
                ", location='" + location + '\'' +
                ", category=" + category +
                ", subCategory=" + subCategory +
                ", upload_image=" + upload_image +
                '}';
    }
}