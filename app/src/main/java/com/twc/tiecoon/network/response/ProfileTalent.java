package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileTalent implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    private String isseletcted = "0";

    protected ProfileTalent(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        name = in.readString();
    }

    public static final Creator<ProfileTalent> CREATOR = new Creator<ProfileTalent>() {
        @Override
        public ProfileTalent createFromParcel(Parcel in) {
            return new ProfileTalent(in);
        }

        @Override
        public ProfileTalent[] newArray(int size) {
            return new ProfileTalent[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public String getIsseletcted() {
        return isseletcted;
    }

    public void setIsseletcted(String isseletcted) {
        this.isseletcted = isseletcted;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(name);
    }
}
