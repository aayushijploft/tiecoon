package com.twc.tiecoon.network;

import com.twc.tiecoon.Activities.views.RemoveUser;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.*;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @FormUrlEncoded
    @POST(Constant.REGISTER)
    Call<OTPVerification> registerByPhone(@Field("type") String type, @Field("phone") String phone);

    @FormUrlEncoded
    @POST(Constant.SENDOTPFORSOCIAL)
    Call<OTPVerificationSocial> sendOtp(@Field("email") String type, @Field("phone") String phone);

    @FormUrlEncoded
    @POST(Constant.RESENDOTP)
    Call<OTPVerificationSocial> ResendOtp( @Field("phone") String phone);

    @FormUrlEncoded
    @POST(Constant.REGISTERSOCIAL)
    Call<OTPVerification> registerBySocial(@Field("appid") String appid, @Field("image") String image,
                                          @Field("type") String type, @Field("email") String email, @Field("phone") String phone);

    @FormUrlEncoded
    @POST(Constant.VERIFYOTP)
    Call<OTPVerification> verifyOTP(@Field("type") String type,@Field("phone") String phone,
                                    @Field("otp") String otp);

    @FormUrlEncoded
    @POST(Constant.VERIFYOTP)
    Call<OTPVerification> verifyEmailOTP(@Field("type") String type,@Field("email") String phone,
                                    @Field("otp") String otp);


    @Multipart
    @POST(Constant.UPDATEPROFILE)
    Call<ProfileFirstUpdate> updateProfile(@Header("Authorization") String token, @Part MultipartBody.Part createImage,
                                           @Part("name") RequestBody name, @Part("status") RequestBody status, @Part("email") RequestBody email,
                                           @Part("phone") RequestBody phone, @Part("gender") RequestBody gender,
                                           @Part("age") int age, @Part("location") RequestBody location,
                                           @Part("bio") RequestBody bio, @Query("talents[]") ArrayList<String> talents_id, @Query("language[]") ArrayList<Integer> language);

    @POST(Constant.GETPROFILE)
    Call<Profile> getUserDetails(@Header("Authorization") String token);

    @POST(Constant.GETCATEGORYCURRENCY)
    Call<CategoryCurrency> getCategoryCurrency(@Header("Authorization") String token);

    @POST(Constant.GETPRODUCTCATEGORY)
    Call<ProductCategory> getProductCategory(@Header("Authorization") String token);

    @FormUrlEncoded
     @POST(Constant.GETPRODUCTSUBCATEGORY)
    Call<ProductCategory> getProductSubCategory(@Header("Authorization") String token, @Field("id") String id);

   @POST(Constant.GETSERVICECATEGORY)
    Call<ServiceCategory> getServiceCategory(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(Constant.GETSERVICESUBCATEGORY)
    Call<ServiceCategory> getServiceSubCategory(@Header("Authorization") String token, @Field("id") String id);

    @POST(Constant.PRODUCTCATEGORY)
    Call<CategoryCurrency> getProductCategoryCurrency(@Header("Authorization") String token);

    @POST(Constant.SERVICECATEGORY)
    Call<CategoryCurrency> getServiceCategoryCurrency(@Header("Authorization") String token);

    @POST(Constant.GETTALENTCURRENCY)
    Call<TalentCurrency> getTalentCurrency(@Header("Authorization") String token);

    @POST(Constant.TALENTCAT)
    Call<TalantCat> getTalentCat(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(Constant.SUBCATEGORY)
    Call<SubCategory> getSubCategory(@Header("Authorization") String token, @Field("id") String id);

    @Multipart
    @POST(Constant.UPLOADTALENTS)
    Call<AddTalent> uploadTalents(@Header("Authorization") String token, @Part("watermark") RequestBody waterMark,
                                  @Part  MultipartBody.Part createImage,
                                  @Part MultipartBody.Part[] partList,
                                  @Part("fontsize") RequestBody fontSize, @Part("fontcolor") RequestBody fontColor,
                                  @Query("user_id[]") ArrayList<String> user_id,
                                  @Part MultipartBody.Part videothumbnail,
                                  @Part MultipartBody.Part image,
                                  @Part MultipartBody.Part audio,
                                  @Part MultipartBody.Part video,
                                  @Part("description") RequestBody description,
                                  @Part("textarea") RequestBody textarea,
                                  @Part("price") RequestBody price,
                                  @Part("pricecode") RequestBody priceCode,
                                  @Part("subcategory_id") RequestBody subcategory_id,
                                  @Part("category_id") RequestBody category_id);

    @Multipart
    @POST(Constant.CREATEPRODUCT)
    Call<CreateProduct> createProduct(@Header("Authorization") String token,
                                      @Part MultipartBody.Part[] partList,
                                      @Part MultipartBody.Part video,
                                      @Part MultipartBody.Part videothumbnail,
                                      @Part("category_id") RequestBody category_id,
                                      @Part("subcategory_id") RequestBody subcategory_id,
                                      @Part("currency") RequestBody currency,
                                      @Part("amount") RequestBody amount,
                                      @Part("qty") RequestBody qty,
                                      @Part("outofstock") RequestBody outofstock,
                                      @Part("description") RequestBody description,
                                      @Query("user_id[]") ArrayList<String> user_id
                                      ,@Part("location") RequestBody location
                                      ,@Part("name") RequestBody name
                                      ,@Part("phone") RequestBody phone
                                      ,@Part("pincode") RequestBody pincode
                                      ,@Part("address") RequestBody address
                                      ,@Part("town") RequestBody town
                                      ,@Part("city") RequestBody city
                                      ,@Part("state") RequestBody state
                                    );

     @Multipart
    @POST(Constant.UPDATEPRODUCT)
    Call<CreateProduct> EditProduct(@Header("Authorization") String token,
                                      @Part MultipartBody.Part[] partList,
                                      @Part MultipartBody.Part video,
                                      @Part MultipartBody.Part videothumbnail,
                                      @Part("product_id") RequestBody product_id,
                                      @Part("category_id") RequestBody category_id,
                                      @Part("subcategory_id") RequestBody subcategory_id,
                                      @Part("currency") RequestBody currency,
                                      @Part("amount") RequestBody amount,
                                      @Part("qty") RequestBody qty,
                                      @Part("outofstock") RequestBody outofstock,
                                      @Part("description") RequestBody description,
                                      @Query("user_id[]") ArrayList<String> user_id,
                                      @Part("location") RequestBody location,
                                      @Part("name") RequestBody name,
                                      @Part("phone") RequestBody phone,
                                      @Part("pincode") RequestBody pincode,
                                      @Part("address") RequestBody address,
                                      @Part("town") RequestBody town,
                                      @Part("city") RequestBody city,
                                      @Part("state") RequestBody state
                                    );

    @Multipart
    @POST(Constant.CREATEERVICE)
    Call<CreateService> createService(@Header("Authorization") String token,
                                      @Part MultipartBody.Part[] partList,
                                      @Part MultipartBody.Part video,
                                      @Part MultipartBody.Part videothumbnail,
                                      @Part MultipartBody.Part audio,
                                      @Part("text") RequestBody text,
                                      @Part("color") RequestBody color,
                                      @Part("category_id") RequestBody category_id,
                                      @Part("subcategory_id") RequestBody subcategory_id,
                                      @Part("currency") RequestBody currency,
                                      @Part("amount") RequestBody amount,
                                      @Part("description") RequestBody description,
                                      @Query("tag_id[]") ArrayList<String> user_id,
                                      @Part("location") RequestBody location,
                                      @Part("day") RequestBody day);

    @Multipart
    @POST(Constant.UPDATESERVICE)
    Call<CreateService> EditService(@Header("Authorization") String token,
                                      @Part MultipartBody.Part[] partList,
                                      @Part MultipartBody.Part video,
                                      @Part MultipartBody.Part videothumbnail,
                                      @Part MultipartBody.Part audio,
                                      @Part("text") RequestBody text,
                                      @Part("color") RequestBody color,
                                      @Part("service_id") RequestBody service_id,
                                      @Part("category_id") RequestBody category_id,
                                      @Part("subcategory_id") RequestBody subcategory_id,
                                      @Part("currency") RequestBody currency,
                                      @Part("amount") RequestBody amount,
                                      @Part("description") RequestBody description,
                                      @Query("tag_id[]") ArrayList<String> user_id,
                                      @Part("location") RequestBody location,
                                      @Part("day") RequestBody day);

    @Multipart
    @POST(Constant.UPDATETALENTS)
    Call<AddTalent> updateTalents(@Header("Authorization") String token, @Part("watermark") RequestBody waterMark,
                                  @Part MultipartBody.Part createImage,
                                  @Part MultipartBody.Part[] partList,
                                  @Part("fontsize") RequestBody fontSize, @Part("fontcolor") RequestBody fontColor,
                                  @Query("user_id[]") ArrayList<String> user_id,
                                  @Part MultipartBody.Part videothumbnail,
                                  @Part MultipartBody.Part image,
                                  @Part MultipartBody.Part audio,
                                  @Part MultipartBody.Part video,
                                  @Part("description") RequestBody description,
                                  @Part("textarea") RequestBody textarea,
                                  @Part("price") RequestBody price,
                                  @Part("pricecode") RequestBody priceCode,
                                  @Part("subcategory_id") RequestBody subcategory_id,
                                  @Part("category_id") RequestBody category_id,
                                  @Part("talant_id") RequestBody talant_id);

    @FormUrlEncoded
    @POST(Constant.DELETETALENTS)
    Call<DeleteTalents> deleteTalents(@Header("Authorization") String token, @Field("id") String id);

    @Multipart
    @POST(Constant.UPDATEUSERIMAGE)
    Call<DeleteTalents> updateUserImage(@Header("Authorization") String token,@Part MultipartBody.Part createImage);

    @FormUrlEncoded
    @POST(Constant.CREATEASSIGNMENT)
    Call<CreateAssignment> createAssignment(@Header("Authorization") String token,  @FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.UPDATEASSIGNMENT)
    Call<CreateAssignment> updateAssignment(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.DELETEASSIGNMENT)
    Call<DeleteTalents> deleteAssignment(@Header("Authorization") String token, @Field("id") String id);



    @Multipart
    @POST(Constant.CREATEPRODUCT)
    Call<AddProducts> uploadProduct(@Header("Authorization") String token, @Part("watermark") RequestBody waterMark,
                                    @Part MultipartBody.Part createImage,
                                    @Part MultipartBody.Part[] partList,
                                    @Part("fontsize") RequestBody fontSize, @Part("fontcolor") RequestBody fontColor,
                                    @Part MultipartBody.Part image,
                                    @Part("description") RequestBody description,
                                    @Part("price") RequestBody price,
                                    @Part("pricecode") RequestBody priceCode,
                                    @Part("subcategory_id") RequestBody subcategory_id,
                                    @Part("category_id") RequestBody category_id,
                                    @Part("location") RequestBody location);

    @Multipart
    @POST(Constant.UPDATEPRODUCT)
    Call<AddProducts> updateProduct(@Header("Authorization") String token, @Part("watermark") RequestBody waterMark,
                                    @Part MultipartBody.Part createImage,
                                    @Part  MultipartBody.Part[] partList,
                                    @Part("fontsize") RequestBody fontSize, @Part("fontcolor") RequestBody fontColor,
                                    @Part MultipartBody.Part image,
                                    @Part("description") RequestBody description,
                                    @Part("price") RequestBody price,
                                    @Part("pricecode") RequestBody priceCode,
                                    @Part("subcategory_id") RequestBody subcategory_id,
                                    @Part("category_id") RequestBody category_id,
                                    @Part("product_id") RequestBody product_id,
                                    @Part("location") RequestBody location);

    @Multipart
    @POST(Constant.CREATEERVICE)
    Call<AddService> uploadService(@Header("Authorization") String token, @Part("watermark") RequestBody waterMark,
                                   @Part MultipartBody.Part createImage,
                                   @Part MultipartBody.Part[] partList,
                                   @Part("fontsize") RequestBody fontSize, @Part("fontcolor") RequestBody fontColor,
                                   @Part MultipartBody.Part image,
                                   @Part("description") RequestBody description,
                                   @Part("price") RequestBody price,
                                   @Part("pricecode") RequestBody priceCode,
                                   @Part("subcategory_id") RequestBody subcategory_id,
                                   @Part("category_id") RequestBody category_id,
                                   @Part("days") RequestBody days,
                                   @Part("location") RequestBody location);
    @Multipart
    @POST(Constant.UPDATESERVICE)
    Call<AddService> updateService(@Header("Authorization") String token, @Part("watermark") RequestBody waterMark,
                                   @Part MultipartBody.Part createImage,
                                   @Part MultipartBody.Part[] partList,
                                   @Part("fontsize") RequestBody fontSize, @Part("fontcolor") RequestBody fontColor,
                                   @Part MultipartBody.Part image,
                                   @Part("description") RequestBody description,
                                   @Part("price") RequestBody price,
                                   @Part("pricecode") RequestBody priceCode,
                                   @Part("subcategory_id") RequestBody subcategory_id,
                                   @Part("category_id") RequestBody category_id,
                                   @Part("service_id") RequestBody service_id,
                                   @Part("days") RequestBody days,
                                   @Part("location") RequestBody location);

    @Multipart
    @POST(Constant.CREATEBRANDS)
    Call<AddBrands> uploadBrands(@Header("Authorization") String token, @Part MultipartBody.Part image,
                                 @Part("talent_id") RequestBody talent_id, @Part("cat_id") RequestBody cat_id,
                                 @Part("subcat_id") RequestBody subcat_id,
                                 @Part("name") RequestBody name,
                                 @Part("price") RequestBody price,
                                 @Part("description") RequestBody description,
                                 @Part("link1") RequestBody link1,
                                 @Part("link2") RequestBody link2,
                                 @Part("link3") RequestBody link3);

    @Multipart
    @POST(Constant.UPDATEBRANDS)
    Call<AddBrands> updateBrands(@Header("Authorization") String token, @Part MultipartBody.Part image,
                                 @Part("talent_id") RequestBody talent_id, @Part("cat_id") RequestBody cat_id,
                                 @Part("subcat_id") RequestBody subcat_id,
                                 @Part("name") RequestBody name,
                                 @Part("price") RequestBody price,
                                 @Part("description") RequestBody description,
                                 @Part("link1") RequestBody link1,
                                 @Part("link2") RequestBody link2,
                                 @Part("link3") RequestBody link3,
                                 @Part("brand_id") RequestBody brand_id);

    @FormUrlEncoded
    @POST(Constant.DELETERANDS)
    Call<DeleteTalents> deleteBrands(@Header("Authorization") String token, @Field("brand_id") String id);

    @FormUrlEncoded
    @POST(Constant.SAVERANDS)
    Call<DeleteTalents> saveBrands(@Header("Authorization") String token, @Field("brand_id") String id);

    @FormUrlEncoded
    @POST(Constant.DELETEPRODUCT)
    Call<DeleteTalents> deleteProduct(@Header("Authorization") String token, @Field("product_id") String id);

    @FormUrlEncoded
    @POST(Constant.DELETESERVICES)
    Call<DeleteTalents> deleteService(@Header("Authorization") String token, @Field("services_id") String id);

    @FormUrlEncoded
    @POST(Constant.GETHOMEUSERDETAILS)
    Call<OtherProfileUserDetail> getHomeUserData(@Header("Authorization") String token, @Field("current_pages") String current_pages,
                                                @Field("name") String name,
                                                 @Field("work_type") String work_type
                                                ,@Field("productCategory") String productCategory
                                                ,@Field("productLocation") String productLocation
                                                ,@Field("productCurrency") String productCurrency
                                                ,@Field("productPrice") String productPrice
                                                ,@Field("talantName") String talantName
                                                ,@Field("gender") String gender
                                                ,@Field("language") String language
                                                ,@Field("filterType") String filterType
                                                ,@Field("age") String age
                                                ,@Field("talantCurrency") String talantCurrency
                                                ,@Field("talantPrice") String talantPrice
                                                ,@Field("serviceName") String serviceName
                                                ,@Field("serviceLocation") String serviceLocation
                                                ,@Field("serviceDay") String serviceDay
                                                ,@Field("projectelant") String projectName
                                                ,@Field("projectCurrency") String projectCurrency
                                                ,@Field("projectPrice") String projectPrice
                                                ,@Field("projectgender") String projectgender
    );

    @FormUrlEncoded
    @POST(Constant.GETSPECIALUSERDATA)
    Call<OtherProfileUserDetail> getSpecialUserData(@Header("Authorization") String token, @Field("current_pages") String current_pages);

    @FormUrlEncoded
    @POST(Constant.OTHERUSERPROFILEBYID)
    Call<OtherUserByIdDetail> otherUserProfileById(@Header("Authorization") String token, @Field("id") String id);

    @POST(Constant.MYPROFILE)
    Call<HomeUserDetail> getMyProfileDetails(@Header("Authorization") String token);


    @POST(Constant.PREFERREDLANG)
    Call<PreferredLanguage> getPreferredLang(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(Constant.EDITPROFILE)
    Call<EditInfo> editUserProfile(@Header("Authorization") String token, @Field("status") String status,
                                   @Field("works") String works, @Field("livesin") String livesin,
                                   @Field("language[]") ArrayList<Integer> language, @Field("gander") String gander,
                                   @Field("respondwithin") String respondwithin,
                                   @Field("dob") String dob,@Field("phone") String phone,@Field("name") String name,
                                   @Field("bio") String bio ,@Field("update_status") String update_status,
                                   @Field(" phonetype") String  phonetype);

    @FormUrlEncoded
    @POST(Constant.CREATEFEEDBACK)
    Call<CreateFeedback> createFeedback(@Header("Authorization") String token,@Field("talant_id") String talant_id,
                                        @Field("rating") String rating, @Field("massage") String massage);

    @FormUrlEncoded
    @POST(Constant.CREATETESTIMONIAL)
    Call<CreateTestimonial> createTestimonial(@Header("Authorization") String token,@Field("product_id") String product_id,
                                              @Field("rating") String rating, @Field("massage") String massage);

    @FormUrlEncoded
    @POST(Constant.CREATEREORGANISATION)
    Call<CreateReorganisation> createReorganisation(@Header("Authorization") String token,@Field("brand_id") String brand_id,
                                                  @Field("rating") String rating, @Field("massage") String massage);

    @FormUrlEncoded
    @POST(Constant.USERLIKE)
    Call<SetUserLikeUnlike> setLike(@Header("Authorization") String token, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constant.SAVEPRODUCT)
    Call<DeleteTalents> saveProduct(@Header("Authorization") String token, @Field("product_id") String user_id);


    @FormUrlEncoded
    @POST(Constant.SAVESERVICES)
    Call<DeleteTalents> saveService(@Header("Authorization") String token, @Field("services_id") String user_id);

    @FormUrlEncoded
    @POST(Constant.SAVETALENT)
    Call<DeleteTalents> saveTalent(@Header("Authorization") String token, @Field("talent_id") String user_id);

    @FormUrlEncoded
    @POST(Constant.DELETEALLTALENTS)
    Call<DeleteTalents> deletTalent(@Header("Authorization") String token, @Field("category_id") String category_id);

    @FormUrlEncoded
    @POST(Constant.VIDEOVIEW)
    Call<DeleteTalents> VideoView(@Header("Authorization") String token, @Field("talant_id") Integer user_id, @Field("talant_user") Integer talant_user);

    @FormUrlEncoded
    @POST(Constant.ADDNOTES)
    Call<DeleteTalents> addNotes(@Header("Authorization") String token, @Field("notes") String notes);

    @FormUrlEncoded
    @POST(Constant.LIKEUSERLIST)
    Call<LikeUserList> getLikeUserData(@Header("Authorization") String token, @Field("current_pages") String current_pages);

    @FormUrlEncoded
    @POST(Constant.DEVICETOKEN)
    Call<DeleteTalents> updateFirebaseToken(@Header("Authorization") String token, @Field("token") String devicetoken);

    @FormUrlEncoded
    @POST(Constant.SUPPORT)
    Call<HelpUserDetails> helpandsupport(@Header("Authorization") String token, @Field("name") String name, @Field("email") String email,
                                         @Field("description") String description);

    @FormUrlEncoded
        @POST(Constant.SUGGESTTALENT)
        Call<SuggestTalent> suggestTalent(@Header("Authorization") String token, @Field("talant") String talant,
                                          @Field("description") String description);

    @FormUrlEncoded
    @POST(Constant.SENDNOTIFICATION)
    Call<DeleteTalents> sendNotification(@Header("Authorization") String token, @Field("token") String devicetoken);

    @FormUrlEncoded
    @POST(Constant.GETCHATS)
    Call<DeleteTalents> sentChat(@Header("Authorization") String token, @Field("chat_user_id") String chat_user_id, @Field("massage") String massage);

    @POST(Constant.DELETEUSERIMAGE)
    Call<DeleteTalents> DeleteUserImage(@Header("Authorization") String token);

    @POST(Constant.SAVEDPRODUCTSSERVICELIST)
    Call<SavedProductsAndServices> getSavedProductsAndServiceList(@Header("Authorization") String token);

    @POST(Constant.SAVEDBRANDSLIST)
    Call<SavedBrands> getSavedBrandsList(@Header("Authorization") String token);

    @POST(Constant.SAVEDTALENTLIST)
    Call<SaveTalent> getSaveTalent(@Header("Authorization") String token);

    @POST(Constant.GETCHATLIST)
    Call<ChatData> getChatList(@Header("Authorization") String token);

    @POST(Constant.GETNOTIFICATIONLIST)
    Call<NotificationData> getNotificationList(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(Constant.UPDATEUSERSTATUS)
    Call<DeleteTalents> updateUserStatus(@Header("Authorization") String token, @Field("status") String status);

    @POST(Constant.AGEGROUPS)
    Call<AgeGroupData> getAgeGroup(@Header("Authorization") String token);

    @Multipart
    @POST(Constant.ADDSTORY)
        Call<AddStory> uploadStory(@Header("Authorization") String token,
                               @Part  MultipartBody.Part storyImage);

    @POST(Constant.VIEWSTORY)
        Call<StoryView> viewStory(@Header("Authorization") String token);

    @FormUrlEncoded
  @POST(Constant.SIMILARPROFILES)
        Call<SimilarProfile> getSimilarProfiles(@Header("Authorization") String token, @Field("user_id") String user_id);

    @POST(Constant.COUNTRIES)
    Call<CountryData> getCountry();


    @GET(Constant.VERIFYCARD+"{input}")
    Call<CardResponse> CardVerify(@Path("input") String input);


//    @GET(Constant.PAYMENTSUCCESS+"{input}")
//    Call<PaymentSuccessModel> paymentsuccess(@Path("input") String input);
    @GET(Constant.PAYMENTSUCCESS) //i.e https://api.test.com/Search?
    Call<PaymentSuccessModel> paymentsuccess(@Query("id") String one);

    @FormUrlEncoded
    @POST(Constant.STATE)
    Call<StateData> getState(@FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.CITY)
    Call<CityData> getCity(@Field("id") String id);

    @FormUrlEncoded
    @POST(Constant.DELETESTORY)
    Call<DeleteStory> deletStory(@Header("Authorization") String token, @Field("id") String id);

    @FormUrlEncoded
    @POST(Constant.PROJECTDETAILS)
    Call<ProjectDetails> projectDetails(@Header("Authorization") String token, @Field("id") String id);

    @FormUrlEncoded
    @POST(Constant.ADDREPORTS)
    Call<ReportAdd> addREPORT(@Header("Authorization") String token, @Field("user_id") String user_id
         , @Field("report_id") String report_id, @Field("description") String description);

    @FormUrlEncoded
    @POST(Constant.SAVEPROJECT)
    Call<SaveProject> saveProject(@Header("Authorization") String token, @Field("project_id") String id);


    @POST(Constant.SAVEPROJECTLIST)
    Call<SaveProjectList> saveProjectList(@Header("Authorization") String token);

    @POST(Constant.REPORTS)
    Call<ReportList> getReportReason(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(Constant.TAGSUSERLIST)
    Call<TagUserList> getTagUserList(@Header("Authorization") String token, @Field("search") String search);

    @FormUrlEncoded
    @POST(Constant.KYCDETAILS)
    Call<AddKYC> addKYCDetails(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);


    @POST(Constant.KYCLIST)
    Call<KycDetails> KYCDetails(@Header("Authorization") String token);


    @FormUrlEncoded
    @POST(Constant.PRODUCTCATDELETE)
    Call<DeleteTalents> deleteProductCat(@Header("Authorization") String token, @Field("cat_id") String id);

    @FormUrlEncoded
    @POST(Constant.SERVICECATDELETE)
    Call<DeleteTalents> deleteServicecat(@Header("Authorization") String token, @Field("cat_id") String id);

    @FormUrlEncoded
    @POST(Constant.PRODUCTDETAILS)
    Call<ViewProduct> productDetails(@Header("Authorization") String token, @Field("id") String id);

    @FormUrlEncoded
    @POST(Constant.SERVICEDETAILS)
    Call<ViewService> serviceDetails(@Header("Authorization") String token, @Field("id") String id);

    @FormUrlEncoded
    @POST(Constant.SERVICEVIEW)
    Call<ServiceView> ServiceView(@Header("Authorization") String token, @Field("service_id") String id);

    @FormUrlEncoded
    @POST(Constant.PRODUCTVIEW)
    Call<ProductView> ProductView(@Header("Authorization") String token, @Field("product_id") String id);

    @FormUrlEncoded
    @POST(Constant.BANKDETAILS)
    Call<BankDetails> setbankDetails(@Header("Authorization") String token, @FieldMap() Map<String, String> parem);

    @FormUrlEncoded
    @POST(Constant.USERPDF)
    Call<UserPdf> downloadUserPDF(@Header("Authorization") String token, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constant.BUTTONONE)
    Call<ButtonModel> BUTTONONE(@Header("Authorization") String token, @Field("contantfirst") String contantfirst
            , @Field("contantsecend") String contantsecend);

    @FormUrlEncoded
    @POST(Constant.BUTTONTWO)
    Call<ButtonModel> BUTTONTWO(@Header("Authorization") String token, @Field("contantfirst") String contantfirst
            , @Field("contantsecend") String contantsecend);

    @POST(Constant.GERBANKDETAILS)
    Call<GetBankDetails> getbankDetails(@Header("Authorization") String token);

    @POST(Constant.PRODUCTENQUIRYLIST)
    Call<EnquiryModel> getEnquiryList(@Header("Authorization") String token);

    @POST(Constant.TALENTENQUIRYLIST)
    Call<EnquiryModel> getTalentEnquiryList(@Header("Authorization") String token);

    @POST(Constant.SERVICEENQUIRYLIST)
    Call<EnquiryModel> getServiceEnquiryList(@Header("Authorization") String token);

    @POST(Constant.ASSIGNMENTLIST)
    Call<AssignmentModel> getAssignmentList(@Header("Authorization") String token);

    @POST(Constant.TRANSACTIONLIST)
    Call<TransactionModel> getTransactionList(@Header("Authorization") String token);

    @POST(Constant.ONETOONECHAT)
    Call<OnetooneChatModel> getOnetoOneChat(@Header("Authorization") String token);

    @POST(Constant.GROUPCHAT)
    Call<GroupChatModel> getGroupChat(@Header("Authorization") String token);

    @POST(Constant.REFUNDLIST)
    Call<RefundList> refundReasonList(@Header("Authorization") String token);

     @POST(Constant.REMOVELIST)
    Call<RemoveList> removeReasonList(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(Constant.PROJECTREQUEST)
    Call<ProjectRequestModel> submitCollab(@Header("Authorization") String token, @Field("projectuser_id") String projectuser_id
            ,@Field("project_id") String project_id, @Field("massage") String massage);

    @FormUrlEncoded
    @POST(Constant.CHECKPROJECT)
    Call<CheckProjectModel> checkProject(@Header("Authorization") String token, @Field("id") String id);

    @FormUrlEncoded
    @POST(Constant.PROJECTUSERS)
    Call<ProjectUsers> ProjectUserList(@Header("Authorization") String token, @Field("group_id") String id);

    @FormUrlEncoded
    @POST(Constant.ACCEPTPROJECT)
    Call<ProjectAccept> acceptProject(@Header("Authorization") String token, @Field("project_id") String project_id, @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constant.REQUESTEDUSERLIST)
    Call<RequestedUser> requestedUserList(@Header("Authorization") String token, @Field("project_id") String project_id);

    @FormUrlEncoded
    @POST(Constant.REFUNDREQUEST)
    Call<RefundRequest> refundRequest(@Header("Authorization") String token, @Field("refund_id") String refund_id,
                                      @Field("project_id") String project_id,
                                      @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constant.REMOVEUSER)
    Call<RemoveUser> removeUserFromGroup(@Header("Authorization") String token,
                                         @Field("project_id") String project_id,
                                         @Field("user_id") String user_id,
                                         @Field("remove_id") String remove_id);

    @FormUrlEncoded
    @POST(Constant.FEEDBACK)
    Call<Feedback> submitFeedback(@Header("Authorization") String token,
                                  @Field("type") String type,
                                  @Field("rating") String rating,
                                  @Field("massage") String massage,
                                  @Field("type_id") String type_id);
    @FormUrlEncoded
    @POST(Constant.OWNERFEEDBACK)
    Call<Feedback> submitOwnerFeedback(@Header("Authorization") String token,
                                  @Field("projectId") String projectId,
                                  @Field("user_id") String user_id,
                                  @Field("rating") String rating,
                                  @Field("massage") String massage);

    @FormUrlEncoded
    @POST(Constant.RELEASEAMOUNT)
    Call<ReleaseAmount> ReleaseAmount(@Header("Authorization") String token
            , @Field("amount") String amount
            , @Field("user_id") String user_id
            , @Field("project_userid") String project_userid
            , @Field("currency") String currency
            , @Field("project_id") String project_id);

    @FormUrlEncoded
    @POST(Constant.FINDUSER)
    Call<Feedback> findUser(@Header("Authorization") String token
            , @Field("user_id") String user_id
            , @Field("project_id") String project_id);

    @FormUrlEncoded
    @POST(Constant.ACCEPTINVITE)
    Call<AcceptInvite> acceptInvite(@Header("Authorization") String token
            , @Field("request_id") String request_id, @Field("type") String type, @Field("project_id") String project_id,
                                    @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constant.REJECTINVITE)
    Call<AcceptInvite> rejectInvite(@Header("Authorization") String token
            , @Field("request_id") String request_id , @Field("type") String type, @Field("project_id") String project_id
            , @Field("user_id") String user_id);


    @POST(Constant.PROJECTREQUESTEDLIST)
    Call<ProjectRequest> getRequestedProjects(@Header("Authorization") String token);

    @POST(Constant.NOTIFICATIONSEE)
    Call<DeleteTalents> NotificationSee(@Header("Authorization") String token);

    @POST(Constant.COLLABSEE)
    Call<DeleteTalents> CollabSee(@Header("Authorization") String token);

    @POST(Constant.SAVEADSLIST)
    Call<AdsModel> saveAdsList(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(Constant.SAVEUNSAVEADS)
    Call<SaveUnsaveAds> saveUnsaveAds(@Header("Authorization") String token , @Field("id") String id);

    @FormUrlEncoded
    @POST(Constant.PROJECTEND)
    Call<SaveUnsaveAds> endProject(@Header("Authorization") String token , @Field("projectId") String id);

    @FormUrlEncoded
    @POST(Constant.FEEDBACKLIST)
    Call<FeedbackModel> feedbackList(@Header("Authorization") String token , @Field("id") String id, @Field("type") String type);


    @POST(Constant.WATERMARKSUBSCRIPTION)
    Call<SubscriptionModel> checkSubscription(@Header("Authorization") String token);

    @POST(Constant.QUICKCHECK)
    Call<QuickbloxCheck> QuickbloxCheck(@Header("Authorization") String token);

    @FormUrlEncoded
    @POST(Constant.PROJECTRATING)
    Call<DeleteTalents> projectrating(@Header("Authorization") String token
            , @Field("project_id") String project_id
            , @Field("rating") String rating
            , @Field("description") String description
                    );

    @FormUrlEncoded
    @POST(Constant.PROJECTRATINGLIST)
    Call<FeedbackModel> projectratingList(@Header("Authorization") String token
            , @Field("project_id") String project_id);

    @FormUrlEncoded
    @POST(Constant.WITHDRAWAMOUNT)
    Call<WithdrawModel> withdrawAmount(@Header("Authorization") String token
            ,@Field("amount") String amount ,@Field("currency") String currency
    );

     @FormUrlEncoded
    @POST(Constant.PROJECTENDUSER)
    Call<UserFeedback> projectEndUserList(@Header("Authorization") String token
            , @Field("projectId") String amount
    );

     @FormUrlEncoded
    @POST(Constant.NEWREGISTER)
    Call<NewRegisterModel> newRegister(@Field("type") String type
            , @Field("phone") String phone, @Field("deviceId") String deviceId, @Field("deviceName") String deviceName
    );

     @FormUrlEncoded
    @POST(Constant.NEWREGISTER)
    Call<NewRegisterModel> newEmailRegister(@Field("type") String type
            , @Field("email") String email, @Field("deviceId") String deviceId, @Field("deviceName") String deviceName
    );

     @FormUrlEncoded
    @POST(Constant.NEWREGISTER)
    Call<NewRegisterModel> newSocialRegister(@Field("type") String type
            , @Field("email") String email, @Field("appId") String appId, @Field("deviceId") String deviceId,
                                             @Field("deviceName") String deviceName
    );

  @FormUrlEncoded
    @POST(Constant.SENDEMAILOTP)
    Call<NewRegisterModel> sendPhoneOTp(@Field("type") String type
            , @Field("phone") String phone, @Field("userId") String userId
    );

  @FormUrlEncoded
    @POST(Constant.SENDEMAILOTP)
    Call<NewRegisterModel> sendEmailOTp(@Field("type") String type
            , @Field("email") String phone, @Field("userId") String userId
    );

  @FormUrlEncoded
    @POST(Constant.OTPCONFIRM)
    Call<NewRegisterModel> confirmEmailOTP(@Field("type") String type
            , @Field("email") String email, @Field("otp") String otp, @Field("userId") String userId
    );

  @FormUrlEncoded
    @POST(Constant.OTPCONFIRM)
    Call<NewRegisterModel> confirmMobileOTP(@Field("type") String type
            , @Field("email") String phone, @Field("otp") String otp, @Field("userId") String userId
    );

    @POST(Constant.VERIFYPHONE)
    Call<VerifyPhoneModel> verifyPhone(@Header("Authorization") String token
    );

    @FormUrlEncoded
    @POST(Constant.REQUESTMONEY)
    Call<RequestMoneyModel> requestMoney(@Header("Authorization") String token
            , @Field("projectId") String projectId
            , @Field("projectownerId") String projectownerId
    );

    @POST(Constant.PROJECTTEMPLATES)
    Call<ProjectTemplatesModel> projectTemplates(@Header("Authorization") String token

    );

   @POST(Constant.DEACTIVATEACCOUNT)
    Call<SuccessModel> deactivateAccount(@Header("Authorization") String token);

   @POST(Constant.DELETEACCOUNT)
    Call<SuccessModel> deleteAccount(@Header("Authorization") String token);

   @POST(Constant.LOGGEDINDEVICESLIST)
    Call<DeviceListModel> loggindevicelists(@Header("Authorization") String token);

   @POST(Constant.USERTRANSACTION)
    Call<UserTransactionModel> usertransaction(@Header("Authorization") String token);

 @FormUrlEncoded
   @POST(Constant.USERTRANSACTIONLIST)
    Call<TransactionListModel> usertransactionlist(@Header("Authorization") String token
           , @Field("type") String type  , @Field("user_id") String user_id
   );

 @FormUrlEncoded
   @POST(Constant.COLLABREQUEST)
    Call<InivtationModel> collabRequest(@Header("Authorization") String token
           , @Field("type") String type
   );

 @FormUrlEncoded
   @POST(Constant.SERVICEAMOUNT)
    Call<ServiceAmountModel> serviceAmount(@Header("Authorization") String token
           , @Field("service_id") String service_id
   );
 @FormUrlEncoded
   @POST(Constant.SERVICEAMOUNTRELEASE)
    Call<SuccessModel> serviceAmountRelease(@Header("Authorization") String token
           , @Field("service_id") String service_id
   );


   @POST(Constant.SERVICEREFUNDLIST)
    Call<RefundList> serviceRefundList(@Header("Authorization") String token
   );

 @FormUrlEncoded
 @POST(Constant.SERVICEREFUNDREQUEST)
    Call<RefundRequest> serviceRefundRequest(@Header("Authorization") String token,@Field("resion_id") String resion_id,
                                          @Field("service_id") String service_id
   );

 @FormUrlEncoded
 @POST(Constant.SERVICEUSERFEEDBACK)
     Call<Feedback> serviceUserFeedback(@Header("Authorization") String token,
                                             @Field("user_id") String user_id,
                                             @Field("service_id") String service_id,
                                             @Field("rating") String rating,
                                             @Field("massage") String massage
 );

  @FormUrlEncoded
 @POST(Constant.SERVICEOWNERFEEDBACK)
     Call<Feedback> serviceOwnerFeedback(@Header("Authorization") String token,
                                             @Field("user_id") String user_id,
                                             @Field("service_id") String service_id,
                                             @Field("rating") String rating,
                                             @Field("massage") String massage
 );

 @FormUrlEncoded
  @POST(Constant.ENDSERVICE)
     Call<ServiceEndModel> endService(@Header("Authorization") String token,
                                      @Field("service_id") String service_id
 );

 @FormUrlEncoded
  @POST(Constant.SERVICEREQUEST)
     Call<RequestServiceModel> serviceRequest(@Header("Authorization") String token,
                                      @Field("serviceId") String service_id,
                                      @Field("userId") String userId,
                                      @Field("amount") String amount
 );

@FormUrlEncoded
  @POST(Constant.APPFEEDBACK)
     Call<Feedback> AppFeedback(@Header("Authorization") String token,
                                      @Field("rating") String rating,@Field("massage") String massage
 );

@FormUrlEncoded
  @POST(Constant.ACCOUNTVERIFY)
     Call<AccountVerifyModel> AccountVerify(@Header("Authorization") String token,
                                      @Field("amount") String massage
 );

  @FormUrlEncoded
  @POST(Constant.LOGGEDINDEVICES)
   Call<AddLogginDeviceModel> AddlogginDevices(@Header("Authorization") String token
          , @Field("devicename") String devicename
          , @Field("location") String location
  );


     @FormUrlEncoded
    @POST(Constant.ADDADDRESS)
    Call<AddAddressModel> addAddress(@Header("Authorization") String token
            , @Field("name") String name
            , @Field("mobile") String mobile
            , @Field("pincode") String pincode
            , @Field("deliveryaddress") String deliveryaddress
            , @Field("town") String town
            , @Field("city") String city
            , @Field("state") String state
            , @Field("bname") String bname
            , @Field("bmobile") String bmobile
            , @Field("bpincode") String bpincode
            , @Field("bdeliveryaddress") String bdeliveryaddress
            , @Field("btown") String btown
            , @Field("bcity") String bcity
            , @Field("bstate") String bstate
    );

    @POST(Constant.ADDRESSLIST)
    Call<AddressModel> getAddress(@Header("Authorization") String token

    );

    @FormUrlEncoded
    @POST(Constant.CHECKRELEASE)
    Call<CheckRelease> checkRelease(@Header("Authorization") String token
            , @Field("userId") String userId
            , @Field("projectId") String projectId
            , @Field("amount") String amount
            , @Field("curreney") String curreney
    );


    @POST(Constant.LOGOUT)
    Call<SuccessModel> Logout(@Header("Authorization") String token);


}
