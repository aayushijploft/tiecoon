package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class ProjectUsers {


    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("relese")
    private String relese;
    @Expose
    @SerializedName("project_id")
    private String project_id;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getRelese() {
        return relese;
    }

    public void setRelese(String relese) {
        this.relese = relese;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class Data {
        @Expose
        @SerializedName("quickbloxuser_id")
        private String quickbloxuser_id;
        @Expose
        @SerializedName("amount")
        private String amount;
        @Expose
        @SerializedName("currency")
        private String currency;
        @Expose
        @SerializedName("project_id")
        private int project_id;
       @Expose
        @SerializedName("admin")
        private String admin;
        @Expose
        @SerializedName("projectuser_id")
        private int projectuser_id;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("collabId")
        private String collabId;
       @Expose
        @SerializedName("relesestatus")
        private String relesestatus;
       @Expose
        @SerializedName("requestmoney")
        private String requestmoney;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("id")
        private int id;

        public String getQuickbloxuser_id() {
            return quickbloxuser_id;
        }

        public void setQuickbloxuser_id(String quickbloxuser_id) {
            this.quickbloxuser_id = quickbloxuser_id;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getProject_id() {
            return project_id;
        }

        public void setProject_id(int project_id) {
            this.project_id = project_id;
        }

        public int getProjectuser_id() {
            return projectuser_id;
        }

        public void setProjectuser_id(int projectuser_id) {
            this.projectuser_id = projectuser_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAdmin() {
            return admin;
        }

        public void setAdmin(String admin) {
            this.admin = admin;
        }

        public String getCollabId() {
            return collabId;
        }

        public void setCollabId(String collabId) {
            this.collabId = collabId;
        }

        public String getRequestmoney() {
            return requestmoney;
        }

        public void setRequestmoney(String requestmoney) {
            this.requestmoney = requestmoney;
        }

        public String getRelesestatus() {
            return relesestatus;
        }

        public void setRelesestatus(String relesestatus) {
            this.relesestatus = relesestatus;
        }
    }
}
