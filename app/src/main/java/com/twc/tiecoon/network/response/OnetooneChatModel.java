package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OnetooneChatModel {

    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("dialog_id")
        private String dialog_id;
        @Expose
        @SerializedName("quickbloxuser_id")
        private int quickbloxuser_id;
        @Expose
        @SerializedName("last_message")
        private String last_message;
        @Expose
        @SerializedName("profile_image")
        private String profile_image;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("user_id")
        private int user_id;

        public String getDialog_id() {
            return dialog_id;
        }

        public void setDialog_id(String dialog_id) {
            this.dialog_id = dialog_id;
        }

        public int getQuickbloxuser_id() {
            return quickbloxuser_id;
        }

        public void setQuickbloxuser_id(int quickbloxuser_id) {
            this.quickbloxuser_id = quickbloxuser_id;
        }

        public String getLast_message() {
            return last_message;
        }

        public void setLast_message(String last_message) {
            this.last_message = last_message;
        }

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }
    }
}
