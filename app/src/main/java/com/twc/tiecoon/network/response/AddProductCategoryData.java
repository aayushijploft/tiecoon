package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddProductCategoryData implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("slug")
    @Expose
    private String slug;
    @SerializedName("parent_id")
    @Expose
    private Object parentId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tresh")
    @Expose
    private String tresh;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private Object updatedAt;

    public AddProductCategoryData(Integer id, String title, String slug, Object parentId, String description, String status, String tresh, Object createdAt, Object updatedAt) {
        this.id = id;
        this.title = title;
        this.slug = slug;
        this.parentId = parentId;
        this.description = description;
        this.status = status;
        this.tresh = tresh;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    protected AddProductCategoryData(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        title = in.readString();
        slug = in.readString();
        description = in.readString();
        status = in.readString();
        tresh = in.readString();
    }

    public static final Creator<AddProductCategoryData> CREATOR = new Creator<AddProductCategoryData>() {
        @Override
        public AddProductCategoryData createFromParcel(Parcel in) {
            return new AddProductCategoryData(in);
        }

        @Override
        public AddProductCategoryData[] newArray(int size) {
            return new AddProductCategoryData[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Object getParentId() {
        return parentId;
    }

    public void setParentId(Object parentId) {
        this.parentId = parentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTresh() {
        return tresh;
    }

    public void setTresh(String tresh) {
        this.tresh = tresh;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(title);
        parcel.writeString(slug);
        parcel.writeString(description);
        parcel.writeString(status);
        parcel.writeString(tresh);
    }

    @Override
    public String toString() {
        return "AddProductCategoryData{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", slug='" + slug + '\'' +
                ", parentId=" + parentId +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", tresh='" + tresh + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
