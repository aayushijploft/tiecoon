package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.twc.tiecoon.network.WorkedOnProject;

import java.util.List;

public class HomeUserDetailData implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("firstname")
    @Expose
    private Object firstname;
    @SerializedName("lastname")
    @Expose
    private Object lastname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("profile")
    @Expose
    private String profile;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tresh")
    @Expose
    private String tresh;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("appid")
    @Expose
    private String appid;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("otp")
    @Expose
    private Object otp;
    @SerializedName("profile_first")
    @Expose
    private String  profile_first;
    @SerializedName("device_token")
    @Expose
    private String  device_token;
    @SerializedName("user_status")
    @Expose
    private String  user_status;
    @SerializedName("notes")
    @Expose
    private String  notes;
    @SerializedName("quickbloxuser_id")
    @Expose
    private String  quickbloxuser_id;
    @SerializedName("data_type")
    @Expose
    private String  data_type;
    @SerializedName("totalearning")
    @Expose
    private String  totalearning;
    @SerializedName("totalearningbyid")
    @Expose
    private String  totalearningbyid;
    @SerializedName("allrating")
    @Expose
    private String  allrating;
    @SerializedName("avgRating")
    @Expose
    private String  avgRating;
    @SerializedName("title")
    @Expose
    private String  title;
    @SerializedName("link")
    @Expose
    private String  link;
    @SerializedName("massage")
    @Expose
    private String  massage;
    @SerializedName("image")
    @Expose
    private String  image;
    @SerializedName("ads_type")
    @Expose
    private String  ads_type;
    @SerializedName("like")
    @Expose
    private int  like;
    @SerializedName("notfication_count")
    @Expose
    private String  notfication_count;
    @SerializedName("is_logout")
    @Expose
    private String  is_logout;
    @SerializedName("quickblox_user_sesson")
    @Expose
    private String  quickblox_user_sesson;
    @SerializedName("respondtime")
    @Expose
    private String  respondtime;
    @SerializedName("save_profile")
    @Expose
    private String  save_profile;
    @SerializedName("connection_count")
    @Expose
    private String  connection_count;
    @SerializedName("collaboration_count")
    @Expose
    private String  collaboration_count;
    @SerializedName("created")
    @Expose
    private String  created;
    @SerializedName("is_like")
    @Expose
    private boolean is_like;
    @SerializedName("user_detils")
    @Expose
    private HomeUserDetailsUserDetils userDetils;
    @SerializedName("preferred_language")
    @Expose
    private List<HomeUserPreferedLanguage> preferredLanguage = null;
    @SerializedName("brand")
    @Expose
    private List<AddBrandsData> brand = null;
    @SerializedName("user_talent")
    @Expose
    private List<HomeUserDetialsUserTalent> userTalent = null;
    @SerializedName("services")
    @Expose
    private List<HomeUserDetailsService> services = null;
    @SerializedName("products")
    @Expose
    private List<HomeUserDetailsProduct> products = null;
    @SerializedName("upload_talent")
    @Expose
    private List<HomeUserDetailsUploadTalent> uploadTalent = null;
    @SerializedName("create_assignments")
    @Expose
    private List<ProfileCreateAssignment> createAssignments = null;
    @SerializedName("complete_assignment")
    @Expose
    private List<ProfileCompleteAssignment> complete_assignment = null;
    @SerializedName("complatedprojectworkon")
    @Expose
    private List<WorkedOnProject> complatedprojectworkon = null;

    @SerializedName("connection")
    @Expose
    private List<ConnectionModel> connection = null;

    @SerializedName("collaboration")
    @Expose
    private List<ConnectionModel> collaboration = null;

    @SerializedName("testimonial")
    @Expose
    private List<HomeUserDetailsTestimonial> testimonial = null;
    @SerializedName("reorganization")
    @Expose
    private List<HomeUserDetailsReOrganisation> reorganization = null;
    @SerializedName("feedback")
    @Expose
    private List<HomeUserDetailsFeedback> feedback = null;




    public HomeUserDetailData() {

    }
    public HomeUserDetailData(Parcel in) {
        in.readTypedList(uploadTalent, HomeUserDetailsUploadTalent.CREATOR);
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        email = in.readString();
        profile = in.readString();
        path = in.readString();
        status = in.readString();
        tresh = in.readString();
        device_token = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        phone = in.readString();
        appid = in.readString();
        type = in.readString();
        created = in.readString();
        connection_count = in.readString();
        collaboration_count = in.readString();
        is_like = in.readByte() != 0;
        userDetils = in.readParcelable(HomeUserDetailsUserDetils.class.getClassLoader());
        preferredLanguage = in.createTypedArrayList(HomeUserPreferedLanguage.CREATOR);
        brand = in.createTypedArrayList(AddBrandsData.CREATOR);
        userTalent = in.createTypedArrayList(HomeUserDetialsUserTalent.CREATOR);
        services = in.createTypedArrayList(HomeUserDetailsService.CREATOR);
        products = in.createTypedArrayList(HomeUserDetailsProduct.CREATOR);
        uploadTalent = in.createTypedArrayList(HomeUserDetailsUploadTalent.CREATOR);
        createAssignments = in.createTypedArrayList(ProfileCreateAssignment.CREATOR);
        complete_assignment = in.createTypedArrayList(ProfileCompleteAssignment.CREATOR);
        complatedprojectworkon = in.createTypedArrayList(WorkedOnProject.CREATOR);
        connection = in.createTypedArrayList(ConnectionModel.CREATOR);
        collaboration = in.createTypedArrayList(ConnectionModel.CREATOR);
        testimonial = in.createTypedArrayList(HomeUserDetailsTestimonial.CREATOR);
        reorganization = in.createTypedArrayList(HomeUserDetailsReOrganisation.CREATOR);
        feedback = in.createTypedArrayList(HomeUserDetailsFeedback.CREATOR);
//        tags = in.createTypedArrayList(UserDetailTags.CREATOR);
    }

    public static final Creator<HomeUserDetailData> CREATOR = new Creator<HomeUserDetailData>() {
        @Override
        public HomeUserDetailData createFromParcel(Parcel in) {
            return new HomeUserDetailData(in);
        }

        @Override
        public HomeUserDetailData[] newArray(int size) {
            return new HomeUserDetailData[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Object getFirstname() {
        return firstname;
    }

    public void setFirstname(Object firstname) {
        this.firstname = firstname;
    }

    public Object getLastname() {
        return lastname;
    }

    public void setLastname(Object lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTresh() {
        return tresh;
    }

    public void setTresh(String tresh) {
        this.tresh = tresh;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getQuickbloxuser_id() {
        return quickbloxuser_id;
    }

    public void setQuickbloxuser_id(String quickbloxuser_id) {
        this.quickbloxuser_id = quickbloxuser_id;
    }

    public String getNotfication_count() {
        return notfication_count;
    }

    public void setNotfication_count(String notfication_count) {
        this.notfication_count = notfication_count;
    }

    public String getIs_logout() {
        return is_logout;
    }

    public void setIs_logout(String is_logout) {
        this.is_logout = is_logout;
    }

    public String getQuickblox_user_sesson() {
        return quickblox_user_sesson;
    }

    public void setQuickblox_user_sesson(String quickblox_user_sesson) {
        this.quickblox_user_sesson = quickblox_user_sesson;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getTotalearning() {
        return totalearning;
    }

    public void setTotalearning(String totalearning) {
        this.totalearning = totalearning;
    }

    public String getTotalearningbyid() {
        return totalearningbyid;
    }

    public void setTotalearningbyid(String totalearningbyid) {
        this.totalearningbyid = totalearningbyid;
    }

    public String getAllrating() {
        return allrating;
    }

    public void setAllrating(String allrating) {
        this.allrating = allrating;
    }

    public String getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(String avgRating) {
        this.avgRating = avgRating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAds_type() {
        return ads_type;
    }

    public void setAds_type(String ads_type) {
        this.ads_type = ads_type;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public String getConnection_count() {
        return connection_count;
    }

    public void setConnection_count(String connection_count) {
        this.connection_count = connection_count;
    }

    public String getCollaboration_count() {
        return collaboration_count;
    }

    public void setCollaboration_count(String collaboration_count) {
        this.collaboration_count = collaboration_count;
    }

    public String getRespondtime() {
        return respondtime;
    }

    public void setRespondtime(String respondtime) {
        this.respondtime = respondtime;
    }

    public String getSave_profile() {
        return save_profile;
    }

    public void setSave_profile(String save_profile) {
        this.save_profile = save_profile;
    }

    public String getAppid() {
        return appid;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Object getOtp() {
        return otp;
    }

    public void setOtp(Object otp) {
        this.otp = otp;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public boolean isIs_like() {
        return is_like;
    }

    public void setIs_like(boolean is_like) {
        this.is_like = is_like;
    }

    public List<AddBrandsData> getHomeUserDetailBrand() {
        return brand;
    }

    public void setHomeUserDetailBrand(List<AddBrandsData> brand) {
        this.brand = brand;
    }

    public List<HomeUserDetialsUserTalent> getHomeUserDetialsUserTalent() {
        return userTalent;
    }

    public void setHomeUserDetialsUserTalent(List<HomeUserDetialsUserTalent> userTalent) {
        this.userTalent = userTalent;
    }

    public HomeUserDetailsUserDetils getHomeUserDetailsUserDetils() {
        return userDetils;
    }

    public void setHomeUserDetailsUserDetils(HomeUserDetailsUserDetils userDetils) {
        this.userDetils = userDetils;
    }

    public List<HomeUserDetailsService> getHomeUserDetailsServices() {
        return services;
    }

    public void setHomeUserDetailsServices(List<HomeUserDetailsService> services) {
        this.services = services;
    }

    public List<HomeUserDetailsProduct> getHomeUserDetailsProducts() {
        return products;
    }

    public void setHomeUserDetailsProducts(List<HomeUserDetailsProduct> products) {
        this.products = products;
    }

    public List<HomeUserDetailsUploadTalent> getHomeUserDetailsUploadTalent() {
        return uploadTalent;
    }

    public void setHomeUserDetailsUploadTalent(List<HomeUserDetailsUploadTalent> uploadTalent) {
        this.uploadTalent = uploadTalent;
    }

    public List<ProfileCreateAssignment> getHomeUserDetailCreateAssignments() {
        return createAssignments;
    }

    public void setHomeUserDetailCreateAssignments(List<ProfileCreateAssignment> createAssignments) {
        this.createAssignments = createAssignments;
    }

    public List<ProfileCompleteAssignment> getComplete_assignment() {
        return complete_assignment;
    }

    public void setComplete_assignment(List<ProfileCompleteAssignment> complete_assignment) {
        this.complete_assignment = complete_assignment;
    }

    public List<WorkedOnProject> getComplatedprojectworkon() {
        return complatedprojectworkon;
    }

    public void setComplatedprojectworkon(List<WorkedOnProject> complatedprojectworkon) {
        this.complatedprojectworkon = complatedprojectworkon;
    }

    public List<ConnectionModel> getConnection() {
        return connection;
    }

    public void setConnection(List<ConnectionModel> connection) {
        this.connection = connection;
    }

    public List<ConnectionModel> getCollaboration() {
        return collaboration;
    }

    public void setCollaboration(List<ConnectionModel> collaboration) {
        this.collaboration = collaboration;
    }

    public List<HomeUserDetailsTestimonial> getHomeUserDetailsTestimonial() {
        return testimonial;
    }

    public void setHomeUserDetailsTestimonial(List<HomeUserDetailsTestimonial> testimonial) {
        this.testimonial = testimonial;
    }

    public List<HomeUserDetailsReOrganisation> getHomeUserDetailsReOrganisation() {
        return reorganization;
    }

    public void setHomeUserDetailsReOrganisation(List<HomeUserDetailsReOrganisation> reorganization) {
        this.reorganization = reorganization;
    }

    public List<HomeUserDetailsFeedback> getHomeUserDetailsFeedback() {
        return feedback;
    }

    public void setHomeUserDetailsFeedback(List<HomeUserDetailsFeedback> feedback) {
        this.feedback = feedback;
    }


    public List<HomeUserPreferedLanguage> getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(List<HomeUserPreferedLanguage> preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
       parcel.writeTypedList(uploadTalent);
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(email);
        parcel.writeString(profile);
        parcel.writeString(path);
        parcel.writeString(status);
        parcel.writeString(tresh);
        parcel.writeString(device_token);
        parcel.writeString(createdAt);
        parcel.writeString(updatedAt);
        parcel.writeString(phone);
        parcel.writeString(connection_count);
        parcel.writeString(collaboration_count);
        parcel.writeString(appid);
        parcel.writeString(type);
        parcel.writeString(created);
        parcel.writeByte((byte) (is_like ? 1 : 0));
        parcel.writeParcelable(userDetils, i);
        parcel.writeTypedList(preferredLanguage);
        parcel.writeTypedList(brand);
        parcel.writeTypedList(userTalent);
        parcel.writeTypedList(services);
        parcel.writeTypedList(products);
        parcel.writeTypedList(uploadTalent);
        parcel.writeTypedList(createAssignments);
        parcel.writeTypedList(complete_assignment);
        parcel.writeTypedList(complatedprojectworkon);
        parcel.writeTypedList(connection);
        parcel.writeTypedList(collaboration);
        parcel.writeTypedList(testimonial);
        parcel.writeTypedList(reorganization);
        parcel.writeTypedList(feedback);
//        parcel.writeTypedList(tags);
    }

    @Override
    public String toString() {
        return "HomeUserDetailData{" +
                "id=" + id +
                ", username=" + username +
                ", firstname=" + firstname +
                ", lastname=" + lastname +
                ", email='" + email + '\'' +
                ", emailVerifiedAt=" + emailVerifiedAt +
                ", profile='" + profile + '\'' +
                ", path='" + path + '\'' +
                ", status='" + status + '\'' +
                ", tresh='" + tresh + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", phone='" + phone + '\'' +
                ", appid='" + appid + '\'' +
                ", type='" + type + '\'' +
                ", otp=" + otp +
                ", is_like=" + is_like +
                ", userDetils=" + userDetils +
                ", preferredLanguage=" + preferredLanguage +
                ", brand=" + brand +
                ", userTalent=" + userTalent +
                ", services=" + services +
                ", products=" + products +
                ", uploadTalent=" + uploadTalent +
                ", createAssignments=" + createAssignments +
                ", testimonial=" + testimonial +
                ", reorganization=" + reorganization +
                ", feedback=" + feedback +
                '}';
    }
}
