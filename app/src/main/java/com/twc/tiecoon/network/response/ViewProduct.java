package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ViewProduct {
    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("image")
        private List<Image> image;
        @Expose
        @SerializedName("tags")
        private List<Tags> tags;
        @Expose
        @SerializedName("user_name")
        private String user_name;
        @Expose
        @SerializedName("subcategory_name")
        private String subcategory_name;
        @Expose
        @SerializedName("location")
        private String location;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("qty")
        private String qty;
       @Expose
        @SerializedName("productid")
        private String productid;
        @Expose
        @SerializedName("myproductcount")
        private String myproductcount;
        @Expose
        @SerializedName("outofstock")
        private String outofstock;
        @Expose
        @SerializedName("price")
        private int price;
        @Expose
        @SerializedName("seecount")
        private String seecount;
        @Expose
        @SerializedName("is_like")
        private boolean is_like;
        @Expose
        @SerializedName("pricecode")
        private String pricecode;
        @Expose
        @SerializedName("category_name")
        private String category_name;
        @Expose
        @SerializedName("id")
        private int id;
        @Expose
        @SerializedName("user_id")
        private int user_id;

        public List<Image> getImage() {
            return image;
        }

        public void setImage(List<Image> image) {
            this.image = image;
        }

        public List<Tags> getTags() {
            return tags;
        }

        public void setTags(List<Tags> tags) {
            this.tags = tags;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getSubcategory_name() {
            return subcategory_name;
        }

        public void setSubcategory_name(String subcategory_name) {
            this.subcategory_name = subcategory_name;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }

        public String getProductid() {
            return productid;
        }

        public void setProductid(String productid) {
            this.productid = productid;
        }

        public String getMyproductcount() {
            return myproductcount;
        }

        public void setMyproductcount(String myproductcount) {
            this.myproductcount = myproductcount;
        }

        public String getOutofstock() {
            return outofstock;
        }

        public void setOutofstock(String outofstock) {
            this.outofstock = outofstock;
        }

        public boolean isIs_like() {
            return is_like;
        }

        public void setIs_like(boolean is_like) {
            this.is_like = is_like;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public String getSeecount() {
            return seecount;
        }

        public void setSeecount(String seecount) {
            this.seecount = seecount;
        }

        public String getPricecode() {
            return pricecode;
        }

        public void setPricecode(String pricecode) {
            this.pricecode = pricecode;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }
    }

    public static class Image {
        @Expose
        @SerializedName("product_type")
        private String product_type;
        @Expose
        @SerializedName("url")
        private String url;
        @Expose
        @SerializedName("id")
        private int id;

        public String getProduct_type() {
            return product_type;
        }

        public void setProduct_type(String product_type) {
            this.product_type = product_type;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static class Tags {
        @Expose
        @SerializedName("user_name")
        private String user_name;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("id")
        private int id;

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
