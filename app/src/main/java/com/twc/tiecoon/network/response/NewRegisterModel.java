package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import de.measite.minidns.record.Data;

public  class NewRegisterModel {

    @Expose
    @SerializedName("data")
    public Data data;
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    @SerializedName("success")
    public boolean success;

    public static class Data {
        @Expose
        @SerializedName("profile_first_status")
        public String profile_first_status;
        @Expose
        @SerializedName("isverfy")
        public String isverfy;
        @Expose
        @SerializedName("type")
        public String type;
        @Expose
        @SerializedName("otp")
        public String otp;
        @Expose
        @SerializedName("token")
        public String token;
        @Expose
        @SerializedName("userid")
        public String userid;
    }
}
