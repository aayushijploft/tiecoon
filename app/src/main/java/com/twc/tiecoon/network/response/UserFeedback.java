package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class UserFeedback {

    @Expose
    @SerializedName("data")
    private List<Data> data;

    @Expose
    @SerializedName("message")
    private String message;

    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("feedbackstatus")
        private String feedbackstatus;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("user_id")
        private String user_id;

        public String getFeedbackstatus() {
            return feedbackstatus;
        }

        public void setFeedbackstatus(String feedbackstatus) {
            this.feedbackstatus = feedbackstatus;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }
    }
}
