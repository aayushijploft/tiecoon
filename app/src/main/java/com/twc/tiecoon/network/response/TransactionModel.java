package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TransactionModel {


    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("total_earning")
    private Total_earning total_earning;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public Total_earning getTotal_earning() {
        return total_earning;
    }

    public void setTotal_earning(Total_earning total_earning) {
        this.total_earning = total_earning;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("time")
        private String time;
        @Expose
        @SerializedName("date")
        private String date;
        @Expose
        @SerializedName("type_id")
        private String type_id;
        @Expose
        @SerializedName("type")
        private String type;
        @Expose
        @SerializedName("download")
        private String download;
        @Expose
        @SerializedName("massage")
        private String massage;
        @Expose
        @SerializedName("trangctionId")
        private String trangctionId;
        @Expose
        @SerializedName("id")
        private String id;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getType_id() {
            return type_id;
        }

        public void setType_id(String type_id) {
            this.type_id = type_id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDownload() {
            return download;
        }

        public void setDownload(String download) {
            this.download = download;
        }

        public String getMassage() {
            return massage;
        }

        public void setMassage(String massage) {
            this.massage = massage;
        }

        public String getTrangctionId() {
            return trangctionId;
        }

        public void setTrangctionId(String trangctionId) {
            this.trangctionId = trangctionId;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Total_earning {
        @Expose
        @SerializedName("inr")
        private String inr;
        @Expose
        @SerializedName("dollor")
        private String dollor;

        public String getInr() {
            return inr;
        }

        public void setInr(String inr) {
            this.inr = inr;
        }

        public String getDollor() {
            return dollor;
        }

        public void setDollor(String dollor) {
            this.dollor = dollor;
        }
    }
}
