package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class AcceptInvite {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("quickbloxuser_id")
        private String quickbloxuser_id;
        @Expose
        @SerializedName("project_name")
        private String project_name;
        @Expose
        @SerializedName("projectOwnerName")
        private String projectOwnerName;
        @Expose
        @SerializedName("data")
        private boolean data;

        public String getQuickbloxuser_id() {
            return quickbloxuser_id;
        }

        public void setQuickbloxuser_id(String quickbloxuser_id) {
            this.quickbloxuser_id = quickbloxuser_id;
        }

        public boolean getData() {
            return data;
        }

        public void setData(boolean data) {
            this.data = data;
        }

        public String getProjectOwnerName() {
            return projectOwnerName;
        }

        public void setProjectOwnerName(String projectOwnerName) {
            this.projectOwnerName = projectOwnerName;
        }

        public String getProject_name() {
            return project_name;
        }

        public void setProject_name(String project_name) {
            this.project_name = project_name;
        }
    }
}
