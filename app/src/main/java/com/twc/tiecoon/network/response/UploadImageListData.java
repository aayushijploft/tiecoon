package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadImageListData implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("url")
    @Expose
    private String image;
    @SerializedName("product_type")
    @Expose
    private String product_type;
    @SerializedName("video_thumbnail")
    @Expose
    private String video_thumbnail;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("watermark")
    @Expose
    private String watermark;

    protected UploadImageListData(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        image = in.readString();
        watermark = in.readString();
        product_type = in.readString();
        video_thumbnail = in.readString();
        path = in.readString();
    }

    public static final Creator<UploadImageListData> CREATOR = new Creator<UploadImageListData>() {
        @Override
        public UploadImageListData createFromParcel(Parcel in) {
            return new UploadImageListData(in);
        }

        @Override
        public UploadImageListData[] newArray(int size) {
            return new UploadImageListData[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getWatermark() {
        return watermark;
    }

    public void setWatermark(String watermark) {
        this.watermark = watermark;
    }

    public String getCreateimage() {
        return product_type;
    }

    public void setCreateimage(String createimage) {
        this.product_type = createimage;
    }

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(image);
        parcel.writeString(watermark);
        parcel.writeString(product_type);
        parcel.writeString(video_thumbnail);
        parcel.writeString(path);
    }

    @Override
    public String toString() {
        return "UploadImageListData{" +
                "id=" + id +
                ", image='" + image + '\'' +
                ", createimage='" + product_type + '\'' +
                ", video_thumbnail='" + video_thumbnail + '\'' +
                '}';
    }
}
