package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TalentCurrencyData {

    @SerializedName("user_talents")
    @Expose
    private List<TalentCurrencyUserTalents> userTalents = null;
    @SerializedName("price")
    @Expose
    private List<CurrencyData> price = null;

    public List<TalentCurrencyUserTalents> getUserTalents() {
        return userTalents;
    }

    public void setUserTalents(List<TalentCurrencyUserTalents> userTalents) {
        this.userTalents = userTalents;
    }

    public List<CurrencyData> getPrice() {
        return price;
    }

    public void setPrice(List<CurrencyData> price) {
        this.price = price;
    }
}
