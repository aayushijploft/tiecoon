package com.twc.tiecoon.network.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class ViewService {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("tags")
        private List<Tags> tags;
        @Expose
        @SerializedName("service_image")
        private List<Service_image> service_image;
        @Expose
        @SerializedName("day")
        private String day;
        @Expose
        @SerializedName("seecount")
        private int seecount;
        @Expose
        @SerializedName("is_like")
        private boolean is_like;
        @Expose
        @SerializedName("user_name")
        private String user_name;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("subcategory_name")
        private String subcategory_name;
        @Expose
        @SerializedName("location")
        private String location;
        @Expose
        @SerializedName("color")
        private String color;
        @Expose
        @SerializedName("description")
        private String description;
        @Expose
        @SerializedName("serviceid")
        private String serviceid;
        @Expose
        @SerializedName("price")
        private int price;
        @Expose
        @SerializedName("pricecode")
        private String pricecode;
        @Expose
        @SerializedName("category_name")
        private String category_name;
         @Expose
        @SerializedName("service_count")
        private String service_count;
        @Expose
        @SerializedName("id")
        private int id;

        public List<Tags> getTags() {
            return tags;
        }

        public void setTags(List<Tags> tags) {
            this.tags = tags;
        }

        public List<Service_image> getService_image() {
            return service_image;
        }

        public void setService_image(List<Service_image> service_image) {
            this.service_image = service_image;
        }

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public int getSeecount() {
            return seecount;
        }

        public void setSeecount(int seecount) {
            this.seecount = seecount;
        }

        public boolean isIs_like() {
            return is_like;
        }

        public void setIs_like(boolean is_like) {
            this.is_like = is_like;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getSubcategory_name() {
            return subcategory_name;
        }

        public void setSubcategory_name(String subcategory_name) {
            this.subcategory_name = subcategory_name;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getServiceid() {
            return serviceid;
        }

        public void setServiceid(String serviceid) {
            this.serviceid = serviceid;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public String getPricecode() {
            return pricecode;
        }

        public void setPricecode(String pricecode) {
            this.pricecode = pricecode;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getService_count() {
            return service_count;
        }

        public void setService_count(String service_count) {
            this.service_count = service_count;
        }
    }

    public static class Tags {
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private int id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "Tags{" +
                    "name='" + name + '\'' +
                    ", id=" + id +
                    '}';
        }
    }

    public static class Service_image {
        @Expose
        @SerializedName("video_thumbnail")
        private String video_thumbnail;
        @Expose
        @SerializedName("product_type")
        private String product_type;
        @Expose
        @SerializedName("image")
        private String image;
        @Expose
        @SerializedName("id")
        private int id;

        public String getVideo_thumbnail() {
            return video_thumbnail;
        }

        public void setVideo_thumbnail(String video_thumbnail) {
            this.video_thumbnail = video_thumbnail;
        }

        public String getProduct_type() {
            return product_type;
        }

        public void setProduct_type(String product_type) {
            this.product_type = product_type;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
