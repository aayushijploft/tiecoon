package com.twc.tiecoon.network.response;

public class TourModel {

    public TourModel (int images, String text){
        this.image = images;
        this.title = text;

    }

    public int image;
    public  String title;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
