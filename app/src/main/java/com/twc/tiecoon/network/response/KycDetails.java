package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class KycDetails {

    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("updated_at")
        private String updated_at;
        @Expose
        @SerializedName("created_at")
        private String created_at;
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("expiration_year")
        private String expiration_year;
        @Expose
        @SerializedName("expiration_month")
        private String expiration_month;
        @Expose
        @SerializedName("cvc")
        private String cvc;
        @Expose
        @SerializedName("card_number")
        private String card_number;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("user_id")
        private int user_id;
        @Expose
        @SerializedName("id")
        private int id;

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getExpiration_year() {
            return expiration_year;
        }

        public void setExpiration_year(String expiration_year) {
            this.expiration_year = expiration_year;
        }

        public String getExpiration_month() {
            return expiration_month;
        }

        public void setExpiration_month(String expiration_month) {
            this.expiration_month = expiration_month;
        }

        public String getCvc() {
            return cvc;
        }

        public void setCvc(String cvc) {
            this.cvc = cvc;
        }

        public String getCard_number() {
            return card_number;
        }

        public void setCard_number(String card_number) {
            this.card_number = card_number;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
