package com.twc.tiecoon.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class DeviceListModel {

    @Expose
    @SerializedName("data")
    public List<Data> data;
    @Expose
    @SerializedName("status")
    public String status;
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    @SerializedName("success")
    public String success;

    public static class Data {
        @Expose
        @SerializedName("updated_at")
        public String updated_at;
        @Expose
        @SerializedName("created_at")
        public String created_at;
        @Expose
        @SerializedName("location")
        public String location;
        @Expose
        @SerializedName("devicename")
        public String devicename;
        @Expose
        @SerializedName("user_id")
        public String user_id;
        @Expose
        @SerializedName("id")
        public String id;
    }
}
