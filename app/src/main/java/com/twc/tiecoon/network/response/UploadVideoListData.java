package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadVideoListData implements Parcelable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("uploadtalant_id")
    @Expose
    private Integer uploadtalant_id;
    @SerializedName("video")
    @Expose
    private String video;

    protected UploadVideoListData(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            uploadtalant_id = null;
        } else {
            uploadtalant_id = in.readInt();
        }
        video = in.readString();
    }

    public static final Creator<UploadVideoListData> CREATOR = new Creator<UploadVideoListData>() {
        @Override
        public UploadVideoListData createFromParcel(Parcel in) {
            return new UploadVideoListData(in);
        }

        @Override
        public UploadVideoListData[] newArray(int size) {
            return new UploadVideoListData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUploadtalant_id() {
        return uploadtalant_id;
    }

    public void setUploadtalant_id(Integer uploadtalant_id) {
        this.uploadtalant_id = uploadtalant_id;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        if (uploadtalant_id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(uploadtalant_id);
        }
        parcel.writeString(video);
    }
}
