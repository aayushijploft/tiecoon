package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class AddLogginDeviceModel {

    @Expose
    @SerializedName("data")
    public Data data;
    @Expose
    @SerializedName("message")
    public String message;
    @Expose
    @SerializedName("success")
    public String success;

    public static class Data {
        @Expose
        @SerializedName("id")
        public String id;
        @Expose
        @SerializedName("created_at")
        public String created_at;
        @Expose
        @SerializedName("updated_at")
        public String updated_at;
        @Expose
        @SerializedName("location")
        public String location;
        @Expose
        @SerializedName("devicename")
        public String devicename;
        @Expose
        @SerializedName("user_id")
        public String user_id;
    }
}
