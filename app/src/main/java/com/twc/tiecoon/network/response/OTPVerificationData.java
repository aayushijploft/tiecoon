package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OTPVerificationData {
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("users")
    @Expose
    private OTPVerificationDataUser user;
    @SerializedName("profile_first_status")
    @Expose
    private String profileFirstStatus;
    @SerializedName("token")
    @Expose
    private String token;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProfileFirstStatus() {
        return profileFirstStatus;
    }

    public void setProfileFirstStatus(String profileFirstStatus) {
        this.profileFirstStatus = profileFirstStatus;
    }

    public OTPVerificationDataUser getUser() {
        return user;
    }

    public void setUser(OTPVerificationDataUser user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "OTPVerificationData{" +
                "type='" + type + '\'' +
                ", profileFirstStatus='" + profileFirstStatus + '\'' +
                ", user=" + user +
                ", token='" + token + '\'' +
                '}';
    }
}
