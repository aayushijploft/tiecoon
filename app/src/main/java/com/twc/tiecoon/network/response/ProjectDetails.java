package com.twc.tiecoon.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class ProjectDetails {


    @Expose
    @SerializedName("data")
    private Data data;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("success")
    private boolean success;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class Data {
        @Expose
        @SerializedName("status")
        private String status;

        @Expose
        @SerializedName("is_save")
        private boolean is_save;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("country_id")
        private String country_id;
        @Expose
        @SerializedName("state")
        private String state;
        @Expose
        @SerializedName("state_id")
        private String state_id;
        @Expose
        @SerializedName("city")
        private String city;
        @Expose
        @SerializedName("city_id")
        private String city_id;
        @Expose
        @SerializedName("talent")
        private List<Talent> talent;
        @Expose
        @SerializedName("gender")
        private String gender;
        @Expose
        @SerializedName("age")
        private String age;
        @Expose
        @SerializedName("age_id")
        private String age_id;
        @Expose
        @SerializedName("amount")
        private int amount;
        @Expose
        @SerializedName("currency")
        private String currency;
        @Expose
        @SerializedName("youtublink")
        private String youtublink;
        @Expose
        @SerializedName("language")
        private String language;
        @Expose
        @SerializedName("language_id")
        private String language_id;
        @Expose
        @SerializedName("text")
        private String text;
        @Expose
        @SerializedName("name")
        private String name;
       @Expose
        @SerializedName("digitId")
        private String digitId;
        @Expose
        @SerializedName("id")
        private int id;
        @Expose
        @SerializedName("ownerName")
        private String ownerName;
        @Expose
        @SerializedName("user_id")
        private int user_id;

        public String  isStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public boolean isIs_save() {
            return is_save;
        }

        public void setIs_save(boolean is_save) {
            this.is_save = is_save;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public List<Talent> getTalent() {
            return talent;
        }

        public void setTalent(List<Talent> talent) {
            this.talent = talent;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getYoutublink() {
            return youtublink;
        }

        public void setYoutublink(String youtublink) {
            this.youtublink = youtublink;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOwnerName() {
            return ownerName;
        }

        public void setOwnerName(String ownerName) {
            this.ownerName = ownerName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getCountry_id() {
            return country_id;
        }

        public void setCountry_id(String country_id) {
            this.country_id = country_id;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getAge_id() {
            return age_id;
        }

        public void setAge_id(String age_id) {
            this.age_id = age_id;
        }

        public String getLanguage_id() {
            return language_id;
        }

        public void setLanguage_id(String language_id) {
            this.language_id = language_id;
        }

        public String getDigitId() {
            return digitId;
        }

        public void setDigitId(String digitId) {
            this.digitId = digitId;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "is_save=" + is_save +
                    ", country='" + country + '\'' +
                    ", state='" + state + '\'' +
                    ", city='" + city + '\'' +
                    ", talent=" + talent +
                    ", gender='" + gender + '\'' +
                    ", age='" + age + '\'' +
                    ", amount=" + amount +
                    ", currency='" + currency + '\'' +
                    ", youtublink='" + youtublink + '\'' +
                    ", language='" + language + '\'' +
                    ", text='" + text + '\'' +
                    ", name='" + name + '\'' +
                    ", id=" + id +
                    '}';
        }
    }

    public static class Talent {
        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("id")
        private String id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "Talent{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ProjectDetails{" +
                "data=" + data +
                ", message='" + message + '\'' +
                ", success=" + success +
                '}';
    }
}

