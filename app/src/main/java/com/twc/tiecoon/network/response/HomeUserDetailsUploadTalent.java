package com.twc.tiecoon.network.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeUserDetailsUploadTalent implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("talentid")
    @Expose
    private String talentid;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("watermark")
    @Expose
    private String watermark;
    @SerializedName("createimage")
    @Expose
    private String createimage;
    @SerializedName("fontsize")
    @Expose
    private String fontsize;
    @SerializedName("fontcolor")
    @Expose
    private String fontcolor;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private Integer subcategoryId;
    @SerializedName("pricecode")
    @Expose
    private String pricecode;

    @SerializedName("liked")
    @Expose
    private String liked;

    @SerializedName("price")
    @Expose
    private Long price;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tresh")
    @Expose
    private String tresh;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("uploadtype")
    @Expose
    private Integer uploadtype;
    @SerializedName("video")
    @Expose
    private String video;

    @SerializedName("audio")
    @Expose
    private String audio;
    @SerializedName("talant_type")
    @Expose
    private String talant_type;
    @SerializedName("videothumbnail")
    @Expose
    private String videothumbnail;
    @SerializedName("talant_see")
    @Expose
    private String talant_see;
    @SerializedName("ownername")
    @Expose
    private String ownername;
    @SerializedName("textarea")
    @Expose
    private String textarea;
    @SerializedName("qbid")
    @Expose
    private String qbid;
    @SerializedName("talant_count")
    @Expose
    private String talant_count;
    @SerializedName("category")
    @Expose
    private AddProductCategoryData category=null;
    @SerializedName("sub_category")
    @Expose
    private AddProductSubCategoryData subCategory=null;
    @SerializedName("upload_image")
    @Expose
    private List<UploadImageListData> upload_image = null;
    @SerializedName("upload_video")
    @Expose
    private List<UploadVideoListData> upload_video = null;
 @SerializedName("tags")
    @Expose
    private List<UserDetailTags> tags = null;

    @SerializedName("is_like")
    @Expose
    private boolean is_like;


    public HomeUserDetailsUploadTalent() {
    }


    protected HomeUserDetailsUploadTalent(Parcel in) {
        category = in.readParcelable(getClass().getClassLoader());
        subCategory = in.readParcelable(getClass().getClassLoader());
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            userId = null;
        } else {
            userId = in.readInt();
        }
        talentid = in.readString();
        image = in.readString();
        path = in.readString();
        watermark = in.readString();
        createimage = in.readString();
        fontsize = in.readString();
        fontcolor = in.readString();
        qbid = in.readString();
        talant_count = in.readString();
        if (in.readByte() == 0) {
            categoryId = null;
        } else {
            categoryId = in.readInt();
        }
        if (in.readByte() == 0) {
            subcategoryId = null;
        } else {
            subcategoryId = in.readInt();
        }
        pricecode = in.readString();
        if (in.readByte() == 0) {
            price = null;
        } else {
            price = in.readLong();
        }
        description = in.readString();
        status = in.readString();
        tresh = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        if (in.readByte() == 0) {
            uploadtype = null;
        } else {
            uploadtype = in.readInt();
        }
        video = in.readString();
        audio = in.readString();
        talant_type = in.readString();
        videothumbnail = in.readString();
        talant_see = in.readString();
        ownername = in.readString();
        textarea = in.readString();
        category = in.readParcelable(AddProductCategoryData.class.getClassLoader());
        subCategory = in.readParcelable(AddProductSubCategoryData.class.getClassLoader());
        upload_image = in.createTypedArrayList(UploadImageListData.CREATOR);
        upload_video = in.createTypedArrayList(UploadVideoListData.CREATOR);
        tags = in.createTypedArrayList(UserDetailTags.CREATOR);
        is_like = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(category, flags);
        dest.writeParcelable(subCategory, flags);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (userId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(userId);
        }
        dest.writeString(talentid);
        dest.writeString(image);
        dest.writeString(path);
        dest.writeString(watermark);
        dest.writeString(createimage);
        dest.writeString(fontsize);
        dest.writeString(fontcolor);
        dest.writeString(qbid);
        dest.writeString(talant_count);
        if (categoryId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(categoryId);
        }
        if (subcategoryId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(subcategoryId);
        }
        dest.writeString(pricecode);
        if (price == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(price);
        }
        dest.writeString(description);
        dest.writeString(status);
        dest.writeString(tresh);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        if (uploadtype == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(uploadtype);
        }
        dest.writeString(video);
        dest.writeString(audio);
        dest.writeString(talant_type);
        dest.writeString(videothumbnail);
        dest.writeString(talant_see);
        dest.writeString(ownername);
        dest.writeString(textarea);
        dest.writeParcelable(category, flags);
        dest.writeParcelable(subCategory, flags);
        dest.writeTypedList(upload_image);
        dest.writeTypedList(upload_video);
        dest.writeTypedList(tags);
        dest.writeByte((byte) (is_like ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomeUserDetailsUploadTalent> CREATOR = new Creator<HomeUserDetailsUploadTalent>() {
        @Override
        public HomeUserDetailsUploadTalent createFromParcel(Parcel in) {
            return new HomeUserDetailsUploadTalent(in);
        }

        @Override
        public HomeUserDetailsUploadTalent[] newArray(int size) {
            return new HomeUserDetailsUploadTalent[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getTalentid() {
        return talentid;
    }

    public void setTalentid(String talentid) {
        this.talentid = talentid;
    }

    public String getImage() {
        return image;
    }

    public String getPath() {
        return path;
    }

    public String getWatermark() {
        return watermark;
    }

    public String getCreateimage() {
        return createimage;
    }

    public String getFontsize() {
        return fontsize;
    }

    public void setFontsize(String fontsize) {
        this.fontsize = fontsize;
    }

    public String getFontcolor() {
        return fontcolor;
    }

    public String getQbid() {
        return qbid;
    }

    public void setQbid(String qbid) {
        this.qbid = qbid;
    }

    public String getTalant_count() {
        return talant_count;
    }

    public void setTalant_count(String talant_count) {
        this.talant_count = talant_count;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public Integer getSubcategoryId() {
        return subcategoryId;
    }

    public String getPricecode() {
        return pricecode;
    }

    public Long getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getStatus() {
        return status;
    }

    public String getTresh() {
        return tresh;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public Integer getUploadtype() {
        return uploadtype;
    }

    public String getVideo() {
        return video;
    }



    public String getAudio() {
        return audio;
    }

    public String getTalant_type() {
        return talant_type;
    }

    public void setTalant_type(String talant_type) {
        this.talant_type = talant_type;
    }

    public String getVideothumbnail() {
        return videothumbnail;
    }

    public void setVideothumbnail(String videothumbnail) {
        this.videothumbnail = videothumbnail;
    }

    public String getTalant_see() {
        return talant_see;
    }

    public void setTalant_see(String talant_see) {
        this.talant_see = talant_see;
    }

    public String getOwnername() {
        return ownername;
    }

    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

    public String getTextarea() {
        return textarea;
    }

    public void setTextarea(String textarea) {
        this.textarea = textarea;
    }

    public void setCategory(AddProductCategoryData category) {
        this.category = category;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public AddProductCategoryData getCategory() {
        return category;
    }

    public AddProductSubCategoryData getSubCategory() {
        return subCategory;
    }

    public List<UploadImageListData> getUpload_image() {
        return upload_image;
    }

    public List<UploadVideoListData> getUpload_video() {
        return upload_video;
    }

    public void setUpload_video(List<UploadVideoListData> upload_video) {
        this.upload_video = upload_video;
    }

    public List<UserDetailTags> getTags() {
        return tags;
    }

    public void setTags(List<UserDetailTags> tags) {
        this.tags = tags;
    }

    public String getLiked() {
        return liked;
    }

    public void setLiked(String liked) {
        this.liked = liked;
    }


    public boolean isIs_like() {
        return is_like;
    }

    public void setIs_like(boolean is_like) {
        this.is_like = is_like;
    }

    @Override
    public String toString() {
        return "HomeUserDetailsUploadTalent{" +
                "id=" + id +
                ", userId=" + userId +
                ", image='" + image + '\'' +
                ", path='" + path + '\'' +
                ", watermark='" + watermark + '\'' +
                ", createimage='" + createimage + '\'' +
                ", fontsize='" + fontsize + '\'' +
                ", fontcolor='" + fontcolor + '\'' +
                ", categoryId=" + categoryId +
                ", subcategoryId=" + subcategoryId +
                ", pricecode='" + pricecode + '\'' +
                ", liked='" + liked + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", tresh='" + tresh + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                ", uploadtype=" + uploadtype +
                ", video='" + video + '\'' +
                ", audio='" + audio + '\'' +
                ", talant_type='" + talant_type + '\'' +
                ", videothumbnail='" + videothumbnail + '\'' +
                ", talant_see='" + talant_see + '\'' +
                ", textarea='" + textarea + '\'' +
                ", category=" + category +
                ", subCategory=" + subCategory +
                ", upload_image=" + upload_image +
                ", upload_video=" + upload_video +
                ", is_like=" + is_like +
                '}';
    }
}
