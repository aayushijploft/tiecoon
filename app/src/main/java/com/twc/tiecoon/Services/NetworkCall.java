package com.twc.tiecoon.Services;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.twc.tiecoon.ApplicationClass;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NetworkCall {
    private Context context;
    private ProgressDialog progressDialog;
    private String url,type;
    private HashMap<String, File> postFiles;
    private HashMap<String, String> postData;
    private MultiPartRequest request;
    private Boolean isShow;
    private boolean isHeader;
    private boolean isToken;

    public NetworkCall(Context context, String url, String type,
                       MultiPartRequest request,
                       HashMap<String, File> postFiles,
                       HashMap<String, String> postData, boolean isShow, boolean isHeader) {
        this.context=context;
        this.url=url;
        this.postFiles=postFiles;
        this.postData=postData;
        this.request=request;
        this.isShow=isShow;
        this.type=type;
        this.isHeader = isHeader;
        if (isConnectingToInternet()) {
            showPopup();
            sendData();
        }
    }

    public NetworkCall(Context context, String url, String type,
                       MultiPartRequest request,
                       HashMap<String, String> postData, boolean isShow, boolean isHeader) {
        this.context=context;
        this.url=url;
        this.postFiles=new HashMap<>();
        this.postData=postData;
        this.request=request;
        this.isShow=isShow;
        this.type=type;
        this.isHeader = isHeader;
        if (isConnectingToInternet()) {
            showPopup();
            sendData();
        }
    }

    public NetworkCall(Context context, String url, String  type, MultiPartRequest multiPartRequest, boolean isShow, boolean isHeader) {
        this.context=context;
        this.url=url;
        this.postFiles=new HashMap<>();
        this.postData=new HashMap<>();
        this.request=multiPartRequest;
        this.isShow=isShow;
        this.type=type;
        this.isHeader = isHeader;
        if (isConnectingToInternet()) {
            showPopup();
            sendData();
        }
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            @SuppressLint("MissingPermission") NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if (info != null)
                for (NetworkInfo anInfo : info)
                    if (anInfo.getState() == NetworkInfo.State.CONNECTED)
                        return true;
        }
        return false;
    }

    private void sendData() {

        Builders.Any.B ion = Ion.with(context).load(type, url);

        if (postData.containsKey("Authorization")) {
            String Authorization = postData.get("Authorization");
            ion.addHeader("Authorization", "Bearer " +  ApplicationClass.appPreferences.GetToken());
            postData.remove("Authorization");
        }

        if (isHeader)
            ion.addHeader("Content-Type", "multipart/form-data");

        List<Part> data = new ArrayList<>();
        if (postData.size()!=0) {
            for (String key : postData.keySet()) {
                String val = postData.get(key);
                if (val==null)
                    val="";
                if (isHeader)
                    ion.setBodyParameter(key, val);
                data.add(new StringPart(key, val));
            }
        }

        List<Part> files = new ArrayList<>();
        if (postFiles.size()!=0) {
            for (String key : postFiles.keySet()) {
                File val = postFiles.get(key);
                if (val!=null)
                    files.add(new FilePart(key, val));
            }
        }



        if (!isHeader) {
            if (files.size() != 0)
                ion.addMultipartParts(files);
            if (data.size() != 0)
                ion.addMultipartParts(data);
        }




        /*ion.asString().setCallback((e, result) -> {
            try {
                Log.e("response ==>", result+"_");
                //System.out.println("response ===> " + result);
                request.myResponseMethod(result);
            } catch (Exception e1) {
                e1.printStackTrace();
                request.myResponseMethod("");
            } finally {
                if (isShow)
                    progressDialog.dismiss();
            }
        });*/

        ion.asString().withResponse().setCallback((e, result) -> {
            try {

                //System.out.println("response ===> " + result);
                request.myResponseMethod(result.getResult(), result.getHeaders().code());
            } catch (Exception e1) {
                e1.printStackTrace();
                request.myResponseMethod("", 0);
            } finally {
                if (isShow)
                    progressDialog.dismiss();
            }
        });
    }

    private void showPopup() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        if (isShow)
            progressDialog.show();
    }
    public interface MultiPartRequest {

        void myResponseMethod(String response, int statusCode);
    }

}
