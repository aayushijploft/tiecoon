package com.twc.tiecoon.prefrences;

import android.app.Activity;
import android.content.SharedPreferences;

import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.ApplicationClass;

public class AppPreferences {
    private static AppPreferences instance;
    private SharedPreferences appSharedPref;
    private SharedPreferences.Editor prefEditor;
    private final String APP_NAME= "Tiecoon";

    private final String APP_TOKEN = "token";
    private final String FIREBASE_TOKEN = "firebasetoken";
    private final String EMAILID = "emaildId";
    private final String USERID = "userId";
    private final String USERNAME = "userName";
    private final String PHONENUMBER = "phoneNumber";
    private final String USERPICURL = "userpicurl";
    private final String FIRSTTIME = "firstTime";
    private final String ISVERIFY = "isverfy";

    private static final String QB_USER_ID = "qb_user_id";
    private static final String QB_USER_LOGIN = "qb_user_login";
    private static final String QB_USER_PASSWORD = "qb_user_password";
    private static final String QB_USER_FULL_NAME = "qb_user_full_name";
    private static final String QB_USER_TAGS = "qb_user_tags";

    private AppPreferences() {
        this.appSharedPref = ApplicationClass.mAppContext.getSharedPreferences(APP_NAME, Activity.MODE_PRIVATE);
        this.prefEditor = appSharedPref.edit();
    }

    public static AppPreferences getInstance() {
        if (instance == null) {
            instance = new AppPreferences();
        }
        return instance;
    }

    public String GetToken() {
        return appSharedPref.getString(APP_TOKEN, "");
    }

    public void SetToken(String FCMToken) {
        prefEditor.putString(APP_TOKEN, FCMToken).commit();
    }

    public String GetFirebaseToken() {
        return appSharedPref.getString(FIREBASE_TOKEN, "");
    }

    public void SetFirebaseToken(String FCMToken) {
        prefEditor.putString(FIREBASE_TOKEN, FCMToken).commit();
    }


    public String getEmailId() {
        return appSharedPref.getString(EMAILID, "");
    }

    public void setEmailId(String FCMToken) {
        prefEditor.putString(EMAILID, FCMToken).commit();
    }

  public String getReferId() {
        return appSharedPref.getString("Referid", "");
    }

    public void setReferId(String FCMToken) {
        prefEditor.putString("Referid", FCMToken).commit();
    }

    public String getUserName() {
        return appSharedPref.getString(USERNAME, "");
    }

    public void setUserName(String FCMToken) {
        prefEditor.putString(USERNAME, FCMToken).commit();
    }

    public String getPhoneNumber() {
        return appSharedPref.getString(PHONENUMBER, "");
    }

    public void setPhoneNumber(String FCMToken) {
        prefEditor.putString(PHONENUMBER, FCMToken).commit();
    }

    public String getUserPicUrl() {
        return appSharedPref.getString(USERPICURL, "");
    }

    public void setUserPicUrl(String FCMToken) {
        prefEditor.putString(USERPICURL, FCMToken).commit();
    }


    public int getUserId() {
        return appSharedPref.getInt(USERID, 0);
    }

    public void setUserId(int FCMToken) {
        prefEditor.putInt(USERID, FCMToken).commit();
    }

    public boolean getFirstTime() {
        return appSharedPref.getBoolean(FIRSTTIME, false);
    }

    public void setFirstTime(boolean FCMToken) {
        prefEditor.putBoolean(FIRSTTIME, FCMToken).commit();
    }

    public boolean getisverfy() {
        return appSharedPref.getBoolean(ISVERIFY, false);
    }

    public void setisverfy(boolean FCMToken) {
        prefEditor.putBoolean(ISVERIFY, FCMToken).commit();
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key) {
        return (T) appSharedPref.getAll().get(key);
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String key, T defValue) {
        T returnValue = (T) appSharedPref.getAll().get(key);
        return returnValue == null ? defValue : returnValue;
    }

    public void save(String key, Object value) {
        SharedPreferences.Editor editor = prefEditor;
        if (value instanceof Boolean) {
            editor.putBoolean(key, (Boolean) value);
        } else if (value instanceof Integer) {
            editor.putInt(key, (Integer) value);
        } else if (value instanceof Float) {
            editor.putFloat(key, (Float) value);
        } else if (value instanceof Long) {
            editor.putLong(key, (Long) value);
        } else if (value instanceof String) {
            editor.putString(key, (String) value);
        } else if (value instanceof Enum) {
            editor.putString(key, value.toString());
        } else if (value != null) {
            throw new RuntimeException("Attempting to save non-supported preference");
        }

        editor.commit();
    }

    public void saveQbUser(QBUser qbUser) {
        save(QB_USER_ID, qbUser.getId());
        save(QB_USER_LOGIN, qbUser.getLogin());
        save(QB_USER_PASSWORD, qbUser.getPassword());
        save(QB_USER_FULL_NAME, qbUser.getFullName());
        save(QB_USER_TAGS, qbUser.getTags().getItemsAsString());
    }

    public void removeQbUser() {
        delete(QB_USER_ID);
        delete(QB_USER_LOGIN);
        delete(QB_USER_PASSWORD);
        delete(QB_USER_FULL_NAME);
        delete(QB_USER_TAGS);
    }
    public void delete(String key) {
        if (appSharedPref.contains(key)) {
            prefEditor.remove(key).commit();
        }
    }

    public boolean has(String key) {
        return appSharedPref.contains(key);
    }
    public boolean hasQbUser() {
        return has(QB_USER_LOGIN) && has(QB_USER_PASSWORD);
    }
    public QBUser getQbUser() {
        if (hasQbUser()) {
            Integer id = get(QB_USER_ID);
            String login = get(QB_USER_LOGIN);
            String password = get(QB_USER_PASSWORD);
            String fullName = get(QB_USER_FULL_NAME);
            String tagsInString = get(QB_USER_TAGS);

            StringifyArrayList<String> tags = null;

            if (tagsInString != null) {
                tags = new StringifyArrayList<>();
                tags.add(tagsInString.split(","));
            }

            QBUser user = new QBUser(login, password);
            user.setId(id);
            user.setFullName(fullName);
            user.setTags(tags);
            return user;
        } else {
            return null;
        }
    }

}
