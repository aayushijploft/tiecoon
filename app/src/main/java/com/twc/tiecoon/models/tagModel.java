package com.twc.tiecoon.models;

import java.io.Serializable;

public class tagModel implements Serializable {

    public String Name;
    public String Id;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    @Override
    public String toString() {
        return "tagModel{" +
                "Name='" + Name + '\'' +
                ", Id='" + Id + '\'' +
                '}';
    }
}
