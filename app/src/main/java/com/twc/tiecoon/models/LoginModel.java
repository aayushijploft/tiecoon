package com.twc.tiecoon.models;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.OTPVerificationSocial;
import com.twc.tiecoon.network.response.OTPVerification;
import com.twc.tiecoon.utils.RetrofitCallback;
import com.twc.tiecoon.utils.Utils;

import androidx.annotation.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginModel extends BasicModel {
    public void registerByPhone(final Context ctx, final View view, String type,String phone) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        Call<OTPVerification> call1 = apiInterface.registerByPhone(type,phone);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<OTPVerification>() {
            @Override
            public void onResponse(@NonNull Call<OTPVerification> call, @NonNull Response<OTPVerification> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                        Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<OTPVerification> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void registerByPhoneForSocial(final Context ctx, final View view, String email,String phone) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        Call<OTPVerificationSocial> call1 = apiInterface.sendOtp(email,phone);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<OTPVerificationSocial>() {
            @Override
            public void onResponse(@NonNull Call<OTPVerificationSocial> call, @NonNull Response<OTPVerificationSocial> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                        Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<OTPVerificationSocial> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void Resend_Otp(final Context ctx, final View view,String phone) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        Call<OTPVerificationSocial> call1 = apiInterface.ResendOtp(phone);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<OTPVerificationSocial>() {
            @Override
            public void onResponse(@NonNull Call<OTPVerificationSocial> call, @NonNull Response<OTPVerificationSocial> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                        Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<OTPVerificationSocial> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }


    public void registerBySocial(final Context ctx, final View view,String appId,String image, String type,String email, String phone) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        Call<OTPVerification> call1 = apiInterface.registerBySocial(appId, image,type,email,phone);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<OTPVerification>() {
            @Override
            public void onResponse(@NonNull Call<OTPVerification> call, @NonNull Response<OTPVerification> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        Log.d("responseSocail",""+response.body().getData().getUser());
                        notifyObservers(response.body().getData());
                        Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<OTPVerification> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void otpVerify(final Context ctx, final View view, String type, String phone, String otp) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        Call<OTPVerification> call1 = apiInterface.verifyOTP(type,phone, otp);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<OTPVerification>() {
            @Override
            public void onResponse(@NonNull Call<OTPVerification> call, @NonNull Response<OTPVerification> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, "Invalid OTP");
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, "Invalid OTP");
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<OTPVerification> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, "Invalid OTP");
            }
        }));
    }
    public void EmailotpVerify(final Context ctx, final View view, String type, String email, String otp) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        Call<OTPVerification> call1 = apiInterface.verifyEmailOTP(type,email, otp);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<OTPVerification>() {
            @Override
            public void onResponse(@NonNull Call<OTPVerification> call, @NonNull Response<OTPVerification> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, "Invalid OTP");
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, "Invalid OTP");
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<OTPVerification> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, "Invalid OTP");
            }
        }));
    }
}
