package com.twc.tiecoon.models;


public class FilterUserData {
    private String name="";
    private String language="";
    private String gender="";
    private String age="";
    private String workType="";
    private String productCategory="";
    private String productLocation="";
    private String productCurrency="";
    private String productPrice="";
    private String talantName="";
    private String talantCurrency="";
    private String talantPrice="";
    private String serviceName="";
    private String filterType="";
    private String serviceDay="";
    private String serviceLocation="";
    private String projectName="";
    private String projectCurrency="";
    private String projectPrice="";
    private String projectgender="";


    public FilterUserData() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return language;
    }

    public void setLocation(String location) {
        this.language = location;
    }

   public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAgeRange() {
        return age;
    }

    public void setAgeRange(String ageRange) {
        this.age = ageRange;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductLocation() {
        return productLocation;
    }

    public void setProductLocation(String productLocation) {
        this.productLocation = productLocation;
    }

    public String getProductCurrency() {
        return productCurrency;
    }

    public void setProductCurrency(String productCurrency) {
        this.productCurrency = productCurrency;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getTalantName() {
        return talantName;
    }

    public void setTalantName(String talantName) {
        this.talantName = talantName;
    }

    public String getTalantCurrency() {
        return talantCurrency;
    }

    public void setTalantCurrency(String talantCurrency) {
        this.talantCurrency = talantCurrency;
    }

    public String getTalantPrice() {
        return talantPrice;
    }

    public void setTalantPrice(String talantPrice) {
        this.talantPrice = talantPrice;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDay() {
        return serviceDay;
    }

    public void setServiceDay(String serviceDay) {
        this.serviceDay = serviceDay;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectCurrency() {
        return projectCurrency;
    }

    public void setProjectCurrency(String projectCurrency) {
        this.projectCurrency = projectCurrency;
    }

    public String getProjectPrice() {
        return projectPrice;
    }

    public void setProjectPrice(String projectPrice) {
        this.projectPrice = projectPrice;
    }

    public String getServiceLocation() {
        return serviceLocation;
    }

    public void setServiceLocation(String serviceLocation) {
        this.serviceLocation = serviceLocation;
    }

    public String getProjectgender() {
        return projectgender;
    }

    public void setProjectgender(String projectgender) {
        this.projectgender = projectgender;
    }

    public String getFilterType() {
        return filterType;
    }

    public void setFilterType(String filterType) {
        this.filterType = filterType;
    }
}
