package com.twc.tiecoon.models;

public class SingleItemRatingModel {
    private String picURL;
    private String userName;
    private String rating;
    private String des;

    public SingleItemRatingModel(String picURL, String userName, String rating, String des) {
        this.picURL = picURL;
        this.userName = userName;
        this.rating = rating;
        this.des = des;
    }

    public String getPicURL() {
        return picURL;
    }

    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}
