package com.twc.tiecoon.models;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.facebook.login.LoginManager;
import com.twc.tiecoon.Activities.LoginActivity;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddKYC;
import com.twc.tiecoon.network.response.AddService;
import com.twc.tiecoon.network.response.AddStory;
import com.twc.tiecoon.network.response.AgeGroupData;
import com.twc.tiecoon.network.response.BankDetails;
import com.twc.tiecoon.network.response.ButtonModel;
import com.twc.tiecoon.network.response.CardResponse;
import com.twc.tiecoon.network.response.ChatData;
import com.twc.tiecoon.network.response.CheckProjectModel;
import com.twc.tiecoon.network.response.CityData;
import com.twc.tiecoon.network.response.CountryData;
import com.twc.tiecoon.network.response.CreateAssignment;
import com.twc.tiecoon.network.response.CreateProduct;
import com.twc.tiecoon.network.response.DeleteStory;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.network.response.EditInfo;
import com.twc.tiecoon.network.response.EnquiryModel;
import com.twc.tiecoon.network.response.GetBankDetails;
import com.twc.tiecoon.network.response.GroupChatModel;
import com.twc.tiecoon.network.response.HelpUserDetails;
import com.twc.tiecoon.network.response.HomeUserDetail;
import com.twc.tiecoon.network.response.KycDetails;
import com.twc.tiecoon.network.response.NotificationData;
import com.twc.tiecoon.network.response.OnetooneChatModel;
import com.twc.tiecoon.network.response.PreferredLanguage;
import com.twc.tiecoon.network.response.ProductCategory;
import com.twc.tiecoon.network.response.ProjectDetails;
import com.twc.tiecoon.network.response.ProjectRequestModel;
import com.twc.tiecoon.network.response.ReportAdd;
import com.twc.tiecoon.network.response.ReportList;
import com.twc.tiecoon.network.response.SaveProject;
import com.twc.tiecoon.network.response.SaveProjectList;
import com.twc.tiecoon.network.response.ServiceCategory;
import com.twc.tiecoon.network.response.StateData;
import com.twc.tiecoon.network.response.StoryView;
import com.twc.tiecoon.network.response.SubCategory;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.Profile;
import com.twc.tiecoon.network.response.ProfileFirstUpdate;
import com.twc.tiecoon.network.response.AddBrands;
import com.twc.tiecoon.network.response.AddProducts;
import com.twc.tiecoon.network.response.AddTalent;
import com.twc.tiecoon.network.response.SuggestTalent;
import com.twc.tiecoon.network.response.TagUserList;
import com.twc.tiecoon.network.response.TalantCat;
import com.twc.tiecoon.network.response.TalentCurrency;
import com.twc.tiecoon.network.response.TransactionModel;
import com.twc.tiecoon.utils.RetrofitCallback;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.FieldMap;

public class ProfileModel extends BasicModel {
    public void getMyProfileDetails(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<HomeUserDetail> call1 = apiInterface.getMyProfileDetails("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<HomeUserDetail>() {
            @Override
            public void onResponse(@NonNull Call<HomeUserDetail> call, @NonNull Response<HomeUserDetail> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<HomeUserDetail> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void deletTalent(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deletTalent("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deleted");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getUserDetails(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<Profile> call1 = apiInterface.getUserDetails("Bearer " + accessToken);
        Log.e("TokenUser22",ApplicationClass.appPreferences.GetToken());
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<Profile>() {
            @Override
            public void onResponse(@NonNull Call<Profile> call, @NonNull Response<Profile> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        Utils.dismissProDialog();
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<Profile> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void updateProfile(final Context ctx, final View view, MultipartBody.Part image, String name, String status, String email,
                              String phone, String gender, int age, String location, String bio,
                              ArrayList<String> list,
                              ArrayList<Integer> languagelist) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody statusBody = RequestBody.create(MediaType.parse("text/plain"), status);
        RequestBody emailBody = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody phoneBody = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody genderBody = RequestBody.create(MediaType.parse("text/plain"), gender);
        RequestBody locationBody = RequestBody.create(MediaType.parse("text/plain"), location);
        RequestBody bioBody = RequestBody.create(MediaType.parse("text/plain"), bio);
        Call<ProfileFirstUpdate> call1 = apiInterface.updateProfile("Bearer " + accessToken, image, nameBody,
                statusBody, emailBody, phoneBody, genderBody, age, locationBody, bioBody, list,languagelist);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ProfileFirstUpdate>() {
            @Override
            public void onResponse(@NonNull Call<ProfileFirstUpdate> call, @NonNull Response<ProfileFirstUpdate> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ProfileFirstUpdate> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }


    public void getCategoryCurrency(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CategoryCurrency> call1 = apiInterface.getCategoryCurrency("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CategoryCurrency>() {
            @Override
            public void onResponse(@NonNull Call<CategoryCurrency> call, @NonNull Response<CategoryCurrency> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CategoryCurrency> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getProductCategoryCurrency(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CategoryCurrency> call1 = apiInterface.getProductCategoryCurrency("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CategoryCurrency>() {
            @Override
            public void onResponse(@NonNull Call<CategoryCurrency> call, @NonNull Response<CategoryCurrency> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CategoryCurrency> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getServiceCategoryCurrency(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CategoryCurrency> call1 = apiInterface.getServiceCategoryCurrency("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CategoryCurrency>() {
            @Override
            public void onResponse(@NonNull Call<CategoryCurrency> call, @NonNull Response<CategoryCurrency> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CategoryCurrency> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getTalentCurrency(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<TalentCurrency> call1 = apiInterface.getTalentCurrency("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<TalentCurrency>() {
            @Override
            public void onResponse(@NonNull Call<TalentCurrency> call, @NonNull Response<TalentCurrency> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<TalentCurrency> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getSubCategory(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SubCategory> call1 = apiInterface.getSubCategory("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<SubCategory>() {
            @Override
            public void onResponse(@NonNull Call<SubCategory> call, @NonNull Response<SubCategory> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<SubCategory> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void uploadTalents(final Context ctx, final View view, String waterMark,
                              String createImage,  MultipartBody.Part[] partList,
                              String fontSize, String fontColor, ArrayList<String> list,

                              MultipartBody.Part videothumbnail,MultipartBody.Part image, MultipartBody.Part audio, MultipartBody.Part video, String description, String textarea, String price, String priceCode, String categoryId, String subCategotyId) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody waterMarkBody = RequestBody.create(MediaType.parse("text/plain"), waterMark);
        RequestBody createImageBody = RequestBody.create(MediaType.parse("text/plain"), createImage);
        RequestBody fontSizeBody = RequestBody.create(MediaType.parse("text/plain"), fontSize);
        RequestBody fontColorBody = RequestBody.create(MediaType.parse("text/plain"), fontColor);
        RequestBody descriptionBody = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody textareaBody = RequestBody.create(MediaType.parse("text/plain"), textarea);
        RequestBody priceBody = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody priceCodeBody = RequestBody.create(MediaType.parse("text/plain"), priceCode);
        RequestBody categoryIdBody = RequestBody.create(MediaType.parse("text/plain"), categoryId);
        RequestBody subCategotyIdBody = RequestBody.create(MediaType.parse("text/plain"), subCategotyId);

        Call<AddTalent> call1 = apiInterface.uploadTalents("Bearer " + accessToken, waterMarkBody, null,partList, fontSizeBody,
                fontColorBody,list,videothumbnail, image,audio,video, descriptionBody,textareaBody, priceBody, priceCodeBody, subCategotyIdBody, categoryIdBody);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<AddTalent>() {
            @Override
            public void onResponse(@NonNull Call<AddTalent> call, @NonNull Response<AddTalent> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<AddTalent> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void updateTalents(final Context ctx, final View view, String waterMark
            , String createImage,MultipartBody.Part[] partList, String fontSize, String fontColor, ArrayList<String> list,
                              MultipartBody.Part videothumbnail,MultipartBody.Part image,  MultipartBody.Part audio, MultipartBody.Part video,String description,String textarea, String price, String priceCode, String categoryId, String subCategotyId,
                             String talentId ) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody waterMarkBody = RequestBody.create(MediaType.parse("text/plain"), waterMark);
        RequestBody createImageBody = RequestBody.create(MediaType.parse("text/plain"), createImage);
        RequestBody fontSizeBody = RequestBody.create(MediaType.parse("text/plain"), fontSize);
        RequestBody fontColorBody = RequestBody.create(MediaType.parse("text/plain"), fontColor);
        RequestBody descriptionBody = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody textareaBody = RequestBody.create(MediaType.parse("text/plain"), textarea);
        RequestBody priceBody = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody priceCodeBody = RequestBody.create(MediaType.parse("text/plain"), priceCode);
        RequestBody categoryIdBody = RequestBody.create(MediaType.parse("text/plain"), categoryId);
        RequestBody subCategotyIdBody = RequestBody.create(MediaType.parse("text/plain"), subCategotyId);
        RequestBody talentIdBody = RequestBody.create(MediaType.parse("text/plain"), talentId);
        Call<AddTalent> call1 = apiInterface.updateTalents("Bearer " + accessToken, waterMarkBody, null, partList,fontSizeBody,
                fontColorBody, list,videothumbnail,image, audio,video,descriptionBody, textareaBody,priceBody, priceCodeBody, subCategotyIdBody, categoryIdBody,talentIdBody);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<AddTalent>() {
            @Override
            public void onResponse(@NonNull Call<AddTalent> call, @NonNull Response<AddTalent> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.dismissProDialog();
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.dismissProDialog();
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<AddTalent> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void updateUserImage(final Context ctx, final View view,  MultipartBody.Part image ) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.updateUserImage("Bearer " + accessToken, image);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void deleteTalents(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteTalents("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }


    public void createAssignment(final Context ctx, final View view, Map<String, String> parem) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CreateAssignment> call1 = apiInterface.createAssignment("Bearer " + accessToken,parem);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CreateAssignment>() {
            @Override
            public void onResponse(@NonNull Call<CreateAssignment> call, @NonNull Response<CreateAssignment> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                      //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                  //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CreateAssignment> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void createProduct(final Context ctx, final View view,  MultipartBody.Part[] partList, MultipartBody.Part video, MultipartBody.Part videothumbnail,String category_id,
                              String subcategory_id, String currency, String amount, String qty, String outofstock,
                              String description,ArrayList<String> list,String location , String name, String phone, String pincode, String address
            , String town, String city, String state) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody Requestcategory_id = RequestBody.create(MediaType.parse("text/plain"), category_id);
        RequestBody Requestsubcategory_id = RequestBody.create(MediaType.parse("text/plain"), subcategory_id);
        RequestBody Requestcurrency = RequestBody.create(MediaType.parse("text/plain"), currency);
        RequestBody Requestamount = RequestBody.create(MediaType.parse("text/plain"), amount);
        RequestBody Requestqty = RequestBody.create(MediaType.parse("text/plain"), qty);
        RequestBody Requestoutofstock = RequestBody.create(MediaType.parse("text/plain"), outofstock);
        RequestBody Requestdescription = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody Requestlocation = RequestBody.create(MediaType.parse("text/plain"), location);
        RequestBody Requestname = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody Requestphone = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody Requestpincode = RequestBody.create(MediaType.parse("text/plain"), pincode);
        RequestBody Requestaddress = RequestBody.create(MediaType.parse("text/plain"), address);
        RequestBody Requesttown = RequestBody.create(MediaType.parse("text/plain"), town);
        RequestBody Requestcity = RequestBody.create(MediaType.parse("text/plain"), city);
        RequestBody Requeststate = RequestBody.create(MediaType.parse("text/plain"), state);
        Call<CreateProduct> call1 = apiInterface.createProduct("Bearer " + accessToken,partList,video,videothumbnail,Requestcategory_id,Requestsubcategory_id,
                Requestcurrency,Requestamount,Requestqty,Requestoutofstock,Requestdescription,list,Requestlocation,
                Requestname,Requestphone,Requestpincode,Requestaddress,Requesttown,Requestcity,Requeststate);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CreateProduct>() {
            @Override
            public void onResponse(@NonNull Call<CreateProduct> call, @NonNull Response<CreateProduct> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                      //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                  //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CreateProduct> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void sendHelp(final Context ctx, final View view, String name, String  email, String description) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<HelpUserDetails> call1 = apiInterface.helpandsupport("Bearer " + accessToken, name,
                email, description);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<HelpUserDetails>() {
            @Override
            public void onResponse(@NonNull Call<HelpUserDetails> call, @NonNull Response<HelpUserDetails> response) {
                if (response.body() != null && response.body().getSuccess()) {
                        notifyObservers(response.body());
                } else {
                      Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<HelpUserDetails> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void suggestTalent(final Context ctx, final View view, String name,  String description) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SuggestTalent> call1 = apiInterface.suggestTalent("Bearer " + accessToken, name, description);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<SuggestTalent>() {
            @Override
            public void onResponse(@NonNull Call<SuggestTalent> call, @NonNull Response<SuggestTalent> response) {
                if (response.body() != null && response.body().getSuccess()) {
                        notifyObservers(response.body());
                } else {
                      Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<SuggestTalent> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void updateAssignment(final Context ctx, final View view,Map<String,String> param) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
//        RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), name);
//        RequestBody talentIdBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(talentId));
//        RequestBody textBody = RequestBody.create(MediaType.parse("text/plain"), text);
//        RequestBody amountBody = RequestBody.create(MediaType.parse("text/plain"), amount);
//        RequestBody startDateBody = RequestBody.create(MediaType.parse("text/plain"), startDate);
//        RequestBody endDateBody = RequestBody.create(MediaType.parse("text/plain"), endDate);
//        RequestBody assignmentIdBody = RequestBody.create(MediaType.parse("text/plain"), assignmentID);
        Call<CreateAssignment> call1 = apiInterface.updateAssignment("Bearer " + accessToken, param);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CreateAssignment>() {
            @Override
            public void onResponse(@NonNull Call<CreateAssignment> call, @NonNull Response<CreateAssignment> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CreateAssignment> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void deleteAssignment(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteAssignment("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("assignmentDeleted");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void uploadProduct(final Context ctx, final View view, String waterMark, String createImage,MultipartBody.Part[] partList, String fontSize, String fontColor,
                              MultipartBody.Part image, String description, String price, String priceCode, String categoryId, String subCategotyId,String location) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody waterMarkBody = RequestBody.create(MediaType.parse("text/plain"), waterMark);
        RequestBody createImageBody = RequestBody.create(MediaType.parse("text/plain"), createImage);
        RequestBody fontSizeBody = RequestBody.create(MediaType.parse("text/plain"), fontSize);
        RequestBody fontColorBody = RequestBody.create(MediaType.parse("text/plain"), fontColor);
        RequestBody descriptionBody = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody priceBody = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody priceCodeBody = RequestBody.create(MediaType.parse("text/plain"), priceCode);
        RequestBody categoryIdBody = RequestBody.create(MediaType.parse("text/plain"), categoryId);
        RequestBody subCategotyIdBody = RequestBody.create(MediaType.parse("text/plain"), subCategotyId);
        RequestBody locationBody = RequestBody.create(MediaType.parse("text/plain"), location);
        Call<AddProducts> call1 = apiInterface.uploadProduct("Bearer " + accessToken, waterMarkBody, null,partList, fontSizeBody,
                fontColorBody, image, descriptionBody, priceBody, priceCodeBody, subCategotyIdBody, categoryIdBody,locationBody);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<AddProducts>() {
            @Override
            public void onResponse(@NonNull Call<AddProducts> call, @NonNull Response<AddProducts> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<AddProducts> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }


    public void updateProduct(final Context ctx, final View view, String waterMark, String createImage, MultipartBody.Part[] partList,String fontSize, String fontColor,
                              MultipartBody.Part image, String description, String price, String priceCode,
                              String categoryId, String subCategotyId,String productId,String location) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody waterMarkBody = RequestBody.create(MediaType.parse("text/plain"), waterMark);
        RequestBody createImageBody = RequestBody.create(MediaType.parse("text/plain"), createImage);
        RequestBody fontSizeBody = RequestBody.create(MediaType.parse("text/plain"), fontSize);
        RequestBody fontColorBody = RequestBody.create(MediaType.parse("text/plain"), fontColor);
        RequestBody descriptionBody = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody priceBody = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody priceCodeBody = RequestBody.create(MediaType.parse("text/plain"), priceCode);
        RequestBody categoryIdBody = RequestBody.create(MediaType.parse("text/plain"), categoryId);
        RequestBody subCategotyIdBody = RequestBody.create(MediaType.parse("text/plain"), subCategotyId);
        RequestBody productIdBody = RequestBody.create(MediaType.parse("text/plain"), productId);
        RequestBody locationBody = RequestBody.create(MediaType.parse("text/plain"), location);
        Call<AddProducts> call1 = apiInterface.updateProduct("Bearer " + accessToken, waterMarkBody, null, partList,fontSizeBody,
                fontColorBody, image, descriptionBody, priceBody, priceCodeBody, subCategotyIdBody, categoryIdBody,productIdBody,locationBody);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<AddProducts>() {
            @Override
            public void onResponse(@NonNull Call<AddProducts> call, @NonNull Response<AddProducts> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<AddProducts> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }


    public void uploadService(final Context ctx, final View view, String waterMark, String createImage,MultipartBody.Part[] partList, String fontSize, String fontColor,
                              MultipartBody.Part image, String description, String price, String priceCode, String categoryId, String subCategotyId,
                              String days,String location) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody waterMarkBody = RequestBody.create(MediaType.parse("text/plain"), waterMark);
        RequestBody createImageBody = RequestBody.create(MediaType.parse("text/plain"), createImage);
        RequestBody fontSizeBody = RequestBody.create(MediaType.parse("text/plain"), fontSize);
        RequestBody fontColorBody = RequestBody.create(MediaType.parse("text/plain"), fontColor);
        RequestBody descriptionBody = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody priceBody = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody priceCodeBody = RequestBody.create(MediaType.parse("text/plain"), priceCode);
        RequestBody categoryIdBody = RequestBody.create(MediaType.parse("text/plain"), categoryId);
        RequestBody subCategotyIdBody = RequestBody.create(MediaType.parse("text/plain"), subCategotyId);
        RequestBody daysBody = RequestBody.create(MediaType.parse("text/plain"), days);
        RequestBody locationBody = RequestBody.create(MediaType.parse("text/plain"), location);
        Call<AddService> call1 = apiInterface.uploadService("Bearer " + accessToken, waterMarkBody, null,partList, fontSizeBody,
                fontColorBody, image, descriptionBody, priceBody, priceCodeBody, subCategotyIdBody, categoryIdBody,daysBody,locationBody);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<AddService>() {
            @Override
            public void onResponse(@NonNull Call<AddService> call, @NonNull Response<AddService> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<AddService> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void updateService(final Context ctx, final View view, String waterMark, String createImage,MultipartBody.Part[] partList, String fontSize, String fontColor,
                              MultipartBody.Part image, String description, String price, String priceCode,
                              String categoryId, String subCategotyId,String serviceId,String days,String location) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody waterMarkBody = RequestBody.create(MediaType.parse("text/plain"), waterMark);
        RequestBody createImageBody = RequestBody.create(MediaType.parse("text/plain"), createImage);
        RequestBody fontSizeBody = RequestBody.create(MediaType.parse("text/plain"), fontSize);
        RequestBody fontColorBody = RequestBody.create(MediaType.parse("text/plain"), fontColor);
        RequestBody descriptionBody = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody priceBody = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody priceCodeBody = RequestBody.create(MediaType.parse("text/plain"), priceCode);
        RequestBody categoryIdBody = RequestBody.create(MediaType.parse("text/plain"), categoryId);
        RequestBody subCategotyIdBody = RequestBody.create(MediaType.parse("text/plain"), subCategotyId);
        RequestBody serviceIdBody = RequestBody.create(MediaType.parse("text/plain"), serviceId);
        RequestBody daysBody = RequestBody.create(MediaType.parse("text/plain"), days);
        RequestBody locationBody = RequestBody.create(MediaType.parse("text/plain"), location);
        Call<AddService> call1 = apiInterface.updateService("Bearer " + accessToken, waterMarkBody, null, partList,fontSizeBody,
                fontColorBody, image, descriptionBody, priceBody, priceCodeBody, subCategotyIdBody, categoryIdBody,serviceIdBody,daysBody,locationBody);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<AddService>() {
            @Override
            public void onResponse(@NonNull Call<AddService> call, @NonNull Response<AddService> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<AddService> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }


    public void uploadBrands(final Context ctx, final View view, MultipartBody.Part image, String talent_id, String cat_id,
                             String subcat_id, String name, String price, String description, String link1, String link2, String link3) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody talent_idBody = RequestBody.create(MediaType.parse("text/plain"), talent_id);
        RequestBody cat_idBody = RequestBody.create(MediaType.parse("text/plain"), cat_id);
        RequestBody subcat_idBody = RequestBody.create(MediaType.parse("text/plain"), subcat_id);
        RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody priceBody = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody descriptionBody = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody link1Body = RequestBody.create(MediaType.parse("text/plain"), link1);
        RequestBody link2Body = RequestBody.create(MediaType.parse("text/plain"), link2);
        RequestBody link3Body = RequestBody.create(MediaType.parse("text/plain"), link3);
        Call<AddBrands> call1 = apiInterface.uploadBrands("Bearer " + accessToken, image, talent_idBody, cat_idBody,
                subcat_idBody, nameBody, priceBody, descriptionBody, link1Body, link2Body, link3Body);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<AddBrands>() {
            @Override
            public void onResponse(@NonNull Call<AddBrands> call, @NonNull Response<AddBrands> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<AddBrands> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }


    public void updateBrands(final Context ctx, final View view, MultipartBody.Part image, String talent_id, String cat_id,
                             String subcat_id, String name, String price, String description, String link1, String link2, String link3,
                             String brandId) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody talent_idBody = RequestBody.create(MediaType.parse("text/plain"), talent_id);
        RequestBody cat_idBody = RequestBody.create(MediaType.parse("text/plain"), cat_id);
        RequestBody subcat_idBody = RequestBody.create(MediaType.parse("text/plain"), subcat_id);
        RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody priceBody = RequestBody.create(MediaType.parse("text/plain"), price);
        RequestBody descriptionBody = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody link1Body = RequestBody.create(MediaType.parse("text/plain"), link1);
        RequestBody link2Body = RequestBody.create(MediaType.parse("text/plain"), link2);
        RequestBody link3Body = RequestBody.create(MediaType.parse("text/plain"), link3);
        RequestBody brandIdBody = RequestBody.create(MediaType.parse("text/plain"), brandId);
        Call<AddBrands> call1 = apiInterface.updateBrands("Bearer " + accessToken, image, talent_idBody, cat_idBody,
                subcat_idBody, nameBody, priceBody, descriptionBody, link1Body, link2Body, link3Body,brandIdBody);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<AddBrands>() {
            @Override
            public void onResponse(@NonNull Call<AddBrands> call, @NonNull Response<AddBrands> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<AddBrands> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getPreferredLang(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<PreferredLanguage> call1 = apiInterface.getPreferredLang("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<PreferredLanguage>() {
            @Override
            public void onResponse(@NonNull Call<PreferredLanguage> call, @NonNull Response<PreferredLanguage> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        Utils.dismissProDialog();
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<PreferredLanguage> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void updateUserProfile(final Context ctx, final View view,String status,String works,String livesin,ArrayList<Integer> language,
                                  String gander, String respondwithin, String dob,String phone, String name,String bio,String update_status,String  phonetype) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<EditInfo> call1 = apiInterface.editUserProfile("Bearer " + accessToken,status,works,livesin,language,
                gander,respondwithin,dob,phone,name,bio,update_status, phonetype);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<EditInfo>() {
            @Override
            public void onResponse(@NonNull Call<EditInfo> call, @NonNull Response<EditInfo> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        Utils.dismissProDialog();
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<EditInfo> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }


    public void deleteBrands(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteBrands("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deleteBrands");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void deleteProduct(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteProduct("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deleteProduct");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void deleteServices(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteService("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deleteServices");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void deleteUserImage(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.DeleteUserImage("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deleteUserImage");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void updateUserFirebaseToken(final Context ctx, final View view, String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.updateFirebaseToken("Bearer " + accessToken, id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    Log.d("FirebaseTokenSuccess",id);
                }
                else {
                    ApplicationClass.appPreferences.SetToken("");
                    LoginManager.getInstance().logOut();
                    ctx.startActivity(new Intent(ctx, LoginActivity.class));
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void sendNotification(final Context ctx, final View view, String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.sendNotification("Bearer " + accessToken, id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    notifyObservers(response.body() );
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
//                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void updateuserstatus(final Context ctx, final View view, String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.updateUserStatus("Bearer " + accessToken, id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    notifyObservers(response.body() );
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
//                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void sendChat(final Context ctx, final View view, String id,String massage) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.sentChat("Bearer " + accessToken, id,massage);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    Log.d("FirebaseTokenSuccess",id);
                }
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getChatList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ChatData> call1 = apiInterface.getChatList("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ChatData>() {
            @Override
            public void onResponse(@NonNull Call<ChatData> call, @NonNull Response<ChatData> response) {
                if (response.body() != null && response.body().getSuccess()) {
                  notifyObservers(response.body());
                }else
                {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ChatData> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getNotificationList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<NotificationData> call1 = apiInterface.getNotificationList("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<NotificationData>() {
            @Override
            public void onResponse(@NonNull Call<NotificationData> call, @NonNull Response<NotificationData> response) {
                if (response.body() != null && response.body().getSuccess()) {
                  notifyObservers(response.body());
                }else
                {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<NotificationData> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void saveStory(final Context ctx, final View view, MultipartBody.Part multiBodyPart) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<AddStory> call1 = apiInterface.uploadStory("Bearer " + accessToken,multiBodyPart);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<AddStory>() {
            @Override
            public void onResponse(@NonNull Call<AddStory> call, @NonNull Response<AddStory> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<AddStory> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getAgeGroup(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<AgeGroupData> call1 = apiInterface.getAgeGroup("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<AgeGroupData>() {
            @Override
            public void onResponse(@NonNull Call<AgeGroupData> call, @NonNull Response<AgeGroupData> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<AgeGroupData> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void getCountry(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CountryData> call1 = apiInterface.getCountry();
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CountryData>() {
            @Override
            public void onResponse(@NonNull Call<CountryData> call, @NonNull Response<CountryData> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CountryData> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void getState(final Context ctx, final View view, Map<String,String> map) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<StateData> call1 = apiInterface.getState(map);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<StateData>() {
            @Override
            public void onResponse(@NonNull Call<StateData> call, @NonNull Response<StateData> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<StateData> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getCity(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CityData> call1 = apiInterface.getCity(id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CityData>() {
            @Override
            public void onResponse(@NonNull Call<CityData> call, @NonNull Response<CityData> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CityData> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void deletStory(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteStory> call1 = apiInterface.deletStory("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteStory>() {
            @Override
            public void onResponse(@NonNull Call<DeleteStory> call, @NonNull Response<DeleteStory> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteStory> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void viewProjectDetails(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProjectDetails> call1 = apiInterface.projectDetails("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ProjectDetails>() {
            @Override
            public void onResponse(@NonNull Call<ProjectDetails> call, @NonNull Response<ProjectDetails> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ProjectDetails> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void saveProject(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SaveProject> call1 = apiInterface.saveProject("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<SaveProject>() {
            @Override
            public void onResponse(@NonNull Call<SaveProject> call, @NonNull Response<SaveProject> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<SaveProject> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void saveProjectList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SaveProjectList> call1 = apiInterface.saveProjectList("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<SaveProjectList>() {
            @Override
            public void onResponse(@NonNull Call<SaveProjectList> call, @NonNull Response<SaveProjectList> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<SaveProjectList> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getReportReasons(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ReportList> call1 = apiInterface.getReportReason("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ReportList>() {
            @Override
            public void onResponse(@NonNull Call<ReportList> call, @NonNull Response<ReportList> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ReportList> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void addReport(final Context ctx, final View view,String urseid,String reportid,String description) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ReportAdd> call1 = apiInterface.addREPORT("Bearer " + accessToken,urseid,reportid,description);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ReportAdd>() {
            @Override
            public void onResponse(@NonNull Call<ReportAdd> call, @NonNull Response<ReportAdd> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ReportAdd> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void getTagUserList(final Context ctx, final View view,String search) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<TagUserList> call1 = apiInterface.getTagUserList("Bearer " + accessToken,search);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<TagUserList>() {
            @Override
            public void onResponse(@NonNull Call<TagUserList> call, @NonNull Response<TagUserList> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<TagUserList> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void addKYCDetails(final Context ctx, final View view,Map<String,String> map) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<AddKYC> call1 = apiInterface.addKYCDetails("Bearer " + accessToken,map);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<AddKYC>() {
            @Override
            public void onResponse(@NonNull Call<AddKYC> call, @NonNull Response<AddKYC> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<AddKYC> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void addBankDetails(final Context ctx, final View view,Map<String,String> map) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<BankDetails> call1 = apiInterface.setbankDetails("Bearer " + accessToken,map);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<BankDetails>() {
            @Override
            public void onResponse(@NonNull Call<BankDetails> call, @NonNull Response<BankDetails> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<BankDetails> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void KYCDetails(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<KycDetails> call1 = apiInterface.KYCDetails("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<KycDetails>() {
            @Override
            public void onResponse(@NonNull Call<KycDetails> call, @NonNull Response<KycDetails> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<KycDetails> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void getbankdetails(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<GetBankDetails> call1 = apiInterface.getbankDetails("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<GetBankDetails>() {
            @Override
            public void onResponse(@NonNull Call<GetBankDetails> call, @NonNull Response<GetBankDetails> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<GetBankDetails> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void ButtonOne(final Context ctx, final View view,String contantfirst,String contantsecend) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ButtonModel> call1 = apiInterface.BUTTONONE("Bearer " + accessToken,contantfirst,contantsecend);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ButtonModel>() {
            @Override
            public void onResponse(@NonNull Call<ButtonModel> call, @NonNull Response<ButtonModel> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ButtonModel> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void ButtonTwo(final Context ctx, final View view,String contantfirst,String contantsecend) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ButtonModel> call1 = apiInterface.BUTTONTWO("Bearer " + accessToken,contantfirst,contantsecend);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ButtonModel>() {
            @Override
            public void onResponse(@NonNull Call<ButtonModel> call, @NonNull Response<ButtonModel> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ButtonModel> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getOnetooneChatList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<OnetooneChatModel> call1 = apiInterface.getOnetoOneChat("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<OnetooneChatModel>() {
            @Override
            public void onResponse(@NonNull Call<OnetooneChatModel> call, @NonNull Response<OnetooneChatModel> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<OnetooneChatModel> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
//                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getGroupChatList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<GroupChatModel> call1 = apiInterface.getGroupChat("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<GroupChatModel>() {
            @Override
            public void onResponse(@NonNull Call<GroupChatModel> call, @NonNull Response<GroupChatModel> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<GroupChatModel> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void submitCollab(final Context ctx, final View view,String projectuser_id,String project_id,String massage) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProjectRequestModel> call1 = apiInterface.submitCollab("Bearer " + accessToken,projectuser_id,project_id,massage);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ProjectRequestModel>() {
            @Override
            public void onResponse(@NonNull Call<ProjectRequestModel> call, @NonNull Response<ProjectRequestModel> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
//                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ProjectRequestModel> call, @NonNull Throwable t) {
                call.cancel();
//                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void checkProject(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CheckProjectModel> call1 = apiInterface.checkProject("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CheckProjectModel>() {
            @Override
            public void onResponse(@NonNull Call<CheckProjectModel> call, @NonNull Response<CheckProjectModel> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
//                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CheckProjectModel> call, @NonNull Throwable t) {
                call.cancel();
//                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getProductCategory(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProductCategory> call1 = apiInterface.getProductCategory("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ProductCategory>() {
            @Override
            public void onResponse(@NonNull Call<ProductCategory> call, @NonNull Response<ProductCategory> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ProductCategory> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getServiceCategory(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ServiceCategory> call1 = apiInterface.getServiceCategory("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ServiceCategory>() {
            @Override
            public void onResponse(@NonNull Call<ServiceCategory> call, @NonNull Response<ServiceCategory> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ServiceCategory> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void getTalentCat(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<TalantCat> call1 = apiInterface.getTalentCat("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<TalantCat>() {
            @Override
            public void onResponse(@NonNull Call<TalantCat> call, @NonNull Response<TalantCat> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<TalantCat> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }


}
