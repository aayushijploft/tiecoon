package com.twc.tiecoon.models;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;

import com.facebook.login.LoginManager;
import com.twc.tiecoon.Activities.LoginActivity;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CreateFeedback;
import com.twc.tiecoon.network.response.CreateProduct;
import com.twc.tiecoon.network.response.CreateReorganisation;
import com.twc.tiecoon.network.response.CreateService;
import com.twc.tiecoon.network.response.CreateTestimonial;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.network.response.EnquiryModel;
import com.twc.tiecoon.network.response.HomeUserDetail;
import com.twc.tiecoon.network.response.LikeUserList;
import com.twc.tiecoon.network.response.OtherProfileUserDetail;
import com.twc.tiecoon.network.response.OtherUserByIdDetail;
import com.twc.tiecoon.network.response.ProductCategory;
import com.twc.tiecoon.network.response.ProductView;
import com.twc.tiecoon.network.response.SaveTalent;
import com.twc.tiecoon.network.response.SavedBrands;
import com.twc.tiecoon.network.response.SavedProductsAndServices;
import com.twc.tiecoon.network.response.ServiceCategory;
import com.twc.tiecoon.network.response.ServiceView;
import com.twc.tiecoon.network.response.SetUserLikeUnlike;
import com.twc.tiecoon.network.response.SimilarProfile;
import com.twc.tiecoon.network.response.StoryView;
import com.twc.tiecoon.network.response.TransactionModel;
import com.twc.tiecoon.network.response.UserPdf;
import com.twc.tiecoon.network.response.ViewProduct;
import com.twc.tiecoon.network.response.ViewService;
import com.twc.tiecoon.utils.RetrofitCallback;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeModel extends BasicModel {
    public void getHomeUserData(final Context ctx, final View view, int id, FilterUserData filterUserData) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<OtherProfileUserDetail> call1 = apiInterface.getHomeUserData("Bearer " + accessToken, String.valueOf(id),
                filterUserData.getName(),filterUserData.getWorkType(),filterUserData.getProductCategory(),filterUserData.getProductLocation(),
                filterUserData.getProductCurrency(),filterUserData.getProductPrice(),filterUserData.getTalantName(),filterUserData.getGender(),
                filterUserData.getLanguage(),filterUserData.getFilterType(),filterUserData.getAge(),filterUserData.getTalantCurrency(),filterUserData.getTalantPrice(),
                filterUserData.getServiceName(),filterUserData.getServiceLocation(),filterUserData.getServiceDay(),filterUserData.getProjectName(),filterUserData.getProjectCurrency(),
                filterUserData.getProjectPrice(),filterUserData.getProjectgender());
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<OtherProfileUserDetail>() {
            @Override
            public void onResponse(@NonNull Call<OtherProfileUserDetail> call, @NonNull Response<OtherProfileUserDetail> response) {
             if(!response.body().getSuccess()){
                 ApplicationClass.appPreferences.SetToken("");
                 LoginManager.getInstance().logOut();
                 ctx.startActivity(new Intent(ctx, LoginActivity.class));
             }
             else {
                 if (response.body() != null && response.body().getSuccess()) {
                     if (response.body().getData() != null) {
                         notifyObservers(response.body());
                     }
                 }

             }

                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<OtherProfileUserDetail> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getCategoryCurrency(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CategoryCurrency> call1 = apiInterface.getCategoryCurrency("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CategoryCurrency>() {
            @Override
            public void onResponse(@NonNull Call<CategoryCurrency> call, @NonNull Response<CategoryCurrency> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CategoryCurrency> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getProductCategory(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProductCategory> call1 = apiInterface.getProductCategory("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ProductCategory>() {
            @Override
            public void onResponse(@NonNull Call<ProductCategory> call, @NonNull Response<ProductCategory> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ProductCategory> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getServiceCategory(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ServiceCategory> call1 = apiInterface.getServiceCategory("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ServiceCategory>() {
            @Override
            public void onResponse(@NonNull Call<ServiceCategory> call, @NonNull Response<ServiceCategory> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ServiceCategory> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void getServiceSubCategory(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ServiceCategory> call1 = apiInterface.getServiceSubCategory("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ServiceCategory>() {
            @Override
            public void onResponse(@NonNull Call<ServiceCategory> call, @NonNull Response<ServiceCategory> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ServiceCategory> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getProductSubCategory(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProductCategory> call1 = apiInterface.getProductSubCategory("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ProductCategory>() {
            @Override
            public void onResponse(@NonNull Call<ProductCategory> call, @NonNull Response<ProductCategory> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ProductCategory> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getMyProfileDetails(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<HomeUserDetail> call1 = apiInterface.getMyProfileDetails("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<HomeUserDetail>() {
            @Override
            public void onResponse(@NonNull Call<HomeUserDetail> call, @NonNull Response<HomeUserDetail> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        //Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    //Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<HomeUserDetail> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void deleteTalents(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteTalents("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deletesingleTalent");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getSpecialUsersData(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<OtherProfileUserDetail> call1 = apiInterface.getSpecialUserData("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<OtherProfileUserDetail>() {
            @Override
            public void onResponse(@NonNull Call<OtherProfileUserDetail> call, @NonNull Response<OtherProfileUserDetail> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    }
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<OtherProfileUserDetail> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getOtherUserDetailsById(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<OtherUserByIdDetail> call1 = apiInterface.otherUserProfileById("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<OtherUserByIdDetail>() {
            @Override
            public void onResponse(@NonNull Call<OtherUserByIdDetail> call, @NonNull Response<OtherUserByIdDetail> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<OtherUserByIdDetail> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void setUserLike(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SetUserLikeUnlike> call1 = apiInterface.setLike("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<SetUserLikeUnlike>() {
            @Override
            public void onResponse(@NonNull Call<SetUserLikeUnlike> call, @NonNull Response<SetUserLikeUnlike> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        // Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<SetUserLikeUnlike> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void createFeedback(final Context ctx, final View view, int talant_id, String rating, String massage) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CreateFeedback> call1 = apiInterface.createFeedback("Bearer " + accessToken, String.valueOf(talant_id), rating, massage);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CreateFeedback>() {
            @Override
            public void onResponse(@NonNull Call<CreateFeedback> call, @NonNull Response<CreateFeedback> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CreateFeedback> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void createTestimonial(final Context ctx, final View view, int product_id, String rating, String massage) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CreateTestimonial> call1 = apiInterface.createTestimonial("Bearer " + accessToken, String.valueOf(product_id), rating, massage);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CreateTestimonial>() {
            @Override
            public void onResponse(@NonNull Call<CreateTestimonial> call, @NonNull Response<CreateTestimonial> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CreateTestimonial> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void createReorganisation(final Context ctx, final View view, int brand_id, String rating, String massage) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CreateReorganisation> call1 = apiInterface.createReorganisation("Bearer " + accessToken, String.valueOf(brand_id), rating, massage);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CreateReorganisation>() {
            @Override
            public void onResponse(@NonNull Call<CreateReorganisation> call, @NonNull Response<CreateReorganisation> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CreateReorganisation> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getlikeUserData(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<LikeUserList> call1 = apiInterface.getLikeUserData("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<LikeUserList>() {
            @Override
            public void onResponse(@NonNull Call<LikeUserList> call, @NonNull Response<LikeUserList> response) {
                if (response.body() != null && response.body().getSuccess()) {
                        notifyObservers(response.body());
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<LikeUserList> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void deleteAssignment(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteAssignment("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("assignmentDeleted");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void deleteBrands(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteBrands("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deleteBrands");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }


    public void saveProduct(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.saveProduct("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void saveService(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.saveService("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void saveTalents(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.saveTalent("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void deletTalent(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deletTalent("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deleted");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void addNote(final Context ctx, final View view, String notes) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.addNotes("Bearer " + accessToken,notes);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("Notesadded");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void addVideo(final Context ctx, final View view, int notes, int talant_user) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.VideoView("Bearer " + accessToken,notes,talant_user);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }

            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();

                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void saveBrands(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.saveBrands("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("brandLiked");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void deleteProduct(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteProduct("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deleteProduct");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void deleteServices(final Context ctx, final View view, int id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteService("Bearer " + accessToken, String.valueOf(id));
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deleteServices");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getSavedProductsAndServicesList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SavedProductsAndServices> call1 = apiInterface.getSavedProductsAndServiceList("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<SavedProductsAndServices>() {
            @Override
            public void onResponse(@NonNull Call<SavedProductsAndServices> call, @NonNull Response<SavedProductsAndServices> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<SavedProductsAndServices> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getSavedBrandsList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SavedBrands> call1 = apiInterface.getSavedBrandsList("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<SavedBrands>() {
            @Override
            public void onResponse(@NonNull Call<SavedBrands> call, @NonNull Response<SavedBrands> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    notifyObservers(response.body());
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<SavedBrands> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getSavedTalentsList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SaveTalent> call1 = apiInterface.getSaveTalent("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<SaveTalent>() {
            @Override
            public void onResponse(@NonNull Call<SaveTalent> call, @NonNull Response<SaveTalent> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    notifyObservers(response.body());
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<SaveTalent> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void viewStory(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<StoryView> call1 = apiInterface.viewStory("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<StoryView>() {
            @Override
            public void onResponse(@NonNull Call<StoryView> call, @NonNull Response<StoryView> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<StoryView> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getSimilarProfiles(final Context ctx, final View view,String userid) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SimilarProfile> call1 = apiInterface.getSimilarProfiles("Bearer " + accessToken,userid);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<SimilarProfile>() {
            @Override
            public void onResponse(@NonNull Call<SimilarProfile> call, @NonNull Response<SimilarProfile> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<SimilarProfile> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void createProduct(final Context ctx, final View view, MultipartBody.Part[] partList, MultipartBody.Part video, MultipartBody.Part videothumbnail, String category_id,
                              String subcategory_id, String currency, String amount,
                              String qty, String outofstock,
                              String description, ArrayList<String> list,
                              String location , String name, String phone, String pincode, String address
            , String town, String city, String state) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody Requestcategory_id = RequestBody.create(MediaType.parse("text/plain"), category_id);
        RequestBody Requestsubcategory_id = RequestBody.create(MediaType.parse("text/plain"), subcategory_id);
        RequestBody Requestcurrency = RequestBody.create(MediaType.parse("text/plain"), currency);
        RequestBody Requestamount = RequestBody.create(MediaType.parse("text/plain"), amount);
        RequestBody Requestqty = RequestBody.create(MediaType.parse("text/plain"), qty);
        RequestBody Requestoutofstock = RequestBody.create(MediaType.parse("text/plain"), outofstock);
        RequestBody Requestdescription = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody Requestlocation = RequestBody.create(MediaType.parse("text/plain"), location);
        RequestBody Requestname = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody Requestphone = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody Requestpincode = RequestBody.create(MediaType.parse("text/plain"), pincode);
        RequestBody Requestaddress = RequestBody.create(MediaType.parse("text/plain"), address);
        RequestBody Requesttown = RequestBody.create(MediaType.parse("text/plain"), town);
        RequestBody Requestcity = RequestBody.create(MediaType.parse("text/plain"), city);
        RequestBody Requeststate = RequestBody.create(MediaType.parse("text/plain"), state);
//        RequestBody Requestlocation = RequestBody.create(MediaType.parse("text/plain"), location);
        Call<CreateProduct> call1 = apiInterface.createProduct("Bearer " + accessToken,partList,video,videothumbnail,Requestcategory_id,Requestsubcategory_id,
                Requestcurrency,Requestamount,Requestqty,Requestoutofstock,Requestdescription,list,Requestlocation,
                Requestname,Requestphone,Requestpincode,Requestaddress,Requesttown,Requestcity,Requeststate);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CreateProduct>() {
            @Override
            public void onResponse(@NonNull Call<CreateProduct> call, @NonNull Response<CreateProduct> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CreateProduct> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void EditProduct(final Context ctx, final View view, MultipartBody.Part[] partList, MultipartBody.Part video, MultipartBody.Part videothumbnail, String product_id, String category_id,
                              String subcategory_id, String currency, String amount, String qty,
                            String outofstock, String description, ArrayList<String> list,
                            String location , String name, String phone, String pincode, String address
            , String town, String city, String state) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody Requestcategory_id = RequestBody.create(MediaType.parse("text/plain"), category_id);
        RequestBody Requestproduct_id = RequestBody.create(MediaType.parse("text/plain"), product_id);
        RequestBody Requestsubcategory_id = RequestBody.create(MediaType.parse("text/plain"), subcategory_id);
        RequestBody Requestcurrency = RequestBody.create(MediaType.parse("text/plain"), currency);
        RequestBody Requestamount = RequestBody.create(MediaType.parse("text/plain"), amount);
        RequestBody Requestqty = RequestBody.create(MediaType.parse("text/plain"), qty);
        RequestBody Requestoutofstock = RequestBody.create(MediaType.parse("text/plain"), outofstock);
        RequestBody Requestdescription = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody Requestlocation = RequestBody.create(MediaType.parse("text/plain"), location);
        RequestBody Requestname = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody Requestphone = RequestBody.create(MediaType.parse("text/plain"), phone);
        RequestBody Requestpincode = RequestBody.create(MediaType.parse("text/plain"), pincode);
        RequestBody Requestaddress = RequestBody.create(MediaType.parse("text/plain"), address);
        RequestBody Requesttown = RequestBody.create(MediaType.parse("text/plain"), town);
        RequestBody Requestcity = RequestBody.create(MediaType.parse("text/plain"), city);
        RequestBody Requeststate = RequestBody.create(MediaType.parse("text/plain"), state);
        Call<CreateProduct> call1 = apiInterface.EditProduct("Bearer " + accessToken,partList,video,videothumbnail,Requestproduct_id,Requestcategory_id,Requestsubcategory_id,
                Requestcurrency,Requestamount,Requestqty,Requestoutofstock,Requestdescription,list,Requestlocation,
                Requestname,Requestphone,Requestpincode,Requestaddress,Requesttown,Requestcity,Requeststate);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CreateProduct>() {
            @Override
            public void onResponse(@NonNull Call<CreateProduct> call, @NonNull Response<CreateProduct> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CreateProduct> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void createService(final Context ctx, final View view, MultipartBody.Part[] partList, MultipartBody.Part video
            , MultipartBody.Part videothumbnail, MultipartBody.Part audio, String category_id, String text, String color,
                              String subcategory_id, String currency, String amount,
                              String description, ArrayList<String> list, String location, String day) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody Requestcategory_id = RequestBody.create(MediaType.parse("text/plain"), category_id);
        RequestBody Requesttext = RequestBody.create(MediaType.parse("text/plain"), text);
        RequestBody Requestcolor = RequestBody.create(MediaType.parse("text/plain"), color);
        RequestBody Requestsubcategory_id = RequestBody.create(MediaType.parse("text/plain"), subcategory_id);
        RequestBody Requestcurrency = RequestBody.create(MediaType.parse("text/plain"), currency);
        RequestBody Requestamount = RequestBody.create(MediaType.parse("text/plain"), amount);
        RequestBody Requestdescription = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody Requestlocation = RequestBody.create(MediaType.parse("text/plain"), location);
        RequestBody Requestday = RequestBody.create(MediaType.parse("text/plain"), day);
        Call<CreateService> call1 = apiInterface.createService("Bearer " + accessToken,partList,video,videothumbnail,audio,Requesttext,
                Requestcolor,Requestcategory_id,Requestsubcategory_id,
                Requestcurrency,Requestamount,Requestdescription,list,Requestlocation,Requestday);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CreateService>() {
            @Override
            public void onResponse(@NonNull Call<CreateService> call, @NonNull Response<CreateService> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CreateService> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void EditService(final Context ctx, final View view, MultipartBody.Part[] partList, MultipartBody.Part video
            , MultipartBody.Part videothumbnail, MultipartBody.Part audio, String category_id, String text, String color, String service_id,
                              String subcategory_id, String currency, String amount,
                              String description, ArrayList<String> list, String location, String day) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        RequestBody Requestcategory_id = RequestBody.create(MediaType.parse("text/plain"), category_id);
        RequestBody Requesttext = RequestBody.create(MediaType.parse("text/plain"), text);
        RequestBody Requestcolor = RequestBody.create(MediaType.parse("text/plain"), color);
        RequestBody Requestservice_id = RequestBody.create(MediaType.parse("text/plain"), service_id);
        RequestBody Requestsubcategory_id = RequestBody.create(MediaType.parse("text/plain"), subcategory_id);
        RequestBody Requestcurrency = RequestBody.create(MediaType.parse("text/plain"), currency);
        RequestBody Requestamount = RequestBody.create(MediaType.parse("text/plain"), amount);
        RequestBody Requestdescription = RequestBody.create(MediaType.parse("text/plain"), description);
        RequestBody Requestlocation = RequestBody.create(MediaType.parse("text/plain"), location);
        RequestBody Requestday = RequestBody.create(MediaType.parse("text/plain"), day);
        Call<CreateService> call1 = apiInterface.EditService("Bearer " + accessToken,partList,video,videothumbnail,audio,Requesttext,
                Requestcolor,Requestservice_id,Requestcategory_id,Requestsubcategory_id,
                Requestcurrency,Requestamount,Requestdescription,list,Requestlocation,Requestday);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<CreateService>() {
            @Override
            public void onResponse(@NonNull Call<CreateService> call, @NonNull Response<CreateService> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body().getData());
                    } else {
                        //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    //  Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<CreateService> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void deleteprodcat(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteProductCat("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deletePcat");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void deleteservicecat(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call1 = apiInterface.deleteServicecat("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<DeleteTalents>() {
            @Override
            public void onResponse(@NonNull Call<DeleteTalents> call, @NonNull Response<DeleteTalents> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers("deleteScat");
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<DeleteTalents> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void viewProductDetails(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ViewProduct> call1 = apiInterface.productDetails("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ViewProduct>() {
            @Override
            public void onResponse(@NonNull Call<ViewProduct> call, @NonNull Response<ViewProduct> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ViewProduct> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void viewServiceDetails(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ViewService> call1 = apiInterface.serviceDetails("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ViewService>() {
            @Override
            public void onResponse(@NonNull Call<ViewService> call, @NonNull Response<ViewService> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ViewService> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void viewServiceVideo(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ServiceView> call1 = apiInterface.ServiceView("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ServiceView>() {
            @Override
            public void onResponse(@NonNull Call<ServiceView> call, @NonNull Response<ServiceView> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ServiceView> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void viewProductVideo(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProductView> call1 = apiInterface.ProductView("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<ProductView>() {
            @Override
            public void onResponse(@NonNull Call<ProductView> call, @NonNull Response<ProductView> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<ProductView> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void downloadUserPDF(final Context ctx, final View view,String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<UserPdf> call1 = apiInterface.downloadUserPDF("Bearer " + accessToken,id);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<UserPdf>() {
            @Override
            public void onResponse(@NonNull Call<UserPdf> call, @NonNull Response<UserPdf> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<UserPdf> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

    public void getEnquiryList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<EnquiryModel> call1 = apiInterface.getEnquiryList("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<EnquiryModel>() {
            @Override
            public void onResponse(@NonNull Call<EnquiryModel> call, @NonNull Response<EnquiryModel> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<EnquiryModel> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void getTalentEnquiryList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<EnquiryModel> call1 = apiInterface.getTalentEnquiryList("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<EnquiryModel>() {
            @Override
            public void onResponse(@NonNull Call<EnquiryModel> call, @NonNull Response<EnquiryModel> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<EnquiryModel> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void getServiceEnquiryList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<EnquiryModel> call1 = apiInterface.getServiceEnquiryList("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<EnquiryModel>() {
            @Override
            public void onResponse(@NonNull Call<EnquiryModel> call, @NonNull Response<EnquiryModel> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<EnquiryModel> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }
    public void getTransactionList(final Context ctx, final View view) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<TransactionModel> call1 = apiInterface.getTransactionList("Bearer " + accessToken);
        call1.enqueue(new RetrofitCallback<>(ctx, new Callback<TransactionModel>() {
            @Override
            public void onResponse(@NonNull Call<TransactionModel> call, @NonNull Response<TransactionModel> response) {
                if (response.body() != null && response.body().getSuccess()) {
                    if (response.body().getData() != null) {
                        notifyObservers(response.body());
                    } else {
                        Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                    }
                } else {
                    Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
                }
                Utils.dismissProDialog();
            }

            @Override
            public void onFailure(@NonNull Call<TransactionModel> call, @NonNull Throwable t) {
                call.cancel();
                Utils.dismissProDialog();
                Utils.warningSnackBar(ctx, view, ctx.getString(R.string.api_failure));
            }
        }));
    }

}
