package com.twc.tiecoon.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.Adapters.StorySliderAdapter;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddStory;
import com.twc.tiecoon.network.response.DeleteStory;
import com.twc.tiecoon.network.response.StoryView;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.Observable;

import de.hdodenhof.circleimageview.CircleImageView;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class ViewStoryActivity extends AbstractFragmentActivity {

    private ProfileModel profileModel = new ProfileModel();
    MaterialProgressBar progressBar;
    LinearLayout canclelayout;
    ArrayList<StoryView.Stores> storyViews = new ArrayList<>();
    CountDownTimer countDownTimer;
    RecyclerView rvStory;
    StorySliderAdapter storySliderAdapter;
    RelativeLayout layoutButtons;
    ImageView ivPreviousButton, ivNextButton, image;
    CircleImageView imgProfile;
    int counter = 0;
    //Declare a variable to hold count down timer's paused status
    private boolean isPaused = false;
    //Declare a variable to hold count down timer's paused status
    private boolean isCanceled = false;
    long s1;
    //Declare a variable to hold CountDownTimer remaining time
    private long timeRemaining = 0;
    String profileimage;
    String username;
    TextView tvName;
    ImageView iv_menu;
    int position;
    RelativeLayout relative_parent;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_view_story);
        storyViews = getIntent().getParcelableArrayListExtra("storyarray");
        profileimage = getIntent().getStringExtra("profileimage");
        username = getIntent().getStringExtra("username");
        position = getIntent().getIntExtra("position",0);
        image = findViewById(R.id.image);
        progressBar = findViewById(R.id.progress);
        rvStory = findViewById(R.id.rvStory);
        layoutButtons = findViewById(R.id.layoutButtons);
        ivNextButton = findViewById(R.id.ivNextButton);
        ivPreviousButton = findViewById(R.id.ivPreviousButton);
        imgProfile = findViewById(R.id.imgProfile);
        tvName = findViewById(R.id.tvName);
        iv_menu = findViewById(R.id.iv_menu);
        relative_parent = findViewById(R.id.relative_parent);
        if(position!=0){
            iv_menu.setVisibility(View.GONE);
        }
        else iv_menu.setVisibility(View.VISIBLE);

        iv_menu.setOnClickListener(v -> {
            //Creating the instance of PopupMenu
            PopupMenu popup = new PopupMenu(ViewStoryActivity.this, iv_menu);
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                   deletStory(storyViews.get(counter).getId()+"");
                    return true;
                }
            });

            popup.show();//showing popup menu

        });

        canclelayout = findViewById(R.id.canclelayout);
        canclelayout.setOnClickListener(v -> {
            countDownTimer.cancel();
            onBackPressed();
        });

        Glide.with(this).load(storyViews.get(0).getImage()).error(R.drawable.dummy_place)
                .error(R.drawable.dummy_place)
                .into(image);
        Glide.with(this).load(profileimage).error(R.drawable.dummy_place)
                .error(R.drawable.dummy_place)
                .into(imgProfile);

        tvName.setText(username);
        ivNextButton.setOnClickListener(view ->
        {
            int size = storyViews.size();
            if (counter < size-1) {
                counter++;
                countDownTimer.start();
                Glide.with(this).load(storyViews.get(counter).getImage()).error(R.drawable.dummy_place)
                        .error(R.drawable.dummy_place)
                        .into(image);
            }
        });
        ivPreviousButton.setOnClickListener(view ->
        {
            if (counter > 0) {
                counter--;
                countDownTimer.start();
                Glide.with(this).load(storyViews.get(counter).getImage()).error(R.drawable.dummy_place)
                        .error(R.drawable.dummy_place)
                        .into(image);
            }
        });
        rvStory = findViewById(R.id.rvStory);
        if (storyViews.size() > 1) {
            layoutButtons.setVisibility(View.VISIBLE);
        }
        else layoutButtons.setVisibility(View.GONE);
        rvStory.setHasFixedSize(true);
        rvStory.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        storySliderAdapter = new StorySliderAdapter(this, storyViews);
        rvStory.setAdapter(storySliderAdapter);
        countDownTimer = new CountDownTimer(10000, 1000) {
            @SuppressLint("NewApi")
            public void onTick(long millisUntilFinished) {
                if(isPaused || isCanceled)
                {
                    //If the user request to cancel or paused the
                    //CountDownTimer we will cancel the current instance
                    cancel();
                }
                else {
                    Log.e("seconds", millisUntilFinished / 1000 + "");
                    progressBar.setMax(10);
                    progressBar.setProgress(Math.toIntExact(millisUntilFinished / 1000));
                    timeRemaining = millisUntilFinished;
                }

            }

            public void onFinish() {
                int size = storyViews.size();
                if (counter < size-1) {
                    countDownTimer.start();
                    counter++;
                    Glide.with(ViewStoryActivity.this).load(storyViews.get(counter).getImage()).error(R.drawable.dummy_place)
                            .error(R.drawable.dummy_place)
                            .into(image);
                }else{
                    finish();
                    countDownTimer.cancel();
                }
            }
        }.start();

    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }
    private void deletStory(String id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deletStory(this, relative_parent,id);
        } else {
            Utils.warningSnackBar(this, relative_parent, getString(R.string.no_internet));
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        countDownTimer.cancel();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof DeleteStory) {
            Toast.makeText(this, "Story Deleted Successfully", Toast.LENGTH_SHORT).show();
            DeleteStory deleteStory = (DeleteStory) arg;
            startActivity(new Intent(getApplicationContext(),UserHomeActivity.class));
        }
    }
}