package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.RequestServiceModel;
import com.twc.tiecoon.network.response.ServiceEndModel;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceRequestMoneyActivity extends AppCompatActivity {

    String userid,serviceid,ServiceIdGen,amount,username;
    TextView tvtitle,tvtitlemsg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_request_money);
        userid = getIntent().getStringExtra("userid");
        serviceid = getIntent().getStringExtra("serviceid");
        ServiceIdGen = getIntent().getStringExtra("ServiceIdGen");
        amount = getIntent().getStringExtra("amount");
        username = getIntent().getStringExtra("username");

        tvtitle = findViewById(R.id.tvtitle);
        tvtitlemsg = findViewById(R.id.tvtitlemsg);

        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());

        tvtitle.setText("Service (S"+ServiceIdGen+")");

        tvtitlemsg.setText(username+"  has deposited Rs. "+amount+" for this servcie and booked it.");

        findViewById(R.id.tvrequestMoney).setOnClickListener(v -> {
            showEndServiceDialog();
        });


    }

    private void showEndServiceDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to send request for money?");
        btnRemove.setText("SEND");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            requestMoneyService();
        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    public void requestMoneyService() {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<RequestServiceModel> call = apiInterface.serviceRequest("Bearer "+accessToken,serviceid,userid,amount);

        call.enqueue(new Callback<RequestServiceModel>() {
            @Override
            public void onResponse(Call<RequestServiceModel> call, Response<RequestServiceModel> response) {
                RequestServiceModel user = response.body();
                Log.e("user",user.toString());
                Toast.makeText(ServiceRequestMoneyActivity.this, user.message, Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onFailure(Call<RequestServiceModel> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


}