package com.twc.tiecoon.Activities.PaymentModule;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;
import com.twc.tiecoon.Activities.UserHomeActivity;
import com.twc.tiecoon.R;

import org.json.JSONException;
import org.json.JSONObject;

public class RazorPayActivity extends AppCompatActivity  implements PaymentResultListener {

    LinearLayout layoutFail,layoutSuccess;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_razor_pay);
        layoutSuccess = findViewById(R.id.layoutSuccess);
        layoutFail = findViewById(R.id.layoutFail);

        findViewById(R.id.btnOK).setOnClickListener(v -> startActivity(new Intent(this, UserHomeActivity.class)));
        findViewById(R.id.btnTry).setOnClickListener(v -> startActivity(new Intent(this, RazorPayActivity.class)));

        startPayment();

    }

    public void startPayment(){
        // amount that is entered by user.
        String samount = "100";

        // rounding off the amount.
        int amount = Math.round(Float.parseFloat(samount) * 100);

        // initialize Razorpay account.
        Checkout checkout = new Checkout();

        // set your id as below
        checkout.setKeyID("rzp_test_gmofpkxwGdC0zG");

        // set image
        checkout.setImage(R.drawable.newsplash);

        // initialize json object
        JSONObject object = new JSONObject();
        try {
            // to put name
            object.put("name", "Tiecoon");

            // put description
            object.put("description", "Test payment");

            // to set theme color
            object.put("theme.color", "#191938");

            // put the currency
            object.put("currency", "INR");

            // put amount
            object.put("amount", amount);

            // put mobile number
            object.put("prefill.contact", "9653908417");

            // put email
            object.put("prefill.email", "aayushiron02@gmail.com");

            // open razorpay to checkout activity
            checkout.open(RazorPayActivity.this, object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String s) {

        layoutSuccess.setVisibility(View.VISIBLE);
        layoutFail.setVisibility(View.GONE);

    }

    @Override
    public void onPaymentError(int i, String s) {
        layoutSuccess.setVisibility(View.GONE);
        layoutFail.setVisibility(View.VISIBLE);
    }
}