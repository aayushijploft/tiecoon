package com.twc.tiecoon.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.LoginModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddLogginDeviceModel;
import com.twc.tiecoon.network.response.OTPVerificationData;
import com.twc.tiecoon.network.response.OTPVerificationDataUser;
import com.twc.tiecoon.network.response.RefundList;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;
import com.twc.tiecoon.utils.SmsBroadcastReceiver;
import com.twc.tiecoon.utils.Utils;

import java.util.Observable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpActivity extends AbstractFragmentActivity implements View.OnClickListener {

    private OtpTextView otp_view;
    private RelativeLayout relativeParent;
    String phone="", otp,email="",type;
    private final LoginModel loginModel = new LoginModel();
    private static final int REQ_USER_CONSENT = 200;
    SmsBroadcastReceiver smsBroadcastReceiver;
    private final String[] LOCATION_PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    private final int LOCATION_PERMISSIONS_REQUEST = 701;
    private boolean currentLocationPermissionDeniedWithDontAskMeAgain = false;
    String devicelocation = "";

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_otp);
        if(getIntent().hasExtra("phone")){
            phone = getIntent().getStringExtra("phone");

        }
        if(getIntent().hasExtra("email")){
            email = getIntent().getStringExtra("email");
        }

        type = getIntent().getStringExtra("type");
        otp = getIntent().getStringExtra("otp");
        initView();
//        otp_view.setOTP(otp);
        checkPermissionForCurrentLocation();

        findViewById(R.id.tvResendOTP).setOnClickListener(v -> {
            if (Utils.isDeviceOnline(this)) {
                Utils.showProDialog(this);
                loginModel.Resend_Otp(this, relativeParent, phone);
            } else {
                Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
            }
        });

    }

    @Override
    protected BasicModel getModel() {
        return loginModel;
    }

    private void initView() {
        relativeParent = findViewById(R.id.relativeParent);
        otp_view = findViewById(R.id.otp_view);
        TextView tvConfirm = findViewById(R.id.tvConfirm);
        tvConfirm.setOnClickListener(this);
        startSmsUserConsent();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.tvConfirm) {
//            if (otp_view.getOTP().equals(otp)) {
            if(phone.equals("")){
                verifyEmailOTP(otp_view.getOTP());
            }
             else   verifyOTP(otp_view.getOTP());
//                prepareUser();
//            } else Toast.makeText(this, "Invalid OTP", Toast.LENGTH_SHORT).show();
        }
    }

    private void verifyOTP(String otp) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            loginModel.otpVerify(this, relativeParent,type, phone, otp);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    private void verifyEmailOTP(String otp) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            loginModel.EmailotpVerify(this, relativeParent,type, email, otp);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof OTPVerificationData) {
            prepareUser();
            OTPVerificationData otpVerificationData = (OTPVerificationData) o;
            ApplicationClass.appPreferences.SetToken(otpVerificationData.getToken());
            ApplicationClass.appPreferences.setPhoneNumber(phone);
            ApplicationClass.appPreferences.setFirstTime(Boolean.parseBoolean(otpVerificationData.getProfileFirstStatus()));
            OTPVerificationDataUser otpVerificationDataUser = otpVerificationData.getUser();
            if (otpVerificationDataUser != null) {
                if (otpVerificationDataUser.getEmail() != null) {
                    ApplicationClass.appPreferences.setEmailId(otpVerificationDataUser.getEmail());
                }
                if(otpVerificationDataUser.getProfile()!=null)
                {
                    if(otpVerificationDataUser.getPath()!=null)
                    {
                        String imageURL = Constant.ImageURL + otpVerificationDataUser.getPath() + "/" + otpVerificationDataUser.getProfile();
                        ApplicationClass.appPreferences.setUserPicUrl(imageURL);
                    }else
                    {
                        ApplicationClass.appPreferences.setUserPicUrl(otpVerificationDataUser.getProfile());
                    }
                }
                if (otpVerificationDataUser.getUserDetils() != null) {
                    if (otpVerificationDataUser.getUserDetils().getName() != null) {
                        ApplicationClass.appPreferences.setUserName(otpVerificationDataUser.getUserDetils().getName());
                    }
                    if (otpVerificationDataUser.getUserDetils().getUserId()!= null) {
                        ApplicationClass.appPreferences.setUserId(otpVerificationDataUser.getUserDetils().getUserId());
                    }
                }
            }
            if (Boolean.parseBoolean(otpVerificationData.getProfileFirstStatus())) {
//                loginToChat(AppPreferences.getInstance().getQbUser());
              //  Toast.makeText(this, "prepare user", Toast.LENGTH_SHORT).show();

                addlogindevice();
                Intent intent = new Intent(this, UserHomeActivity.class);
                intent.putExtra("user","second");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
//                addlogindevice();
                Intent intent = new Intent(this, ProfileFirst_Activity.class);
                intent.putExtra("first", true);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }
    }

    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
//We can add sender phone number or leave it blank
// I'm adding null here
        client.startSmsUserConsent(null).addOnSuccessListener(aVoid -> {
//                Toast.makeText(getApplicationContext(), "On Success", Toast.LENGTH_LONG).show();
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(getApplicationContext(), "On OnFailure", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
//That gives all message to us.
// We need to get the code from inside with regex
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
/* textViewMessage.setText(
String.format("%s - %s", getString(R.string.received_message), message));
*/
                getOtpFromMessage(message);
            }
        }
    }

    private void getOtpFromMessage(String message) {
// This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{4}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            Log.d("sdfg",matcher.group(0)+"");
            int num = Integer.parseInt(matcher.group(0)+"");
            String number = String.valueOf(num);
            otp_view.setOTP(number);
//            for(int i = 0; i < number.length(); i++) {
//                int j = Character.digit(number.charAt(i), 10);
//
//                System.out.println("digit: " + j);
//                if (i == 0) {
//                    Log.d("num1", j + "");
//                    et1.setText(j+"");
//                }
//                if (i == 1) {
//                    Log.d("num2", j + "");
//                    et2.setText(j+"");
//                }
//                if (i == 2) {
//                    Log.d("num3", j + "");
//                    et3.setText(j+"");
//                }
//                if (i == 3) {
//                    Log.d("num4", j + "");
//                    et4.setText(j+"");
//                }
//            }
        }
    }
    private void registerBroadcastReceiver() {
        smsBroadcastReceiver = new SmsBroadcastReceiver();
        smsBroadcastReceiver.smsBroadcastReceiverListener =
                new SmsBroadcastReceiver.SmsBroadcastReceiverListener() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, REQ_USER_CONSENT);
                    }

                    @Override
                    public void onFailure() {

                    }
                };
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerBroadcastReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(smsBroadcastReceiver);
    }


//    private void loginToChat(final QBUser user) {
//        //Need to set password, because the server will not register to chat without password
//        user.setPassword(ApplicationClass.USER_DEFAULT_PASSWORD);
//        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
//            @Override
//            public void onSuccess(Void aVoid, Bundle bundle) {
//                AppPreferences.getInstance().saveQbUser(user);
////                if (!chbSave.isChecked()) {
////                    clearDrafts();
////                }
////                DialogsActivity.start(ProfileFirst_Activity.this);
//                finish();
////                hideProgressDialog();
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
////                hideProgressDialog();
////                showErrorSnackbar(R.string.login_chat_login_error, e, null);
//            }
//        });
//    }
//    private void prepareUser() {
//        QBUser qbUser = new QBUser();
//        qbUser.setLogin(phone);
//        // qbUser.setFullName(usernameEt.getText().toString().trim());
//        qbUser.setPassword(phone);
//        //qbUser.setPassword(App.USER_DEFAULT_PASSWORD);
//        signIn(qbUser);
//    }
//    private void signIn(final QBUser user) {
//
//        ChatHelper.getInstance().login(user, new QBEntityCallback<QBUser>() {
//            @Override
//            public void onSuccess(QBUser userFromRest, Bundle bundle) {
//                if (userFromRest.getId().equals(user.getId())) {
//                    loginToChat(user);
//                } else {
//                    //Need to set password NULL, because server will update user only with NULL password
//                    user.setPassword(null);
//                    updateUser(user);
//                }
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//
//            }
//        });
//    }
//    private void updateUser(final QBUser user) {
//        ChatHelper.getInstance().updateUser(user, new QBEntityCallback<QBUser>() {
//            @Override
//            public void onSuccess(QBUser user, Bundle bundle) {
//                loginToChat(user);
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//
//            }
//        });
//    }

private void prepareUser() {
    QBUser qbUser = new QBUser();
    qbUser.setLogin(phone);
//         qbUser.setFullName(usernameEt.getText().toString().trim());
    qbUser.setPassword(phone);
    //qbUser.setPassword(App.USER_DEFAULT_PASSWORD);
    signIn(qbUser);
}

    private void signIn(final QBUser user) {
        ChatHelper.getInstance().login(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser userFromRest, Bundle bundle) {
                if (userFromRest.getId().equals(user.getId())) {
                    loginToChat(user);
                    Log.d("user",user.toString()+"@!#$%");
//                    Toast.makeText(OtpActivity.this, "login", Toast.LENGTH_SHORT).show();
                } else {
                    //Need to set password NULL, because server will update user only with NULL password
                    user.setPassword(null);
                    Toast.makeText(OtpActivity.this, "register", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    private void loginToChat(final QBUser user) {
        //Need to set password, because the server will not register to chat without password
        user.setPassword(phone);
        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                AppPreferences.getInstance().saveQbUser(user);
                Log.e("OTPSESSION",user.toString());
                finish();
//                hideProgressDialog();
            }

            @Override
            public void onError(QBResponseException e) {
//                hideProgressDialog();
//                showErrorSnackbar(R.string.login_chat_login_error, e, null);
            }
        });
    }

    public void addlogindevice() {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        String device_name = android.os.Build.MODEL;
        Call<AddLogginDeviceModel> call = apiInterface.AddlogginDevices("Bearer "+accessToken,device_name,devicelocation);

        call.enqueue(new Callback<AddLogginDeviceModel>() {
            @Override
            public void onResponse(Call<AddLogginDeviceModel> call, Response<AddLogginDeviceModel> response) {
                AddLogginDeviceModel user = response.body();
                Log.e("user",user.toString());

            }

            @Override
            public void onFailure(Call<AddLogginDeviceModel> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


    private void checkPermissionForCurrentLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, LOCATION_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs location permission for current location.",
                            LOCATION_PERMISSIONS, LOCATION_PERMISSIONS_REQUEST);
                } else if (currentLocationPermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs location permission for current location.",
                            LOCATION_PERMISSIONS_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, LOCATION_PERMISSIONS, LOCATION_PERMISSIONS_REQUEST);
                }
            } else {
                if (!(Utils.isGPSEnabled(this))) {
                    Utils.enableGPS(this);
                } else {
                    setUserCurrentLocation();
                }
            }
        } else {
            if (!(Utils.isGPSEnabled(this))) {
                Utils.enableGPS(this);
            } else {
                setUserCurrentLocation();
            }

        }
    }
    @SuppressLint("SetTextI18n")
    private void setUserCurrentLocation() {
        Location location = Utils.getLocation(this);
        if (location != null) {
            Address address = Utils.getAddressUsingLatLang(this, location.getLatitude(), location.getLongitude());
            if (address != null) {
                devicelocation = address.getLocality() + "," + address.getCountryName();
                Log.e("location",devicelocation);
                Toast.makeText(this, devicelocation, Toast.LENGTH_SHORT).show();
            }
        } else {
            fetchLocation();
        }
    }

    private void fetchLocation() {
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    Address address = Utils.getAddressUsingLatLang(OtpActivity.this, location.getLatitude(), location.getLongitude());

                    if (address != null) {
                        devicelocation = address.getLocality() + "," + address.getCountryName();
                        Log.e("location",devicelocation);
                        Toast.makeText(OtpActivity.this, devicelocation, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

}