package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Activities.views.RemoveUser;
import com.twc.tiecoon.Adapters.AgeGroupAdapter;
import com.twc.tiecoon.Adapters.GenderAdapter;
import com.twc.tiecoon.Adapters.PreferedLangAdapter;
import com.twc.tiecoon.Adapters.SelectCurrencyAdapter;
import com.twc.tiecoon.Adapters.SelectedProjectTalentAdapter;
import com.twc.tiecoon.Adapters.TalentsAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AgeGroupData;
import com.twc.tiecoon.network.response.AgeGroupDataList;
import com.twc.tiecoon.network.response.AssignmentModel;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.CurrencyData;
import com.twc.tiecoon.network.response.Feedback;
import com.twc.tiecoon.network.response.PreferredLanguage;
import com.twc.tiecoon.network.response.PreferredLanguageData;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.network.response.ProfileData;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.network.response.ProjectTemplatesModel;
import com.twc.tiecoon.network.response.RequestedUser;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Observable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CollaborationActivity extends AbstractFragmentActivity implements TextWatcher {

    TextView tvSelectProject,tvCreateProject;
    String userid,qbid;
    Button btnCollab;
    String projectid="",name="";
    EditText editText;
    NestedScrollView layoutProject;
    boolean click = true;

//    Create Project
    EditText et_name,editTextdes,editText_price_range,editTextYoutubeLink;
    TextView textView_select_Talents,tv_select_language,tv_select_Gender,tv_select_agegroup,tv_select_currency;
    RecyclerView recyclerView_selected_preferedLang,editrecyclerView_selected_preferedLang,rvTemplates;
    Button btnSave;
    LinearLayout linearParent;

    private final ProfileModel profileModel = new ProfileModel();
    private final List<ProfileTalent> addTalentDataList = new ArrayList<>();
    private final List<ProfileTalent> addTalentDataList2 = new ArrayList<>();
    private final List<ProfileTalent> selectedaddTalentDataList = new ArrayList<>();
    private ProfileCreateAssignment profileCreateAssignment;
    private ProfileTalent profileTalent = null;
    private Dialog dialogTalents;
    private TalentsAdapter talentsAdapter;
    List<String> list = new ArrayList<>();
    SelectedProjectTalentAdapter selectedProjectTalentAdapter;
    //    For Language
    private final List<PreferredLanguageData> preferredLanguageDataList = new ArrayList<>();
    private final List<PreferredLanguageData> preferredLanguageDataList2 = new ArrayList<>();
    private final List<String> genderList = new ArrayList<>();
    private final List<String> genderList2 = new ArrayList<>();
    //    For Gender
    private Dialog genderDialog;
    private GenderAdapter genderAdapter;
    String gender = "";
    private PreferedLangAdapter preferedLangAdapter;
    private Dialog preferredlangDialog;
    private PreferredLanguageData preferredLanguageData = null;
    String PreferredLang = "";
    //    For Currency
    private final List<CurrencyData> currencyDataList = new ArrayList<>();
    private final List<CurrencyData> currencyDataList2 = new ArrayList<>();
    private SelectCurrencyAdapter selectCurrencyAdapter;
    private Dialog dialogCurrency;
    private CurrencyData currencyData = null;
    String currencyCode="";
    //    For Age Group
    private final List<AgeGroupDataList> ageGroupDataList = new ArrayList<>();
    private final List<AgeGroupDataList> ageGroupDataList2 = new ArrayList<>();
    private AgeGroupAdapter ageGroupAdapter;
    private Dialog dialogAgeGroup;
    private AgeGroupDataList groupDataList = null;
    String AgeGroup = "";



    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_collaboration);

        userid = getIntent().getStringExtra("userid");
        qbid = getIntent().getStringExtra("qbid");
        name = getIntent().getStringExtra("name");

        btnCollab = findViewById(R.id.btnCollab);
        editText = findViewById(R.id.editText);
        tvCreateProject = findViewById(R.id.tvCreateProject);
        layoutProject = findViewById(R.id.layoutProject);

//       ================== Create Project ===================
        et_name = findViewById(R.id.et_name);
        editTextdes = findViewById(R.id.editTextdes);
        editText_price_range = findViewById(R.id.editText_price_range);
        editTextYoutubeLink = findViewById(R.id.editTextYoutubeLink);
        textView_select_Talents = findViewById(R.id.textView_select_Talents);
        tv_select_language = findViewById(R.id.tv_select_language);
        tv_select_Gender = findViewById(R.id.tv_select_Gender);
        tv_select_agegroup = findViewById(R.id.tv_select_agegroup);
        tv_select_currency = findViewById(R.id.tv_select_currency);
        recyclerView_selected_preferedLang = findViewById(R.id.recyclerView_selected_preferedLang);
        editrecyclerView_selected_preferedLang = findViewById(R.id.editrecyclerView_selected_preferedLang);
        rvTemplates = findViewById(R.id.rvTemplates);
        btnSave = findViewById(R.id.btnSave);
        linearParent = findViewById(R.id.linearParent);
//       ================== Create Project End ===================

        genderList2.add("male");
        genderList2.add("female");
        genderList2.add("other");
        genderList2.add("No preference");

        textView_select_Talents.setOnClickListener(v -> {
            if (addTalentDataList2.isEmpty()) {
                getTalents();

            } else {
                showTalentsDialog();
            }
        });
        tv_select_language.setOnClickListener(v -> {
            showPreferredLangDialog();
        });
        tv_select_agegroup.setOnClickListener(v -> {
            showAgeGroupDialog();
        });
        tv_select_currency.setOnClickListener(v -> {
            showCurrencyDialog();
        });
        tv_select_Gender.setOnClickListener(v -> {
            showGenderDialog();
        });

        selectedProjectTalentAdapter = new SelectedProjectTalentAdapter(this, selectedaddTalentDataList, pos -> {
            if (list.contains(selectedaddTalentDataList.get(pos).getId())) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).equals(selectedaddTalentDataList.get(pos).getId())) {
                        list.remove(i);
                    }
                }
            }
            selectedaddTalentDataList.remove(pos);
            selectedProjectTalentAdapter.notifyItemRemoved(pos);
            selectedProjectTalentAdapter.notifyItemRangeChanged(pos, selectedaddTalentDataList.size());
        });
        recyclerView_selected_preferedLang.setAdapter(selectedProjectTalentAdapter);

        btnCollab.setOnClickListener(v -> {
            String text = editText.getText().toString();
            showRefundConfirmDialog(text);
        });

        findViewById(R.id.iv_close).setOnClickListener(v -> onBackPressed());
        tvSelectProject=findViewById(R.id.tvSelectProject);

        tvSelectProject.setOnClickListener(v -> {
            editText.setText("");
            showProjectDialog();
        });

        tvCreateProject.setOnClickListener(v -> {
            if(click){
                layoutProject.setVisibility(View.VISIBLE);
                tvSelectProject.setEnabled(false);
                click = false;
            }
            else {
                layoutProject.setVisibility(View.GONE);
                tvSelectProject.setEnabled(true);
                click = true;
            }
        });
        projectTemplates();
        getCategory();
        btnSave.setOnClickListener(v -> CheckAssignmentValidation());

    }

    private void CheckAssignmentValidation() {
        if (et_name.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Name can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (editTextdes.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Description can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (editText_price_range.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Price can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (tv_select_currency.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Currency can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }

            if (selectedaddTalentDataList.size() == 0) {
                Toast.makeText(this, "Talents can't be empty", Toast.LENGTH_SHORT).show();
                return;
            }


        if(!editTextYoutubeLink.getText().toString().isEmpty()){

            String pattern = "^(http(s)?:\\/\\/)?((w){3}.)?youtu(be|.be)?(\\.com)?\\/.+";
            if (!editTextYoutubeLink.getText().toString().isEmpty() && editTextYoutubeLink.getText().toString().matches(pattern))
            {
                saveAssignment();
            }
            else
            {
                // Not Valid youtube URL
                Toast.makeText(this, "Enter valid youtube url", Toast.LENGTH_SHORT).show();
            }
////            String pattern = "https?:\\/\\/(?:[0-9A-Z-]+\\.)?(?:youtu\\.be\\/|youtube\\.com\\S*[^\\w\\-\\s])([\\w\\-]{11})(?=[^\\w\\-]|$)(?![?=&+%\\w]*(?:['\"][^<>]*>|<\\/a>))[?=&+%\\w]*";
//            if (!editTextYoutubeLink.getText().toString().matches(pattern)) {
//                ///Not Valid youtube URL
//                Toast.makeText(this, "Enter valid youtube url", Toast.LENGTH_SHORT).show();
//
//            }
//            else

        }
        else   saveAssignment();
    }

    private void saveAssignment() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);


                Map<String, String> map = new HashMap<>();
                map.put("name", et_name.getText().toString().trim());
                map.put("taxt", editTextdes.getText().toString().trim());
                for (int i = 0; i < selectedaddTalentDataList.size(); i++) {
                    map.put("talent_id[" + i + "]", selectedaddTalentDataList.get(i).getId() + "");
                }
                if (preferredLanguageData != null) {
                    map.put("language", preferredLanguageData.getId() + "");
                }

                if(gender.equals("No preference")){
                    map.put("gender", "no_preference");
                }
                else map.put("gender", gender);
                if (groupDataList != null) {
                    map.put("age", groupDataList.getId() + "");
                }
                if (currencyData != null) {
                    map.put("currency", currencyData.getCode() + "");
                }

                map.put("amount", editText_price_range.getText().toString().trim());

                map.put("youtublink", editTextYoutubeLink.getText().toString().trim());

                profileModel.createAssignment(this, linearParent, map);

        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }


    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void showAgeGroupDialog() {
        dialogAgeGroup = new Dialog(this);
        dialogAgeGroup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAgeGroup.setCancelable(true);
        dialogAgeGroup.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogAgeGroup.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Age Group");
        RecyclerView rvCurrency = dialogAgeGroup.findViewById(R.id.rvCurrency);
        ageGroupDataList.clear();
        ageGroupDataList.addAll(ageGroupDataList2);
        ageGroupAdapter = new AgeGroupAdapter(this, ageGroupDataList, pos -> {
            tv_select_agegroup.setText(ageGroupDataList.get(pos).getAge());
            tv_select_agegroup.setTextColor(getResources().getColor(R.color.white));
            groupDataList = ageGroupDataList.get(pos);
            AgeGroup = groupDataList.getAge();
            dialogAgeGroup.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(ageGroupAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                ageGroupDataList.clear();
                if (s.toString().isEmpty()) {
                    ageGroupDataList.addAll(ageGroupDataList2);
                } else {
                    for (AgeGroupDataList list : ageGroupDataList2) {
                        if (list.getAge().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            ageGroupDataList.add(list);
                        }
                    }
                }
                ageGroupAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogAgeGroup.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogAgeGroup.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogAgeGroup.show();
    }

    private void showCurrencyDialog() {
        dialogCurrency = new Dialog(this);
        dialogCurrency.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCurrency.setCancelable(true);
        dialogCurrency.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogCurrency.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Country Currency");
        RecyclerView rvCurrency = dialogCurrency.findViewById(R.id.rvCurrency);
        currencyDataList.clear();
        currencyDataList.addAll(currencyDataList2);
        selectCurrencyAdapter = new SelectCurrencyAdapter(this, currencyDataList, pos -> {
            tv_select_currency.setText(currencyDataList.get(pos).getCurrency());
            tv_select_currency.setTextColor(getResources().getColor(R.color.white));
            currencyData = currencyDataList.get(pos);
            currencyCode = currencyData.getCode();
            dialogCurrency.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(selectCurrencyAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                currencyDataList.clear();
                if (s.toString().isEmpty()) {
                    currencyDataList.addAll(currencyDataList2);
                } else {
                    for (CurrencyData list : currencyDataList2) {
                        if (list.getCountry().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            currencyDataList.add(list);
                        }
                    }
                }
                selectCurrencyAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogCurrency.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogCurrency.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCurrency.show();
    }


    private void showPreferredLangDialog() {
        preferredlangDialog = new Dialog(this);
        preferredlangDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        preferredlangDialog.setCancelable(true);
        preferredlangDialog.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = preferredlangDialog.findViewById(R.id.editText_price_search);
        editText_price_search.setHint(getString(R.string.preferred_language));
        RecyclerView rvCurrency = preferredlangDialog.findViewById(R.id.rvCurrency);
        preferredLanguageDataList.clear();
        preferredLanguageDataList.addAll(preferredLanguageDataList2);
        preferedLangAdapter = new PreferedLangAdapter(this, preferredLanguageDataList, pos -> {
            tv_select_language.setText(preferredLanguageDataList.get(pos).getName());
            tv_select_language.setTextColor(getResources().getColor(R.color.white));
            preferredLanguageData = preferredLanguageDataList.get(pos);
            PreferredLang = preferredLanguageData.getName();
            preferredlangDialog.dismiss();
        });
        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(preferedLangAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                preferredLanguageDataList.clear();
                if (s.toString().isEmpty()) {
                    preferredLanguageDataList.addAll(preferredLanguageDataList2);
                } else {
                    for (PreferredLanguageData preferredLanguageData : preferredLanguageDataList2) {
                        if (preferredLanguageData.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            preferredLanguageDataList.add(preferredLanguageData);
                        }
                    }
                }
                preferedLangAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        preferredlangDialog.setCanceledOnTouchOutside(true);
        preferredlangDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        preferredlangDialog.show();
    }


    RecyclerView rvProject;
    private void showProjectDialog() {

        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.project_popup);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(v -> dialog.dismiss());

        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);

        rvProject = dialog.findViewById(R.id.rvProject);
        rvProject.setHasFixedSize(true);
        rvProject.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));

        getAssignmentList(dialog,tvConfirm);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    private void showRefundConfirmDialog(String text) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText(text);
        btnRemove.setText("SEND");
        tvTitle.setVisibility(View.VISIBLE);
        tvTitle.setText("Collaboration Request");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            findUser(userid,projectid);
        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof ProfileData) {
            setTalents((ProfileData) o);


        }
        if (o instanceof CategoryCurrency) {
            setCategoryCurrency((CategoryCurrency) o);
            getUserLanguage();
        }
        else if (o instanceof PreferredLanguage) {
            setLanguage((PreferredLanguage) o);
            getAgeGroup();

        }
       else if (o instanceof AgeGroupData) {
            setageGroup((AgeGroupData) o);
//            getUserLanguage();
        }
        else if (o instanceof ProfileCreateAssignment) {
            Toast.makeText(this, "Project Created Successfully", Toast.LENGTH_SHORT).show();
            layoutProject.setVisibility(View.GONE);
            tvSelectProject.setEnabled(true);
            ProfileCreateAssignment profileCreateAssignment = (ProfileCreateAssignment) o;
            tvSelectProject.setText(profileCreateAssignment.getName());
            projectid = profileCreateAssignment.getId()+"";
            editText.setText("Hello, I want to invite you for my " +profileCreateAssignment.getName()+" If you are interested in " +
                    "working on this project please accept my invitation.");
        }
    }

    public void getCategory() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getCategoryCurrency(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    public void getAgeGroup() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getAgeGroup(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void getUserLanguage() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getPreferredLang(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1", "size:" + categoryDataList1.size() + ",  " + categoryDataList1.toString());

        List<CurrencyData> currencyDataList1 = categoryCurrency.getData().getPrice();
        if (currencyDataList1 != null && !(currencyDataList1.isEmpty())) {
            currencyDataList.clear();
            currencyDataList2.clear();
            currencyDataList2.addAll(currencyDataList1);
            currencyDataList.addAll(currencyDataList1);
        }
    }
    private void setLanguage(PreferredLanguage preferredLanguage) {
        List<PreferredLanguageData> preferredLanguageData = preferredLanguage.getData();
        if (preferredLanguageData != null && !(preferredLanguageData.isEmpty())) {
            preferredLanguageDataList.clear();
            preferredLanguageDataList2.clear();
            preferredLanguageDataList2.addAll(preferredLanguageData);
            preferredLanguageDataList.addAll(preferredLanguageData);
        }
    }

    private void setageGroup(AgeGroupData ageGroupData) {
        List<AgeGroupDataList> ageGroupDataLists = ageGroupData.getData();
        if (ageGroupDataLists != null && !(ageGroupDataLists.isEmpty())) {
            ageGroupDataList.clear();
            ageGroupDataList2.clear();
            ageGroupDataList2.addAll(ageGroupDataLists);
            ageGroupDataList.addAll(ageGroupDataLists);
        }
    }


    private void setTalents(ProfileData profileData) {
        if (!(profileData.getTalents().isEmpty())) {
            addTalentDataList.addAll(profileData.getTalents());
            addTalentDataList2.addAll(profileData.getTalents());
            if (profileCreateAssignment != null) {
                if (profileCreateAssignment.getTalant_id() != null) {
                    for (ProfileTalent profileTalent1 : profileData.getTalents()) {
                        if (profileTalent1.getId() == Integer.parseInt(profileCreateAssignment.getTalant_id())) {
                            textView_select_Talents.setText(profileTalent1.getName());
                            profileTalent = profileTalent1;
                        }
                    }
                }
            } else {
                showTalentsDialog();
            }
        } else {
            Toast.makeText(this, "No talents found!!", Toast.LENGTH_SHORT).show();
        }
    }

    private void showGenderDialog() {
        genderDialog = new Dialog(this);
        genderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        genderDialog.setCancelable(true);
        genderDialog.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = genderDialog.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Gender");
        RecyclerView rvCurrency = genderDialog.findViewById(R.id.rvCurrency);
        genderList.add("male");
        genderList.add("female");
        genderList.add("other");
        genderList.add("No preference");
        genderList.clear();
        genderList.addAll(genderList2);
        Log.e("genderlist", genderList.toString());
        Log.e("genderlist2", genderList2.toString());

        genderAdapter = new GenderAdapter(this, genderList, pos -> {
            tv_select_Gender.setText(genderList.get(pos));
            tv_select_Gender.setTextColor(getResources().getColor(R.color.white));
            gender = genderList.get(pos);
            genderDialog.dismiss();
        });
        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(genderAdapter);

        genderDialog.setCanceledOnTouchOutside(true);
        genderDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        genderDialog.show();
    }

    private void showTalentsDialog() {
        dialogTalents = new Dialog(this);
        dialogTalents.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTalents.setCancelable(true);
        dialogTalents.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = dialogTalents.findViewById(R.id.editText_price_search);
        editText_price_search.setHint(getString(R.string.talents));
        RecyclerView rvCurrency = dialogTalents.findViewById(R.id.rvCurrency);
        addTalentDataList.clear();
        addTalentDataList.addAll(addTalentDataList2);

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);

        talentsAdapter = new TalentsAdapter(this, addTalentDataList, pos -> {

         /*   if (addTalentDataList.get(pos).getIsseletcted() == null ||addTalentDataList.get(pos).getIsseletcted().equals("0")){
                addTalentDataList.get(pos).setIsseletcted("1");
            }else{
                addTalentDataList.get(pos).setIsseletcted("0");
            }
            talentsAdapter.notifyDataSetChanged();*/
            textView_select_Talents.setText(addTalentDataList.get(pos).getName());
            profileTalent = addTalentDataList.get(pos);
            selectedaddTalentDataList.add(addTalentDataList.get(pos));

            if (list.contains(addTalentDataList.get(pos).getId())) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).equals(addTalentDataList.get(pos).getId())) {
                        list.remove(i);
                    }
                }
            } else {
                list.add(addTalentDataList.get(pos).getId() + "");
            }
            selectedProjectTalentAdapter.notifyDataSetChanged();
            dialogTalents.dismiss();
        });
        rvCurrency.setAdapter(talentsAdapter);

        editText_price_search.addTextChangedListener(this);
        dialogTalents.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogTalents.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogTalents.show();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        filter(s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void filter(String charText) {
        String newCharText = charText.toLowerCase(Locale.getDefault()).trim();
        addTalentDataList.clear();
        if (charText.isEmpty()) {
            addTalentDataList.addAll(addTalentDataList2);
        } else {
            for (ProfileTalent profileTalent : addTalentDataList2) {
                if (profileTalent.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                    addTalentDataList.add(profileTalent);
                }
            }
        }
        talentsAdapter.notifyDataSetChanged();
    }


    public static class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.ViewHolder> {

        private Context context;
        private List<AssignmentModel.Data> list;
        private ItemClickListener itemClickListener;

        public ProjectAdapter(Context context,List<AssignmentModel.Data> list,ItemClickListener itemClickListener) {

            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;

        }

        @Override
        public ProjectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.project_dropdown_list, parent, false);
            return new ProjectAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(ProjectAdapter.ViewHolder holder, final int position) {
            holder.tvReportName.setText(new StringBuilder().append(list.get(position).getName()));
            holder.tvReportName.setOnClickListener(v -> itemClickListener.itemClick(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            LinearLayout layoutReport;
            TextView tvReportName;
            public ViewHolder(View itemView) {
                super(itemView);
                layoutReport = itemView.findViewById(R.id.layoutReport);
                tvReportName = itemView.findViewById(R.id.tvReportName);
            }
        }
    }

    public void getAssignmentList(Dialog dialog,TextView tvConfirm) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<AssignmentModel> call = apiInterface.getAssignmentList("Bearer "+accessToken);

        call.enqueue(new Callback<AssignmentModel>() {
            @Override
            public void onResponse(Call<AssignmentModel> call, Response<AssignmentModel> response) {
                AssignmentModel user = response.body();
                Log.e("user",user.toString());
                if(user.getData().size()>0){
                    tvConfirm.setVisibility(View.GONE);
                    rvProject.setVisibility(View.VISIBLE);

                    ProjectAdapter projectAdapter = new ProjectAdapter(CollaborationActivity.this, user.getData(), pos -> {
                        dialog.dismiss();
                        tvSelectProject.setText(user.getData().get(pos).getName());
                        projectid = user.getData().get(pos).getId();
                        editText.setText("Hello, I want to invite you for my " +user.getData().get(pos).getName()+" If you are interested in " +
                                "working on this project please accept my invitation.");
                    });
                    rvProject.setAdapter(projectAdapter);
                }
                else {
                    tvConfirm.setVisibility(View.VISIBLE);
                    tvConfirm.setText("You have not created any project.");

                    rvProject.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AssignmentModel> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void findUser(String userid,String projevtid) {
        final Dialog dialog = new Dialog(CollaborationActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<Feedback> call = apiInterface.findUser("Bearer "+accessToken,userid,projevtid);

        call.enqueue(new Callback<Feedback>() {
            @Override
            public void onResponse(Call<Feedback> call, Response<Feedback> response) {
                dialog.dismiss();
                Feedback user = response.body();
                assert user != null;

                if(user.getSuccess().equals("true")){
                    Toast.makeText(CollaborationActivity.this, user.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else
                     Toast.makeText(CollaborationActivity.this, user.getMessage(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<Feedback> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });

    }

    private void getTalents() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getUserDetails(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    public void projectTemplates() {
//        final Dialog dialog = new Dialog(CreateAssignentNew.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProjectTemplatesModel> call = apiInterface.projectTemplates("Bearer "+accessToken);
        call.enqueue(new Callback<ProjectTemplatesModel>() {
            @Override
            public void onResponse(Call<ProjectTemplatesModel> call, Response<ProjectTemplatesModel> response) {
//                dialog.dismiss();
                ProjectTemplatesModel user = response.body();
                // if(user.message.equals("Already Request sent.")){
                TemplatesAdapter templatesAdapter = new TemplatesAdapter(CollaborationActivity.this,user.data,(pos) ->{
                    editTextdes.setText(user.data.get(pos).description);
                });
                rvTemplates.setAdapter(templatesAdapter);
                // }
            }
            @Override
            public void onFailure(Call<ProjectTemplatesModel> call, Throwable t) {
//                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


    public class TemplatesAdapter extends RecyclerView.Adapter<TemplatesAdapter.ViewHolder> {
        private Context context;
        private ItemClickListener itemClickListener;
        List<ProjectTemplatesModel.Data> list;

        public TemplatesAdapter(Context context,   List<ProjectTemplatesModel.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.itemClickListener = itemClickListener;
            this.list = list;
        }

        @Override
        public TemplatesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.layout_templates, parent, false);
            return new TemplatesAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(TemplatesAdapter.ViewHolder holder, final int position) {
            holder.tv_talent.setText(list.get(position).title);
            holder.linear.setOnClickListener(v -> itemClickListener.itemClick(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
        public class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout linear;
            TextView tv_talent;
            public ViewHolder(View itemView) {
                super(itemView);
                linear = itemView.findViewById(R.id.linear);
                tv_talent = itemView.findViewById(R.id.tv_talent);
            }
        }
    }




}