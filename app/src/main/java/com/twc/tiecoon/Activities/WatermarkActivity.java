package com.twc.tiecoon.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gowtham.library.utils.TrimType;
import com.gowtham.library.utils.TrimVideo;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.theartofdev.edmodo.cropper.CropImage;
import com.twc.instagrampicker.InstagramPicker;
import com.twc.tiecoon.Adapters.SelectCategoryAdapter;
import com.twc.tiecoon.Adapters.SelectColorAdapter;
import com.twc.tiecoon.Adapters.SelectCurrencyAdapter;
import com.twc.tiecoon.Adapters.SelectSubCategoryAdapter;
import com.twc.tiecoon.Adapters.UploadImageListAdapter;
import com.twc.tiecoon.Adapters.UploadVideoListAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.CustomMedia.JZMediaExo;
import com.twc.tiecoon.Helper.AppLog;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.models.tagModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.CurrencyData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.SaveUnsaveAds;
import com.twc.tiecoon.network.response.SubCategory;
import com.twc.tiecoon.network.response.SubCategoryData;
import com.twc.tiecoon.network.response.SubscriptionModel;
import com.twc.tiecoon.network.response.UploadImageListData;
import com.twc.tiecoon.network.response.UploadVideoListData;
import com.twc.tiecoon.network.response.UserDetailTags;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ProgressRequestBody;
import com.twc.tiecoon.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Observable;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;
import jp.wasabeef.richeditor.RichEditor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WatermarkActivity extends AbstractFragmentActivity implements View.OnClickListener, TextWatcher, ProgressRequestBody.UploadCallbacks {
    private final String[] PROFILE_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int PROFILE_PERMISSION_REQUEST = 501;
    private MultipartBody.Part multiBodyPart = null;
    private MultipartBody.Part videothumbnail = null;
    List<MultipartBody.Part> partList = new ArrayList<>();
    private MultipartBody.Part multiBodyPartAudio = null;
    private MultipartBody.Part multiBodyPartVideo = null;
    private Uri photoURI;
    private static final int REQUEST_GALLERY_CODE = 505;
    private static final int REQUEST_CAMERA_CODE = 506;
    private static final int REQUEST_AUDIO = 1;
    private static final int REQUEST_VIDEO = 2;
    private boolean profilePermissionDeniedWithDontAskMeAgain = false;
    public static final int WATERMARKBUY = 101;
    private LinearLayout linearParent, linearSpinnerSubCategory;
    private CardView cardViewImg;
    private Spinner spinnerCategory, spinnerSubCategory;
    private ImageView iv_cancel, img_select, imgDemo;
    private TextView tvDone, tvWatermarkDemo, textViewWaterMark, tvWatermark, tvEdit, tv_select_currency, tv_sub_category, tvSave;
    private RelativeLayout relativeImageView, relativeWaterMark, relativeWaterMarkBorder, relativeAdd, relativeRefresh, rlAdd;
    private SeekBar seekBar_textSize;
    private RecyclerView recyclerView_SelectColor, rvCategory, rvSubCategory;
    private EditText editText_price_range, et_description;
    private ProfileModel profileModel = new ProfileModel();
    private List<String> listColor = new ArrayList<>();
    private List<String> listCategorySpinner = new ArrayList<>();
    private List<String> listSubCategorySpinner = new ArrayList<>();
    private List<CategoryData> categoryDataList = new ArrayList<>();
    private SelectCategoryAdapter selectCategoryAdapter;
    private SelectSubCategoryAdapter selectSubCategoryAdapter;
    private List<SubCategoryData> subCategoryDataList = new ArrayList<>();
    private List<CurrencyData> currencyDataList = new ArrayList<>();
    private List<CurrencyData> currencyDataList2 = new ArrayList<>();
    private SelectCurrencyAdapter selectCurrencyAdapter;
    private Dialog dialogCurrency;
    private String watermarkColor = "#3B6FFF";
    private SubCategoryData subCategoryData = null;
    private CategoryData categoryData = null;
    private CurrencyData currencyData = null;
    private String uploadType = "";
    private boolean isCategory = false;
    private int categoryId = 0;
    private int subCategoryId = 0;
    private Object updateItem = null;
    private int itemId = 0;
    private String categoryName = "";
    private String currencyCode = "";
    private String currency = "";
    private String categoryTitle = "";
    private String subcategoryTitle = "";
    LinearLayout layoutAudio, layoutVideo;
    TextView tvAudio, tvVideo;
    Dialog progressDialog;

    String talent_type = "";
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private MediaRecorder recorder = null;
    private int currentFormat = 0;
    private int output_formats[] = {MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP};
    private String file_exts[] = {AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP};
    List<Uri> uriList1;
    private List<UploadImageListData> uploadImageListData = new ArrayList<>();
    private List<UploadVideoListData> uploadVideoListData = new ArrayList<>();
    RecyclerView recyclerViewUploadImageList, recyclerViewUploadVideoList, recyclerViewUploadImageList1;
    private UploadImageListAdapter uploadImageListAdapter;
    private UploadVideoListAdapter uploadVideoListAdapter;
    private final static int FILE_REQUEST_CODE = 3;
    private final static int VIDEO_REQUEST_CODE = 4;
    private final static int VIDEO_TRIMMER_REQ_CODE = 111;
    private final static int AUDIO_REQUEST_CODE = 5;
    List<MediaFile> mediaFiles = new ArrayList<>();
    List<MediaFile> mediaVideo = new ArrayList<>();
    List<MediaFile> mediaAudio = new ArrayList<>();
    RadioButton rbImage, rbVideo, rbAudio, rbText;
    LinearLayout layoutImage, layoutText,lineareditor;
    CardView cardViewVideo, cardViewAudio;
    EditText et_text;
    TextView tvPercentage;
    TextView tvTitle;
    ImageView vdo_select, audio_select, ivPlayButton;
    LinearLayout layoutAddWatermark,layoutUploads;
    Uri urivideo;
    ScrollView mScrollview;
    LinearLayout layoutButtons, layoutAudioButtons;
    ArrayList<Uri> uris = new ArrayList<>();
    JzvdStd jz_video;
    Button button_AudioView, button_AudioEdit, button_UploadAudio;
    String percentage_text;
    MultipartBody.Part[] fileToUpload1;
    RichEditor mEditor;
    ImageView action_bold;
    Button button_Edit;
    MediaPlayer mp = new MediaPlayer();
    RelativeLayout layoutTag;
    ArrayList<String> TagUser_Id = new ArrayList<>();
    public static final int TAGUSER = 687;
    TextView tvTag;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_watermark);
        initView();
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {

        uriList1 = new ArrayList<>();

        if (getIntent() != null && getIntent().hasExtra("type")) {
            uploadType = getIntent().getStringExtra("type");
        }

        if (getIntent() != null && getIntent().hasExtra("updateItem")) {
            updateItem = getIntent().getParcelableExtra("updateItem");
            Log.e("methodcall",updateItem.toString());
        }

        categoryName = Objects.requireNonNull(getIntent()).getStringExtra("categoryName");
        if (getIntent() != null && getIntent().hasExtra("categoryId")) {
            isCategory = true;
            categoryId = getIntent().getIntExtra("categoryId", 0);
        }


        linearParent = findViewById(R.id.linearParent);
        iv_cancel = findViewById(R.id.iv_cancel);
        tvDone = findViewById(R.id.tvDone);
        img_select = findViewById(R.id.img_select);
        imgDemo = findViewById(R.id.imgDemo);
        cardViewImg = findViewById(R.id.cardViewImg);
        layoutAudio = findViewById(R.id.layoutAudio);
        layoutVideo = findViewById(R.id.layoutVideo);
        rbImage = findViewById(R.id.rbImage);
        rbVideo = findViewById(R.id.rbVideo);
        rbAudio = findViewById(R.id.rbAudio);
        tvTag = findViewById(R.id.tvTag);

        lineareditor = findViewById(R.id.lineareditor);

        layoutButtons = findViewById(R.id.layoutButtons);
        layoutAudioButtons = findViewById(R.id.layoutAudioButtons);
        button_AudioView = findViewById(R.id.button_AudioView);
        button_AudioEdit = findViewById(R.id.button_AudioEdit);
        button_UploadAudio = findViewById(R.id.button_UploadAudio);
        layoutUploads = findViewById(R.id.layoutUploads);
        rbText = findViewById(R.id.rbText);
        jz_video = findViewById(R.id.jz_video);
        ivPlayButton = findViewById(R.id.ivPlayButton);
        layoutImage = findViewById(R.id.layoutImage);
        layoutText = findViewById(R.id.layoutText);
        cardViewVideo = findViewById(R.id.cardViewVideo);
        cardViewAudio = findViewById(R.id.cardViewAudio);
        et_text = findViewById(R.id.et_text);
        vdo_select = findViewById(R.id.vdo_select);
        button_Edit = findViewById(R.id.button_Edit);
        audio_select = findViewById(R.id.audio_select);
        tvTitle = findViewById(R.id.tvTitle);
        relativeImageView = findViewById(R.id.relativeImageView);
        relativeWaterMark = findViewById(R.id.relativeWaterMark);
        relativeWaterMarkBorder = findViewById(R.id.relativeWaterMarkBorder);
        relativeAdd = findViewById(R.id.relativeAdd);
        relativeRefresh = findViewById(R.id.relativeRefresh);
        rlAdd = findViewById(R.id.rlAdd);
        textViewWaterMark = findViewById(R.id.textViewWaterMark);
        tvAudio = findViewById(R.id.tvAudio);
        tvVideo = findViewById(R.id.tvVideo);
        layoutTag = findViewById(R.id.layoutTag);
//dialog
        progressDialog = new Dialog(WatermarkActivity.this);
        progressDialog.setContentView(R.layout.dialog_login);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCancelable(false);

        tvPercentage = progressDialog.findViewById(R.id.tvPercentage);

        seekBar_textSize = findViewById(R.id.seekBar_textSize);
        recyclerView_SelectColor = findViewById(R.id.recyclerView_SelectColor);
        tvWatermark = findViewById(R.id.tvWatermark);
        layoutAddWatermark = findViewById(R.id.layoutAddWatermark);
        tvWatermarkDemo = findViewById(R.id.tvWatermarkDemo);
        tvEdit = findViewById(R.id.tvEdit);
        spinnerCategory = findViewById(R.id.spinnerCategory);
        linearSpinnerSubCategory = findViewById(R.id.linearSpinnerSubCategory);
        spinnerSubCategory = findViewById(R.id.spinnerSubCategory);
        rvCategory = findViewById(R.id.rvCategory);
        tv_sub_category = findViewById(R.id.tv_sub_category);
        rvSubCategory = findViewById(R.id.rvSubCategory);
        tv_select_currency = findViewById(R.id.tv_select_currency);
        editText_price_range = findViewById(R.id.editText_price_range);
        et_description = findViewById(R.id.et_description);
        tvSave = findViewById(R.id.tvSave);
        recyclerViewUploadImageList = findViewById(R.id.recyclerViewUploadImageList);
        recyclerViewUploadImageList1 = findViewById(R.id.recyclerViewUploadImageList1);
        recyclerViewUploadVideoList = findViewById(R.id.recyclerViewUploadVideoList);
        mScrollview = findViewById(R.id.mScrollview);
        iv_cancel.setOnClickListener(this);
        tvDone.setOnClickListener(this);
        img_select.setOnClickListener(this);
        relativeImageView.setOnClickListener(this);
        cardViewImg.setOnClickListener(this);
        textViewWaterMark.setOnClickListener(this);
        relativeWaterMarkBorder.setOnClickListener(this);
        tvWatermark.setOnClickListener(this);
        tvEdit.setOnClickListener(this);

        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setEditorHeight(200);
        mEditor.setEditorFontSize(22);
        mEditor.setEditorFontColor(Color.BLUE);
        //mEditor.setEditorBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundResource(R.drawable.bg);
        mEditor.setPadding(10, 10, 10, 10);
        //mEditor.setBackground("https://raw.githubusercontent.com/wasabeef/art/master/chip.jpg");
        mEditor.setPlaceholder("Insert text here...");
        //mEditor.setInputEnabled(false);

        mEditor.setOnTextChangeListener(new RichEditor.OnTextChangeListener() {
            @Override
            public void onTextChange(String text) {
                textViewWaterMark.setText(text);
            }
        });

        findViewById(R.id.action_undo).setOnClickListener(v -> mEditor.undo());

        findViewById(R.id.action_redo).setOnClickListener(v -> mEditor.redo());

        action_bold = findViewById(R.id.action_bold);
        action_bold.setOnClickListener(v -> {
//            if(!bold){
//                action_bold.setBackgroundColor(Color.BLUE);
            mEditor.setBold();
//                bold = true;
//            }
//            else {
//                action_bold.setBackgroundColor(Color.BLACK);
//                mEditor.removeView(v);
//                bold = false;
//            }
        });

        findViewById(R.id.action_italic).setOnClickListener(v -> mEditor.setItalic());

        findViewById(R.id.action_strikethrough).setOnClickListener(v -> mEditor.setStrikeThrough());

        findViewById(R.id.action_underline).setOnClickListener(v -> mEditor.setUnderline());

        findViewById(R.id.action_align_left).setOnClickListener(v -> mEditor.setAlignLeft());

        findViewById(R.id.action_align_center).setOnClickListener(v -> mEditor.setAlignCenter());

        findViewById(R.id.action_align_right).setOnClickListener(v -> mEditor.setAlignRight());

        findViewById(R.id.action_insert_bullets).setOnClickListener(v -> mEditor.setBullets());

        findViewById(R.id.action_insert_numbers).setOnClickListener(v -> mEditor.setNumbers());

        tvWatermarkDemo.setText("Tiecoon");
        tv_select_currency.setOnClickListener(this);
        tvSave.setOnClickListener(this);
        cardViewVideo.setOnClickListener(this);
        button_AudioView.setOnClickListener(this);
        button_AudioEdit.setOnClickListener(this);
        button_UploadAudio.setOnClickListener(this);
        tvTitle.setText(categoryName);

        rbImage.setOnCheckedChangeListener((compoundButton, b) -> {
            if (rbImage.isChecked()) {
                talent_type = "image";
                audio_select.setImageBitmap(null);
                vdo_select.setImageBitmap(null);
                layoutImage.setVisibility(View.VISIBLE);
                layoutText.setVisibility(View.GONE);
                cardViewVideo.setVisibility(View.GONE);
                cardViewAudio.setVisibility(View.GONE);
                button_UploadAudio.setVisibility(View.GONE);
                layoutAddWatermark.setVisibility(View.VISIBLE);
            }
        });

        rbVideo.setOnCheckedChangeListener((compoundButton, b) -> {
            if (rbVideo.isChecked()) {
                talent_type = "video";
                audio_select.setImageBitmap(null);
                img_select.setImageBitmap(null);
                layoutImage.setVisibility(View.GONE);
                layoutText.setVisibility(View.GONE);
                cardViewVideo.setVisibility(View.VISIBLE);
                cardViewAudio.setVisibility(View.GONE);
                button_UploadAudio.setVisibility(View.GONE);
                layoutAddWatermark.setVisibility(View.GONE);
            }
        });

//        mp.setOnCompletionListener(mp -> {
//            try {
//
//                mp.start();
////                mp.setLooping(true);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });

        rbAudio.setOnCheckedChangeListener((compoundButton, b) -> {
            if (rbAudio.isChecked()) {
                talent_type = "audio";
                vdo_select.setImageBitmap(null);
                img_select.setImageBitmap(null);
                layoutImage.setVisibility(View.GONE);
                layoutText.setVisibility(View.GONE);
                cardViewVideo.setVisibility(View.GONE);
                cardViewAudio.setVisibility(View.VISIBLE);
                button_UploadAudio.setVisibility(View.VISIBLE);
                layoutAddWatermark.setVisibility(View.GONE);
            }
        });

        rbText.setOnCheckedChangeListener((compoundButton, b) -> {
            if (rbText.isChecked()) {
                talent_type = "text";
                layoutImage.setVisibility(View.GONE);
                layoutText.setVisibility(View.VISIBLE);
                cardViewVideo.setVisibility(View.GONE);
                cardViewAudio.setVisibility(View.GONE);
                button_UploadAudio.setVisibility(View.GONE);
                layoutAddWatermark.setVisibility(View.GONE);
            }
        });

       // tvWatermarkDemo.setText(ApplicationClass.appPreferences.getUserName());

        layoutAudio.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, REQUEST_AUDIO);
        });

        layoutVideo.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, REQUEST_VIDEO);
        });

        uploadImageListAdapter = new UploadImageListAdapter(this, uploadImageListData);
        recyclerViewUploadImageList.setAdapter(uploadImageListAdapter);

        uploadVideoListAdapter = new UploadVideoListAdapter(this, uploadVideoListData);
        recyclerViewUploadVideoList.setAdapter(uploadVideoListAdapter);

        et_description.setOnTouchListener((view, event) -> {
            if (view.getId() == R.id.et_description) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                }
            }
            return false;
        });

        layoutTag.setOnClickListener(v -> {
            Intent intent = new Intent(this,TagUserActivity.class);
            startActivityForResult(intent,TAGUSER);
        });

        setUpdateDataForItem();

        SelectColorAdapter selectColorAdapter = new SelectColorAdapter(this, getArrayList(), pos -> {
            imgDemo.setVisibility(View.GONE);
            textViewWaterMark.setTextColor(Color.WHITE);
            img_select.setVisibility(View.GONE);
            if (textViewWaterMark.getText().toString().isEmpty()) {
                relativeWaterMark.setVisibility(View.VISIBLE);
            }

//            relativeImageView.setVisibility(View.VISIBLE);
            Drawable drawable = relativeImageView.getBackground();
            drawable.setTint(Color.parseColor(listColor.get(pos)));
            relativeImageView.setBackground(drawable);
            multiBodyPart = null;
            watermarkColor = listColor.get(pos);
            mEditor.setBackground(drawable);

        });
        recyclerView_SelectColor.setAdapter(selectColorAdapter);

        rvCategory.setHasFixedSize(true);
        rvCategory.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        selectCategoryAdapter = new SelectCategoryAdapter(this, categoryId, isCategory, categoryDataList, (pos, type) -> {
            if (type.equals("update")) {
                categoryData = categoryDataList.get(pos);
                getSubCategoryFromServer(categoryDataList.get(pos).getId());
            } else {
                if (!(categoryId == categoryDataList.get(pos).getId())) {
                    categoryId = 0;
                    subCategoryData = null;
                    categoryData = categoryDataList.get(pos);
                    if (selectSubCategoryAdapter != null && subCategoryId > 0) {
                        selectSubCategoryAdapter.updateId();
                    }
                    getSubCategoryFromServer(categoryDataList.get(pos).getId());
                }
            }
        });
        rvCategory.setAdapter(selectCategoryAdapter);

        rvSubCategory.setHasFixedSize(true);
        rvSubCategory.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        selectSubCategoryAdapter = new SelectSubCategoryAdapter(this, subCategoryId, subCategoryDataList, pos -> {
            subCategoryData = subCategoryDataList.get(pos);
            subCategoryId = subCategoryDataList.get(pos).getId();
        });
        rvSubCategory.setAdapter(selectSubCategoryAdapter);



        seekBar_textSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                relativeWaterMark.setVisibility(View.GONE);
                textViewWaterMark.setVisibility(View.VISIBLE);
                textViewWaterMark.setTextSize(TypedValue.COMPLEX_UNIT_SP, progress);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    categoryId = 0;
                } else {
                    categoryId = getCategoryIdData(listCategorySpinner.get(i));
                    getSubCategoryFromServer(categoryId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    subCategoryId = 0;
                } else {
                    subCategoryId = getSubCategoryIdData(listSubCategorySpinner.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        et_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textViewWaterMark.setText(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        Button button_View = findViewById(R.id.button_View);
         button_Edit = findViewById(R.id.button_Edit);

        button_View.setOnClickListener(view -> {
            jz_video.setVisibility(View.VISIBLE);
            cardViewVideo.setVisibility(View.GONE);
            jz_video.startVideo();
        });

        button_Edit.setOnClickListener(view -> {
            checkPermissionForVideo();
        });

        getCategory();

    }

    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }
        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + file_exts[currentFormat]);
    }

    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(output_formats[currentFormat]);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(getFilename());
        recorder.setOnErrorListener(errorListener);
        recorder.setOnInfoListener(infoListener);

        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final MediaRecorder.OnErrorListener errorListener = (mr, what, extra) -> AppLog.logString("Error: " + what + ", " + extra);

    private final MediaRecorder.OnInfoListener infoListener = (mr, what, extra) -> AppLog.logString("Warning: " + what + ", " + extra);


    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof CategoryCurrency) {
            setCategoryCurrency((CategoryCurrency) o);
            getSubCategoryFromServer(categoryId);
        } else if (o instanceof SubCategory) {
            List<SubCategoryData> subCategoryDataList1 = ((SubCategory) o).getData();
            if (subCategoryDataList1 != null && !(subCategoryDataList1.isEmpty())) {
                for (SubCategoryData subCategoryData : subCategoryDataList1) {
                    if (subCategoryData.getTitle() != null) {
                        listSubCategorySpinner.add(subCategoryData.getTitle());
                    }
                }
                listSubCategorySpinner.add(0, "Select Subcategory");
                ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, listSubCategorySpinner);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Setting the ArrayAdapter data on the Spinner
                spinnerSubCategory.setAdapter(arrayAdapter);
                if (updateItem != null && !(subcategoryTitle.isEmpty())) {
                    setSubCategoryInSubSpinner(subcategoryTitle);
                }
                subCategoryData = null;
                tv_sub_category.setVisibility(View.VISIBLE);
                //   linearSpinnerSubCategory.setVisibility(View.VISIBLE);
                subCategoryDataList.clear();
                subCategoryDataList.addAll(subCategoryDataList1);
                selectSubCategoryAdapter.notifyDataSetChanged();
            } else {
                subCategoryData = null;
                subCategoryDataList.clear();
                selectSubCategoryAdapter.notifyDataSetChanged();
                tv_sub_category.setVisibility(View.GONE);
            }
        } else if (o instanceof HomeUserDetailsProduct) {
            HomeUserDetailsProduct addProductData = (HomeUserDetailsProduct) o;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("UploadData", addProductData);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else if (o instanceof HomeUserDetailsService) {
            HomeUserDetailsService homeUserDetailsService = (HomeUserDetailsService) o;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("UploadData", homeUserDetailsService);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else if (o instanceof AddBrandsData) {
            AddBrandsData uploadsBrandsData = (AddBrandsData) o;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("UploadData", uploadsBrandsData);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else if (o instanceof HomeUserDetailsUploadTalent) {
            Toast.makeText(this, "Talent Uploaded Successfully", Toast.LENGTH_SHORT).show();
            HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = (HomeUserDetailsUploadTalent) o;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("UploadData", homeUserDetailsUploadTalent);
            returnIntent.putExtra("Talent", "add");
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_cancel:
                if (Jzvd.backPress()) {
                    return;
                }
                onBackPressed();
                mp.stop();
                break;
            case R.id.tvDone:
                break;
            case R.id.img_select:
                checkPermissionForProfile();
                break;
            case R.id.cardViewImg:
                checkPermissionForProfile();
                break;
            case R.id.cardViewVideo:
                checkPermissionForVideo();
                break;
            case R.id.button_AudioEdit:
                checkPermissionForAudio();
                break;
            case R.id.button_UploadAudio:
                checkPermissionForAudio();
                break;
            case R.id.button_AudioView:
                // TODO Auto-generated method stub
                mp.setOnCompletionListener(MediaPlayer::stop);
//                mp.start();
                try {
                    mp.prepare();
                    mp.start();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exception", e.toString());
                }
                break;
            case R.id.relativeWaterMarkBorder:
                openSetWaterMarkDialog();
                break;
            case R.id.tvWatermark:
                openSetWaterMarkDialog();
                break;
            case R.id.tvEdit:
                openSetWaterMarkDialog();
                break;
            case R.id.tv_select_currency:
                showCurrencyDialog();
                break;
            case R.id.rlAdd:
                break;
            case R.id.tvSave:
                submit();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PROFILE_PERMISSION_REQUEST) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else if (PermissionChecker.checkCallingOrSelfPermission(this,
                    Manifest.permission.CAMERA) != PermissionChecker.PERMISSION_GRANTED ||
                    PermissionChecker.checkCallingOrSelfPermission(this
                            , Manifest.permission.READ_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED ||
                    PermissionChecker.checkCallingOrSelfPermission(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
                profilePermissionDeniedWithDontAskMeAgain = true;
            }
        }
    }

    String audiopath = "", videopath = "";
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == TrimVideo.VIDEO_TRIMMER_REQ_CODE && data != null) {
                Uri uri = Uri.parse(TrimVideo.getTrimmedVideoPath(data));
                Log.d("2345678", "Trimmed path:: " + uri.getPath());
                cardViewVideo.setVisibility(View.VISIBLE);
                jz_video.setVisibility(View.GONE);
                if (mediaVideo.size() > 0) {
                    MediaFile mediaVideos = mediaVideo.get(0);
                    if (mediaVideos.getMediaType() == MediaFile.TYPE_VIDEO) {
                        String filePath = Utils.getUriRealPath(this, urivideo);
                        File file = new File(Objects.requireNonNull(uri.getPath()));
                        File file2 = new File(Objects.requireNonNull(uri.getPath()));
                        videopath = Utils.getUriRealPath(this, mediaVideos.getUri());
//                        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
                        ProgressRequestBody fileBody = new ProgressRequestBody(file, "*/*", this);
                        ProgressRequestBody fileBody1 = new ProgressRequestBody(file2, "*/*", this);
//                        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
//                        fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), fileBody);
                        Log.e("filesss",file.getName());
                        videothumbnail = MultipartBody.Part.createFormData("videothumbnail", file.getName(), fileBody);
                        multiBodyPartVideo = MultipartBody.Part.createFormData("video", file2.getName(), fileBody1);
                    }
                }
            }

            try {
                switch (requestCode) {
                    case REQUEST_GALLERY_CODE:
                        assert data != null;
                        Uri imageUri = data.getData();
                        Bitmap bitmap = Utils.getScaledBitmap(this, imageUri);
                        Uri uri = Utils.getImageUri(this, bitmap);
                        Utils.cropImage(this, uri);
                        break;
                    case REQUEST_CAMERA_CODE:
                        Uri uri2 = photoURI;
                        Utils.cropImage(this, uri2);
                        break;
                    case REQUEST_AUDIO:
                        Uri selectedImage = data.getData();
                        String[] filePath = {MediaStore.Audio.Media.DATA};
                        Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                        c.moveToFirst();
                        int columnIndex = c.getColumnIndex(filePath[0]);
                        audiopath = c.getString(columnIndex);
                        String filename = audiopath.substring(audiopath.lastIndexOf("/") + 1);
                        tvAudio.setText(filename);
                        c.close();
                        Bitmap thumbnail = (BitmapFactory.decodeFile(audiopath));
                        Log.w("PATH", audiopath + "");
                        RequestBody requestFile = RequestBody.create(MediaType.parse("audio/*"), audiopath);
                        multiBodyPartAudio = MultipartBody.Part.createFormData("audio", audiopath, requestFile);
                        break;
                    case REQUEST_VIDEO:
                        Uri selectedImage1 = data.getData();
                        String[] filePath1 = {MediaStore.Video.Media.DATA};
                        Cursor c1 = getContentResolver().query(selectedImage1, filePath1, null, null, null);
                        c1.moveToFirst();
                        int columnIndex1 = c1.getColumnIndex(filePath1[0]);

                        videopath = c1.getString(columnIndex1);
                        String filename1 = videopath.substring(videopath.lastIndexOf("/") + 1);
                        tvVideo.setText(filename1);
                        c1.close();
                        Bitmap thumbnail1 = (BitmapFactory.decodeFile(videopath));
                        Log.w("PATH", videopath + "");
                        break;

                    case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                        if (data != null) {
                            CropImage.ActivityResult result = CropImage.getActivityResult(data);
                            Uri resultUri = result.getUri();
                            setImage(resultUri);
                        }
                        break;
                    case FILE_REQUEST_CODE:
                        mediaFiles = data.<MediaFile>getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                        MediaFile mediaFile = mediaFiles.get(0);
                        if (mediaFile != null) {
                            imgDemo.setVisibility(View.GONE);
                            textViewWaterMark.setTextColor(Color.WHITE);
                            img_select.setImageURI(mediaFile.getUri());
                            watermarkColor = "";
                            relativeImageView.setVisibility(View.GONE);
                            img_select.setVisibility(View.VISIBLE);
                        } else {
                            Toast.makeText(this, "Image not selected", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case VIDEO_REQUEST_CODE:
                        File dir = new File("/storage/emulated/0/DCIM/TESTFOLDER");
                        try {
                            if (dir.mkdir()) {
                                System.out.println("Directory created");
                            } else {
                                System.out.println("Directory is not created");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mediaVideo = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                        MediaFile mediaFileVideo = mediaVideo.get(0);
                        TrimVideo.activity(String.valueOf(mediaFileVideo.getUri()))
                                .setDestination("/storage/emulated/0/DCIM/TESTFOLDER")
                                .setTrimType(TrimType.MIN_MAX_DURATION)
                                .setMinToMax(5, 180)  //seconds
                                .start(this);
                        if (mediaFileVideo.getMediaType() == MediaFile.TYPE_VIDEO) {
                            vdo_select.setVisibility(View.VISIBLE);
                            Glide.with(this)
                                    .load(mediaFileVideo.getUri())
                                    .into(vdo_select);
                        }
                        break;
                    case AUDIO_REQUEST_CODE:
                        mediaAudio = data.<MediaFile>getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                        MediaFile mediaFileAudio = mediaAudio.get(0);
                        Log.d("asdfas", mediaAudio.get(0).getPath());
                        Log.d("asdfas", String.valueOf(mediaAudio.size()));
                        if (mediaFileAudio.getMediaType() == MediaFile.TYPE_AUDIO) {
                            audio_select.setVisibility(View.VISIBLE);
                            Glide.with(this)
                                    .load(mediaFileAudio.getUri())
                                    .into(audio_select);
                        }
//                        button_AudioView.setVisibility(View.VISIBLE);
                        layoutUploads.setVisibility(View.VISIBLE);
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(requestCode == TAGUSER){
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Bundle bundle = data.getExtras();
                assert bundle != null;
                ArrayList<tagModel> thumbs=
                        (ArrayList<tagModel>)bundle.getSerializable("value");
                for (int i = 0; i< Objects.requireNonNull(thumbs).size(); i++){
                    TagUser_Id.add(thumbs.get(i).getId());
                }
                tvTag.setText(String.valueOf(thumbs.size())+" User Tagged");
                Log.e("thumbs",TagUser_Id.toString());
            }

        }

        if(requestCode == WATERMARKBUY){
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Bundle bundle = data.getExtras();
                assert bundle != null;
                String value = bundle.getString("value");
//                Log.e("thumbs",value);
                if(value.equals("success")){
                    checkSubscription();
                }
            }

        }
    }

    SubscriptionModel subscriptionModel;
    public void checkSubscription() {
        final Dialog dialog = new Dialog(WatermarkActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SubscriptionModel> call = apiInterface.checkSubscription("Bearer "+accessToken);
        call.enqueue(new Callback<SubscriptionModel>() {
            @Override
            public void onResponse(Call<SubscriptionModel> call, Response<SubscriptionModel> response) {
                dialog.dismiss();
                 subscriptionModel = response.body();
                if (subscriptionModel.getData().getSubscription()){
                    LayoutBuy.setVisibility(View.GONE);
                    editTextWatermark.setEnabled(true);
                }
                else {
                    LayoutBuy.setVisibility(View.VISIBLE);
                    editTextWatermark.setEnabled(false);
                }
            }
            @Override
            public void onFailure(Call<SubscriptionModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    private String getAudioPath(Uri uri) {
        String[] data = {MediaStore.Audio.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), uri, data, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    private void checkPermissionForProfile() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",

                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
//                Intent intent = new Intent(this, FilePickerActivity.class);
//                intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
//                        .setCheckPermission(true)
//                        .setShowFiles(false)
//                        .setShowImages(true)
//                        .setShowAudios(false)
//                        .setShowVideos(false)
//                        .setIgnoreNoMedia(false)
//                        .enableVideoCapture(false)
//                        .enableImageCapture(true)
//                        .setIgnoreHiddenFile(false)
//                        .setMaxSelection(5)
//                        .build());
//                startActivityForResult(intent, FILE_REQUEST_CODE);
                InstagramPicker a = new InstagramPicker(WatermarkActivity.this);
                a.show(16, 9, 5, addresses -> {
//              receive image addresses in here
                    imgDemo.setVisibility(View.GONE);
                    textViewWaterMark.setTextColor(Color.WHITE);
                    img_select.setImageURI(Uri.parse(addresses.get(0)));
                    watermarkColor = "";
                    relativeImageView.setVisibility(View.GONE);
                    img_select.setVisibility(View.VISIBLE);
                    fileToUpload1 = new MultipartBody.Part[addresses.size()];
                    if (addresses.size() > 0) {
                        for (int i = 0; i < addresses.size(); i++) {
//                            MediaFile mediaFile = addresses.get(i);
//                            if (mediaFile.getMediaType() == MediaFile.TYPE_IMAGE) {
                                String filePath = Utils.getUriRealPath(this, Uri.parse(addresses.get(i)));
                                File file = new File(filePath);
                                ProgressRequestBody fileBody = new ProgressRequestBody(file, "*/*", this);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
                                fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), fileBody);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
//                            fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), requestBody);
                                Log.e("imagelist", fileToUpload1[i].toString());
//                            }
                            multiBodyPart = fileToUpload1[0];
                        }

                    }
                });

            }
        } else {
//            Intent intent = new Intent(this, FilePickerActivity.class);
//            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
//                    .setCheckPermission(true)
//                    .setShowFiles(false)
//                    .setShowImages(true)
//                    .setShowAudios(false)
//                    .setShowVideos(false)
//                    .setIgnoreNoMedia(false)
//                    .enableVideoCapture(false)
//                    .enableImageCapture(true)
//                    .setIgnoreHiddenFile(false)
//                    .setMaxSelection(5)
//                    .build());
//            startActivityForResult(intent, FILE_REQUEST_CODE);
            InstagramPicker a = new InstagramPicker(WatermarkActivity.this);
            a.show(8, 8, 5, addresses -> {
//              receive image addresses in here
                Log.d("deskfs",addresses.size()+"");
                imgDemo.setVisibility(View.GONE);
                textViewWaterMark.setTextColor(Color.WHITE);
                img_select.setImageURI(Uri.parse(addresses.get(0)));
                watermarkColor = "";
                relativeImageView.setVisibility(View.GONE);
                img_select.setVisibility(View.VISIBLE);
                fileToUpload1 = new MultipartBody.Part[addresses.size()];
                if (addresses.size() > 0) {
                    for (int i = 0; i < addresses.size(); i++) {
//                            MediaFile mediaFile = addresses.get(i);
//                            if (mediaFile.getMediaType() == MediaFile.TYPE_IMAGE) {
                        String filePath = Utils.getUriRealPath(this, Uri.parse(addresses.get(i)));
                        File file = new File(filePath);
                        ProgressRequestBody fileBody = new ProgressRequestBody(file, "*/*", this);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
                        fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), fileBody);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
//                            fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), requestBody);
                        Log.e("imagelist", fileToUpload1[i].toString());
//                            }

                        multiBodyPart = fileToUpload1[0];
                    }

                }
            });
        }
    }


    private void checkPermissionForVideo() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",

                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                Intent intent = new Intent(this, FilePickerActivity.class);
                intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                        .setCheckPermission(true)
                        .setShowFiles(false)
                        .setShowImages(false)
                        .setShowAudios(false)
                        .setShowVideos(true)
                        .setIgnoreNoMedia(false)
                        .enableVideoCapture(false)
                        .enableImageCapture(false)
                        .setIgnoreHiddenFile(false)
                        .setMaxSelection(1)
                        .build());
                startActivityForResult(intent, VIDEO_REQUEST_CODE);
            }
        } else {
            Intent intent = new Intent(this, FilePickerActivity.class);
            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(false)
                    .setShowImages(false)
                    .setShowAudios(false)
                    .setShowVideos(true)
                    .setIgnoreNoMedia(false)
                    .enableVideoCapture(false)
                    .enableImageCapture(false)
                    .setIgnoreHiddenFile(false)
                    .setMaxSelection(1)
                    .build());
            startActivityForResult(intent, VIDEO_REQUEST_CODE);
        }
    }


    private void checkPermissionForAudio() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",

                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                Intent intent = new Intent(this, FilePickerActivity.class);
                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 3000);
                intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                        .setCheckPermission(true)
                        .setShowFiles(false)
                        .setShowImages(false)
                        .setShowAudios(true)
                        .setShowVideos(false)
                        .setIgnoreNoMedia(false)
                        .enableVideoCapture(false)
                        .enableImageCapture(false)
                        .setIgnoreHiddenFile(false)
                        .setMaxSelection(1)
                        .build());
                startActivityForResult(intent, AUDIO_REQUEST_CODE);

            }
        } else {
            Intent intent = new Intent(this, FilePickerActivity.class);
            intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 3000);
            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(false)
                    .setShowImages(false)
                    .setShowAudios(true)
                    .setShowVideos(false)
                    .setIgnoreNoMedia(false)
                    .enableVideoCapture(false)
                    .enableImageCapture(false)
                    .setIgnoreHiddenFile(false)
                    .setMaxSelection(1)
                    .build());
            startActivityForResult(intent, AUDIO_REQUEST_CODE);

        }
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Profile photo");
        String[] pictureDialogItems = {"Gallery", "Capture photo"};
        pictureDialog.setItems(pictureDialogItems, (dialog, which) -> {
            switch (which) {
                case 0:
                    Utils.choosePhotoFromGallery(this, REQUEST_GALLERY_CODE);
                    break;
                case 1:
                    photoURI = Utils.takePhotoFromCamera(this, REQUEST_CAMERA_CODE);
                    break;
            }
        });
        pictureDialog.setCancelable(true);
        pictureDialog.show();
    }


    private void setImage(Uri uri) {
        String filePath = Utils.getUriRealPath(this, uri);
        File file = new File(filePath);
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
//        editivProfile.setImageBitmap(bitmap);
//        try {
//            imgDemo.setVisibility(View.GONE);
//            textViewWaterMark.setTextColor(Color.WHITE);
//            img_select.setImageBitmap(Utils.getScaledBitmap(this, uri));
//            watermarkColor = "";
//            relativeImageView.setVisibility(View.GONE);
//            img_select.setVisibility(View.VISIBLE);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
        Log.d("File", "Filename " + file.getName());
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
        multiBodyPart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
    }


    private void showCurrencyDialog() {
        dialogCurrency = new Dialog(this);
        dialogCurrency.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCurrency.setCancelable(true);
        dialogCurrency.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogCurrency.findViewById(R.id.editText_price_search);
        RecyclerView rvCurrency = dialogCurrency.findViewById(R.id.rvCurrency);
        currencyDataList.clear();
        currencyDataList.addAll(currencyDataList2);
        selectCurrencyAdapter = new SelectCurrencyAdapter(this, currencyDataList, pos -> {
            tv_select_currency.setText(currencyDataList.get(pos).getCurrency());
            tv_select_currency.setTextColor(getResources().getColor(R.color.white));
            currencyData = currencyDataList.get(pos);
            currencyCode = currencyData.getCode();
            dialogCurrency.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(selectCurrencyAdapter);

        editText_price_search.addTextChangedListener(this);
        dialogCurrency.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogCurrency.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCurrency.show();
    }


    private void getSubCategoryFromServer(Integer id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getSubCategory(this, linearParent, id);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1", "size:" + categoryDataList1.size() + ",  " + categoryDataList1.toString());
        if (!categoryDataList1.isEmpty()) {
            for (CategoryData categoryData : categoryDataList1) {
                if (categoryData.getTitle() != null) {
                    listCategorySpinner.add(categoryData.getTitle());
                }
            }
            listCategorySpinner.add(0, "Select Category");
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, listCategorySpinner);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            spinnerCategory.setAdapter(arrayAdapter);
            categoryDataList.clear();
            categoryDataList.addAll(categoryDataList1);
            if (updateItem != null && !(categoryTitle.isEmpty())) {
                setCategoryInSpinner(categoryTitle);
            }
            if (isCategory) {
                CategoryData categoryData = getCategoryData(categoryDataList1);
                categoryDataList.clear();
                categoryDataList.add(categoryData);
                selectCategoryAdapter.notifyDataSetChanged();
            } else {
                categoryDataList.clear();
                categoryDataList.addAll(categoryDataList1);
                selectCategoryAdapter.notifyDataSetChanged();
            }
        }

        List<CurrencyData> currencyDataList1 = categoryCurrency.getData().getPrice();
        if (currencyDataList1 != null && !(currencyDataList1.isEmpty())) {
            currencyDataList.clear();
            currencyDataList2.clear();
            currencyDataList2.addAll(currencyDataList1);
            currencyDataList.addAll(currencyDataList1);
        }
        Log.e("Categorylist", "size:" + categoryDataList.size() + ",  " + categoryDataList.toString());
    }

    private void setUpdateDataForItem() {
        if (updateItem != null) {
            if (!(uploadType.isEmpty())) {
                if ("talent".equals(uploadType.toLowerCase())) {
                    rbImage.setEnabled(false);
                    rbVideo.setEnabled(false);
                    rbText.setEnabled(false);
                    rbAudio.setEnabled(false);
                    img_select.setEnabled(false);
                    cardViewImg.setEnabled(false);
                    cardViewVideo.setEnabled(false);
                    button_Edit.setEnabled(false);
                    mEditor.setEnabled(false);
                    button_AudioEdit.setEnabled(false);
                    button_UploadAudio.setEnabled(false);
                    layoutAddWatermark.setEnabled(false);
                    tvWatermark.setEnabled(false);
//                    layoutTag.setEnabled(false);

                    textViewWaterMark.setTextColor(Color.WHITE);
                    imgDemo.setVisibility(View.GONE);
                    HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = (HomeUserDetailsUploadTalent) updateItem;
                    if (homeUserDetailsUploadTalent != null) {
                        if (!(ApplicationClass.uploadImageListDataArrayList.isEmpty())) {
                            uploadImageListData.addAll(ApplicationClass.uploadImageListDataArrayList);
                            uploadImageListAdapter.notifyDataSetChanged();
                        }
                        if (!(ApplicationClass.uploadVideoListDataArrayList.isEmpty())) {
                            uploadVideoListData.addAll(ApplicationClass.uploadVideoListDataArrayList);
                            uploadVideoListAdapter.notifyDataSetChanged();
                        }
                        currencyCode = homeUserDetailsUploadTalent.getPricecode();
                        itemId = homeUserDetailsUploadTalent.getId();

                        if (homeUserDetailsUploadTalent.getCategoryId() != null) {
                            categoryId = homeUserDetailsUploadTalent.getCategoryId();
                            if (homeUserDetailsUploadTalent.getCategory() != null) {
                                categoryTitle = homeUserDetailsUploadTalent.getCategory().getTitle();
                            }
                        }

                        if (homeUserDetailsUploadTalent.getSubCategory() != null) {
                            subcategoryTitle = homeUserDetailsUploadTalent.getSubCategory().getTitle();
                        }
                        if (homeUserDetailsUploadTalent.getSubcategoryId() != null) {
                            subCategoryId = homeUserDetailsUploadTalent.getSubcategoryId();
                        }

                        if (homeUserDetailsUploadTalent.getTalant_type() != null) {
                            switch (homeUserDetailsUploadTalent.getTalant_type()) {
                                case "image":
                                    rbImage.setChecked(true);
                                    layoutImage.setVisibility(View.VISIBLE);
                                    cardViewAudio.setVisibility(View.GONE);
                                    layoutAudioButtons.setVisibility(View.GONE);
                                    layoutButtons.setVisibility(View.GONE);
                                    cardViewVideo.setVisibility(View.GONE);
                                    layoutAddWatermark.setVisibility(View.VISIBLE);
                                    et_text.setVisibility(View.GONE);
                                    String imageURL = Constant.ImageURL + homeUserDetailsUploadTalent.getPath() + "/" + homeUserDetailsUploadTalent.getCreateimage();
                                    Glide.with(this).load(imageURL).error(R.drawable.dummy_place)
                                            .error(R.drawable.dummy_place)
                                            .into(img_select);
                                    break;
                                case "audio":
                                    rbAudio.setChecked(true);
                                    layoutImage.setVisibility(View.GONE);
                                    cardViewAudio.setVisibility(View.VISIBLE);
                                    layoutAudioButtons.setVisibility(View.VISIBLE);
                                    button_UploadAudio.setVisibility(View.GONE);
                                    cardViewVideo.setVisibility(View.GONE);
                                    layoutButtons.setVisibility(View.GONE);
                                    et_text.setVisibility(View.GONE);
                                    layoutAddWatermark.setVisibility(View.GONE);
                                    try {
                                        mp.setDataSource(Constant.ImageURL + homeUserDetailsUploadTalent.getAudio());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                                case "video":
                                    rbVideo.setChecked(true);
                                    layoutImage.setVisibility(View.GONE);
                                    cardViewAudio.setVisibility(View.GONE);
                                    layoutAudioButtons.setVisibility(View.GONE);
                                    cardViewVideo.setVisibility(View.VISIBLE);
                                    cardViewVideo.setEnabled(false);
                                    layoutButtons.setVisibility(View.VISIBLE);
                                    layoutAddWatermark.setVisibility(View.GONE);
                                    et_text.setVisibility(View.GONE);
                                    if (homeUserDetailsUploadTalent.getVideothumbnail() != null) {
                                        String imageURL1 = Constant.ImageURL + "/" + homeUserDetailsUploadTalent.getVideothumbnail();
                                        Log.e("talentvdo",Constant.ImageURL + "/" + homeUserDetailsUploadTalent.getVideothumbnail());
                                        Glide.with(this).load(imageURL1).error(R.drawable.dummy_place)
                                                .error(R.drawable.dummy_place)
                                                .into(vdo_select);
                                        jz_video.setUp(Constant.ImageURL + homeUserDetailsUploadTalent.getVideo(), "", JzvdStd.SCREEN_NORMAL, JZMediaExo.class);
                                    }
                                    break;
                                case "textarea":
                                    layoutAddWatermark.setVisibility(View.GONE);
                                    rbText.setChecked(true);
                                    layoutImage.setVisibility(View.GONE);
                                    cardViewAudio.setVisibility(View.GONE);
                                    cardViewVideo.setVisibility(View.GONE);
                                    layoutButtons.setVisibility(View.GONE);
                                    layoutAudioButtons.setVisibility(View.GONE);
                                    img_select.setVisibility(View.GONE);
                                    textViewWaterMark.setVisibility(View.GONE);
                                    relativeImageView.setVisibility(View.GONE);
                                    et_text.setVisibility(View.GONE);
                                    mEditor.setVisibility(View.VISIBLE);
                                    if (homeUserDetailsUploadTalent.getTextarea() != null) {
                                        textViewWaterMark.setText(homeUserDetailsUploadTalent.getTextarea());
                                        mEditor.setHtml(homeUserDetailsUploadTalent.getTextarea());
                                    }
                                    Drawable drawable = relativeImageView.getBackground();
                                    drawable.setTint(Color.parseColor(homeUserDetailsUploadTalent.getFontcolor()));
                                    relativeImageView.setBackground(drawable);
                                    mEditor.setBackground(drawable);

                                    multiBodyPart = null;
                                    watermarkColor = homeUserDetailsUploadTalent.getFontcolor();
                                    break;
                            }
                        }
                        if (homeUserDetailsUploadTalent.getPricecode() != null) {
                            tv_select_currency.setText(homeUserDetailsUploadTalent.getPricecode());
                            tv_select_currency.setTextColor(getResources().getColor(R.color.white));
                        }
                        if (homeUserDetailsUploadTalent.getTextarea() != null) {
                            et_text.setText(homeUserDetailsUploadTalent.getTextarea());
                        }
                        if (homeUserDetailsUploadTalent.getPrice() != null) {
                            editText_price_range.setText(String.valueOf(homeUserDetailsUploadTalent.getPrice()));
                        }
                        if (homeUserDetailsUploadTalent.getAudio() != null) {
                            tvAudio.setText(String.valueOf(homeUserDetailsUploadTalent.getAudio()));
                        }
                        if (homeUserDetailsUploadTalent.getVideo() != null) {
                            tvVideo.setText(String.valueOf(homeUserDetailsUploadTalent.getVideo()));
                        }
                        if (homeUserDetailsUploadTalent.getDescription() != null) {
                            et_description.setText(homeUserDetailsUploadTalent.getDescription());
                        }
                    }
                }
            }
        }

    }

    private CategoryData getCategoryData(List<CategoryData> categoryDataList1) {
        for (CategoryData categoryData : categoryDataList1) {
            if (categoryData.getId() == categoryId) {
                return categoryData;
            }
        }
        return null;
    }

    private void submit() {
        if (categoryId == 0) {
            Toast.makeText(this, "Please select category", Toast.LENGTH_SHORT).show();
            return;
        }

        if (subCategoryId == 0) {
            Toast.makeText(this, "Please select sub talent", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(editText_price_range.getText().toString().trim())) {
            Toast.makeText(this, "Please enter price", Toast.LENGTH_SHORT).show();
            return;
        }
        if (editText_price_range.getText().toString().trim().equals("0")) {
            Toast.makeText(this, "Price should be more than 0", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(talent_type)) {
            Toast.makeText(this, "Please Upload Media", Toast.LENGTH_SHORT).show();
            return;
        }
        if (updateItem == null) {
            if (currencyData == null) {
                Toast.makeText(this, "Please select currency", Toast.LENGTH_SHORT).show();
                return;
            }
        }

//        if (TextUtils.isEmpty(et_description.getText().toString().trim())) {
//            Toast.makeText(this, "Please enter description", Toast.LENGTH_SHORT).show();
//            return;
//        }

        uploadTalents();
    }

    String text="";
    private void uploadTalents() {
        if (Utils.isDeviceOnline(this)) {
            progressDialog.show();
//            Log.e("percentage")
            if ("talent".equals(uploadType)) {

                if (mediaAudio.size() > 0) {
                    MediaFile mediaAudioa = mediaAudio.get(0);
                    if (mediaAudioa.getMediaType() == MediaFile.TYPE_AUDIO) {
                        audiopath = Utils.getUriRealPath(this, mediaAudioa.getUri());
                        String filePath = Utils.getUriRealPath(this, mediaAudioa.getUri());
                        File file = new File(filePath);

//                        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
                        ProgressRequestBody fileBody = new ProgressRequestBody(file, "*/*", this);
//                        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
                        multiBodyPartAudio = MultipartBody.Part.createFormData("audio", file.getName(), fileBody);
//                        multiBodyPartAudio = MultipartBody.Part.createFormData("audio", file.getName(), requestFile);
                    }
                }
                if(mEditor.getHtml()!=null){
                    text =  mEditor.getHtml();
                }
                if (updateItem == null) {
                    if (subCategoryId == 0) {
                        profileModel.uploadTalents(this, linearParent, tvWatermarkDemo.getText().toString().trim(), "", fileToUpload1,
                                String.valueOf(seekBar_textSize.getProgress()), watermarkColor, TagUser_Id,videothumbnail, multiBodyPart, multiBodyPartAudio, multiBodyPartVideo,
                                et_description.getText().toString().trim(), text,
                                editText_price_range.getText().toString().trim(), currencyData.getCode(), String.valueOf(categoryId),
                                String.valueOf(subCategoryId));
                    } else {
                        profileModel.uploadTalents(this, linearParent, tvWatermarkDemo.getText().toString().trim(), "", fileToUpload1,
                                String.valueOf(seekBar_textSize.getProgress()), watermarkColor, TagUser_Id,videothumbnail, multiBodyPart, multiBodyPartAudio, multiBodyPartVideo,
                                et_description.getText().toString().trim(), text,
                                editText_price_range.getText().toString().trim(), currencyData.getCode(), String.valueOf(categoryId),
                                String.valueOf(subCategoryId));
                    }
                } else {
                    if (subCategoryId == 0) {
                        profileModel.updateTalents(this, linearParent, tvWatermarkDemo.getText().toString().trim(), "", fileToUpload1,
                                String.valueOf(seekBar_textSize.getProgress()), watermarkColor,TagUser_Id, videothumbnail, multiBodyPart, multiBodyPartAudio,
                                multiBodyPartVideo, et_description.getText().toString().trim(),text,
                                editText_price_range.getText().toString().trim(), currencyCode, String.valueOf(categoryId),
                                "", String.valueOf(itemId));
                    } else {
                        profileModel.updateTalents(this, linearParent, tvWatermarkDemo.getText().toString().trim(), "", fileToUpload1,
                                String.valueOf(seekBar_textSize.getProgress()), watermarkColor,TagUser_Id, videothumbnail, multiBodyPart, multiBodyPartAudio,
                                multiBodyPartVideo, et_description.getText().toString().trim(),text,
                                editText_price_range.getText().toString().trim(), currencyCode, String.valueOf(categoryId),
                                String.valueOf(subCategoryId), String.valueOf(itemId));
                    }
                }
            }
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    LinearLayout LayoutBuy;
    EditText editTextWatermark;
    private void openSetWaterMarkDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_watermark);

         editTextWatermark = dialog.findViewById(R.id.editTextWatermark);
        TextView textView_Cancel = dialog.findViewById(R.id.textView_Cancel);
        Button button_addWatermark = dialog.findViewById(R.id.button_addWatermark);
        Button btnBuy = dialog.findViewById(R.id.btnBuy);
         LayoutBuy = dialog.findViewById(R.id.LayoutBuy);
         checkSubscription();
        btnBuy.setOnClickListener(v -> {
            Intent intent = new Intent(this,WatermarkPaymentActivity.class);
            startActivityForResult(intent,WATERMARKBUY);
        });
        if(!(tvWatermarkDemo.getText().toString()).isEmpty()){
            editTextWatermark.setText(tvWatermarkDemo.getText().toString());
        }
        button_addWatermark.setOnClickListener(view -> {
            relativeWaterMark.setVisibility(View.VISIBLE);
            tvWatermarkDemo.setVisibility(View.VISIBLE);
            if (subscriptionModel.getData().getSubscription()){
                tvWatermarkDemo.setText(editTextWatermark.getText().toString());
             //   Toast.makeText(this, editTextWatermark.getText().toString(), Toast.LENGTH_SHORT).show();
            }
            else tvWatermarkDemo.setText("Tiecoon");
            dialog.dismiss();
        });
        textView_Cancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

//    private void openPremiumWaterMarkDialog() {
//        Dialog dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.dialog_premium);
//
//        Button button_BuyPremium = dialog.findViewById(R.id.button_BuyPremium);
//        button_BuyPremium.setOnClickListener(view -> dialog.dismiss());
//
//        dialog.setCanceledOnTouchOutside(true);
//        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//    }

    public void getCategory() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getCategoryCurrency(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private List<String> getArrayList() {
        listColor.add("#3B6FFF");
        listColor.add("#8f348b");
        listColor.add("#af4a39");
        listColor.add("#af4a39");
        listColor.add("#b95959");
        listColor.add("#9c9c9c");
        listColor.add("#767676");
        listColor.add("#505050");
        listColor.add("#2a2a2a");
        return listColor;
    }

    @Override
    public void onProgressUpdate(int percentage) {
//        percentage_text = String.valueOf(percentage);
        Log.d("cgvhjkl", percentage + "");
        tvPercentage.setText(percentage + " %");
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {
        progressDialog.dismiss();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        filter(s.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        super.onBackPressed();
        mp.stop();
    }

    private void filter(String charText) {
        String newCharText = charText.toLowerCase(Locale.getDefault()).trim();
        currencyDataList.clear();
        if (charText.isEmpty()) {
            currencyDataList.addAll(currencyDataList2);
        } else {
            for (CurrencyData list : currencyDataList2) {
                if (list.getCountry().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                    currencyDataList.add(list);
                }
            }
        }
        selectCurrencyAdapter.notifyDataSetChanged();
    }


    private void setCategoryInSpinner(String title) {
        for (int i = 0; i < listCategorySpinner.size(); i++) {
            if (listCategorySpinner.get(i).equals(title)) {
                spinnerCategory.setSelection(i);
                break;
            }
        }
    }

    private void setSubCategoryInSubSpinner(String title) {
        for (int i = 0; i < listSubCategorySpinner.size(); i++) {
            if (listSubCategorySpinner.get(i).equals(title)) {
                spinnerSubCategory.setSelection(i);
                break;
            }
        }
    }

    private int getSubCategoryIdData(String data) {
        for (SubCategoryData subCategoryData : subCategoryDataList) {
            if (data.equals(subCategoryData.getTitle())) {
                return subCategoryData.getId();
            }
        }
        return 0;
    }

    private int getCategoryIdData(String data) {
        for (CategoryData categoryData : categoryDataList) {
            if (categoryData.getTitle() != null && data.equals(categoryData.getTitle())) {
                return categoryData.getId();
            }
        }
        return 0;
    }

    public interface UpdateSubcategory {
        void updateId();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
        mp.stop();
    }

}