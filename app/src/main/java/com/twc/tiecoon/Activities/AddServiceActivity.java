package com.twc.tiecoon.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gowtham.library.utils.TrimType;
import com.gowtham.library.utils.TrimVideo;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.twc.instagrampicker.InstagramPicker;
import com.twc.tiecoon.Adapters.SelectColorAdapter;
import com.twc.tiecoon.Adapters.SelectCurrencyAdapter;
import com.twc.tiecoon.Adapters.SelectDurationAdapter;
import com.twc.tiecoon.Adapters.ServiceSubCategoryAdapter;
import com.twc.tiecoon.Adapters.UploadProductImagesAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.CustomMedia.JZMediaExo;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.models.tagModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.CreateService;
import com.twc.tiecoon.network.response.CurrencyData;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.ServiceCategory;
import com.twc.tiecoon.network.response.UploadImageListData;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.ProgressRequestBody;
import com.twc.tiecoon.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Observable;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;
import jp.wasabeef.richeditor.RichEditor;
import okhttp3.MultipartBody;

public class AddServiceActivity extends AbstractFragmentActivity implements ProgressRequestBody.UploadCallbacks {

    private final String[] PROFILE_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int PROFILE_PERMISSION_REQUEST = 501;
    private boolean profilePermissionDeniedWithDontAskMeAgain = false;
    private final HomeModel homeModel = new HomeModel();
    RadioButton rbImage, rbVideo, rbAudio, rbText;
    LinearLayout layoutButtons, layoutAudioButtons,linearParent;
    Button button_AudioView, button_AudioEdit, button_UploadAudio,button_View,button_Edit;
    JzvdStd jz_video;
    LinearLayout layoutImage, layoutText,lineareditor;
    CardView cardViewVideo, cardViewAudio,cardViewImg;
    ImageView vdo_select, audio_select, ivPlayButton;
    EditText et_text,editText_productlocation,editText_price_range,et_description;
    String service_type="",service_name="";
    ImageView img_select,imgDemo;
    RecyclerView rvSubCategory,recyclerView_SelectColor;
    TextView tv_select_currency,tv_select_Duration,textViewWaterMark,tvTitle,tvTag;
    private List<String> listColor = new ArrayList<>();
    RelativeLayout relativeImageView;
    private String watermarkColor = "#3B6FFF";
    ServiceSubCategoryAdapter serviceSubCategoryAdapter;
    boolean is_category = false;
    MultipartBody.Part[] fileToUpload1;
    private int ProductsubCategoryId = 0;
    private final static int VIDEO_REQUEST_CODE = 4;
    List<MediaFile> mediaVideo = new ArrayList<>();
    List<MediaFile> mediaAudio = new ArrayList<>();
    Uri urivideo;
    String videopath="";
    private MultipartBody.Part videothumbnail = null;
    private MultipartBody.Part multiBodyPartVideo = null;
    private final static int AUDIO_REQUEST_CODE = 5;
    public static final int TAGUSER = 101;
    int service_id = 0;

    //    For Currency
    private final List<CurrencyData> currencyDataList = new ArrayList<>();
    private final List<CurrencyData> currencyDataList2 = new ArrayList<>();
    private SelectCurrencyAdapter selectCurrencyAdapter;
    private Dialog dialogCurrency;
    private CurrencyData currencyData = null;
    String currencyCode;

    //    For Duration
    private final List<String> durationList = new ArrayList<>();
    private final List<String> durationList1 = new ArrayList<>();
    private SelectDurationAdapter selectDurationAdapter;
    private Dialog dialogDuration;
    String duration="";

    LinearLayout layoutTag;
    ArrayList<String> TagUser_Id = new ArrayList<>();
    private MultipartBody.Part multiBodyPartAudio = null;
    private Object updateItem = null;
    private List<UploadImageListData> uploadImageListData = new ArrayList<>();
    private UploadProductImagesAdapter uploadImageListAdapter;
    RecyclerView recyclerViewUploadImageList;
    MediaPlayer mp = new MediaPlayer();
    RichEditor mEditor;
    ImageView action_bold;
    boolean bold = false;
    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);
        init();
    }

    public void init(){
        service_id = Integer.parseInt(getIntent().getStringExtra("service_id"));
        service_name = getIntent().getStringExtra("service_name");
        if (getIntent() != null && getIntent().hasExtra("updateItem")) {
            updateItem = getIntent().getParcelableExtra("updateItem");
            Log.e("updatedata",updateItem.toString());
        }

        rbImage = findViewById(R.id.rbImage);
        rbVideo = findViewById(R.id.rbVideo);
        rbAudio = findViewById(R.id.rbAudio);
        layoutButtons = findViewById(R.id.layoutButtons);
        layoutAudioButtons = findViewById(R.id.layoutAudioButtons);
        button_AudioView = findViewById(R.id.button_AudioView);
        button_AudioEdit = findViewById(R.id.button_AudioEdit);
        button_UploadAudio = findViewById(R.id.button_UploadAudio);
        rbText = findViewById(R.id.rbText);
        jz_video = findViewById(R.id.jz_video);
        ivPlayButton = findViewById(R.id.ivPlayButton);
        layoutImage = findViewById(R.id.layoutImage);
        layoutText = findViewById(R.id.layoutText);
        cardViewVideo = findViewById(R.id.cardViewVideo);
        cardViewAudio = findViewById(R.id.cardViewAudio);
        lineareditor = findViewById(R.id.lineareditor);
        et_text = findViewById(R.id.et_text);
        vdo_select = findViewById(R.id.vdo_select);
        audio_select = findViewById(R.id.audio_select);
        img_select = findViewById(R.id.img_select);
        rvSubCategory = findViewById(R.id.rvSubCategory);
        editText_productlocation = findViewById(R.id.editText_productlocation);
        tv_select_currency = findViewById(R.id.tv_select_currency);
        tv_select_Duration = findViewById(R.id.tv_select_Duration);
        editText_price_range = findViewById(R.id.editText_price_range);
        et_description = findViewById(R.id.et_description);
        textViewWaterMark = findViewById(R.id.textViewWaterMark);
        cardViewImg = findViewById(R.id.cardViewImg);
        imgDemo = findViewById(R.id.imgDemo);
        button_View = findViewById(R.id.button_View);
        button_Edit = findViewById(R.id.button_Edit);
        recyclerView_SelectColor = findViewById(R.id.recyclerView_SelectColor);
        relativeImageView = findViewById(R.id.relativeImageView);
        linearParent = findViewById(R.id.linearParent);
        tvTitle = findViewById(R.id.tvTitle);
        layoutTag = findViewById(R.id.layoutTag);
        tvTag = findViewById(R.id.tvTag);
        recyclerViewUploadImageList = findViewById(R.id.recyclerViewUploadImageList);

        mEditor = (RichEditor) findViewById(R.id.editor);
        mEditor.setEditorHeight(200);
        mEditor.setEditorFontSize(22);
        mEditor.setEditorFontColor(Color.BLUE);
        //mEditor.setEditorBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundColor(Color.BLUE);
        //mEditor.setBackgroundResource(R.drawable.bg);
        mEditor.setPadding(10, 10, 10, 10);
        //mEditor.setBackground("https://raw.githubusercontent.com/wasabeef/art/master/chip.jpg");
        mEditor.setPlaceholder("Insert text here...");
        //mEditor.setInputEnabled(false);

        mEditor.setOnTextChangeListener(new RichEditor.OnTextChangeListener() {
            @Override
            public void onTextChange(String text) {
                textViewWaterMark.setText(text);
            }
        });

        findViewById(R.id.action_undo).setOnClickListener(v -> mEditor.undo());

        findViewById(R.id.action_redo).setOnClickListener(v -> mEditor.redo());

        action_bold = findViewById(R.id.action_bold);
        action_bold.setOnClickListener(v -> {
//            if(!bold){
//                action_bold.setBackgroundColor(Color.BLUE);
                mEditor.setBold();
//                bold = true;
//            }
//            else {
//                action_bold.setBackgroundColor(Color.BLACK);
//                mEditor.removeView(v);
//                bold = false;
//            }
        });

        findViewById(R.id.action_italic).setOnClickListener(v -> mEditor.setItalic());

        findViewById(R.id.action_strikethrough).setOnClickListener(v -> mEditor.setStrikeThrough());

        findViewById(R.id.action_underline).setOnClickListener(v -> mEditor.setUnderline());

        findViewById(R.id.action_align_left).setOnClickListener(v -> mEditor.setAlignLeft());

        findViewById(R.id.action_align_center).setOnClickListener(v -> mEditor.setAlignCenter());

        findViewById(R.id.action_align_right).setOnClickListener(v -> mEditor.setAlignRight());

        findViewById(R.id.action_insert_bullets).setOnClickListener(v -> mEditor.setBullets());

        findViewById(R.id.action_insert_numbers).setOnClickListener(v -> mEditor.setNumbers());

        uploadImageListAdapter = new UploadProductImagesAdapter(this, uploadImageListData);
        recyclerViewUploadImageList.setAdapter(uploadImageListAdapter);
        findViewById(R.id.iv_cancel).setOnClickListener(v -> onBackPressed());

        durationList1.add("Per Service");
        durationList1.add("Per Hour");
        durationList1.add("Per Day");
        durationList1.add("Per Week");
        durationList1.add("Per Month");
        durationList1.add("Per Year");

        tvTitle.setText(service_name);
        SelectColorAdapter selectColorAdapter = new SelectColorAdapter(this, getArrayList(), pos -> {
            imgDemo.setVisibility(View.GONE);
            img_select.setVisibility(View.GONE);
//            relativeImageView.setVisibility(View.VISIBLE);
            Drawable drawable = relativeImageView.getBackground();
            drawable.setTint(Color.parseColor(listColor.get(pos)));
            relativeImageView.setBackground(drawable);
            mEditor.setBackground(drawable);
            watermarkColor = listColor.get(pos);
        });
        recyclerView_SelectColor.setAdapter(selectColorAdapter);

        rbImage.setOnCheckedChangeListener((compoundButton, b) -> {
            if (rbImage.isChecked()) {
                service_type = "image";
                audio_select.setImageBitmap(null);
                vdo_select.setImageBitmap(null);
                layoutImage.setVisibility(View.VISIBLE);
                layoutText.setVisibility(View.GONE);
                cardViewVideo.setVisibility(View.GONE);
                cardViewAudio.setVisibility(View.GONE);
                button_UploadAudio.setVisibility(View.GONE);
            }
        });

        rbVideo.setOnCheckedChangeListener((compoundButton, b) -> {
            if (rbVideo.isChecked()) {
                service_type = "video";
                audio_select.setImageBitmap(null);
                img_select.setImageBitmap(null);
                layoutImage.setVisibility(View.GONE);
                layoutText.setVisibility(View.GONE);
                cardViewVideo.setVisibility(View.VISIBLE);
                cardViewAudio.setVisibility(View.GONE);
                button_UploadAudio.setVisibility(View.GONE);
            }
        });

        rbAudio.setOnCheckedChangeListener((compoundButton, b) -> {
            if (rbAudio.isChecked()) {
                service_type = "audio";
                vdo_select.setImageBitmap(null);
                img_select.setImageBitmap(null);
                layoutImage.setVisibility(View.GONE);
                layoutText.setVisibility(View.GONE);
                cardViewVideo.setVisibility(View.GONE);
                cardViewAudio.setVisibility(View.VISIBLE);
                button_UploadAudio.setVisibility(View.VISIBLE);
            }
        });

        rbText.setOnCheckedChangeListener((compoundButton, b) -> {
            if (rbText.isChecked()) {
                service_type = "text";
                layoutImage.setVisibility(View.GONE);
                layoutText.setVisibility(View.VISIBLE);
                cardViewVideo.setVisibility(View.GONE);
                cardViewAudio.setVisibility(View.GONE);
                button_UploadAudio.setVisibility(View.GONE);
            }
        });

        et_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textViewWaterMark.setText(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        tv_select_currency.setOnClickListener(v -> showCurrencyDialog() );
        tv_select_Duration.setOnClickListener(v -> showDurationDialog() );
        cardViewImg.setOnClickListener(v -> checkPermissionForProfile());
        cardViewVideo.setOnClickListener(v -> checkPermissionForVideo());
        button_UploadAudio.setOnClickListener(v -> checkPermissionForAudio());

        layoutTag.setOnClickListener(v -> {
            Intent intent = new Intent(this,TagUserActivity.class);
            startActivityForResult(intent,TAGUSER);
        });

        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getServiceSubCategory(this, linearParent, String.valueOf(service_id));
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }

        findViewById(R.id.tvSave).setOnClickListener(v -> {
            if (service_id == 0) {
                Toast.makeText(this, "Please select category", Toast.LENGTH_SHORT).show();
                return;
            }

            if (ProductsubCategoryId == 0) {
                Toast.makeText(this, "Please select sub talent", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(editText_price_range.getText().toString().trim())) {
                Toast.makeText(this, "Please enter price", Toast.LENGTH_SHORT).show();
                return;
            }
            if (editText_price_range.getText().toString().trim().equals("0")) {
                Toast.makeText(this, "Price should be more than 0", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(currencyCode)) {
                Toast.makeText(this, "Please Select Currency", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(service_type)) {
                Toast.makeText(this, "Please Upload Media", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(editText_productlocation.getText().toString())) {
                Toast.makeText(this, "Please Enter Service Location", Toast.LENGTH_SHORT).show();
                return;
            }
            CreateService();
        });

        Button button_View = findViewById(R.id.button_View);
        Button button_Edit = findViewById(R.id.button_Edit);

        button_View.setOnClickListener(view -> {
            jz_video.setVisibility(View.VISIBLE);
            cardViewVideo.setVisibility(View.GONE);
            jz_video.startVideo();
        });

        button_Edit.setOnClickListener(view -> {
            checkPermissionForVideo();
        });

        button_AudioEdit.setOnClickListener(view -> {
            checkPermissionForAudio();
        });


        button_AudioView.setOnClickListener(v -> {
            // TODO Auto-generated method stub
            mp.setOnCompletionListener(MediaPlayer::stop);
//                mp.start();
            try {
                mp.prepare();
                mp.start();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("exception", e.toString());
            }
        });


        setUpdateDataForItem();
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    int itemId = 0;
    public void setUpdateDataForItem(){
        if(updateItem!=null){
            rbImage.setEnabled(false);
            rbVideo.setEnabled(false);
            rbText.setEnabled(false);
            rbAudio.setEnabled(false);
            img_select.setEnabled(false);
            cardViewImg.setEnabled(false);
            cardViewVideo.setEnabled(false);
            button_Edit.setEnabled(false);
            mEditor.setEnabled(false);
            button_AudioEdit.setEnabled(false);
            button_UploadAudio.setEnabled(false);
            editText_productlocation.setEnabled(false);
            layoutTag.setEnabled(false);
            HomeUserDetailsService homeUserDetailsProduct = (HomeUserDetailsService) updateItem;
            Log.e("methodcall",homeUserDetailsProduct.toString());

            if (!(ApplicationClass.uploadImageListDataArrayList2.isEmpty())) {
                uploadImageListData.addAll(ApplicationClass.uploadImageListDataArrayList2);
                uploadImageListAdapter.notifyDataSetChanged();
            }

            currencyCode = homeUserDetailsProduct.getPricecode();
            itemId = homeUserDetailsProduct.getId();
            if (homeUserDetailsProduct.getUpload_image().get(0).getCreateimage()!=null) {

                switch (homeUserDetailsProduct.getUpload_image().get(0).getCreateimage()) {
                    case "image":
                        rbImage.setChecked(true);
                        layoutImage.setVisibility(View.VISIBLE);
                        cardViewVideo.setVisibility(View.GONE);
                        imgDemo.setVisibility(View.GONE);
                        Log.e("imageurl",homeUserDetailsProduct.getUpload_image().get(0).getImage());
                        Glide.with(this).load(homeUserDetailsProduct.getUpload_image().get(0).getImage()).error(R.drawable.dummy_place)
                                .error(R.drawable.dummy_place)
                                .into(img_select);
                        break;
                    case "video":
                        rbVideo.setChecked(true);
                        layoutImage.setVisibility(View.GONE);
                        cardViewVideo.setVisibility(View.VISIBLE);
                        layoutButtons.setVisibility(View.VISIBLE);
                        cardViewVideo.setEnabled(false);
                        if (homeUserDetailsProduct.getUpload_image().get(0).getVideo_thumbnail() != null) {
                            String imageURL1 = homeUserDetailsProduct.getUpload_image().get(0).getVideo_thumbnail();
                          Log.e("thumbnail",imageURL1);
                            Glide.with(this).load(imageURL1).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(vdo_select);
                            jz_video.setUp( homeUserDetailsProduct.getUpload_image().get(0).getImage(), "", JzvdStd.SCREEN_NORMAL, JZMediaExo.class);
                        }
                        break;
                    case "audio":
                        rbAudio.setChecked(true);
                        layoutImage.setVisibility(View.GONE);
                        cardViewAudio.setVisibility(View.VISIBLE);
                        layoutAudioButtons.setVisibility(View.VISIBLE);
                        button_UploadAudio.setVisibility(View.GONE);
                        cardViewVideo.setVisibility(View.GONE);
                        layoutButtons.setVisibility(View.GONE);
                        et_text.setVisibility(View.GONE);
                        try {
                            mp.setDataSource(homeUserDetailsProduct.getUpload_image().get(0).getImage());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "text":
                        rbText.setChecked(true);
                        layoutImage.setVisibility(View.GONE);
                        cardViewAudio.setVisibility(View.GONE);
                        cardViewVideo.setVisibility(View.GONE);
                        layoutButtons.setVisibility(View.GONE);
                        layoutAudioButtons.setVisibility(View.GONE);
                        img_select.setVisibility(View.GONE);
                        textViewWaterMark.setVisibility(View.GONE);
                        relativeImageView.setVisibility(View.GONE);
                        et_text.setVisibility(View.GONE);
                        mEditor.setVisibility(View.VISIBLE);
                        if (homeUserDetailsProduct.getUpload_image().get(0).getImage() != null) {
                            textViewWaterMark.setText(homeUserDetailsProduct.getUpload_image().get(0).getImage());
                            et_text.setText(homeUserDetailsProduct.getUpload_image().get(0).getImage());
                            mEditor.setHtml(homeUserDetailsProduct.getUpload_image().get(0).getImage());
                        }
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//
//                        } else {
//                            tvWatermark.setText(Html.fromHtml(homeUserDetailsUploadTalent.getTextarea()));
//                        }
//                        Drawable drawable = mEditor.getBackground();
//                        drawable.setTint(Color.parseColor(homeUserDetailsProduct.getColor()));
                        Drawable drawable = relativeImageView.getBackground();
                        drawable.setTint(Color.parseColor(homeUserDetailsProduct.getColor()));
                        relativeImageView.setBackground(drawable);
                        mEditor.setBackground(drawable);
                        //mEditor.setBackgroundColor(Color.parseColor(homeUserDetailsProduct.getColor()));
                        watermarkColor = homeUserDetailsProduct.getColor();
                        break;

                }
            }

            if (homeUserDetailsProduct.getCategoryId() != null) {
                service_id = homeUserDetailsProduct.getCategoryId();
            }
            if (homeUserDetailsProduct.getSubcategoryId() != null) {
                ProductsubCategoryId = homeUserDetailsProduct.getSubcategoryId();
            }
            if (homeUserDetailsProduct.getLocation() != null) {
                editText_productlocation .setText(homeUserDetailsProduct.getLocation());
            }
            if (homeUserDetailsProduct.getPricecode() != null) {
                tv_select_currency.setText(homeUserDetailsProduct.getPricecode());
            }
          if (homeUserDetailsProduct.getDay() != null) {

              duration = homeUserDetailsProduct.getDay();
              switch (homeUserDetailsProduct.getDay()) {
                  case "1":
                      tv_select_Duration.setText("Per Week");
                      break;
                  case "2":
                      tv_select_Duration.setText("Per Month");
                      break;
                  case "0":
                      tv_select_Duration.setText("Per Day");
                      break;
              }

          }
            if (homeUserDetailsProduct.getPrice() != null) {
                editText_price_range.setText(String.valueOf(homeUserDetailsProduct.getPrice()));
            }
            if (homeUserDetailsProduct.getDescription() != null) {
                et_description.setText(homeUserDetailsProduct.getDescription());
            }
        }
    }

    private List<String> getArrayList() {
        listColor.add("#3B6FFF");
        listColor.add("#8f348b");
        listColor.add("#af4a39");
//        listColor.add("#af4a39");
        listColor.add("#b95959");
        listColor.add("#9c9c9c");
        listColor.add("#767676");
        listColor.add("#505050");
        listColor.add("#2a2a2a");
        return listColor;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof ServiceCategory) {
            ServiceCategory serviceCategory = (ServiceCategory) arg;
            serviceSubCategoryAdapter = new ServiceSubCategoryAdapter(this, service_id, is_category, serviceCategory.getData(), new ItemClickListenerExtraParam() {
                @Override
                public void itemClick(int pos, String type) {
                    ProductsubCategoryId = serviceCategory.getData().get(pos).getId();
                }
            });
            rvSubCategory.setAdapter(serviceSubCategoryAdapter);
            getCategory();
        }
        else if (arg instanceof CategoryCurrency) {
            setCategoryCurrency((CategoryCurrency) arg);

        }else if (arg instanceof CreateService.Data) {
            CreateService.Data createProduct = (CreateService.Data) arg;
            Toast.makeText(this, "Service Added Successfully", Toast.LENGTH_SHORT).show();
            Intent returnIntent = new Intent();
            returnIntent.putExtra("Product", "Padd");
            setResult(Activity.RESULT_OK, returnIntent);
            finish();

        }
    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1", "size:" + categoryDataList1.size() + ",  " + categoryDataList1.toString());

        List<CurrencyData> currencyDataList1 = categoryCurrency.getData().getPrice();
        if (currencyDataList1 != null && !(currencyDataList1.isEmpty())) {
            currencyDataList.clear();
            currencyDataList2.clear();
            currencyDataList2.addAll(currencyDataList1);
            currencyDataList.addAll(currencyDataList1);
        }
    }

    public void getCategory() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getCategoryCurrency(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void showCurrencyDialog() {
        dialogCurrency = new Dialog(this);
        dialogCurrency.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCurrency.setCancelable(true);
        dialogCurrency.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogCurrency.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Country Currency");
        RecyclerView rvCurrency = dialogCurrency.findViewById(R.id.rvCurrency);
        currencyDataList.clear();
        currencyDataList.addAll(currencyDataList2);
        selectCurrencyAdapter = new SelectCurrencyAdapter(this, currencyDataList, pos -> {
            tv_select_currency.setText(currencyDataList.get(pos).getCurrency());
            tv_select_currency.setTextColor(getResources().getColor(R.color.white));
            currencyData = currencyDataList.get(pos);
            currencyCode = currencyData.getCode();
            dialogCurrency.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(selectCurrencyAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                currencyDataList.clear();
                if (s.toString().isEmpty()) {
                    currencyDataList.addAll(currencyDataList2);
                } else {
                    for (CurrencyData list : currencyDataList2) {
                        if (list.getCountry().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            currencyDataList.add(list);
                        }
                    }
                }
                selectCurrencyAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogCurrency.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogCurrency.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCurrency.show();
    }
    private void showDurationDialog() {
        dialogDuration = new Dialog(this);
        dialogDuration.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDuration.setCancelable(true);
        dialogDuration.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogDuration.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Duration");
        RecyclerView rvCurrency = dialogDuration.findViewById(R.id.rvCurrency);
        durationList.add("Per Service");
        durationList.add("Per Hour");
        durationList.add("Per Day");
        durationList.add("Per Week");
        durationList.add("Per Month");
        durationList.add("Per Year");
        durationList.clear();
        durationList.addAll(durationList1);
        selectDurationAdapter = new SelectDurationAdapter(this, durationList, pos -> {
            tv_select_Duration.setText(durationList.get(pos));
            tv_select_Duration.setTextColor(getResources().getColor(R.color.white));
            switch (durationList.get(pos)) {
                case "Per Week":
                    duration = "1";
                    break;
                case "Per Month":
                    duration = "2";
                    break;
                case "Per Day":
                    duration = "0";
                    break;
           case "Per Service":
                    duration = "3";
                    break;
           case "Per Hour":
                    duration = "4";
                    break;
           case "Per Year":
                    duration = "5";
                    break;
            }
            dialogDuration.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(selectDurationAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                durationList.clear();
                if (s.toString().isEmpty()) {
                    durationList.addAll(durationList1);
                } else {
                    for (String list : durationList1) {
                        if (list.toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            durationList.add(list);
                        }
                    }
                }
                selectDurationAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogDuration.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogDuration.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogDuration.show();
    }

    private void checkPermissionForProfile() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",

                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                InstagramPicker a = new InstagramPicker(this);
                a.show(16, 9, 5, addresses -> {
                    imgDemo.setVisibility(View.GONE);
                    img_select.setImageURI(Uri.parse(addresses.get(0)));
                    img_select.setVisibility(View.VISIBLE);
                    fileToUpload1 = new MultipartBody.Part[addresses.size()];
                    if (addresses.size() > 0) {
                        for (int i = 0; i < addresses.size(); i++) {
//                            MediaFile mediaFile = addresses.get(i);
//                            if (mediaFile.getMediaType() == MediaFile.TYPE_IMAGE) {
                            String filePath = Utils.getUriRealPath(this, Uri.parse(addresses.get(i)));
                            File file = new File(filePath);
                            ProgressRequestBody fileBody = new ProgressRequestBody(file, "*/*", this);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
                            fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), fileBody);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
//                            fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), requestBody);
                            Log.e("imagelist", fileToUpload1[i].toString());
//                            }

//                            multiBodyPart = fileToUpload1[0];
                        }

                    }
                });

            }
        } else {
            InstagramPicker a = new InstagramPicker(this);
            a.show(8, 8, 5, addresses -> {
//              receive image addresses in here
                Log.d("deskfs",addresses.size()+"");
                imgDemo.setVisibility(View.GONE);
                img_select.setImageURI(Uri.parse(addresses.get(0)));
                img_select.setVisibility(View.VISIBLE);
                if (addresses.size() > 0) {
                    for (int i = 0; i < addresses.size(); i++) {
//                            MediaFile mediaFile = addresses.get(i);
//                            if (mediaFile.getMediaType() == MediaFile.TYPE_IMAGE) {
                        String filePath = Utils.getUriRealPath(this, Uri.parse(addresses.get(i)));
                        File file = new File(filePath);
                        ProgressRequestBody fileBody = new ProgressRequestBody(file, "*/*", this);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
                        fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), fileBody);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
//                            fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), requestBody);
                        Log.e("imagelist", fileToUpload1[i].toString());
//                            }

//                        multiBodyPart = fileToUpload1[0];
                    }

                }
            });
        }
    }

    private void checkPermissionForVideo() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",

                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                Intent intent = new Intent(this, FilePickerActivity.class);
                intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                        .setCheckPermission(true)
                        .setShowFiles(false)
                        .setShowImages(false)
                        .setShowAudios(false)
                        .setShowVideos(true)
                        .setIgnoreNoMedia(false)
                        .enableVideoCapture(false)
                        .enableImageCapture(false)
                        .setIgnoreHiddenFile(false)
                        .setMaxSelection(1)
                        .build());
                startActivityForResult(intent, VIDEO_REQUEST_CODE);
            }
        } else {
            Intent intent = new Intent(this, FilePickerActivity.class);
            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(false)
                    .setShowImages(false)
                    .setShowAudios(false)
                    .setShowVideos(true)
                    .setIgnoreNoMedia(false)
                    .enableVideoCapture(false)
                    .enableImageCapture(false)
                    .setIgnoreHiddenFile(false)
                    .setMaxSelection(1)
                    .build());
            startActivityForResult(intent, VIDEO_REQUEST_CODE);
        }
    }

    private void checkPermissionForAudio() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",

                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                Intent intent = new Intent(this, FilePickerActivity.class);
                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 3000);
                intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                        .setCheckPermission(true)
                        .setShowFiles(false)
                        .setShowImages(false)
                        .setShowAudios(true)
                        .setShowVideos(false)
                        .setIgnoreNoMedia(false)
                        .enableVideoCapture(false)
                        .enableImageCapture(false)
                        .setIgnoreHiddenFile(false)
                        .setMaxSelection(1)
                        .build());
                startActivityForResult(intent, AUDIO_REQUEST_CODE);
            }
        } else {
            Intent intent = new Intent(this, FilePickerActivity.class);
            intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 3000);
            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(false)
                    .setShowImages(false)
                    .setShowAudios(true)
                    .setShowVideos(false)
                    .setIgnoreNoMedia(false)
                    .enableVideoCapture(false)
                    .enableImageCapture(false)
                    .setIgnoreHiddenFile(false)
                    .setMaxSelection(1)
                    .build());
            startActivityForResult(intent, AUDIO_REQUEST_CODE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == TrimVideo.VIDEO_TRIMMER_REQ_CODE && data != null) {
                Uri uri = Uri.parse(TrimVideo.getTrimmedVideoPath(data));
                Log.d("2345678", "Trimmed path:: " + uri.getPath());
                cardViewVideo.setVisibility(View.VISIBLE);
                jz_video.setVisibility(View.GONE);
                if (mediaVideo.size() > 0) {
                    MediaFile mediaVideos = mediaVideo.get(0);
                    if (mediaVideos.getMediaType() == MediaFile.TYPE_VIDEO) {
                        String filePath = Utils.getUriRealPath(this, urivideo);
                        File file = new File(Objects.requireNonNull(uri.getPath()));
                        File file2 = new File(Objects.requireNonNull(uri.getPath()));
                        videopath = Utils.getUriRealPath(this, mediaVideos.getUri());
//                        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
                        ProgressRequestBody fileBody = new ProgressRequestBody(file, "*/*", this);
                        ProgressRequestBody fileBody1 = new ProgressRequestBody(file2, "*/*", this);
//                        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
//                        fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), fileBody);
                        Log.e("filesss", file.getName());
                        videothumbnail = MultipartBody.Part.createFormData("video_thumbnail", file.getName(), fileBody);
                        multiBodyPartVideo = MultipartBody.Part.createFormData("video", file2.getName(), fileBody1);
                    }
                }
            }
        }

        if (requestCode == VIDEO_REQUEST_CODE) {
            File dir = new File("/storage/emulated/0/DCIM/TESTFOLDER");
            try {
                if (dir.mkdir()) {
                    System.out.println("Directory created");
                } else {
                    System.out.println("Directory is not created");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            mediaVideo = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
            MediaFile mediaFileVideo = mediaVideo.get(0);
            TrimVideo.activity(String.valueOf(mediaFileVideo.getUri()))
                    .setDestination("/storage/emulated/0/DCIM/TESTFOLDER")
                    .setTrimType(TrimType.MIN_MAX_DURATION)
                    .setMinToMax(5, 180)  //seconds
                    .start(this);
            if (mediaFileVideo.getMediaType() == MediaFile.TYPE_VIDEO) {
                cardViewVideo.setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(mediaFileVideo.getUri())
                        .into(vdo_select);
            }
        }
        if(requestCode == AUDIO_REQUEST_CODE) {
            mediaAudio = data.<MediaFile>getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
            MediaFile mediaFileAudio = mediaAudio.get(0);
            Log.d("asdfas", mediaAudio.get(0).getPath());
            Log.d("asdfas", String.valueOf(mediaAudio.size()));
            if (mediaFileAudio.getMediaType() == MediaFile.TYPE_AUDIO) {
                audio_select.setVisibility(View.VISIBLE);
                Glide.with(this)
                        .load(mediaFileAudio.getUri())
                        .into(audio_select);
            }
        }
        if(requestCode == TAGUSER){
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Bundle bundle = data.getExtras();
                assert bundle != null;
                ArrayList<tagModel> thumbs=
                        (ArrayList<tagModel>)bundle.getSerializable("value");
                for (int i = 0; i< Objects.requireNonNull(thumbs).size(); i++){
                    TagUser_Id.add(thumbs.get(i).getId());
                }
                tvTag.setText(String.valueOf(thumbs.size())+" User Tagged");
                Log.e("thumbs",TagUser_Id.toString());
            }

        }
        if (resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(this, "No User Tagged", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
String text="";
    private void CreateService() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            if (mediaAudio.size() > 0) {
                MediaFile mediaAudioa = mediaAudio.get(0);
                if (mediaAudioa.getMediaType() == MediaFile.TYPE_AUDIO) {

                    String filePath = Utils.getUriRealPath(this, mediaAudioa.getUri());
                    File file = new File(filePath);
                    ProgressRequestBody fileBody = new ProgressRequestBody(file, "*/*", this);
                    multiBodyPartAudio = MultipartBody.Part.createFormData("audio", file.getName(), fileBody);
                }
            }
            if(mEditor.getHtml()!=null){
                text =  mEditor.getHtml();
            }

            if(updateItem!=null){
                homeModel.EditService(this, linearParent,fileToUpload1,multiBodyPartVideo,videothumbnail,multiBodyPartAudio, String.valueOf(service_id),
                       text,watermarkColor, String.valueOf(itemId),String.valueOf(ProductsubCategoryId),
                        currencyCode, editText_price_range.getText().toString(), et_description.getText().toString(),TagUser_Id,
                        editText_productlocation.getText().toString(),duration);
            }
            else {
                homeModel.createService(this, linearParent,fileToUpload1,multiBodyPartVideo,videothumbnail,multiBodyPartAudio, String.valueOf(service_id),
                        text,watermarkColor, String.valueOf(ProductsubCategoryId),
                        currencyCode, editText_price_range.getText().toString(), et_description.getText().toString(),TagUser_Id,
                        editText_productlocation.getText().toString(),duration);
            }
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }


    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        super.onBackPressed();
        mp.stop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
        mp.stop();
    }


}