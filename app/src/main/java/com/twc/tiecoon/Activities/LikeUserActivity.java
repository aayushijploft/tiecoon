package com.twc.tiecoon.Activities;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Adapters.LikeScreenAdapters2;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.LikeUserList;
import com.twc.tiecoon.network.response.LikeUserListData;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class LikeUserActivity extends AbstractFragmentActivity implements View.OnClickListener{
    RecyclerView recyclerViewLikeUser;
    private LinearLayout linearParent;
    private HomeModel homeModel = new HomeModel();
    private static final int USERUPDATE = 301;
    private ImageView imgJoinGroupOne, imgJoinGroupTwo, imgJoinGroupCreateNew;
    private List<LikeUserListData> homeUserDetailDataList = new ArrayList<>();
    private LikeScreenAdapters2 likeUserAdapter;
    private int currentPage = 1;
    //for pagination
    private boolean isLoading = true;
    private int pastVisibleItems, visibleItemCount, totalItemCount, previousTotal = 0;
    private int viewThreshold = 1;
    private int updatePosition;
    private boolean noMore = false;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_like_user);
        linearParent = findViewById(R.id.linearParent);
        ImageView ivBack = (ImageView) findViewById(R.id.ivBack);
        recyclerViewLikeUser = findViewById(R.id.recyclerViewLikeUser);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerViewLikeUser.setLayoutManager(linearLayoutManager);
        likeUserAdapter = new LikeScreenAdapters2(this, getSupportFragmentManager(), homeUserDetailDataList, new ItemClickListenerExtraParam() {
            @Override
            public void itemClick(int pos, String type) {
                updatePosition = pos;
                handleItemClick(type);
            }
        });
        Log.e("tokenuser", ApplicationClass.appPreferences.GetToken());
        recyclerViewLikeUser.setAdapter(likeUserAdapter);
        recyclerViewLikeUser.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (!noMore) {
                        if (totalItemCount > previousTotal) {
                            previousTotal = totalItemCount;
                        }
                        if ((totalItemCount - visibleItemCount) <= pastVisibleItems + viewThreshold) {
                            currentPage++;
                            getlikeUser();
                            isLoading = true;
                        }
                    }
                }
            }
        });
        ivBack.setOnClickListener(view -> startActivity(new Intent(this,UserHomeActivity.class)));
        getlikeUser();
    }


    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof LikeUserList) {
            LikeUserList likeUserList = (LikeUserList) o;
            List<LikeUserListData> likeUserListData1 = likeUserList.getData();
            if (likeUserListData1 != null && !(likeUserListData1.isEmpty())) {
                for (LikeUserListData likeUserListData : likeUserListData1) {
                        homeUserDetailDataList.add(likeUserListData);
                }
                likeUserAdapter.notifyDataSetChanged();
            } else {
                if (homeUserDetailDataList.isEmpty()) {
                    Toast.makeText(this, "No liked user found", Toast.LENGTH_SHORT).show();
                }
                noMore = true;
            }
        } else if (o instanceof HomeUserDetailData) {
            if (!(homeUserDetailDataList.isEmpty())) {
                homeUserDetailDataList.remove(updatePosition);
                likeUserAdapter.notifyItemRemoved(updatePosition);
                likeUserAdapter.notifyItemRangeChanged(updatePosition, homeUserDetailDataList.size());
            }
        }
    }

    private void getlikeUser() {
        if (Utils.isDeviceOnline(this)) {
            if (currentPage == 1) {
                Utils.showProDialog(this);
            }
            homeModel.getlikeUserData(this, linearParent, currentPage);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void setUserLike(int id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.setUserLike(this, linearParent, id);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void handleItemClick(String type) {
        switch (type) {
            case "like":
                setUserLike(homeUserDetailDataList.get(updatePosition).getUser().getId());
                break;
            case "chat":
                ProgressDialog progressDialog ;
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Connecting to chat, please wait...");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(true);
                progressDialog.show();
                ArrayList<Integer> occupantIdsList = new ArrayList<Integer>();
                occupantIdsList.add(Integer.valueOf(homeUserDetailDataList.get(updatePosition).getUser().getQuickbloxuser_id()));

                QBChatDialog dialog = new QBChatDialog();
                dialog.setType(QBDialogType.PRIVATE);
                dialog.setOccupantsIds(occupantIdsList);

                QBRestChatService.createChatDialog(dialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog result, Bundle params) {
                        Log.e("result",result.toString());
                        Log.e("qid",homeUserDetailDataList.get(updatePosition).getUser().getQuickbloxuser_id());
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        ChatActivity.startForResult(LikeUserActivity.this,101,homeUserDetailDataList.get(updatePosition).getId()+"",
                                result,homeUserDetailDataList.get(updatePosition).getUserDetils().getName());
                    }

                    @Override
                    public void onError(QBResponseException responseException) {
                        progressDialog.dismiss();
                        Toast.makeText(LikeUserActivity.this, "ERRROR", Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case "share":
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "C_Ony");
                    String message = "https://play.google.com/store/apps/details?id=" +"com.twc.c_ony"+ "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, message);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "collaborate":
                startActivity(new Intent(this, CollaborationActivity.class).
                        putExtra("userid", String.valueOf(homeUserDetailDataList.get(updatePosition).getId()))
                        .putExtra("qbid",homeUserDetailDataList.get(updatePosition).getUser().getQuickbloxuser_id())
                        .putExtra("name",homeUserDetailDataList.get(updatePosition).getUserDetils().getName())
                );
                break;
            default:
                Intent i = new Intent(this, OtherUserProfileActivity.class);
                i.putExtra("UserId",String.valueOf(homeUserDetailDataList.get(updatePosition).getLikeUserId()));
                startActivityForResult(i, USERUPDATE);
                break;
        }
    }

    private void openCollaborationDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_groupchat);

        LinearLayout linearJoinGroupOne = dialog.findViewById(R.id.linearJoinGroupOne);
        LinearLayout linearJoinGroupTwo = dialog.findViewById(R.id.linearJoinGroupTwo);
        LinearLayout linearNewGroup = dialog.findViewById(R.id.linearNewGroup);
        imgJoinGroupOne = dialog.findViewById(R.id.imgJoinGroupOne);
        imgJoinGroupTwo = dialog.findViewById(R.id.imgJoinGroupTwo);
        imgJoinGroupCreateNew = dialog.findViewById(R.id.imgJoinGroupCreateNew);

        EditText etGroupName = dialog.findViewById(R.id.etGroupName);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(view -> dialog.dismiss());
        linearJoinGroupOne.setOnClickListener(this);
        linearJoinGroupTwo.setOnClickListener(this);
        linearNewGroup.setOnClickListener(this);

        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linearJoinGroupOne:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_checked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_unchecked);
                break;
            case R.id.linearJoinGroupTwo:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_checked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_unchecked);
                break;
            case R.id.linearNewGroup:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_checked);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this,UserHomeActivity.class));
    }
}