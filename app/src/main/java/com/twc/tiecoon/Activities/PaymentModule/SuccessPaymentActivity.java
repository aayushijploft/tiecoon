package com.twc.tiecoon.Activities.PaymentModule;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.Activities.AmountDepositActivity;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Activities.TransactionActivity;
import com.twc.tiecoon.Activities.UserHomeActivity;
import com.twc.tiecoon.Activities.ViewAssignment;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.CardResponse;
import com.twc.tiecoon.network.response.PaymentSuccessModel;
import com.twc.tiecoon.network.response.ProjectAccept;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SuccessPaymentActivity extends AppCompatActivity {

    String url="";
    String id="",qbID="";
    String projectid="",user_id="",user_name="",adduser="";
    TextView tvtransactionid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_payment);

        tvtransactionid = findViewById(R.id.tvtransactionid);

        if(getIntent().hasExtra("url")){
            url = getIntent().getStringExtra("url");
        }
        if(getIntent().hasExtra("qbID")){
            qbID = getIntent().getStringExtra("qbID");
        }
        if(getIntent().hasExtra("projectid")){
            projectid = getIntent().getStringExtra("projectid");
        }
        if(getIntent().hasExtra("user_id")){
            user_id = getIntent().getStringExtra("user_id");
        }
        if(getIntent().hasExtra("user_name")){ 
            user_name = getIntent().getStringExtra("user_name");
        }
        if(getIntent().hasExtra("adduser")){
            adduser = getIntent().getStringExtra("adduser");
        }
        if(!url.equals("")){
//            String[] items = url.split("https://tiecoonapp.com/demo2/public/payment/1/");
//            for (String item : items)
//            {
//                    System.out.println("item = " + item);
//                id = item;
//                Log.e("id",id);
//            }
            try {
                Map<String, String> values = getUrlValues(url);
                id = values.get("id");
                    String transactionId = values.get("transactionId");
                tvtransactionid.setText("TRANSACTION ID: "+transactionId);
                Log.d("id", id);
                Log.d("transactionId", transactionId);
            } catch (UnsupportedEncodingException e) {
                Log.e("Error", e.getMessage());
            }
        }
        if(adduser.equals("")){
            VerifyPayment(id);
        }
        else acceptRequest(projectid,user_id);
    }

    public Map<String, String> getUrlValues(String url) throws UnsupportedEncodingException {
        int i = url.indexOf("?");
        Map<String, String> paramsMap = new HashMap<>();
        if (i > -1) {
            String searchURL = url.substring(url.indexOf("?") + 1);
            String params[] = searchURL.split("&");

            for (String param : params) {
                String temp[] = param.split("=");
                paramsMap.put(temp[0], java.net.URLDecoder.decode(temp[1], "UTF-8"));
            }
        }

        return paramsMap;
    }

    public void VerifyPayment(String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        Call<PaymentSuccessModel> call = apiInterface.paymentsuccess(id);

        call.enqueue(new Callback<PaymentSuccessModel>() {
            @Override
            public void onResponse(Call<PaymentSuccessModel> call, Response<PaymentSuccessModel> response) {
                PaymentSuccessModel user = response.body();
                Log.e("user",user.toString());
                finish();
                startActivity(new Intent(SuccessPaymentActivity.this, TransactionActivity.class));
            }

            @Override
            public void onFailure(Call<PaymentSuccessModel> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void acceptRequest(String id,String user_id) {
        final Dialog dialog = new Dialog(SuccessPaymentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProjectAccept> call = apiInterface.acceptProject("Bearer "+accessToken,id,user_id);

        call.enqueue(new Callback<ProjectAccept>() {
            @Override
            public void onResponse(Call<ProjectAccept> call, Response<ProjectAccept> response) {
                dialog.dismiss();
                ProjectAccept user = response.body();
//                ArrayList<Integer> occupantIdsList = new ArrayList<Integer>();
//                occupantIdsList.add(user.getData().getOccupants_id());
//
//                QBChatDialog qbChatDialog = new QBChatDialog();
//                qbChatDialog.setType(QBDialogType.PRIVATE);
//                qbChatDialog.setOccupantsIds(occupantIdsList);
//
//
//                QBRestChatService.createChatDialog(qbChatDialog).performAsync(new QBEntityCallback<QBChatDialog>() {
//                    @Override
//                    public void onSuccess(QBChatDialog result, Bundle params) {
//                        Log.e("result",result.toString());
//                        String message = String.valueOf(Html.fromHtml(user.getMessage()));
//                        finish();
//                        ChatActivity.startForResult(SuccessPaymentActivity.this,101, result,user_name,message);
//                    }
//
//                    @Override
//                    public void onError(QBResponseException responseException) {
//                        Toast.makeText(SuccessPaymentActivity.this, "ERRROR", Toast.LENGTH_SHORT).show();
//                    }
//                });
                //startActivity(new Intent(SuccessPaymentActivity.this,UserHomeActivity.class));
                finish();
                startActivity(new Intent(SuccessPaymentActivity.this, TransactionActivity.class));
            }

            @Override
            public void onFailure(Call<ProjectAccept> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, UserHomeActivity.class));
    }

    @Override
    public void onResume() {
        super.onResume();
        QBUser currentUser = AppPreferences.getInstance().getQbUser();
        if (currentUser != null && !QBChatService.getInstance().isLoggedIn()) {
            Log.d("TAG", "Resuming with Relogin");
            ChatHelper.getInstance().login(currentUser, new QBEntityCallback<QBUser>() {
                @Override
                public void onSuccess(QBUser qbUser, Bundle bundle) {
                    Log.d("TAG", "Relogin Successful");
                    reloginToChat();
                }

                @Override
                public void onError(QBResponseException e) {
                    Log.d("TAG", e.getMessage());
                }
            });
        } else {
            Log.d("TAG", "Resuming without Relogin to Chat");
            onResumeFinished();
        }
    }

    private void reloginToChat() {
        ChatHelper.getInstance().loginToChat(AppPreferences.getInstance().getQbUser(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                Log.d("TAG", "Relogin to Chat Successful");
                onResumeFinished();
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("TAG", "Relogin to Chat Error: " + e.getMessage());
                onResumeFinished();
            }
        });
    }

    public void onResumeFinished() {

    }

}