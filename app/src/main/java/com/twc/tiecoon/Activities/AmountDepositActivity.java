package com.twc.tiecoon.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.Activities.PaymentModule.FailPaymentActivity;
import com.twc.tiecoon.Activities.PaymentModule.SuccessPaymentActivity;
import com.twc.tiecoon.Adapters.SelectCurrencyAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.CurrencyData;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AmountDepositActivity extends AppCompatActivity {

    String user_id="",projectid="",user_name="",currency="";
    EditText editText_price_range;
    //   For Currency
    TextView tv_select_currency;
    String currencyCode;
    private SelectCurrencyAdapter selectCurrencyAdapter;
    private Dialog dialogCurrency;
    private CurrencyData currencyData = null;
    private final List<CurrencyData> currencyDataList = new ArrayList<>();
    private final List<CurrencyData> currencyDataList2 = new ArrayList<>();
    LinearLayout layoutAmount;
    WebView webview;
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount_deposit);
        user_id = getIntent().getStringExtra("user_id");
        projectid = getIntent().getStringExtra("projectid");
        
        user_name = getIntent().getStringExtra("user_name");
        currency = getIntent().getStringExtra("currency");
        Log.e("userid",user_id);
        Log.e("projectid",projectid);
        tv_select_currency = findViewById(R.id.tv_select_currency);
        editText_price_range = findViewById(R.id.editText_price_range);
        layoutAmount = findViewById(R.id.layoutAmount);
        webview = findViewById(R.id.webview);

        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());

        tv_select_currency.setOnClickListener(v ->  showCurrencyDialog());
        tv_select_currency.setText(currency);
        tv_select_currency.setEnabled(false);

        getCurrency();

        findViewById(R.id.btnDeposit).setOnClickListener(v -> {
            String Amount = editText_price_range.getText().toString();
            if (TextUtils.isEmpty(Amount)) {
                Toast.makeText(this, "Please enter Amount", Toast.LENGTH_SHORT).show();
                return;
            }
            layoutAmount.setVisibility(View.GONE);
            webview.setVisibility(View.VISIBLE);
//           2/120/25

            if(currency.equals("INR")){
                webview.loadUrl("https://tiecoonapp.com/demo2/api/v3/razorpay/project/amount/deposit/"+projectid+"/"+ Integer.parseInt(user_id) + "/" + Amount);
            }
            else webview.loadUrl("https://tiecoonapp.com/demo2/api/v3/project/amount/deposit/"+projectid+"/"+ Integer.parseInt(user_id) + "/" + Amount+ "/" + currency);
            Log.d("projectdeposit", "https://tiecoonapp.com/demo2/api/v3/razorpay/project/amount/deposit/"+projectid+"/"+ Integer.parseInt(user_id) + "/" + Amount);
        });

        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webview.setWebViewClient(new MyWebViewClient());
//        webview.setWebViewClient(new WebViewClient() {
//                                     @Override
//                                     public boolean shouldOverrideUrlLoading(WebView wView, String url) {
//        Log.d("vishnal", url);
//        if (url.contains("https://tiecoonapp.com/demo2/public/amount/deposit/success/1")) //check if that's a url you want to load internally
//         {
////                webview.setVisibility(View.GONE);
////                layoutAmount.setVisibility(View.VISIBLE);
////                acceptRequest(projectid,user_id);
//             finish();
//             startActivity(new Intent(AmountDepositActivity.this, SuccessPaymentActivity.class)
//             .putExtra("projectid",projectid)
//             .putExtra("user_id",user_id)
//             .putExtra("user_name",user_name)
//             .putExtra("url",url)
//             .putExtra("adduser","yes")
//             );

//          }
//            else if(url.equals("https://tiecoonapp.com/demo2/public/amount/deposit/success/0"))
//           {
//               startActivity(new Intent(AmountDepositActivity.this, FailPaymentActivity.class));
//          }
//              return true;
//             }

//        });
    }

    private  class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            Log.d("sdsgd", "_" + url + "");
            if (url.contains("https://tiecoonapp.com/demo2/amount/deposit/success/1")) //check if that's a url you want to load internally
            {
                finish();
                startActivity(new Intent(AmountDepositActivity.this, SuccessPaymentActivity.class)
                        .putExtra("projectid",projectid)
                        .putExtra("user_id",user_id)
                        .putExtra("user_name",user_name)
                        .putExtra("url",url)
                        .putExtra("adduser","yes")
                );
            }
            else if(url.equals("https://tiecoonapp.com/demo2/amount/deposit/success/0"))
            {
                startActivity(new Intent(AmountDepositActivity.this, FailPaymentActivity.class));
            }
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.d("sdsgd", "_" + url + "");

            if (url.contains("https://tiecoonapp.com/demo2/api/v3/razorpay/payment/1")) //check if that's a url you want to load internally
         {
             finish();
             startActivity(new Intent(AmountDepositActivity.this, SuccessPaymentActivity.class)
             .putExtra("projectid",projectid)
             .putExtra("user_id",user_id)
             .putExtra("user_name",user_name)
             .putExtra("url",url)
             .putExtra("adduser","yes")
             );

          }
            else if(url.equals("https://tiecoonapp.com/demo2/api/v3/razorpay/payment/0"))
           {
               startActivity(new Intent(AmountDepositActivity.this, FailPaymentActivity.class));
          }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }
    }


    private void showCurrencyDialog() {
        dialogCurrency = new Dialog(this);
        dialogCurrency.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCurrency.setCancelable(true);
        dialogCurrency.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogCurrency.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Country Currency");
        RecyclerView rvCurrency = dialogCurrency.findViewById(R.id.rvCurrency);
        currencyDataList.clear();
        currencyDataList.addAll(currencyDataList2);

        selectCurrencyAdapter = new SelectCurrencyAdapter(this, currencyDataList, pos -> {
            tv_select_currency.setText(currencyDataList.get(pos).getCurrency());
            tv_select_currency.setTextColor(getResources().getColor(R.color.white));
            currencyData = currencyDataList.get(pos);
            currencyCode = currencyData.getCode();
            dialogCurrency.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(selectCurrencyAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                currencyDataList.clear();
                if (s.toString().isEmpty()) {
                    currencyDataList.addAll(currencyDataList2);
                } else {
                    for (CurrencyData list : currencyDataList2) {
                        if (list.getCountry().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            currencyDataList.add(list);
                        }
                    }
                }
                selectCurrencyAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogCurrency.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogCurrency.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCurrency.show();
    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1", "size:" + categoryDataList1.size() + ",  " + categoryDataList1.toString());

        List<CurrencyData> currencyDataList1 = categoryCurrency.getData().getPrice();
        if (currencyDataList1 != null && !(currencyDataList1.isEmpty())) {
            currencyDataList.clear();
            currencyDataList2.clear();
            currencyDataList2.addAll(currencyDataList1);
            currencyDataList.addAll(currencyDataList1);
        }
    }

    public void getCurrency() {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CategoryCurrency> call = apiInterface.getCategoryCurrency("Bearer "+accessToken);

        call.enqueue(new Callback<CategoryCurrency>() {
            @Override
            public void onResponse(@NotNull Call<CategoryCurrency> call, @NotNull Response<CategoryCurrency> response) {
                CategoryCurrency user = response.body();
                Log.e("user",user.toString());
                setCategoryCurrency(user);
            }

            @Override
            public void onFailure(@NotNull Call<CategoryCurrency> call, @NotNull Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

}