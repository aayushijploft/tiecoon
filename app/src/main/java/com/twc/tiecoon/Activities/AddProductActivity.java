package com.twc.tiecoon.Activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gowtham.library.utils.TrimType;
import com.gowtham.library.utils.TrimVideo;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.twc.instagrampicker.InstagramPicker;
import com.twc.tiecoon.Adapters.ProductSubCategoryAdapter;
import com.twc.tiecoon.Adapters.SelectCurrencyAdapter;
import com.twc.tiecoon.Adapters.UploadImageListAdapter;
import com.twc.tiecoon.Adapters.UploadProductImagesAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.CustomMedia.JZMediaExo;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.models.tagModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.CreateProduct;
import com.twc.tiecoon.network.response.CurrencyData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.ProductCategory;
import com.twc.tiecoon.network.response.ProjectDetails;
import com.twc.tiecoon.network.response.SubCategory;
import com.twc.tiecoon.network.response.SubCategoryData;
import com.twc.tiecoon.network.response.UploadImageListData;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.ProgressRequestBody;
import com.twc.tiecoon.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Observable;

import cn.jzvd.JzvdStd;
import okhttp3.MultipartBody;

public class AddProductActivity extends AbstractFragmentActivity implements ProgressRequestBody.UploadCallbacks {

    private final HomeModel homeModel = new HomeModel();
    private final String[] PROFILE_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int PROFILE_PERMISSION_REQUEST = 501;
    private final static int VIDEO_REQUEST_CODE = 4;
    private boolean profilePermissionDeniedWithDontAskMeAgain = false;
    ImageView iv_cancel,img_select,imgDemo,vdo_select;
    RadioButton rbImage,rbVideo;
    LinearLayout layoutImage;
    CardView cardViewVideo,cardViewImg;
    String talent_type = "";
    Button tvSave;
    JzvdStd jz_video;
    List<MediaFile> mediaVideo = new ArrayList<>();
    Uri urivideo;
    String videopath="";
    private MultipartBody.Part videothumbnail = null;
    private MultipartBody.Part multiBodyPartVideo = null;
    RecyclerView  rvSubCategory;
    LinearLayout linearParent;
    String productcat_name="";
    int productcat_id=0;
    ProductSubCategoryAdapter productSubCategoryAdapter;
    boolean is_category = false;
    private int ProductsubCategoryId = 0;
    public static final int TAGUSER = 101;
    LinearLayout layoutButtons;

    //    For Currency
    private final List<CurrencyData> currencyDataList = new ArrayList<>();
    private final List<CurrencyData> currencyDataList2 = new ArrayList<>();
    private SelectCurrencyAdapter selectCurrencyAdapter;
    private Dialog dialogCurrency;
    private CurrencyData currencyData = null;
    TextView tv_select_currency,tvTitle,tvTag;
    String currencyCode;
    LinearLayout layoutTag;
    ArrayList<String> TagUser_Id = new ArrayList<>();
    MultipartBody.Part[] fileToUpload1;
    EditText editText_price_range,et_description,editText_productlocation;
    private Object updateItem = null;
    private UploadProductImagesAdapter uploadImageListAdapter;
    RecyclerView recyclerViewUploadImageList;
    private List<UploadImageListData> uploadImageListData = new ArrayList<>();
    Button button_Edit;
    Switch switchquantity;
    TextView tvquantity;
    EditText editText_quantity;
    String quantitystatus = "";
    EditText et_name,et_phone,et_pincode,et_address
            ,et_locality,et_city,et_state;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        init();
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    public void init(){
        productcat_id = Integer.parseInt(getIntent().getStringExtra("productcat_id"));

        productcat_name = getIntent().getStringExtra("productcat_name");
        if (getIntent() != null && getIntent().hasExtra("updateItem")) {
            updateItem = getIntent().getParcelableExtra("updateItem");
            Log.e("updatedata",updateItem.toString());
        }
        iv_cancel = findViewById(R.id.iv_cancel);
        rbImage = findViewById(R.id.rbImage);
        rbVideo = findViewById(R.id.rbVideo);
        layoutImage = findViewById(R.id.layoutImage);
        cardViewVideo = findViewById(R.id.cardViewVideo);
        cardViewImg = findViewById(R.id.cardViewImg);
        tvSave = findViewById(R.id.tvSave);
        img_select = findViewById(R.id.img_select);
        imgDemo = findViewById(R.id.imgDemo);
        vdo_select = findViewById(R.id.vdo_select);
        jz_video = findViewById(R.id.jz_video);
        rvSubCategory  = findViewById(R.id.rvSubCategory);
        linearParent  = findViewById(R.id.linearParent);
        tv_select_currency  = findViewById(R.id.tv_select_currency);
        layoutTag = findViewById(R.id.layoutTag);
        editText_price_range = findViewById(R.id.editText_price_range);
        et_description = findViewById(R.id.et_description);
        editText_productlocation = findViewById(R.id.editText_productlocation);
        tvTitle = findViewById(R.id.tvTitle);
        tvTag = findViewById(R.id.tvTag);
        layoutButtons = findViewById(R.id.layoutButtons);
        recyclerViewUploadImageList = findViewById(R.id.recyclerViewUploadImageList);
        switchquantity = findViewById(R.id.switchquantity);
        tvquantity = findViewById(R.id.tvquantity);
        editText_quantity = findViewById(R.id.editText_quantity);
        et_name = findViewById(R.id.et_name);
        et_phone = findViewById(R.id.et_phone);
        et_pincode = findViewById(R.id.et_pincode);
        et_address = findViewById(R.id.et_address);
        et_locality = findViewById(R.id.et_locality);
        et_city = findViewById(R.id.et_city);
        et_state = findViewById(R.id.et_state);

        uploadImageListAdapter = new UploadProductImagesAdapter(this, uploadImageListData);
        recyclerViewUploadImageList.setAdapter(uploadImageListAdapter);
        Button button_View = findViewById(R.id.button_View);
         button_Edit = findViewById(R.id.button_Edit);

        button_View.setOnClickListener(view -> {
            jz_video.setVisibility(View.VISIBLE);
            cardViewVideo.setVisibility(View.GONE);
            jz_video.startVideo();
        });

        switchquantity.setChecked(true);
        quantitystatus = "1";
        tvquantity.setText("AVAILABLE");
        editText_quantity.setVisibility(View.VISIBLE);
        switchquantity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(switchquantity.isChecked()){
                    quantitystatus = "1";
                    tvquantity.setText("AVAILABLE");
                    editText_quantity.setVisibility(View.VISIBLE);
                }
                else {
                    quantitystatus = "0";
                    tvquantity.setText("OUT OF STOCK");
                    editText_quantity.setVisibility(View.GONE);
                }
            }
        });

        button_Edit.setOnClickListener(view -> {
            checkPermissionForVideo();
        });
        iv_cancel.setOnClickListener(v -> onBackPressed());

        tvTitle.setText(productcat_name);
        rbImage.setOnCheckedChangeListener((compoundButton, b) -> {
            if (rbImage.isChecked()) {
                talent_type = "image";
                layoutImage.setVisibility(View.VISIBLE);
                cardViewVideo.setVisibility(View.GONE);
            }
        });
        rbVideo.setOnCheckedChangeListener((compoundButton, b) -> {
            if (rbVideo.isChecked()) {
                talent_type = "video";
                layoutImage.setVisibility(View.GONE);
                cardViewVideo.setVisibility(View.VISIBLE);
            }
        });

        tvSave.setOnClickListener(v -> {
            if (productcat_id == 0) {
                Toast.makeText(this, "Please select category", Toast.LENGTH_SHORT).show();
                return;
            }

            if (ProductsubCategoryId == 0) {
                Toast.makeText(this, "Please select sub category", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(editText_price_range.getText().toString().trim())) {
                Toast.makeText(this, "Please enter price", Toast.LENGTH_SHORT).show();
                return;
            }
            if (editText_price_range.getText().toString().trim().equals("0")) {
                Toast.makeText(this, "Price should be more than 0", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(currencyCode)) {
                Toast.makeText(this, "Please Select Currency", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(talent_type)) {
                Toast.makeText(this, "Please Upload Media", Toast.LENGTH_SHORT).show();
                return;
            }
            if(quantitystatus.equals("1")){
                if(TextUtils.isEmpty(editText_quantity.getText().toString())){
                    Toast.makeText(this, "Please Enter Quantity", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            if (TextUtils.isEmpty(et_name.getText().toString())) {
                Toast.makeText(this, "Please Enter Pickup Person Name", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(et_phone.getText().toString())) {
                Toast.makeText(this, "Please Enter Pickup Person Phone number", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(et_pincode.getText().toString())) {
                Toast.makeText(this, "Please Enter Pickup Pincode", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(et_address.getText().toString())) {
                Toast.makeText(this, "Please Enter Pickup Complete Address", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(et_locality.getText().toString())) {
                Toast.makeText(this, "Please Enter Pickup Locality", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(et_city.getText().toString())) {
                Toast.makeText(this, "Please Enter Pickup City", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(et_state.getText().toString())) {
                Toast.makeText(this, "Please Enter Pickup State", Toast.LENGTH_SHORT).show();
                return;
            }
            CreateProduct();
        });

        cardViewImg.setOnClickListener(v -> checkPermissionForProfile());
        cardViewVideo.setOnClickListener(v -> checkPermissionForVideo());

        tv_select_currency.setOnClickListener(v -> showCurrencyDialog() );

        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getProductSubCategory(this, linearParent, String.valueOf(productcat_id));
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }

        layoutTag.setOnClickListener(v -> {
            Intent intent = new Intent(this,TagUserActivity.class);
            startActivityForResult(intent,TAGUSER);
        });

        setUpdateDataForItem();

    }

    int itemId=0;


    public void setUpdateDataForItem(){
        if(updateItem!=null){
            rbImage.setEnabled(false);
            rbVideo.setEnabled(false);
//            rbText.setEnabled(false);
//            rbAudio.setEnabled(false);
            img_select.setEnabled(false);
            cardViewImg.setEnabled(false);
            cardViewVideo.setEnabled(false);
            button_Edit.setEnabled(false);
            editText_productlocation.setEnabled(false);
            layoutTag.setEnabled(false);
//            mEditor.setEnabled(false);
//            button_AudioEdit.setEnabled(false);
//            button_UploadAudio.setEnabled(false);
            HomeUserDetailsProduct homeUserDetailsProduct = (HomeUserDetailsProduct) updateItem;
            Log.e("methodcall",homeUserDetailsProduct.toString());

            if (!(ApplicationClass.uploadImageListDataArrayList1.isEmpty())) {
                uploadImageListData.addAll(ApplicationClass.uploadImageListDataArrayList1);
                uploadImageListAdapter.notifyDataSetChanged();
            }

            currencyCode = homeUserDetailsProduct.getPricecode();
            itemId = homeUserDetailsProduct.getId();
            if (homeUserDetailsProduct.getUpload_image().get(0).getCreateimage()!=null) {

                switch (homeUserDetailsProduct.getUpload_image().get(0).getCreateimage()) {
                    case "image":
                        rbImage.setChecked(true);
                        layoutImage.setVisibility(View.VISIBLE);
                        cardViewVideo.setVisibility(View.GONE);
                        imgDemo.setVisibility(View.GONE);
                        Log.e("imageurl",homeUserDetailsProduct.getUpload_image().get(0).getImage());
                        Glide.with(this).load(homeUserDetailsProduct.getUpload_image().get(0).getImage()).error(R.drawable.dummy_place)
                                .error(R.drawable.dummy_place)
                                .into(img_select);
//                        uploadImageListData.addAll(ApplicationClass.uploadImageListDataArrayList);
//                        uploadImageListAdapter.notifyDataSetChanged();

                    break;
                    case "video":

                        rbVideo.setChecked(true);
                        layoutImage.setVisibility(View.GONE);
                        cardViewVideo.setVisibility(View.VISIBLE);
                        layoutButtons.setVisibility(View.VISIBLE);
                        cardViewVideo.setEnabled(false);
                        if (homeUserDetailsProduct.getUpload_image().get(0).getVideo_thumbnail() != null) {
                            String imageURL1 = Constant.ImageURL + "/"+homeUserDetailsProduct.getUpload_image().get(0).getPath() +homeUserDetailsProduct.getUpload_image().get(0).getVideo_thumbnail();
                            Glide.with(this).load(imageURL1).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(vdo_select);
//                            Log.e("videourl",Constant.ImageURL +"/"+homeUserDetailsProduct.getUpload_image().get(0).getPath()+ homeUserDetailsProduct.getUpload_image().get(0).getVideo_thumbnail());
                            jz_video.setUp( homeUserDetailsProduct.getUpload_image().get(0).getImage(), "", JzvdStd.SCREEN_NORMAL, JZMediaExo.class);
                        }
                    break;
                }
            }

            if (homeUserDetailsProduct.getCategoryId() != null) {
                productcat_id = homeUserDetailsProduct.getCategoryId();
            }
          if (homeUserDetailsProduct.getSubcategoryId() != null) {
                ProductsubCategoryId = homeUserDetailsProduct.getSubcategoryId();
            }
            if (homeUserDetailsProduct.getLocation() != null) {
                editText_productlocation .setText(homeUserDetailsProduct.getLocation());
            }
            if (homeUserDetailsProduct.getPricecode() != null) {
                tv_select_currency.setText(homeUserDetailsProduct.getPricecode());
            }
            if (homeUserDetailsProduct.getPrice() != null) {
                editText_price_range.setText(String.valueOf(homeUserDetailsProduct.getPrice()));
            }
            if (homeUserDetailsProduct.getDescription() != null) {
                et_description.setText(homeUserDetailsProduct.getDescription());
            }
             if (homeUserDetailsProduct.getName() != null) {
                et_name.setText(homeUserDetailsProduct.getName());
            }
             if (homeUserDetailsProduct.getPhone() != null) {
                et_phone.setText(homeUserDetailsProduct.getPhone());
            }
             if (homeUserDetailsProduct.getPincode() != null) {
                et_pincode.setText(homeUserDetailsProduct.getPincode());
            }
             if (homeUserDetailsProduct.getAddress() != null) {
                et_address.setText(homeUserDetailsProduct.getAddress());
            }
             if (homeUserDetailsProduct.getTown() != null) {
                et_locality.setText(homeUserDetailsProduct.getTown());
            }
             if (homeUserDetailsProduct.getCity() != null) {
                et_city.setText(homeUserDetailsProduct.getCity());
            }
             if (homeUserDetailsProduct.getState() != null) {
                et_state.setText(homeUserDetailsProduct.getState());
            }
            if (homeUserDetailsProduct.getQty() != null) {
                switchquantity.setChecked(true);
                editText_quantity.setText(homeUserDetailsProduct.getQty());
            }
            if (homeUserDetailsProduct.getOutofstock() != null) {
                if(homeUserDetailsProduct.getOutofstock().equals("0")){
                    switchquantity.setChecked(false);
                    editText_quantity.setVisibility(View.GONE);
                }

            }
        }
    }

    private void checkPermissionForProfile() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",

                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                InstagramPicker a = new InstagramPicker(this);
                a.show(16, 9, 5, addresses -> {
                    imgDemo.setVisibility(View.GONE);
                    img_select.setImageURI(Uri.parse(addresses.get(0)));
                    img_select.setVisibility(View.VISIBLE);
                    fileToUpload1 = new MultipartBody.Part[addresses.size()];
                    if (addresses.size() > 0) {
                        for (int i = 0; i < addresses.size(); i++) {
//                            MediaFile mediaFile = addresses.get(i);
//                            if (mediaFile.getMediaType() == MediaFile.TYPE_IMAGE) {
                            String filePath = Utils.getUriRealPath(this, Uri.parse(addresses.get(i)));
                            File file = new File(filePath);
                            ProgressRequestBody fileBody = new ProgressRequestBody(file, "*/*", this);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
                            fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), fileBody);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
//                            fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), requestBody);
                            Log.e("imagelist", fileToUpload1[i].toString());
//                            }

//                            multiBodyPart = fileToUpload1[0];
                        }

                    }
                });

            }
        } else {
            InstagramPicker a = new InstagramPicker(this);
            a.show(8, 8, 5, addresses -> {
//              receive image addresses in here
                Log.d("deskfs",addresses.size()+"");
                imgDemo.setVisibility(View.GONE);
                img_select.setImageURI(Uri.parse(addresses.get(0)));
                img_select.setVisibility(View.VISIBLE);
                if (addresses.size() > 0) {
                    for (int i = 0; i < addresses.size(); i++) {
//                            MediaFile mediaFile = addresses.get(i);
//                            if (mediaFile.getMediaType() == MediaFile.TYPE_IMAGE) {
                        String filePath = Utils.getUriRealPath(this, Uri.parse(addresses.get(i)));
                        File file = new File(filePath);
                        ProgressRequestBody fileBody = new ProgressRequestBody(file, "*/*", this);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
                        fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), fileBody);
//                            RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
//                            fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), requestBody);
                        Log.e("imagelist", fileToUpload1[i].toString());
//                            }

//                        multiBodyPart = fileToUpload1[0];
                    }

                }
            });
        }
    }

    private void checkPermissionForVideo() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",

                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                Intent intent = new Intent(this, FilePickerActivity.class);
                intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                        .setCheckPermission(true)
                        .setShowFiles(false)
                        .setShowImages(false)
                        .setShowAudios(false)
                        .setShowVideos(true)
                        .setIgnoreNoMedia(false)
                        .enableVideoCapture(false)
                        .enableImageCapture(false)
                        .setIgnoreHiddenFile(false)
                        .setMaxSelection(1)
                        .build());
                startActivityForResult(intent, VIDEO_REQUEST_CODE);
            }
        } else {
            Intent intent = new Intent(this, FilePickerActivity.class);
            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(false)
                    .setShowImages(false)
                    .setShowAudios(false)
                    .setShowVideos(true)
                    .setIgnoreNoMedia(false)
                    .enableVideoCapture(false)
                    .enableImageCapture(false)
                    .setIgnoreHiddenFile(false)
                    .setMaxSelection(1)
                    .build());
            startActivityForResult(intent, VIDEO_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == TrimVideo.VIDEO_TRIMMER_REQ_CODE && data != null) {
                Uri uri = Uri.parse(TrimVideo.getTrimmedVideoPath(data));
                Log.d("2345678", "Trimmed path:: " + uri.getPath());
                cardViewVideo.setVisibility(View.VISIBLE);
                jz_video.setVisibility(View.GONE);
                if (mediaVideo.size() > 0) {
                    MediaFile mediaVideos = mediaVideo.get(0);
                    if (mediaVideos.getMediaType() == MediaFile.TYPE_VIDEO) {
                        String filePath = Utils.getUriRealPath(this, urivideo);
                        File file = new File(Objects.requireNonNull(uri.getPath()));
                        File file2 = new File(Objects.requireNonNull(uri.getPath()));
                        videopath = Utils.getUriRealPath(this, mediaVideos.getUri());
//                        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
                        ProgressRequestBody fileBody = new ProgressRequestBody(file, "*/*", this);
                        ProgressRequestBody fileBody1 = new ProgressRequestBody(file2, "*/*", this);
//                        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
//                        fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), fileBody);
                        Log.e("filesss", file.getName());
                        videothumbnail = MultipartBody.Part.createFormData("videothumbnail", file.getName(), fileBody);
                        multiBodyPartVideo = MultipartBody.Part.createFormData("video", file2.getName(), fileBody1);
                    }
                }
            }
        }

        switch (requestCode) {
            case VIDEO_REQUEST_CODE:
                File dir = new File("/storage/emulated/0/DCIM/TESTFOLDER");
                try {
                    if (dir.mkdir()) {
                        System.out.println("Directory created");
                    } else {
                        System.out.println("Directory is not created");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mediaVideo = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                MediaFile mediaFileVideo = mediaVideo.get(0);
                TrimVideo.activity(String.valueOf(mediaFileVideo.getUri()))
                        .setDestination("/storage/emulated/0/DCIM/TESTFOLDER")
                        .setTrimType(TrimType.MIN_MAX_DURATION)
                        .setMinToMax(5, 180)  //seconds
                        .start(this);
                if (mediaFileVideo.getMediaType() == MediaFile.TYPE_VIDEO) {
                    cardViewVideo.setVisibility(View.VISIBLE);
                    Glide.with(this)
                            .load(mediaFileVideo.getUri())
                            .into(vdo_select);
                }
                break;
//                case TAGUSER:
//                    assert data != null;
//                    Bundle bundle = data.getExtras();
//                    assert bundle != null;
//                    ArrayList<tagModel> thumbs=
//                            (ArrayList<tagModel>)bundle.getSerializable("value");
//                    for (int i = 0; i< Objects.requireNonNull(thumbs).size(); i++){
//                        TagUser_Id.add(thumbs.get(i).getId());
//                    }
//                    Log.e("thumbs",TagUser_Id.toString());
//                    tvTag.setText(thumbs.size()+ " user tagged");
//                    break;
        }
        if(requestCode == TAGUSER){
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Bundle bundle = data.getExtras();
                assert bundle != null;
                ArrayList<tagModel> thumbs=
                        (ArrayList<tagModel>)bundle.getSerializable("value");
                for (int i = 0; i< Objects.requireNonNull(thumbs).size(); i++){
                    TagUser_Id.add(thumbs.get(i).getId());
                }
                tvTag.setText(String.valueOf(thumbs.size())+" User Tagged");
                Log.e("thumbs",TagUser_Id.toString());
            }

        }
        if (resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(this, "No User Tagged", Toast.LENGTH_SHORT).show();
        }
        }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof ProductCategory) {
            ProductCategory productCategory = (ProductCategory) arg;
            productSubCategoryAdapter = new ProductSubCategoryAdapter(this, productcat_id, is_category, productCategory.getData(), new ItemClickListenerExtraParam() {
                @Override
                public void itemClick(int pos, String type) {
                    ProductsubCategoryId = productCategory.getData().get(pos).getId();
                }
            });
            rvSubCategory.setAdapter(productSubCategoryAdapter);
            getCategory();

        }
        else if (arg instanceof CategoryCurrency) {
            setCategoryCurrency((CategoryCurrency) arg);

        }
        else if (arg instanceof CreateProduct.Data) {
            CreateProduct.Data createProduct = (CreateProduct.Data) arg;
            Toast.makeText(this, "Product Added Successfully", Toast.LENGTH_SHORT).show();
            Intent returnIntent = new Intent();
            returnIntent.putExtra("Product", "Padd");
            setResult(Activity.RESULT_OK, returnIntent);
            finish();

        }

    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1", "size:" + categoryDataList1.size() + ",  " + categoryDataList1.toString());

        List<CurrencyData> currencyDataList1 = categoryCurrency.getData().getPrice();
        if (currencyDataList1 != null && !(currencyDataList1.isEmpty())) {
            currencyDataList.clear();
            currencyDataList2.clear();
            currencyDataList2.addAll(currencyDataList1);
            currencyDataList.addAll(currencyDataList1);
        }
    }

    public void getCategory() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getCategoryCurrency(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void showCurrencyDialog() {
        dialogCurrency = new Dialog(this);
        dialogCurrency.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCurrency.setCancelable(true);
        dialogCurrency.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogCurrency.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Country Currency");
        RecyclerView rvCurrency = dialogCurrency.findViewById(R.id.rvCurrency);
        currencyDataList.clear();
        currencyDataList.addAll(currencyDataList2);
        selectCurrencyAdapter = new SelectCurrencyAdapter(this, currencyDataList, pos -> {
            tv_select_currency.setText(currencyDataList.get(pos).getCurrency());
            tv_select_currency.setTextColor(getResources().getColor(R.color.white));
            currencyData = currencyDataList.get(pos);
            currencyCode = currencyData.getCode();
            dialogCurrency.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(selectCurrencyAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                currencyDataList.clear();
                if (s.toString().isEmpty()) {
                    currencyDataList.addAll(currencyDataList2);
                } else {
                    for (CurrencyData list : currencyDataList2) {
                        if (list.getCountry().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            currencyDataList.add(list);
                        }
                    }
                }
                selectCurrencyAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogCurrency.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogCurrency.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCurrency.show();
    }

    private void CreateProduct() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            if(updateItem!=null){
                homeModel.EditProduct(this, linearParent,fileToUpload1,multiBodyPartVideo,videothumbnail, String.valueOf(itemId), String.valueOf(productcat_id),
                        String.valueOf(ProductsubCategoryId),
                        currencyCode, editText_price_range.getText().toString(),editText_quantity.getText().toString(),quantitystatus, et_description.getText().toString(),TagUser_Id,
                        editText_productlocation.getText().toString(),et_name.getText().toString(),et_phone.getText().toString(),et_pincode.getText().toString()
                        ,et_address.getText().toString(),et_locality.getText().toString(),et_city.getText().toString(),et_state.getText().toString());
            }
            else {
                homeModel.createProduct(this, linearParent,fileToUpload1,multiBodyPartVideo,videothumbnail, String.valueOf(productcat_id), String.valueOf(ProductsubCategoryId),
                        currencyCode, editText_price_range.getText().toString(),editText_quantity.getText().toString(),quantitystatus,
                        et_description.getText().toString(),TagUser_Id,
                        editText_productlocation.getText().toString(),et_name.getText().toString(),et_phone.getText().toString(),et_pincode.getText().toString()
                        ,et_address.getText().toString(),et_locality.getText().toString(),et_city.getText().toString(),et_state.getText().toString());
            }


        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }


}