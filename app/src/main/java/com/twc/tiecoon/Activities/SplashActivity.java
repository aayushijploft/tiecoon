package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.quickblox.auth.session.QBSessionManager;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.google.firebase.iid.FirebaseInstanceId;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends AppCompatActivity {

    String Device_Key="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        printHashKey(this);
        getKey();
        new Handler().postDelayed(() -> {
            if (!(ApplicationClass.appPreferences.GetToken().isEmpty())) {
                if (ApplicationClass.appPreferences.getFirstTime()) {
                    QBUser currentUser = getUserFromSession();
                    if(currentUser != null){
                        loginToChat(currentUser);
                    }
                    else {
                         currentUser = AppPreferences.getInstance().getQbUser();
                        if (currentUser != null && !QBChatService.getInstance().isLoggedIn()) {
                            Log.d("TAG", "Resuming with Relogin");
                            ChatHelper.getInstance().login(currentUser, new QBEntityCallback<QBUser>() {
                                @Override
                                public void onSuccess(QBUser qbUser, Bundle bundle) {
                                    Log.d("TAG", "Relogin Successful");
                                    reloginToChat();
                                }
                                @Override
                                public void onError(QBResponseException e) {
                                    Log.d("TAG", e.getMessage());
                                }
                            });
                        } else {
                            Log.d("TAG", "Resuming without Relogin to Chat");
                            onResumeFinished();
                        }
                    }
                    Intent i = new Intent(SplashActivity.this, UserHomeActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    if(ApplicationClass.appPreferences.getisverfy()){
                        Intent i = new Intent(SplashActivity.this, ProfileFirst_Activity.class);
                        i.putExtra("first", true);
                        startActivity(i);
                        finish();
                    }
                    else {
                        Intent i = new Intent(SplashActivity.this, MobileScreen.class);
                        startActivity(i);
                        finish();
                    }
                }
            } else {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 2000);
    }

    private void reloginToChat() {
        ChatHelper.getInstance().loginToChat(AppPreferences.getInstance().getQbUser(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                Log.d("TAG", "Relogin to Chat Successful");
                onResumeFinished();
            }
            @Override
            public void onError(QBResponseException e) {
                Log.d("TAG", "Relogin to Chat Error: " + e.getMessage());
                onResumeFinished();
            }
        });
    }

    public void onResumeFinished() {

        // Need to Override onResumeFinished() method in nested classes if we need to handle returning from background in Activity
    }


    private void getKey () {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener
                (SplashActivity.this, instanceIdResult -> {
                    Device_Key = instanceIdResult.getToken();
                    Log.e("key", Device_Key);
                });
    }


    private QBUser getUserFromSession() {
        QBUser user = AppPreferences.getInstance().getQbUser();
        QBSessionManager qbSessionManager = QBSessionManager.getInstance();
        if (qbSessionManager.getSessionParameters() == null || user == null) {
            ChatHelper.getInstance().destroy();
            return null;
        }
        return user;
    }

    private void  loginToChat(final QBUser user) {

//        showProgressDialog(R.string.dlg_restoring_chat_session);

        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void result, Bundle bundle) {
                Log.v("Splash", "Chat login onSuccess()");
//                hideProgressDialog();
//                DialogsActivity.start(SplashActivity.this);
                finish();
            }

            @Override
            public void onError(QBResponseException e) {
                if (e.getMessage().equals("You have already logged in chat")) {
                    loginToChat(user);
                } else {
//                    hideProgressDialog();
                    Log.w("Splash", "Chat login onError(): " + e);
//                    showErrorSnackbar(R.string.error_recreate_session, e,
//                            new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    loginToChat(user);
//                                }
//                            });
                }
            }
        });
    }

    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.d("Hash Key: ", hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.d("printHashKey()", e.toString());
        } catch (Exception e) {
            Log.e("printHashKey()", e.toString());
        }
    }

}