package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.twc.tiecoon.Activities.PaymentModule.FailPaymentActivity;
import com.twc.tiecoon.Activities.PaymentModule.PaymentActivity;
import com.twc.tiecoon.Activities.PaymentModule.SuccessPaymentActivity;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.utils.Utils;

public class WatermarkPaymentActivity extends AppCompatActivity {

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watermark_payment);

        String user_id = ApplicationClass.appPreferences.getUserId()+"";
        WebView webView = findViewById(R.id.webview);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
//        Utils.showProDialog(this);
        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());
        webView.loadUrl("https://tiecoonapp.com/demo2/api/v3/razorpay/talent/watermark/"+ user_id);
        Log.d("watermarkurl", "https://tiecoonapp.com/demo2/api/v3/razorpay/talent/watermark/"+ user_id);
        webView.setWebViewClient(new MyWebViewClient());
    }

    private  class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.d("sdsgd", "_" + url + "");

            if (url.contains("https://tiecoonapp.com/demo2/api/v3/razorpay/payment/1")) //check if that's a url you want to load internally
            {
                Intent returnIntent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString("value", "success");
                returnIntent.putExtras(bundle);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
            else if(url.equals("https://tiecoonapp.com/demo2/api/v3/razorpay/payment/0"))
            {
                Intent returnIntent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putString("value", "fail");
                returnIntent.putExtras(bundle);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            /*if(dialog!=null){
                dialog.dismiss();
            }*/
        }
    }


}