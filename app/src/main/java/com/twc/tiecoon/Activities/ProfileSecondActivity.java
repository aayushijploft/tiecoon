package com.twc.tiecoon.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.Adapters.AddProductAdapters;
import com.twc.tiecoon.Adapters.AddServiceAdapters;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.HomeUserDetail;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class ProfileSecondActivity extends AbstractFragmentActivity implements View.OnClickListener {
    private ProfileModel profileModel = new ProfileModel();
    private LinearLayout linear_upload_products, linear_upload_services;
    public static final int UPLOAD_PRODUCTS = 101;
    public static final int UPLOAD_SERVICES = 102;
    private ArrayList<HomeUserDetailsProduct> uploadProductList = new ArrayList<>();
    private ArrayList<HomeUserDetailsService> uploadServiceList = new ArrayList<>();
    private AddProductAdapters addProductAdapters;
    private AddServiceAdapters addServiceAdapters;
    private LinearLayout linearParent;
    private RecyclerView recyclerView_upload_product, recyclerView_upload_services;
    private ImageView iv_menu;
    private TextView tvSkip;
    private Button btnNext;
    private int updateProductPos;
    private int updateServicePos;
    private boolean updateProduct = false;
    private boolean updateService = false;
    private boolean home = false;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_profile_second);
        home = getIntent().getBooleanExtra("home", false);
        initView();
    }

    private void initView() {
        ImageView iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_menu = (ImageView) findViewById(R.id.iv_menu);
        linearParent = findViewById(R.id.linearParent);
        linear_upload_products = findViewById(R.id.linear_upload_products);
        linear_upload_services = findViewById(R.id.linear_upload_services);
        recyclerView_upload_product = findViewById(R.id.recyclerView_upload_product);
        recyclerView_upload_services = findViewById(R.id.recyclerView_upload_services);
        btnNext = findViewById(R.id.btnNext);
        tvSkip = (TextView) findViewById(R.id.tvSkip);

        iv_back.setOnClickListener(this);
        iv_menu.setOnClickListener(this);
        linear_upload_products.setOnClickListener(this);
        linear_upload_services.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        tvSkip.setOnClickListener(this);
        if (home) {
            tvSkip.setVisibility(View.GONE);
            btnNext.setVisibility(View.GONE);
        }
        addProductAdapters = new AddProductAdapters(this, uploadProductList, new ItemClickListenerExtraThreeParam() {
            @Override
            public void itemClick(int pos, String subPos, String type) {
                updateProductPos = pos;
                if (type.equals("update")) {
                    updateProduct = true;
                    updateProduct(uploadProductList.get(pos));
                } else {
                    deleteProduct(uploadProductList.get(pos));
                }
            }
        });
                recyclerView_upload_product.setAdapter(addProductAdapters);
        addServiceAdapters = new AddServiceAdapters(this, uploadServiceList, new ItemClickListenerExtraThreeParam() {
            @Override
            public void itemClick(int pos, String subPos, String type) {
                updateServicePos = pos;
                if (type.equals("update")) {
                    updateService = true;
                    updateService(uploadServiceList.get(pos));
                } else {
                    deleteService(uploadServiceList.get(pos));
                }
            }
        });

                recyclerView_upload_services.setAdapter(addServiceAdapters);
        getUserDetails();
    }

    private void deleteService(HomeUserDetailsService homeUserDetailsService) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deleteServices(this, linearParent, homeUserDetailsService.getId());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void deleteProduct(HomeUserDetailsProduct homeUserDetailsProduct) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deleteProduct(this, linearParent, homeUserDetailsProduct.getId());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void updateProduct(HomeUserDetailsProduct homeUserDetailsProduct) {
        ApplicationClass.uploadImageListDataArrayList.clear();
        if (homeUserDetailsProduct.getUpload_image() != null && !(homeUserDetailsProduct.getUpload_image().isEmpty())) {
            ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsProduct.getUpload_image());
        }
        Intent i = new Intent(this, CreateProductServiceActivity.class);
        i.putExtra("type", "product");
        i.putExtra("updateItem", homeUserDetailsProduct);
        startActivityForResult(i, UPLOAD_PRODUCTS);
    }

    private void updateService(HomeUserDetailsService homeUserDetailsService) {
        ApplicationClass.uploadImageListDataArrayList.clear();
        if (homeUserDetailsService.getUpload_image() != null && !(homeUserDetailsService.getUpload_image().isEmpty())) {
            ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsService.getUpload_image());
        }
        Intent i = new Intent(this, CreateProductServiceActivity.class);
        i.putExtra("type", "service");
        i.putExtra("updateItem", homeUserDetailsService);
        startActivityForResult(i, UPLOAD_SERVICES);
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.linear_upload_products:
                updateProduct=false;
                Intent i = new Intent(this, CreateProductServiceActivity.class);
                i.putExtra("type", "product");
                startActivityForResult(i, UPLOAD_PRODUCTS);
                break;
            case R.id.linear_upload_services:
                updateService=false;
                Intent intent = new Intent(this, CreateProductServiceActivity.class);
                intent.putExtra("type", "service");
                startActivityForResult(intent, UPLOAD_SERVICES);
                break;
            case R.id.iv_menu:
                //to do
                break;
            case R.id.btnNext:
                startActivity(new Intent(this, CreateBrandsActivity.class));
                break;
            case R.id.tvSkip:
                startActivity(new Intent(this, CreateBrandsActivity.class));
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case UPLOAD_PRODUCTS:
                if (resultCode == RESULT_OK) {
                    if (!home) {
                        tvSkip.setVisibility(View.GONE);
                        btnNext.setVisibility(View.VISIBLE);
                    }
                    HomeUserDetailsProduct addProductData = (HomeUserDetailsProduct) data.getParcelableExtra("UploadData");
                    if (updateProduct) {
                        uploadProductList.set(updateProductPos,addProductData);
                        addProductAdapters.notifyItemChanged(updateProductPos);
                    } else {
                        uploadProductList.add(addProductData);
                        addProductAdapters.notifyDataSetChanged();
                    }
                }
                break;
            case UPLOAD_SERVICES:
                if (resultCode == RESULT_OK) {
                    if (!home) {
                        tvSkip.setVisibility(View.GONE);
                        btnNext.setVisibility(View.VISIBLE);
                    }
                    HomeUserDetailsService addProductData = (HomeUserDetailsService) data.getParcelableExtra("UploadData");
                    if (updateService) {
                        uploadServiceList.set(updateServicePos,addProductData);
                        addServiceAdapters.notifyItemChanged(updateServicePos);
                    } else {
                        uploadServiceList.add(addProductData);
                        addServiceAdapters.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof HomeUserDetail) {
            if (!home) {
                tvSkip.setVisibility(View.VISIBLE);
            }
            HomeUserDetail homeUserDetail = (HomeUserDetail) o;
            HomeUserDetailData homeUserDetailData = homeUserDetail.getData();
            if (homeUserDetailData != null) {
                List<HomeUserDetailsProduct> homeUserDetailsProductList1 = homeUserDetailData.getHomeUserDetailsProducts();
                if (homeUserDetailsProductList1 != null && !(homeUserDetailsProductList1.isEmpty())) {
                    if (!home) {
                        tvSkip.setVisibility(View.GONE);
                        btnNext.setVisibility(View.VISIBLE);
                    }
                    uploadProductList.addAll(homeUserDetailsProductList1);
                    addProductAdapters.notifyDataSetChanged();
                }
                List<HomeUserDetailsService> homeUserDetailsServiceList1 = homeUserDetailData.getHomeUserDetailsServices();
                if (homeUserDetailsServiceList1 != null && !(homeUserDetailsServiceList1.isEmpty())) {
                    if (!home) {
                        tvSkip.setVisibility(View.GONE);
                        btnNext.setVisibility(View.VISIBLE);
                    }
                    uploadServiceList.addAll(homeUserDetailsServiceList1);
                    addServiceAdapters.notifyDataSetChanged();
                }
            }
        }else if (o instanceof String) {
            String data = (String) o;
            if(data.equals("deleteProduct"))
            {
                uploadProductList.remove(updateProductPos);
                addProductAdapters.notifyItemRemoved(updateProductPos);
            }else if(data.equals("deleteServices"))
            {
                uploadServiceList.remove(updateServicePos);
                addServiceAdapters.notifyItemRemoved(updateServicePos);
            }
        }
    }

    private void getUserDetails() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getMyProfileDetails(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }
}