package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.twc.tiecoon.R;
import com.twc.tiecoon.utils.Utils;

public class PolicyActivity extends AppCompatActivity {

    String title = "";
    TextView tvTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);

        title = getIntent().getStringExtra("title");


        tvTitle  = findViewById(R.id.tvTitle);
        WebView webView = findViewById(R.id.webview);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        Utils.showProDialog(this);

        tvTitle.setText(title);
        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Utils.dismissProDialog();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(PolicyActivity.this, "Error:" + description, Toast.LENGTH_SHORT).show();

            }
        });
        switch (title) {
            case "Terms Of Service":
                webView.loadUrl("http://tiecoonapp.com/demo2/Privacy_tiecoon.html");
                break;
            case "Privacy Policy":
                webView.loadUrl("http://tiecoonapp.com/demo2/privacy_policy.html");
                break;
            case "Refund Policy":
                webView.loadUrl("http://tiecoonapp.com/demo2/refund_policy.html");
                break;
            case "Cookie Policy":
                webView.loadUrl("http://tiecoonapp.com/demo2/cookie_policy.html");
                break;
            case "Help Center":
                webView.loadUrl("http://tiecoonapp.com/demo2/Help_Center.html");
                break;
            case "Usage Guideline":
                webView.loadUrl("http://tiecoonapp.com/demo2/Usage_guidelines.html");
                break;
        }

    }
}