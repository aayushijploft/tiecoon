package com.twc.tiecoon.Activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.jsibbold.zoomage.ZoomageView;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.twc.tiecoon.Activities.PaymentModule.PaymentActivity;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Adapters.EnquiryReasonAdapter;
import com.twc.tiecoon.Adapters.ImageAdapter;
import com.twc.tiecoon.Adapters.ServiceImageAdapter;
import com.twc.tiecoon.Adapters.TagServiceUserAdapter;
import com.twc.tiecoon.Adapters.TagUserAdapter;
import com.twc.tiecoon.Adapters.UploadImageListAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.CustomMedia.JZMediaExo;
import com.twc.tiecoon.Helper.CircleIndicator;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.network.response.EnquiryModel;
import com.twc.tiecoon.network.response.FeedbackModel;
import com.twc.tiecoon.network.response.UploadImageListData;
import com.twc.tiecoon.network.response.ViewProduct;
import com.twc.tiecoon.network.response.ViewService;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.Utils;

import org.w3c.dom.Text;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Observable;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceDetailsActivity extends AbstractFragmentActivity {
    String service_id="",video_id="",qbID="";
    HomeModel homeModel = new HomeModel();
    ViewPager vpBanner;
    CircleIndicator ciBanner;
    FrameLayout layoutViewpager,layoutAudio,layoutVideo;
    JzvdStd jz_video;
    TextView tvTextArea,tvTitle,tvEnquire,tvViews,tv_Category,tv_SubCategory,tvDes,tv_price,tvWatermark,tv_Duration;
    ImageView ivThumbnail,ivPlayButton,imgLike;
    LinearLayout linear_like,linear_Tag,linear_share;
    Button btnPlayAudio,btnPauseAudio,btnResumeAudio,btnBuy;
    RelativeLayout relativeParent,relativeWaterMark;
    ZoomageView myZoomageView;
    private final List<UploadImageListData> uploadImageListData = new ArrayList<>();
    RecyclerView recyclerViewUploadImageList;
    private UploadImageListAdapter uploadImageListAdapter;
    MediaPlayer mp = new MediaPlayer();
    int length;
    RecyclerView rvTags;
    List<ViewService.Tags> tagsList = new ArrayList<>();
    EnquiryReasonAdapter enquiryReasonAdapter;
    RecyclerView rvReasons;
    RecyclerView recyclerView_Feedback;
    String Reason="";
    String user_name = "",userid="",ServiceIdGen="";
    TextView tvLikeCount,tvServiceId,tv_Location;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_service_details);
        service_id = getIntent().getStringExtra("service_id");
        qbID = getIntent().getStringExtra("qbID");
        init();
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    public void init(){
        relativeParent = findViewById(R.id.relativeParent);
        relativeWaterMark = findViewById(R.id.relativeWaterMark);
        vpBanner = findViewById(R.id.vpBanner);
        ciBanner = findViewById(R.id.ciBanner);
        layoutViewpager = findViewById(R.id.layoutViewpager);
        layoutAudio = findViewById(R.id.layoutAudio);
        tvTitle = findViewById(R.id.tvTitle);
        layoutVideo = findViewById(R.id.layoutVideo);
        tv_Location = findViewById(R.id.tv_Location);
        jz_video = findViewById(R.id.jz_video);
        tvTextArea = findViewById(R.id.tvTextArea);
        tvViews = findViewById(R.id.tvViews);
        tv_Category = findViewById(R.id.tv_Category);
        tv_SubCategory = findViewById(R.id.tv_SubCategory);
        tv_Duration = findViewById(R.id.tv_Duration);
        tvServiceId = findViewById(R.id.tvServiceId);
        tvDes = findViewById(R.id.tvDes);
        tvLikeCount = findViewById(R.id.tvLikeCount);
        tv_price = findViewById(R.id.tv_price);
        tvWatermark = findViewById(R.id.tvWatermark);
        ivThumbnail = findViewById(R.id.ivThumbnail);
        ivPlayButton = findViewById(R.id.ivPlayButton);
        imgLike = findViewById(R.id.imgLike);
        linear_like = findViewById(R.id.linear_like);
        linear_Tag = findViewById(R.id.linear_Tag);
        linear_share = findViewById(R.id.linear_share);
        btnPlayAudio = findViewById(R.id.btnPlayAudio);
        btnPauseAudio = findViewById(R.id.btnPauseAudio);
        btnResumeAudio = findViewById(R.id.btnResumeAudio);
        btnBuy = findViewById(R.id.btnBuy);
        myZoomageView = findViewById(R.id.myZoomageView);
        recyclerView_Feedback = findViewById(R.id.recyclerView_Feedback);
        rvTags = findViewById(R.id.rvTags);
        rvTags.setHasFixedSize(true);
        rvTags.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
        recyclerViewUploadImageList = findViewById(R.id.recyclerViewUploadImageList);
        uploadImageListAdapter = new UploadImageListAdapter(this, uploadImageListData);
        recyclerViewUploadImageList.setAdapter(uploadImageListAdapter);

        tvTitle.setOnClickListener(v -> {
            Intent i = new Intent(this, OtherUserProfileActivity.class);
            i.putExtra("UserId",userid);
            startActivity(i);
        });
        linear_Tag.setOnClickListener(v ->{
            if(tagsList.size()>0){
                rvTags.setVisibility(View.VISIBLE);
            } else Toast.makeText(this, "No Tagged Users", Toast.LENGTH_SHORT).show();

        });

        linear_share.setOnClickListener(v ->{
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Tiecoon");
                String message = "https://play.google.com/store/apps/details?id=" + "com.twc.tiecoon" + "\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, message);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } );


        findViewById(R.id.iv_close).setOnClickListener(v -> {
            if (Jzvd.backPress()) {
                return;
            }
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
            mp.stop();
        });

        btnPlayAudio.setOnClickListener(view -> {
            btnPauseAudio.setVisibility(View.VISIBLE);
            btnResumeAudio.setVisibility(View.GONE);
            btnPlayAudio.setVisibility(View.GONE);

            mp.setOnCompletionListener(mp -> {
//                Toast.makeText(this, "stop", Toast.LENGTH_SHORT).show();
                mp.stop();
                btnPauseAudio.setVisibility(View.GONE);
                btnPlayAudio.setVisibility(View.VISIBLE);
            });

            try {

                mp.prepare();
                mp.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        btnPauseAudio.setOnClickListener(view -> {
            btnPauseAudio.setVisibility(View.GONE);
            btnResumeAudio.setVisibility(View.VISIBLE);
            btnPlayAudio.setVisibility(View.GONE);

            mp.pause();
            length = mp.getCurrentPosition();
        });
        btnResumeAudio.setOnClickListener(view -> {
            btnPauseAudio.setVisibility(View.VISIBLE);
            btnResumeAudio.setVisibility(View.GONE);
            btnPlayAudio.setVisibility(View.GONE);

            if (!mp.isPlaying()) {
                mp.seekTo(length);
                mp.start();
            }
        });
        ivPlayButton.setOnClickListener(view -> {
            saveVideo();
            ivThumbnail.setVisibility(View.GONE);
            ivPlayButton.setVisibility(View.GONE);
            jz_video.startVideo();
        });

        ivThumbnail.setOnClickListener(view -> {
            saveVideo();
            ivThumbnail.setVisibility(View.GONE);
            ivPlayButton.setVisibility(View.GONE);
            jz_video.startVideo();
        });

            linear_like.setOnClickListener(v -> setItemLike(Integer.parseInt(service_id)));

        btnBuy.setOnClickListener(v -> {
            if(Utils.isDeviceOnline(this)){
                startActivity(new Intent(this, BuyServiceActivity.class).
                        putExtra("serviceid",service_id)
                        .putExtra("ServiceIdGen",ServiceIdGen) .putExtra("userid",userid));

            }
            else Toast.makeText(this, "No Internet", Toast.LENGTH_SHORT).show();
              });
        tvEnquire = findViewById(R.id.tvEnquire);
        tvEnquire.setOnClickListener(v ->EnquireFirst() );
        viewServiceDetails(service_id);
    }


    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof ViewService) {
            ViewService viewService = (ViewService) arg;
            if (!(ApplicationClass.uploadImageListDataArrayList1.isEmpty())) {
                uploadImageListData.addAll(ApplicationClass.uploadImageListDataArrayList1);
                uploadImageListAdapter.notifyDataSetChanged();
            }

            if (viewService.getData().getUser_id() == ApplicationClass.appPreferences.getUserId()) {
                btnBuy.setVisibility(View.GONE);
                tvEnquire.setVisibility(View.GONE);
                linear_like.setVisibility(View.VISIBLE);
                if (viewService.getData().getService_count() != null) {
                    tvLikeCount.setText(viewService.getData().getService_count());
                }
            }

            if (viewService.getData().getService_image().get(0).getProduct_type()!=null) {
                switch (viewService.getData().getService_image().get(0).getProduct_type()) {
                    case "image":
                        layoutVideo.setVisibility(View.GONE);
                        if (viewService.getData().getService_image().size() > 0) {
                            myZoomageView.setVisibility(View.GONE);
                            layoutViewpager.setVisibility(View.VISIBLE);
                            relativeWaterMark.setVisibility(View.GONE);
                            ServiceImageAdapter bannerAdapter = new ServiceImageAdapter(this, viewService.getData().getService_image());
                            vpBanner.setAdapter(bannerAdapter);
                            ciBanner.setViewPager(vpBanner);
                        } else {
                            myZoomageView.setVisibility(View.VISIBLE);
                            layoutViewpager.setVisibility(View.GONE);
                            Glide.with(this).load(viewService.getData().getService_image().get(0).getImage()).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(myZoomageView);
                        }
                        break;
                    case "video":
                        myZoomageView.setVisibility(View.GONE);
                        layoutViewpager.setVisibility(View.GONE);
                        layoutAudio.setVisibility(View.GONE);
                        layoutVideo.setVisibility(View.VISIBLE);
                        tvViews.setVisibility(View.VISIBLE);
                        tvTextArea.setVisibility(View.GONE);
                        relativeWaterMark.setVisibility(View.GONE);
                        if (viewService.getData().getService_image().get(0).getImage() != null) {
                            Glide.with(this).load(viewService.getData().getService_image().get(0).getImage()).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(ivThumbnail);
                        }
                        video_id = viewService.getData().getService_image().get(0).getId()+"";
                        jz_video.setUp(viewService.getData().getService_image().get(0).getImage(), "", JzvdStd.SCREEN_NORMAL, JZMediaExo.class);
//                        if (viewService.getData().getSeecount() != null) {
                            tvViews.setText(viewService.getData().getSeecount()+ " Views");
                        //}
                        break;
                    case "text":
                        myZoomageView.setVisibility(View.GONE);
                        layoutViewpager.setVisibility(View.GONE);
                        layoutAudio.setVisibility(View.GONE);
                        layoutVideo.setVisibility(View.GONE);
                        tvViews.setVisibility(View.GONE);
                        tvTextArea.setVisibility(View.VISIBLE);
                        relativeWaterMark.setVisibility(View.VISIBLE);
                        if (viewService.getData().getService_image().get(0).getImage() != null) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                tvWatermark.setText(Html.fromHtml(viewService.getData().getService_image().get(0).getImage(), Html.FROM_HTML_MODE_COMPACT));
                            } else {
                                tvWatermark.setText(Html.fromHtml(viewService.getData().getService_image().get(0).getImage()));
                            }
                        }

                        if (viewService.getData().getColor() != null && !(viewService.getData().getColor().isEmpty())) {
                            try {
                                relativeWaterMark.setBackgroundColor(Color.parseColor(viewService.getData().getColor()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    case "audio":
                        myZoomageView.setVisibility(View.GONE);
                        layoutViewpager.setVisibility(View.GONE);
                        layoutAudio.setVisibility(View.VISIBLE);
                        btnPlayAudio.setVisibility(View.VISIBLE);
                        layoutVideo.setVisibility(View.GONE);
                        try {
                            mp.setDataSource(viewService.getData().getService_image().get(0).getImage());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
            tagsList = viewService.getData().getTags();

            Log.e("taglist",tagsList.toString());
            TagServiceUserAdapter tagUserAdapter = new TagServiceUserAdapter(this, viewService.getData().getTags(), new ItemClickListener() {
                @Override
                public void itemClick(int pos) {
                    Intent i = new Intent(ServiceDetailsActivity.this, OtherUserProfileActivity.class);
                    i.putExtra("UserId", String.valueOf(viewService.getData().getTags().get(pos).getId()));
                    startActivity(i);
                }
            }) ;
            rvTags.setAdapter(tagUserAdapter);

            if (viewService.getData().getCategory_name() != null) {
                tv_Category.setText(viewService.getData().getCategory_name());
            }
            if (viewService.getData().getSubcategory_name() != null) {
                tv_SubCategory.setText(viewService.getData().getSubcategory_name());
            }
            if (viewService.getData().getDay() != null) {
                tv_Duration.setText(viewService.getData().getDay());
            }
            if (viewService.getData().getLocation() != null) {
                tv_Location.setText(viewService.getData().getLocation());
            }
            if (viewService.getData().getDescription() != null) {
                tvDes.setText(viewService.getData().getDescription());
            }
            if (viewService.getData().getServiceid() != null) {
                tvServiceId.setText("Service ID: S"+viewService.getData().getServiceid());
                ServiceIdGen = "S"+viewService.getData().getServiceid();

            }
            user_name = viewService.getData().getUser_name();
            userid = viewService.getData().getUser_id()+"";
            tvTitle.setText(user_name);
            if (viewService.getData().getPricecode() != null) {
                tv_price.setText(viewService.getData().getPricecode()+" "+viewService.getData().getPrice());
            }
            if (viewService.getData().isIs_like()) {
                imgLike.setImageResource(R.drawable.ic_like_solid);
            } else imgLike.setImageResource(R.drawable.ic_like);
            feedbackList(viewService.getData().getId()+"");
        }else if (arg instanceof DeleteTalents) {
            DeleteTalents deleteTalents = (DeleteTalents) arg;
            if (deleteTalents.getMessage().equals("Services unlike successfully")) {
                imgLike.setImageResource(R.drawable.ic_like);
            } else imgLike.setImageResource(R.drawable.ic_like_solid);
        }

        else if (arg instanceof EnquiryModel) {
            EnquiryModel enquiryModel = (EnquiryModel) arg;
            List<EnquiryModel.Data> enquiryModelList = enquiryModel.getData();
            enquiryReasonAdapter = new EnquiryReasonAdapter(this,enquiryModelList,pos -> {

                Reason = enquiryModelList.get(pos).getName()+"\n";
                Log.e("Reason",Reason);
            }) ;
            rvReasons.setAdapter(enquiryReasonAdapter);
        }
    }

    private void setItemLike(int id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.saveService(this, relativeParent,id);

        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }


    private void viewServiceDetails(String id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.viewServiceDetails(this, relativeParent,id);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void onBackPressed() {
        if (Jzvd.backPress()) {
            return;
        }
        super.onBackPressed();

        mp.stop();

    }
    @Override
    protected void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
        mp.stop();
    }

    private void EnquireFirst() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.service_enquire_layout);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        EditText editText =dialog.findViewById(R.id.editText);
        Button btnSave =dialog.findViewById(R.id.btnSave);
        Button btnCancle =dialog.findViewById(R.id.btnCancle);
       TextView et_dob = dialog.findViewById(R.id.et_dob);
//        CheckBox cb1 = dialog.findViewById(R.id.cb1);
//        CheckBox cb2 = dialog.findViewById(R.id.cb2);
//        String reasons = cb1.getText().toString()+"\n"+cb2.getText().toString();
        et_dob.setOnClickListener(view ->{
            editText.setText("");
                    openCalender(et_dob,editText);
        });
        rvReasons = dialog.findViewById(R.id.rvReasons);
        rvReasons.setHasFixedSize(true);
        rvReasons.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));



        imgCancel.setOnClickListener(v -> dialog.dismiss());
        btnCancle.setOnClickListener(v -> dialog.dismiss());
        getEnquiryList();
        btnSave.setOnClickListener(v -> {
            dialog.dismiss();
            ArrayList<Integer> occupantIdsList = new ArrayList<Integer>();
            occupantIdsList.add(Integer.valueOf(qbID));

            QBChatDialog qbChatDialog = new QBChatDialog();
            qbChatDialog.setType(QBDialogType.PRIVATE);
            qbChatDialog.setOccupantsIds(occupantIdsList);

            // or just use DialogUtils
            //QBChatDialog dialog = DialogUtils.buildPrivateDialog(recipientId);

            QBRestChatService.createChatDialog(qbChatDialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                @Override
                public void onSuccess(QBChatDialog result, Bundle params) {
                    Log.e("result",result.toString());
                    Log.e("ReasonList",ApplicationClass.ReasonList.toString());
                    StringBuilder string = new StringBuilder();
                    for (Object str : ApplicationClass.ReasonList ) {
                        string.append(str.toString()+"\n");
                    }
                    String singleString = string.toString();
                    ChatActivity.startForResult(ServiceDetailsActivity.this,101, result,user_name,editText.getText().toString(),singleString);
                }

                @Override
                public void onError(QBResponseException responseException) {
                    Toast.makeText(ServiceDetailsActivity.this, "ERRROR", Toast.LENGTH_SHORT).show();
                }
            });
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    public void openCalender (final TextView tvDate,EditText editText) {
        Calendar newCalendar = Calendar.getInstance();
        final SimpleDateFormat dateFormatter = new
                SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,R.style.DatePickerTheme,
                (view, year, monthOfYear, dayOfMonth) -> {
                    String select_date;
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    select_date = dateFormatter.format(newDate.getTime()) + "";
                    tvDate.setText(select_date);
                    editText.setText("I want to get this service on "+ tvDate.getText().toString());
                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    private void getEnquiryList() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getServiceEnquiryList(this, relativeParent);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    private void saveVideo() {
        if (Utils.isDeviceOnline(this)) {
            homeModel.viewServiceVideo(this, relativeParent, video_id);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        ApplicationClass.ReasonList.clear();
    }

    public void feedbackList(String id) {
        final Dialog dialog = new Dialog(ServiceDetailsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<FeedbackModel> call = apiInterface.feedbackList("Bearer "+accessToken,id,"service");
        call.enqueue(new Callback<FeedbackModel>() {
            @Override
            public void onResponse(Call<FeedbackModel> call, Response<FeedbackModel> response) {
                dialog.dismiss();
                FeedbackModel user = response.body();
                assert user != null;
                if(user.getData().size()>0){
                    recyclerView_Feedback.setVisibility(View.VISIBLE);
                    FeedbackAdapter feedbackAdapter = new FeedbackAdapter(ServiceDetailsActivity.this,user.getData(), pos -> {
                    });
                    recyclerView_Feedback.setAdapter(feedbackAdapter);
                }
                else recyclerView_Feedback.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<FeedbackModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


    public static class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {
        private Context context;
        private List<FeedbackModel.Data> list;
        private ItemClickListener itemClickListener;
        public FeedbackAdapter(Context context,List<FeedbackModel.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public FeedbackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.buying_items_user_rating_item_layout, parent, false);
            return new FeedbackAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(FeedbackAdapter.ViewHolder holder, final int position) {
            Glide.with(context).load(list.get(position).getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.imgProfile);
            holder.tv_UserName.setText(list.get(position).getUsername());
            holder.simpleRatingBar.setText(String.valueOf(list.get(position).getRating()));
            holder.textViewDescription.setText(list.get(position).getMassage());
            holder.rating_feedback.setRating(Float.parseFloat(String.valueOf(list.get(position).getRating())));

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CircleImageView imgProfile;
            TextView tv_UserName,simpleRatingBar,textViewDescription;
            RatingBar rating_feedback;
            public ViewHolder(View itemView) {
                super(itemView);
                imgProfile = itemView.findViewById(R.id.imgProfile);
                tv_UserName = itemView.findViewById(R.id.tv_UserName);
                simpleRatingBar = itemView.findViewById(R.id.simpleRatingBar);
                rating_feedback = itemView.findViewById(R.id.rating_feedback);
                textViewDescription = itemView.findViewById(R.id.textViewDescription);

            }
        }
    }

}
