package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.twc.tiecoon.Adapters.SavedPojectAdapter;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.DeleteStory;
import com.twc.tiecoon.network.response.SaveProjectList;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.Utils;

import java.util.Observable;

public class SavedProjectsActivity extends AbstractFragmentActivity {
    private final ProfileModel profileModel = new ProfileModel();
    LinearLayout linearParent;
    RecyclerView recyclerViewSavedProjects;
    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_projects);
        linearParent=findViewById(R.id.linearParent);
        recyclerViewSavedProjects=findViewById(R.id.recyclerViewSavedProjects);
        findViewById(R.id.ivBack).setOnClickListener(v -> onBackPressed());
        getSaveProjectList();
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void getSaveProjectList() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);

            profileModel.saveProjectList(this, linearParent);

        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void update(Observable observable, Object o) {

        if (o instanceof SaveProjectList) {
//            Toast.makeText(this, "Story Deleted Successfully", Toast.LENGTH_SHORT).show();
            SaveProjectList saveProjectList = (SaveProjectList) o;
            SavedPojectAdapter savedPojectAdapter = new SavedPojectAdapter(this, saveProjectList.getData(), new ItemClickListener() {
                @Override
                public void itemClick(int pos) {
                    Intent intent = new Intent(SavedProjectsActivity.this,ViewAssignment.class);
                    intent.putExtra("project_id", saveProjectList.getData().get(pos).getId()+"");
                    intent.putExtra("qbID",saveProjectList.getData().get(pos).getQbid());
                    intent.putExtra("name",saveProjectList.getData().get(pos).getProjectOwner());
                    startActivity(intent);
                }
            });
            recyclerViewSavedProjects.setAdapter(savedPojectAdapter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSaveProjectList();
    }
}