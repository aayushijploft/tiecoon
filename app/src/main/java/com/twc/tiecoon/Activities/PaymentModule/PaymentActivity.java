package com.twc.tiecoon.Activities.PaymentModule;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;

public class PaymentActivity extends AppCompatActivity {
    String id="",type="",qbID="",qty="",amount="",currency="";
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        id = getIntent().getStringExtra("id");
        type = getIntent().getStringExtra("type");
        qbID = getIntent().getStringExtra("qbID");
        amount = getIntent().getStringExtra("amount");
        currency = getIntent().getStringExtra("currency");
        Log.e("currency",currency);

        if(getIntent().hasExtra("qty")){
            qty = getIntent().getStringExtra("qty");
        }
        String user_id = ApplicationClass.appPreferences.getUserId()+"";
        WebView webView = findViewById(R.id.webview);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());
        if(type.equals("product")){
            if(currency.equals("INR")){
                webView.loadUrl("https://tiecoonapp.com/demo2/public/api/v3/razorpay/product/buy/"+ user_id + "/" + id+ "/" + amount+ "/" + qty);
            }
//            Product :- https://tiecoonapp.com/demo2/api/v3/product/buy/137/3/3
           else webView.loadUrl("https://tiecoonapp.com/demo2/api/v3/product/buy/"+ user_id + "/" + id+ "/" + qty);
        }
        else{
            if(currency.equals("INR")){
                webView.loadUrl("https://tiecoonapp.com/demo2/api/v3/razorpay/talent/buy/"+ user_id + "/" + id+ "/" + amount);
            }
//            Product :- https://tiecoonapp.com/demo2/api/v3/product/buy/137/3/3
            else webView.loadUrl("https://tiecoonapp.com/demo2/api/v3/buy/talent/"+ user_id + "/" + id);
          //  webView.loadUrl("https://tiecoonapp.com/demo2/api/v3/razorpay/talent/buy/"+ user_id + "/" + id+ "/" + amount);
        }
        Log.d("vishnal", "https://tiecoonapp.com/demo2/api/v3/razorpay/talent/buy/"+ user_id + "/" + id+ "/" + amount);
        Log.d("product", "https://tiecoonapp.com/demo2/public/api/v3/razorpay/product/buy/"+ user_id + "/" + id+ "/" + amount+ "/" + qty);
        if(type.equals("product")){
            webView.setWebViewClient(new MyWebViewClient());
        }
        else {
            webView.setWebViewClient(new MyWebViewClient1());
        }


    }

    private  class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            Log.d("sdsgd", "_" + url + "");
            if(url.equals("https://tiecoonapp.com/demo2/public/payment/address/2")){
                projectreject();
            }
            else if (url.contains("https://tiecoonapp.com/demo2/payment/1")) //check if that's a url you want to load internally
            {
                finish();
                startActivity(new Intent(PaymentActivity.this, SuccessPaymentActivity.class).putExtra("url",url)
                        .putExtra("qbID",qbID));
            }
            else if(url.equals("https://tiecoonapp.com/demo2/payment/0"))
            {
                startActivity(new Intent(PaymentActivity.this, FailPaymentActivity.class));
            }
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.d("sdsgd", "_" + url + "");
           /* dialog = new Dialog(PrivacyPolicy.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_login);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();*/
            if(url.equals("https://tiecoonapp.com/demo2/public/payment/address/2")){
                    projectreject();
                }
                else if (url.contains("https://tiecoonapp.com/demo2/public/api/v3/razorpay/payment/1")) //check if that's a url you want to load internally
                {
                    finish();
                    startActivity(new Intent(PaymentActivity.this, SuccessPaymentActivity.class).putExtra("url",url)
                            .putExtra("qbID",qbID));
                }
                else if(url.equals("https://tiecoonapp.com/demo2/public/api/v3/razorpay/payment/0"))
                {
                    startActivity(new Intent(PaymentActivity.this, FailPaymentActivity.class));
                }

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            /*if(dialog!=null){
                dialog.dismiss();
            }*/
        }
    }
    private  class MyWebViewClient1 extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            Log.d("sdsgd", "_" + url + "");
            if(url.equals("https://tiecoonapp.com/demo2/public/payment/address/2")){
                projectreject();
            }
            else if (url.contains("https://tiecoonapp.com/demo2/payment/1")) //check if that's a url you want to load internally
            {
                finish();
                startActivity(new Intent(PaymentActivity.this, SuccessPaymentActivity.class).putExtra("url",url)
                        .putExtra("qbID",qbID));
            }
            else if(url.equals("https://tiecoonapp.com/demo2/payment/0"))
            {
                startActivity(new Intent(PaymentActivity.this, FailPaymentActivity.class));
            }
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.d("sdsgd", "_" + url + "");
           /* dialog = new Dialog(PrivacyPolicy.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_login);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();*/
            if(url.equals("https://tiecoonapp.com/demo2/public/payment/address/2")){
                    projectreject();
                }
                else if (url.contains("https://tiecoonapp.com/demo2/public/api/v3/razorpay/payment/1")) //check if that's a url you want to load internally
                {
                    finish();
                    startActivity(new Intent(PaymentActivity.this, SuccessPaymentActivity.class).putExtra("url",url)
                            .putExtra("qbID",qbID));
                }
                else if(url.equals("https://tiecoonapp.com/demo2/public/api/v3/razorpay/payment/0"))
                {
                    startActivity(new Intent(PaymentActivity.this, FailPaymentActivity.class));
                }

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            /*if(dialog!=null){
                dialog.dismiss();
            }*/
        }
    }

    private void projectreject() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("Please add Delivery Address from side menu");
        btnRemove.setText("OK");
        tvTitle.setVisibility(View.GONE);
        btnCancle.setVisibility(View.GONE);
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
           onBackPressed();

        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

}
