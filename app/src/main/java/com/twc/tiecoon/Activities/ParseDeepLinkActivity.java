package com.twc.tiecoon.Activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;

@SuppressWarnings("ConstantConditions")
public class ParseDeepLinkActivity extends Activity {
  public static final String PROFILE_DEEP_LINK = "/share";
  //public static final String XMAS_DEEP_LINK = "/campaigns/xmas";
  
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    // Extrapolates the deeplink data
    Intent intent = getIntent();
    Uri deeplink = intent.getData();

    // Parse the deeplink and take the adequate action 
    if (deeplink != null) {

        parseDeepLink(deeplink);

    } else openPage("", "","");
  }
  
  private void parseDeepLink(Uri deeplink) {
    // The path of the deep link, e.g. '/products/123?coupon=save90'
    // The path of the deep link, e.g. '/share?id=90'
    String path = deeplink.getPath();

    Log.e("DL_PATH", path);

 if(path.startsWith(PROFILE_DEEP_LINK)) {
      String Share_Id = deeplink.getQueryParameter("id");
      String REF_CODE = deeplink.getQueryParameter("refer");

      //Log.e("DL_ID", deeplink.getLastPathSegment());
      Log.e("DL_coupon", Share_Id+"___"+REF_CODE);


      openPage("Profile", Share_Id,REF_CODE);

    }else{
      openPage("", "","");
    }
  }

  private void openPage(String Type, String Id, String REF_CODE) {


      if (!(ApplicationClass.appPreferences.GetToken().isEmpty())) {
          Intent intent = new Intent(this,
                  OtherUserProfileActivity.class);
          intent.putExtra("UserId", Id);
          intent.putExtra("flag", "1");

//            intent.putExtra("REF_CODE", REF_CODE);
          startActivity(intent);
      }
      else {
          Intent i = new Intent(this, LoginActivity.class);
          startActivity(i);
          finish();
      }
  }
}