package com.twc.tiecoon.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.LoginModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.NewRegisterModel;
import com.twc.tiecoon.network.response.OTPVerificationData;
import com.twc.tiecoon.network.response.OTPVerificationDataUser;
import com.twc.tiecoon.network.response.OTPVerificationSocialData;
import com.twc.tiecoon.network.response.VerifyPhoneModel;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;
import com.twc.tiecoon.utils.SmsBroadcastReceiver;
import com.twc.tiecoon.utils.Utils;
import com.hbb20.CountryCodePicker;

import java.util.Observable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MobileScreen  extends AbstractFragmentActivity{
    private final LoginModel loginModel = new LoginModel();
    private RelativeLayout relativeParent;
    private EditText etPhone;
    private EditText etEmail;
    CountryCodePicker ccp;
    String emailotp="";
    String phoneotp="";
    String phone="";
    String email="";
    String profileurl="";
    String userId="";
    String type="";
    String userid="";
    OtpTextView otp_view_email,otp_view_mobile;
    private TextView tvTimer,tvtitle,tvTimerMobile,tvtitleMobile,tvVerifyEmail,tvVerifyMobile,tvNext;
    LinearLayout layoutEmail,layoutMobile;
    private static final int REQ_USER_CONSENT = 200;
    SmsBroadcastReceiver smsBroadcastReceiver;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_mobile_screen);

        if(getIntent().hasExtra("phone")){
            phone=getIntent().getStringExtra("phone");
        }
        if(getIntent().hasExtra("email")){
            email=getIntent().getStringExtra("email");
        }

        if(getIntent().hasExtra("type")){
            type=getIntent().getStringExtra("type");
        }

        startSmsUserConsent();

        if(getIntent().hasExtra("userid")){
            userid=getIntent().getStringExtra("userid");
        }

        etPhone = findViewById(R.id.etPhone);
        etEmail = findViewById(R.id.etEmail);
        layoutMobile = findViewById(R.id.layoutMobile);
        layoutEmail = findViewById(R.id.layoutEmail);
        relativeParent= findViewById(R.id.relativeParent);
        ccp = findViewById(R.id.ccp);
        TextView tvVerify = findViewById(R.id.tvVerify);
        tvTimer = findViewById(R.id.tvTimer);
        tvtitle = findViewById(R.id.tvtitle);
        tvTimerMobile = findViewById(R.id.tvTimerMobile);
        tvtitleMobile = findViewById(R.id.tvtitleMobile);
        tvVerifyEmail = findViewById(R.id.tvVerifyEmail);
        tvVerifyMobile = findViewById(R.id.tvVerifyMobile);
        otp_view_email = findViewById(R.id.otp_view_email);
        otp_view_mobile = findViewById(R.id.otp_view_mobile);
        tvNext = findViewById(R.id.tvNext);

        tvtitleMobile.setOnClickListener(v -> {
            String phone = etPhone.getText().toString();
            if(TextUtils.isEmpty(phone)){
                Toast.makeText(this, "Please Enter Phone", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!(Utils.isValidMobile(phone))) {
                Toast.makeText(this, "Phone not valid", Toast.LENGTH_SHORT).show();
                return;
            }
            sendPhoneOTP("phone",phone);
        });

        tvtitle.setOnClickListener(v -> {
            String email = etEmail.getText().toString();
            if(TextUtils.isEmpty(email)){
                Toast.makeText(this, "Please Enter Email", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!(Utils.isValidEmail(email))) {
                Toast.makeText(this, "Email not valid", Toast.LENGTH_SHORT).show();
                return;
            }
            sendEmailOTP("email",email);
        });

        tvTimer.setOnClickListener(v -> {
            String email = etEmail.getText().toString();
            if(tvTimer.getText().toString().equals("Resend OTP")){
                sendEmailOTP("email",email);
            }
        });

        tvTimerMobile.setOnClickListener(v -> {
            String phone = etPhone.getText().toString();
            if(tvTimerMobile.getText().toString().equals("Resend OTP")){
                sendPhoneOTP("phone",phone);
            }
        });

        if(!phone.equals("")){
            etPhone.setText(phone);
            etPhone.setEnabled(false);
        }

        if(!email.equals("")){
            etEmail.setText(email);
            etEmail.setEnabled(false);
        }

        if(type.equals("gmail")){
            tvtitle.setVisibility(View.GONE);
            otp_view_email.setVisibility(View.GONE);
            tvVerifyEmail.setText("Verified");
        }

        findViewById(R.id.tvNext).setOnClickListener(v -> {
            verifyPhoneNumber();
        });

        verifyPhone();

        tvVerifyEmail.setOnClickListener(v -> {
                if(TextUtils.isEmpty(otp_view_email.getOTP().toString())){
                    Toast.makeText(this, "Please Enter OTP", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!otp_view_email.getOTP().equals(emailotp)){
                    Toast.makeText(this, "Please Enter Valid OTP", Toast.LENGTH_SHORT).show();
                    return;
                }
                confirmEmailOTP("email",etEmail.getText().toString(),emailotp);
        });

        tvVerifyMobile.setOnClickListener(v -> {
                if(TextUtils.isEmpty(otp_view_mobile.getOTP().toString())){
                    Toast.makeText(this, "Please Enter OTP", Toast.LENGTH_SHORT).show();
                    return;
                }
            if(!otp_view_mobile.getOTP().equals(phoneotp)){
                Toast.makeText(this, "Please Enter Valid OTP", Toast.LENGTH_SHORT).show();
                return;
            }
                confirmPhoneOTP("email",etPhone.getText().toString(),phoneotp);
        });

        tvNext.setOnClickListener(v -> {
            if(tvVerifyEmail.getText().toString().equals("Verify")){
                Toast.makeText(this, "Please Verify Email", Toast.LENGTH_SHORT).show();
                return;
            }
            if(tvVerifyMobile.getText().toString().equals("Verify")){
                Toast.makeText(this, "Please Verify Phone", Toast.LENGTH_SHORT).show();
                return;
            }
            prepareUser();
            ApplicationClass.appPreferences.setisverfy(Boolean.parseBoolean("true"));
            Intent intent = new Intent(this, ProfileFirst_Activity.class);
            intent.putExtra("first", true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        });
    }

    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
//We can add sender phone number or leave it blank
// I'm adding null here
        client.startSmsUserConsent(null).addOnSuccessListener(aVoid -> {
//                Toast.makeText(getApplicationContext(), "On Success", Toast.LENGTH_LONG).show();
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
//                Toast.makeText(getApplicationContext(), "On OnFailure", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
//That gives all message to us.
// We need to get the code from inside with regex
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
/* textViewMessage.setText(
String.format("%s - %s", getString(R.string.received_message), message));
*/
                getOtpFromMessage(message);
            }
        }
    }

    private void getOtpFromMessage(String message) {
// This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{4}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            Log.d("sdfg",matcher.group(0)+"");
            int num = Integer.parseInt(matcher.group(0)+"");
            String number = String.valueOf(num);
            otp_view_mobile.setOTP(number);
//            for(int i = 0; i < number.length(); i++) {
//                int j = Character.digit(number.charAt(i), 10);
//
////                System.out.println("digit: " + j);
////                if (i == 0) {
////                    Log.d("num1", j + "");
////                    et1.setText(j+"");
////                }
////                if (i == 1) {
////                    Log.d("num2", j + "");
////                    et2.setText(j+"");
////                }
////                if (i == 2) {
////                    Log.d("num3", j + "");
////                    et3.setText(j+"");
////                }
////                if (i == 3) {
////                    Log.d("num4", j + "");
////                    et4.setText(j+"");
////                }
//            }
        }
    }


    private void registerBroadcastReceiver() {
        smsBroadcastReceiver = new SmsBroadcastReceiver();
        smsBroadcastReceiver.smsBroadcastReceiverListener =
                new SmsBroadcastReceiver.SmsBroadcastReceiverListener() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, REQ_USER_CONSENT);
                    }

                    @Override
                    public void onFailure() {

                    }
                };
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerBroadcastReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(smsBroadcastReceiver);
    }
    @Override
    protected BasicModel getModel() {
        return loginModel;
    }

    private void verifyPhoneNumber() {
        String phone = etPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(this, "Please enter phone number", Toast.LENGTH_SHORT).show();
            return;
        }
        if (phone.length() < 7) {
            Toast.makeText(this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!(Utils.isValidMobile(phone))){
            Toast.makeText(this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof OTPVerificationSocialData) {
            OTPVerificationSocialData otpVerificationSocialData = (OTPVerificationSocialData) o;
                Intent intent = new Intent(this, OtpActivity.class);
                intent.putExtra("phone",etPhone.getText().toString().trim());
                intent.putExtra("otp", otpVerificationSocialData.getOtp());
                startActivity(intent);
        }else if (o instanceof OTPVerificationData) {
            OTPVerificationData otpVerificationData = (OTPVerificationData) o;
            if (Boolean.parseBoolean(otpVerificationData.getProfileFirstStatus())) {
                ApplicationClass.appPreferences.SetToken(otpVerificationData.getToken());
                if (otpVerificationData.getUser() != null && otpVerificationData.getUser().getPhone() != null) {
                    ApplicationClass.appPreferences.setPhoneNumber(otpVerificationData.getUser().getPhone());
                }
                ApplicationClass.appPreferences.setFirstTime(Boolean.parseBoolean(otpVerificationData.getProfileFirstStatus()));
                OTPVerificationDataUser otpVerificationDataUser = otpVerificationData.getUser();
                if (otpVerificationDataUser != null) {
                    if (otpVerificationDataUser.getEmail() != null) {
                        ApplicationClass.appPreferences.setEmailId(otpVerificationDataUser.getEmail());
                    }
                    if (otpVerificationDataUser.getProfile() != null) {
                        if (otpVerificationDataUser.getPath() != null) {
                            String imageURL = Constant.ImageURL + otpVerificationDataUser.getPath() + "/" + otpVerificationDataUser.getProfile();
                            ApplicationClass.appPreferences.setUserPicUrl(imageURL);
                        } else {
                            ApplicationClass.appPreferences.setUserPicUrl(otpVerificationDataUser.getProfile());
                        }
                    }
                    if (otpVerificationDataUser.getUserDetils() != null) {
                        if (otpVerificationDataUser.getUserDetils().getName() != null) {
                            ApplicationClass.appPreferences.setUserName(otpVerificationDataUser.getUserDetils().getName());
                        }
                        if (otpVerificationDataUser.getUserDetils().getUserId() != null) {
                            userId = String.valueOf(otpVerificationDataUser.getUserDetils().getUserId());
                            ApplicationClass.appPreferences.setUserId(otpVerificationDataUser.getUserDetils().getUserId());
                        }
                    }
                }
                else {
                    Intent intent = new Intent(this, UserHomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            } else {
                ApplicationClass.appPreferences.SetToken(otpVerificationData.getToken());
                    Intent intent = new Intent(this, ProfileFirst_Activity.class);
                    intent.putExtra("first", true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
            }
        }
    }

    private void registerUserWithSocial(String appId, String image, String email, String phone) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            loginModel.registerBySocial(this, relativeParent, appId, image, type, email, phone);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    public void sendEmailOTP(String type,String email) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<NewRegisterModel> call = apiInterface.sendEmailOTp(type,email,userid);

        call.enqueue(new Callback<NewRegisterModel>() {
            @Override
            public void onResponse(Call<NewRegisterModel> call, Response<NewRegisterModel> response) {
                dialog.dismiss();
                NewRegisterModel user = response.body();
                if(user.success){
                    if(user.message.equals("Alaready registered.")){
                        Toast.makeText(MobileScreen.this, "This Email Id is already registered", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        emailotp = user.data.otp;
                        tvtitle.setVisibility(View.GONE);
                        tvTimer.setVisibility(View.VISIBLE);
                        new CountDownTimer(120000, 1000) {
                            public void onTick(long millisUntilFinished) {
                                tvTimer.setText(millisUntilFinished / 1000 + " S" );
                                //here you can have your logic to set text to edittext
                            }
                            public void onFinish() {
                                tvTimer.setText("Resend OTP");
                                tvTimer.setTextColor(getResources().getColor(R.color.white));
                            }
                        }.start();
                    }
                }

            }

            @Override
            public void onFailure(Call<NewRegisterModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


    public void confirmEmailOTP(String type,String email,String otp) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<NewRegisterModel> call = apiInterface.confirmEmailOTP(type,email,otp,userid);

        call.enqueue(new Callback<NewRegisterModel>() {
            @Override
            public void onResponse(Call<NewRegisterModel> call, Response<NewRegisterModel> response) {
                dialog.dismiss();
                NewRegisterModel user = response.body();
                if(user.success){
//                    tvVerifyEmail.setText("verified");
                    layoutEmail.setOrientation(LinearLayout.VERTICAL);
                    tvTimer.setVisibility(View.GONE);
                    otp_view_email.setVisibility(View.GONE);
                    tvVerifyEmail.setText("Verified");
                    tvVerifyEmail.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_radio_checked), null, null, null);
                }
            }

            @Override
            public void onFailure(Call<NewRegisterModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void sendPhoneOTP(String type,String phone) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<NewRegisterModel> call = apiInterface.sendPhoneOTp(type,phone,userid);
        call.enqueue(new Callback<NewRegisterModel>() {
            @Override
            public void onResponse(Call<NewRegisterModel> call, Response<NewRegisterModel> response) {
                dialog.dismiss();
                NewRegisterModel user = response.body();
                if(user.success){
                    if(user.message.equals("mobile alrady register anoter account.")){
                        Toast.makeText(MobileScreen.this, "This Mobile Number is already registered", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        phoneotp = user.data.otp;
                        tvtitleMobile.setVisibility(View.GONE);
                        tvTimerMobile.setVisibility(View.VISIBLE);
                        new CountDownTimer(120000, 1000) {
                            public void onTick(long millisUntilFinished) {
                                tvTimerMobile.setText(millisUntilFinished / 1000 + " S" );
                                //here you can have your logic to set text to edittext
                            }
                            public void onFinish() {
                                tvTimerMobile.setText("Resend OTP");
                                tvTimerMobile.setTextColor(getResources().getColor(R.color.white));
                            }
                        }.start();
                    }
                }
            }

            @Override
            public void onFailure(Call<NewRegisterModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void confirmPhoneOTP(String type,String phone,String otp) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<NewRegisterModel> call = apiInterface.confirmMobileOTP(type,phone,otp,userid);

        call.enqueue(new Callback<NewRegisterModel>() {
            @Override
            public void onResponse(Call<NewRegisterModel> call, Response<NewRegisterModel> response) {
                dialog.dismiss();
                NewRegisterModel user = response.body();
                if(user.success){
                    layoutMobile.setOrientation(LinearLayout.VERTICAL);
                    otp_view_mobile.setVisibility(View.GONE);
                    tvTimerMobile.setVisibility(View.GONE);
                    tvVerifyMobile.setText("Verified");
                    tvVerifyMobile.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_radio_checked), null, null, null);
                }
            }
            @Override
            public void onFailure(Call<NewRegisterModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void verifyPhone() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<VerifyPhoneModel> call = apiInterface.verifyPhone("Bearer "+accessToken);

        call.enqueue(new Callback<VerifyPhoneModel>() {
            @Override
            public void onResponse(Call<VerifyPhoneModel> call, Response<VerifyPhoneModel> response) {
                dialog.dismiss();
                VerifyPhoneModel user = response.body();
                if(user.data.email!=null){
                    etEmail.setText(user.data.email);
                    etEmail.setEnabled(false);
                }
                if(user.data.phone!=null){
                    etPhone.setText(user.data.phone);
                    etPhone.setEnabled(false);
                }
                userid = user.data.userid;
                type = user.data.type;


            }

            @Override
            public void onFailure(Call<VerifyPhoneModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
    private void prepareUser() {
        QBUser qbUser = new QBUser();
        qbUser.setLogin(etPhone.getText().toString());
//         qbUser.setFullName(usernameEt.getText().toString().trim());
        qbUser.setPassword(etPhone.getText().toString());
        //qbUser.setPassword(App.USER_DEFAULT_PASSWORD);
        signIn(qbUser);
    }

    private void signIn(final QBUser user) {
        ChatHelper.getInstance().login(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser userFromRest, Bundle bundle) {
                if (userFromRest.getId().equals(user.getId())) {
                    loginToChat(user);
                    Log.d("user",user.toString()+"@!#$%");
//                    Toast.makeText(OtpActivity.this, "login", Toast.LENGTH_SHORT).show();
                } else {
                    //Need to set password NULL, because server will update user only with NULL password
                    user.setPassword(null);
                    Toast.makeText(MobileScreen.this, "register", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    private void loginToChat(final QBUser user) {
        //Need to set password, because the server will not register to chat without password
        user.setPassword(etPhone.getText().toString());
        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                AppPreferences.getInstance().saveQbUser(user);
                Log.e("OTPSESSION",user.toString());
                finish();
//                hideProgressDialog();
            }

            @Override
            public void onError(QBResponseException e) {
//                hideProgressDialog();
//                showErrorSnackbar(R.string.login_chat_login_error, e, null);
            }
        });
    }

}