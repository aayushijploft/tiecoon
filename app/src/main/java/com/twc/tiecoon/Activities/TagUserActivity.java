package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.twc.tiecoon.Adapters.ReportReasonAdapter;
import com.twc.tiecoon.Adapters.SelectedTagUserAdapter;
import com.twc.tiecoon.Adapters.TagUserListAdapter;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.models.tagModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.CityData;
import com.twc.tiecoon.network.response.CityDataList;
import com.twc.tiecoon.network.response.ReportList;
import com.twc.tiecoon.network.response.TagUserList;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Observable;

public class TagUserActivity extends AbstractFragmentActivity {

    private ProfileModel profileModel = new ProfileModel();
    RecyclerView rvUserList,rvSelectedUserList;
    LinearLayout linearParent;
    TagUserListAdapter tagUserListAdapter;
    ArrayList<tagModel> userid = new ArrayList<>();
    SelectedTagUserAdapter selectedTagUserAdapter;
    List<TagUserList.Data> tagUserLists = new ArrayList<>();
    EditText et_name;
    Button btnTag;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_user);
        rvUserList = findViewById(R.id.rvUserList);
        rvSelectedUserList = findViewById(R.id.rvSelectedUserList);
        linearParent = findViewById(R.id.linearParent);
        et_name = findViewById(R.id.et_name);
        btnTag = findViewById(R.id.btnTag);
        findViewById(R.id.iv_back).setOnClickListener(v -> {
//            onBackPressed();
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        });

        rvUserList.setHasFixedSize(true);
        rvUserList.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        rvSelectedUserList.setHasFixedSize(true);
        rvSelectedUserList.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));

        et_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                if(newCharText.length() > 2){
                    getTagUserList(newCharText,false);
                }
                if(newCharText.length() == 0){
                    getTagUserList("",false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        selectedTagUserAdapter = new SelectedTagUserAdapter(this, userid, pos -> {
                userid.remove(userid.get(pos));
                selectedTagUserAdapter.notifyDataSetChanged();
        });
        rvSelectedUserList.setAdapter(selectedTagUserAdapter);

        btnTag.findViewById(R.id.btnTag).setOnClickListener(v -> {
            Intent returnIntent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable("value", userid);
            returnIntent.putExtras(bundle);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        });

        getTagUserList("",true);

    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof TagUserList) {
            TagUserList tagUserList = (TagUserList) arg;
            tagUserLists = tagUserList.getData();
            tagUserListAdapter = new TagUserListAdapter(this, tagUserLists, pos -> {
                tagModel model = new tagModel();
                model.setName(tagUserLists.get(pos).getName());
                model.setId(tagUserLists.get(pos).getId()+"");
                userid.add(model);
                selectedTagUserAdapter.notifyDataSetChanged();
            });
            rvUserList.setAdapter(tagUserListAdapter);
            tagUserListAdapter.notifyDataSetChanged();
        }
    }


    private void getTagUserList(String search,boolean isload) {
        if (Utils.isDeviceOnline(this)) {
            if(isload){
                Utils.showProDialog(this);
            }

            profileModel.getTagUserList(this, linearParent,search);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }
}