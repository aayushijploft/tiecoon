package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.twc.tiecoon.Adapters.ReportReasonAdapter;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.DeleteStory;
import com.twc.tiecoon.network.response.ReportAdd;
import com.twc.tiecoon.network.response.ReportList;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.Utils;

import java.util.Observable;

public class ReportReasonScreen extends AbstractFragmentActivity {

    private ProfileModel profileModel = new ProfileModel();
    RecyclerView rvReportReason;
    LinearLayout linearParent;
    ReportReasonAdapter reportReasonAdapter;
    Button btnPost;
    String user_id,report_id="";
    EditText editText;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_report_reason_screen);

        user_id = getIntent().getStringExtra("user_id");
        rvReportReason=findViewById(R.id.rvReportReason);
        linearParent=findViewById(R.id.linearParent);
        btnPost=findViewById(R.id.btnPost);
        editText=findViewById(R.id.editText);
        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());
        rvReportReason.setHasFixedSize(true);
        rvReportReason.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        btnPost.setOnClickListener(v -> {
            if(report_id.equals("")){
                Toast.makeText(this, "Why you want to report this user ?", Toast.LENGTH_SHORT).show();
            }
            else postReport();
        });
        getReportReasons();
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void getReportReasons() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getReportReasons(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }


    @Override
    public void update(Observable o, Object arg) {

        if (arg instanceof ReportList) {
            ReportList reportList = (ReportList) arg;
            reportReasonAdapter = new ReportReasonAdapter(this, reportList.getData(), pos -> {
                report_id = reportList.getData().get(pos).getId()+"";
            });
            rvReportReason.setAdapter(reportReasonAdapter);
        }
        if (arg instanceof ReportAdd) {
            Toast.makeText(this, "User Reported", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }

    private void postReport() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.addReport(this, linearParent,user_id,report_id,editText.getText().toString().trim());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

}