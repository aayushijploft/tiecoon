package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.twc.tiecoon.Adapters.MyProfileProductAdapters;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.ProductCategory;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class DropdownActivity extends AbstractFragmentActivity {
    private final HomeModel homeModel = new HomeModel();
    Spinner spinnerProducts;
    RecyclerView recyclerView_upload_product;
    private final List<ProductCategory.Data> listProductCategorySpinnerData = new ArrayList<>();
    private final List<String> listProductSpinner = new ArrayList<>();
    LinearLayout linearParent;
    MyProfileProductAdapters myProfileProductAdapters;
    List<ProductCategory.Data> dataList = new ArrayList<>();
    public static final int UPLOAD_PRODUCTS = 102;


    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dropdown);
        spinnerProducts = findViewById(R.id.spinnerProducts);
        linearParent = findViewById(R.id.linearParent);
        recyclerView_upload_product = findViewById(R.id.recyclerView_upload_product);

        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getProductCategory(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }

        spinnerProducts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    ((TextView) spinnerProducts.getSelectedView()).setTextColor(getResources().getColor(R.color.white));
                } else {
                    ((TextView) spinnerProducts.getSelectedView()).setTextColor(getResources().getColor(R.color.white));
                    ProductCategory.Data categoryData = getProductCategoryData(listProductSpinner.get(i));
                    dataList.add(categoryData);
                    if (categoryData != null) {
                        myProfileProductAdapters = new MyProfileProductAdapters(DropdownActivity.this, dataList, (pos, subPos, type) -> {
                            if(type.equals("Upload")){
                                Intent intent = new Intent(DropdownActivity.this,AddProductActivity.class);
                                intent.putExtra("productcat_id",dataList.get(pos).getId()+"");
                                intent.putExtra("productcat_name",dataList.get(pos).getTitle());
                                startActivityForResult(intent,UPLOAD_PRODUCTS);
                            }
                            else if(type.equals("remove")){
                                dataList.remove(pos);
                                myProfileProductAdapters.notifyDataSetChanged();
                            }

                        });
                        recyclerView_upload_product.setAdapter(myProfileProductAdapters);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof ProductCategory) {
//            Toast.makeText(getContext(), "Products", Toast.LENGTH_SHORT).show();
            listProductSpinner.clear();
            setProductCategory((ProductCategory) arg);
        }
    }

    private ProductCategory.Data getProductCategoryData(String title) {
        for (ProductCategory.Data categoryData : listProductCategorySpinnerData) {
            if (categoryData.getTitle() != null && title.equals(categoryData.getTitle())) {
                return categoryData;
            }
        }
        return null;
    }

    private void setProductCategory(ProductCategory productCategory) {
        List<ProductCategory.Data> productcategory = productCategory.getData();
        Log.e("Categorylist1", "size:" + productcategory.size() + ",  " + productcategory.toString());
        if (productcategory != null && !(productcategory.isEmpty())) {
            for (ProductCategory.Data productCategory1 : productcategory) {
                if (productCategory1.getTitle() != null) {
                    listProductCategorySpinnerData.add(productCategory1);
                    listProductSpinner.add(productCategory1.getTitle());
                }
            }
            listProductSpinner.add(0, "Select Product");
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, listProductSpinner);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            spinnerProducts.setAdapter(arrayAdapter);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case UPLOAD_PRODUCTS:
                if (resultCode == Activity.RESULT_OK) {
                   String createProduct = data.getStringExtra("createProduct");
                    Toast.makeText(this, createProduct, Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }


}