package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jsibbold.zoomage.ZoomageView;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddStory;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.utils.Utils;

import java.io.File;
import java.util.Observable;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddStoryScreen extends AbstractFragmentActivity {
    private ProfileModel profileModel = new ProfileModel();
    String image;
    ZoomageView myZoomageView;
    private MultipartBody.Part multiBodyPart = null;
    Button btnPost;
    ImageView imgCancel;
    LinearLayout linearParent;
    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_story_screen);
        image = getIntent().getStringExtra("image");
        myZoomageView = findViewById(R.id.myZoomageView);
        imgCancel = findViewById(R.id.imgCancel);
        btnPost = findViewById(R.id.btnPost);
        linearParent = findViewById(R.id.linearParent);
        myZoomageView.setImageURI(Uri.parse(image));

        String filePath = Utils.getUriRealPath(this, Uri.parse(image));
        File file = new File(filePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
        multiBodyPart = MultipartBody.Part.createFormData("story", file.getName(), requestFile);

        imgCancel.setOnClickListener(v -> onBackPressed());

        btnPost.setOnClickListener(v -> saveStory());
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void saveStory() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);

                profileModel.saveStory(this, linearParent, multiBodyPart);

        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }



    @Override
    public void update(Observable o, Object arg) {

        if (arg instanceof AddStory) {
            Toast.makeText(this, "Story Added Successfully", Toast.LENGTH_SHORT).show();
            AddStory addStory = (AddStory) arg;
            startActivity(new Intent(getApplicationContext(),UserHomeActivity.class));
        }
    }
}