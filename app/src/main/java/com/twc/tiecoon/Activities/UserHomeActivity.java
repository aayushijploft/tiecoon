package com.twc.tiecoon.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.Adapters.PreferedLangAdapter;
import com.twc.tiecoon.Adapters.SelectCurrencyAdapter;
import com.twc.tiecoon.Adapters.SelectDurationAdapter;
import com.twc.tiecoon.Adapters.SelectProductAdapter;
import com.twc.tiecoon.Adapters.SelectProjectAdapter;
import com.twc.tiecoon.Adapters.SelectServiceAdapter;
import com.twc.tiecoon.Adapters.TalentsAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.Fragments.HomeFragment;
import com.twc.tiecoon.Fragments.MessageFragment;
import com.twc.tiecoon.Fragments.NotificationFragment;
import com.twc.tiecoon.Fragments.ProfileFragment;
import com.twc.tiecoon.Fragments.ProjectCollabFragment;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.FilterUserData;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.CurrencyData;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.PreferredLanguage;
import com.twc.tiecoon.network.response.PreferredLanguageData;
import com.twc.tiecoon.network.response.ProductCategory;
import com.twc.tiecoon.network.response.ProfileData;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.network.response.ProjectUsers;
import com.twc.tiecoon.network.response.ServiceCategory;
import com.twc.tiecoon.network.response.TalantCat;
import com.twc.tiecoon.network.response.TourModel;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Observable;

public class UserHomeActivity extends AbstractFragmentActivity  implements View.OnClickListener, TextWatcher, AdapterView.OnItemSelectedListener, GoogleApiClient.OnConnectionFailedListener {
    private static final int MYPROFILE = 105;
    public static TextView tvUserName, tvUserEmail;
    public static ImageView imgProfile;
    public static BottomNavigationView bottom_navigation;
    //     Currency
    private final List<CurrencyData> currencyDataList = new ArrayList<>();
    private final List<CurrencyData> currencyDataList2 = new ArrayList<>();
    //    For Duration
    private final List<String> durationList = new ArrayList<>();
    private final List<String> durationList1 = new ArrayList<>();
    public  static   ImageView ivTiconn;
  public  static   CardView layoutSearch;
    ActionBarDrawerToggle actionBarDrawerToggle;
    RelativeLayout bottombar;
    LinearLayout home_rel, my_account_rel1, track_order_rel, my_profile_rel, share_app_rel, projectInvite,saved,
            notification_rel, TransactionLayout, suggestTalent, kycDetails, nav_logout,lay_support, help_rel, savetalent_rel,layoutProfile,
            saveProject, saveAds,addAddress,layout_setting,layoutFeedback;
    String talentprice = "";
    String Productprice = "";
    Spinner spinnerStatus;
    List<String> listTalentsSpinner = new ArrayList<>();
    List<Integer> listTalentsSpinnerId = new ArrayList<>();
    Spinner spinnerTalents;
    int selectedProjectId;
    ArrayAdapter arrayAdapter;
    String currencyCode = "";
    TextView tv_select_currency, tvSelectProducts, tv_select_Duration, tvSelectProject,
            tv_select_Productcurrency, tvSelectService, tv_select_Servicelanguage, tv_select_servicecurrency;
    String FilterType = "";
    String ProductCategory = "";
    String ServiceCategory = "";
    String Projects = "";
    String Language = "";
    String user = "";
    boolean isshow = false;
    public static String type = "1";
    String duration;
    FilterUserData filterUserData;
    RecyclerView rvProduct;
    RecyclerView rvServices;
    RecyclerView rvProjects;
    RecyclerView rvLanguages;
    private final ProfileModel profileModel = new ProfileModel();
    private CoordinatorLayout coordinatorParent;
    private DrawerLayout drawer;
    private View navHeader;
    private Toolbar mToolbar;

    private LinearLayout ll_fullTime, ll_PartTime, ll_male, ll_female, ll_other, linearongoingAssignment, layoutHeader;
    private LinearLayout ll_servicemale, ll_servicefemale, ll_serviceother, ll_projectemale,ll_projectnopref, ll_projectfemale, ll_projectother;
    private ImageView imgFullTime, imgPartTime, imgMale, imgfemale, imgOther;
    private ImageView imgserviceMale, imgserviceFemale, imgserviceOther, imgprojectMale,imgprojectnopref, imgprojectFemale, imgprojectOther;
    private FragmentData fragmentData;
    private TextView toolbar_title;
    private MenuItem filterItem;
    private MenuItem filterItem1;
    public static EditText et_Search;
   public static boolean click = true;
    private final String[] ageRAngeArray = {"Select Age Range", "Less than 5", "Less than 10", "Less than 15", "Less than 20", "Less than 25", "Less than 30", "Less than 35", "Less than 40",
            "Less than 45", "Less than 50", "Less than 55", "Less than 60"};
    private ImageView imgTalents, imgProductsAndServices, imgBrands;
    private TextView tvSelectTalents, tvOngoingAssignnmets;
    private ProfileTalent profileTalent = null;
    //    private CategoryCurrency categoryCurrency = null;
    private String workType = "";
    private String gender = "";
    private String servicegender = "";
    private String projectgender = "";
    private String ageRange = "";
    private final String projectId = "";
    private boolean OngoingAssignmets = false;
    private final List<ProfileTalent> addTalentDataList = new ArrayList<>();
    private final List<ProfileTalent> addTalentDataList2 = new ArrayList<>();
    private Dialog dialogTalents;
    private TalentsAdapter talentsAdapter;
    private final String[] statustype = {"Online", "Away", "Inactive"};
    private final List<CategoryData> listCategorySpinnerData = new ArrayList<>();
    private final List<List<HomeUserDetailsUploadTalent>> listTalent = new ArrayList<>();
    private GoogleApiClient mGoogleApiClient;
    private SelectCurrencyAdapter selectCurrencyAdapter;
    private Dialog dialogCurrency;
    private CurrencyData currencyData = null;
    //    Product
    private Dialog dialogProduct;
    private SelectProductAdapter selectProductAdapter;
    private List<ProductCategory.Data> Productlist = new ArrayList<>();
    //    Service
    private Dialog dialogService;
    private SelectServiceAdapter selectServiceAdapter;
    private List<ServiceCategory.Data> Servicelist = new ArrayList<>();
    //    Project
    private Dialog dialogProject;
    private SelectProjectAdapter selectProjectAdapter;
    private List<TalantCat.Data> Projectlist = new ArrayList<>();
    //    Language
    private Dialog preferredlangDialog;
    private PreferedLangAdapter preferedLangAdapter;
    private List<PreferredLanguageData> preferredLanguageDataList = new ArrayList<>();
    private SelectDurationAdapter selectDurationAdapter;
    private Dialog dialogDuration;
    TextView tvSaved,tvTalents,tvProSer,tvProj,tvAds,tvProfile;
    TextView tvTerms,tvPrivacy,tvRefund,tvCookie,tvUsageGuideline;
    LinearLayout layout_policies,layoutpolicyclick;
    String open = "";
    String REF_CODE = "TIECOON";

    ArrayList<TourModel> tourlist = new ArrayList();
    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.homeFragment:
                    HomeFragment homeFragment = new HomeFragment();
                    fragmentData = (FragmentData) homeFragment;
                    addFragment(homeFragment,"home");
                    filterItem.setVisible(true);
                    filterItem1.setVisible(true);
                    toolbar_title.setText("Earn Your Place");
                    toolbar_title.setVisibility(View.GONE);
                    ivTiconn.setVisibility(View.VISIBLE);
                    return true;
                case R.id.notificationsFragment:
                    addFragment(new NotificationFragment(),"notification");
                    filterItem.setVisible(false);
                    filterItem1.setVisible(false);
                    toolbar_title.setText(getString(R.string.notification));
                    toolbar_title.setVisibility(View.VISIBLE);
                    layoutSearch.setVisibility(View.GONE);
                    ivTiconn.setVisibility(View.GONE);
                    return true;
                  case R.id.projectFragment:
                    addFragment(new ProjectCollabFragment(),"ProjectCollab");
                    filterItem.setVisible(false);
                      filterItem1.setVisible(false);
                    toolbar_title.setText("Collaboration Requests");
                      layoutSearch.setVisibility(View.GONE);
                    toolbar_title.setVisibility(View.VISIBLE);
                    ivTiconn.setVisibility(View.GONE);
                    return true;
                case R.id.profileFragment:
                    addFragment(new ProfileFragment(),"profile");
                    filterItem.setVisible(false);
                    filterItem1.setVisible(false);
                    layoutSearch.setVisibility(View.GONE);
                    toolbar_title.setText("Profile");
                    toolbar_title.setVisibility(View.VISIBLE);
                    ivTiconn.setVisibility(View.GONE);
                    return true;
                case R.id.messageFragment:
                    addFragment(new MessageFragment(),"message");
                    filterItem.setVisible(false);
                    filterItem1.setVisible(false);
                    toolbar_title.setText(getString(R.string.message));
                    layoutSearch.setVisibility(View.GONE);
                    toolbar_title.setVisibility(View.VISIBLE);
                    ivTiconn.setVisibility(View.GONE);
                    return true;
            }
            return false;
        }
    };

    public static void setNavHeaderData(Context context) {
        if (ApplicationClass.appPreferences.getUserName() != null) {
            tvUserName.setText(ApplicationClass.appPreferences.getUserName());
        }
        if (ApplicationClass.appPreferences.getEmailId() != null) {
            tvUserEmail.setText(ApplicationClass.appPreferences.getEmailId());
        }
        Glide.with(context).load(ApplicationClass.appPreferences.getUserPicUrl()).error(R.drawable.userddummyset)
                .error(R.drawable.userddummyset)
                .into(imgProfile);
    }

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_user_home);
        if(getIntent().hasExtra("user")){
            user = getIntent().getStringExtra("user");
            isshow = true;
            Log.e("user",user);
        }

        if(getIntent().hasExtra("open")){
            open = getIntent().getStringExtra("open");
        }

        coordinatorParent = findViewById(R.id.coordinatorParent);
        mToolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_nav_bar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        drawer = findViewById(R.id.drawer_layout);
        ivTiconn = findViewById(R.id.ivTiconn);
        layoutSearch = findViewById(R.id.layoutSearch);
        NavigationView navigationView = findViewById(R.id.nav_view);
        home_rel = findViewById(R.id.home_rel);
        saved = findViewById(R.id.saved);
        spinnerStatus = findViewById(R.id.spinnerStatus);
        layoutProfile = findViewById(R.id.layoutProfile);
        my_account_rel1 = findViewById(R.id.my_account_rel1);
        track_order_rel = findViewById(R.id.track_order_rel);
        my_profile_rel = findViewById(R.id.my_profile_rel);
        share_app_rel = findViewById(R.id.share_app_rel);
        notification_rel = findViewById(R.id.notification_rel);
        layout_setting = findViewById(R.id.layout_setting);
        layoutFeedback = findViewById(R.id.layoutFeedback);
        savetalent_rel = findViewById(R.id.savetalent_rel);
        projectInvite = findViewById(R.id.projectInvite);
        tvSaved = findViewById(R.id.tvSaved);
        tvTalents = findViewById(R.id.tvTalents);
        tvProSer = findViewById(R.id.tvProSer);
        et_Search = findViewById(R.id.et_Search);
        tvProj = findViewById(R.id.tvProj);
        tvAds = findViewById(R.id.tvAds);
        tvProfile = findViewById(R.id.tvProfile);
        saveProject = findViewById(R.id.saveProject);
        suggestTalent = findViewById(R.id.suggestTalent);
        TransactionLayout = findViewById(R.id.TransactionLayout);
        kycDetails = findViewById(R.id.kycDetails);
        help_rel = findViewById(R.id.help_rel);
        lay_support = findViewById(R.id.lay_support);
        layoutHeader = findViewById(R.id.layoutHeader);
        nav_logout = findViewById(R.id.nav_logout);
        saveAds = findViewById(R.id.saveAds);
        addAddress = findViewById(R.id.addAddress);
        layout_policies = findViewById(R.id.layout_policies);
        layoutpolicyclick = findViewById(R.id.layoutpolicyclick);
        tvTerms = findViewById(R.id.tvTerms);
        tvPrivacy = findViewById(R.id.tvPrivacy);
        tvRefund = findViewById(R.id.tvRefund);
        tvCookie = findViewById(R.id.tvCookie);
        tvUsageGuideline = findViewById(R.id.tvUsageGuideline);
        tvTerms.setOnClickListener(this);
        tvPrivacy.setOnClickListener(this);
        tvRefund.setOnClickListener(this);
        tvCookie.setOnClickListener(this);
        tvUsageGuideline.setOnClickListener(this);
        layout_policies.setOnClickListener(this);
        layoutpolicyclick.setOnClickListener(this);
        home_rel.setOnClickListener(this);
        saved.setOnClickListener(this);
        tvSaved.setOnClickListener(this);
        tvTalents.setOnClickListener(this);
        tvProSer.setOnClickListener(this);
        tvProj.setOnClickListener(this);
        tvAds.setOnClickListener(this);
        tvProfile.setOnClickListener(this);
        my_account_rel1.setOnClickListener(this);
        my_profile_rel.setOnClickListener(this);
        track_order_rel.setOnClickListener(this);
        share_app_rel.setOnClickListener(this);
        notification_rel.setOnClickListener(this);
        layout_setting.setOnClickListener(this);
        layoutFeedback.setOnClickListener(this);
        layoutHeader.setOnClickListener(this);
        nav_logout.setOnClickListener(this);
        savetalent_rel.setOnClickListener(this);
        saveProject.setOnClickListener(this);
        suggestTalent.setOnClickListener(this);
        kycDetails.setOnClickListener(this);
        help_rel.setOnClickListener(this);
        lay_support.setOnClickListener(this);
        projectInvite.setOnClickListener(this);
        TransactionLayout.setOnClickListener(this);
        saveAds.setOnClickListener(this);
        addAddress.setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_nav_bar);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        imgProfile = findViewById(R.id.imgProfile);
        tvUserName = findViewById(R.id.tvUserName);
        tvUserEmail = findViewById(R.id.tvUserEmail);
        ImageView imgCancel = findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(this);
        bottom_navigation = findViewById(R.id.bottom_navigation);
        bottombar = findViewById(R.id.bottombar);
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) bottom_navigation.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(2);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;

//        navigationView.setNavigationItemSelectedListener(this);

        if(open.equals("profile")){
            bottom_navigation.setSelectedItemId(R.id.profileFragment);
            addFragment(new ProfileFragment(),"profile");
            filterItem.setVisible(false);
            toolbar_title.setText("Profile");
            toolbar_title.setVisibility(View.VISIBLE);
            ivTiconn.setVisibility(View.GONE);
        }
        else {
            HomeFragment homeFragment = new HomeFragment();
            fragmentData = (FragmentData) homeFragment;
            addFragment(homeFragment,"home");
        }

        String Type, Id;
        try {
            Type = getIntent().getStringExtra("Type");
            Id = getIntent().getStringExtra("Id");
            REF_CODE = getIntent().getStringExtra("REF_CODE");
        } catch (Exception e) {
            e.printStackTrace();
            Type = "";
            Id = "";
        }
        if (Type == null) Type = "";
        if (Id == null) Id = "";
        Log.d("drtfyguhij",Type+"_____"+REF_CODE);

        if (Type.equals("Profile")) {

        }

        if(user.equals("first")){
            isshow = true;
            if(isshow){
                showTourDialog();
            }
        }

        et_Search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

               new Handler().postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       filterUserData = new FilterUserData();
                       filterUserData.setName(s.toString());
                       if(s.length() > 3){
                           fragmentData.sendData(filterUserData,type,false);
                           filterUserData.setName("");
//                    s.clear();
                       }
                   }
               },3000);
            }
        });


        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, mToolbar, R.string.openDrawer, R.string.closeDrawer);
        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.spinner_list, statustype);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(arrayAdapter);
        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String status = spinnerStatus.getSelectedItem().toString().trim();
                updateuserStatus(status);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });

        layoutProfile.setOnClickListener(v1 -> {
            bottom_navigation.setSelectedItemId(R.id. profileFragment);
            drawer.closeDrawer(GravityCompat.START);
            addFragment(new ProfileFragment(),"profile");
            filterItem.setVisible(false);
            toolbar_title.setText("Profile");
            toolbar_title.setVisibility(View.VISIBLE);
            ivTiconn.setVisibility(View.GONE);
        });

        durationList1.add("Per Week");
        durationList1.add("Per Month");
        durationList1.add("Per Day");
        setNavHeaderData(this);
        Log.e("userfbtoken", ApplicationClass.appPreferences.GetFirebaseToken());
        setUserToken();
//        prepareUser();
    }

    private void updateuserStatus(String status) {
        profileModel.updateuserstatus(this, coordinatorParent, status);
    }

    private void setUserToken() {
        if (Utils.isDeviceOnline(this)) {
            profileModel.updateUserFirebaseToken(this, coordinatorParent, ApplicationClass.appPreferences.GetFirebaseToken());
        }
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_screen_filter_menu, menu);
        filterItem = menu.findItem(R.id.filter);
        filterItem1 = menu.findItem(R.id.search);
        filterItem.setVisible(true);
        filterItem1.setVisible(true);
        filterItem1.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(click){
                    layoutSearch.setVisibility(View.VISIBLE);
                    ivTiconn.setVisibility(View.GONE);
                    click = false;
                }
                else {
                    layoutSearch.setVisibility(View.GONE);
                    ivTiconn.setVisibility(View.VISIBLE);
                    click = true;
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.filter) {
            openUserFiltersDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgCancel:
                drawer.closeDrawer(GravityCompat.START);
                break;
            case R.id.linearTalents:
                drawer.closeDrawer(GravityCompat.START);
                imgTalents.setImageResource(R.drawable.ic_radio_checked);
                imgProductsAndServices.setImageResource(R.drawable.ic_radio_unchecked);
                imgBrands.setImageResource(R.drawable.ic_radio_unchecked);
                break;
            case R.id.linearProductsAndServices:
                drawer.closeDrawer(GravityCompat.START);
                imgTalents.setImageResource(R.drawable.ic_radio_unchecked);
                imgProductsAndServices.setImageResource(R.drawable.ic_radio_checked);
                imgBrands.setImageResource(R.drawable.ic_radio_unchecked);
                break;
            case R.id.linearBrands:
                drawer.closeDrawer(GravityCompat.START);
                imgTalents.setImageResource(R.drawable.ic_radio_unchecked);
                imgProductsAndServices.setImageResource(R.drawable.ic_radio_unchecked);
                imgBrands.setImageResource(R.drawable.ic_radio_checked);
                break;
            case R.id.home_rel:
                home_rel.setVisibility(View.GONE);
                saved.setVisibility(View.VISIBLE);
                break;
           case R.id.tvSaved:
               home_rel.setVisibility(View.VISIBLE);
               saved.setVisibility(View.GONE);
                break;
            case R.id.layout_policies:
                layout_policies.setVisibility(View.GONE);
                layoutpolicyclick.setVisibility(View.VISIBLE);
                break;
          case R.id.layoutpolicyclick:
              layout_policies.setVisibility(View.VISIBLE);
              layoutpolicyclick.setVisibility(View.GONE);
                break;
            case R.id.tvProfile:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, LikeUserActivity.class));
                break;
             case R.id.tvTerms:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, PolicyActivity.class)
                .putExtra("title","Terms Of Service"));
                break;
             case R.id.tvPrivacy:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, PolicyActivity.class)
                        .putExtra("title","Privacy Policy"));
                break;
             case R.id.tvRefund:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, PolicyActivity.class)
                        .putExtra("title","Refund Policy"));
                break;
             case R.id.tvCookie:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, PolicyActivity.class)
                        .putExtra("title","Cookie Policy"));
                break;
            case R.id.track_order_rel:
                drawer.closeDrawer(GravityCompat.START);
                Intent i = new Intent(this, OtherUserSavedBrandsActivity.class);
                startActivity(i);
                break;
            case R.id.TransactionLayout:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, TransactionActivity.class));
                break;
            case R.id.tvProSer:
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(this, OtherUserSavedProductsAndServiceActivity.class);
                startActivity(intent);
                break;
            case R.id.share_app_rel:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, AboutUsActivity.class));
                break;
            case R.id.projectInvite:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, ProjectInvitationActivity.class));
                break;
            case R.id.help_rel:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, PolicyActivity.class)
                        .putExtra("title","Help Center"));
                break;
             case R.id.tvUsageGuideline:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, PolicyActivity.class)
                        .putExtra("title","Usage Guideline"));
                break;
           case R.id.lay_support:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, HelpSupport_Activity.class));
                break;
            case R.id.tvTalents:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, OtherUserSavedTalentActivity.class));
                break;
            case R.id.tvProj:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, SavedProjectsActivity.class));
                break;
            case R.id.suggestTalent:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, SuggestTalentActivity.class));
                break;
            case R.id.kycDetails:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, AddKYCActivity.class));
                break;
            case R.id.tvAds:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, SavedAdvertismentActivity.class));
                break;
            case R.id.addAddress:
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(this, AddAddress_Activity.class));
                break;
            case R.id.notification_rel:
                drawer.closeDrawer(GravityCompat.START);
                Intent intent2 = new Intent(this, MyProfileActivity.class);
                startActivityForResult(intent2, MYPROFILE);
                break;
           case R.id.layout_setting:
                drawer.closeDrawer(GravityCompat.START);
               startActivity(new Intent(this, SettingActivity.class));
                break;
           case R.id.layoutFeedback:
                drawer.closeDrawer(GravityCompat.START);
               startActivity(new Intent(this, ApplicationFeedbackActivity.class));
                break;
            case R.id.layoutHeader:
//                Intent intent4 = new Intent(this, MyProfileActivity.class);
//                startActivityForResult(intent4, MYPROFILE);
                break;
            case R.id.nav_logout:
                drawer.closeDrawer(GravityCompat.START);
                showLogoutDialog();
                break;
            case R.id.cardViewMyTalents:
                drawer.closeDrawer(GravityCompat.START);
                if (addTalentDataList2.isEmpty()) {
                    getTalents();
                } else {
                    showTalentsDialog();
                }
                break;
            case R.id.ll_male:
                imgMale.setImageResource(R.drawable.ic_radio_checked);
                imgfemale.setImageResource(R.drawable.ic_radio_unchecked);
                imgOther.setImageResource(R.drawable.ic_radio_unchecked);
                gender = "male";
                break;
            case R.id.ll_female:
                imgMale.setImageResource(R.drawable.ic_radio_unchecked);
                imgfemale.setImageResource(R.drawable.ic_radio_checked);
                imgOther.setImageResource(R.drawable.ic_radio_unchecked);
                gender = "female";
                break;
            case R.id.ll_other:
                imgMale.setImageResource(R.drawable.ic_radio_unchecked);
                imgfemale.setImageResource(R.drawable.ic_radio_unchecked);
                imgOther.setImageResource(R.drawable.ic_radio_checked);
                gender = "other";
                break;
            case R.id.ll_servicemale:
                imgserviceMale.setImageResource(R.drawable.ic_radio_checked);
                imgserviceFemale.setImageResource(R.drawable.ic_radio_unchecked);
                imgserviceOther.setImageResource(R.drawable.ic_radio_unchecked);
                servicegender = "male";
                break;
            case R.id.ll_servicefemale:
                imgserviceMale.setImageResource(R.drawable.ic_radio_unchecked);
                imgserviceFemale.setImageResource(R.drawable.ic_radio_checked);
                imgserviceOther.setImageResource(R.drawable.ic_radio_unchecked);
                servicegender = "female";
                break;
            case R.id.ll_serviceother:
                imgserviceMale.setImageResource(R.drawable.ic_radio_unchecked);
                imgserviceFemale.setImageResource(R.drawable.ic_radio_unchecked);
                imgserviceOther.setImageResource(R.drawable.ic_radio_checked);
                servicegender = "other";
                break;
            case R.id.ll_projectnopref:
                imgprojectnopref.setImageResource(R.drawable.ic_radio_checked);
                imgprojectMale.setImageResource(R.drawable.ic_radio_unchecked);
                imgprojectFemale.setImageResource(R.drawable.ic_radio_unchecked);
                imgprojectOther.setImageResource(R.drawable.ic_radio_unchecked);
                projectgender = "no_preference";
                break;
           case R.id.ll_projectemale:
                imgprojectMale.setImageResource(R.drawable.ic_radio_checked);
                imgprojectFemale.setImageResource(R.drawable.ic_radio_unchecked);
                imgprojectOther.setImageResource(R.drawable.ic_radio_unchecked);
               imgprojectnopref.setImageResource(R.drawable.ic_radio_unchecked);
                projectgender = "male";
                break;
            case R.id.ll_projectfemale:
                imgprojectMale.setImageResource(R.drawable.ic_radio_unchecked);
                imgprojectFemale.setImageResource(R.drawable.ic_radio_checked);
                imgprojectOther.setImageResource(R.drawable.ic_radio_unchecked);
                imgprojectnopref.setImageResource(R.drawable.ic_radio_unchecked);
                projectgender = "female";
                break;
            case R.id.ll_projectother:
                imgprojectMale.setImageResource(R.drawable.ic_radio_unchecked);
                imgprojectFemale.setImageResource(R.drawable.ic_radio_unchecked);
                imgprojectnopref.setImageResource(R.drawable.ic_radio_unchecked);
                imgprojectOther.setImageResource(R.drawable.ic_radio_checked);
                projectgender = "other";
                break;
            case R.id.ll_fullTime:
                imgPartTime.setImageResource(R.drawable.ic_radio_unchecked);
                imgFullTime.setImageResource(R.drawable.ic_radio_checked);
                workType = "Full Time";
                break;
            case R.id.ll_PartTime:
                imgPartTime.setImageResource(R.drawable.ic_radio_checked);
                imgFullTime.setImageResource(R.drawable.ic_radio_unchecked);
                workType = "Part Time";
                break;
            case R.id.linearongoingAssignment:
                if (OngoingAssignmets) {
                    linearongoingAssignment.setBackground(getDrawable(R.drawable.rounder_border_linear_stroke));
                    tvOngoingAssignnmets.setTextColor(getResources().getColor(R.color.black));
                    OngoingAssignmets = false;
                } else {
                    linearongoingAssignment.setBackground(getDrawable(R.drawable.rounder_border_linear_solid));
                    tvOngoingAssignnmets.setTextColor(Color.WHITE);
                    OngoingAssignmets = true;
                }
                break;
        }

    }

    private void getTalents() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getUserDetails(this, coordinatorParent);
        } else {
            Utils.warningSnackBar(this, coordinatorParent, getString(R.string.no_internet));
        }
    }

    public void addFragment(Fragment fragment,String value) {
    /*    FragmentManager fm1 = getSupportFragmentManager();
//        fm1.popBackStack("Home", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fm1.beginTransaction();
        fragmentTransaction.replace(R.id.frame_container, fragment).addToBackStack(value);
        fragmentTransaction.commit();*/


        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.replace(R.id.frame_container, fragment, value);
        fragTrans.addToBackStack(null);
        fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTrans.commit();

    }

    private void showLogoutDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_logout);
        Button button_Cancel = dialog.findViewById(R.id.button_Cancel);
        Button button_logout = dialog.findViewById(R.id.button_logout);
        button_logout.setOnClickListener(view -> {
            ApplicationClass.appPreferences.SetToken("");
            LoginManager.getInstance().logOut();
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                mGoogleApiClient.clearDefaultAccountAndReconnect().setResultCallback(status -> mGoogleApiClient.disconnect());
            }
            startActivity(new Intent(UserHomeActivity.this, LoginActivity.class));
            dialog.dismiss();
            finish();
        });

        button_Cancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void onBackPressed() {

        Log.d("asfasdf","123456789");
        HomeFragment myFragment = (HomeFragment)getSupportFragmentManager().findFragmentByTag("home");
        if (myFragment != null && myFragment.isVisible()) {
            finish();
        }else{
            toolbar_title.setText("Earn Your Place");
            toolbar_title.setVisibility(View.GONE);
            ivTiconn.setVisibility(View.VISIBLE);
            bottom_navigation.setSelectedItemId(R.id.homeFragment);
            addFragment(new HomeFragment(),"home");

        }

       /* NotificationFragment myFragment1 = (NotificationFragment)getSupportFragmentManager().findFragmentByTag("notification");
        if (myFragment1 != null && myFragment1.isVisible()) {
            Log.d("sgfsdfgsdfg","___1___notification");
            bottom_navigation.setSelectedItemId(R.id.notificationsFragment);
        }
        ProfileFragment myFragment2 = (ProfileFragment)getSupportFragmentManager().findFragmentByTag("profile");
        if (myFragment2 != null && myFragment2.isVisible()) {
            Log.d("sgfsdfgsdfg","___1___profile");
            bottom_navigation.setSelectedItemId(R.id.messageFragment);
        }
        MessageFragment myFragment3 = (MessageFragment)getSupportFragmentManager().findFragmentByTag("message");
        if (myFragment3 != null && myFragment3.isVisible()) {
            Log.d("sgfsdfgsdfg","___1___message");
            bottom_navigation.setSelectedItemId(R.id.profileFragment);
        }*/


       /*
        FragmentManager fragmentManager = UserHomeActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible()){
                if (fragment instanceof HomeFragment) {
                    Log.d("sgfsdfgsdfg",fragment.getTag()+"___1___");
                }else if(fragment instanceof NotificationFragment){
                    Log.d("sgfsdfgsdfg",fragment.getTag()+"___2___");
                }else if(fragment instanceof MessageFragment){
                    Log.d("sgfsdfgsdfg",fragment.getTag()+"_____3_");
                }else if(fragment instanceof ProfileFragment){
                    Log.d("sgfsdfgsdfg",fragment.getTag()+"____4__");
                }
            }

        }*/

       /* if (getFragmentManager().getBackStackEntryCount() > 0 ){
            Log.d("asfasdf","________________________________");
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
//            showExit();
        }
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
////            showExit();
//        }*/
    }

    private void showExit() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to exit the app?");
        btnRemove.setText("EXIT");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void filterUsers() {
        View view = getLayoutInflater().inflate(R.layout.filter_fragment_bottom_sheet, null);
        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(view);

        EditText etSearch = view.findViewById(R.id.etSearch);
        LinearLayout linearTalents = view.findViewById(R.id.linearTalents);
        LinearLayout linearProductsAndServices = view.findViewById(R.id.linearProductsAndServices);
        LinearLayout linearBrands = view.findViewById(R.id.linearBrands);
        imgTalents = view.findViewById(R.id.imgTalents);
        imgProductsAndServices = view.findViewById(R.id.imgProductsAndServices);
        imgBrands = view.findViewById(R.id.imgBrands);

        EditText editText_price_range = view.findViewById(R.id.editText_price_range);
        RecyclerView recyclerViewTalents = view.findViewById(R.id.recyclerViewTalents);
        RecyclerView recyclerViewProductsAndService = view.findViewById(R.id.recyclerViewProductsAndService);
        RecyclerView recyclerViewBrands = view.findViewById(R.id.recyclerViewBrands);
        TextView tvSubmit = view.findViewById(R.id.tvSubmit);

        linearTalents.setOnClickListener(this);
        linearProductsAndServices.setOnClickListener(this);
        linearBrands.setOnClickListener(this);

        tvSubmit.setOnClickListener(view1 -> {
        });

        dialog.show();
    }

    private CategoryData getCategoryData(String title) {
        for (CategoryData categoryData : listCategorySpinnerData) {
            if (categoryData.getTitle() != null && title.equals(categoryData.getTitle())) {
                return categoryData;
            }
        }
        return null;
    }

    private void showTalentsDialog() {
        dialogTalents = new Dialog(this);
        dialogTalents.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTalents.setCancelable(true);
        dialogTalents.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = dialogTalents.findViewById(R.id.editText_price_search);
        editText_price_search.setHint(getString(R.string.talents));
        RecyclerView rvCurrency = dialogTalents.findViewById(R.id.rvCurrency);
        addTalentDataList.clear();
        addTalentDataList.addAll(addTalentDataList2);

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);

        talentsAdapter = new TalentsAdapter(this, addTalentDataList, pos -> {
            tvSelectTalents.setText(addTalentDataList.get(pos).getName());
            profileTalent = addTalentDataList.get(pos);
            if (OngoingAssignmets) {
                Toast.makeText(UserHomeActivity.this, "Filter will be done as per your selected Talent", Toast.LENGTH_LONG).show();
                selectedProjectId = Integer.parseInt(null);
                OngoingAssignmets = false;
                arrayAdapter.notifyDataSetChanged();
                spinnerTalents.setSelection(0);
            }
            dialogTalents.dismiss();
        });
        rvCurrency.setAdapter(talentsAdapter);

        editText_price_search.addTextChangedListener(this);
        dialogTalents.setCanceledOnTouchOutside(true);
        dialogTalents.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogTalents.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MYPROFILE) {
            if (resultCode == RESULT_OK) {
                setNavHeaderData(this);
            }
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof ProfileData) {
            setTalents((ProfileData) o);
        }
        if (o instanceof CategoryCurrency) {
            setCategoryCurrency((CategoryCurrency) o);
        }
        if (o instanceof ProductCategory) {
            ProductCategory homeUserDetail = (ProductCategory) o;
            Productlist = homeUserDetail.getData();
            selectProductAdapter = new SelectProductAdapter(this, Productlist, pos -> {
                tvSelectProducts.setText(Productlist.get(pos).getTitle());
                tvSelectProducts.setTextColor(getResources().getColor(R.color.black));
                ProductCategory = Productlist.get(pos).getTitle();
                dialogProduct.dismiss();
            });
            rvProduct.setAdapter(selectProductAdapter);
        }
        if (o instanceof ServiceCategory) {
            ServiceCategory homeUserDetail = (ServiceCategory) o;
            Servicelist = homeUserDetail.getData();
            selectServiceAdapter = new SelectServiceAdapter(this, Servicelist, pos -> {
                tvSelectService.setText(Servicelist.get(pos).getTitle());
                tvSelectService.setTextColor(getResources().getColor(R.color.black));
                ServiceCategory = Servicelist.get(pos).getTitle();
                dialogService.dismiss();
            });
            rvServices.setAdapter(selectServiceAdapter);
        }
        if (o instanceof PreferredLanguage) {
            PreferredLanguage homeUserDetail = (PreferredLanguage) o;
            preferredLanguageDataList = homeUserDetail.getData();
            preferedLangAdapter = new PreferedLangAdapter(this, preferredLanguageDataList, pos -> {
                tv_select_Servicelanguage.setText(preferredLanguageDataList.get(pos).getName());
                tv_select_Servicelanguage.setTextColor(getResources().getColor(R.color.black));
                Language = preferredLanguageDataList.get(pos).getName();
                preferredlangDialog.dismiss();
            });
            rvLanguages.setAdapter(preferedLangAdapter);
        }
        if (o instanceof TalantCat) {
            TalantCat homeUserDetail = (TalantCat) o;
            Projectlist = homeUserDetail.getData();
            selectProjectAdapter = new SelectProjectAdapter(this, Projectlist, pos -> {
                tvSelectProject.setText(Projectlist.get(pos).getTitle());
                tvSelectProject.setTextColor(getResources().getColor(R.color.black));
                Projects = Projectlist.get(pos).getTitle();
                dialogProject.dismiss();
            });
            rvProjects.setAdapter(selectProjectAdapter);
        }
    }

    private void setTalents(ProfileData profileData) {
        if (!(profileData.getTalents().isEmpty())) {
            addTalentDataList.addAll(profileData.getTalents());
            addTalentDataList2.addAll(profileData.getTalents());
            showTalentsDialog();
        } else {
            Toast.makeText(this, "No talents found!!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (i == 0) {
            ageRange = "";
        } else {
            ageRange = ageRAngeArray[i];
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        filter(s.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void filter(String charText) {
        String newCharText = charText.toLowerCase(Locale.getDefault()).trim();
        addTalentDataList.clear();
        if (charText.isEmpty()) {
            addTalentDataList.addAll(addTalentDataList2);
        } else {
            for (ProfileTalent profileTalent : addTalentDataList2) {
                if (profileTalent.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                    addTalentDataList.add(profileTalent);
                }
            }
        }
        talentsAdapter.notifyDataSetChanged();
    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1", "size:" + categoryDataList1.size() + ",  " + categoryDataList1.toString());

        List<CurrencyData> currencyDataList1 = categoryCurrency.getData().getPrice();
        if (currencyDataList1 != null && !(currencyDataList1.isEmpty())) {
            currencyDataList.clear();
            currencyDataList2.clear();
            currencyDataList2.addAll(currencyDataList1);
            currencyDataList.addAll(currencyDataList1);
        }
    }

    public void getCategory() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getCategoryCurrency(this, coordinatorParent);
        } else {
            Utils.warningSnackBar(this, coordinatorParent, getString(R.string.no_internet));
        }
    }

    private void showCurrencyDialog() {
        dialogCurrency = new Dialog(this);
        dialogCurrency.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCurrency.setCancelable(true);
        dialogCurrency.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogCurrency.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Country Currency");
        RecyclerView rvCurrency = dialogCurrency.findViewById(R.id.rvCurrency);
        currencyDataList.clear();
        currencyDataList.addAll(currencyDataList2);
        selectCurrencyAdapter = new SelectCurrencyAdapter(this, currencyDataList, pos -> {

            switch (FilterType) {
                case "Talent":
                    tv_select_currency.setText(currencyDataList.get(pos).getCurrency());
                    tv_select_currency.setTextColor(getResources().getColor(R.color.black));
                    break;
                case "Product":
                    tv_select_Productcurrency.setText(currencyDataList.get(pos).getCurrency());
                    tv_select_Productcurrency.setTextColor(getResources().getColor(R.color.black));
                    break;
                case "Service":
                    tv_select_servicecurrency.setText(currencyDataList.get(pos).getCurrency());
                    tv_select_servicecurrency.setTextColor(getResources().getColor(R.color.black));
                    break;
            }
            currencyData = currencyDataList.get(pos);
            currencyCode = currencyData.getCode();
            dialogCurrency.dismiss();
        });


        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(selectCurrencyAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                currencyDataList.clear();
                if (s.toString().isEmpty()) {
                    currencyDataList.addAll(currencyDataList2);
                } else {
                    for (CurrencyData list : currencyDataList2) {
                        if (list.getCountry().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            currencyDataList.add(list);
                        }
                    }
                }
                selectCurrencyAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogCurrency.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogCurrency.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCurrency.show();
    }

    private void showProductDialog() {
        dialogProduct = new Dialog(this);
        dialogProduct.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogProduct.setCancelable(true);
        dialogProduct.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogProduct.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Products");
        rvProduct = dialogProduct.findViewById(R.id.rvCurrency);
        getProducts();
        rvProduct.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvProduct.getContext(),
                linearLayoutManager.getOrientation());
        rvProduct.addItemDecoration(dividerItemDecoration);
        rvProduct.setLayoutManager(linearLayoutManager);


        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                Productlist.clear();
                if (s.toString().isEmpty()) {
                    Productlist.addAll(Productlist);
                } else {
                    for (ProductCategory.Data list : Productlist) {
                        if (list.getTitle().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            Productlist.add(list);
                        }
                    }
                }
                selectProductAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogProduct.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogProduct.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogProduct.show();
    }

    private void showServiceDialog() {
        dialogService = new Dialog(this);
        dialogService.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogService.setCancelable(true);
        dialogService.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogService.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Services");
        rvServices = dialogService.findViewById(R.id.rvCurrency);
        getServices();
        rvServices.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvServices.getContext(),
                linearLayoutManager.getOrientation());
        rvServices.addItemDecoration(dividerItemDecoration);
        rvServices.setLayoutManager(linearLayoutManager);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                Servicelist.clear();
                if (s.toString().isEmpty()) {
                    Servicelist.addAll(Servicelist);
                } else {
                    for (ServiceCategory.Data list : Servicelist) {
                        if (list.getTitle().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            Servicelist.add(list);
                        }
                    }
                }
                selectServiceAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogService.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogService.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogService.show();
    }

    private void showProjectDialog() {
        dialogProject = new Dialog(this);
        dialogProject.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogProject.setCancelable(true);
        dialogProject.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogProject.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Projects");
        rvProjects = dialogProject.findViewById(R.id.rvCurrency);
        getProjectList();
        rvProjects.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvProjects.getContext(),
                linearLayoutManager.getOrientation());
        rvProjects.addItemDecoration(dividerItemDecoration);
        rvProjects.setLayoutManager(linearLayoutManager);


        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                Projectlist.clear();
                if (s.toString().isEmpty()) {
                    Projectlist.addAll(Projectlist);
                } else {
                    for (TalantCat.Data list : Projectlist) {
                        if (list.getTitle().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            Projectlist.add(list);
                        }
                    }
                }
                selectProjectAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogProject.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogProject.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogProject.show();
    }

    private void showPreferredLangDialog() {
        preferredlangDialog = new Dialog(this);
        preferredlangDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        preferredlangDialog.setCancelable(true);
        preferredlangDialog.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = preferredlangDialog.findViewById(R.id.editText_price_search);
        editText_price_search.setHint(getString(R.string.preferred_language));
        rvLanguages = preferredlangDialog.findViewById(R.id.rvCurrency);
        getLanguages();
        rvLanguages.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvLanguages.getContext(), linearLayoutManager.getOrientation());
        rvLanguages.addItemDecoration(dividerItemDecoration);
        rvLanguages.setLayoutManager(linearLayoutManager);

        preferredlangDialog.setCanceledOnTouchOutside(true);
        preferredlangDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        preferredlangDialog.show();
    }

    private void showDurationDialog() {
        dialogDuration = new Dialog(this);
        dialogDuration.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogDuration.setCancelable(true);
        dialogDuration.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogDuration.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Duration");
        RecyclerView rvCurrency = dialogDuration.findViewById(R.id.rvCurrency);
        durationList.add("Per Week");
        durationList.add("Per Month");
        durationList.add("Per Day");
        durationList.clear();
        durationList.addAll(durationList1);
        selectDurationAdapter = new SelectDurationAdapter(this, durationList, pos -> {
            tv_select_Duration.setText(durationList.get(pos));
            tv_select_Duration.setTextColor(getResources().getColor(R.color.black));
            switch (durationList.get(pos)) {
                case "Per Week":
                    duration = "1";
                    break;
                case "Per Month":
                    duration = "2";
                    break;
                case "Per Day":
                    duration = "0";
                    break;
            }

            dialogDuration.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(selectDurationAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                durationList.clear();
                if (s.toString().isEmpty()) {
                    durationList.addAll(durationList1);
                } else {
                    for (String list : durationList1) {
                        if (list.toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            durationList.add(list);
                        }
                    }
                }
                selectDurationAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogDuration.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogDuration.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogDuration.show();
    }
    private void showTourDialog() {
        Dialog   dialogTour = new Dialog(this);
        dialogTour.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTour.setCancelable(true);
        dialogTour.setContentView(R.layout.layout_tour);

        ViewPager viewpager = dialogTour.findViewById(R.id.viewpager);
        TabLayout tabLayout = (TabLayout) dialogTour.findViewById(R.id.tab_layout);
        tourlist.add(new TourModel(R.drawable.exp1,"1"));
        tourlist.add(new TourModel(R.drawable.exp2,"2"));
        tourlist.add(new TourModel(R.drawable.exp3,"3"));
        tourlist.add(new TourModel(R.drawable.exp4,"4"));
        tourlist.add(new TourModel(R.drawable.exp5,"5"));
        tourlist.add(new TourModel(R.drawable.exp6,"6"));
        tourlist.add(new TourModel(R.drawable.tour_img,"7"));
        TourAdapter adapter = new TourAdapter(tourlist, UserHomeActivity.this,dialogTour);
        viewpager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewpager, true);
        dialogTour.setCanceledOnTouchOutside(false);
        Objects.requireNonNull(dialogTour.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogTour.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        QBUser currentUser = AppPreferences.getInstance().getQbUser();
        if (currentUser != null && !QBChatService.getInstance().isLoggedIn()) {
            Log.d("TAG", "Resuming with Relogin");
            ChatHelper.getInstance().login(currentUser, new QBEntityCallback<QBUser>() {
                @Override
                public void onSuccess(QBUser qbUser, Bundle bundle) {
                    Log.d("TAG", "Relogin Successful");
                    reloginToChat();
                }

                @Override
                public void onError(QBResponseException e) {
                    Log.d("TAG", e.getMessage());
                }
            });
        } else {
            Log.d("TAG", "Resuming without Relogin to Chat");
            onResumeFinished();
        }
    }

    private void reloginToChat() {
        ChatHelper.getInstance().loginToChat(AppPreferences.getInstance().getQbUser(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                Log.d("TAG", "Relogin to Chat Successful");
                onResumeFinished();
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("TAG", "Relogin to Chat Error: " + e.getMessage());
                onResumeFinished();
            }
        });
    }

    public void onResumeFinished() {
        // Need to Override onResumeFinished() method in nested classes if we need to handle returning from background in Activity
    }

    private void prepareUser() {
        QBUser qbUser = new QBUser();
        qbUser.setLogin(ApplicationClass.appPreferences.getPhoneNumber());
//         qbUser.setFullName(usernameEt.getText().toString().trim());
        qbUser.setPassword(ApplicationClass.appPreferences.getPhoneNumber());
        //qbUser.setPassword(App.USER_DEFAULT_PASSWORD);
        signIn(qbUser);
    }

    private void signIn(final QBUser user) {

        ChatHelper.getInstance().login(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser userFromRest, Bundle bundle) {
                if (userFromRest.getId().equals(user.getId())) {
                    loginToChat(user);
//                    Toast.makeText(UserHomeActivity.this, "login", Toast.LENGTH_SHORT).show();
                } else {
                    //Need to set password NULL, because server will update user only with NULL password
                    user.setPassword(null);
                    Toast.makeText(UserHomeActivity.this, "register", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    private void loginToChat(final QBUser user) {
        //Need to set password, because the server will not register to chat without password
        user.setPassword(ApplicationClass.appPreferences.getPhoneNumber());
        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                AppPreferences.getInstance().saveQbUser(user);
                finish();
//                hideProgressDialog();
            }

            @Override
            public void onError(QBResponseException e) {
//                hideProgressDialog();
//                showErrorSnackbar(R.string.login_chat_login_error, e, null);
            }
        });
    }

    public void getProducts() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getProductCategory(this, coordinatorParent);
        } else {
            Utils.warningSnackBar(this, coordinatorParent, getString(R.string.no_internet));
        }
    }

    public void getServices() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getServiceCategory(this, coordinatorParent);
        } else {
            Utils.warningSnackBar(this, coordinatorParent, getString(R.string.no_internet));
        }
    }

    public void getProjectList() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getTalentCat(this, coordinatorParent);
        } else {
            Utils.warningSnackBar(this, coordinatorParent, getString(R.string.no_internet));
        }
    }

    public void getLanguages() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getPreferredLang(this, coordinatorParent);
        } else {
            Utils.warningSnackBar(this, coordinatorParent, getString(R.string.no_internet));
        }
    }

    EditText et_Name;
    EditText et_ProductLocation ;
    EditText et_Location;
    EditText editText_price_range ;
    EditText editText_Productprice_range;
    EditText editText_servicelocation;
    private void openUserFiltersDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_location_filter);
        ImageView imgCancel = dialog.findViewById(R.id.imgCancel);
        TextView tvReset = dialog.findViewById(R.id.tvReset);
        et_Name = dialog.findViewById(R.id.et_Name);
        et_ProductLocation = dialog.findViewById(R.id.et_ProductLocation);
        et_Location = dialog.findViewById(R.id.et_Location);
        editText_price_range = dialog.findViewById(R.id.editText_price_range);
        editText_Productprice_range = dialog.findViewById(R.id.editText_Productprice_range);
        editText_servicelocation = dialog.findViewById(R.id.editText_servicelocation);
        LinearLayout layouttalentFilter = dialog.findViewById(R.id.layouttalentFilter);
        LinearLayout layoutProductFilter = dialog.findViewById(R.id.layoutProductFilter);
        LinearLayout layoutServiceFilter = dialog.findViewById(R.id.layoutServiceFilter);
        LinearLayout layoutProjectFilter = dialog.findViewById(R.id.layoutProjectFilter);
        RadioButton rbTalent = dialog.findViewById(R.id.rbTalent);
        RadioButton rbProduct = dialog.findViewById(R.id.rbProduct);
        RadioButton rbService = dialog.findViewById(R.id.rbService);
        RadioButton rvProject = dialog.findViewById(R.id.rvProject);

        rbTalent.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (rbTalent.isChecked()) {
                clearfield();
                filterUserData = new FilterUserData();
                FilterType = "Talent";
                type = "1";
                layouttalentFilter.setVisibility(View.VISIBLE);
                layoutProductFilter.setVisibility(View.GONE);
                layoutServiceFilter.setVisibility(View.GONE);
                layoutProjectFilter.setVisibility(View.GONE);
            } else layouttalentFilter.setVisibility(View.GONE);
        });

        rbProduct.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if (rbProduct.isChecked()) {
                clearfield();
                filterUserData = new FilterUserData();
                FilterType = "Product";
                type = "2";
                layoutProductFilter.setVisibility(View.VISIBLE);
                layouttalentFilter.setVisibility(View.GONE);
                layoutServiceFilter.setVisibility(View.GONE);
                layoutProjectFilter.setVisibility(View.GONE);
            } else layoutProductFilter.setVisibility(View.GONE);
        });

        rbService.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (rbService.isChecked()) {
                clearfield();
                filterUserData = new FilterUserData();
                FilterType = "Service";
                type = "3";
                layoutServiceFilter.setVisibility(View.VISIBLE);
                layoutProjectFilter.setVisibility(View.GONE);
                layouttalentFilter.setVisibility(View.GONE);
                layoutProductFilter.setVisibility(View.GONE);
            } else layoutServiceFilter.setVisibility(View.GONE);
        });

        rvProject.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (rvProject.isChecked()) {
                clearfield();
                filterUserData = new FilterUserData();
                FilterType = "Project";
                layoutProjectFilter.setVisibility(View.VISIBLE);
                layouttalentFilter.setVisibility(View.GONE);
                layoutProductFilter.setVisibility(View.GONE);
                layoutServiceFilter.setVisibility(View.GONE);
            } else layoutProjectFilter.setVisibility(View.GONE);
        });

        spinnerTalents = dialog.findViewById(R.id.spinnerTalents);
        tvSelectTalents = dialog.findViewById(R.id.tvSelectTalents);
        CardView cardViewMyTalents = dialog.findViewById(R.id.cardViewMyTalents);
        Spinner spinnerAgeRange = dialog.findViewById(R.id.spinnerAgeRange);
        Spinner spinnerserviceAgeRange = dialog.findViewById(R.id.spinnerserviceAgeRange);
        Spinner spinnerprojectAgeRange = dialog.findViewById(R.id.spinnerprojectAgeRange);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, ageRAngeArray);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinnerAgeRange.setAdapter(arrayAdapter);
        spinnerserviceAgeRange.setAdapter(arrayAdapter);
        spinnerprojectAgeRange.setAdapter(arrayAdapter);
        linearongoingAssignment = dialog.findViewById(R.id.linearongoingAssignment);
        tvOngoingAssignnmets = dialog.findViewById(R.id.tvOngoingAssignnmets);
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getCategoryCurrency(this, coordinatorParent);
        } else {
            Utils.warningSnackBar(this, coordinatorParent, getString(R.string.no_internet));
        }

        spinnerTalents.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    selectedProjectId = 0;
                } else {

                    Toast.makeText(UserHomeActivity.this, "Filter will be done as per your selected Project", Toast.LENGTH_LONG).show();
//                    Toast.makeText(UserHomeActivity.this, listTalentsSpinnerId.get(i)+"  ", Toast.LENGTH_SHORT).show();
                    profileTalent = null;
                    selectedProjectId = listTalentsSpinnerId.get(i);
                    OngoingAssignmets = true;
                    addTalentDataList.clear();
                    tvSelectTalents.setHint("Select Talents");
                    tvSelectTalents.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ll_male = dialog.findViewById(R.id.ll_male);
        ll_female = dialog.findViewById(R.id.ll_female);
        ll_other = dialog.findViewById(R.id.ll_other);
        imgMale = dialog.findViewById(R.id.imgMale);
        imgfemale = dialog.findViewById(R.id.imgFemale);
        imgOther = dialog.findViewById(R.id.imgOther);

        ll_servicemale = dialog.findViewById(R.id.ll_servicemale);
        ll_servicefemale = dialog.findViewById(R.id.ll_servicefemale);
        ll_serviceother = dialog.findViewById(R.id.ll_serviceother);
        imgserviceMale = dialog.findViewById(R.id.imgserviceMale);
        imgserviceFemale = dialog.findViewById(R.id.imgserviceFemale);
        imgserviceOther = dialog.findViewById(R.id.imgserviceOther);

        ll_projectemale = dialog.findViewById(R.id.ll_projectemale);
        ll_projectnopref = dialog.findViewById(R.id.ll_projectnopref);
        ll_projectfemale = dialog.findViewById(R.id.ll_projectfemale);
        ll_projectother = dialog.findViewById(R.id.ll_projectother);
        imgprojectMale = dialog.findViewById(R.id.imgprojectMale);
        imgprojectnopref = dialog.findViewById(R.id.imgprojectnopref);
        imgprojectFemale = dialog.findViewById(R.id.imgprojectFemale);
        imgprojectOther = dialog.findViewById(R.id.imgprojectOther);

        ll_fullTime = dialog.findViewById(R.id.ll_fullTime);
        ll_PartTime = dialog.findViewById(R.id.ll_PartTime);
        imgFullTime = dialog.findViewById(R.id.imgFullTime);
        imgPartTime = dialog.findViewById(R.id.imgPartTime);
        tv_select_currency = dialog.findViewById(R.id.tv_select_currency);
        tvSelectProducts = dialog.findViewById(R.id.tvSelectProducts);
        tvSelectService = dialog.findViewById(R.id.tvSelectService);
        tv_select_Servicelanguage = dialog.findViewById(R.id.tv_select_Servicelanguage);
        tv_select_Productcurrency = dialog.findViewById(R.id.tv_select_Productcurrency);
        tv_select_servicecurrency = dialog.findViewById(R.id.tv_select_servicecurrency);
        tv_select_Duration = dialog.findViewById(R.id.tv_select_Duration);
        tvSelectProject = dialog.findViewById(R.id.tvSelectProject);

        cardViewMyTalents.setOnClickListener(this);
        spinnerAgeRange.setOnItemSelectedListener(this);
        ll_fullTime.setOnClickListener(this);
        ll_PartTime.setOnClickListener(this);
        ll_male.setOnClickListener(this);
        ll_female.setOnClickListener(this);
        ll_other.setOnClickListener(this);

        ll_servicemale.setOnClickListener(this);
        ll_servicefemale.setOnClickListener(this);
        ll_serviceother.setOnClickListener(this);

        ll_projectemale.setOnClickListener(this);
        ll_projectnopref.setOnClickListener(this);
        ll_projectfemale.setOnClickListener(this);
        ll_projectother.setOnClickListener(this);
        linearongoingAssignment.setOnClickListener(this);

        getCategory();

        tv_select_currency.setOnClickListener(v -> showCurrencyDialog());
        tv_select_Productcurrency.setOnClickListener(v -> showCurrencyDialog());
        tv_select_servicecurrency.setOnClickListener(v -> showCurrencyDialog());
        tvSelectProducts.setOnClickListener(v -> showProductDialog());
        tvSelectService.setOnClickListener(v -> showServiceDialog());
        tvSelectProject.setOnClickListener(v -> showProjectDialog());
        tv_select_Servicelanguage.setOnClickListener(v -> showPreferredLangDialog());
        tv_select_Duration.setOnClickListener(v -> showDurationDialog());

        if (filterUserData != null) {
            et_Name.setText(filterUserData.getName());
            et_Location.setText(filterUserData.getLocation());
            if (filterUserData.getTalantName() != null) {
                tvSelectTalents.setText(String.valueOf(filterUserData.getTalantName()));
            }
            switch (filterUserData.getGender()) {
                case "male":
                    imgMale.setImageResource(R.drawable.ic_radio_checked);
                    imgfemale.setImageResource(R.drawable.ic_radio_unchecked);
                    imgOther.setImageResource(R.drawable.ic_radio_unchecked);
                    break;
                case "female":
                    imgMale.setImageResource(R.drawable.ic_radio_unchecked);
                    imgfemale.setImageResource(R.drawable.ic_radio_checked);
                    imgOther.setImageResource(R.drawable.ic_radio_unchecked);
                    break;
                case "other":
                    imgMale.setImageResource(R.drawable.ic_radio_unchecked);
                    imgfemale.setImageResource(R.drawable.ic_radio_unchecked);
                    imgOther.setImageResource(R.drawable.ic_radio_checked);
                    break;
            }
            switch (filterUserData.getWorkType()) {
                case "Full Time":
                    imgPartTime.setImageResource(R.drawable.ic_radio_unchecked);
                    imgFullTime.setImageResource(R.drawable.ic_radio_checked);
                    break;
                case "Part Time":
                    imgPartTime.setImageResource(R.drawable.ic_radio_checked);
                    imgFullTime.setImageResource(R.drawable.ic_radio_unchecked);
                    break;
            }
        }

        imgCancel.setOnClickListener(view -> {
            profileTalent = null;
            gender = "";
            workType = "";
            ageRange = "";
            OngoingAssignmets = false;
            dialog.dismiss();
        });

        tvReset.setOnClickListener(view -> {
            dialog.dismiss();
//            type = "1";
            if (fragmentData != null) {
//                filterUserData.setName("");

                if(filterUserData == null) {
                    Toast.makeText(this, "No Filters Applied", Toast.LENGTH_SHORT).show();
                }

                else
                {
                filterUserData.setTalantName("");
                filterUserData.setGender("");
                filterUserData.setAgeRange("");
                filterUserData.setWorkType("");
                filterUserData.setProjectName("");
                filterUserData.setServiceName("");
                filterUserData.setServiceDay("");
                filterUserData.setServiceLocation("");
                filterUserData.setLanguage("");
                filterUserData.setProjectgender("");
                filterUserData.setProductCategory("");
                filterUserData.setProductLocation("");
                filterUserData.setProductCurrency("");
                filterUserData.setProductPrice("");
                fragmentData.sendData(filterUserData,"1",true);

                profileTalent = null;
                gender = "";
                workType = "";
                ageRange = "";
                listTalentsSpinner.clear();
            }
                }
        });

        Button button_ApplyFilter = dialog.findViewById(R.id.button_ApplyFilter);
        button_ApplyFilter.setOnClickListener(view -> {
            filterUserData = new FilterUserData();
            String name = et_Name.getText().toString().trim();
            String productLocation = et_ProductLocation.getText().toString().trim();
            String servicelocation = editText_servicelocation.getText().toString().trim();

            filterUserData.setName(name);
            filterUserData.setWorkType(workType);

            if (fragmentData != null) {
                if (FilterType.equals("Talent")) {
                    type = "1";
                    filterUserData.setName(name);
                    filterUserData.setWorkType(workType);
                    filterUserData.setFilterType("talent");
                    //optional
                    //optional
                    filterUserData.setGender(gender);
                    if (!(ageRange.isEmpty())) {
                        String[] ageNewArray = ageRange.split("\\s+");
                        String age = ageNewArray[2];
                        filterUserData.setAgeRange(age);
                    } else {
                        filterUserData.setAgeRange(null);
                    }
                    talentprice = editText_price_range.getText().toString();
                    filterUserData.setTalantCurrency(currencyCode);
                    if (profileTalent == null) {
                        filterUserData.setTalantName(null);
                    } else {
                        filterUserData.setTalantName(profileTalent.getName());
                    }
                    filterUserData.setTalantPrice(talentprice);


                    Log.e("fragmentdata", filterUserData.toString());
//                    filterUserData=null;
//                        profileTalent = null;
//                        gender = "";
//                        workType = "";
//                        ageRange = "";
//                        talentprice = "";
//                        OngoingAssignmets = false;
//                        listTalentsSpinner.clear();
                }
                if (FilterType.equals("Product")) {
                    type = "2";
                    filterUserData.setName(name);
                    filterUserData.setWorkType(workType);
                    filterUserData.setFilterType("product");
                    //optional
                    filterUserData.setProductCategory(ProductCategory);
                    filterUserData.setProductLocation(productLocation);
                    Productprice = editText_Productprice_range.getText().toString();
                    filterUserData.setProductCurrency(currencyCode);
                    filterUserData.setProductPrice(Productprice);
//                        fragmentData.sendData(filterUserData);

//                        Log.e("fragmentdata", filterUserData.toString());
////                    filterUserData=null;
//                        workType = "";
//                        Productprice = "";
//                        listTalentsSpinner.clear();

                }
                if (FilterType.equals("Service")) {
                    type = "3";
                    filterUserData.setName(name);
                    filterUserData.setWorkType(workType);
                    filterUserData.setGender(servicegender);
                    filterUserData.setFilterType("service");
                    if (!(ageRange.isEmpty())) {
                        String[] ageNewArray = ageRange.split("\\s+");
                        String age = ageNewArray[2];
                        filterUserData.setAgeRange(age);
                    } else {
                        filterUserData.setAgeRange(null);
                    }
                    //optional
                    filterUserData.setServiceName(ServiceCategory);
                    filterUserData.setServiceDay(duration);
                    filterUserData.setServiceLocation(servicelocation);
                    filterUserData.setLanguage(Language);
                }

                if (FilterType.equals("Project")) {
                    type = "1";
                    filterUserData.setName(name);
                    filterUserData.setWorkType(workType);
                    filterUserData.setProjectgender(projectgender);
                    filterUserData.setFilterType("project");
                    if (!(ageRange.isEmpty())) {
                        String[] ageNewArray = ageRange.split("\\s+");
                        String age = ageNewArray[2];
                        filterUserData.setAgeRange(age);
                    } else {
                        filterUserData.setAgeRange(null);
                    }
                    //optional
                    filterUserData.setProjectName(Projects);
                }

                fragmentData.sendData(filterUserData,type,true);
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void clearfield() {
        tv_select_currency.setText("");
        tv_select_Duration.setText("");
        tv_select_Productcurrency.setText("");
        tv_select_servicecurrency.setText("");
        tv_select_Servicelanguage.setText("");
        tvSelectProducts.setText("");
        tvSelectProject.setText("");
        tvSelectService.setText("");
        tvSelectTalents.setText("");
        et_Name.setText("");
        et_ProductLocation .setText("");
        et_Location.setText("");
        editText_price_range .setText("");
        editText_Productprice_range.setText("");
        editText_servicelocation.setText("");
    }

    public interface FragmentData {
        void sendData(FilterUserData data,String type,boolean show);
    }

    public static void setHomeItem(Activity activity) {
        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                activity.findViewById(R.id.nav_view);
        bottomNavigationView.setSelectedItemId(R.id.home);
    }


    public class TourAdapter extends PagerAdapter {

        private ArrayList<TourModel> models;
        private LayoutInflater layoutInflater;
        private Context context;
        Dialog dialog;

        public TourAdapter(ArrayList<TourModel> models, Context context,Dialog dialog) {
            this.models = models;
            this.context = context;
            this.dialog = dialog;
        }

        @Override
        public int getCount() {
            return models.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view.equals(object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, final int position) {
            layoutInflater = LayoutInflater.from(context);
            View view = layoutInflater.inflate(R.layout.tour_list, container, false);

            ImageView ivImage = view.findViewById(R.id.ivImage);
            TextView tvTitle = view.findViewById(R.id.tvTitle);
            RelativeLayout layoutJoin = view.findViewById(R.id.layoutJoin);
            LinearLayout layoutimages = view.findViewById(R.id.layoutimages);
            Button btn2 = view.findViewById(R.id.btn2);
            if(models.get(position).getTitle().equals("7")){
                layoutimages.setVisibility(View.GONE);
                layoutJoin.setVisibility(View.VISIBLE);
            }
            else {
                layoutimages.setVisibility(View.VISIBLE);
                layoutJoin.setVisibility(View.GONE);
            }
            ivImage.setImageResource(models.get(position).getImage());
            tvTitle.setText(models.get(position).getTitle());
            tvTitle.setVisibility(View.GONE);

            btn2.setOnClickListener(v -> {
                dialog.dismiss();
                isshow = false;
//                Toast.makeText(context, user, Toast.LENGTH_SHORT).show();
            });

            container.addView(view, 0);
            return view;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View)object);
        }
    }
}