package com.twc.tiecoon.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.R;

public class ChatListActivity extends AppCompatActivity {

    private RelativeLayout rlChat;
    private ImageView ivBack;
    private RecyclerView recycler_view_chat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        initView();

/*
        rlChat = findViewById(R.id.rlChat);
        rlChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ChatListActivity.this, ChatActivity.class));
            }
        });
*/

    }

    private void initView() {
        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        recycler_view_chat = (RecyclerView) findViewById(R.id.recycler_view_chat);
        recycler_view_chat.setAdapter(new CoursesAdapter());
    }


    public class CoursesAdapter extends RecyclerView.Adapter<CoursesAdapter.MyViewHolder> {
        /* Context context ;
                 List<FetchBloodRequestModel> lists;
                 private ArrayList<FetchBloodRequestModel> listpicOrigin;
                 private LayoutInflater mInflater;*/
        public CoursesAdapter(/*Context context, List<FetchBloodRequestModel> lists*/) {
          /*  this.context = context;
            this.lists = lists;
            //  mInflater = LayoutInflater.from(context);
            this.listpicOrigin = new ArrayList<FetchBloodRequestModel>();
            this.listpicOrigin.addAll(lists);*/
            // this.listpicOrigin=lists;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(ChatListActivity.this).inflate(R.layout.chatlistitem, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, final int i) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(ChatListActivity.this, ChatActivity.class));
                }
            });

        }

        @Override
        public int getItemCount() {
            return 20;
        }

        public long getItemId(int position) {
            return position;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {


            public MyViewHolder(View v) {
                super(v);

            }
        }
    }
}