package com.twc.tiecoon.Activities.PaymentModule;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.twc.tiecoon.Activities.UserHomeActivity;
import com.twc.tiecoon.R;

public class FailPaymentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fail_payment);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, UserHomeActivity.class));
    }

}