package com.twc.tiecoon.Activities;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.Adapters.SelectCategoryAdapter;
import com.twc.tiecoon.Adapters.SelectSubCategoryAdapter;
import com.twc.tiecoon.Adapters.TalentsAdapter;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.ProfileData;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.network.response.SubCategory;
import com.twc.tiecoon.network.response.SubCategoryData;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Observable;

public class UploadBrandsActivity extends AbstractFragmentActivity implements View.OnClickListener, TextWatcher {
    private final String[] PROFILE_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int PROFILE_PERMISSION_REQUEST = 501;
    private MultipartBody.Part multiBodyPart = null;
    private Uri photoURI;
    private static final int REQUEST_GALLERY_CODE = 505;
    private static final int REQUEST_CAMERA_CODE = 506;
    private boolean profilePermissionDeniedWithDontAskMeAgain = false;

    private LinearLayout linearParent, linearScreen;
    private CardView cardViewImgSelect;
    private ImageView iv_cancel, img_select,imgDemo;
    private TextView tvDone, tv_sub_category, tvUploadBrands, tvSelectTalents;
    private RecyclerView rvCategory, rvSubCategory;
    private EditText editText_Name, editText_price_range, et_description, editText_link1, editText_link2, editText_link3;
    private ProfileModel profileModel = new ProfileModel();
    private List<CategoryData> categoryDataList = new ArrayList<>();
    private SelectCategoryAdapter selectCategoryAdapter;
    private SelectSubCategoryAdapter selectSubCategoryAdapter;
    private List<SubCategoryData> subCategoryDataList = new ArrayList<>();
    private SubCategoryData subCategoryData = null;
    private CategoryData categoryData = null;
    private String uploadType = "";
    private boolean isCategory = false;
    private int categoryId = 0;
    private int subCategoryId = 0;
    private Object updateItem = null;
    private ProfileTalent profileTalent = null;
    private Dialog dialogTalents;
    private TalentsAdapter talentsAdapter;
    private List<ProfileTalent> addTalentDataList = new ArrayList<>();
    private List<ProfileTalent> addTalentDataList2 = new ArrayList<>();
    private AddBrandsData addBrandsData = null;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_upload_brands);
        initView();
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void initView() {
        linearParent = findViewById(R.id.linearParent);
        linearScreen = findViewById(R.id.linearScreen);
        iv_cancel = (ImageView) findViewById(R.id.iv_cancel);
        tvDone = findViewById(R.id.tvDone);
        imgDemo= findViewById(R.id.imgDemo);
        img_select = findViewById(R.id.img_select);
        tvSelectTalents = findViewById(R.id.tvSelectTalents);
        cardViewImgSelect = findViewById(R.id.cardViewImgSelect);

        editText_Name = findViewById(R.id.editText_Name);
        rvCategory = findViewById(R.id.rvCategory);
        tv_sub_category = findViewById(R.id.tv_sub_category);
        rvSubCategory = findViewById(R.id.rvSubCategory);

        editText_price_range = (EditText) findViewById(R.id.editText_price_range);
        et_description = (EditText) findViewById(R.id.et_description);
        editText_link1 = findViewById(R.id.editText_link1);
        editText_link2 = findViewById(R.id.editText_link2);
        editText_link3 = findViewById(R.id.editText_link3);
        tvUploadBrands = (TextView) findViewById(R.id.tvUploadBrands);

        iv_cancel.setOnClickListener(this);
        tvDone.setOnClickListener(this);
        img_select.setOnClickListener(this);
        cardViewImgSelect.setOnClickListener(this);
        tvSelectTalents.setOnClickListener(this);
        tvUploadBrands.setOnClickListener(this);

        if (getIntent().hasExtra("updateItem")) {
            addBrandsData = getIntent().getParcelableExtra("updateItem");
            setUpdateItem();
        }

        et_description.setOnTouchListener((view, event) -> {
            if (view.getId() == R.id.et_description) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
            }
            return false;
        });

        rvCategory.setHasFixedSize(true);
        rvCategory.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        selectCategoryAdapter = new SelectCategoryAdapter(this, categoryId, isCategory, categoryDataList,  new ItemClickListenerExtraParam() {
            @Override
            public void itemClick(int pos, String type) {
                if (type.equals("update")) {
                    categoryData = categoryDataList.get(pos);
                    getSubCategoryFromServer(categoryDataList.get(pos).getId());
                } else {
                    if (!(categoryId == categoryDataList.get(pos).getId())) {
                        categoryId=0;
                        subCategoryData=null;
                        categoryData = categoryDataList.get(pos);
                        if (selectSubCategoryAdapter != null && subCategoryId > 0) {
                            selectSubCategoryAdapter.updateId();
                        }
                        getSubCategoryFromServer(categoryDataList.get(pos).getId());
                    }
                }
            }
        });
        rvCategory.setAdapter(selectCategoryAdapter);

        rvSubCategory.setHasFixedSize(true);
        rvSubCategory.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        selectSubCategoryAdapter = new SelectSubCategoryAdapter(this, subCategoryId, subCategoryDataList, new ItemClickListener() {
            @Override
            public void itemClick(int pos) {
                subCategoryData = subCategoryDataList.get(pos);
            }
        });
        rvSubCategory.setAdapter(selectSubCategoryAdapter);
        getCategory();
    }

    private void setUpdateItem() {
        if (addBrandsData.getCatId() != null) {
            categoryId = Integer.parseInt(addBrandsData.getCatId());
        }
        if (addBrandsData.getSubcatId() != null) {
            subCategoryId = Integer.parseInt(addBrandsData.getSubcatId());
        }
        imgDemo.setVisibility(View.GONE);
        String imageURL = (new StringBuilder().append(Constant.ImageURL).append(addBrandsData.getPath()).append("/").append(addBrandsData.getImage())).toString();
        Glide.with(this).load(imageURL).error(R.drawable.dummy_place)
                .error(R.drawable.dummy_place)
                .into(img_select);
        if (addBrandsData.getName() != null) {
            editText_Name.setText(addBrandsData.getName());
        }
        if (addBrandsData.getPrice() != null) {
            editText_price_range.setText(addBrandsData.getPrice());
        }
        if (addBrandsData.getDescription() != null) {
            et_description.setText(addBrandsData.getDescription());
        }
        if (addBrandsData.getLink1() != null) {
            editText_link1.setText(addBrandsData.getLink1());
        }
        if (addBrandsData.getLink2() != null) {
            editText_link2.setText(addBrandsData.getLink2());
        }
        if (addBrandsData.getLink3() != null) {
            editText_link3.setText(addBrandsData.getLink3());
        }
        getTalents();
    }


    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof CategoryCurrency) {
            setCategoryCurrency((CategoryCurrency) o);
        } else if (o instanceof SubCategory) {
            List<SubCategoryData> subCategoryDataList1 = ((SubCategory) o).getData();
            if (subCategoryDataList1 != null && !(subCategoryDataList1.isEmpty())) {
                tv_sub_category.setVisibility(View.VISIBLE);
                subCategoryDataList.clear();
                subCategoryDataList.addAll(subCategoryDataList1);
                selectSubCategoryAdapter.notifyDataSetChanged();
            } else {
                subCategoryDataList.clear();
                selectSubCategoryAdapter.notifyDataSetChanged();
                tv_sub_category.setVisibility(View.GONE);
            }
        } else if (o instanceof AddBrandsData) {
            AddBrandsData uploadsBrandsData = (AddBrandsData) o;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("UploadData", uploadsBrandsData);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
        if (o instanceof ProfileData) {
            setTalents((ProfileData) o);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_cancel:
                onBackPressed();
                break;
            case R.id.tvDone:
                break;
            case R.id.cardViewImgSelect:
                checkPermissionForProfile();
                break;
            case R.id.img_select:
                checkPermissionForProfile();
                break;
            case R.id.tvSelectTalents:
                if (addTalentDataList2.isEmpty()) {
                    getTalents();
                } else {
                    showTalentsDialog();
                }
                break;
            case R.id.tvUploadBrands:
                submit();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PROFILE_PERMISSION_REQUEST) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else if (PermissionChecker.checkCallingOrSelfPermission(this,
                    Manifest.permission.CAMERA) != PermissionChecker.PERMISSION_GRANTED ||
                    PermissionChecker.checkCallingOrSelfPermission(this
                            , Manifest.permission.READ_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED ||
                    PermissionChecker.checkCallingOrSelfPermission(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
                profilePermissionDeniedWithDontAskMeAgain = true;
            } else {
                showPictureDialog();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {
                switch (requestCode) {
                    case REQUEST_GALLERY_CODE:
                        Uri imageUri = data.getData();
                        Bitmap bitmap = Utils.getScaledBitmap(this, imageUri);
                        Uri uri = Utils.getImageUri(this, bitmap);
                        Utils.cropImage(this, uri);
                        break;
                    case REQUEST_CAMERA_CODE:
                        Uri uri2 = photoURI;
                        Utils.cropImage(this, uri2);
                        break;

                    case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                        if (data != null) {
                            CropImage.ActivityResult result = CropImage.getActivityResult(data);
                            Uri resultUri = result.getUri();
                            setImage(resultUri);
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void checkPermissionForProfile() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                showPictureDialog();
            }
        } else {
            showPictureDialog();
        }
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Profile photo");
        String[] pictureDialogItems = {"Gallery", "Capture photo"};
        pictureDialog.setItems(pictureDialogItems, (dialog, which) -> {
            switch (which) {
                case 0:
                    Utils.choosePhotoFromGallery(this, REQUEST_GALLERY_CODE);
                    break;
                case 1:
                    photoURI = Utils.takePhotoFromCamera(this, REQUEST_CAMERA_CODE);
                    break;
            }
        });
        pictureDialog.setCancelable(true);
        pictureDialog.show();
    }

    private void setTalents(ProfileData profileData) {
        if (!(profileData.getTalents().isEmpty())) {
            addTalentDataList.addAll(profileData.getTalents());
            addTalentDataList2.addAll(profileData.getTalents());
            if (addBrandsData != null) {
                if (addBrandsData.getTalentId() != null) {
                    for (ProfileTalent profileTalent1 : profileData.getTalents()) {
                        if (profileTalent1.getId() == Integer.parseInt(addBrandsData.getTalentId())) {
                            tvSelectTalents.setText(profileTalent1.getName());
                            profileTalent = profileTalent1;
                        }
                    }
                }
            } else {
                showTalentsDialog();
            }
        } else {
            Toast.makeText(this, "No talents found!!", Toast.LENGTH_SHORT).show();
        }
    }


    private void showTalentsDialog() {
        dialogTalents = new Dialog(this);
        dialogTalents.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTalents.setCancelable(true);
        dialogTalents.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = dialogTalents.findViewById(R.id.editText_price_search);
        editText_price_search.setHint(getString(R.string.talents));
        RecyclerView rvCurrency = dialogTalents.findViewById(R.id.rvCurrency);
        addTalentDataList.clear();
        addTalentDataList.addAll(addTalentDataList2);

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);

        talentsAdapter = new TalentsAdapter(this, addTalentDataList, new ItemClickListener() {
            @Override
            public void itemClick(int pos) {
                String name = addTalentDataList.get(pos).getName().substring(0, 1).toUpperCase() +addTalentDataList.get(pos).getName().substring(1);
                tvSelectTalents.setText(name);
                profileTalent = addTalentDataList.get(pos);
                dialogTalents.dismiss();
            }
        });
        rvCurrency.setAdapter(talentsAdapter);

        editText_price_search.addTextChangedListener(this);
        dialogTalents.setCanceledOnTouchOutside(true);
        dialogTalents.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogTalents.show();
    }

    private void setImage(Uri uri) {
        String filePath = Utils.getUriRealPath(this, uri);
        File file = new File(filePath);
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
//        editivProfile.setImageBitmap(bitmap);
        try {
            imgDemo.setVisibility(View.GONE);
            img_select.setImageBitmap(Utils.getScaledBitmap(this, uri));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.d("File", "Filename " + file.getName());
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
        //  RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        multiBodyPart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        //RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
    }

    private void getTalents() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getUserDetails(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void getSubCategoryFromServer(Integer id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getSubCategory(this, linearParent, id);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        if (categoryDataList1 != null && !(categoryDataList1.isEmpty())) {
            linearScreen.setVisibility(View.VISIBLE);
            if (isCategory) {
                CategoryData categoryData = getCategoryData(categoryDataList1);
                categoryDataList.clear();
                categoryDataList.add(categoryData);
                selectCategoryAdapter.notifyDataSetChanged();
            } else {
                categoryDataList.clear();
                categoryDataList.addAll(categoryDataList1);
                selectCategoryAdapter.notifyDataSetChanged();
            }
        }
    }

    private CategoryData getCategoryData(List<CategoryData> categoryDataList1) {
        for (CategoryData categoryData : categoryDataList1) {
            if (categoryData.getId() == categoryId) {
                return categoryData;
            }
        }
        return null;
    }

    private void submit() {
        if (addBrandsData == null) {
            if (multiBodyPart == null) {
                Toast.makeText(this, "Please select image", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (tvSelectTalents.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please select talent", Toast.LENGTH_SHORT).show();
            return;
        }
        if (addBrandsData == null) {
            if (categoryData == null) {
                Toast.makeText(this, "Please select category", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (TextUtils.isEmpty(editText_Name.getText().toString().trim())) {
            Toast.makeText(this, "Please enter name", Toast.LENGTH_SHORT).show();
            return;
        }

//        if (subCategoryData == null) {
//            Toast.makeText(this, "Please select subcategory", Toast.LENGTH_SHORT).show();
//            return;
//        }

        if (TextUtils.isEmpty(editText_price_range.getText().toString().trim())) {
            Toast.makeText(this, "Please enter price", Toast.LENGTH_SHORT).show();
            return;
        }


        if (TextUtils.isEmpty(et_description.getText().toString().trim())) {
            Toast.makeText(this, "Please enter description", Toast.LENGTH_SHORT).show();
            return;
        }
        uploadBrands();
    }

    private void uploadBrands() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            if (addBrandsData != null) {
                if (subCategoryData == null) {
                    if (profileTalent == null) {
                        if (categoryData == null) {
                            profileModel.updateBrands(this, linearParent, multiBodyPart, "", "",
                                    "", editText_Name.getText().toString().trim(), editText_price_range.getText().toString().trim(),
                                    et_description.getText().toString().trim(), editText_link1.getText().toString().trim(), editText_link2.getText().toString().trim(), editText_link3.getText().toString().trim(),
                                    String.valueOf(addBrandsData.getId()));
                        } else {
                            profileModel.updateBrands(this, linearParent, multiBodyPart, "", String.valueOf(categoryData.getId()),
                                    "", editText_Name.getText().toString().trim(), editText_price_range.getText().toString().trim(),
                                    et_description.getText().toString().trim(), editText_link1.getText().toString().trim(), editText_link2.getText().toString().trim(), editText_link3.getText().toString().trim(),
                                    String.valueOf(addBrandsData.getId()));
                        }
                    } else {
                        profileModel.updateBrands(this, linearParent, multiBodyPart, String.valueOf(profileTalent.getId()), String.valueOf(categoryData.getId()),
                                "", editText_Name.getText().toString().trim(), editText_price_range.getText().toString().trim(),
                                et_description.getText().toString().trim(), editText_link1.getText().toString().trim(), editText_link2.getText().toString().trim(), editText_link3.getText().toString().trim(),
                                String.valueOf(addBrandsData.getId()));
                    }

                } else {
                    profileModel.updateBrands(this, linearParent, multiBodyPart, String.valueOf(profileTalent.getId()), String.valueOf(categoryData.getId()),
                            String.valueOf(subCategoryData.getId()), editText_Name.getText().toString().trim(), editText_price_range.getText().toString().trim(),
                            et_description.getText().toString().trim(), editText_link1.getText().toString().trim(), editText_link2.getText().toString().trim(), editText_link3.getText().toString().trim(),
                            String.valueOf(addBrandsData.getId()));
                }
            } else {
                if (subCategoryData == null) {
                    profileModel.uploadBrands(this, linearParent, multiBodyPart, String.valueOf(profileTalent.getId()), String.valueOf(categoryData.getId()),
                            "", editText_Name.getText().toString().trim(), editText_price_range.getText().toString().trim(),
                            et_description.getText().toString().trim(), editText_link1.getText().toString().trim(), editText_link2.getText().toString().trim(), editText_link3.getText().toString().trim());
                } else {
                    profileModel.uploadBrands(this, linearParent, multiBodyPart, String.valueOf(profileTalent.getId()), String.valueOf(categoryData.getId()),
                            String.valueOf(subCategoryData.getId()), editText_Name.getText().toString().trim(), editText_price_range.getText().toString().trim(),
                            et_description.getText().toString().trim(), editText_link1.getText().toString().trim(), editText_link2.getText().toString().trim(), editText_link3.getText().toString().trim());
                }
            }
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    public void getCategory() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getCategoryCurrency(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }


    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        super.onBackPressed();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        filter(s.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void filter(String charText) {
        String newCharText = charText.toLowerCase(Locale.getDefault()).trim();
        addTalentDataList.clear();
        if (charText.isEmpty()) {
            addTalentDataList.addAll(addTalentDataList2);
        } else {
            for (ProfileTalent profileTalent : addTalentDataList2) {
                if (profileTalent.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                    addTalentDataList.add(profileTalent);
                }
            }
        }
        talentsAdapter.notifyDataSetChanged();
    }
}
