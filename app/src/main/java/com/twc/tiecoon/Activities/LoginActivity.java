package com.twc.tiecoon.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.LoginModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.NewRegisterModel;
import com.twc.tiecoon.network.response.OTPVerification;
import com.twc.tiecoon.network.response.OTPVerificationData;
import com.twc.tiecoon.network.response.OTPVerificationDataUser;
import com.twc.tiecoon.network.response.UserFeedback;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;
import com.twc.tiecoon.utils.Utils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.hbb20.CountryCodePicker;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.camera.core.CameraX.getContext;

public class LoginActivity extends AbstractFragmentActivity implements View.OnClickListener , GoogleApiClient.OnConnectionFailedListener{
    String TAG = "LoginActivity";
    private LoginModel loginModel = new LoginModel();
    private RelativeLayout relativeParent, rlFacebook;
    private EditText etPhone,etEmail;
    private TextView tvVerify,tvtitle,tvtitlemobile;
    private CallbackManager callbackManager;
    private LoginManager loginManager;
    CountryCodePicker ccp;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 777;
    private CardView card_google_login,cardMobile,cardEmail;
    SignInButton btn_sign_in;


    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        initView();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    public  void signOut() {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }
    }

    private void initView() {
        relativeParent = findViewById(R.id.relativeParent);
        card_google_login = findViewById(R.id.card_google_login);
        rlFacebook = findViewById(R.id.rlFacebook);
        etPhone = findViewById(R.id.etPhone);
        etEmail = findViewById(R.id.etEmail);
        tvVerify = findViewById(R.id.tvVerify);
        btn_sign_in = findViewById(R.id.btn_sign_in);
        ccp = findViewById(R.id.ccp);
        cardMobile = findViewById(R.id.cardMobile);
        cardEmail = findViewById(R.id.cardEmail);
        tvtitle = findViewById(R.id.tvtitle);
        tvtitlemobile = findViewById(R.id.tvtitlemobile);

        tvtitle.setOnClickListener(v -> {
            cardMobile.setVisibility(View.GONE);
            tvtitle.setVisibility(View.GONE);
            cardEmail.setVisibility(View.VISIBLE);
            tvtitlemobile.setVisibility(View.VISIBLE);
        });

        tvtitlemobile.setOnClickListener(v -> {
            cardMobile.setVisibility(View.VISIBLE);
            tvtitle.setVisibility(View.VISIBLE);
            cardEmail.setVisibility(View.GONE);
            tvtitlemobile.setVisibility(View.GONE);
        });

        card_google_login.setOnClickListener(this);
        btn_sign_in.setOnClickListener(this);
        rlFacebook.setOnClickListener(this);
        tvVerify.setOnClickListener(this);
        callbackManager = CallbackManager.Factory.create();
        facebookLogin();
    }

    String userId="";
    @Override
    protected BasicModel getModel() {
        return loginModel;
    }



//    private void signUp(final QBUser newUser) {
//        AppPreferences.getInstance().removeQbUser();
//        QBUsers.signUp(newUser).performAsync(new QBEntityCallback<QBUser>() {
//            @Override
//            public void onSuccess(QBUser user, Bundle bundle) {
////                hideProgressDialog();
//                signIn(newUser);
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
////                hideProgressDialog();
////                showErrorSnackbar(R.string.login_sign_up_error, e, null);
//            }
//        });
//    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof OTPVerification) {
            OTPVerification otpVerification = (OTPVerification) o;
            if(otpVerification.getData().getUser()!=null)
            {
//                prepareUser();
                goToOtpActivity(otpVerification.getData().getUser());
            }

        } else if (o instanceof OTPVerificationData) {
            OTPVerificationData otpVerificationData = (OTPVerificationData) o;
            if (Boolean.parseBoolean(otpVerificationData.getProfileFirstStatus())) {
                ApplicationClass.appPreferences.SetToken(otpVerificationData.getToken());
                if (otpVerificationData.getUser() != null && otpVerificationData.getUser().getPhone() != null) {
                    ApplicationClass.appPreferences.setPhoneNumber(otpVerificationData.getUser().getPhone());
                }

                ApplicationClass.appPreferences.setFirstTime(Boolean.parseBoolean(otpVerificationData.getProfileFirstStatus()));
                OTPVerificationDataUser otpVerificationDataUser = otpVerificationData.getUser();
                if (otpVerificationDataUser != null) {
                    if (otpVerificationDataUser.getEmail() != null) {
                        ApplicationClass.appPreferences.setEmailId(otpVerificationDataUser.getEmail());
                    }
                    if (otpVerificationDataUser.getProfile() != null) {
                        if (otpVerificationDataUser.getPath() != null) {
                            String imageURL = Constant.ImageURL + otpVerificationDataUser.getPath() + "/" + otpVerificationDataUser.getProfile();
                            ApplicationClass.appPreferences.setUserPicUrl(imageURL);
                        } else {
                            ApplicationClass.appPreferences.setUserPicUrl(otpVerificationDataUser.getProfile());
                        }
                    }
                    if (otpVerificationDataUser.getUserDetils() != null) {
                        if (otpVerificationDataUser.getUserDetils().getName() != null) {
                            ApplicationClass.appPreferences.setUserName(otpVerificationDataUser.getUserDetils().getName());
                        }
                        if (otpVerificationDataUser.getUserDetils().getUserId() != null) {
                            userId = String.valueOf(otpVerificationDataUser.getUserDetils().getUserId());
                            ApplicationClass.appPreferences.setUserId(otpVerificationDataUser.getUserDetils().getUserId());
                        }
                    }
                }
                Intent intent = new Intent(this, UserHomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else {
                ApplicationClass.appPreferences.SetToken(otpVerificationData.getToken());
                if(otpVerificationData.getPhone()==null)
                {
                }else {
                    Intent intent = new Intent(this, ProfileFirst_Activity.class);
                    intent.putExtra("first", true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.card_google_login:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.rlFacebook:
                if (Utils.isDeviceOnline(this)) {
                    loginManager.logInWithReadPermissions(this,
                            Arrays.asList("email", "public_profile"));
                } else {
                    Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
                }
                break;
            case R.id.tvVerify:
                verifyPhoneNumber();
//                startActivity(new Intent(this,MobileScreen.class));
                break;
        }
    }

    private void facebookLogin() {
        loginManager = LoginManager.getInstance();
        // If you are using in a fragment, call loginButton.setFragment(this);
        // Callback registration
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e(TAG, "loginResult = " + loginResult);
                getUserDetails(loginResult.getAccessToken());
            }
            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {
                Log.e(TAG, "onError = " + exception.getMessage());
            }

        });
    }

    private void verifyPhoneNumber() {
        String phone = etPhone.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        if(cardMobile.getVisibility() == View.VISIBLE){
            if (TextUtils.isEmpty(phone)) {
                Toast.makeText(this, "Please enter phone number", Toast.LENGTH_SHORT).show();
                return;
            }
            if (phone.length() < 7) {
                Toast.makeText(this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                return;
            }
            if(!(Utils.isValidMobile(phone))){
                Toast.makeText(this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                return;
            }
            String device_name = android.os.Build.MODEL;
            @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            if (Utils.isDeviceOnline(this)) {
                newRegister("phone",phone,android_id,device_name);
//            loginModel.registerByPhone(this,relativeParent,"phone",phone);
            }
            else {
                Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
            }
        }
        else if(cardEmail.getVisibility() == View.VISIBLE){
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();
                return;
            }
            if(!(Utils.isValidEmail(email))){
                Toast.makeText(this, "Please enter valid email", Toast.LENGTH_SHORT).show();
                return;
            }
            String device_name = android.os.Build.MODEL;
            @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            if (Utils.isDeviceOnline(this)) {
                newRegisterEmail("email",email,android_id,device_name);
//            loginModel.registerByPhone(this,relativeParent,"phone",phone);
            }
            else {
                Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
            }
        }

    }

    public void newRegister(String type,String phone,String DeviceId,String DeviceName) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<NewRegisterModel> call = apiInterface.newRegister(type,phone,DeviceId,DeviceName);

        call.enqueue(new Callback<NewRegisterModel>() {
            @Override
            public void onResponse(Call<NewRegisterModel> call, Response<NewRegisterModel> response) {
                dialog.dismiss();
                NewRegisterModel user = response.body();
                if(user.success){
                    ApplicationClass.appPreferences.SetToken(user.data.token);
                    ApplicationClass.appPreferences.setisverfy(Boolean.parseBoolean(user.data.isverfy));
                    ApplicationClass.appPreferences.setFirstTime(Boolean.parseBoolean(user.data.profile_first_status));
                    if(user.message.equals("Your Account is deactivated. Please contact to admin.")){
                        Toast.makeText(LoginActivity.this, user.message, Toast.LENGTH_SHORT).show();
                    }
                   else {
                        if(user.data.profile_first_status.equals("false")){
                            startActivity(new Intent(LoginActivity.this,MobileScreen.class)
                                    .putExtra("phone",phone).putExtra("userid",user.data.userid));
                        }
                        else {
                            Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
                            intent.putExtra("phone", phone);
                            intent.putExtra("otp",user.data.otp);
                            intent.putExtra("type","phone");
                            startActivity(intent);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<NewRegisterModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }

        });
    }

    public void newRegisterEmail(String type,String email,String DeviceId,String DeviceName) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<NewRegisterModel> call = apiInterface.newEmailRegister(type,email,DeviceId,DeviceName);

        call.enqueue(new Callback<NewRegisterModel>() {
            @Override
            public void onResponse(Call<NewRegisterModel> call, Response<NewRegisterModel> response) {
                dialog.dismiss();
                NewRegisterModel user = response.body();
                if(user.success){
                    ApplicationClass.appPreferences.SetToken(user.data.token);
                    ApplicationClass.appPreferences.setisverfy(Boolean.parseBoolean(user.data.isverfy));
                    ApplicationClass.appPreferences.setFirstTime(Boolean.parseBoolean(user.data.profile_first_status));
                    if(user.data.profile_first_status.equals("false")){
                        startActivity(new Intent(LoginActivity.this,MobileScreen.class)
                                .putExtra("email",email).putExtra("userid",user.data.userid));
                    }
                   else {
                        Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
                        intent.putExtra("email", email);
                        intent.putExtra("otp",user.data.otp);
                        intent.putExtra("type","email");
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onFailure(Call<NewRegisterModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    private void goToOtpActivity(OTPVerificationDataUser otpVerificationDataUser) {
        Intent intent = new Intent(this, OtpActivity.class);
        intent.putExtra("phone", etPhone.getText().toString());
        intent.putExtra("otp",String.valueOf(otpVerificationDataUser.getOtp()));
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount acct = completedTask.getResult(ApiException.class);
//            Log.d("iiiResult...", "handleSignInResult:" +acct.getEmail());
            String personName = acct.getDisplayName();
            String personPhotoUrl="";
           if(acct.getPhotoUrl()!=null){
                personPhotoUrl = acct.getPhotoUrl().toString();
           }

            String email = acct.getEmail();
            String id = acct.getId();
            String type= "google";
            //Log.e("iiiResult...", "Name: " + personName + ", email: " + email + ", Image: " + personPhotoUrl+ "Id: " +id);

//            Intent intent = new Intent(this, MobileScreen.class);
//            intent.putExtra("email", email);
//            intent.putExtra("id", id);
//            intent.putExtra("profileurl", personPhotoUrl);
//            intent.putExtra("type", "google");
//            startActivity(intent);
//            finish();
            String device_name = android.os.Build.MODEL;
            @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            newSocialRegister("gmail",email,id,android_id,device_name);
            // Signed in successfully, show authenticated UI.
//            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }

    public void newSocialRegister(String type,String email,String appid,String DeviceId,String DeviceName) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<NewRegisterModel> call = apiInterface.newSocialRegister(type,email,appid,DeviceId,DeviceName);

        call.enqueue(new Callback<NewRegisterModel>() {
            @Override
            public void onResponse(Call<NewRegisterModel> call, Response<NewRegisterModel> response) {
                dialog.dismiss();
                NewRegisterModel user = response.body();
                if(user.success){
                    ApplicationClass.appPreferences.SetToken(user.data.token);
                    ApplicationClass.appPreferences.setFirstTime(Boolean.parseBoolean(user.data.profile_first_status));
                    ApplicationClass.appPreferences.setisverfy(Boolean.parseBoolean(user.data.isverfy));
                    if(user.data.profile_first_status.equals("false")){
                        startActivity(new Intent(LoginActivity.this,MobileScreen.class)
                                .putExtra("type","gmail")
                                .putExtra("email",email).putExtra("userid",user.data.userid));
                    }
                    else {
                        Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
                        intent.putExtra("email", email);
                        intent.putExtra("otp",user.data.otp);
                        intent.putExtra("type","gmail");
                        startActivity(intent);
                    }
                }

            }

            @Override
            public void onFailure(Call<NewRegisterModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


    private void handleSignInResult(GoogleSignInResult result) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();

        Log.d("iiiResult...", "handleSignInResult:" + result.isSuccess() + result.getStatus());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();
            String id = acct.getId();
            String type= "google";
            //Log.e("iiiResult...", "Name: " + personName + ", email: " + email + ", Image: " + personPhotoUrl+ "Id: " +id);

            Intent intent = new Intent(this, MobileScreen.class);
            intent.putExtra("email", email);
            intent.putExtra("id", id);
            intent.putExtra("profileurl", personPhotoUrl);
            intent.putExtra("type", "google");
            startActivity(intent);
            finish();
//            tokentxt = getSharedPreferences("Firbase_Token", MODE_PRIVATE).getString("regId", "");
//            map.put("token", tokentxt);
//            Log.d("token","here it is my token : "+tokentxt );
//            socialLoginProcess(map);

//            startActivity(new Intent(Login.this, Register.class).putExtra("email", email).putExtra("name", personName).putExtra("tag", getIntent().getStringExtra("tag")));
//            finish();
            try {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("excetion","Exception"+e.toString());
            }
            dialog.dismiss();

        } else {
            dialog.dismiss();
        }
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    protected void getUserDetails(AccessToken currentAccessToken) {
        String appId = currentAccessToken.getApplicationId();
        GraphRequest data_request = GraphRequest.newMeRequest(currentAccessToken, (json_object, response) -> {
            try {
                String profileUrl = "";
                 String email = json_object.get("email").toString();
                String name = json_object.get("name").toString();
                String id = json_object.get("id").toString();

                String image_url = "https://graph.facebook.com/" + id + "/picture?type=normal";
                if (json_object.has("picture")) {
                    profileUrl = json_object.getJSONObject("picture").getJSONObject("data").getString("url");
                }
                Log.e("facebookdata",id+","+profileUrl+","+email);


                Intent intent = new Intent(this, MobileScreen.class);
                intent.putExtra("email", email);
                intent.putExtra("id", id);
                intent.putExtra("profileurl", profileUrl);
                intent.putExtra("type", "facebook");
                startActivity(intent);
                finish();

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "name,email,picture.type(large)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }

}