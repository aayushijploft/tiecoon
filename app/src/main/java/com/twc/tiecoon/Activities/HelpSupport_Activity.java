package com.twc.tiecoon.Activities;


import android.os.Bundle;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.HelpUserDetails;
import com.twc.tiecoon.utils.Utils;

import java.util.Observable;

public class HelpSupport_Activity extends AbstractFragmentActivity {

    EditText et_name,et_email,editText;
    RelativeLayout relativeParent;
    private final ProfileModel profileModel = new ProfileModel();

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_support_);

        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        editText = findViewById(R.id.editText);
        relativeParent = findViewById(R.id.relativeParent);

        et_name.setText(ApplicationClass.appPreferences.getUserName());
        et_email.setText(ApplicationClass.appPreferences.getEmailId());
        findViewById(R.id.iv_back).setOnClickListener(View-> onBackPressed());
        findViewById(R.id.btnSave).setOnClickListener(view -> CheckAssignmentValidation());
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void CheckAssignmentValidation() {
        if (et_name.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Name can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (et_email.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Email can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!(Utils.isValidEmail(et_email.getText().toString().trim()))) {
            Toast.makeText(this, "Email not valid", Toast.LENGTH_SHORT).show();
            return;
        }
        if (editText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Description can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        SubmitSupport();
    }

    private void SubmitSupport() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
                profileModel.sendHelp(this, relativeParent, et_name.getText().toString().trim(),et_email.getText().toString().trim()
                        ,editText.getText().toString().trim());

        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof HelpUserDetails) {
           Toast.makeText(this,"Submitted successfully",Toast.LENGTH_SHORT).show();
           onBackPressed();
        }
    }
}