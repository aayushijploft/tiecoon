package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.twc.tiecoon.R;

public class VideoViewActivity extends AppCompatActivity {
    private String mVideoUrl = "";
    private ProgressBar progress;
    String videourl="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_view);
        progress = ((ProgressBar) findViewById(R.id.progressBar1));
        progress.setVisibility(View.VISIBLE);
        if (getIntent().getExtras() != null) {
            mVideoUrl = getIntent().getExtras().getString("videourl");
        }
        playVideo();
    }
    private void playVideo() {
        VideoView videoview = (VideoView) findViewById(R.id.playvideo);
        Uri uri = Uri.parse(mVideoUrl);
        videoview.setVideoURI(uri);
        videoview.setMediaController(new MediaController(this));
        videoview.setOnErrorListener(new OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                progress.setVisibility(View.GONE);
                finish();
                return false;
            }
        });

        videoview.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progress.setVisibility(View.GONE);
                mp.setOnVideoSizeChangedListener(new OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int arg1,int arg2) {
                        progress.setVisibility(View.GONE);
                    }
                });
            }
        });

        videoview.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                finish();
            }
        });

        videoview.requestFocus();
        videoview.start();
    }
}