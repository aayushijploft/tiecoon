package com.twc.tiecoon.Activities;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.twc.tiecoon.Fragments.KycAccountFragment;
import com.twc.tiecoon.Fragments.KycCardFragment;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddKYC;
import com.twc.tiecoon.network.response.KycDetails;
import com.twc.tiecoon.utils.Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class AddKYCActivity extends AppCompatActivity {
    RelativeLayout relativeParent;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_k_y_c);

        viewPager = findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        relativeParent = findViewById(R.id.relativeParent);
        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0)
            {
                fragment = new KycAccountFragment();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            if (position == 0)
            {
                title = "Card Details";
            }
            return title;
        }
    }
}