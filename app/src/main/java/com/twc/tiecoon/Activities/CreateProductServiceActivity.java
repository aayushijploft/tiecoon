package com.twc.tiecoon.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.twc.tiecoon.Adapters.SelectColorAdapter;
import com.twc.tiecoon.Adapters.SelectCurrencyAdapter;
import com.twc.tiecoon.Adapters.SelectProductServiceTalentsAdapter;
import com.twc.tiecoon.Adapters.SelectSubCategoryAdapter;
import com.twc.tiecoon.Adapters.UploadImageListAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.CurrencyData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.SubCategory;
import com.twc.tiecoon.network.response.SubCategoryData;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.TalentCurrencyUserTalents;
import com.twc.tiecoon.network.response.TalentCurrencyUserTalentsData;
import com.twc.tiecoon.network.response.UploadImageListData;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.Utils;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Observable;


public class CreateProductServiceActivity extends AbstractFragmentActivity implements View.OnClickListener, TextWatcher {
    private final String[] PROFILE_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int PROFILE_PERMISSION_REQUEST = 501;
    private MultipartBody.Part multiBodyPart = null;
    private Uri photoURI;
    private static final int REQUEST_GALLERY_CODE = 505;
    private static final int REQUEST_CAMERA_CODE = 506;
    private boolean profilePermissionDeniedWithDontAskMeAgain = false;
    List<MultipartBody.Part> partList = new ArrayList<>();
    private LinearLayout linearParent, linearSpinnerService;
    private CardView cardViewImg;
    private ImageView iv_cancel, img_select, imgDemo;
    private Spinner spinnerAgeRange;
    private Spinner spinnerCategory;
    private String[] serviceDays = {"Select duration", "Per day", "Per week", "Per month"};
    private TextView tvDone, tvWatermarkDemo, textViewWaterMark, tvWatermark, tvEdit, tv_select_currency, tv_sub_category, tvSave;
    private RelativeLayout relativeImageView, relativeWaterMark, relativeWaterMarkBorder, relativeAdd, relativeRefresh, rlAdd;
    private SeekBar seekBar_textSize;
    private RecyclerView recyclerView_SelectColor, rvCategory, rvSubCategory;
    private EditText editText_price_range, et_description, et_Location;
    private ProfileModel profileModel = new ProfileModel();
    private List<String> listColor = new ArrayList<>();
    private List<String> listCategorySpinner = new ArrayList<>();
    private List<TalentCurrencyUserTalentsData> categoryDataList = new ArrayList<>();
    private List<CategoryData> categoryDataListNew = new ArrayList<>();
    private SelectProductServiceTalentsAdapter selectCategoryAdapter;
    private SelectSubCategoryAdapter selectSubCategoryAdapter;
    private List<SubCategoryData> subCategoryDataList = new ArrayList<>();
    private List<CurrencyData> currencyDataList = new ArrayList<>();
    private List<CurrencyData> currencyDataList2 = new ArrayList<>();
    private SelectCurrencyAdapter selectCurrencyAdapter;
    private Dialog dialogCurrency;
    private String watermarkColor = "";
    private SubCategoryData subCategoryData = null;
    private TalentCurrencyUserTalentsData categoryData = null;
    private CurrencyData currencyData = null;
    private String uploadType = "";
    private boolean isCategory = false;
    private int categoryId = 0;
    private int subCategoryId = 0;
    private Object updateItem = null;
    private int itemId = 0;
    private String currencyCode = "";
    private String serviceDaysSelected = "";
    private String title = "";
    List<Uri> uriList1 = new ArrayList<>();
    private List<UploadImageListData> uploadImageListData = new ArrayList<>();
    RecyclerView recyclerViewUploadImageList;
    private UploadImageListAdapter uploadImageListAdapter;
    private final static int FILE_REQUEST_CODE = 3;
    TextView tvTitle;
    List<MediaFile> mediaFiles;
    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_create_product_service);
        initView();
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void initView() {

        if (getIntent() != null && getIntent().hasExtra("type")) {
            uploadType = getIntent().getStringExtra("type");
        }

        if (getIntent() != null && getIntent().hasExtra("updateItem")) {
            updateItem = getIntent().getParcelableExtra("updateItem");
        }

        if (getIntent() != null && getIntent().hasExtra("categoryId")) {
            isCategory = true;
            categoryId = getIntent().getIntExtra("categoryId", 0);
        }
        uriList1 = new ArrayList<>();
        linearParent = findViewById(R.id.linearParent);
        iv_cancel = (ImageView) findViewById(R.id.iv_cancel);
        tvDone = findViewById(R.id.tvDone);
        imgDemo = findViewById(R.id.imgDemo);
        img_select = findViewById(R.id.img_select);
        cardViewImg = findViewById(R.id.cardViewImg);
        recyclerViewUploadImageList = findViewById(R.id.recyclerViewUploadImageList);
        relativeImageView = findViewById(R.id.relativeImageView);
        relativeWaterMark = (RelativeLayout) findViewById(R.id.relativeWaterMark);
        relativeWaterMarkBorder = (RelativeLayout) findViewById(R.id.relativeWaterMarkBorder);
        relativeAdd = (RelativeLayout) findViewById(R.id.relativeAdd);
        relativeRefresh = (RelativeLayout) findViewById(R.id.relativeRefresh);
        rlAdd = (RelativeLayout) findViewById(R.id.rlAdd);
        textViewWaterMark = findViewById(R.id.textViewWaterMark);
        tvTitle = findViewById(R.id.tvTitle);

        seekBar_textSize = (SeekBar) findViewById(R.id.seekBar_textSize);
        recyclerView_SelectColor = findViewById(R.id.recyclerView_SelectColor);
        tvWatermark = findViewById(R.id.tvWatermark);
        tvWatermarkDemo = findViewById(R.id.tvWatermarkDemo);
        tvEdit = (TextView) findViewById(R.id.tvEdit);
        et_Location = findViewById(R.id.et_Location);
        rvCategory = findViewById(R.id.rvCategory);
        tv_sub_category = findViewById(R.id.tv_sub_category);
        rvSubCategory = findViewById(R.id.rvSubCategory);
        linearSpinnerService = findViewById(R.id.linearSpinnerService);
        spinnerAgeRange = findViewById(R.id.spinnerAgeRange);
        spinnerCategory = findViewById(R.id.spinnerCategory);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, serviceDays);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinnerAgeRange.setAdapter(arrayAdapter);

        tv_select_currency = findViewById(R.id.tv_select_currency);
        editText_price_range = (EditText) findViewById(R.id.editText_price_range);
        et_description = (EditText) findViewById(R.id.et_description);
        tvSave = (TextView) findViewById(R.id.tvSave);

        iv_cancel.setOnClickListener(this);
        tvDone.setOnClickListener(this);
        img_select.setOnClickListener(this);
        relativeImageView.setOnClickListener(this);
        cardViewImg.setOnClickListener(this);
        textViewWaterMark.setOnClickListener(this);
        relativeWaterMarkBorder.setOnClickListener(this);
        tvWatermark.setOnClickListener(this);
        tvEdit.setOnClickListener(this);
        tv_select_currency.setOnClickListener(this);
        tvSave.setOnClickListener(this);

        tvWatermarkDemo.setText(ApplicationClass.appPreferences.getUserName());
        textViewWaterMark.setText(ApplicationClass.appPreferences.getUserName());

        uploadImageListAdapter = new UploadImageListAdapter(this, uploadImageListData);
        recyclerViewUploadImageList.setAdapter(uploadImageListAdapter);

        if (uploadType.equals("product")) {
            linearSpinnerService.setVisibility(View.GONE);
            tvTitle.setText("Add Products");

        }
        if (uploadType.equals("service")) {

            tvTitle.setText("Add Services");

        }

        et_description.setOnTouchListener((view, event) -> {
            if (view.getId() == R.id.et_description) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
            }
            return false;
        });


        setUpdateDataForItem();
        SelectColorAdapter selectColorAdapter = new SelectColorAdapter(this, getArrayList(), new ItemClickListener() {
            @Override
            public void itemClick(int pos) {
                textViewWaterMark.setTextColor(Color.WHITE);
                imgDemo.setVisibility(View.GONE);
                img_select.setVisibility(View.GONE);
                if (textViewWaterMark.getText().toString().isEmpty()) {
                    relativeWaterMark.setVisibility(View.VISIBLE);
                }
                relativeImageView.setVisibility(View.VISIBLE);
                Drawable drawable = relativeImageView.getBackground();
                drawable.setTint(Color.parseColor(listColor.get(pos)));
                relativeImageView.setBackground(drawable);
                multiBodyPart = null;
                watermarkColor = listColor.get(pos);
            }
        });
        recyclerView_SelectColor.setAdapter(selectColorAdapter);

        rvCategory.setHasFixedSize(true);
        rvCategory.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        selectCategoryAdapter = new SelectProductServiceTalentsAdapter(this, categoryId, isCategory, categoryDataList, new ItemClickListenerExtraParam() {
            @Override
            public void itemClick(int pos, String type) {
                if (type.equals("update")) {
                    categoryData = categoryDataList.get(pos);
                    // getSubCategoryFromServer(categoryDataList.get(pos).getId());
                } else {
                    if (!(categoryId == categoryDataList.get(pos).getId())) {
                        categoryId = 0;
                        subCategoryData = null;
                        categoryData = categoryDataList.get(pos);
                        if (selectSubCategoryAdapter != null && subCategoryId > 0) {
                            selectSubCategoryAdapter.updateId();
                        }
                        //  getSubCategoryFromServer(categoryDataList.get(pos).getId());
                    }
                }
            }
        });
        rvCategory.setAdapter(selectCategoryAdapter);

        rvSubCategory.setHasFixedSize(true);
        rvSubCategory.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        selectSubCategoryAdapter = new SelectSubCategoryAdapter(this, subCategoryId, subCategoryDataList, new ItemClickListener() {
            @Override
            public void itemClick(int pos) {
                subCategoryData = subCategoryDataList.get(pos);
            }
        });
        rvSubCategory.setAdapter(selectSubCategoryAdapter);

        seekBar_textSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                relativeWaterMark.setVisibility(View.GONE);
                textViewWaterMark.setVisibility(View.VISIBLE);
                textViewWaterMark.setTextSize(TypedValue.COMPLEX_UNIT_SP, progress);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        spinnerAgeRange.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    serviceDaysSelected = "";
                } else {
                    serviceDaysSelected = serviceDays[i];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    categoryId = 0;
                } else {
                    categoryId = getCategoryIdData(listCategorySpinner.get(i));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        getCategory();
    }


    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof CategoryCurrency) {
            setCategoryCurrency((CategoryCurrency) o);
        } else if (o instanceof SubCategory) {
            List<SubCategoryData> subCategoryDataList1 = ((SubCategory) o).getData();
            if (subCategoryDataList1 != null && !(subCategoryDataList1.isEmpty())) {
                subCategoryData = null;
                tv_sub_category.setVisibility(View.VISIBLE);
                subCategoryDataList.clear();
                subCategoryDataList.addAll(subCategoryDataList1);
                selectSubCategoryAdapter.notifyDataSetChanged();
            } else {
                subCategoryData = null;
                subCategoryDataList.clear();
                selectSubCategoryAdapter.notifyDataSetChanged();
                tv_sub_category.setVisibility(View.GONE);
            }
        } else if (o instanceof HomeUserDetailsProduct) {
            HomeUserDetailsProduct addProductData = (HomeUserDetailsProduct) o;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("UploadData", addProductData);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else if (o instanceof HomeUserDetailsService) {
            HomeUserDetailsService homeUserDetailsService = (HomeUserDetailsService) o;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("UploadData", homeUserDetailsService);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else if (o instanceof AddBrandsData) {
            AddBrandsData uploadsBrandsData = (AddBrandsData) o;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("UploadData", uploadsBrandsData);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else if (o instanceof HomeUserDetailsUploadTalent) {
            HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = (HomeUserDetailsUploadTalent) o;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("UploadData", homeUserDetailsUploadTalent);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_cancel:
                onBackPressed();
                break;
            case R.id.tvDone:

                break;
            case R.id.relativeImageView:
                checkPermissionForProfile();
                break;
            case R.id.img_select:
                checkPermissionForProfile();
                break;
            case R.id.cardViewImg:
                checkPermissionForProfile();
                break;
            case R.id.textViewWaterMark:
                openSetWaterMarkDialog();
                break;
            case R.id.relativeWaterMarkBorder:
                openSetWaterMarkDialog();
                break;
            case R.id.tvWatermark:
                //openPremiumWaterMarkDialog();
                break;
            case R.id.tvEdit:
                openSetWaterMarkDialog();
                break;
            case R.id.tv_select_currency:
                showCurrencyDialog();
                break;
            case R.id.rlAdd:
                break;
            case R.id.tvSave:
                // startActivity(new Intent(WatermarkActivity.this, ImagePreview.class));
                submit();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PROFILE_PERMISSION_REQUEST) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else if (PermissionChecker.checkCallingOrSelfPermission(this,
                    Manifest.permission.CAMERA) != PermissionChecker.PERMISSION_GRANTED ||
                    PermissionChecker.checkCallingOrSelfPermission(this
                            , Manifest.permission.READ_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED ||
                    PermissionChecker.checkCallingOrSelfPermission(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
                profilePermissionDeniedWithDontAskMeAgain = true;
            } else {
                showPictureDialog();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {
                switch (requestCode) {
                    case REQUEST_GALLERY_CODE:
                        Uri imageUri = data.getData();
                        Bitmap bitmap = Utils.getScaledBitmap(this, imageUri);
                        Uri uri = Utils.getImageUri(this, bitmap);
                        Utils.cropImage(this, uri);
                        break;
                    case REQUEST_CAMERA_CODE:
                        Uri uri2 = photoURI;
                        Utils.cropImage(this, uri2);
                        break;

                    case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                        if (data != null) {
                            CropImage.ActivityResult result = CropImage.getActivityResult(data);
                            Uri resultUri = result.getUri();
                            setImage(resultUri);
                        }
                        break;
                    case FILE_REQUEST_CODE:
                        mediaFiles = data.<MediaFile>getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);
                        MediaFile mediaFile = mediaFiles.get(0);
                        if(mediaFiles != null) {
                            Log.d("asdfas",mediaFiles.get(0).getPath());
                            try {
                                imgDemo.setVisibility(View.GONE);
                                textViewWaterMark.setTextColor(Color.WHITE);
                                img_select.setImageBitmap(Utils.getScaledBitmap(this,mediaFile.getUri()));
                                watermarkColor = "";
                                relativeImageView.setVisibility(View.GONE);
                                img_select.setVisibility(View.VISIBLE);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
// setMediaFiles(mediaFiles);
                        } else {
                            Toast.makeText(this, "Image not selected", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void checkPermissionForProfile() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                Intent intent = new Intent(this, FilePickerActivity.class);
                intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                        .setCheckPermission(true)
                        .setShowFiles(false)
                        .setShowImages(true)
                        .setShowAudios(false)
                        .setShowVideos(false)
                        .setIgnoreNoMedia(false)
                        .enableVideoCapture(false)
                        .enableImageCapture(false)
                        .setIgnoreHiddenFile(false)
                        .setMaxSelection(5)
                        .build());
                startActivityForResult(intent, FILE_REQUEST_CODE);
            }
        } else {
            Intent intent = new Intent(this, FilePickerActivity.class);
            intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowFiles(false)
                    .setShowImages(true)
                    .setShowAudios(false)
                    .setShowVideos(false)
                    .setIgnoreNoMedia(false)
                    .enableVideoCapture(false)
                    .enableImageCapture(false)
                    .setIgnoreHiddenFile(false)
                    .setMaxSelection(5)
                    .build());
            startActivityForResult(intent, FILE_REQUEST_CODE);
        }
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Profile photo");
        String[] pictureDialogItems = {"Gallery", "Capture photo"};
        pictureDialog.setItems(pictureDialogItems, (dialog, which) -> {
            switch (which) {
                case 0:
                    Utils.choosePhotoFromGallery(this, REQUEST_GALLERY_CODE);
                    break;
                case 1:
                    photoURI = Utils.takePhotoFromCamera(this, REQUEST_CAMERA_CODE);
                    break;
            }
        });
        pictureDialog.setCancelable(true);
        pictureDialog.show();
    }


    private void setImage(Uri uri) {
        String filePath = Utils.getUriRealPath(this, uri);
        File file = new File(filePath);
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
//        editivProfile.setImageBitmap(bitmap);
        try {
            imgDemo.setVisibility(View.GONE);
            textViewWaterMark.setTextColor(Color.WHITE);
            img_select.setImageBitmap(Utils.getScaledBitmap(this, uri));
            watermarkColor = "";
            relativeImageView.setVisibility(View.GONE);
            img_select.setVisibility(View.VISIBLE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.d("File", "Filename " + file.getName());
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
        //  RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        multiBodyPart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        //RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
    }


    private void showCurrencyDialog() {
        dialogCurrency = new Dialog(this);
        dialogCurrency.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCurrency.setCancelable(true);
        dialogCurrency.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = dialogCurrency.findViewById(R.id.editText_price_search);
        RecyclerView rvCurrency = dialogCurrency.findViewById(R.id.rvCurrency);
        currencyDataList.clear();
        currencyDataList.addAll(currencyDataList2);
        selectCurrencyAdapter = new SelectCurrencyAdapter(this, currencyDataList, new ItemClickListener() {
            @Override
            public void itemClick(int pos) {
                tv_select_currency.setText(currencyDataList.get(pos).getCurrency());
                currencyData = currencyDataList.get(pos);
                dialogCurrency.dismiss();
            }
        });
        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(selectCurrencyAdapter);

        editText_price_search.addTextChangedListener(this);
        dialogCurrency.setCanceledOnTouchOutside(true);
        dialogCurrency.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCurrency.show();
    }


    private void getSubCategoryFromServer(Integer id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getSubCategory(this, linearParent, id);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }


    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1","size:"+categoryDataList1.size()+",  " +categoryDataList1.toString());
        if (categoryDataList1 != null && !(categoryDataList1.isEmpty())) {
            for (CategoryData categoryData : categoryDataList1) {
                if (categoryData.getTitle() != null) {
                    listCategorySpinner.add(categoryData.getTitle());
                }
            }
            listCategorySpinner.add(0, "Select Category");
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, listCategorySpinner);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            spinnerCategory.setAdapter(arrayAdapter);
            categoryDataListNew.clear();
            categoryDataListNew.addAll(categoryDataList1);
            if (updateItem != null && !(title.isEmpty())) {
                setCategoryInSpinner(title);
            }
//            if (isCategory) {
//                TalentCurrencyUserTalentsData talentCurrencyUserTalentsData = getCategoryData(categoryDataList1);
//                categoryData = talentCurrencyUserTalentsData;
//                categoryDataList.clear();
//                categoryDataList.add(talentCurrencyUserTalentsData);
//                selectCategoryAdapter.notifyDataSetChanged();
//            } else {
//                categoryDataList.clear();
//                for (TalentCurrencyUserTalents talentCurrencyUserTalents : categoryDataList1) {
//                    if (talentCurrencyUserTalents.getTalent() != null) {
//                        categoryDataList.add(talentCurrencyUserTalents.getTalent());
//                    }
//                }
//                selectCategoryAdapter.notifyDataSetChanged();
//            }
        }
        List<CurrencyData> currencyDataList1 = categoryCurrency.getData().getPrice();
        if (currencyDataList1 != null && !(currencyDataList1.isEmpty())) {
            currencyDataList.clear();
            currencyDataList2.clear();
            currencyDataList2.addAll(currencyDataList1);
            currencyDataList.addAll(currencyDataList1);
        }
    }

    private void setUpdateDataForItem() {
        if (updateItem != null) {
            if (!(uploadType.isEmpty())) {
                switch (uploadType.toLowerCase()) {
                    case "product":
                        Log.d("productUpdate",""+ (HomeUserDetailsProduct) updateItem);
                        textViewWaterMark.setTextColor(Color.WHITE);
                        imgDemo.setVisibility(View.GONE);
                        HomeUserDetailsProduct homeUserDetailsProduct = (HomeUserDetailsProduct) updateItem;
                        if (homeUserDetailsProduct != null) {
                            if (!(ApplicationClass.uploadImageListDataArrayList.isEmpty())) {
                                uploadImageListData.addAll(ApplicationClass.uploadImageListDataArrayList);
                                uploadImageListAdapter.notifyDataSetChanged();
                            }
                            currencyCode = homeUserDetailsProduct.getPricecode();
                            itemId = homeUserDetailsProduct.getId();
                            if (homeUserDetailsProduct.getCategory() != null && homeUserDetailsProduct.getCategory().getTitle() != null) {
                                title = homeUserDetailsProduct.getCategory().getTitle();
                            }
                            if (homeUserDetailsProduct.getCategoryId() != null) {
                                categoryId = homeUserDetailsProduct.getCategoryId();
                            }
//                            if (homeUserDetailsProduct.getSubcategoryId() != null) {
//                                subCategoryId = homeUserDetailsProduct.getSubcategoryId();
//                            }
                            if (homeUserDetailsProduct.getUploadtype() != null) {
                                if (homeUserDetailsProduct.getUploadtype() == 1) {
                                    relativeWaterMark.setVisibility(View.GONE);
                                    String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailsProduct.getPath()).append("/").append(homeUserDetailsProduct.getCreateimage())).toString();
                                    Glide.with(this).load(imageURL).error(R.drawable.dummy_place)
                                            .error(R.drawable.dummy_place)
                                            .into(img_select);
                                } else {
                                    if (homeUserDetailsProduct.getWatermark() != null) {
                                        textViewWaterMark.setText(homeUserDetailsProduct.getWatermark());
                                    }
                                    img_select.setVisibility(View.GONE);
                                    relativeWaterMark.setVisibility(View.GONE);
                                    textViewWaterMark.setVisibility(View.VISIBLE);
                                    relativeImageView.setVisibility(View.VISIBLE);
                                    Drawable drawable = relativeImageView.getBackground();
                                    drawable.setTint(Color.parseColor(homeUserDetailsProduct.getFontcolor()));
                                    relativeImageView.setBackground(drawable);
                                    multiBodyPart = null;
                                    watermarkColor = homeUserDetailsProduct.getFontcolor();
                                }
                            }

                            if (homeUserDetailsProduct.getLocation() != null) {
                                et_Location.setText(homeUserDetailsProduct.getLocation());
                            }
                            if (homeUserDetailsProduct.getPricecode() != null) {
                                tv_select_currency.setText(homeUserDetailsProduct.getPricecode());
                            }
                            if (homeUserDetailsProduct.getPrice() != null) {
                                editText_price_range.setText(String.valueOf(homeUserDetailsProduct.getPrice()));
                            }
                            if (homeUserDetailsProduct.getDescription() != null) {
                                et_description.setText(homeUserDetailsProduct.getDescription());
                            }
                        }
                        break;
                    case "service":
                        textViewWaterMark.setTextColor(Color.WHITE);
                        imgDemo.setVisibility(View.GONE);
                        HomeUserDetailsService homeUserDetailsService = (HomeUserDetailsService) updateItem;
                        if (homeUserDetailsService != null) { if (!(ApplicationClass.uploadImageListDataArrayList.isEmpty())) {
                            uploadImageListData.addAll(ApplicationClass.uploadImageListDataArrayList);
                            uploadImageListAdapter.notifyDataSetChanged();
                        }

                            currencyCode = homeUserDetailsService.getPricecode();
                            itemId = homeUserDetailsService.getId();
                            categoryId = homeUserDetailsService.getCategoryId();
                            if (homeUserDetailsService.getCategory() != null && homeUserDetailsService.getCategory().getTitle() != null) {
                                title = homeUserDetailsService.getCategory().getTitle();
                            }

//                            if (homeUserDetailsService.getSubCategory() != null) {
//                                subCategoryId = homeUserDetailsService.getSubcategoryId();
//                            }
                            if (homeUserDetailsService.getUploadtype() != null) {
                                if (homeUserDetailsService.getUploadtype() == 1) {
                                    relativeWaterMark.setVisibility(View.GONE);
                                    String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailsService.getPath()).append("/").append(homeUserDetailsService.getCreateimage())).toString();
                                    Glide.with(this).load(imageURL).error(R.drawable.dummy_place)
                                            .error(R.drawable.dummy_place)
                                            .into(img_select);
                                } else {
                                    if (homeUserDetailsService.getWatermark() != null) {
                                        textViewWaterMark.setText(homeUserDetailsService.getWatermark());
                                    }
                                    img_select.setVisibility(View.GONE);
                                    relativeWaterMark.setVisibility(View.GONE);
                                    textViewWaterMark.setVisibility(View.VISIBLE);
                                    relativeImageView.setVisibility(View.VISIBLE);
                                    Drawable drawable = relativeImageView.getBackground();
                                    drawable.setTint(Color.parseColor(homeUserDetailsService.getFontcolor()));
                                    relativeImageView.setBackground(drawable);
                                    multiBodyPart = null;
                                    watermarkColor = homeUserDetailsService.getFontcolor();
                                }
                            }
                            if (homeUserDetailsService.getPricecode() != null) {
                                tv_select_currency.setText(homeUserDetailsService.getPricecode());
                            }
                            if (homeUserDetailsService.getLocation() != null) {
                                et_Location.setText(homeUserDetailsService.getLocation());
                            }
                            if (homeUserDetailsService.getPrice() != null) {
                                editText_price_range.setText(String.valueOf(homeUserDetailsService.getPrice()));
                            }
                            if (homeUserDetailsService.getDescription() != null) {
                                et_description.setText(homeUserDetailsService.getDescription());
                            }

                            if (homeUserDetailsService.getDays() != null) {
                                switch (homeUserDetailsService.getDays().trim().toLowerCase()) {
                                    case "per day":
                                        spinnerAgeRange.setSelection(1);
                                        break;
                                    case "per week":
                                        spinnerAgeRange.setSelection(2);
                                        break;
                                    case "per month":
                                        spinnerAgeRange.setSelection(3);
                                        break;
                                }
                            }

                        }
                        break;
                    case "brands":
                        textViewWaterMark.setTextColor(Color.WHITE);
                        AddBrandsData addBrandsData = (AddBrandsData) updateItem;
                        if (addBrandsData != null) {
                            categoryId = Integer.parseInt(addBrandsData.getCatId());
                            subCategoryId = Integer.parseInt(addBrandsData.getSubcatId());
                            String imageURL = (new StringBuilder().append(Constant.ImageURL).append(addBrandsData.getPath()).append("/").append(addBrandsData.getImage())).toString();
                            Glide.with(this).load(imageURL).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(img_select);
//                            if (addBrandsData.getPricecode() != null) {
//                                tv_select_currency.setText(addBrandsData.getPricecode());
//                            }
                            if (addBrandsData.getPrice() != null) {
                                editText_price_range.setText(String.valueOf(addBrandsData.getPrice()));
                            }
                            if (addBrandsData.getDescription() != null) {
                                et_description.setText(addBrandsData.getDescription());
                            }

                        }
                        break;
                }
            }
        }
    }

    private TalentCurrencyUserTalentsData getCategoryData(List<TalentCurrencyUserTalents> categoryDataList1) {
        for (TalentCurrencyUserTalents talentCurrencyUserTalents : categoryDataList1) {
            if (talentCurrencyUserTalents.getTalent() != null) {
                if (talentCurrencyUserTalents.getTalentId() == categoryId) {
                    return talentCurrencyUserTalents.getTalent();
                }
            }
        }
        return null;
    }

    private void submit() {
        if (updateItem == null) {
//            if (multiBodyPart == null && watermarkColor.isEmpty()) {
//                Toast.makeText(this, "Please select image", Toast.LENGTH_SHORT).show();
//                return;
//            }
        }
        if (categoryId == 0) {
            Toast.makeText(this, "Please select category", Toast.LENGTH_SHORT).show();
            return;
        }

        if (et_Location.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter location", Toast.LENGTH_SHORT).show();
            return;
        }
        if (uploadType.equals("service")) {
            if (serviceDaysSelected.isEmpty()) {
                Toast.makeText(this, "Please select duration", Toast.LENGTH_SHORT).show();
                return;
            }
        }


        if (TextUtils.isEmpty(editText_price_range.getText().toString().trim())) {
            Toast.makeText(this, "Please enter price", Toast.LENGTH_SHORT).show();
            return;
        }
        if (updateItem == null) {
            if (currencyData == null) {
                Toast.makeText(this, "Please select currency", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (TextUtils.isEmpty(et_description.getText().toString().trim())) {
            Toast.makeText(this, "Please enter description", Toast.LENGTH_SHORT).show();
            return;
        }

        uploadTalents();
    }

    private void uploadTalents() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            switch (uploadType) {
                case "product":
                    MultipartBody.Part[] fileToUpload= new MultipartBody.Part[mediaFiles.size()];
                    for (int i = 0; i < mediaFiles.size(); i++) {
//                        File file = new File(uriList1.get(i).getPath());
//                        String filePath =mediaFiles.get(i).getPath();
                        MediaFile mediaFile =mediaFiles.get(i);
                        String filePath = Utils.getUriRealPath(this,mediaFile.getUri());
                        File file = new File(filePath);
                        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
                        fileToUpload[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), requestBody);
//                            partList.add(MultipartBody.Part.createFormData("image" +"["+ i+"]", file.getName(), requestBody));
                        multiBodyPart = fileToUpload[0];
                    }
                    if (updateItem == null) {

                        if (subCategoryData == null) {
                            profileModel.uploadProduct(this, linearParent, textViewWaterMark.getText().toString().trim(), "", fileToUpload,
                                    String.valueOf(seekBar_textSize.getProgress()), watermarkColor, multiBodyPart, et_description.getText().toString().trim(),
                                    editText_price_range.getText().toString().trim(), currencyData.getCode(), String.valueOf(categoryId),
                                    "", et_Location.getText().toString().trim());
                        } else {
                            profileModel.uploadProduct(this, linearParent, textViewWaterMark.getText().toString().trim(), "", fileToUpload,
                                    String.valueOf(seekBar_textSize.getProgress()), watermarkColor, multiBodyPart, et_description.getText().toString().trim(),
                                    editText_price_range.getText().toString().trim(), currencyData.getCode(), String.valueOf(categoryId),
                                    String.valueOf(subCategoryData.getId()), et_Location.getText().toString().trim());
                        }
                    } else {
                        if (subCategoryData == null) {
                            profileModel.updateProduct(this, linearParent, textViewWaterMark.getText().toString().trim(), "", fileToUpload,
                                    String.valueOf(seekBar_textSize.getProgress()), watermarkColor, multiBodyPart, et_description.getText().toString().trim(),
                                    editText_price_range.getText().toString().trim(), currencyCode, String.valueOf(categoryId),
                                    "", String.valueOf(itemId), et_Location.getText().toString().trim());
                        } else {
                            profileModel.updateProduct(this, linearParent, textViewWaterMark.getText().toString().trim(), "", fileToUpload,
                                    String.valueOf(seekBar_textSize.getProgress()), watermarkColor, multiBodyPart, et_description.getText().toString().trim(),
                                    editText_price_range.getText().toString().trim(), currencyCode, String.valueOf(categoryId),
                                    String.valueOf(subCategoryData.getId()), String.valueOf(itemId), et_Location.getText().toString().trim());
                        }
                    }
                    break;

                case "service":
                    MultipartBody.Part[] fileToUpload1 = new MultipartBody.Part[mediaFiles.size()];
                    for (int i = 0; i < mediaFiles.size(); i++) {
//                        File file = new File(uriList1.get(i).getPath());
//                        String filePath =mediaFiles.get(i).getPath();
                        MediaFile mediaFile =mediaFiles.get(i);
                        String filePath = Utils.getUriRealPath(this,mediaFile.getUri());
                        File file = new File(filePath);
                        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg"), file);
                        fileToUpload1[i] = MultipartBody.Part.createFormData("image" + "[" + i + "]", file.getName(), requestBody);
//                            partList.add(MultipartBody.Part.createFormData("image" +"["+ i+"]", file.getName(), requestBody));
                        multiBodyPart = fileToUpload1[0];
                    }
                    if (updateItem == null) {

                        if (subCategoryData == null) {
                            profileModel.uploadService(this, linearParent, textViewWaterMark.getText().toString().trim(), "", fileToUpload1,
                                    String.valueOf(seekBar_textSize.getProgress()), watermarkColor, multiBodyPart, et_description.getText().toString().trim(),
                                    editText_price_range.getText().toString().trim(), currencyData.getCode(), String.valueOf(categoryId),
                                    "", serviceDaysSelected, et_Location.getText().toString().trim());
                        } else {
                            profileModel.uploadService(this, linearParent, textViewWaterMark.getText().toString().trim(), "", fileToUpload1,
                                    String.valueOf(seekBar_textSize.getProgress()), watermarkColor, multiBodyPart, et_description.getText().toString().trim(),
                                    editText_price_range.getText().toString().trim(), currencyData.getCode(), String.valueOf(categoryId),
                                    String.valueOf(subCategoryData.getId()), serviceDaysSelected, et_Location.getText().toString().trim());
                        }
                    } else {
                        if (subCategoryData == null) {
                            profileModel.updateService(this, linearParent, textViewWaterMark.getText().toString().trim(), "", fileToUpload1,
                                    String.valueOf(seekBar_textSize.getProgress()), watermarkColor, multiBodyPart, et_description.getText().toString().trim(),
                                    editText_price_range.getText().toString().trim(), currencyCode, String.valueOf(categoryId),
                                    "", String.valueOf(itemId), serviceDaysSelected, et_Location.getText().toString().trim());
                        } else {
                            profileModel.updateService(this, linearParent, textViewWaterMark.getText().toString().trim(), "", fileToUpload1,
                                    String.valueOf(seekBar_textSize.getProgress()), watermarkColor, multiBodyPart, et_description.getText().toString().trim(),
                                    editText_price_range.getText().toString().trim(), currencyCode, String.valueOf(categoryId),
                                    String.valueOf(subCategoryData.getId()), String.valueOf(itemId), serviceDaysSelected, et_Location.getText().toString().trim());
                        }
                    }
                    break;
            }
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void openSetWaterMarkDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_watermark);

        EditText editTextWatermark = dialog.findViewById(R.id.editTextWatermark);
        TextView textView_Cancel = dialog.findViewById(R.id.textView_Cancel);
        Button button_addWatermark = dialog.findViewById(R.id.button_addWatermark);

        if (!(textViewWaterMark.getText().toString().isEmpty())) {
            editTextWatermark.setText(textViewWaterMark.getText().toString().trim());
        }
        button_addWatermark.setOnClickListener(view -> {
            if (!(editTextWatermark.getText().toString().trim().isEmpty())) {
                relativeWaterMark.setVisibility(View.GONE);
                textViewWaterMark.setVisibility(View.VISIBLE);
                textViewWaterMark.setText(editTextWatermark.getText().toString().trim());
            }
            dialog.dismiss();
        });
        textView_Cancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void openPremiumWaterMarkDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_premium);

        Button button_BuyPremium = dialog.findViewById(R.id.button_BuyPremium);
        button_BuyPremium.setOnClickListener(view -> dialog.dismiss());

        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void setCategoryInSpinner(String title) {
        for (int i = 0; i < listCategorySpinner.size(); i++) {
            if (listCategorySpinner.get(i).equals(title)) {
                spinnerCategory.setSelection(i);
                break;
            }
        }
    }

    private int getCategoryIdData(String data) {
        for (CategoryData categoryData : categoryDataListNew) {
            if (data.equals(categoryData.getTitle())) {
                return categoryData.getId();
            }
        }
        return 0;
    }


    public void getCategory() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            if (uploadType.equals("product")) {
                profileModel.getProductCategoryCurrency(this, linearParent);
            } else {
                profileModel.getServiceCategoryCurrency(this, linearParent);
            }
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private List<String> getArrayList() {
        listColor.add("#3B6FFF");
        listColor.add("#8f348b");
        listColor.add("#af4a39");
        listColor.add("#af4a39");
        listColor.add("#b95959");
        listColor.add("#9c9c9c");
        listColor.add("#767676");
        listColor.add("#505050");
        listColor.add("#2a2a2a");
        return listColor;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        filter(s.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        super.onBackPressed();
    }

    private void filter(String charText) {
        String newCharText = charText.toLowerCase(Locale.getDefault()).trim();
        currencyDataList.clear();
        if (charText.isEmpty()) {
            currencyDataList.addAll(currencyDataList2);
        } else {
            for (CurrencyData list : currencyDataList2) {
                if (list.getCountry().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                    currencyDataList.add(list);
                }
            }
        }
        selectCurrencyAdapter.notifyDataSetChanged();
    }

//    @Override
//    public void loadImage(Uri imageUri, ImageView ivImage) {
//        Glide.with(CreateProductServiceActivity.this).load(imageUri).into(ivImage);
//    }

//    @Override
//    public void onMultiImageSelected(List<Uri> uriList, String tag) {
//       /* Bitmap bm = BitmapFactory.decodeFile(uriList.get(0).getPath());
//        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
//        bm.compress(Bitmap.CompressFormat.JPEG, 100, bOut);*/
//       /* base64 = Base64.encodeToString(bOut.toByteArray(), Base64.DEFAULT);
//        civProfileimage.setImageBitmap(bm);
//        selectedImagePath= uriList.get(0).getPath();*/
////        uriList1.clear();
//        if (uriList1.size() > 5) {
//            Toast.makeText(this, "Maximum 6 images you can select", Toast.LENGTH_SHORT).show();
//        } else uriList1.addAll(uriList);
//
//        try {
//            imgDemo.setVisibility(View.GONE);
//            textViewWaterMark.setTextColor(Color.WHITE);
//            img_select.setImageBitmap(Utils.getScaledBitmap(this, uriList1.get(0)));
//            watermarkColor = "";
//            relativeImageView.setVisibility(View.GONE);
//            img_select.setVisibility(View.VISIBLE);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//
////        PhotosVideosAdapter photosVideosAdapter = new PhotosVideosAdapter();
////        recycler_photos_videos.setAdapter(photosVideosAdapter);
////        photosVideosAdapter.notifyDataSetChanged();
//    }

    public interface UpdateSubcategory {
        void updateId();
    }
}