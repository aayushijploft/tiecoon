package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.twc.tiecoon.Activities.PaymentModule.SuccessPaymentActivity;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Adapters.SelectCurrencyAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.AddressModel;
import com.twc.tiecoon.network.response.AssignmentModel;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.CheckRelease;
import com.twc.tiecoon.network.response.CurrencyData;
import com.twc.tiecoon.network.response.ProjectAccept;
import com.twc.tiecoon.network.response.ReleaseAmount;
import com.twc.tiecoon.network.response.SaveUnsaveAds;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReleaseAmountActivity extends AppCompatActivity {

   String project_id = "",user_id="",project_userid="",Amount="",currency="";
   TextView tvSelectProject;
   EditText editText_price_range;

    TextView tv_select_currency;
    String currencyCode;
    private SelectCurrencyAdapter selectCurrencyAdapter;
    private Dialog dialogCurrency;
    private CurrencyData currencyData = null;
    private final List<CurrencyData> currencyDataList = new ArrayList<>();
    private final List<CurrencyData> currencyDataList2 = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_release_amount);
        user_id = getIntent().getStringExtra("user_id");
        project_id = getIntent().getStringExtra("project_id");
        project_userid = getIntent().getStringExtra("project_userid");
        Amount = getIntent().getStringExtra("Amount");
        currency = getIntent().getStringExtra("Currency");
        Log.e("Amount",Amount);
        tvSelectProject = findViewById(R.id.tvSelectProject);
        tv_select_currency = findViewById(R.id.tv_select_currency);
        editText_price_range = findViewById(R.id.editText_price_range);


        tv_select_currency.setText(currency);

        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());
        tv_select_currency.setOnClickListener(v ->  showCurrencyDialog());
        getCurrency();
        findViewById(R.id.btnRelease).setOnClickListener(v -> {
            String amount = editText_price_range.getText().toString().trim();
            if (TextUtils.isEmpty(amount)) {
                Toast.makeText(this, "Please enter Amount", Toast.LENGTH_SHORT).show();
                return;
            }

            if(Integer.parseInt(amount) > Integer.parseInt(Amount))
            {
                Validationpopup();
            }
            else {
                checkRelease(amount);
            }
        });
    }

    private void Validationpopup() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Release amount cannot be more than deposited amount. Your releasing amount is :"+Amount);
        btnRemove.setVisibility(View.GONE);
        btnCancle.setText("OK");
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showCurrencyDialog() {
        dialogCurrency = new Dialog(this);
        dialogCurrency.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCurrency.setCancelable(true);
        dialogCurrency.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogCurrency.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Country Currency");
        RecyclerView rvCurrency = dialogCurrency.findViewById(R.id.rvCurrency);
        currencyDataList.clear();
        currencyDataList.addAll(currencyDataList2);
        selectCurrencyAdapter = new SelectCurrencyAdapter(this, currencyDataList, pos -> {
            tv_select_currency.setText(currencyDataList.get(pos).getCurrency());
            tv_select_currency.setTextColor(getResources().getColor(R.color.white));
            currencyData = currencyDataList.get(pos);
            currencyCode = currencyData.getCode();
            dialogCurrency.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(selectCurrencyAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                currencyDataList.clear();
                if (s.toString().isEmpty()) {
                    currencyDataList.addAll(currencyDataList2);
                } else {
                    for (CurrencyData list : currencyDataList2) {
                        if (list.getCountry().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            currencyDataList.add(list);
                        }
                    }
                }
                selectCurrencyAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogCurrency.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogCurrency.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCurrency.show();
    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1", "size:" + categoryDataList1.size() + ",  " + categoryDataList1.toString());
        List<CurrencyData> currencyDataList1 = categoryCurrency.getData().getPrice();
        if (currencyDataList1 != null && !(currencyDataList1.isEmpty())) {
            currencyDataList.clear();
            currencyDataList2.clear();
            currencyDataList2.addAll(currencyDataList1);
            currencyDataList.addAll(currencyDataList1);
        }
    }

    public void getCurrency() {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CategoryCurrency> call = apiInterface.getCategoryCurrency("Bearer "+accessToken);

        call.enqueue(new Callback<CategoryCurrency>() {
            @Override
            public void onResponse(Call<CategoryCurrency> call, Response<CategoryCurrency> response) {
                CategoryCurrency user = response.body();
                Log.e("user",user.toString());
//                dialog.dismiss();
                setCategoryCurrency(user);
            }

            @Override
            public void onFailure(Call<CategoryCurrency> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
    public void checkRelease(String amount) {
        final Dialog dialog = new Dialog(ReleaseAmountActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<CheckRelease> call = apiInterface.checkRelease("Bearer " + accessToken,user_id,project_id,amount,currency);

        call.enqueue(new Callback<CheckRelease>() {
            @Override
            public void onResponse(Call<CheckRelease> call, Response<CheckRelease> response) {
                dialog.dismiss();
                CheckRelease user = response.body();
                Log.e("user", user.toString());
                if(user.getData().getAmount().equals("1")){
                    releaseAmount(amount);
                }
                else if(user.getData().getAmount().equals("0")){
                    Releasepopup(user.getMessage());
                }

            }

            @Override
            public void onFailure(Call<CheckRelease> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void releaseAmount(String Amount) {
        final Dialog dialog = new Dialog(ReleaseAmountActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ReleaseAmount> call = apiInterface.ReleaseAmount("Bearer "+accessToken,Amount,user_id,project_userid,currency,project_id);

        call.enqueue(new Callback<ReleaseAmount>() {
            @Override
            public void onResponse(Call<ReleaseAmount> call, Response<ReleaseAmount> response) {
                dialog.dismiss();
                ReleaseAmount user = response.body();
                finish();
                startActivity(new Intent(ReleaseAmountActivity.this,TransactionActivity.class));
            }

            @Override
            public void onFailure(Call<ReleaseAmount> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    private void Releasepopup(String message) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText(message);
        btnRemove.setVisibility(View.GONE);
        btnCancle.setText("OK");
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

}