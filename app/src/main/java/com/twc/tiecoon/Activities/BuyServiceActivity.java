package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.Feedback;
import com.twc.tiecoon.network.response.RefundList;
import com.twc.tiecoon.network.response.RefundRequest;
import com.twc.tiecoon.network.response.ServiceAmountModel;
import com.twc.tiecoon.network.response.ServiceEndModel;
import com.twc.tiecoon.network.response.SuccessModel;
import com.twc.tiecoon.utils.ItemClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyServiceActivity extends AppCompatActivity {

    String serviceid = "";
    String ServiceIdGen = "";
    String userid = "";
    TextView tvtitle,tvAmount;
    Button tvEndService;

    RelativeLayout btnRelease,btnDepositmore,btnRefund,btnDeposit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_service);

        serviceid = getIntent().getStringExtra("serviceid");
        ServiceIdGen = getIntent().getStringExtra("ServiceIdGen");
        userid = getIntent().getStringExtra("userid");

        tvtitle = findViewById(R.id.tvtitle);
        tvAmount = findViewById(R.id.tvAmount);
        btnRelease = findViewById(R.id.btnRelease);
        btnDepositmore = findViewById(R.id.btnDepositmore);
        tvEndService = findViewById(R.id.tvEndService);
        btnRefund = findViewById(R.id.btnRefund);
        btnDeposit = findViewById(R.id.btnDeposit);

        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());

        tvtitle.setText("Service ("+ServiceIdGen+")");

        btnDeposit.setOnClickListener(v -> {
            showDepositDialog(serviceid);


        });
        btnDepositmore.setOnClickListener(v -> {
            showDepositMoreDialog();
        });
        btnRelease.setOnClickListener(v -> {
            showReleaseDialog();
        });
        btnRefund.setOnClickListener(v -> {
            showRefundConfirmDialog();
        });
        tvEndService.setOnClickListener(v -> {
            showEndServiceDialog();
        });

        getServiceAmount(serviceid);
    }

    public void getServiceAmount(String serviceid) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ServiceAmountModel> call = apiInterface.serviceAmount("Bearer "+accessToken,serviceid);
        call.enqueue(new Callback<ServiceAmountModel>() {
            @Override
            public void onResponse(@NotNull Call<ServiceAmountModel> call, @NotNull Response<ServiceAmountModel> response) {
                ServiceAmountModel user = response.body();
                Log.e("user",user.toString());
                tvAmount.setText(user.data.price);
                if(user.data.relese_amount.equals("0")){
                    btnRelease.setBackground(getResources().getDrawable(R.drawable.greybtn));
                    btnRelease.setEnabled(false);
                    btnRefund.setBackground(getResources().getDrawable(R.drawable.greybtn));
                    btnRefund.setEnabled(false);
                }
                else {
                    btnRelease.setBackground(getResources().getDrawable(R.drawable.rounded_btn));
                    btnRelease.setEnabled(true);
                    btnRefund.setBackground(getResources().getDrawable(R.drawable.red_round));
                    btnRefund.setEnabled(true);
                }
                if(user.data.service_end.equals("yes")){
                    tvEndService.setVisibility(View.VISIBLE);
                }
                else tvEndService.setVisibility(View.GONE);

                if(user.data.end_service.equals("0")){
                    tvEndService.setBackground(getResources().getDrawable(R.drawable.greybtn));
                    tvEndService.setEnabled(false);
                    btnDepositmore.setBackground(getResources().getDrawable(R.drawable.greybtn));
                    btnDepositmore.setEnabled(false);
                    btnDeposit.setBackground(getResources().getDrawable(R.drawable.greybtn));
                    btnDeposit.setEnabled(false);
                }
                else {
                    tvEndService.setBackground(getResources().getDrawable(R.drawable.rounded_btn));
                    tvEndService.setEnabled(true);
                }
            }
            @Override
            public void onFailure(@NotNull Call<ServiceAmountModel> call, @NotNull Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void serviceAmountRelease() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SuccessModel> call = apiInterface.serviceAmountRelease("Bearer "+accessToken,serviceid);

        call.enqueue(new Callback<SuccessModel>() {
            @Override
            public void onResponse(@NotNull Call<SuccessModel> call, @NotNull Response<SuccessModel> response) {
                dialog.dismiss();
                SuccessModel user = response.body();
                Log.e("user",user.toString());
                Toast.makeText(BuyServiceActivity.this, "Service amount has been released successfully", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(BuyServiceActivity.this,TransactionActivity.class));
            }
            @Override
            public void onFailure(@NotNull Call<SuccessModel> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    private void showDepositMoreDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);

        tvConfirm.setText("Are you sure, you want to add more Amount to this service provider ?");
        btnRemove.setText("Deposit More");

        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            startActivity(new Intent(this, ServiceDepositActivity.class).
                    putExtra("serviceid",serviceid));
        });

        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showReleaseDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);

        tvConfirm.setText("Are you sure, you want to release Amount to this service provider ?");
        btnRemove.setText("Release");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            serviceAmountRelease();
        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showDepositDialog(String serviceid) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to Amount to this service provider ?");
        btnRemove.setText("Deposit");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            startActivity(new Intent(this, ServiceDepositActivity.class).
                    putExtra("serviceid",serviceid));
        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showRefundConfirmDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to initiate refund for this service provider?");
        btnRemove.setText("REFUND");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            showRefundDialog();
        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showEndServiceDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to end this service?");
        btnRemove.setText("END");

        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            endService();
        });

        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    RecyclerView rvProject;
    private void showRefundDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.project_popup);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(v -> dialog.dismiss());
        rvProject = dialog.findViewById(R.id.rvProject);
        rvProject.setHasFixedSize(true);
        rvProject.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        getRefundReasons(dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void getRefundReasons(Dialog dialog) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<RefundList> call = apiInterface.serviceRefundList("Bearer "+accessToken);

        call.enqueue(new Callback<RefundList>() {
            @Override
            public void onResponse(Call<RefundList> call, Response<RefundList> response) {
                RefundList user = response.body();
                Log.e("user",user.toString());
                ProjectAdapter projectAdapter = new ProjectAdapter(BuyServiceActivity.this,
                        user.getData(), pos -> {
                    dialog.dismiss();
                    refundRequest(user.getData().get(pos).getId()+"");
                });
                rvProject.setAdapter(projectAdapter);
            }

            @Override
            public void onFailure(Call<RefundList> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public static class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.ViewHolder> {
        private Context context;
        private List<RefundList.Data> list;
        private ItemClickListener itemClickListener;

        public ProjectAdapter(Context context,List<RefundList.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public ProjectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.project_dropdown_list, parent, false);
            return new ProjectAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(ProjectAdapter.ViewHolder holder, final int position) {
            holder.tvReportName.setText(new StringBuilder().append(list.get(position).getTitle()));
            holder.tvReportName.setOnClickListener(v -> itemClickListener.itemClick(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            LinearLayout layoutReport;
            TextView tvReportName;
            public ViewHolder(View itemView) {
                super(itemView);
                layoutReport = itemView.findViewById(R.id.layoutReport);
                tvReportName = itemView.findViewById(R.id.tvReportName);
            }
        }
    }

    public void refundRequest(String reasonid) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<RefundRequest> call = apiInterface.serviceRefundRequest("Bearer "+accessToken,reasonid,serviceid);

        call.enqueue(new Callback<RefundRequest>() {
            @Override
            public void onResponse(Call<RefundRequest> call, Response<RefundRequest> response) {
                RefundRequest user = response.body();
                Log.e("user",user.toString());
                if(user.getSuccess()){
                    Toast.makeText(BuyServiceActivity.this, user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RefundRequest> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
    public void endService() {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ServiceEndModel> call = apiInterface.endService("Bearer "+accessToken,serviceid);

        call.enqueue(new Callback<ServiceEndModel>() {
            @Override
            public void onResponse(Call<ServiceEndModel> call, Response<ServiceEndModel> response) {
                ServiceEndModel user = response.body();
                Log.e("user",user.toString());
                startActivity(new Intent(BuyServiceActivity.this, ServiceFeedbackActivity.class)
                .putExtra("userid",userid).putExtra("serviceid",serviceid)
                        .putExtra("type","user"));
            }

            @Override
            public void onFailure(Call<ServiceEndModel> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getServiceAmount(serviceid);
    }

}