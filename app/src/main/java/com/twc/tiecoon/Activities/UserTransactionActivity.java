package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import com.twc.tiecoon.Adapters.TransactionAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.TransactionListModel;
import com.twc.tiecoon.network.response.UserTransactionModel;
import com.twc.tiecoon.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserTransactionActivity extends AppCompatActivity {

    RecyclerView rvTransaction;
    String type="",user_id="";
    RelativeLayout rLNoData;
    SwipeRefreshLayout swiperefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_transaction);

        type = getIntent().getStringExtra("type");
        user_id = getIntent().getStringExtra("user_id");
        rvTransaction=findViewById(R.id.rvTransaction);
        rLNoData=findViewById(R.id.rLNoData);
        swiperefresh = findViewById(R.id.swiperefresh);
        rvTransaction.setHasFixedSize(true);
        rvTransaction.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());
        getUserTransaction(true);
        swiperefresh.setOnRefreshListener(() -> {
            if (Utils.isDeviceOnline(this)) {
                getUserTransaction(false);
            }
        });
    }

    public void getUserTransaction(boolean isshow) {
        final Dialog dialog = new Dialog(UserTransactionActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if(isshow){
            dialog.show();
        }

        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<TransactionListModel> call = apiInterface.usertransactionlist("Bearer "+accessToken,type,user_id);
        call.enqueue(new Callback<TransactionListModel>() {
            @Override
            public void onResponse(Call<TransactionListModel> call, Response<TransactionListModel> response) {
                dialog.dismiss();
                swiperefresh.setRefreshing(false);
                TransactionListModel user = response.body();
                if(user.data.size() == 0){
                    rLNoData.setVisibility(View.VISIBLE);
                    rvTransaction.setVisibility(View.GONE);
                }
                else {
                    rLNoData.setVisibility(View.GONE);
                    rvTransaction.setVisibility(View.VISIBLE);
                    TransactionAdapter transactionAdapter = new TransactionAdapter(
                            UserTransactionActivity.this,user.data);
                    rvTransaction.setAdapter(transactionAdapter);
                }
            }

            @Override
            public void onFailure(Call<TransactionListModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

}