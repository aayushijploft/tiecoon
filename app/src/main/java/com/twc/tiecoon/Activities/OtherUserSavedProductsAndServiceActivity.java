package com.twc.tiecoon.Activities;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.twc.tiecoon.Adapters.AddProductAdapters;
import com.twc.tiecoon.Adapters.AddServiceAdapters;
import com.twc.tiecoon.Adapters.SavedProductAdapter;
import com.twc.tiecoon.Adapters.SavedServiceAdapter;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.SavedProductsAndServicesData;
import com.twc.tiecoon.network.response.SavedProductsAndServicesDataProducts;
import com.twc.tiecoon.network.response.SavedProductsAndServicesDataServices;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class OtherUserSavedProductsAndServiceActivity extends AbstractFragmentActivity {

    private HomeModel homeModel = new HomeModel();
    private LinearLayout linearParent;
    private SavedProductAdapter addProductAdapters;
    private SavedServiceAdapter addServiceAdapters;
    private ArrayList<HomeUserDetailsProduct> uploadProductList = new ArrayList<>();
    private ArrayList<HomeUserDetailsService> uploadServiceList = new ArrayList<>();
    private TextView tvProductsTitle,tvServicesTitle;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_other_user_saved_products_and_service);
        ImageView ivBack = findViewById(R.id.ivBack);
        linearParent = findViewById(R.id.linearParent);
        ivBack.setOnClickListener(view -> onBackPressed());
        RecyclerView recyclerView_saved_product = findViewById(R.id.recyclerView_saved_product);
        RecyclerView recyclerView_saved_service = findViewById(R.id.recyclerView_saved_service);
         tvProductsTitle = findViewById(R.id.tvProductsTitle);
         tvServicesTitle = findViewById(R.id.tvServicesTitle);
        addProductAdapters = new SavedProductAdapter(this, uploadProductList, (pos, subPos, type) -> {
            Intent intent = new Intent(OtherUserSavedProductsAndServiceActivity.this, ProductDetailActivity.class);
            intent.putExtra("product_id", uploadProductList.get(pos).getId()+"");
            intent.putExtra("qbID", uploadProductList.get(pos).getQuickbloxuser_id());
            startActivityForResult(intent,101);
        });

        recyclerView_saved_product.setAdapter(addProductAdapters);
        addServiceAdapters = new SavedServiceAdapter(this, uploadServiceList, (pos, subPos, type) -> {
            Intent intent = new Intent(OtherUserSavedProductsAndServiceActivity.this, ServiceDetailsActivity.class);
            intent.putExtra("service_id", uploadServiceList.get(pos).getId()+"");
            intent.putExtra("qbID", uploadServiceList.get(pos).getQuickbloxuser_id());
            startActivityForResult(intent,102);
        });
        recyclerView_saved_service.setAdapter(addServiceAdapters);
        getSavedProductsAndServices();
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    @Override
    public void update(Observable observable, Object o) {
        SavedProductsAndServicesData savedProductsAndServicesData = (SavedProductsAndServicesData) o;
        List<SavedProductsAndServicesDataProducts> savedProductsAndServicesDataProductsList = savedProductsAndServicesData.getProduct();
        if (savedProductsAndServicesDataProductsList != null && !(savedProductsAndServicesDataProductsList.isEmpty())) {
            for (SavedProductsAndServicesDataProducts savedProductsAndServicesDataProducts : savedProductsAndServicesDataProductsList) {
                if (savedProductsAndServicesDataProducts != null && savedProductsAndServicesDataProducts.getProduct() != null) {
                    uploadProductList.add(savedProductsAndServicesDataProducts.getProduct());
                }
            }

            addProductAdapters.notifyDataSetChanged();
        }

        List<SavedProductsAndServicesDataServices> savedProductsAndServicesDataServicesList = savedProductsAndServicesData.getService();
        if (savedProductsAndServicesDataServicesList != null && !(savedProductsAndServicesDataServicesList.isEmpty())) {
            for (SavedProductsAndServicesDataServices savedProductsAndServicesDataServices : savedProductsAndServicesDataServicesList) {
                if (savedProductsAndServicesDataServices != null && savedProductsAndServicesDataServices.getService() != null) {
                    uploadServiceList.add(savedProductsAndServicesDataServices.getService());
                }
            }

            addServiceAdapters.notifyDataSetChanged();
        }

        if (uploadProductList.isEmpty() && uploadServiceList.isEmpty()) {
            tvProductsTitle.setVisibility(View.GONE);
            tvServicesTitle.setVisibility(View.GONE);
            Toast.makeText(this, "No saved products & services", Toast.LENGTH_SHORT).show();
        } if (uploadProductList.isEmpty()) {
             tvProductsTitle.setVisibility(View.GONE);
        } if (uploadServiceList.isEmpty()) {
            tvServicesTitle.setVisibility(View.GONE);
        }
    }

    private void getSavedProductsAndServices() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getSavedProductsAndServicesList(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==101){
            if (resultCode == Activity.RESULT_OK) {
                finish();
                startActivity(getIntent());
            }
        }
        if(requestCode==102){
            if (resultCode == Activity.RESULT_OK) {
                finish();
                startActivity(getIntent());
            }
        }
    }

}