package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.ButtonModel;
import com.twc.tiecoon.network.response.DeleteStory;
import com.twc.tiecoon.utils.Utils;

import java.util.Observable;

public class ButtonScreen extends AbstractFragmentActivity {
    private ProfileModel profileModel = new ProfileModel();
    EditText editText,editText1;
    Button btnSummit;
    String value;
    LinearLayout linearParent;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button_screen);
        value = getIntent().getStringExtra("value");
        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());
        editText = findViewById(R.id.editText);
        editText1 = findViewById(R.id.editText1);
        btnSummit = findViewById(R.id.btnSummit);
        linearParent = findViewById(R.id.linearParent);

        btnSummit.setOnClickListener(v -> {
            Validation();
        });

//        button1  button2
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void Validation() {
        String one = editText.getText().toString().trim();
        String two = editText1.getText().toString().trim();

        if (TextUtils.isEmpty(one)) {
            Toast.makeText(this, "Please enter First Text", Toast.LENGTH_SHORT).show();
            return;
        }
         if (TextUtils.isEmpty(two)) {
            Toast.makeText(this, "Please enter Second Text", Toast.LENGTH_SHORT).show();
            return;
        }

        confirm();

    }

    private void confirm() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("Are you sure, you want to submit?");
        btnRemove.setText("SUBMIT");
        tvTitle.setVisibility(View.GONE);
//        tvTitle.setText("Collaboration Request");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            if(value.equals("button1")){
                ButtonOne();
            }
            else if(value.equals("button2")){
                ButtonTwo();
            }

        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    private void ButtonOne() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.ButtonOne(this, linearParent,editText.getText().toString(),editText1.getText().toString());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }
    private void ButtonTwo() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.ButtonTwo(this, linearParent,editText.getText().toString(),editText1.getText().toString());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof ButtonModel) {
            Toast.makeText(this, "Submitted Successfully", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }
}