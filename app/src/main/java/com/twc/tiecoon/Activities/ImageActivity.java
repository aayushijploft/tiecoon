package com.twc.tiecoon.Activities;

import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.utils.Utils;
import com.jsibbold.zoomage.ZoomageView;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Observable;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ImageActivity extends AbstractFragmentActivity {
    private final String[] PROFILE_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int PROFILE_PERMISSION_REQUEST = 601;
    private Uri photoURI;
    private static final int REQUEST_GALLERY_CODE = 605;
    private static final int REQUEST_CAMERA_CODE = 606;
    private final boolean profilePermissionDeniedWithDontAskMeAgain = false;
    private final ProfileModel profileModel=new ProfileModel();
    ImageView imgCancel,imgEdit;
    ZoomageView myZoomageView;
    String imageurl="",key="";
    private LinearLayout linearParent;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_image);
        imageurl = getIntent().getStringExtra("image");
        key = getIntent().getStringExtra("key");
        imgCancel=findViewById(R.id.imgCancel);
        linearParent=findViewById(R.id.linearParent);
        imgEdit=findViewById(R.id.imgEdit);
        myZoomageView=findViewById(R.id.myZoomageView);
        Glide.with(this).load(imageurl).error(R.drawable.dummy_place)
                .into(myZoomageView);

        if(key.equals("show")){
            imgEdit.setVisibility(View.VISIBLE);
        }

        checkPermissionForProfile1();
        imgCancel.setOnClickListener(view ->onBackPressed());
        imgEdit.setOnClickListener(view -> checkPermissionForProfile());
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    @Override
    public void update(Observable observable, Object o) {
        if(o instanceof DeleteTalents)
        {
            Toast.makeText(this, "Profile Image updated successfully!", Toast.LENGTH_SHORT).show();
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    private void checkPermissionForProfile() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                showPictureDialog();
            }
        } else {
            showPictureDialog();
        }
    }
    private void checkPermissionForProfile1() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            }
        }
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Profile photo");
        String[] pictureDialogItems = {"Gallery", "Capture photo"};
        pictureDialog.setItems(pictureDialogItems, (dialog, which) -> {
            switch (which) {
                case 0:
                    Utils.choosePhotoFromGallery(this, REQUEST_GALLERY_CODE);
                    break;
                case 1:
                    photoURI = Utils.takePhotoFromCamera(this, REQUEST_CAMERA_CODE);
                    break;
            }
        });
        pictureDialog.setCancelable(true);
        pictureDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_GALLERY_CODE:
                try {
                    Uri imageUri = data.getData();
                    Bitmap bitmap = Utils.getScaledBitmap(this, imageUri);
                    Uri uri = Utils.getImageUri(this, bitmap);
                    Utils.cropImage(this, uri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case REQUEST_CAMERA_CODE:
                Uri uri2 = photoURI;
                Utils.cropImage(this, uri2);
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    Uri resultUri = result.getUri();
                    setImage(resultUri);
                }
                break;
        }
    }

    private void setImage(Uri uri) {
        if (Utils.isDeviceOnline(this)) {
            String filePath = Utils.getUriRealPath(this, uri);
            File file = new File(filePath);
            try {
                myZoomageView.setImageBitmap(Utils.getScaledBitmap(this, uri));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Log.d("File", "Filename " + file.getName());
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
            MultipartBody.Part multiBodyPart = MultipartBody.Part.createFormData("profile", file.getName(), requestFile);
            Utils.showProDialog(this);
            profileModel.updateUserImage(this, linearParent, multiBodyPart);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }
}