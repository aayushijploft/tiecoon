package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.CustomMedia.JZMediaExo;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.AdsModel;
import com.twc.tiecoon.network.response.ProjectRequest;
import com.twc.tiecoon.network.response.SaveUnsaveAds;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;

import java.util.List;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SavedAdvertismentActivity extends AppCompatActivity {

    RecyclerView rvSavedAds;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_advertisment);

        rvSavedAds=findViewById(R.id.rvSavedAds);
        rvSavedAds.setHasFixedSize(true);
        rvSavedAds.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));

        findViewById(R.id.iv_back).setOnClickListener(v -> startActivity(new Intent(this,UserHomeActivity.class)));
        saveAds();
    }

    public void saveAds() {
        final Dialog dialog = new Dialog(SavedAdvertismentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<AdsModel> call = apiInterface.saveAdsList("Bearer "+accessToken);
        call.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(Call<AdsModel> call, Response<AdsModel> response) {
                dialog.dismiss();
                AdsModel user = response.body();
                assert user != null;
                if(user.getData().size()>0){
                    AdsAdapter adsAdapter = new AdsAdapter(SavedAdvertismentActivity.this,user.getData(),pos -> {
                        saveAds(user.getData().get(pos).getId()+"");
                    });
                    rvSavedAds.setAdapter(adsAdapter);
                }
                else Toast.makeText(SavedAdvertismentActivity.this, "No Saved Ads Found.", Toast.LENGTH_SHORT).show();

            }
            @Override
            public void onFailure(Call<AdsModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


    public static class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.ViewHolder> {
        private Context context;
        private List<AdsModel.Data> list;
        private ItemClickListener itemClickListener;
        public AdsAdapter(Context context,List<AdsModel.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public AdsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.save_ads_layout, parent, false);
            return new AdsAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(AdsAdapter.ViewHolder holder, final int position) {
            holder.ivAdvertisement.setOnClickListener(v -> itemClickListener.itemClick(position));
            holder.layoutVideo.setOnClickListener(v -> itemClickListener.itemClick(position));
            holder.layoutVisitAds.setOnClickListener(v -> itemClickListener.itemClick(position));
            if(list.get(position).getAds_type().equals("video")){
                holder.ivAdvertisement.setVisibility(View.GONE);
                holder.layoutVideo.setVisibility(View.VISIBLE);
            }
            else {
                holder.ivAdvertisement.setVisibility(View.VISIBLE);
                holder.layoutVideo.setVisibility(View.GONE);
            }
            holder.ivThumbnail.setOnClickListener(view -> {
                holder.ivThumbnail.setVisibility(View.GONE);
                holder.ivPlayButton.setVisibility(View.GONE);
                holder.jz_video.startVideo();
            });

            if(list.get(position).getLike() == 0){
                holder.AdsLike.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_like));
            }
            else if(list.get(position).getLike() == 1){
                holder.AdsLike.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_like_solid));
            }

            holder.jz_video.setUp(list.get(position).getImage(), "", JzvdStd.SCREEN_NORMAL, JZMediaExo.class);
            Glide.with(context).load(list.get(position).getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.ivAdvertisement);
            Glide.with(context).load(list.get(position).getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.ivThumbnail);
            holder.tvAdTitle.setText(list.get(position).getTitle());
            holder.tvAdDescription.setText(list.get(position).getMassage());
            holder.ivAdvertisement.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(position).getLink()));
                context.startActivity(browserIntent);
            });
            holder.layoutVideo.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(position).getLink()));
                context.startActivity(browserIntent);
            });
            holder.layoutVisitAds.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(position).getLink()));
                context.startActivity(browserIntent);
            });
            holder.AdsLike.setOnClickListener(view -> itemClickListener.itemClick(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            FrameLayout layoutVideo;
            TextView tvAdTitle,tvAdDescription;
            ImageView AdsLike,ivAdvertisement,ivThumbnail,ivPlayButton;
            Jzvd jz_video;
            LinearLayout layoutVisitAds;
            public ViewHolder(View itemView) {
                super(itemView);
                layoutVideo = itemView.findViewById(R.id.layoutVideo);
                tvAdTitle = itemView.findViewById(R.id.tvAdTitle);
                AdsLike = itemView.findViewById(R.id.AdsLike);
                ivAdvertisement = itemView.findViewById(R.id.ivAdvertisement);
                jz_video = itemView.findViewById(R.id.jz_video);
                ivThumbnail = itemView.findViewById(R.id.ivThumbnail);
                ivPlayButton = itemView.findViewById(R.id.ivPlayButton);
                tvAdDescription = itemView.findViewById(R.id.tvAdDescription);
                layoutVisitAds = itemView.findViewById(R.id.layoutVisitAds);
            }
        }
    }

    public void saveAds(String id) {
        final Dialog dialog = new Dialog(SavedAdvertismentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SaveUnsaveAds> call = apiInterface.saveUnsaveAds("Bearer "+accessToken,id);
        call.enqueue(new Callback<SaveUnsaveAds>() {
            @Override
            public void onResponse(Call<SaveUnsaveAds> call, Response<SaveUnsaveAds> response) {
                dialog.dismiss();
                SaveUnsaveAds user = response.body();
                assert user != null;
                Intent intent = getIntent();
                finish();
                startActivity(intent);

            }

            @Override
            public void onFailure(Call<SaveUnsaveAds> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,UserHomeActivity.class));
    }
}