package com.twc.tiecoon.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.Adapters.AddBrandsAdapters;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.HomeUserDetail;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class CreateBrandsActivity extends AbstractFragmentActivity implements View.OnClickListener {
    private ProfileModel profileModel = new ProfileModel();
    private LinearLayout linear_upload_brands;
    public static final int UPLOAD_BRANDS = 101;
    private ArrayList<AddBrandsData> uploadBrandsList = new ArrayList<>();
    private AddBrandsAdapters addBrandsAdapters;
    private LinearLayout linearParent;
    private RecyclerView recyclerView_upload_brands;
    private EditText editTextNote;
    private ImageView iv_back;
    private ImageView iv_menu;
    private Button btnNext;
    private TextView tvSkip;
    private int updateBrandPos;
    private boolean home = false;
    private boolean uploadsBrands = false;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_create_brands);
        home = getIntent().getBooleanExtra("home", false);
        initView();
    }

    private void initView() {
        iv_back =  findViewById(R.id.iv_back);
        iv_menu =  findViewById(R.id.iv_menu);
        linearParent = findViewById(R.id.linearParent);
        editTextNote = findViewById(R.id.editTextNote);
        linear_upload_brands = findViewById(R.id.linear_upload_brands);
        recyclerView_upload_brands = findViewById(R.id.recyclerView_upload_brands);
        btnNext = findViewById(R.id.btnNext);
        tvSkip =  findViewById(R.id.tvSkip);

        iv_back.setOnClickListener(this);
        iv_menu.setOnClickListener(this);
        linear_upload_brands.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        tvSkip.setOnClickListener(this);

        if (home) {
            tvSkip.setVisibility(View.GONE);
            btnNext.setVisibility(View.GONE);
        }

        addBrandsAdapters = new AddBrandsAdapters(this, uploadBrandsList, new ItemClickListenerExtraParam() {
            @Override
            public void itemClick(int pos, String type) {
                updateBrandPos = pos;
                if (type.equals("update")) {
                    uploadsBrands = true;
                    updateBrands(uploadBrandsList.get(pos));
                } else {
                    deleteBrands(uploadBrandsList.get(pos));
                }
            }
        });
        recyclerView_upload_brands.setAdapter(addBrandsAdapters);

        getUserDetails();
    }

    private void updateBrands(AddBrandsData addBrandsData) {
        Intent i = new Intent(this, UploadBrandsActivity.class);
        i.putExtra("updateItem", addBrandsData);
        startActivityForResult(i, UPLOAD_BRANDS);
    }

    private void deleteBrands(AddBrandsData addBrandsData) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deleteBrands(this, linearParent, addBrandsData.getId());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }


    @Override
    protected BasicModel getModel() {
        return profileModel;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.linear_upload_brands:
                uploadsBrands=false;
                Intent i = new Intent(this, UploadBrandsActivity.class);
                i.putExtra("type", "brands");
                startActivityForResult(i, UPLOAD_BRANDS);
                break;
            case R.id.iv_menu:
                //to do
                break;
            case R.id.btnNext:
                Intent intent1 = new Intent(this, UserHomeActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
                break;
            case R.id.tvSkip:
                Intent intent = new Intent(this, UserHomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case UPLOAD_BRANDS:
                if (resultCode == RESULT_OK) {
                    if (!home) {
                        tvSkip.setVisibility(View.GONE);
                        btnNext.setVisibility(View.VISIBLE);
                    }
                    AddBrandsData addBrandsData = (AddBrandsData) data.getParcelableExtra("UploadData");
                    if (uploadsBrands) {
                        uploadBrandsList.set(updateBrandPos, addBrandsData);
                        addBrandsAdapters.notifyItemChanged(updateBrandPos);
                    } else {
                        uploadBrandsList.add(addBrandsData);
                        addBrandsAdapters.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof HomeUserDetail) {
            if (!home) {
                tvSkip.setVisibility(View.VISIBLE);
            }
            HomeUserDetail homeUserDetail = (HomeUserDetail) o;
            HomeUserDetailData homeUserDetailData = homeUserDetail.getData();
            if (homeUserDetailData != null) {
                List<AddBrandsData> addBrandsDataList = homeUserDetailData.getHomeUserDetailBrand();
                if (addBrandsDataList != null && !(addBrandsDataList.isEmpty())) {
                    if (!home) {
                        tvSkip.setVisibility(View.GONE);
                        btnNext.setVisibility(View.VISIBLE);
                    }
                    uploadBrandsList.addAll(addBrandsDataList);
                    addBrandsAdapters.notifyDataSetChanged();
                }
            }
        }else if (o instanceof String) {
            String data = (String) o;
            if(data.equals("deleteBrands"))
            {
                uploadBrandsList.remove(updateBrandPos);
                addBrandsAdapters.notifyItemRemoved(updateBrandPos);
            }
        }

    }

    private void getUserDetails() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getMyProfileDetails(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }
}