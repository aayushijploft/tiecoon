package com.twc.tiecoon.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.theartofdev.edmodo.cropper.CropImage;
import com.twc.tiecoon.Adapters.AgeGroupAdapter;
import com.twc.tiecoon.Adapters.CityAdapter;
import com.twc.tiecoon.Adapters.CountryAdapter;
import com.twc.tiecoon.Adapters.GenderAdapter;
import com.twc.tiecoon.Adapters.PreferedLangAdapter;
import com.twc.tiecoon.Adapters.SelectCurrencyAdapter;
import com.twc.tiecoon.Adapters.SelectedProjectTalentAdapter;
import com.twc.tiecoon.Adapters.StateAdapter;
import com.twc.tiecoon.Adapters.TalentsAdapter;
import com.twc.tiecoon.Adapters.selectedProjectTalentAdapterEdit;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AgeGroupData;
import com.twc.tiecoon.network.response.AgeGroupDataList;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.CityData;
import com.twc.tiecoon.network.response.CityDataList;
import com.twc.tiecoon.network.response.CountryData;
import com.twc.tiecoon.network.response.CountryDataList;
import com.twc.tiecoon.network.response.CurrencyData;
import com.twc.tiecoon.network.response.PreferredLanguage;
import com.twc.tiecoon.network.response.PreferredLanguageData;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.network.response.ProfileData;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.network.response.ProjectDetails;
import com.twc.tiecoon.network.response.ProjectTemplatesModel;
import com.twc.tiecoon.network.response.ProjectUsers;
import com.twc.tiecoon.network.response.RequestMoneyModel;
import com.twc.tiecoon.network.response.StateData;
import com.twc.tiecoon.network.response.StateDataList;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Observable;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateAssignentNew extends AbstractFragmentActivity implements View.OnClickListener, TextWatcher {
    private static final int REQUEST_GALLERY_CODE = 605;
    private static final int REQUEST_CAMERA_CODE = 606;
    private final String[] PROFILE_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int PROFILE_PERMISSION_REQUEST = 601;
    private final ProfileModel profileModel = new ProfileModel();
    private final List<ProfileTalent> addTalentDataList = new ArrayList<>();
    private final List<ProfileTalent> addTalentDataList2 = new ArrayList<>();
    private final List<ProfileTalent> selectedaddTalentDataList = new ArrayList<>();
    //    For Currency
    private final List<CurrencyData> currencyDataList = new ArrayList<>();
    private final List<CurrencyData> currencyDataList2 = new ArrayList<>();
    //    For Age Group
    private final List<AgeGroupDataList> ageGroupDataList = new ArrayList<>();
    private final List<AgeGroupDataList> ageGroupDataList2 = new ArrayList<>();
    //    For Language
    private final List<PreferredLanguageData> preferredLanguageDataList = new ArrayList<>();
    private final List<PreferredLanguageData> preferredLanguageDataList2 = new ArrayList<>();
    private final List<String> genderList = new ArrayList<>();
    private final List<String> genderList2 = new ArrayList<>();
    //    For Country
    private final List<CountryDataList> countryDataLists = new ArrayList<>();
    private final List<CountryDataList> countryDataLists1 = new ArrayList<>();
    //    For State
    private final List<StateDataList> stateDataLists = new ArrayList<>();
    private final List<StateDataList> stateDataLists1 = new ArrayList<>();
    //    For City
    private final List<CityDataList> cityDataLists = new ArrayList<>();
    private final List<CityDataList> cityDataLists1 = new ArrayList<>();
    private final List<ProjectDetails.Data> talentArrayList = new ArrayList<>();
    TextView tvStartDate, tvEndDate;
    RecyclerView recyclerView_selected_preferedLang;
    SelectedProjectTalentAdapter selectedProjectTalentAdapter;
    String projectid = "";
    TextView tv_select_currency;
    String currencyCode;
    selectedProjectTalentAdapterEdit s;
    TextView tv_select_agegroup;
    String AgeGroup;
    TextView tv_select_language;
    String PreferredLang;
    TextView tv_select_Gender;
    String gender = "";
    TextView tv_select_Country;
    TextView tv_select_State;
    TextView tv_select_City;
    EditText editText_price_range, editTextYoutubeLink;
    RecyclerView editrecyclerView_selected_preferedLang;
    String age_id="", country_id="", state_id="", city_id="", language_id="", currencycode="";
    List<String> list = new ArrayList<>();
    private MultipartBody.Part multiBodyPart = null;
    private Uri photoURI;
    private boolean profilePermissionDeniedWithDontAskMeAgain = false;
    private RelativeLayout relativeParent;
    private ImageView image;
    private EditText et_name, editText;
    private TextView textView_select_Talents;
    private ProfileTalent profileTalent = null;
    private Dialog dialogTalents;
    private TalentsAdapter talentsAdapter;
    private ProfileCreateAssignment profileCreateAssignment;
    private SelectCurrencyAdapter selectCurrencyAdapter;
    private Dialog dialogCurrency;
    private CurrencyData currencyData = null;
    private AgeGroupAdapter ageGroupAdapter;
    private Dialog dialogAgeGroup;
    private AgeGroupDataList groupDataList = null;
    private PreferedLangAdapter preferedLangAdapter;
    private Dialog preferredlangDialog;
    private PreferredLanguageData preferredLanguageData = null;
    //    For Gender
    private Dialog genderDialog;
    private GenderAdapter genderAdapter;
    private CountryAdapter countryAdapter;
    private Dialog countryDialog;
    private CountryDataList countryDataList = null;
    private StateAdapter stateAdapter;
    private Dialog stateDialog;
    private StateDataList stateDataList = null;
    private CityAdapter cityAdapter;
    private Dialog cityDialog;
    private CityDataList cityDataList = null;
    RecyclerView rvTemplates;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_create_assignent);
        relativeParent = findViewById(R.id.relativeParent);
//        ImageView ivBack = findViewById(R.id.ivBack);
        image = findViewById(R.id.image);
        Button btnSave = findViewById(R.id.btnSave);
        et_name = findViewById(R.id.et_name);
        tvEndDate = findViewById(R.id.tvEndDate);
        tvStartDate = findViewById(R.id.tvStartDate);
        textView_select_Talents = findViewById(R.id.textView_select_Talents);
        editText = findViewById(R.id.editText);
        tv_select_currency = findViewById(R.id.tv_select_currency);
        tv_select_agegroup = findViewById(R.id.tv_select_agegroup);
        tv_select_language = findViewById(R.id.tv_select_language);
        tv_select_Gender = findViewById(R.id.tv_select_Gender);
        tv_select_Country = findViewById(R.id.tv_select_Country);
        tv_select_State = findViewById(R.id.tv_select_State);
        tv_select_City = findViewById(R.id.tv_select_City);
        editText_price_range = findViewById(R.id.editText_price_range);
        editTextYoutubeLink = findViewById(R.id.editTextYoutubeLink);
        rvTemplates = findViewById(R.id.rvTemplates);
        recyclerView_selected_preferedLang = findViewById(R.id.recyclerView_selected_preferedLang);
        editrecyclerView_selected_preferedLang = findViewById(R.id.editrecyclerView_selected_preferedLang);
        findViewById(R.id.ivBack).setOnClickListener(v -> onBackPressed());
        image.setOnClickListener(this);
        tvEndDate.setOnClickListener(this);
        tvStartDate.setOnClickListener(this);
        textView_select_Talents.setOnClickListener(this);
        tv_select_currency.setOnClickListener(this);
        tv_select_agegroup.setOnClickListener(this);
        tv_select_language.setOnClickListener(this);
        tv_select_Gender.setOnClickListener(this);
        tv_select_Country.setOnClickListener(this);
        tv_select_State.setOnClickListener(this);
        tv_select_City.setOnClickListener(this);
        textView_select_Talents.setTextColor(getResources().getColor(R.color.white));
        genderList2.add("male");
        genderList2.add("female");
        genderList2.add("other");
        genderList2.add("No preference");
        btnSave.setOnClickListener(this);
        if (getIntent().hasExtra("updateItem")) {
            projectid = getIntent().getStringExtra("updateItem");
            viewProjectDetails(projectid);
        }


        editText.setOnTouchListener((view, event) -> {
            if (view.getId() == R.id.et_description) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                }
            }
            return false;
        });

        selectedProjectTalentAdapter = new SelectedProjectTalentAdapter(this, selectedaddTalentDataList, pos -> {
            if (list.contains(selectedaddTalentDataList.get(pos).getId())) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).equals(selectedaddTalentDataList.get(pos).getId())) {
                        list.remove(i);
                    }
                }
            }
            selectedaddTalentDataList.remove(pos);
            selectedProjectTalentAdapter.notifyItemRemoved(pos);
            selectedProjectTalentAdapter.notifyItemRangeChanged(pos, selectedaddTalentDataList.size());
        });
        recyclerView_selected_preferedLang.setAdapter(selectedProjectTalentAdapter);
        projectTemplates();
        getCategory();

    }


    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof ProfileData) {
            setTalents((ProfileData) o);

        } else if (o instanceof ProfileCreateAssignment) {
            Toast.makeText(this, "Project Created Successfully", Toast.LENGTH_SHORT).show();
            ProfileCreateAssignment profileCreateAssignment = (ProfileCreateAssignment) o;
            Intent returnIntent = new Intent();
            returnIntent.putExtra("createAssign", profileCreateAssignment);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
        if (o instanceof CategoryCurrency) {
            setCategoryCurrency((CategoryCurrency) o);
            getAgeGroup();
        }
        if (o instanceof AgeGroupData) {
            setageGroup((AgeGroupData) o);
            getUserLanguage();
        } else if (o instanceof PreferredLanguage) {
            setLanguage((PreferredLanguage) o);
            getCountry();
        } else if (o instanceof CountryData) {
            setCountry((CountryData) o);

        } else if (o instanceof StateData) {
            setState((StateData) o);

        } else if (o instanceof CityData) {
            setCity((CityData) o);



        } else if (o instanceof ProjectDetails) {
            ProjectDetails projectDetails = (ProjectDetails) o;
            Log.e("projectdetails", projectDetails.getData().toString());

            et_name.setText(projectDetails.getData().getName());
            editText.setText(projectDetails.getData().getText());
            editTextYoutubeLink.setText(projectDetails.getData().getYoutublink());
            editText_price_range.setText(projectDetails.getData().getAmount() + "");
            tv_select_Country.setText(projectDetails.getData().getCountry());
            tv_select_State.setText(projectDetails.getData().getState());
            tv_select_City.setText(projectDetails.getData().getCity());
            tv_select_language.setText(projectDetails.getData().getLanguage());
            if(projectDetails.getData().getGender()!=null){
                  if(projectDetails.getData().getGender().equals("no_preference")){
                tv_select_Gender.setText("No preference");
            }
           else tv_select_Gender.setText(projectDetails.getData().getGender());
            }

            tv_select_agegroup.setText(projectDetails.getData().getAge());
            tv_select_currency.setText(projectDetails.getData().getCurrency());
            s = new selectedProjectTalentAdapterEdit(this, projectDetails.getData().getTalent(), pos -> {

                if (list.contains(projectDetails.getData().getTalent().get(pos).getId())) {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).equals(projectDetails.getData().getTalent().get(pos).getId())) {
                            list.remove(i);
                        }
                    }
                }

                projectDetails.getData().getTalent().remove(pos);
                s.notifyItemRemoved(pos);
                s.notifyItemRangeChanged(pos, projectDetails.getData().getTalent().size());
                talentArrayList.remove(pos);
            });
            editrecyclerView_selected_preferedLang.setAdapter(s);

            for (int i = 0; i < projectDetails.getData().getTalent().size(); i++) {
                talentArrayList.add(projectDetails.getData());
                list.add(projectDetails.getData().getTalent().get(i).getId());
            }
            if(projectDetails.getData().getLanguage_id() != null){
                language_id = projectDetails.getData().getLanguage_id();
            }
            if(projectDetails.getData().getAge_id() != null){
                age_id = projectDetails.getData().getAge_id();
            }
            if(projectDetails.getData().getCountry_id() != null){
                country_id = projectDetails.getData().getCountry_id();
            }
            if(projectDetails.getData().getState_id() != null){
                state_id = projectDetails.getData().getState_id();
            }
            if(projectDetails.getData().getCity_id() != null){
                city_id = projectDetails.getData().getCity_id();
            }
            if(projectDetails.getData().getCurrency() != null){
                currencycode = projectDetails.getData().getCurrency();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        super.onBackPressed();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                break;
            case R.id.image:
                checkPermissionForProfile();
                break;
            case R.id.textView_select_Talents:
                if (addTalentDataList2.isEmpty()) {
                    getTalents();

                } else {
                    showTalentsDialog();
                }
                break;
            case R.id.btnSave:
                CheckAssignmentValidation();
                break;
            case R.id.tvStartDate:
                openCalender(tvStartDate);
                break;
            case R.id.tvEndDate:
                openCalender(tvEndDate);
                break;
            case R.id.tv_select_currency:
                showCurrencyDialog();
                break;
            case R.id.tv_select_agegroup:
                showAgeGroupDialog();
                break;
            case R.id.tv_select_language:
                showPreferredLangDialog();
                break;
            case R.id.tv_select_Gender:
                showGenderDialog();
                break;
            case R.id.tv_select_Country:
                showCountryDialog();
                break;
            case R.id.tv_select_State:
                showStateDialog();
                break;
            case R.id.tv_select_City:
                showCityDialog();
                break;
        }
    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1", "size:" + categoryDataList1.size() + ",  " + categoryDataList1.toString());

        List<CurrencyData> currencyDataList1 = categoryCurrency.getData().getPrice();
        if (currencyDataList1 != null && !(currencyDataList1.isEmpty())) {
            currencyDataList.clear();
            currencyDataList2.clear();
            currencyDataList2.addAll(currencyDataList1);
            currencyDataList.addAll(currencyDataList1);
        }
    }

    private void setageGroup(AgeGroupData ageGroupData) {
        List<AgeGroupDataList> ageGroupDataLists = ageGroupData.getData();
        if (ageGroupDataLists != null && !(ageGroupDataLists.isEmpty())) {
            ageGroupDataList.clear();
            ageGroupDataList2.clear();
            ageGroupDataList2.addAll(ageGroupDataLists);
            ageGroupDataList.addAll(ageGroupDataLists);
        }
    }

    private void setLanguage(PreferredLanguage preferredLanguage) {
        List<PreferredLanguageData> preferredLanguageData = preferredLanguage.getData();
        if (preferredLanguageData != null && !(preferredLanguageData.isEmpty())) {
            preferredLanguageDataList.clear();
            preferredLanguageDataList2.clear();
            preferredLanguageDataList2.addAll(preferredLanguageData);
            preferredLanguageDataList.addAll(preferredLanguageData);
        }
    }

    private void setCountry(CountryData country) {
        List<CountryDataList> countryDataListList = country.getData();
        if (countryDataListList != null && !(countryDataListList.isEmpty())) {
            countryDataLists.clear();
            countryDataLists1.clear();
            countryDataLists1.addAll(countryDataListList);
            countryDataLists.addAll(countryDataListList);
        }
    }

    private void setState(StateData stateData) {
        List<StateDataList> stateDataListList = stateData.getData();
        if (stateDataListList != null && !(stateDataListList.isEmpty())) {
            stateDataLists.clear();
            stateDataLists1.clear();
            stateDataLists.addAll(stateDataListList);
            stateDataLists1.addAll(stateDataListList);
        }
    }

    private void setCity(CityData cityData) {
        List<CityDataList> cityDataListsl = cityData.getData();
        if (cityDataListsl != null && !(cityDataListsl.isEmpty())) {
            cityDataLists.clear();
            cityDataLists1.clear();
            cityDataLists.addAll(cityDataListsl);
            cityDataLists1.addAll(cityDataListsl);
        }

    }

    private void setGender(String string) {
        if (string != null && !(string.isEmpty())) {
            genderList.clear();
            genderList2.clear();
            genderList.addAll(Collections.singleton(string));
            genderList2.addAll(Collections.singleton(string));
        }
    }

    private void showCurrencyDialog() {
        dialogCurrency = new Dialog(this);
        dialogCurrency.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogCurrency.setCancelable(true);
        dialogCurrency.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogCurrency.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Country Currency");
        RecyclerView rvCurrency = dialogCurrency.findViewById(R.id.rvCurrency);
        currencyDataList.clear();
        currencyDataList.addAll(currencyDataList2);

        selectCurrencyAdapter = new SelectCurrencyAdapter(this, currencyDataList, pos -> {
            tv_select_currency.setText(currencyDataList.get(pos).getCurrency());
            tv_select_currency.setTextColor(getResources().getColor(R.color.white));
            currencyData = currencyDataList.get(pos);
            currencyCode = currencyData.getCode();
            dialogCurrency.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(selectCurrencyAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                currencyDataList.clear();
                if (s.toString().isEmpty()) {
                    currencyDataList.addAll(currencyDataList2);
                } else {
                    for (CurrencyData list : currencyDataList2) {
                        if (list.getCountry().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            currencyDataList.add(list);
                        }
                    }
                }
                selectCurrencyAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogCurrency.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogCurrency.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogCurrency.show();
    }

    private void showAgeGroupDialog() {

        dialogAgeGroup = new Dialog(this);
        dialogAgeGroup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAgeGroup.setCancelable(true);
        dialogAgeGroup.setContentView(R.layout.currency_dialog_list);
        EditText editText_price_search = dialogAgeGroup.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Age Group");
        RecyclerView rvCurrency = dialogAgeGroup.findViewById(R.id.rvCurrency);
        ageGroupDataList.clear();
        ageGroupDataList.addAll(ageGroupDataList2);

        ageGroupAdapter = new AgeGroupAdapter(this, ageGroupDataList, pos -> {
            tv_select_agegroup.setText(ageGroupDataList.get(pos).getAge());
            tv_select_agegroup.setTextColor(getResources().getColor(R.color.white));
            groupDataList = ageGroupDataList.get(pos);
            AgeGroup = groupDataList.getAge();
            dialogAgeGroup.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(),
                linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(ageGroupAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                ageGroupDataList.clear();
                if (s.toString().isEmpty()) {
                    ageGroupDataList.addAll(ageGroupDataList2);
                } else {
                    for (AgeGroupDataList list : ageGroupDataList2) {
                        if (list.getAge().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            ageGroupDataList.add(list);
                        }
                    }
                }
                ageGroupAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dialogAgeGroup.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogAgeGroup.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogAgeGroup.show();
    }

    private void showPreferredLangDialog() {
        preferredlangDialog = new Dialog(this);
        preferredlangDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        preferredlangDialog.setCancelable(true);
        preferredlangDialog.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = preferredlangDialog.findViewById(R.id.editText_price_search);
        editText_price_search.setHint(getString(R.string.preferred_language));
        RecyclerView rvCurrency = preferredlangDialog.findViewById(R.id.rvCurrency);
        preferredLanguageDataList.clear();
        preferredLanguageDataList.addAll(preferredLanguageDataList2);
        preferedLangAdapter = new PreferedLangAdapter(this, preferredLanguageDataList, pos -> {
            tv_select_language.setText(preferredLanguageDataList.get(pos).getName());
            tv_select_language.setTextColor(getResources().getColor(R.color.white));
            preferredLanguageData = preferredLanguageDataList.get(pos);
            PreferredLang = preferredLanguageData.getName();
            preferredlangDialog.dismiss();
        });
        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(preferedLangAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                preferredLanguageDataList.clear();
                if (s.toString().isEmpty()) {
                    preferredLanguageDataList.addAll(preferredLanguageDataList2);
                } else {
                    for (PreferredLanguageData preferredLanguageData : preferredLanguageDataList2) {
                        if (preferredLanguageData.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            preferredLanguageDataList.add(preferredLanguageData);
                        }
                    }
                }
                preferedLangAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        preferredlangDialog.setCanceledOnTouchOutside(true);
        preferredlangDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        preferredlangDialog.show();
    }

    private void showGenderDialog() {
        genderDialog = new Dialog(this);
        genderDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        genderDialog.setCancelable(true);
        genderDialog.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = genderDialog.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Gender");
        RecyclerView rvCurrency = genderDialog.findViewById(R.id.rvCurrency);
        genderList.add("male");
        genderList.add("female");
        genderList.add("other");
        genderList.add("No preference");
        genderList.clear();
        genderList.addAll(genderList2);

        Log.e("genderlist", genderList.toString());
        Log.e("genderlist2", genderList2.toString());

        genderAdapter = new GenderAdapter(this, genderList, pos -> {
            tv_select_Gender.setText(genderList.get(pos));
            tv_select_Gender.setTextColor(getResources().getColor(R.color.white));
            gender = genderList.get(pos);
            genderDialog.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(genderAdapter);

        genderDialog.setCanceledOnTouchOutside(true);
        genderDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        genderDialog.show();
    }

    private void showCountryDialog() {
        countryDialog = new Dialog(this);
        countryDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        countryDialog.setCancelable(true);
        countryDialog.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = countryDialog.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select Country");
        RecyclerView rvCurrency = countryDialog.findViewById(R.id.rvCurrency);
        countryDataLists.clear();
        countryDataLists.addAll(countryDataLists1);
        countryAdapter = new CountryAdapter(this, countryDataLists, pos -> {
            tv_select_Country.setText(countryDataLists.get(pos).getName());
            tv_select_Country.setTextColor(getResources().getColor(R.color.white));
            countryDataList = countryDataLists.get(pos);
//            PreferredLang = countryDataList.getName();
            getState(String.valueOf(countryDataLists.get(pos).getId()));
            countryDialog.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(countryAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                countryDataLists.clear();
                if (s.toString().isEmpty()) {
                    countryDataLists.addAll(countryDataLists1);
                } else {
                    for (CountryDataList countryDataList : countryDataLists1) {
                        if (countryDataList.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            countryDataLists.add(countryDataList);
                        }
                    }
                }
                countryAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        countryDialog.setCanceledOnTouchOutside(true);
        countryDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        countryDialog.show();
    }

    private void showStateDialog() {
        stateDialog = new Dialog(this);
        stateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        stateDialog.setCancelable(true);
        stateDialog.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = stateDialog.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select State");
        RecyclerView rvCurrency = stateDialog.findViewById(R.id.rvCurrency);
        stateDataLists.clear();
        stateDataLists.addAll(stateDataLists1);
        stateAdapter = new StateAdapter(this, stateDataLists, pos -> {
            tv_select_State.setText(stateDataLists.get(pos).getName());
            tv_select_State.setTextColor(getResources().getColor(R.color.white));
            stateDataList = stateDataLists.get(pos);
//            PreferredLang = countryDataList.getName();
            getCity(String.valueOf(stateDataLists.get(pos).getId()));
            stateDialog.dismiss();
        });
        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(stateAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                stateDataLists.clear();
                if (s.toString().isEmpty()) {
                    stateDataLists.addAll(stateDataLists1);
                } else {
                    for (StateDataList stateDataList : stateDataLists1) {
                        if (stateDataList.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            stateDataLists.add(stateDataList);
                        }
                    }
                }
                stateAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        stateDialog.setCanceledOnTouchOutside(true);
        stateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        stateDialog.show();
    }

    private void showCityDialog() {
        cityDialog = new Dialog(this);
        cityDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cityDialog.setCancelable(true);
        cityDialog.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = cityDialog.findViewById(R.id.editText_price_search);
        editText_price_search.setHint("Select City");
        RecyclerView rvCurrency = cityDialog.findViewById(R.id.rvCurrency);
        cityDataLists.clear();
        cityDataLists.addAll(cityDataLists1);
        cityAdapter = new CityAdapter(this, cityDataLists, pos -> {
            tv_select_City.setText(cityDataLists.get(pos).getName());
            tv_select_City.setTextColor(getResources().getColor(R.color.white));
            cityDataList = cityDataLists.get(pos);
//            PreferredLang = countryDataList.getName();
            cityDialog.dismiss();
        });

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        rvCurrency.setAdapter(cityAdapter);

        editText_price_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newCharText = s.toString().toLowerCase(Locale.getDefault()).trim();
                cityDataLists.clear();
                if (s.toString().isEmpty()) {
                    cityDataLists.addAll(cityDataLists1);
                } else {
                    for (CityDataList cityDataList : cityDataLists1) {
                        if (cityDataList.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                            cityDataLists.add(cityDataList);
                        }
                    }
                }
                cityAdapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        cityDialog.setCanceledOnTouchOutside(true);
        cityDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cityDialog.show();
    }

    public void getCategory() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getCategoryCurrency(this, relativeParent);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    public void getAgeGroup() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getAgeGroup(this, relativeParent);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    private void getUserLanguage() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getPreferredLang(this, relativeParent);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    private void getCountry() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getCountry(this, relativeParent);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    private void getState(String id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            Map<String, String> map = new HashMap<>();
            map.put("id", id);
            profileModel.getState(this, relativeParent, map);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }
    private void getCity(String id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getCity(this, relativeParent, id);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }
    private void checkPermissionForProfile() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                showPictureDialog();
            }
        } else {
            showPictureDialog();
        }
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Profile photo");
        String[] pictureDialogItems = {"Gallery", "Capture photo"};
        pictureDialog.setItems(pictureDialogItems, (dialog, which) -> {
            switch (which) {
                case 0:
                    Utils.choosePhotoFromGallery(this, REQUEST_GALLERY_CODE);
                    break;
                case 1:
                    photoURI = Utils.takePhotoFromCamera(this, REQUEST_CAMERA_CODE);
                    break;
            }
        });
        pictureDialog.setCancelable(true);
        pictureDialog.show();
    }

    private void setImage(Uri uri) {
        String filePath = Utils.getUriRealPath(this, uri);
        File file = new File(filePath);
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
//        editivProfile.setImageBitmap(bitmap);
        try {
            image.setImageBitmap(Utils.getScaledBitmap(this, uri));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.d("File", "Filename " + file.getName());
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
        //  RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        multiBodyPart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        //RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
    }

    private void getTalents() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getUserDetails(this, relativeParent);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    private void CheckAssignmentValidation() {
        if (et_name.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Name can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (editText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Description can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (editText_price_range.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Price can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (tv_select_currency.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Currency can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if(projectid.equals("")){
            if (selectedaddTalentDataList.size() == 0) {
                Toast.makeText(this, "Talents can't be empty", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if(!editTextYoutubeLink.getText().toString().isEmpty()){

            String pattern = "^(http(s)?:\\/\\/)?((w){3}.)?youtu(be|.be)?(\\.com)?\\/.+";
            if (!editTextYoutubeLink.getText().toString().isEmpty() && editTextYoutubeLink.getText().toString().matches(pattern))
            {
                saveAssignment();
            }
            else
            {
                // Not Valid youtube URL
                Toast.makeText(this, "Enter valid youtube url", Toast.LENGTH_SHORT).show();
            }
////            String pattern = "https?:\\/\\/(?:[0-9A-Z-]+\\.)?(?:youtu\\.be\\/|youtube\\.com\\S*[^\\w\\-\\s])([\\w\\-]{11})(?=[^\\w\\-]|$)(?![?=&+%\\w]*(?:['\"][^<>]*>|<\\/a>))[?=&+%\\w]*";
//            if (!editTextYoutubeLink.getText().toString().matches(pattern)) {
//                ///Not Valid youtube URL
//                Toast.makeText(this, "Enter valid youtube url", Toast.LENGTH_SHORT).show();
//
//            }
//            else

        }
        else   saveAssignment();
    }

    private void saveAssignment() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            if (!projectid.equals("")) {
                Map<String, String> map = new HashMap<>();
                map.put("assignement_id", projectid);
                map.put("name", et_name.getText().toString().trim());
                map.put("taxt", editText.getText().toString().trim());

                for (int i = 0; i < list.size(); i++) {
                    map.put("talent_id[" + i + "]", list.get(i) + "");
                    Log.d("sdfadsfads", list.get(i) + "____");
                }


                if (preferredLanguageData != null) {
                    map.put("language", preferredLanguageData.getId() + "");
                } else map.put("language", language_id);

//                Toast.makeText(this, tv_select_Gender.getText().toString(), Toast.LENGTH_SHORT).show();
                if(tv_select_Gender.getText().toString().equals("No preference")){
                   map.put("gender", "no_preference");
                }
               else map.put("gender", tv_select_Gender.getText().toString().trim());
                if (groupDataList != null) {
                    map.put("age", groupDataList.getId() + "");
                } else map.put("age", age_id);

                if (currencyData != null) {
                    map.put("currency", currencyData.getCode() + "");
                } else map.put("currency", currencycode);

                map.put("amount", editText_price_range.getText().toString().trim());
                if (countryDataList != null) {
                    map.put("country", countryDataList.getId() + "");
                }

                if (stateDataList != null) {
                    map.put("state", stateDataList.getId() + "");
                }

                if (cityDataList != null) {
                    map.put("city", cityDataList.getId() + "");
                }

                map.put("youtublink", editTextYoutubeLink.getText().toString().trim());

                profileModel.updateAssignment(this, relativeParent, map);
            } else {
                Map<String, String> map = new HashMap<>();
                map.put("name", et_name.getText().toString().trim());
                map.put("taxt", editText.getText().toString().trim());
                for (int i = 0; i < selectedaddTalentDataList.size(); i++) {
                    map.put("talent_id[" + i + "]", selectedaddTalentDataList.get(i).getId() + "");
                }
                if (preferredLanguageData != null) {
                    map.put("language", preferredLanguageData.getId() + "");
                }

                if(gender.equals("No preference")){
                    map.put("gender", "no_preference");
                }
                else map.put("gender", gender);
                if (groupDataList != null) {
                    map.put("age", groupDataList.getId() + "");
                }
                if (currencyData != null) {
                    map.put("currency", currencyData.getCode() + "");
                }

                map.put("amount", editText_price_range.getText().toString().trim());
                if (countryDataList != null) {
                    map.put("country", countryDataList.getId() + "");
                }

                if (stateDataList != null) {
                    map.put("state", stateDataList.getId() + "");
                }
                if (cityDataList != null) {
                    map.put("city", cityDataList.getId() + "");
                }

                map.put("youtublink", editTextYoutubeLink.getText().toString().trim());

                profileModel.createAssignment(this, relativeParent, map);
            }
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    private void setTalents(ProfileData profileData) {
        if (!(profileData.getTalents().isEmpty())) {
            addTalentDataList.addAll(profileData.getTalents());
            addTalentDataList2.addAll(profileData.getTalents());
            if (profileCreateAssignment != null) {
                if (profileCreateAssignment.getTalant_id() != null) {
                    for (ProfileTalent profileTalent1 : profileData.getTalents()) {
                        if (profileTalent1.getId() == Integer.parseInt(profileCreateAssignment.getTalant_id())) {
                            textView_select_Talents.setText(profileTalent1.getName());
                            profileTalent = profileTalent1;
                        }
                    }
                }
            } else {
                showTalentsDialog();
            }
        } else {
            Toast.makeText(this, "No talents found!!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PROFILE_PERMISSION_REQUEST) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else if (PermissionChecker.checkCallingOrSelfPermission(this,
                    Manifest.permission.CAMERA) != PermissionChecker.PERMISSION_GRANTED ||
                    PermissionChecker.checkCallingOrSelfPermission(this
                            , Manifest.permission.READ_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED ||
                    PermissionChecker.checkCallingOrSelfPermission(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
                profilePermissionDeniedWithDontAskMeAgain = true;
            } else {
                showPictureDialog();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_GALLERY_CODE:
                try {
                    Uri imageUri = data.getData();
                    Bitmap bitmap = Utils.getScaledBitmap(this, imageUri);
                    Uri uri = Utils.getImageUri(this, bitmap);
                    Utils.cropImage(this, uri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case REQUEST_CAMERA_CODE:
                Uri uri2 = photoURI;
                Utils.cropImage(this, uri2);
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    Uri resultUri = result.getUri();
                    setImage(resultUri);
                }
                break;
        }
    }

    private void showTalentsDialog() {
        dialogTalents = new Dialog(this);
        dialogTalents.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTalents.setCancelable(true);
        dialogTalents.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = dialogTalents.findViewById(R.id.editText_price_search);
        editText_price_search.setHint(getString(R.string.talents));
        RecyclerView rvCurrency = dialogTalents.findViewById(R.id.rvCurrency);
        addTalentDataList.clear();
        addTalentDataList.addAll(addTalentDataList2);

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);

        talentsAdapter = new TalentsAdapter(this, addTalentDataList, pos -> {

         /*   if (addTalentDataList.get(pos).getIsseletcted() == null ||addTalentDataList.get(pos).getIsseletcted().equals("0")){
                addTalentDataList.get(pos).setIsseletcted("1");
            }else{
                addTalentDataList.get(pos).setIsseletcted("0");
            }
            talentsAdapter.notifyDataSetChanged();*/
            textView_select_Talents.setText(addTalentDataList.get(pos).getName());
            profileTalent = addTalentDataList.get(pos);
            selectedaddTalentDataList.add(addTalentDataList.get(pos));

            if (list.contains(addTalentDataList.get(pos).getId())) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).equals(addTalentDataList.get(pos).getId())) {
                        list.remove(i);
                    }
                }
            } else {
                list.add(addTalentDataList.get(pos).getId() + "");
            }
            selectedProjectTalentAdapter.notifyDataSetChanged();
            dialogTalents.dismiss();
        });
        rvCurrency.setAdapter(talentsAdapter);

        editText_price_search.addTextChangedListener(this);
        dialogTalents.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialogTalents.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogTalents.show();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2)
    {
        filter(s.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void filter(String charText) {
        String newCharText = charText.toLowerCase(Locale.getDefault()).trim();
        addTalentDataList.clear();
        if (charText.isEmpty()) {
            addTalentDataList.addAll(addTalentDataList2);
        } else {
            for (ProfileTalent profileTalent : addTalentDataList2) {
                if (profileTalent.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                    addTalentDataList.add(profileTalent);
                }
            }
        }
        talentsAdapter.notifyDataSetChanged();
    }

    private void viewProjectDetails(String id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.viewProjectDetails(this, relativeParent, id);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    public void openCalender(final TextView tvDate) {
        Calendar newCalendar = Calendar.getInstance();
        final SimpleDateFormat dateFormatter = new
                SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this, R.style.DatePickerTheme,
                (view, year, monthOfYear, dayOfMonth) -> {
                    String select_date;
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    select_date = dateFormatter.format(newDate.getTime()) + "";
                    tvDate.setText(select_date);
                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }
    public void projectTemplates() {
//        final Dialog dialog = new Dialog(CreateAssignentNew.this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProjectTemplatesModel> call = apiInterface.projectTemplates("Bearer "+accessToken);
        call.enqueue(new Callback<ProjectTemplatesModel>() {
            @Override
            public void onResponse(Call<ProjectTemplatesModel> call, Response<ProjectTemplatesModel> response) {
//                dialog.dismiss();
                ProjectTemplatesModel user = response.body();
                // if(user.message.equals("Already Request sent.")){
                TemplatesAdapter templatesAdapter = new TemplatesAdapter(CreateAssignentNew.this,user.data,(pos) ->{
                    editText.setText(user.data.get(pos).description);
                });
                rvTemplates.setAdapter(templatesAdapter);
                // }
            }
            @Override
            public void onFailure(Call<ProjectTemplatesModel> call, Throwable t) {
//                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


    public class TemplatesAdapter extends RecyclerView.Adapter<TemplatesAdapter.ViewHolder> {
        private Context context;
        private ItemClickListener itemClickListener;
        List<ProjectTemplatesModel.Data> list;

        public TemplatesAdapter(Context context,   List<ProjectTemplatesModel.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.itemClickListener = itemClickListener;
            this.list = list;
        }

        @Override
        public TemplatesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.layout_templates, parent, false);
            return new TemplatesAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(TemplatesAdapter.ViewHolder holder, final int position) {
            holder.tv_talent.setText(list.get(position).title);
            holder.linear.setOnClickListener(v -> itemClickListener.itemClick(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
        public class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout linear;
            TextView tv_talent;
            public ViewHolder(View itemView) {
                super(itemView);
                linear = itemView.findViewById(R.id.linear);
                tv_talent = itemView.findViewById(R.id.tv_talent);
            }
        }
    }


}