package com.twc.tiecoon.Activities;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.PermissionChecker;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.internal.Utility;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Adapters.AgeGroupAdapter;
import com.twc.tiecoon.Adapters.MyProfileCompletedProjectsAdapter;
import com.twc.tiecoon.Adapters.MyProfileWorkedOnProjectsAdapter;
import com.twc.tiecoon.Adapters.MyprofileOngoingAssignmentAdapters;
import com.twc.tiecoon.Adapters.OtherProfileBrandsHorizontalAdapters;
import com.twc.tiecoon.Adapters.OtherProfileProductAdapters;
import com.twc.tiecoon.Adapters.OtherProfileServiceAdapters;
import com.twc.tiecoon.Adapters.OtherProfileTalentAdapters;
import com.twc.tiecoon.Adapters.SimilarProfileAdapter;
import com.twc.tiecoon.Adapters.UserHomeTabTalentAdapters;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.Fragments.HomeFeedbackFragment;
import com.twc.tiecoon.Fragments.HomeReorganisationFragment;
import com.twc.tiecoon.Fragments.HomeTestimonialFragment;
import com.twc.tiecoon.Fragments.ProfileFragment;
import com.twc.tiecoon.Fragments.SuccessfulCollaborationFragment;
import com.twc.tiecoon.Fragments.SuccessfulConnectionFragment;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.WorkedOnProject;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.AgeGroupDataList;
import com.twc.tiecoon.network.response.ConnectionModel;
import com.twc.tiecoon.network.response.FeedbackModel;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.HomeUserDetailsFeedback;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsReOrganisation;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.HomeUserDetailsTestimonial;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.HomeUserDetailsUserDetils;
import com.twc.tiecoon.network.response.HomeUserPreferedLanguage;
import com.twc.tiecoon.network.response.ProfileCompleteAssignment;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.network.response.SimilarProfile;
import com.twc.tiecoon.network.response.UserPdf;
import com.twc.tiecoon.utils.FileDownloader;
import com.twc.tiecoon.utils.FragmentTestimonialListListner;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.LinePagerIndicatorDecoration;
import com.twc.tiecoon.utils.Utils;
import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Observable;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtherUserProfileActivity extends AbstractFragmentActivity implements
        View.OnClickListener {
    private HomeModel homeModel = new HomeModel();
    private String[] AUDIO_CALLING_PERMISSIONS = {Manifest.permission.CALL_PHONE};
    private final int AUDIO_CALLING_PERMISSIONS_REQUEST = 402;
    private boolean audioCallPermissionDeniedWithDontAskMeAgain = false;
    private LinearLayout linearParent, linearScreen;
    private ImageView ivBack, imgProfile, imgShare, imgLike, imgJoinGroupOne, imgJoinGroupTwo, imgJoinGroupCreateNew;
    public static final int UPLOAD_TALENT = 101;
    public static final int UPLOAD_PRODUCTS = 102;
    public static final int UPLOAD_SERVICES = 103;
    public static final int UPLOAD_BRANDS = 104;
    private static final int CREATEASSGN = 105;
    private TextView tvName, tv_rating, tvResponse,tvMember,tvMyProfileDes, tvAddress, tvLang, tvWorks, tvTalentsTotal,tvWorkedProject, tvProductAndServicesTotal, tvViewContact, tvProfileTotal;
    private RelativeLayout rlEditInfo;
    private RecyclerView recyclerView_profile, recyclerView_add_talents, recyclerView_upload_product,
            recyclerView_upload_services, recyclerView_upload_brands, recyclerView_ongoing_assignments, recyclerView_worked_assignments,recyclerView_completed_assignments;

    private LinearLayout linear_upload_brands, linearConnect, linearChat, linear3Profile;
    private TabLayout tabLayout, tabLayout2;
    private ViewPager viewPager, viewPager2;

    private Fragment successfulCollaborationFragment, successfulConnectionFragment, testimonialFragment, feedbackFragment,
            reorganisationFragment;
    private List<List<HomeUserDetailsUploadTalent>> listTalent = new ArrayList<>();
    private List<List<HomeUserDetailsProduct>> uploadProductList = new ArrayList<>();
    private List<List<HomeUserDetailsService>> uploadServiceList = new ArrayList<>();
    private List<ProfileCreateAssignment> homeUserDetailOngoingAssignmentList = new ArrayList<>();
    private List<ProfileCompleteAssignment> profileCompleteAssignmentArrayList = new ArrayList<>();
    private List<WorkedOnProject> workedOnProjectArrayList = new ArrayList<>();
    private List<AddBrandsData> homeUserDetailBrandList = new ArrayList<>();
    private OtherProfileProductAdapters myProfileProductAdapters;
    private OtherProfileServiceAdapters myProfileServiceAdapters;
    private OtherProfileTalentAdapters myprofileTalentAdapters;
    private OtherProfileBrandsHorizontalAdapters myProfileBrandsHorizontalAdapters;
    private MyprofileOngoingAssignmentAdapters myprofileOngoingAssignmentAdapters;
    private MyProfileCompletedProjectsAdapter myProfileCompletedProjectsAdapter;
    private MyProfileWorkedOnProjectsAdapter myProfileWorkedOnProjectsAdapter;
    private FragmentTestimonialListListner fragmentTestimonialListListner;
    private ArrayList<HomeUserDetailsFeedback> homeUserDetailsFeedbackArrayList = new ArrayList<>();
    private ArrayList<HomeUserDetailsReOrganisation> homeUserDetailsReOrganisationArrayList = new ArrayList<>();
    private int updatePos;
    private int userId;
    private boolean userLike = false;
    private boolean likeUnlikeChange = false;
    private HomeUserDetailData homeUserDetailData = null;
    private String phone = "";
    private String name="";
    private String deviceToken="";
    private String qbid="";
    TextView tvOngoingAssignnmets,tvCompleteProject,tvRevenuePurchase,tvUserName;
    ImageView ivStatusDot;
    RecyclerView recyclerViewUploadImageList;
    SwipeRefreshLayout swiperefresh;

    SimilarProfileAdapter similarProfileAdapter;
    RecyclerView recycler_similarprofiles;
    TextView tvSimilarprofile;
    TextView linecollab,tvcollab,lineconnection,tvconnection,tvRate,tvFeedback;
    RecyclerView rv_connections,rv_collab,rvFeedback;
    LinearLayout linearconnection,linearCollab,linearCollabButton;

    String flag = "";

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_other_user_profile);
        userId = Integer.parseInt(getIntent().getStringExtra("UserId"));
        Log.e("id", String.valueOf(userId));
        if(getIntent().hasExtra("flag")){
            flag = getIntent().getStringExtra("flag");
        }
        initView(userId);
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    private void initView(int id) {
        linearParent = findViewById(R.id.linearParent);
        linearScreen = findViewById(R.id.linearScreen);
        linecollab = findViewById(R.id.linecollab);
        tvcollab = findViewById(R.id.tvcollab);
        lineconnection = findViewById(R.id.lineconnection);
        tvconnection = findViewById(R.id.tvconnection);
        rv_connections = findViewById(R.id.rv_connections);
        rv_collab = findViewById(R.id.rv_collab);
        rvFeedback = findViewById(R.id.rvFeedback);
        linearconnection = findViewById(R.id.linearconnection);
        linearCollab = findViewById(R.id.linearCollab);
        ivBack = findViewById(R.id.ivBack);
        imgProfile = findViewById(R.id.imgProfile);
        tvRate = findViewById(R.id.tvRate);
        tvFeedback = findViewById(R.id.tvFeedback);
        tvName = findViewById(R.id.tvName);
        tvUserName = findViewById(R.id.tvUserName);
        imgShare = findViewById(R.id.imgShare);
        imgLike = findViewById(R.id.imgLike);
        ivStatusDot = findViewById(R.id.ivStatusDot);
        tvSimilarprofile = findViewById(R.id.tvSimilarprofile);
        recycler_similarprofiles = findViewById(R.id.recycler_similarprofiles);
        tvResponse = findViewById(R.id.tvResponse);
        tv_rating = findViewById(R.id.tv_rating);
        tvMyProfileDes = findViewById(R.id.tvMyProfileDes);
        tvCompleteProject = findViewById(R.id.tvCompleteProject);
        tvRevenuePurchase = findViewById(R.id.tvRevenuePurchase);
        tvOngoingAssignnmets = findViewById(R.id.tvOngoingAssignnmets);
        recyclerViewUploadImageList = findViewById(R.id.recyclerViewUploadImageList);

        tvAddress = findViewById(R.id.tvAddress);
        tvMember = findViewById(R.id.tvMember);
        tvLang = findViewById(R.id.tvLang);
        tvWorks = findViewById(R.id.tvWorks);
        swiperefresh = findViewById(R.id.swiperefresh);

        recyclerView_profile = findViewById(R.id.recyclerView_profile);
        linearConnect = findViewById(R.id.linearConnect);
        linearChat = findViewById(R.id.linearChat);
        linear3Profile = findViewById(R.id.linear3Profile);

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        tvTalentsTotal = findViewById(R.id.tvTalentsTotal);
        tvProductAndServicesTotal = findViewById(R.id.tvProductAndServicesTotal);
        tvProfileTotal = findViewById(R.id.tvProfileTotal);

        recyclerView_add_talents = findViewById(R.id.recyclerView_add_talents);
        recyclerView_upload_product = findViewById(R.id.recyclerView_upload_product);
        recyclerView_upload_services = findViewById(R.id.recyclerView_upload_services);
        linear_upload_brands = findViewById(R.id.linear_upload_brands);
        recyclerView_upload_brands = findViewById(R.id.recyclerView_upload_brands);

        tvViewContact = findViewById(R.id.tvViewContact);
        tvWorkedProject = findViewById(R.id.tvWorkedProject);
        linearCollabButton = findViewById(R.id.linearCollabButton);

        recyclerView_ongoing_assignments = findViewById(R.id.recyclerView_ongoing_assignments);
        recyclerView_completed_assignments = findViewById(R.id.recyclerView_completed_assignments1);
        recyclerView_worked_assignments = findViewById(R.id.recyclerView_worked_assignments);

        rv_connections.setHasFixedSize(true);
        rv_collab.setHasFixedSize(true);
        rvFeedback.setHasFixedSize(true);
        rv_connections.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
        rvFeedback.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
        rv_collab.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
        imgProfile.setOnClickListener(view -> {
            Intent intent = new Intent(this,ImageActivity.class);
            intent.putExtra("image",homeUserDetailData.getProfile());
            intent.putExtra("key","hide");
            startActivity(intent);
        });


        myProfileBrandsHorizontalAdapters = new OtherProfileBrandsHorizontalAdapters(this, homeUserDetailBrandList, new ItemClickListener() {
            @Override
            public void itemClick(int pos) {
                Intent intent = new Intent(OtherUserProfileActivity.this, ProductServiceBrandsBuyActivity.class);
                intent.putExtra("brandData", homeUserDetailBrandList.get(pos));
                startActivity(intent);
            }
        });
        recyclerView_upload_brands.setAdapter(myProfileBrandsHorizontalAdapters);

        tabLayout2 = findViewById(R.id.tabLayout2);
        viewPager2 = findViewById(R.id.viewPager2);
        setupViewPager2(viewPager2);
        tabLayout2.setupWithViewPager(viewPager2);


        ivBack.setOnClickListener(this);
        linearConnect.setOnClickListener(this);
        linearCollabButton.setOnClickListener(this);
        imgShare.setOnClickListener(this);
        imgLike.setOnClickListener(this);
        linearChat.setOnClickListener(this);
        linear3Profile.setOnClickListener(this);
        linear_upload_brands.setOnClickListener(this);
        tvViewContact.setOnClickListener(this);
        swiperefresh.setOnRefreshListener(() -> getUserDetails(id));

        ImageView iv_menu = findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(v -> {
            //Creating the instance of PopupMenu
            PopupMenu popup = new PopupMenu(OtherUserProfileActivity.this, iv_menu);
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.report_popupmenu, popup.getMenu());

            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId())
                    {
                        case R.id.one:
                            confirm();
                            break;

//                        case R.id.two:
//
//                            break;
                    }

                    return true;
                }
            });

            popup.show();//showing popup menu

        });

        findViewById(R.id.imgpdf).setOnClickListener(v -> downloadPDF(String.valueOf(userId)) );

        linearCollab.setOnClickListener(v -> {
            linecollab.setVisibility(View.VISIBLE);
            rv_collab.setVisibility(View.VISIBLE);
            lineconnection.setVisibility(View.GONE);
            rv_connections.setVisibility(View.GONE);
        });
        linearconnection.setOnClickListener(v -> {
            linecollab.setVisibility(View.GONE);
            rv_collab.setVisibility(View.GONE);
            lineconnection.setVisibility(View.VISIBLE);
            rv_connections.setVisibility(View.VISIBLE);
        });

        getUserDetails(id);
    }

    private void confirm() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("Are you sure, you want to report this user?");
        btnRemove.setText("REPORT");
        tvTitle.setVisibility(View.GONE);
//        tvTitle.setText("Collaboration Request");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            Intent intent = new Intent(OtherUserProfileActivity.this,ReportReasonScreen.class);
            intent.putExtra("user_id",String.valueOf(userId));
            startActivity(intent);

        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.imgLike:
                setUserLike(userId);
                break;
            case R.id.imgShare:
                String url = String.format(Constant.SHARE_URL, userId+"");
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Tiecoon");
                    String message = "Check this Profile : "+ url +"\nTo check, Download this Application : https://play.google.com/store/apps/details?id=" + "com.twc.tiecoon"+ "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, message);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.linearConnect:
                ProgressDialog progressDialog ;
                progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Connecting to chat, please wait...");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(true);
                progressDialog.show();
                ArrayList<Integer> occupantIdsList = new ArrayList<Integer>();
                occupantIdsList.add(Integer.valueOf(homeUserDetailData.getQuickbloxuser_id()));

                QBChatDialog dialog = new QBChatDialog();
                dialog.setType(QBDialogType.PRIVATE);
                dialog.setOccupantsIds(occupantIdsList);

// or just use DialogUtils
//QBChatDialog dialog = DialogUtils.buildPrivateDialog(recipientId);

                QBRestChatService.createChatDialog(dialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog result, Bundle params) {
//                        Toast.makeText(getContext(), "SUCCESS", Toast.LENGTH_SHORT).show();
                        Log.e("result",result.toString());
                        Log.e("qid",homeUserDetailData.getQuickbloxuser_id());

//                        QBChatDialog chatDialog = new QBChatDialog();
//                        chatDialog.setCustomData(result);
//                        QBUser qbUser=new QBUser();
//                        qbUser.setCustomData(result.toString());
//                        QBChatDialog existingPrivateDialog = QbDialogHolder.getInstance().getPrivateDialogWithUser(qbUser);
//
//                        if (existingPrivateDialog != null) {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        ChatActivity.startForResult(OtherUserProfileActivity.this,101,homeUserDetailData.getId()+"", result,homeUserDetailData.getHomeUserDetailsUserDetils().getName());
//                        }
//                        Intent intent = new Intent(view.getContext(), ChatActivity.class);
//                        intent.putExtra("dialogId",result.getDialogId());
////                intent.putExtra("name", homeUserDetailDataList.get(updatePosition).getHomeUserDetailsUserDetils().getName());
////                intent.putExtra("UserId",String.valueOf(homeUserDetailDataList.get(updatePosition).getId()) );
////                intent.putExtra("devicedToken",String.valueOf(homeUserDetailDataList.get(updatePosition).getDevice_token()) );
////                intent.putExtra("tag","1");
//                        startActivity(intent);
                    }

                    @Override
                    public void onError(QBResponseException responseException) {
                        progressDialog.dismiss();
                        Toast.makeText(OtherUserProfileActivity.this, "ERRROR", Toast.LENGTH_SHORT).show();
                    }
                });
                break;

                case R.id.linearCollabButton:
                    startActivity(new Intent(this, CollaborationActivity.class).
                            putExtra("userid", String.valueOf(userId))
                            .putExtra("qbid",homeUserDetailData.getQuickbloxuser_id())
                            .putExtra("name",homeUserDetailData.getHomeUserDetailsUserDetils().getName())
                    );
                    break;

            case R.id.linearChat:
                openCollaborationDialog();
                break;
            case R.id.linear3Profile:

                break;
            case R.id.linear_upload_brands:
                Intent i = new Intent(this, WatermarkActivity.class);
                i.putExtra("type", "brands");
                startActivityForResult(i, UPLOAD_BRANDS);
                break;
            case R.id.linearJoinGroupOne:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_checked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_unchecked);
                break;
            case R.id.linearJoinGroupTwo:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_checked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_unchecked);
                break;
            case R.id.linearNewGroup:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_checked);
                break;
            case R.id.tvViewContact:
                if (phone.isEmpty())
                {
                    Toast.makeText(this,"Contact details not found",Toast.LENGTH_SHORT).show();
                }else
                {
                    checkPermissionForAudioCall();
                }

                break;
        }
    }

    private void checkPermissionForAudioCall() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, AUDIO_CALLING_PERMISSIONS))) {
                Utils.dismissProDialog();
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs call permission to make call.",
                            AUDIO_CALLING_PERMISSIONS, AUDIO_CALLING_PERMISSIONS_REQUEST);
                } else if (audioCallPermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs call permission to make call.",
                            AUDIO_CALLING_PERMISSIONS_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, AUDIO_CALLING_PERMISSIONS, AUDIO_CALLING_PERMISSIONS_REQUEST);
                }
            } else {
                makeAudioCall();
            }
        } else {
            makeAudioCall();
        }
    }

    private void makeAudioCall() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case AUDIO_CALLING_PERMISSIONS_REQUEST:
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
                    Log.e("Permission: ", "User Has Denied Permission");
                } else if (PermissionChecker.checkCallingOrSelfPermission(this,
                        Manifest.permission.CALL_PHONE) != PermissionChecker.PERMISSION_GRANTED) {
                    Log.e("Permission: ", "User Has Denied Permission with Don't Ask Again");
                    audioCallPermissionDeniedWithDontAskMeAgain = true;
                } else {
                    makeAudioCall();
                }
                break;
        }
    }


    private void openCollaborationDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_groupchat);

        LinearLayout linearJoinGroupOne = dialog.findViewById(R.id.linearJoinGroupOne);
        LinearLayout linearJoinGroupTwo = dialog.findViewById(R.id.linearJoinGroupTwo);
        LinearLayout linearNewGroup = dialog.findViewById(R.id.linearNewGroup);
        imgJoinGroupOne = dialog.findViewById(R.id.imgJoinGroupOne);
        imgJoinGroupTwo = dialog.findViewById(R.id.imgJoinGroupTwo);
        imgJoinGroupCreateNew = dialog.findViewById(R.id.imgJoinGroupCreateNew);

        EditText etGroupName = dialog.findViewById(R.id.etGroupName);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        linearJoinGroupOne.setOnClickListener(this);
        linearJoinGroupTwo.setOnClickListener(this);
        linearNewGroup.setOnClickListener(this);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void getUserDetails(int id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getOtherUserDetailsById(this, linearParent, id);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }


    private void downloadPDF(String  id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.downloadUserPDF(this, linearParent, id);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void setUserLike(int id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            userLike = true;
            homeModel.setUserLike(this, linearParent, id);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        successfulConnectionFragment = new SuccessfulConnectionFragment();
        successfulCollaborationFragment = new SuccessfulCollaborationFragment();

        adapter.addFragment(successfulConnectionFragment, "Successful Connection");
        adapter.addFragment(successfulCollaborationFragment, "Successful & Collaboration");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void update(Observable observable, Object o) {
        swiperefresh.setRefreshing(false);
        if (o instanceof HomeUserDetailData) {
            homeUserDetailData = (HomeUserDetailData) o;

            myProfileProductAdapters = new OtherProfileProductAdapters(this, uploadProductList,homeUserDetailData.getQuickbloxuser_id(), pos -> {
                List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = uploadProductList.get(pos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                Intent i = new Intent(OtherUserProfileActivity.this, CreateProductServiceActivity.class);
                i.putExtra("type", "product");
                i.putExtra("categoryId", categoryId);
                startActivityForResult(i, UPLOAD_PRODUCTS);
            });
            recyclerView_upload_product.setAdapter(myProfileProductAdapters);

            myProfileServiceAdapters = new OtherProfileServiceAdapters(this ,uploadServiceList,homeUserDetailData.getQuickbloxuser_id(), pos -> {
                List<HomeUserDetailsService> homeUserDetailsUploadTalentList = uploadServiceList.get(pos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                Intent i = new Intent(OtherUserProfileActivity.this, CreateProductServiceActivity.class);
                i.putExtra("type", "service");
                i.putExtra("categoryId", categoryId);
                startActivityForResult(i, UPLOAD_SERVICES);
            });
            recyclerView_upload_services.setAdapter(myProfileServiceAdapters);

            myprofileTalentAdapters = new OtherProfileTalentAdapters(this, listTalent, homeUserDetailData.getQuickbloxuser_id(),pos -> {
                updatePos = pos;
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                Intent i = new Intent(OtherUserProfileActivity.this, WatermarkActivity.class);
                i.putExtra("categoryId", categoryId);
                startActivityForResult(i, UPLOAD_TALENT);
            });
            recyclerView_add_talents.setAdapter(myprofileTalentAdapters);

            myprofileOngoingAssignmentAdapters = new MyprofileOngoingAssignmentAdapters(this, homeUserDetailOngoingAssignmentList, new ItemClickListenerExtraParam() {
                @Override
                public void itemClick(int pos, String type) {
                    if(type.equals("view")){
                        Intent i = new Intent(OtherUserProfileActivity.this, ViewAssignment.class);
                        i.putExtra("project_id", homeUserDetailOngoingAssignmentList.get(pos).getId()+"");
                        i.putExtra("qbID", homeUserDetailData.getQuickbloxuser_id());
                        i.putExtra("name", homeUserDetailData.getHomeUserDetailsUserDetils().getName());
                        startActivity(i);
                    }

                }
            });
            recyclerView_ongoing_assignments.setAdapter(myprofileOngoingAssignmentAdapters);

            myProfileCompletedProjectsAdapter = new MyProfileCompletedProjectsAdapter(this, profileCompleteAssignmentArrayList, new ItemClickListenerExtraParam() {
                @Override
                public void itemClick(int pos, String type) {
                    if(type.equals("view")){
                        Intent i = new Intent(OtherUserProfileActivity.this, ViewAssignment.class);
                        i.putExtra("project_id", profileCompleteAssignmentArrayList.get(pos).getId()+"");
                        i.putExtra("qbID", homeUserDetailData.getQuickbloxuser_id());
                        i.putExtra("name", homeUserDetailData.getHomeUserDetailsUserDetils().getName());
                        startActivity(i);
                    }
                }
            });
            recyclerView_completed_assignments.setAdapter(myProfileCompletedProjectsAdapter);

            myProfileWorkedOnProjectsAdapter = new MyProfileWorkedOnProjectsAdapter(this, workedOnProjectArrayList, new ItemClickListenerExtraParam() {
                @Override
                public void itemClick(int pos, String type) {
                    if(type.equals("view")){
                        Intent i = new Intent(OtherUserProfileActivity.this, ViewAssignment.class);
                        i.putExtra("project_id", workedOnProjectArrayList.get(pos).getProject_id()+"");
                        i.putExtra("qbID", homeUserDetailData.getQuickbloxuser_id());
                        i.putExtra("name", homeUserDetailData.getHomeUserDetailsUserDetils().getName());
                        startActivity(i);
                    }
                    if(type.equals("rate")){
                        Toast.makeText(OtherUserProfileActivity.this, "Rate Us", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            recyclerView_worked_assignments.setAdapter(myProfileWorkedOnProjectsAdapter);

            if (homeUserDetailData.getPhone() != null) {
                phone = homeUserDetailData.getPhone();
            }
             if (homeUserDetailData.getUsername() != null) {
                 tvUserName.setText("(WY"+homeUserDetailData.getUsername()+")");
            }
            if (homeUserDetailData.getRespondtime() != null) {
                tvResponse.setText("Responds within "+homeUserDetailData.getRespondtime()+" hours");
            }
              if (homeUserDetailData.getTotalearningbyid()!=null) {
                  tvRevenuePurchase.setText(homeUserDetailData.getTotalearningbyid());
//                  Toast.makeText(this, homeUserDetailData.getTotalearning(), Toast.LENGTH_SHORT).show();
            }
               if (homeUserDetailData.getAvgRating() != null) {
                    tvRate.setText(homeUserDetailData.getAvgRating());
                }
            if (userLike) {
                likeUnlikeChange = true;
                userLike = false;
                if (homeUserDetailData.isIs_like()) {
                    imgLike.setImageResource(R.drawable.ic_like_solid);
                } else {
                    imgLike.setImageResource(R.drawable.ic_like);
                }
            }
            else {
                linearScreen.setVisibility(View.VISIBLE);
                if (homeUserDetailData.getHomeUserDetailsUserDetils() != null) {
                    HomeUserDetailsUserDetils homeUserDetailsUserDetils = homeUserDetailData.getHomeUserDetailsUserDetils();
                    if (homeUserDetailsUserDetils != null) {
                        if (homeUserDetailsUserDetils.getName() != null) {
                            name=homeUserDetailsUserDetils.getName();
                            String des = homeUserDetailsUserDetils.getName().substring(0, 1).toUpperCase() + homeUserDetailsUserDetils.getName().substring(1);
                            tvName.setText(des);
                        }
                       if (homeUserDetailData.isIs_like()) {
                            imgLike.setImageResource(R.drawable.ic_like_solid);
                        } else {
                            imgLike.setImageResource(R.drawable.ic_like);
                        }
                        switch (homeUserDetailData.getUser_status()) {
                            case "Online":
                                ivStatusDot.setColorFilter(ContextCompat.getColor(this, R.color.green));
                                break;
                            case "Away":
                                ivStatusDot.setColorFilter(ContextCompat.getColor(this, R.color.yellow));
                                break;
                            case "Inactive":
                                ivStatusDot.setColorFilter(ContextCompat.getColor(this, R.color.white));
                                break;
                        }
                        List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentsNew = homeUserDetailData.getHomeUserDetailsUploadTalent();
                        if (homeUserDetailsUploadTalentsNew != null && !(homeUserDetailsUploadTalentsNew.isEmpty())) {
                            UserHomeTabTalentAdapters userHomeTabTalentAdapters = new UserHomeTabTalentAdapters(this, homeUserDetailsUploadTalentsNew, new ItemClickListener() {
                                @Override
                                public void itemClick(int pos) {
                                    ApplicationClass.uploadImageListDataArrayList.clear();
                                    if (homeUserDetailsUploadTalentsNew.get(pos).getUpload_image() != null && !(homeUserDetailsUploadTalentsNew.get(pos).getUpload_image().isEmpty())) {
                                        ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsUploadTalentsNew.get(pos).getUpload_image());
                                    }

                                    Intent intent = new Intent(OtherUserProfileActivity.this, ProductServiceBrandsBuyActivity.class);
                                    intent.putExtra("talentData", homeUserDetailsUploadTalentsNew.get(pos));
                                    startActivity(intent);
                                }
                            });
                            recyclerViewUploadImageList.setAdapter(userHomeTabTalentAdapters);
                        }
                        if(homeUserDetailData.getDevice_token()!=null)
                        {
                            deviceToken=homeUserDetailData.getDevice_token();
                        }
                        if (homeUserDetailsUserDetils.getBio() != null) {
                            String des = homeUserDetailsUserDetils.getBio().substring(0, 1).toUpperCase() + homeUserDetailsUserDetils.getBio().substring(1);
                            tvMyProfileDes.setText(des);
                        }
                        if (homeUserDetailsUserDetils.getLocation() != null) {
                            String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                            tvAddress.setText("Lives in "+homeUserDetailData.getHomeUserDetailsUserDetils().getLocation()+" "+currentTime);
                        }
                        if (homeUserDetailData.getCreated() != null) {
                            tvMember.setText("Member Since "+homeUserDetailData.getCreated());
                        }

                        if (homeUserDetailData.getConnection_count() != null) {
                            tvconnection.setText("Successful Connections ("+homeUserDetailData.getConnection_count()+")");
                        }
                        if (homeUserDetailData.getCollaboration_count() != null) {
                            tvcollab.setText("Successful Collaborations ("+homeUserDetailData.getCollaboration_count()+")");
                        }

                        if (homeUserDetailsUserDetils.getWorks() != null) {
                            tvWorks.setText(new StringBuilder().append("Works ").append(homeUserDetailData.getHomeUserDetailsUserDetils().getWorks()));
                        }

                        ConnectionAdapter connectionAdapter = new ConnectionAdapter(this,homeUserDetailData.getConnection(), pos -> {
                            if(Integer.parseInt(homeUserDetailData.getConnection().get(pos).getUser_id()) == ApplicationClass.appPreferences.getUserId() ){
                                Intent i = new Intent(this, UserHomeActivity.class);
                                i.putExtra("open","profile");
                                startActivity(i);
                            }
                            else {
                                Intent i = new Intent(this, OtherUserProfileActivity.class);
                                i.putExtra("UserId", String.valueOf(homeUserDetailData.getConnection().get(pos).getUser_id()));
                                startActivity(i);
                            }

                        });
                        rv_connections.setAdapter(connectionAdapter);

                        ConnectionAdapter connectionAdapter1 = new ConnectionAdapter(this,homeUserDetailData.getCollaboration(), pos -> {
                            if(String.valueOf(homeUserDetailData.getCollaboration().get(pos).getUser_id()).equals(ApplicationClass.appPreferences.getUserId()+"") ){
                                Intent i = new Intent(this, UserHomeActivity.class);
                                i.putExtra("open","profile");
                                startActivity(i);
                            }
                            else {
                                Intent i = new Intent(this, OtherUserProfileActivity.class);
                                i.putExtra("UserId", String.valueOf(homeUserDetailData.getCollaboration().get(pos).getUser_id()));
                                startActivity(i);
                            }
                        });
                        rv_collab.setAdapter(connectionAdapter1);

                        feedbackList(String.valueOf(userId));
                    }
                }

                List<HomeUserPreferedLanguage> homeUserPreferedLanguageList = homeUserDetailData.getPreferredLanguage();
                if (homeUserPreferedLanguageList != null && !(homeUserPreferedLanguageList.isEmpty())) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (HomeUserPreferedLanguage homeUserPreferedLanguage : homeUserPreferedLanguageList) {
                        if (homeUserPreferedLanguage != null && homeUserPreferedLanguage.getLanguage() != null) {
                            if (homeUserPreferedLanguage.getLanguage().getName() != null) {
                                if (stringBuilder.length() == 0) {
                                    stringBuilder.append(homeUserPreferedLanguage.getLanguage().getName());
                                } else {
                                    stringBuilder.append("/").append(homeUserPreferedLanguage.getLanguage().getName());
                                }
                            }
                        }
                    }
                    tvLang.setText(stringBuilder.toString());
                }

                if (homeUserDetailData.getProfile() != null ) {
                    String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailData.getPath()).append("/").append(homeUserDetailData.getProfile())).toString();
                    Glide.with(this).load(homeUserDetailData.getProfile()).placeholder(R.drawable.ic_dummy_user)
                            .error(R.drawable.ic_dummy_user)
                            .into(imgProfile);
                }

                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents1 = homeUserDetailData.getHomeUserDetailsUploadTalent();
                if (homeUserDetailsUploadTalents1 != null && !(homeUserDetailsUploadTalents1.isEmpty())) {
                    listTalent.clear();
                    setUploadTalentData(homeUserDetailsUploadTalents1);
                }
                List<HomeUserDetailsProduct> homeUserDetailsProductList1 = homeUserDetailData.getHomeUserDetailsProducts();
                if (homeUserDetailsProductList1 != null && !(homeUserDetailsProductList1.isEmpty())) {
                    uploadProductList.clear();
                    setProducttData(homeUserDetailsProductList1);
                }
                List<HomeUserDetailsService> homeUserDetailsServiceList1 = homeUserDetailData.getHomeUserDetailsServices();
                if (homeUserDetailsServiceList1 != null && !(homeUserDetailsServiceList1.isEmpty())) {
                    uploadServiceList.clear();
                    setServiceData(homeUserDetailsServiceList1);
                }
                List<AddBrandsData> addBrandsDataList = homeUserDetailData.getHomeUserDetailBrand();
                if (addBrandsDataList != null && !(addBrandsDataList.isEmpty())) {
                    homeUserDetailBrandList.clear();
                    homeUserDetailBrandList.addAll(addBrandsDataList);
                    myProfileBrandsHorizontalAdapters.notifyDataSetChanged();
                }
                List<HomeUserDetailsTestimonial> homeUserDetailsTestimonialList = homeUserDetailData.getHomeUserDetailsTestimonial();
                if (homeUserDetailsTestimonialList != null && !(homeUserDetailsTestimonialList.isEmpty())) {
                    fragmentTestimonialListListner.setList(homeUserDetailsTestimonialList);
                }

                if (homeUserDetailData.getHomeUserDetailsFeedback() != null && !(homeUserDetailData.getHomeUserDetailsFeedback().isEmpty())) {
                    homeUserDetailsFeedbackArrayList.clear();
                    homeUserDetailsFeedbackArrayList.addAll(homeUserDetailData.getHomeUserDetailsFeedback());
                }

                if (homeUserDetailData.getHomeUserDetailsReOrganisation() != null && !(homeUserDetailData.getHomeUserDetailsReOrganisation().isEmpty())) {
                    homeUserDetailsReOrganisationArrayList.clear();
                    homeUserDetailsReOrganisationArrayList.addAll(homeUserDetailData.getHomeUserDetailsReOrganisation());
                }

                if (homeUserDetailData.getHomeUserDetailCreateAssignments() != null && !(homeUserDetailData.getHomeUserDetailCreateAssignments().isEmpty())) {
                    homeUserDetailOngoingAssignmentList.clear();
                    for (ProfileCreateAssignment profileCreateAssignment : homeUserDetailData.getHomeUserDetailCreateAssignments()) {
//                        if (profileCreateAssignment.getPath() != null && profileCreateAssignment.getImage() != null) {
                            homeUserDetailOngoingAssignmentList.add(profileCreateAssignment);
//                        }
                    }
                    myprofileOngoingAssignmentAdapters.notifyDataSetChanged();
                }   else tvOngoingAssignnmets.setVisibility(View.GONE);

                if (homeUserDetailData.getComplete_assignment() != null && !(homeUserDetailData.getComplete_assignment().isEmpty())) {
                    profileCompleteAssignmentArrayList.clear();
                    for (ProfileCompleteAssignment profileCreateAssignment : homeUserDetailData.getComplete_assignment()) {
//                        if (profileCreateAssignment.getPath() != null && profileCreateAssignment.getImage() != null) {
                        profileCompleteAssignmentArrayList.add(profileCreateAssignment);
//                        }
                    }
                    myProfileCompletedProjectsAdapter.notifyDataSetChanged();
                }
                else tvCompleteProject.setVisibility(View.GONE);

            if (homeUserDetailData.getComplatedprojectworkon() != null && !(homeUserDetailData.getComplatedprojectworkon().isEmpty())) {
                    workedOnProjectArrayList.clear();
                    for (WorkedOnProject workedOnProject : homeUserDetailData.getComplatedprojectworkon()) {
//                        if (profileCreateAssignment.getPath() != null && profileCreateAssignment.getImage() != null) {
                        workedOnProjectArrayList.add(workedOnProject);
//                        }
                    }
                    myProfileWorkedOnProjectsAdapter.notifyDataSetChanged();
                }
                else tvWorkedProject.setVisibility(View.GONE);
            }

            if(this!=null){
                getSimilarProfiles();
            }

        }
        else if (o instanceof SimilarProfile) {
            SimilarProfile similarProfile = (SimilarProfile) o;
            List<SimilarProfile.Data> similarDataList = similarProfile.getData();
            Log.e("simmiii",similarDataList.toString());
            if(similarDataList.size()>0){
                similarProfileAdapter = new SimilarProfileAdapter(this, similarDataList, new ItemClickListener() {
                    @Override
                    public void itemClick(int pos) {
                        Intent i = new Intent(OtherUserProfileActivity.this, OtherUserProfileActivity.class);
                        i.putExtra("UserId", String.valueOf(similarDataList.get(pos).getUser_id()));
                        startActivity(i);
                    }
                }) ;
                recycler_similarprofiles.setAdapter(similarProfileAdapter);
            }
            else tvSimilarprofile.setVisibility(View.GONE);
        }

       else if (o instanceof UserPdf) {
            UserPdf userPdf= (UserPdf) o;
            displayPdf(userPdf.getData().getPdf());
        }

        }

    private void displayPdf(String fileName) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(Uri.parse(fileName), "application/pdf");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_GRANT_READ_URI_PERMISSION);

        if (intent.resolveActivity(getPackageManager()) == null) {

        } else {
            startActivity(intent);
        }
    }

    @SuppressLint("SetTextI18n")
    private void setUploadTalentData(List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents1) {
        List<HomeUserDetailsUploadTalent> listTalentNew = new ArrayList<>(homeUserDetailsUploadTalents1);
        Iterator<HomeUserDetailsUploadTalent> iteratormain = homeUserDetailsUploadTalents1.iterator();
        while (iteratormain.hasNext()) {
            HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = (HomeUserDetailsUploadTalent) iteratormain.next();
            List<HomeUserDetailsUploadTalent> newlist = new ArrayList<>();
            Iterator<HomeUserDetailsUploadTalent> iteratorsub = listTalentNew.iterator();
            if (iteratorsub.hasNext()) {
                while (iteratorsub.hasNext()) {
                    HomeUserDetailsUploadTalent homeUserDetailsUploadTalent1 = (HomeUserDetailsUploadTalent) iteratorsub.next();
                    if (homeUserDetailsUploadTalent1.getCategory() != null) {
                        if ((homeUserDetailsUploadTalent.getCategoryId().equals(homeUserDetailsUploadTalent1.getCategoryId()))) {
                            newlist.add(homeUserDetailsUploadTalent1);
                            iteratorsub.remove();
                        }
                    }
                }
            } else {
                break;
            }
            if (!(newlist.isEmpty())) {
                iteratormain.remove();
                listTalent.add(newlist);
            }
        }
        int talentTotal = listTalent.size();
        if (talentTotal > 0) {
            tvTalentsTotal.setText("Talent" + "(" + talentTotal + ")");
        }
        myprofileTalentAdapters.notifyDataSetChanged();
    }

    private void setProducttData(List<HomeUserDetailsProduct> homeUserDetailsProductList) {
        List<HomeUserDetailsProduct> listTalentNew = new ArrayList<>(homeUserDetailsProductList);
        Iterator<HomeUserDetailsProduct> iteratormain = homeUserDetailsProductList.iterator();
        while (iteratormain.hasNext()) {
            HomeUserDetailsProduct homeUserDetailsUploadTalent = (HomeUserDetailsProduct) iteratormain.next();
            List<HomeUserDetailsProduct> newlist = new ArrayList<>();
            Iterator<HomeUserDetailsProduct> iteratorsub = listTalentNew.iterator();
            if (iteratorsub.hasNext()) {
                while (iteratorsub.hasNext()) {
                    HomeUserDetailsProduct homeUserDetailsUploadTalent1 = (HomeUserDetailsProduct) iteratorsub.next();
                    if (homeUserDetailsUploadTalent1.getCategory() != null) {
                        if ((homeUserDetailsUploadTalent.getCategoryId().equals(homeUserDetailsUploadTalent1.getCategoryId()))) {
                            newlist.add(homeUserDetailsUploadTalent1);
                            iteratorsub.remove();
                        }
                    }
                }
            } else {
                break;
            }
            if (!(newlist.isEmpty())) {
                iteratormain.remove();
                uploadProductList.add(newlist);
                qbid = homeUserDetailData.getQuickbloxuser_id();
                Log.e("productqb",qbid);
            }
        }

    }

    private void setServiceData(List<HomeUserDetailsService> homeUserDetailsProductList) {
        List<HomeUserDetailsService> listTalentNew = new ArrayList<>(homeUserDetailsProductList);
        Iterator<HomeUserDetailsService> iteratormain = homeUserDetailsProductList.iterator();
        while (iteratormain.hasNext()) {
            HomeUserDetailsService homeUserDetailsUploadTalent = (HomeUserDetailsService) iteratormain.next();
            List<HomeUserDetailsService> newlist = new ArrayList<>();
            Iterator<HomeUserDetailsService> iteratorsub = listTalentNew.iterator();
            if (iteratorsub.hasNext()) {
                while (iteratorsub.hasNext()) {
                    HomeUserDetailsService homeUserDetailsUploadTalent1 = (HomeUserDetailsService) iteratorsub.next();
                    if (homeUserDetailsUploadTalent1.getCategory() != null) {
                        if ((homeUserDetailsUploadTalent.getCategoryId() == homeUserDetailsUploadTalent1.getCategoryId())) {
                            newlist.add(homeUserDetailsUploadTalent1);
                            iteratorsub.remove();
                        }
                    }
                }
            } else {
                break;
            }
            if (!(newlist.isEmpty())) {
                iteratormain.remove();
                uploadServiceList.add(newlist);
                qbid = homeUserDetailData.getQuickbloxuser_id();
                Log.e("serviceqb",qbid);
            }
        }
        int productAndServiceTotal = uploadProductList.size() + uploadServiceList.size();
        if (productAndServiceTotal > 0) {
            tvProductAndServicesTotal.setText("Product & Services" + "(" + productAndServiceTotal + ")");
        }
        myProfileServiceAdapters.notifyDataSetChanged();
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setupViewPager2(ViewPager viewPager) {
        ViewPagerAdapter2 adapter = new ViewPagerAdapter2(getSupportFragmentManager());
        testimonialFragment = new HomeTestimonialFragment();
        fragmentTestimonialListListner = (FragmentTestimonialListListner) testimonialFragment;
        feedbackFragment = new HomeFeedbackFragment();
        reorganisationFragment = new HomeReorganisationFragment();

        Bundle bundleFeeback = new Bundle();
        bundleFeeback.putParcelableArrayList("feedback", homeUserDetailsFeedbackArrayList);
        feedbackFragment.setArguments(bundleFeeback);

        Bundle bundleReorganisation = new Bundle();
        bundleReorganisation.putParcelableArrayList("reorganisation", homeUserDetailsReOrganisationArrayList);
        reorganisationFragment.setArguments(bundleReorganisation);

        adapter.addFragment(testimonialFragment, "Testimonial");
        adapter.addFragment(feedbackFragment, "Feedback");
        adapter.addFragment(reorganisationFragment, "Reorganisation");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter2 extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter2(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case UPLOAD_TALENT:
                if (resultCode == RESULT_OK) {
                    HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = (HomeUserDetailsUploadTalent) data.getParcelableExtra("UploadData");
                    List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(updatePos);
                    homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                    listTalent.set(updatePos, homeUserDetailsUploadTalentList);
                    myprofileTalentAdapters.notifyItemChanged(updatePos);
                }
                break;
            case UPLOAD_PRODUCTS:
                if (resultCode == RESULT_OK) {
                    HomeUserDetailsProduct homeUserDetailsUploadTalent = (HomeUserDetailsProduct) data.getParcelableExtra("UploadData");
                    List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = uploadProductList.get(updatePos);
                    homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                    uploadProductList.set(updatePos, homeUserDetailsUploadTalentList);
                    myProfileProductAdapters.notifyItemChanged(updatePos);
                }
                break;
            case UPLOAD_BRANDS:
                if (resultCode == RESULT_OK) {
                    AddBrandsData addBrandsData = (AddBrandsData) data.getParcelableExtra("UploadData");
                    homeUserDetailBrandList.add(addBrandsData);
                    myProfileBrandsHorizontalAdapters.notifyDataSetChanged();
                }
                break;
            case CREATEASSGN:
                if (resultCode == RESULT_OK) {
                    ProfileCreateAssignment profileCreateAssignment = data.getParcelableExtra("createAssign");
                    homeUserDetailOngoingAssignmentList.add(profileCreateAssignment);
                    myprofileOngoingAssignmentAdapters.notifyDataSetChanged();
                }
                break;
        }
    }

    private void getSimilarProfiles() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getSimilarProfiles(this, linearParent, String.valueOf(userId));
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void onBackPressed() {
        if (likeUnlikeChange) {
            ApplicationClass.homeUserDetailData = homeUserDetailData;
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
        }
        if(flag.equals("1")){
            startActivity(new Intent(this,UserHomeActivity.class));
        }
        else super.onBackPressed();
    }

    public static class ConnectionAdapter extends RecyclerView.Adapter<ConnectionAdapter.MyViewHolder> {

        private final List<ConnectionModel> list;
        Context context;
        private ItemClickListener itemClickListener;
        public ConnectionAdapter(Context context, List<ConnectionModel> list,
                                 ItemClickListener itemClickListener) {
            this.context=context;
            this.list=list;
            this.itemClickListener=itemClickListener;

        }

        @NotNull
        @Override
        public ConnectionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.conncoll, parent, false);
            return new ConnectionAdapter.MyViewHolder(itemView);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull ConnectionAdapter.MyViewHolder holder, int position) {
            Glide.with(context).load(list.get(position).getProfile_image()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.imgProfile);


            holder.tvUsername.setVisibility(View.GONE);
            holder.imgProfile.setOnClickListener(v -> itemClickListener.itemClick(position));

        }
        @Override
        public int getItemCount() {
            return list.size();
        }

        static class MyViewHolder extends RecyclerView.ViewHolder {

            CircleImageView imgProfile;
            CardView layoutStory;
            TextView tvUsername;
            MyViewHolder(View view) {
                super(view);
                imgProfile = view.findViewById(R.id.imgProfile);
                layoutStory = view.findViewById(R.id.layoutStory);
                tvUsername = view.findViewById(R.id.tvUsername);
            }

        }
    }

    public void feedbackList(String id) {
        final Dialog dialog = new Dialog(OtherUserProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<FeedbackModel> call = apiInterface.feedbackList("Bearer "+accessToken,id,"project");
        call.enqueue(new Callback<FeedbackModel>() {
            @Override
            public void onResponse(Call<FeedbackModel> call, Response<FeedbackModel> response) {
                dialog.dismiss();
                FeedbackModel user = response.body();
                assert user != null;
                if(user.getData().size()>0){
                    rvFeedback.setVisibility(View.VISIBLE);
                    tvFeedback.setVisibility(View.VISIBLE);
                    FeedbackAdapter feedbackAdapter = new FeedbackAdapter(OtherUserProfileActivity.this,
                            user.getData(), pos -> {
                        showFeedbackDialog();
                    });
                    rvFeedback.setAdapter(feedbackAdapter);
                }
                else {
                    rvFeedback.setVisibility(View.GONE);
                    tvFeedback.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<FeedbackModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void feedbackListexpand(String id,RecyclerView rvFeedback1) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<FeedbackModel> call = apiInterface.feedbackList("Bearer "+accessToken,id,"project");
        call.enqueue(new Callback<FeedbackModel>() {
            @Override
            public void onResponse(Call<FeedbackModel> call, Response<FeedbackModel> response) {
//                dialog.dismiss();
                FeedbackModel user = response.body();
                assert user != null;
                if(user.getData().size()>0){

                    FeedbackAdapterExpand feedbackAdapter = new FeedbackAdapterExpand(OtherUserProfileActivity.this,
                            user.getData(), pos -> {
                        showFeedbackDialog();
                    });
                    rvFeedback1.setAdapter(feedbackAdapter);
                }
            }

            @Override
            public void onFailure(Call<FeedbackModel> call, Throwable t) {
//                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    private void showFeedbackDialog() {
       Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dailog_feedback);

        RecyclerView rvFeedback = dialog.findViewById(R.id.rvFeedback);
        rvFeedback.setHasFixedSize(true);
        rvFeedback.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
        rvFeedback.addItemDecoration(new LinePagerIndicatorDecoration());
        feedbackListexpand(String.valueOf(userId),rvFeedback);
        dialog.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public static class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {
        private Context context;
        private List<FeedbackModel.Data> list;
        private ItemClickListener itemClickListener;
        public FeedbackAdapter(Context context,List<FeedbackModel.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public FeedbackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.feedback_item_list, parent, false);
            return new FeedbackAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(FeedbackAdapter.ViewHolder holder, final int position) {
            Glide.with(context).load(list.get(position).getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.imgProfile);
            holder.tv_UserName.setText(list.get(position).getProject_name());
            holder.textViewDescription.setText(list.get(position).getMassage());
            holder.rating_feedback.setRating(Float.parseFloat(String.valueOf(list.get(position).getRating())));
            holder.cardView.setOnClickListener(v -> itemClickListener.itemClick(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            CircleImageView imgProfile;
            TextView tv_UserName,textViewDescription;
            RatingBar rating_feedback;
            CardView cardView;
            public ViewHolder(View itemView) {
                super(itemView);
                imgProfile = itemView.findViewById(R.id.imgProfile);
                tv_UserName = itemView.findViewById(R.id.tvName);
                rating_feedback = itemView.findViewById(R.id.simpleRatingBar);
                textViewDescription = itemView.findViewById(R.id.textViewDescription);
                cardView = itemView.findViewById(R.id.cardView);
            }
        }

    }
    public static class FeedbackAdapterExpand extends RecyclerView.Adapter<FeedbackAdapterExpand.ViewHolder> {
        private Context context;
        private List<FeedbackModel.Data> list;
        private ItemClickListener itemClickListener;
        public FeedbackAdapterExpand(Context context,List<FeedbackModel.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public FeedbackAdapterExpand.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.item_expand_feedback, parent, false);
            return new FeedbackAdapterExpand.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(FeedbackAdapterExpand.ViewHolder holder, final int position) {
            Glide.with(context).load(list.get(position).getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.imgProfile);
            holder.tv_UserName.setText(list.get(position).getProject_name());
            holder.textViewDescription.setText(list.get(position).getMassage());
            holder.rating_feedback.setRating(Float.parseFloat(String.valueOf(list.get(position).getRating())));

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            CircleImageView imgProfile;
            TextView tv_UserName,textViewDescription;
            RatingBar rating_feedback;
            CardView cardView;
            public ViewHolder(View itemView) {
                super(itemView);
                imgProfile = itemView.findViewById(R.id.imgProfile);
                tv_UserName = itemView.findViewById(R.id.tvName);
                rating_feedback = itemView.findViewById(R.id.simpleRatingBar);
                textViewDescription = itemView.findViewById(R.id.textViewDescription);
                cardView = itemView.findViewById(R.id.cardView);
            }
        }
    }
}