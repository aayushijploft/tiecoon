package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.quickblox.chat.QBMessageStatusesManager;
import com.quickblox.chat.QBSystemMessagesManager;
import com.quickblox.chat.listeners.QBMessageStatusListener;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.Adapters.AttachmentPreviewAdapter;
import com.twc.tiecoon.R;
import com.twc.tiecoon.prefrences.AppPreferences;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.chat.ChatMessageListener;

import java.util.ArrayList;
import java.util.List;

public class MessageActivity extends AppCompatActivity implements QBMessageStatusListener {
    private static final String TAG = MessageActivity.class.getSimpleName();

    public static final int REQUEST_CODE_SELECT_PEOPLE = 752;
    private static final int REQUEST_CODE_ATTACHMENT = 721;
    private static final int PERMISSIONS_FOR_SAVE_FILE_IMAGE_REQUEST = 1010;

    public static final String PROPERTY_FORWARD_USER_NAME = "origin_sender_name";
    public static final String EXTRA_DIALOG_ID = "dialogId";
    public static final String EXTRA_IS_NEW_DIALOG = "isNewDialog";
    public static final String IS_IN_BACKGROUND = "is_in_background";

    public static final String ORDER_RULE = "order";
    private static final String ORDER_VALUE = "desc string created_at";

    public static final long TYPING_STATUS_DELAY = 2000;
    public static final long TYPING_STATUS_INACTIVITY_DELAY = 10000;
    private static final long SEND_TYPING_STATUS_DELAY = 3000;
    public static final int MAX_ATTACHMENTS_COUNT = 1;
    public static final int MAX_MESSAGE_SYMBOLS_LENGTH = 1000;


    private ProgressBar progressBar;
    private EditText messageEditText;
    private ImageView attachmentBtnChat;
    private TextView typingStatus;
    private QBUser currentUser;

    private LinearLayout attachmentPreviewContainerLayout;
    private RecyclerView chatMessagesRecyclerView;

//    private ChatAdapter chatAdapter;
    private AttachmentPreviewAdapter attachmentPreviewAdapter;
    private ConnectionListener chatConnectionListener;
//    private ImageAttachClickListener imageAttachClickListener;
//    private VideoAttachClickListener videoAttachClickListener;
//    private FileAttachClickListener fileAttachClickListener;
//    private MessageLongClickListenerImpl messageLongClickListener;
    private QBMessageStatusesManager qbMessageStatusesManager;
//    private ChatMessageListener chatMessageListener = new ChatMessageListener();
//    private DialogsManager dialogsManager = new DialogsManager();
//    private SystemMessagesListener systemMessagesListener = new SystemMessagesListener();
    private QBSystemMessagesManager systemMessagesManager;

    private List<QBChatMessage> messagesList;
    private QBChatDialog qbChatDialog;
    private ArrayList<QBChatMessage> unShownMessages;
    private int skipPagination = 0;
    private Boolean checkAdapterInit = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        AppPreferences.getInstance().delete(IS_IN_BACKGROUND);
    }

    @Override
    public void processMessageDelivered(String s, String s1, Integer integer) {

    }

    @Override
    public void processMessageRead(String s, String s1, Integer integer) {

    }
}