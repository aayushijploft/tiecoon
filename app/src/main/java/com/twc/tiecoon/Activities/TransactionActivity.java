package com.twc.tiecoon.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.Adapters.TransactionAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.DeviceListModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.TransactionModel;
import com.twc.tiecoon.network.response.UserTransactionModel;
import com.twc.tiecoon.network.response.WithdrawModel;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.Utils;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.Observable;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class TransactionActivity extends AbstractFragmentActivity {
    HomeModel homeModel = new HomeModel();
    LinearLayout linearParent;
    RecyclerView rvTransaction;
    Button btnWithdraw;
    LinearLayout layoutTransaction;
    TextView tvNote,tvEarnings;
    String WithdrwaAmount = "";
    SwipeRefreshLayout swiperefresh;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        ImageView iv_back=findViewById(R.id.iv_back);
        linearParent=findViewById(R.id.linearParent);
        rvTransaction=findViewById(R.id.rvTransaction);
        swiperefresh = findViewById(R.id.swiperefresh);
        btnWithdraw=findViewById(R.id.btnWithdraw);
        layoutTransaction=findViewById(R.id.layoutTransaction);
        tvNote=findViewById(R.id.tvNote);
        tvEarnings=findViewById(R.id.tvEarnings);
        rvTransaction.setHasFixedSize(true);
        rvTransaction.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        iv_back.setOnClickListener(view -> onBackPressed());
        getUserTransaction(true);
        swiperefresh.setOnRefreshListener(() -> {
            if (Utils.isDeviceOnline(this)) {
                getUserTransaction(false);
            }
        });
        btnWithdraw.setOnClickListener(v -> withdrawConfirmation());

    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    private void getTransactionList() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getTransactionList(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void update(Observable o, Object arg) {

    }

    public void getUserTransaction(boolean isshow) {
    final Dialog dialog = new Dialog(TransactionActivity.this);
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialog.setContentView(R.layout.dialog_login);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    if(isshow){
        dialog.show();
    }

    final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
    String accessToken = ApplicationClass.appPreferences.GetToken();
    Call<UserTransactionModel> call = apiInterface.usertransaction("Bearer "+accessToken);
    call.enqueue(new Callback<UserTransactionModel>() {
        @Override
        public void onResponse(Call<UserTransactionModel> call, Response<UserTransactionModel> response) {
            dialog.dismiss();
            swiperefresh.setRefreshing(false);
            UserTransactionModel user = response.body();
            TransactionUserAdapter transactionAdapter = new TransactionUserAdapter(TransactionActivity.this
                    ,user.data
                    ,(pos, type) ->{
                if(type.equals("talent")){
                    startActivity(new Intent(TransactionActivity.this,UserTransactionActivity.class)
                            .putExtra("type","talent").putExtra("user_id",user.data.get(pos).userid));
                }
                if(type.equals("product")){
                    startActivity(new Intent(TransactionActivity.this,UserTransactionActivity.class)
                            .putExtra("type","product").putExtra("user_id",user.data.get(pos).userid));
                }
                if(type.equals("service")){
                    startActivity(new Intent(TransactionActivity.this,UserTransactionActivity.class)
                            .putExtra("type","service").putExtra("user_id",user.data.get(pos).userid));
                }
                if(type.equals("project")){
                    startActivity(new Intent(TransactionActivity.this,UserTransactionActivity.class)
                            .putExtra("type","project").putExtra("user_id",user.data.get(pos).userid));
                }
            });

            rvTransaction.setAdapter(transactionAdapter);
        }
        @Override
        public void onFailure(Call<UserTransactionModel> call, Throwable t) {
            dialog.dismiss();
            Log.d("response", "gh" + t.getMessage());
        }
        });
    }


    private void withdrawConfirmation() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        btnRemove.setText("CONFIRM");
        tvConfirm.setText("Check your Account number/Paypal details in KYC page before pressing the Confirm.");

        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            sendWithdrawRequest();
        });

        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void sendWithdrawRequest() {
        final Dialog dialog = new Dialog(TransactionActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<WithdrawModel> call = apiInterface.withdrawAmount("Bearer "+accessToken,WithdrwaAmount,"INR");

        call.enqueue(new Callback<WithdrawModel>() {
            @Override
            public void onResponse(@NotNull Call<WithdrawModel> call, @NotNull Response<WithdrawModel> response) {
                dialog.dismiss();
                WithdrawModel user = response.body();
                Log.e("user",user.toString());
                Toast.makeText(TransactionActivity.this, "Your withdraw request has been sent to Admin", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<WithdrawModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public class TransactionUserAdapter extends RecyclerView.Adapter<TransactionUserAdapter.MyViewHolder> {


        Context context;
        ItemClickListenerExtraParam itemClickListenerExtraParam;
        List<UserTransactionModel.Data> list;

        public TransactionUserAdapter(Context context,List<UserTransactionModel.Data> list,
                                      ItemClickListenerExtraParam itemClickListenerExtraParam) {
            this.context=context;
            this.list=list;
            this.itemClickListenerExtraParam=itemClickListenerExtraParam;
        }

        @NotNull
        @Override
        public TransactionUserAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_transactions_userlist, parent, false);
            return new TransactionUserAdapter.MyViewHolder(itemView);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull TransactionUserAdapter.MyViewHolder holder, int position) {
            holder.tvTalent.setOnClickListener(v -> itemClickListenerExtraParam.itemClick(position,"talent"));
            holder.tvProduct.setOnClickListener(v -> itemClickListenerExtraParam.itemClick(position,"product"));
            holder.tvService.setOnClickListener(v -> itemClickListenerExtraParam.itemClick(position,"service"));
            holder.tvProject.setOnClickListener(v -> itemClickListenerExtraParam.itemClick(position,"project"));
            Glide.with(context)
                    .load(list.get(position).image)
                    .thumbnail(Glide.with(context).load(R.drawable.talent1))
                    .error(Glide.with(context)
                            .load(R.drawable.talent1))
                    .into(holder.imgUserimage);
            holder.tvName.setText(list.get(position).name);
            holder.imgUserimage.setOnClickListener(v -> {
                Intent i = new Intent(context, OtherUserProfileActivity.class);
                i.putExtra("UserId",list.get(position).userid+"");
                context.startActivity(i);
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

         class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tvProject,tvService,tvProduct,tvTalent,tvName;
            CircleImageView imgUserimage;
            MyViewHolder(View view) {
                super(view);
                tvName = view.findViewById(R.id.tvName);
                tvTalent = view.findViewById(R.id.tvTalent);
                tvProduct = view.findViewById(R.id.tvProduct);
                tvService = view.findViewById(R.id.tvService);
                tvProject = view.findViewById(R.id.tvProject);
                imgUserimage = view.findViewById(R.id.imgUserimage);
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}