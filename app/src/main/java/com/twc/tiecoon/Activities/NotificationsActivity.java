package com.twc.tiecoon.Activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.R;

public class NotificationsActivity extends AppCompatActivity {

    private ImageView iv_back;
    private RelativeLayout header;
    private RecyclerView recycler_notifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        initView();
    }

    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        header = (RelativeLayout) findViewById(R.id.header);
        recycler_notifications = (RecyclerView) findViewById(R.id.recycler_notifications);
        recycler_notifications.setAdapter(new NotificationAdapter());
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(NotificationsActivity.this).inflate(R.layout.list_notification, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        }

        @Override
        public int getItemCount() {
            return 10;
        }

        public long getItemId(int position) {
            return position;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView bell1;
            private LinearLayout ll_bell;
            private TextView tv_notification;
            private TextView tv_time;
            private TextView tv_new;
            private LinearLayout ll_cardNotification;

            public MyViewHolder(View v) {
                super(v);
                bell1 = v.findViewById(R.id.bell1);
                ll_bell = v.findViewById(R.id.ll_bell);
                tv_notification = v.findViewById(R.id.tv_notification);
                tv_time = v.findViewById(R.id.tv_time);
                tv_new = v.findViewById(R.id.tv_new);
                ll_cardNotification = v.findViewById(R.id.ll_cardNotification);

            }
        }

    }

}