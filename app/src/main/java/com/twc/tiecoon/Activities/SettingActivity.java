package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.DeviceListModel;
import com.twc.tiecoon.network.response.ProjectTemplatesModel;
import com.twc.tiecoon.network.response.SuccessModel;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingActivity extends AppCompatActivity {

    RecyclerView rvDevices;
    Button btnDeactivate,btnDelete,tvLogout;
    TextView tvDeactivated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        rvDevices = findViewById(R.id.rvDevices);
        btnDeactivate = findViewById(R.id.btnDeactivate);
        tvDeactivated = findViewById(R.id.tvDeactivated);
        btnDelete = findViewById(R.id.btnDelete);
        tvLogout = findViewById(R.id.tvLogout);
        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());

        btnDeactivate.setOnClickListener(v -> showDeactivateDialog());
        btnDelete.setOnClickListener(v -> showDeleteDialog());

        tvLogout.setOnClickListener(v -> {
            showLogoutDialog();
        });
        getdevices();

    }

    public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.ViewHolder> {
        private Context context;
        private ItemClickListener itemClickListener;
        List<DeviceListModel.Data> list;

        public DevicesAdapter(Context context,
                              List<DeviceListModel.Data> list,
                             ItemClickListener itemClickListener) {
            this.context = context;
            this.itemClickListener = itemClickListener;
            this.list = list;
        }

        @Override
        public DevicesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.layout_devices, parent, false);
            return new DevicesAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(DevicesAdapter.ViewHolder holder, final int position) {
            holder.tv_device.setText("* "+list.get(position).devicename + ", "+list.get(position).location);
            holder.linear.setOnClickListener(v -> itemClickListener.itemClick(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
        public class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout linear;
            TextView tv_device;
            public ViewHolder(View itemView) {
                super(itemView);
                linear = itemView.findViewById(R.id.linear);
                tv_device = itemView.findViewById(R.id.tv_device);
            }
        }
    }

    private void showDeactivateDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to Deactivate Account?");
        btnRemove.setText("DEACTIVATE");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            deactivateUser();
        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showDeleteDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to Delete Account?");
        btnRemove.setText("DELETE");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            deleteUser();
        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showLogoutDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to Logout from all devices?");
        btnRemove.setText("LOGOUT");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            logout();
        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void deactivateUser() {
        final Dialog dialog = new Dialog(SettingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SuccessModel> call = apiInterface.deactivateAccount("Bearer "+accessToken);
        call.enqueue(new Callback<SuccessModel>() {
            @Override
            public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                dialog.dismiss();
                SuccessModel user = response.body();

                btnDeactivate.setBackground(getResources().getDrawable(R.drawable.rounded_btn));
                btnDeactivate.setText("Activate Account");
                btnDeactivate.setEnabled(false);
                tvDeactivated.setVisibility(View.VISIBLE);
                Toast.makeText(SettingActivity.this, "Your account has been Deactivated",
                        Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(Call<SuccessModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void getdevices() {
        final Dialog dialog = new Dialog(SettingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeviceListModel> call = apiInterface.loggindevicelists("Bearer "+accessToken);
        call.enqueue(new Callback<DeviceListModel>() {
            @Override
            public void onResponse(Call<DeviceListModel> call, Response<DeviceListModel> response) {
                dialog.dismiss();
                DeviceListModel user = response.body();
                if(user.status.equals("1")){
                    btnDeactivate.setBackground(getResources().getDrawable(R.drawable.rounded_btn));
                    btnDeactivate.setText("Activate Account");
                    btnDeactivate.setEnabled(false);
                    tvDeactivated.setVisibility(View.VISIBLE);
                }
                else {
                    btnDeactivate.setBackground(getResources().getDrawable(R.drawable.red_round));
                    btnDeactivate.setText("Deactivate Account");
                    btnDeactivate.setEnabled(true);
                    tvDeactivated.setVisibility(View.GONE);
                }
                DevicesAdapter devicesAdapter = new DevicesAdapter(SettingActivity.this,user.data,pos -> {

                });
                rvDevices.setAdapter(devicesAdapter);
            }
            @Override
            public void onFailure(Call<DeviceListModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void logout() {
        final Dialog dialog = new Dialog(SettingActivity.this);
        dialog.requestWindowFeature (Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SuccessModel> call = apiInterface.Logout("Bearer "+accessToken);
        call.enqueue(new Callback<SuccessModel>() {
            @Override
            public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                dialog.dismiss();
                SuccessModel user = response.body();
                ApplicationClass.appPreferences.SetToken("");
                LoginManager.getInstance().logOut();
                startActivity(new Intent(SettingActivity.this, LoginActivity.class));
            }
            @Override
            public void onFailure(Call<SuccessModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void deleteUser() {

        final Dialog dialog = new Dialog(SettingActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SuccessModel> call = apiInterface.deleteAccount("Bearer "+accessToken);
        call.enqueue(new Callback<SuccessModel>() {

            @Override
            public void onResponse(Call<SuccessModel> call, Response<SuccessModel> response) {
                dialog.dismiss();
                SuccessModel user = response.body();
                ApplicationClass.appPreferences.SetToken("");
                LoginManager.getInstance().logOut();
                startActivity(new Intent(SettingActivity.this, LoginActivity.class));
            }

            @Override
            public void onFailure(Call<SuccessModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

}