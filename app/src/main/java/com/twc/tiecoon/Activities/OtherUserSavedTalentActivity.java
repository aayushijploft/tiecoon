package com.twc.tiecoon.Activities;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.twc.tiecoon.Adapters.OtherProfileTalentHorizontalAdapters;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.SaveTalent;
import com.twc.tiecoon.network.response.SaveTalentData;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class OtherUserSavedTalentActivity extends AbstractFragmentActivity {
    private final HomeModel homeModel = new HomeModel();
    private LinearLayout linearParent;
    private OtherProfileTalentHorizontalAdapters otherProfileTalentHorizontalAdapters;
    List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList=new ArrayList<>();
    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_other_user_saved_talent);
        ImageView ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(view -> onBackPressed());
        linearParent = findViewById(R.id.linearParent);
        RecyclerView recyclerViewSavedTalents = findViewById(R.id.recyclerViewSavedTalents);
        otherProfileTalentHorizontalAdapters = new OtherProfileTalentHorizontalAdapters(this,
                homeUserDetailsUploadTalentList, pos -> {
            Intent intent=new Intent(this, ProductServiceBrandsBuyActivity.class);
            intent.putExtra("talentData",homeUserDetailsUploadTalentList.get(pos));
            intent.putExtra("qbID", homeUserDetailsUploadTalentList.get(pos).getQbid());
//            intent.putExtra("talent","true");
            startActivity(intent);
//            Toast.makeText(this, homeUserDetailsUploadTalentList.get(pos).getQbid(), Toast.LENGTH_SHORT).show();
        });
        recyclerViewSavedTalents.setAdapter(otherProfileTalentHorizontalAdapters);

        getSavedBrands();
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    @Override
    public void update(Observable observable, Object o) {
        SaveTalent saveTalent = (SaveTalent) o;
        List<SaveTalentData> saveTalentDataList = saveTalent.getData();
        if (saveTalentDataList != null && !(saveTalentDataList.isEmpty())) {
            for (SaveTalentData saveTalentData : saveTalentDataList) {
                if (saveTalentData != null && saveTalentData.getTalent() != null) {
                    homeUserDetailsUploadTalentList.add(saveTalentData.getTalent());
                }
            }
            otherProfileTalentHorizontalAdapters.notifyDataSetChanged();
        } else {
            Toast.makeText(this, "No saved talents", Toast.LENGTH_SHORT).show();
        }
    }

    private void getSavedBrands() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getSavedTalentsList(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }
}