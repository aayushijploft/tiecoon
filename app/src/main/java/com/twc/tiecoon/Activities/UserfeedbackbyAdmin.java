package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.UserFeedback;
import com.twc.tiecoon.network.response.WithdrawModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserfeedbackbyAdmin extends AppCompatActivity {

    ImageView iv_back;
    RecyclerView rvGroupUser;
    String projectid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userfeedbackby_admin);
        projectid = getIntent().getStringExtra("projectid");
        iv_back = findViewById(R.id.iv_back);

        rvGroupUser = findViewById(R.id.rvGroupUser);
        rvGroupUser.setHasFixedSize(true);
        rvGroupUser.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));

        findViewById(R.id.iv_back).setOnClickListener(v -> {
            startActivity(new Intent(UserfeedbackbyAdmin.this,UserHomeActivity.class));
        });
        userlistsforfeedback(projectid);
    }

    public void userlistsforfeedback(String projectid) {
        final Dialog dialog = new Dialog(UserfeedbackbyAdmin.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<UserFeedback> call = apiInterface.projectEndUserList("Bearer "+accessToken,projectid);

        call.enqueue(new Callback<UserFeedback>() {
            @Override
            public void onResponse(Call<UserFeedback> call, Response<UserFeedback> response) {
                dialog.dismiss();
                UserFeedback user = response.body();
                Log.e("user",user.toString());
                FeedbackUserAdapter feedbackUserAdapter = new FeedbackUserAdapter(UserfeedbackbyAdmin.this,user.getData(),projectid);
                rvGroupUser.setAdapter(feedbackUserAdapter);
//                Toast.makeText(ViewAssignment.this, "Your withdraw request has been sent to Admin", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<UserFeedback> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public static class FeedbackUserAdapter extends RecyclerView.Adapter<ViewAssignment.FeedbackUserAdapter.ViewHolder> {
        private final Context context;
        private final List<UserFeedback.Data> list;
        String projectid;


        public FeedbackUserAdapter(Context context, List<UserFeedback.Data> list,String projectid) {
            this.context = context;
            this.list = list;
            this.projectid = projectid;
        }

        @NotNull
        @Override
        public ViewAssignment.FeedbackUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.feedbackuser_layout, parent, false);
            return new ViewAssignment.FeedbackUserAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(ViewAssignment.FeedbackUserAdapter.ViewHolder holder, final int position) {
            holder.tvUserName.setText(list.get(position).getName());
            holder.tvUserName.setTextColor(context.getResources().getColor(R.color.white));
            if(list.get(position).getFeedbackstatus().equals("1")){
                holder.btnFeedback.setBackground(context.getResources().getDrawable(R.drawable.greybtn));
                holder.btnFeedback.setEnabled(false);
            }
            else {
                holder.btnFeedback.setBackground(context.getResources().getDrawable(R.drawable.bg2));
                holder.btnFeedback.setEnabled(true);
            }
            holder.btnFeedback.setOnClickListener(v -> {
                context.startActivity(new Intent(context,UserfeedbackActivity.class)
                        .putExtra("projectid",projectid)
                        .putExtra("userid",list.get(position).getUser_id())
                        );
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {

            TextView tvUserName,btnFeedback;
            public ViewHolder(View itemView) {
                super(itemView);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                btnFeedback = itemView.findViewById(R.id.btnFeedback);


            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userlistsforfeedback(projectid);
    }
}