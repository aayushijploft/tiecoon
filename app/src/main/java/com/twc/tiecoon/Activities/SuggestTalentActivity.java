package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.HelpUserDetails;
import com.twc.tiecoon.network.response.SuggestTalent;
import com.twc.tiecoon.utils.Utils;

import java.util.Observable;

public class SuggestTalentActivity extends AbstractFragmentActivity {
    EditText et_name,editText;
    RelativeLayout relativeParent;
    private ProfileModel profileModel = new ProfileModel();


    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_talent);
        et_name = findViewById(R.id.et_name);
        editText = findViewById(R.id.editText);
        relativeParent = findViewById(R.id.relativeParent);
        findViewById(R.id.iv_back).setOnClickListener(View-> onBackPressed());
        findViewById(R.id.btnSave).setOnClickListener(view -> CheckAssignmentValidation());
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }
    private void CheckAssignmentValidation() {
        if (et_name.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Name can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (editText.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Description can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        SubmitSupport();
    }
    private void SubmitSupport() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.suggestTalent(this, relativeParent, et_name.getText().toString().trim(),editText.getText().toString().trim());

        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }
    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof SuggestTalent) {
            Toast.makeText(this,"Submitted successfully",Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }
}