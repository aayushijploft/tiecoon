package com.twc.tiecoon.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.Adapters.MyProfileBrandsHorizontalAdapters;
import com.twc.tiecoon.Adapters.MyProfileProductAdapters;
import com.twc.tiecoon.Adapters.MyProfileServiceAdapters;
import com.twc.tiecoon.Adapters.MyProfileTalentAdapters;
import com.twc.tiecoon.Adapters.MyprofileOngoingAssignmentAdapters;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.Fragments.MyProfileReorganisationFragment;
import com.twc.tiecoon.Fragments.MyProfileTestimonialFragment;
import com.twc.tiecoon.Fragments.MyProfilefeedbackFragment;
import com.twc.tiecoon.Fragments.SuccessfulCollaborationFragment;
import com.twc.tiecoon.Fragments.SuccessfulConnectionFragment;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.AddProductCategoryData;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.network.response.HomeUserDetail;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.HomeUserDetailsFeedback;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsReOrganisation;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.HomeUserDetailsTestimonial;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.HomeUserDetailsUserDetils;
import com.twc.tiecoon.network.response.HomeUserPreferedLanguage;
import com.twc.tiecoon.network.response.ProductCategory;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.utils.FragmentTestimonialListListner;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;
import com.twc.tiecoon.utils.Utils;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;

public class MyProfileActivity extends AbstractFragmentActivity implements View.OnClickListener {
    private HomeModel homeModel = new HomeModel();
    private LinearLayout linearParent, linearScreen, linearSpinnerCategory;
    private ImageView ivBack, imgShare, imgProfile, imgCamera;
    public static final int UPLOAD_TALENT = 101;
    public static final int UPLOAD_PRODUCTS = 102;
    public static final int UPLOAD_SERVICES = 103;
    public static final int UPLOAD_BRANDS = 104;
    private static final int CREATEASSGN = 105;
    public static final int UPLOAD_PROFILE = 106;
    private static final int EDITINFO = 107;
    private static final int UPDATEIMAGE = 108;
    private TextView tvName, tv_rating, tvMyProfileDes, tvTalentsTotal, tvProductAndServicesTotal, linear_add_assignments;
    private RelativeLayout rlEditInfo, relativeImageProfile;
    private RecyclerView recyclerView_profile, recyclerView_add_talents, recyclerView_upload_product,
            recyclerView_upload_services, recyclerView_upload_brands, recyclerView_ongoing_assignments, recyclerView_completed_assignments;

    private LinearLayout linear_upload_brands, linearConnect, linearChat, linear3Profile, linearSpinnerTalents;
    private TabLayout tabLayout, tabLayout2;
    private ViewPager viewPager, viewPager2;

    private Fragment successfulCollaborationFragment, successfulConnectionFragment, testimonialFragment, feedbackFragment,
            reorganisationFragment;
    private List<List<HomeUserDetailsUploadTalent>> listTalent = new ArrayList<>();
    private List<List<HomeUserDetailsProduct>> uploadProductList = new ArrayList<>();
    private List<ProductCategory.Data> uploadProductList1 = new ArrayList<>();
    private List<List<HomeUserDetailsService>> uploadServiceList = new ArrayList<>();
    private List<ProfileCreateAssignment> homeUserDetailOngoingAssignmentList = new ArrayList<>();
    private List<AddBrandsData> homeUserDetailBrandList = new ArrayList<>();
    private MyProfileProductAdapters myProfileProductAdapters;
    private MyProfileServiceAdapters myProfileServiceAdapters;
    private MyProfileTalentAdapters myprofileTalentAdapters;
    private MyProfileBrandsHorizontalAdapters myProfileBrandsHorizontalAdapters;
    private MyprofileOngoingAssignmentAdapters myprofileOngoingAssignmentAdapters;
    private FragmentTestimonialListListner fragmentTestimonialListListner;
    private ArrayList<HomeUserDetailsFeedback> homeUserDetailsFeedbackArrayList = new ArrayList<>();
    private ArrayList<HomeUserDetailsReOrganisation> homeUserDetailsReOrganisationArrayList = new ArrayList<>();
    private HomeUserDetailsUserDetils homeUserDetailsUserDetils;
    private String phone = "";
    ArrayList<HomeUserPreferedLanguage> preferedLanguageList = new ArrayList<>();
    private boolean updateProduct = false;
    private int updatePos = 0;
    private int updateSubPos = 0;
    private boolean updateService = false;
    private boolean updateTalent = false;
    private boolean updateBrand = false;
    private boolean updateAssign = false;
    CardView cardView;
    private int deleteAssignments;
    private boolean myprofilechanged = false;
    HomeUserDetailData homeUserDetailData;
    ImageView ivStatusDot;
    SwipeRefreshLayout swiperefresh;
    EditText et_notes;
    Button btnSubmit;
    TextView tvLikeCount;
    LinearLayout layoutLikeCount;

    private Spinner spinnerTalents;
    private final List<String> listTalentsSpinner = new ArrayList<>();
    private final List<CategoryData> listCategorySpinnerData = new ArrayList<>();

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_my_profile);
        initView();
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    private void initView() {
        linearParent = findViewById(R.id.linearParent);
        linearScreen = findViewById(R.id.linearScreen);
        ivBack = findViewById(R.id.ivBack);
        imgProfile = findViewById(R.id.imgProfile);
        imgShare = findViewById(R.id.imgShare);
        imgCamera = findViewById(R.id.imgCamera);
        ivStatusDot = findViewById(R.id.ivStatusDot);
        relativeImageProfile = findViewById(R.id.relativeImageProfile);
        tvName = findViewById(R.id.tvName);
        et_notes = findViewById(R.id.et_notes);
        tv_rating = findViewById(R.id.tv_rating);
        tvMyProfileDes = findViewById(R.id.tvMyProfileDes);
        rlEditInfo = findViewById(R.id.rlEditInfo);
        cardView = findViewById(R.id.cardView);
        swiperefresh = findViewById(R.id.swiperefresh);
        btnSubmit = findViewById(R.id.btnSubmit);
        tvLikeCount = findViewById(R.id.tvLikeCount);

        recyclerView_profile = findViewById(R.id.recyclerView_profile);
        linearConnect = findViewById(R.id.linearConnect);
        linearChat = findViewById(R.id.linearChat);
        linear3Profile = findViewById(R.id.linear3Profile);
        layoutLikeCount = findViewById(R.id.layoutLikeCount);
        linear_add_assignments = findViewById(R.id.linear_add_assignments);
        linearSpinnerTalents = findViewById(R.id.linearSpinnerTalents);

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        tvTalentsTotal = findViewById(R.id.tvTalentsTotal);
        spinnerTalents = findViewById(R.id.spinnerTalents);
        linearSpinnerCategory = findViewById(R.id.linearSpinnerCategory);
        tvProductAndServicesTotal = findViewById(R.id.tvProductAndServicesTotal);

        recyclerView_add_talents = findViewById(R.id.recyclerView_add_talents);
        recyclerView_upload_product = findViewById(R.id.recyclerView_upload_product);
        recyclerView_upload_services = findViewById(R.id.recyclerView_upload_services);
        linear_upload_brands = findViewById(R.id.linear_upload_brands);
        recyclerView_upload_brands = findViewById(R.id.recyclerView_upload_brands);

        recyclerView_ongoing_assignments = findViewById(R.id.recyclerView_ongoing_assignments);
        recyclerView_completed_assignments = findViewById(R.id.recyclerView_completed_assignments);

        imgProfile.setOnClickListener(view -> {
            String imageUrl = "";
            if (homeUserDetailData.getProfile() != null) {
                if (homeUserDetailData.getPath() != null) {
                    imageUrl = Constant.ImageURL + homeUserDetailData.getPath() + "/" + homeUserDetailData.getProfile();
                } else {
                    imageUrl = homeUserDetailData.getProfile();
                }
            }

            Intent intent = new Intent(this, ImageActivity.class);
            intent.putExtra("image", imageUrl);
            intent.putExtra("key", "show");
            startActivityForResult(intent, UPDATEIMAGE);
        });

        imgCamera.setOnClickListener(view -> {
            String imageUrl = "";
            if (homeUserDetailData.getProfile() != null) {
                if (homeUserDetailData.getPath() != null) {
                    imageUrl = Constant.ImageURL + homeUserDetailData.getPath() + "/" + homeUserDetailData.getProfile();
                } else {
                    imageUrl = homeUserDetailData.getProfile();
                }
            }

            Intent intent = new Intent(this, ImageActivity.class);
            intent.putExtra("image", imageUrl);
            intent.putExtra("key", "show");
            startActivityForResult(intent, UPDATEIMAGE);
        });

        imgCamera.setOnClickListener(view -> {
            String imageUrl = "";
            if (homeUserDetailData.getProfile() != null) {
                if (homeUserDetailData.getPath() != null) {
                    imageUrl = Constant.ImageURL + homeUserDetailData.getPath() + "/" + homeUserDetailData.getProfile();
                } else {
                    imageUrl = homeUserDetailData.getProfile();
                }
            }

            Intent intent = new Intent(this, ImageActivity.class);
            intent.putExtra("image", imageUrl);
            intent.putExtra("key", "show");
            startActivityForResult(intent, UPDATEIMAGE);
        });

        spinnerTalents.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) spinnerTalents.getSelectedView()).setTextColor(getResources().getColor(R.color.white));
                if (i != 0)  {
                    CategoryData categoryData = getCategoryData(listTalentsSpinner.get(i));
                    if (categoryData != null) {
                        if (listTalent.isEmpty()) {
                            HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = new HomeUserDetailsUploadTalent();
                            homeUserDetailsUploadTalent.setCategoryId(categoryData.getId());
                            AddProductCategoryData addProductCategoryData = new AddProductCategoryData(categoryData.getId(), categoryData.getTitle(), categoryData.getSlug(),
                                    categoryData.getParentId(), categoryData.getDescription(), categoryData.getStatus(), categoryData.getTresh(), categoryData.getCreatedAt(), categoryData.getUpdatedAt());
                            homeUserDetailsUploadTalent.setCategory(addProductCategoryData);
                            List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = new ArrayList<>();
                            homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                            listTalent.add(homeUserDetailsUploadTalentList);
                            Log.e("talentlist", listTalent.toString());
                            myprofileTalentAdapters.notifyDataSetChanged();
                        } else {
                            boolean present = checkCategoryAdded(categoryData);
                            if (!present) {
                                HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = new HomeUserDetailsUploadTalent();
                                homeUserDetailsUploadTalent.setCategoryId(categoryData.getId());
                                AddProductCategoryData addProductCategoryData = new AddProductCategoryData(categoryData.getId(), categoryData.getTitle(), categoryData.getSlug(),
                                        categoryData.getParentId(), categoryData.getDescription(), categoryData.getStatus(), categoryData.getTresh(), categoryData.getCreatedAt(), categoryData.getUpdatedAt());
                                homeUserDetailsUploadTalent.setCategory(addProductCategoryData);
                                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = new ArrayList<>();
                                homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                                listTalent.add(homeUserDetailsUploadTalentList);
                                Log.e("talentlist", listTalent.toString());
                                myprofileTalentAdapters.notifyDataSetChanged();
                            } else {
                                Toast.makeText(MyProfileActivity.this, "Talent already present", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        myprofileTalentAdapters = new MyProfileTalentAdapters(this, listTalent, (pos, subPos, type) -> {
            updatePos = pos;
            if (type.isEmpty()) {
                updateTalent = false;
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                Intent i = new Intent(MyProfileActivity.this, WatermarkActivity.class);
                i.putExtra("type", "talent");
                i.putExtra("categoryId", categoryId);
                i.putExtra("categoryName", homeUserDetailsUploadTalentList.get(0).getCategory().getTitle());
                startActivityForResult(i, UPLOAD_TALENT);
            } else if (type.equals("delete")) {
                updateSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                HomeUserDetailsUploadTalent homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateSubPos);
                deleteTalents(homeUserDetailsProduct);
            } else if (type.equals("deletetalent")) {
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                showDeleteDialog(categoryId);
            } else {
                updateTalent = true;
                updateSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                HomeUserDetailsUploadTalent homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateSubPos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                Intent i = new Intent(MyProfileActivity.this, WatermarkActivity.class);
                ApplicationClass.uploadImageListDataArrayList.clear();
                if (homeUserDetailsProduct.getUpload_image() != null && !(homeUserDetailsProduct.getUpload_image().isEmpty())) {
                    ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsProduct.getUpload_image());
                }
                i.putExtra("type", "talent");
                i.putExtra("updateItem", homeUserDetailsProduct);
                i.putExtra("categoryId", categoryId);
                startActivityForResult(i, UPLOAD_TALENT);
            }
        });
        recyclerView_add_talents.setAdapter(myprofileTalentAdapters);

        myProfileProductAdapters = new MyProfileProductAdapters(this, uploadProductList1, (pos, subPos, type) -> {
            updatePos = pos;
            if (type.isEmpty()) {
                updateProduct = false;
                Intent i = new Intent(MyProfileActivity.this, CreateProductServiceActivity.class);
                i.putExtra("type", "product");
                startActivityForResult(i, UPLOAD_PRODUCTS);
            } else if (type.equals("update")) {
                updateProduct = true;
                updateSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = uploadProductList.get(pos);
                HomeUserDetailsProduct homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateSubPos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                ApplicationClass.uploadImageListDataArrayList.clear();
                if (homeUserDetailsProduct.getUpload_image() != null && !(homeUserDetailsProduct.getUpload_image().isEmpty())) {
                    ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsProduct.getUpload_image());
                }
                Intent i = new Intent(MyProfileActivity.this, CreateProductServiceActivity.class);
                i.putExtra("type", "product");
                i.putExtra("updateItem", homeUserDetailsProduct);
                i.putExtra("categoryId", categoryId);
                startActivityForResult(i, UPLOAD_PRODUCTS);
            } else if (type.equals("delete")) {
                updateSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = uploadProductList.get(pos);
                HomeUserDetailsProduct homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateSubPos);
                deleteproduct(homeUserDetailsProduct);
            }
        });
        recyclerView_upload_product.setAdapter(myProfileProductAdapters);


        myProfileBrandsHorizontalAdapters = new MyProfileBrandsHorizontalAdapters(this, homeUserDetailBrandList, (pos, type) -> {
            updatePos = pos;
            if (type.equals("update")) {
                updateBrand = true;
                AddBrandsData homeUserDetailsProduct = homeUserDetailBrandList.get(updatePos);
                Intent i = new Intent(MyProfileActivity.this, UploadBrandsActivity.class);
                i.putExtra("updateItem", homeUserDetailsProduct);
                startActivityForResult(i, UPLOAD_BRANDS);
            } else {
                deleteBrands(homeUserDetailBrandList.get(pos));
            }
        });
        recyclerView_upload_brands.setAdapter(myProfileBrandsHorizontalAdapters);

        myprofileOngoingAssignmentAdapters = new MyprofileOngoingAssignmentAdapters(this, homeUserDetailOngoingAssignmentList, (pos, type) -> {
            deleteAssignments = pos;
            if (type.equals("update")) {
                updateAssign = true;
                Intent i = new Intent(MyProfileActivity.this, CreateAssignentNew.class);
                i.putExtra("updateItem", homeUserDetailOngoingAssignmentList.get(pos).getId()+"");
                startActivityForResult(i, CREATEASSGN);
            } else if (type.equals("view")) {
                Intent i = new Intent(MyProfileActivity.this, ViewAssignment.class);
                i.putExtra("project_id", homeUserDetailOngoingAssignmentList.get(pos).getId()+"");
                startActivity(i);
            } else {
                deleteAssignment(homeUserDetailOngoingAssignmentList.get(pos));
            }
        });

        recyclerView_ongoing_assignments.setAdapter(myprofileOngoingAssignmentAdapters);

        tabLayout2 = findViewById(R.id.tabLayout2);
        viewPager2 = findViewById(R.id.viewPager2);
        setupViewPager2(viewPager2);
        tabLayout2.setupWithViewPager(viewPager2);


        ivBack.setOnClickListener(this);
        relativeImageProfile.setOnClickListener(this);
        imgShare.setOnClickListener(this);
        rlEditInfo.setOnClickListener(this);
        linearConnect.setOnClickListener(this);
        linearChat.setOnClickListener(this);
        linear3Profile.setOnClickListener(this);
        linearSpinnerTalents.setOnClickListener(this);
        linear_upload_brands.setOnClickListener(this);
        linear_add_assignments.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        swiperefresh.setOnRefreshListener(() -> {
            listTalentsSpinner.clear();
            getUserDetails();
        });
        getUserDetails();
    }

    private boolean checkCategoryAdded(CategoryData categoryData) {
        for (List<HomeUserDetailsUploadTalent> list : listTalent) {
            if (list.get(0).getCategoryId().equals(categoryData.getId())) {
                return true;
            }
        }
        return false;
    }

    private void saveNote() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);

            homeModel.addNote(this, linearParent, et_notes.getText().toString().trim());

        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }

    }

    private CategoryData getCategoryData(String title) {
        for (CategoryData categoryData : listCategorySpinnerData) {
            if (categoryData.getTitle() != null && title.equals(categoryData.getTitle())) {
                return categoryData;
            }
        }
        return null;
    }

    private void deleteTalents(HomeUserDetailsUploadTalent homeUserDetailsUploadTalent) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.deleteTalents(this, linearParent, homeUserDetailsUploadTalent.getId());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void deleteService(HomeUserDetailsService homeUserDetailsProduct) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.deleteServices(this, linearParent, homeUserDetailsProduct.getId());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void deleteproduct(HomeUserDetailsProduct homeUserDetailsProduct) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.deleteProduct(this, linearParent, homeUserDetailsProduct.getId());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void deleteAssignment(ProfileCreateAssignment profileCreateAssignment) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.deleteAssignment(this, linearScreen, profileCreateAssignment.getId());
        } else {
            Utils.warningSnackBar(this, linearScreen, getString(R.string.no_internet));
        }
    }

    private void deleteBrands(AddBrandsData addBrandsData) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.deleteBrands(this, linearScreen, addBrandsData.getId());
        } else {
            Utils.warningSnackBar(this, linearScreen, getString(R.string.no_internet));
        }
    }

    private void showDeleteDialog(int categoryId) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_popup);

        Button button_Cancel = dialog.findViewById(R.id.button_Cancel);
        Button button_logout = dialog.findViewById(R.id.button_logout);

        button_logout.setOnClickListener(view -> {
            deleteTalent(categoryId);
            dialog.dismiss();
            listTalentsSpinner.clear();

        });

        button_Cancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.relativeImageProfile:
//                Intent intentProfile = new Intent(this, ProfileFirst_Activity.class);
//                startActivityForResult(intentProfile, UPLOAD_PROFILE);
                break;
            case R.id.btnSubmit:
                saveNote();
                break;
            case R.id.imgShare:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Tiecoon");
                    String message = "https://play.google.com/store/apps/details?id=" + "com.twc.tiecoon" + "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, message);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.rlEditInfo:
                Intent intent2 = new Intent(this, EditInfoActivity.class);
                intent2.putExtra("data", homeUserDetailsUserDetils);
                intent2.putExtra("phone", phone);
                intent2.putParcelableArrayListExtra("lang", preferedLanguageList);
                startActivityForResult(intent2, EDITINFO);
                break;
            case R.id.linearConnect:
                break;

            case R.id.linearChat:
                break;

            case R.id.linear3Profile:
                break;

            case R.id.linearSpinnerTalents:
                if (listTalentsSpinner.isEmpty()) {
                    if (Utils.isDeviceOnline(this)) {
                        Utils.showProDialog(this);
                        homeModel.getCategoryCurrency(this, linearParent);
                    } else {
                        Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
                    }
                }
                break;
            case R.id.linear_upload_brands:
                updateBrand = false;
                Intent i = new Intent(this, UploadBrandsActivity.class);
                i.putExtra("type", "brands");
                startActivityForResult(i, UPLOAD_BRANDS);
                break;
            case R.id.linear_add_assignments:
                Intent intent1 = new Intent(this, CreateAssignentNew.class);
                startActivityForResult(intent1, CREATEASSGN);
                break;
        }
    }

    private void getUserDetails() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getMyProfileDetails(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }
        private void deleteTalent(int category_id) {
            if (Utils.isDeviceOnline(this)) {
                Utils.showProDialog(this);
                homeModel.deletTalent(this, linearParent,category_id);
            } else {
                Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
            }
        }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        successfulConnectionFragment = new SuccessfulConnectionFragment();
        successfulCollaborationFragment = new SuccessfulCollaborationFragment();
        adapter.addFragment(successfulConnectionFragment, "Successful Connection");
        adapter.addFragment(successfulCollaborationFragment, "Successful & Collaboration");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void update(Observable observable, Object o) {
        swiperefresh.setRefreshing(false);
        if (o instanceof HomeUserDetail) {
            HomeUserDetail homeUserDetail = (HomeUserDetail) o;
            homeUserDetailData = homeUserDetail.getData();
            if (homeUserDetailData != null) {
                linearScreen.setVisibility(View.VISIBLE);
                if (homeUserDetailData.getPhone() != null) {
                    phone = homeUserDetailData.getPhone();
                }
                if (homeUserDetailData.getEmail() != null) {
                    ApplicationClass.appPreferences.setEmailId(homeUserDetailData.getEmail());
                }
                if (homeUserDetailData.getNotes() != null) {
                    et_notes.setText(homeUserDetailData.getNotes());
                }
                if (homeUserDetailData.getSave_profile() != null) {
                    tvLikeCount.setText(homeUserDetailData.getSave_profile());
                } else if (homeUserDetailData.getSave_profile().equals("0")) {
                    layoutLikeCount.setVisibility(View.GONE);
                }

//                Toast.makeText(this, homeUserDetailData.getUser_status(), Toast.LENGTH_SHORT).show();
                switch (homeUserDetailData
                        .getUser_status()) {
                    case "Online":
                        ivStatusDot.setColorFilter(ContextCompat.getColor(this, R.color.green));
                        break;
                    case "Away":
                        ivStatusDot.setColorFilter(ContextCompat.getColor(this, R.color.yellow));
                        break;
                    case "Inactive":
                        ivStatusDot.setColorFilter(ContextCompat.getColor(this, R.color.white));
                        break;
                }
                homeUserDetailsUserDetils = homeUserDetailData.getHomeUserDetailsUserDetils();
                if (homeUserDetailsUserDetils != null) {
                    if (homeUserDetailsUserDetils.getName() != null) {
                        String des = homeUserDetailsUserDetils.getName().substring(0, 1).toUpperCase() + homeUserDetailsUserDetils.getName().substring(1);
                        tvName.setText(des);
                        ApplicationClass.appPreferences.setUserName(des);
                    }
                    if (homeUserDetailsUserDetils.getBio() != null) {
                        String des = homeUserDetailsUserDetils.getBio().substring(0, 1).toUpperCase() + homeUserDetailsUserDetils.getBio().substring(1);
                        tvMyProfileDes.setText(des);
                    } else {
                        tvMyProfileDes.setVisibility(View.GONE);
                    }
                } else {
                    tvName.setVisibility(View.GONE);
                    tvMyProfileDes.setVisibility(View.GONE);
                }
                if (homeUserDetailData.getPreferredLanguage() != null && !(homeUserDetailData.getPreferredLanguage().isEmpty())) {
                    preferedLanguageList.clear();
                    preferedLanguageList.addAll(homeUserDetailData.getPreferredLanguage());
                }

                if (homeUserDetailData.getProfile() != null) {
                    if (homeUserDetailData.getPath() != null) {
                        imgCamera.setVisibility(View.GONE);
                        String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailData.getPath()).append("/").append(homeUserDetailData.getProfile())).toString();
                        Glide.with(this).load(imageURL).error(R.drawable.dummy_place)
                                .into(imgProfile);
                        ApplicationClass.appPreferences.setUserPicUrl(imageURL);
                    } else {
                        imgCamera.setVisibility(View.VISIBLE);
                        Glide.with(this).load(homeUserDetailData.getProfile()).error(R.drawable.dummy_place)
                                .into(imgProfile);
                        ApplicationClass.appPreferences.setUserPicUrl(homeUserDetailData.getProfile());
                    }
                }
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents1 = homeUserDetailData.getHomeUserDetailsUploadTalent();
                listTalent.clear();
                if (homeUserDetailsUploadTalents1 != null && !(homeUserDetailsUploadTalents1.isEmpty())) {
                    listTalent.clear();
                    setUploadTalentData(homeUserDetailsUploadTalents1);
                }
                List<HomeUserDetailsProduct> homeUserDetailsProductList1 = homeUserDetailData.getHomeUserDetailsProducts();
                if (homeUserDetailsProductList1 != null && !(homeUserDetailsProductList1.isEmpty())) {
                    uploadProductList.clear();
                    setProducttData(homeUserDetailsProductList1);
                }
                List<HomeUserDetailsService> homeUserDetailsServiceList1 = homeUserDetailData.getHomeUserDetailsServices();
                if (homeUserDetailsServiceList1 != null && !(homeUserDetailsServiceList1.isEmpty())) {
                    uploadServiceList.clear();
                    setServiceData(homeUserDetailsServiceList1);
                }
                List<AddBrandsData> addBrandsDataList = homeUserDetailData.getHomeUserDetailBrand();
                if (addBrandsDataList != null && !(addBrandsDataList.isEmpty())) {
                    cardView.setVisibility(View.VISIBLE);
                    homeUserDetailBrandList.clear();
                    homeUserDetailBrandList.addAll(addBrandsDataList);
                    myProfileBrandsHorizontalAdapters.notifyDataSetChanged();
                }
                List<HomeUserDetailsTestimonial> homeUserDetailsTestimonialList = homeUserDetailData.getHomeUserDetailsTestimonial();
                if (homeUserDetailsTestimonialList != null && !(homeUserDetailsTestimonialList.isEmpty())) {
                    fragmentTestimonialListListner.setList(homeUserDetailsTestimonialList);
                }

                if (homeUserDetailData.getHomeUserDetailsFeedback() != null && !(homeUserDetailData.getHomeUserDetailsFeedback().isEmpty())) {
                    homeUserDetailsFeedbackArrayList.clear();
                    homeUserDetailsFeedbackArrayList.addAll(homeUserDetailData.getHomeUserDetailsFeedback());
                }

                if (homeUserDetailData.getHomeUserDetailsReOrganisation() != null && !(homeUserDetailData.getHomeUserDetailsReOrganisation().isEmpty())) {
                    homeUserDetailsReOrganisationArrayList.clear();
                    homeUserDetailsReOrganisationArrayList.addAll(homeUserDetailData.getHomeUserDetailsReOrganisation());
                }
                if (homeUserDetailData.getHomeUserDetailCreateAssignments() != null && !(homeUserDetailData.getHomeUserDetailCreateAssignments().isEmpty())) {
                    homeUserDetailOngoingAssignmentList.clear();
                    homeUserDetailOngoingAssignmentList.addAll(homeUserDetailData.getHomeUserDetailCreateAssignments());
                    myprofileOngoingAssignmentAdapters.notifyDataSetChanged();
                }
            }
            if (Utils.isDeviceOnline(this)) {
                Utils.showProDialog(this);
                homeModel.getCategoryCurrency(this, linearParent);
            } else {
                Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
            }
        } else if (o instanceof String) {
            String data = (String) o;
            switch (data) {
                case "assignmentDeleted":
                    homeUserDetailOngoingAssignmentList.remove(deleteAssignments);
                    myprofileOngoingAssignmentAdapters.notifyItemRemoved(deleteAssignments);
                    break;
                case "deleteBrands":
                    homeUserDetailBrandList.remove(updatePos);
                    myProfileBrandsHorizontalAdapters.notifyItemRemoved(updatePos);
                    break;
                case "deleted":
                    finish();
                    startActivity(getIntent());
                    break;
                case "Notesadded":
                    Toast.makeText(this, "Notes added successfully", Toast.LENGTH_SHORT).show();
                    break;
                case "deleteProduct":
                    List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = uploadProductList.get(updatePos);
                    homeUserDetailsUploadTalentList.remove(updateSubPos);
                    uploadProductList.set(updatePos, homeUserDetailsUploadTalentList);
                    myProfileProductAdapters.notifyItemRemoved(updatePos);
                    break;
                case "deleteServices":
                    List<HomeUserDetailsService> homeUserDetailsServiceList = uploadServiceList.get(updatePos);
                    homeUserDetailsServiceList.remove(updateSubPos);
                    uploadServiceList.set(updatePos, homeUserDetailsServiceList);
                    myProfileServiceAdapters.notifyItemRemoved(updatePos);
                    break;
            }
        } else if (o instanceof CategoryCurrency) {
            setCategoryCurrency((CategoryCurrency) o);
        } else if (o instanceof DeleteTalents) {
            List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(updatePos);
            homeUserDetailsUploadTalentList.remove(updateSubPos);
            listTalent.set(updatePos, homeUserDetailsUploadTalentList);
            myprofileTalentAdapters.notifyItemRemoved(updatePos);
            finish();
            startActivity(getIntent());
        }

    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1", "size:" + categoryDataList1.size() + ",  " + categoryDataList1.toString());
        if (categoryDataList1 != null && !(categoryDataList1.isEmpty())) {
            for (CategoryData categoryData : categoryDataList1) {
                if (categoryData.getTitle() != null) {
                    listCategorySpinnerData.add(categoryData);
                    listTalentsSpinner.add(categoryData.getTitle());
                }
            }
            listTalentsSpinner.add(0, "Select Talents");
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, listTalentsSpinner);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            spinnerTalents.setAdapter(arrayAdapter);
        }
    }

    private void setUploadTalentData(List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents1) {
        List<HomeUserDetailsUploadTalent> listTalentNew = new ArrayList<>(homeUserDetailsUploadTalents1);
        Iterator<HomeUserDetailsUploadTalent> iteratormain = homeUserDetailsUploadTalents1.iterator();
        while (iteratormain.hasNext()) {
            HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = (HomeUserDetailsUploadTalent) iteratormain.next();
            List<HomeUserDetailsUploadTalent> newlist = new ArrayList<>();
            Iterator<HomeUserDetailsUploadTalent> iteratorsub = listTalentNew.iterator();
            if (iteratorsub.hasNext()) {
                while (iteratorsub.hasNext()) {
                    HomeUserDetailsUploadTalent homeUserDetailsUploadTalent1 = (HomeUserDetailsUploadTalent) iteratorsub.next();
                    if (homeUserDetailsUploadTalent1.getCategory() != null) {
                        if ((homeUserDetailsUploadTalent.getCategoryId() == homeUserDetailsUploadTalent1.getCategoryId())) {
                            newlist.add(homeUserDetailsUploadTalent1);
                            iteratorsub.remove();
                        }
                    }
                }
            } else {
                break;
            }
            if (!(newlist.isEmpty())) {
                iteratormain.remove();
                listTalent.add(newlist);
            }
        }
        int talentTotal = listTalent.size();
        if (talentTotal > 0) {
            tvTalentsTotal.setText("Talent" + "(" + talentTotal + ")");
        }
        myprofileTalentAdapters.notifyDataSetChanged();
    }

    private void setProducttData(List<HomeUserDetailsProduct> homeUserDetailsProductList) {
        List<HomeUserDetailsProduct> listTalentNew = new ArrayList<>(homeUserDetailsProductList);
        Iterator<HomeUserDetailsProduct> iteratormain = homeUserDetailsProductList.iterator();
        while (iteratormain.hasNext()) {
            HomeUserDetailsProduct homeUserDetailsUploadTalent = (HomeUserDetailsProduct) iteratormain.next();
            List<HomeUserDetailsProduct> newlist = new ArrayList<>();
            Iterator<HomeUserDetailsProduct> iteratorsub = listTalentNew.iterator();
            if (iteratorsub.hasNext()) {
                while (iteratorsub.hasNext()) {
                    HomeUserDetailsProduct homeUserDetailsUploadTalent1 = (HomeUserDetailsProduct) iteratorsub.next();
                    if (homeUserDetailsUploadTalent1.getCategory() != null) {
                        if ((homeUserDetailsUploadTalent.getCategoryId().equals(homeUserDetailsUploadTalent1.getCategoryId()))) {
                            newlist.add(homeUserDetailsUploadTalent1);
                            iteratorsub.remove();
                        }
                    }
                }
            } else {
                break;
            }
            if (!(newlist.isEmpty())) {
                iteratormain.remove();
                uploadProductList.add(newlist);
            }
        }
        myProfileProductAdapters.notifyDataSetChanged();
    }

    private void setServiceData(List<HomeUserDetailsService> homeUserDetailsProductList) {
        List<HomeUserDetailsService> listTalentNew = new ArrayList<>(homeUserDetailsProductList);
        Iterator<HomeUserDetailsService> iteratormain = homeUserDetailsProductList.iterator();
        while (iteratormain.hasNext()) {
            HomeUserDetailsService homeUserDetailsUploadTalent = (HomeUserDetailsService) iteratormain.next();
            List<HomeUserDetailsService> newlist = new ArrayList<>();
            Iterator<HomeUserDetailsService> iteratorsub = listTalentNew.iterator();
            if (iteratorsub.hasNext()) {
                while (iteratorsub.hasNext()) {
                    HomeUserDetailsService homeUserDetailsUploadTalent1 = (HomeUserDetailsService) iteratorsub.next();
                    if (homeUserDetailsUploadTalent1.getCategory() != null) {
                        if ((homeUserDetailsUploadTalent.getCategoryId() == homeUserDetailsUploadTalent1.getCategoryId())) {
                            newlist.add(homeUserDetailsUploadTalent1);
                            iteratorsub.remove();
                        }
                    }
                }
            } else {
                break;
            }
            if (!(newlist.isEmpty())) {
                iteratormain.remove();
                uploadServiceList.add(newlist);
            }
        }
        int productAndServiceTotal = uploadProductList.size() + uploadServiceList.size();
        if (productAndServiceTotal > 0) {
            tvProductAndServicesTotal.setText("Product & Services" + "(" + productAndServiceTotal + ")");
        }
        myProfileServiceAdapters.notifyDataSetChanged();
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setupViewPager2(ViewPager viewPager) {
        ViewPagerAdapter2 adapter = new ViewPagerAdapter2(getSupportFragmentManager());
        testimonialFragment = new MyProfileTestimonialFragment();
        fragmentTestimonialListListner = (FragmentTestimonialListListner) testimonialFragment;
        feedbackFragment = new MyProfilefeedbackFragment();
        reorganisationFragment = new MyProfileReorganisationFragment();

        Bundle bundleFeeback = new Bundle();
        bundleFeeback.putParcelableArrayList("feedback", homeUserDetailsFeedbackArrayList);
        feedbackFragment.setArguments(bundleFeeback);

        Bundle bundleReorganisation = new Bundle();
        bundleReorganisation.putParcelableArrayList("reorganisation", homeUserDetailsReOrganisationArrayList);
        reorganisationFragment.setArguments(bundleReorganisation);

        adapter.addFragment(testimonialFragment, "Testimonial");
        adapter.addFragment(feedbackFragment, "Feedback");
        adapter.addFragment(reorganisationFragment, "Reorganisation");
        viewPager.setAdapter(adapter);
    }

    static class ViewPagerAdapter2 extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter2(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("value",requestCode+"12345678");
        switch (requestCode) {




            case UPLOAD_TALENT:
                if (resultCode == RESULT_OK) {
                    HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = (HomeUserDetailsUploadTalent) data.getParcelableExtra("UploadData");
                    List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(updatePos);
                    if (updateTalent) {
//                        homeUserDetailsUploadTalentList.set(updateSubPos, homeUserDetailsUploadTalent);
                        listTalentsSpinner.clear();
                        finish();
                        startActivity(getIntent());
                        Log.e("value","edit");
                    } else {
                        listTalentsSpinner.clear();
                        finish();
                        startActivity(getIntent());
                        Log.e("value","add");
//                        homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                    }
                    listTalent.set(updatePos, homeUserDetailsUploadTalentList);
                    myprofileTalentAdapters.notifyItemChanged(updatePos);
                }
                break;
            case UPLOAD_PRODUCTS:
                if (resultCode == RESULT_OK) {
                    HomeUserDetailsProduct homeUserDetailsUploadTalent =  data.getParcelableExtra("UploadData");
                    List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = uploadProductList.get(updatePos);
                    if (updateProduct) {
                        homeUserDetailsUploadTalentList.set(updateSubPos, homeUserDetailsUploadTalent);
                    } else {
                        homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                    }
                    uploadProductList.set(updatePos, homeUserDetailsUploadTalentList);
                    myProfileProductAdapters.notifyItemChanged(updatePos);
                }
                break;
            case UPLOAD_SERVICES:
                if (resultCode == RESULT_OK) {
                    HomeUserDetailsService homeUserDetailsService = (HomeUserDetailsService) data.getParcelableExtra("UploadData");
                    List<HomeUserDetailsService> homeUserDetailsServiceList = uploadServiceList.get(updatePos);
                    if (updateService) {
                        homeUserDetailsServiceList.set(updateSubPos, homeUserDetailsService);
                    } else {
                        homeUserDetailsServiceList.add(homeUserDetailsService);
                    }
                    uploadServiceList.set(updatePos, homeUserDetailsServiceList);
                    myProfileServiceAdapters.notifyItemChanged(updatePos);
                }
                break;
            case UPLOAD_BRANDS:
                if (resultCode == RESULT_OK) {
                    AddBrandsData addBrandsData = data.getParcelableExtra("UploadData");
                    if (updateBrand) {
                        homeUserDetailBrandList.set(updatePos, addBrandsData);
                        myProfileBrandsHorizontalAdapters.notifyItemChanged(updatePos);
                    } else {
                        homeUserDetailBrandList.add(addBrandsData);
                        myProfileBrandsHorizontalAdapters.notifyDataSetChanged();
                    }
                }
                break;
            case CREATEASSGN:
                if (resultCode == RESULT_OK) {
                    ProfileCreateAssignment profileCreateAssignment = (ProfileCreateAssignment) data.getParcelableExtra("createAssign");
                    if (updateAssign) {
                        listTalentsSpinner.clear();
                        getUserDetails();
                        homeUserDetailOngoingAssignmentList.set(deleteAssignments, profileCreateAssignment);
                        myprofileOngoingAssignmentAdapters.notifyItemChanged(deleteAssignments);

                    } else {
                        listTalentsSpinner.clear();
                        getUserDetails();
                        homeUserDetailOngoingAssignmentList.add(profileCreateAssignment);
                        myprofileOngoingAssignmentAdapters.notifyDataSetChanged();
                    }
                }
                break;
            case UPLOAD_PROFILE:
                if (resultCode == RESULT_OK) {
                    myprofilechanged = true;
                    listTalentsSpinner.clear();
                    getUserDetails();
                }
                break;
            case UPDATEIMAGE:
                if (resultCode == RESULT_OK) {
                    myprofilechanged = true;
                    listTalentsSpinner.clear();
                    getUserDetails();
                }
                break;
            case EDITINFO:
                if (resultCode == RESULT_OK) {
                    listTalentsSpinner.clear();
                    getUserDetails();
                }
                break;
        }
    }

    private void showImageDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.imagedailog);

        ImageView image = dialog.findViewById(R.id.image);
        ImageView imgCancel = dialog.findViewById(R.id.imgCancel);
        if (homeUserDetailData.getProfile() != null) {
            if (homeUserDetailData.getPath() != null) {
                imgCamera.setVisibility(View.GONE);
                String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailData.getPath()).append("/").append(homeUserDetailData.getProfile())).toString();
                Glide.with(this).load(imageURL).error(R.drawable.dummy_place)
                        .into(image);
                ApplicationClass.appPreferences.setUserPicUrl(imageURL);
            } else {
                imgCamera.setVisibility(View.GONE);
                Glide.with(this).load(homeUserDetailData.getProfile()).error(R.drawable.dummy_place)
                        .into(image);
                ApplicationClass.appPreferences.setUserPicUrl(homeUserDetailData.getProfile());
            }
        }
        imgCancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (myprofilechanged) {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
        }
        super.onBackPressed();
    }

}