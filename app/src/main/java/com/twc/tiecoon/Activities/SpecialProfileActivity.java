package com.twc.tiecoon.Activities;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.PermissionChecker;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.twc.tiecoon.Adapters.SpecialProfileAdapters;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.OtherProfileUserDetail;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class SpecialProfileActivity extends AbstractFragmentActivity {
    RecyclerView recyclerView_SpecialProfile;
    private String[] AUDIO_CALLING_PERMISSIONS = {Manifest.permission.CALL_PHONE};
    private final int AUDIO_CALLING_PERMISSIONS_REQUEST = 402;
    private boolean audioCallPermissionDeniedWithDontAskMeAgain = false;
    private LinearLayout linearParent;
    private HomeModel homeModel = new HomeModel();
    private List<HomeUserDetailData> homeUserDetailDataList = new ArrayList<>();
    private SpecialProfileAdapters specialProfileAdapters;
    private int currentPage = 1;
    //for pagination
    private boolean noMore = false;
    private int pastVisibleItems, visibleItemCount, totalItemCount, previousTotal = 0;
    private int viewThreshold = 1;
    private String phone = "";

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_special_profile);
        linearParent = findViewById(R.id.linearParent);
        ImageView ivBack = (ImageView) findViewById(R.id.ivBack);
        recyclerView_SpecialProfile = findViewById(R.id.recyclerView_SpecialProfile);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView_SpecialProfile.setLayoutManager(linearLayoutManager);
        specialProfileAdapters = new SpecialProfileAdapters(this, homeUserDetailDataList, new ItemClickListener() {
            @Override
            public void itemClick(int pos) {
                phone = homeUserDetailDataList.get(pos).getPhone();
                if (phone != null && !(phone.isEmpty())) {
                    checkPermissionForAudioCall();
                }
            }
        });
        recyclerView_SpecialProfile.setAdapter(specialProfileAdapters);
        recyclerView_SpecialProfile.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                if (dy > 0) {
                    if (!noMore) {
                        if (totalItemCount > previousTotal) {
                            previousTotal = totalItemCount;
                        }
                        if ((totalItemCount - visibleItemCount) <= pastVisibleItems + viewThreshold) {
                            currentPage++;
                            getSpecialUserDetails();
                        }
                    }
                }
            }
        });
        ivBack.setOnClickListener(view -> onBackPressed());
        getSpecialUserDetails();
    }

    private void getSpecialUserDetails() {
        if (Utils.isDeviceOnline(this)) {
            if (currentPage == 1) {
                Utils.showProDialog(this);
            }
            homeModel.getSpecialUsersData(this, linearParent, currentPage);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof OtherProfileUserDetail) {
            OtherProfileUserDetail otherProfileUserDetail = (OtherProfileUserDetail) o;
            List<HomeUserDetailData> homeUserDetailDataList1 = otherProfileUserDetail.getData();
            if (homeUserDetailDataList1 != null && !(homeUserDetailDataList1.isEmpty())) {
                for (HomeUserDetailData homeUserDetailData : homeUserDetailDataList1) {
                    if (homeUserDetailData != null) {
                        if (homeUserDetailData.getProfile() != null && homeUserDetailData.getPath() != null) {
                            homeUserDetailDataList.add(homeUserDetailData);
                        }
                    }
                }
                specialProfileAdapters.notifyDataSetChanged();
            } else {
                noMore = true;
                if (homeUserDetailDataList.isEmpty()) {
                    Toast.makeText(this, "No special user found", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void checkPermissionForAudioCall() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, AUDIO_CALLING_PERMISSIONS))) {
                Utils.dismissProDialog();
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs call permission to make call.",
                            AUDIO_CALLING_PERMISSIONS, AUDIO_CALLING_PERMISSIONS_REQUEST);
                } else if (audioCallPermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs call permission to make call.",
                            AUDIO_CALLING_PERMISSIONS_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, AUDIO_CALLING_PERMISSIONS, AUDIO_CALLING_PERMISSIONS_REQUEST);
                }
            } else {
                makeAudioCall();
            }
        } else {
            makeAudioCall();
        }
    }

    private void makeAudioCall() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        if (ActivityCompat.checkSelfPermission(SpecialProfileActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case AUDIO_CALLING_PERMISSIONS_REQUEST:
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE)) {
                    Log.e("Permission: ", "User Has Denied Permission");
                } else if (PermissionChecker.checkCallingOrSelfPermission(this,
                        Manifest.permission.CALL_PHONE) != PermissionChecker.PERMISSION_GRANTED) {
                    Log.e("Permission: ", "User Has Denied Permission with Don't Ask Again");
                    audioCallPermissionDeniedWithDontAskMeAgain = true;
                } else {
                    makeAudioCall();
                }
                break;
        }
    }

}