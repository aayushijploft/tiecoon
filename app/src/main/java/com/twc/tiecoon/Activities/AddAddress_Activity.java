package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.AddAddressModel;
import com.twc.tiecoon.network.response.AddressModel;
import com.twc.tiecoon.network.response.UserFeedback;
import com.twc.tiecoon.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAddress_Activity extends AppCompatActivity {

    EditText et_name,et_name1,et_phone,et_phone1,et_pincode,et_pincode1,et_address,et_address1
            ,et_locality,et_locality1,et_city,et_city1,et_state,et_state1;
    CheckBox cbCheck;
    Button btnSave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address_);

        et_name = findViewById(R.id.et_name);
        et_name1 = findViewById(R.id.et_name1);
        et_phone = findViewById(R.id.et_phone);
        et_phone1 = findViewById(R.id.et_phone1);
        et_pincode = findViewById(R.id.et_pincode);
        et_pincode1 = findViewById(R.id.et_pincode1);
        et_address = findViewById(R.id.et_address);
        et_address1 = findViewById(R.id.et_address1);
        et_locality = findViewById(R.id.et_locality);
        et_locality1 = findViewById(R.id.et_locality1);
        et_city = findViewById(R.id.et_city);
        et_city1 = findViewById(R.id.et_city1);
        et_state = findViewById(R.id.et_state);
        et_state1 = findViewById(R.id.et_state1);
        cbCheck = findViewById(R.id.cbCheck);
        btnSave = findViewById(R.id.btnSave);
        getAddress();

        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());
        cbCheck.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if(cbCheck.isChecked()){
                String name = et_name.getText().toString();
                String phone = et_phone.getText().toString();
                String pincode = et_pincode.getText().toString();
                String address = et_address.getText().toString();
                String locality = et_locality.getText().toString();
                String city= et_city.getText().toString();
                String state= et_state.getText().toString();

                if(TextUtils.isEmpty(name)){
                    Toast.makeText(this, "Enter Delivery name", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(phone)){
                    Toast.makeText(this, "Enter Delivery phone", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!(Utils.isValidMobile(phone))){
                    Toast.makeText(this, "Enter Valid Phone number", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(pincode)){
                    Toast.makeText(this, "Enter Delivery pincode", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(address)){
                    Toast.makeText(this, "Enter Delivery address", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(locality)){
                    Toast.makeText(this, "Enter Delivery locality", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(city)){
                    Toast.makeText(this, "Enter Delivery city", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(state)){
                    Toast.makeText(this, "Enter Delivery state", Toast.LENGTH_SHORT).show();
                    return;
                }

                et_name1.setText(name);
                et_phone1.setText(phone);
                et_pincode1.setText(pincode);
                et_address1.setText(address);
                et_locality1.setText(locality);
                et_city1.setText(city);
                et_state1 .setText(city);
            }
            else {
                et_name1.setText("");
                et_phone1.setText("");
                et_pincode1.setText("");
                et_address1.setText("");
                et_locality1.setText("");
                et_city1.setText("");
                et_state1 .setText("");
            }
        });

        btnSave.setOnClickListener(v -> validate() );
    }


    public void validate(){
            String name = et_name.getText().toString();
            String name1 = et_name1.getText().toString();
            String phone = et_phone.getText().toString();
            String phone1 = et_phone1.getText().toString();
            String pincode = et_pincode.getText().toString();
            String pincode1 = et_pincode1.getText().toString();
            String address = et_address.getText().toString();
            String address1 = et_address1.getText().toString();
            String locality = et_locality.getText().toString();
            String locality1= et_locality1.getText().toString();
            String city= et_city.getText().toString();
            String city1= et_city1.getText().toString();
            String state= et_state.getText().toString();
            String state1= et_state1.getText().toString();

        if(TextUtils.isEmpty(name)){
            Toast.makeText(this, "Enter Delivery name", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(phone)){
            Toast.makeText(this, "Enter Delivery phone", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!(Utils.isValidMobile(phone))){
            Toast.makeText(this, "Enter valid Delivery phone", Toast.LENGTH_SHORT).show();
            return;
        }

        if(TextUtils.isEmpty(pincode)){
            Toast.makeText(this, "Enter Delivery pincode", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!(Utils.isValidPincode(pincode))){
            Toast.makeText(this, "Enter valid pincode", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(address)){
            Toast.makeText(this, "Enter Delivery address", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(locality)){
            Toast.makeText(this, "Enter Delivery locality", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(city)){
            Toast.makeText(this, "Enter Delivery city", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(state)){
            Toast.makeText(this, "Enter Delivery state", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(name1)){
            Toast.makeText(this, "Enter Billing name", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(phone1)){
            Toast.makeText(this, "Enter Billing phone", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!(Utils.isValidMobile(phone1))){
            Toast.makeText(this, "Enter valid Delivery phone", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(pincode1)){
            Toast.makeText(this, "Enter Billing pincode", Toast.LENGTH_SHORT).show();
            return;
        }
        if(!(Utils.isValidPincode(pincode1))){
            Toast.makeText(this, "Enter valid pincode", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(address1)){
            Toast.makeText(this, "Enter Billing address", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(locality1)){
            Toast.makeText(this, "Enter Billing locality", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(city1)){
            Toast.makeText(this, "Enter Billing city", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(state1)){
            Toast.makeText(this, "Enter Billing state", Toast.LENGTH_SHORT).show();
            return;
        }

        addAddress(name,phone,pincode,address,locality,city,state,name1,phone1,pincode1,address1,locality1,city1,state1);
    }


    public void addAddress(String name,String mobile,String pincode,String deliveryaddress
            ,String town ,String city,String state,String bname,String bmobile,String bpincode
            ,String bdeliveryaddress,String btown,String bcity,String bstate) {
        final Dialog dialog = new Dialog(AddAddress_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<AddAddressModel> call = apiInterface.addAddress("Bearer " + accessToken,
                name,mobile,pincode,deliveryaddress,town,city,state,bname,bmobile,bpincode,bdeliveryaddress,btown,bcity,bstate);

        call.enqueue(new Callback<AddAddressModel>() {
            @Override
            public void onResponse(Call<AddAddressModel> call, Response<AddAddressModel> response) {
                dialog.dismiss();
                AddAddressModel user = response.body();
                Log.e("user", user.toString());
                finish();
                Toast.makeText(AddAddress_Activity.this, user.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<AddAddressModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
    public void getAddress() {
        final Dialog dialog = new Dialog(AddAddress_Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<AddressModel> call = apiInterface.getAddress("Bearer " + accessToken);

        call.enqueue(new Callback<AddressModel>() {
            @Override
            public void onResponse(Call<AddressModel> call, Response<AddressModel> response) {
                dialog.dismiss();
                AddressModel user = response.body();
                Log.e("user", user.toString());
                et_name.setText(user.getData().getName());
                et_phone.setText(user.getData().getMobile());
                et_pincode.setText(user.getData().getPincode());
                et_address.setText(user.getData().getDeliveryaddress());
                et_locality.setText(user.getData().getTown());
                et_city.setText(user.getData().getCity());
                et_state.setText(user.getData().getState());
                et_name1.setText(user.getData().getBname());
                et_phone1.setText(user.getData().getBmobile());
                et_pincode1.setText(user.getData().getBpincode());
                et_address1.setText(user.getData().getBdeliveryaddress());
                et_locality1.setText(user.getData().getBtown());
                et_city1.setText(user.getData().getBcity());
                et_state1 .setText(user.getData().getBstate());
            }

            @Override
            public void onFailure(Call<AddressModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

}




































