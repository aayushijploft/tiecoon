package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.Feedback;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceFeedbackActivity extends AppCompatActivity {
    RatingBar rating_bar;
    EditText editText1;
    Button btnSummit;
    String service_id,userid,type = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_feedback);
        rating_bar = findViewById(R.id.rating_bar);
        editText1 = findViewById(R.id.editText1);
        btnSummit = findViewById(R.id.btnSummit);

        findViewById(R.id.iv_back).setOnClickListener(v -> onBackPressed());
        userid = getIntent().getStringExtra("userid");
        service_id = getIntent().getStringExtra("serviceid");
        type = getIntent().getStringExtra("type");

        btnSummit.setOnClickListener(v -> {
            String rating = String.valueOf(rating_bar.getRating());
            String message = editText1.getText().toString();

            if (TextUtils.isEmpty(rating)) {
                Toast.makeText(this, "Please give rating", Toast.LENGTH_SHORT).show();
                return;
            }

            confirm(rating,message);
        });
    }

    @SuppressLint("SetTextI18n")
    private void confirm(String rating, String message) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("This Feedback once submitted cannot be editted or deleted. Are sure you want to submit?");
        btnRemove.setText("SUBMIT");
        tvTitle.setVisibility(View.GONE);
//        tvTitle.setText("Collaboration Request");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            if(type.equals("user")){
                submitFeedback(rating,message);
            }
            else  serviceOwnerFeedback(rating,message);
        });
        btnCancle.setOnClickListener(v -> dialog.dismiss());

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    public void submitFeedback(String rating,String message) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<Feedback> call = apiInterface.serviceUserFeedback
                ("Bearer "+accessToken,userid,service_id,
                rating, message);

        call.enqueue(new Callback<Feedback>() {
            @Override
            public void onResponse(@NotNull Call<Feedback> call, @NotNull Response<Feedback> response) {
                dialog.dismiss();
                Feedback user = response.body();
                Log.e("user",user.toString());
                Toast.makeText(ServiceFeedbackActivity.this, user.getMessage(), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ServiceFeedbackActivity.this,UserHomeActivity.class));
            }

            @Override
            public void onFailure(@NotNull Call<Feedback> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });

    }
    public void serviceOwnerFeedback(String rating,String message) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<Feedback> call = apiInterface.serviceOwnerFeedback
                ("Bearer "+accessToken,userid,service_id,
                rating,message);

        call.enqueue(new Callback<Feedback>() {
            @Override
            public void onResponse(@NotNull Call<Feedback> call, @NotNull Response<Feedback> response) {
                dialog.dismiss();
                Feedback user = response.body();
                Log.e("user",user.toString());
                Toast.makeText(ServiceFeedbackActivity.this, user.getMessage(), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ServiceFeedbackActivity.this,UserHomeActivity.class));
            }

            @Override
            public void onFailure(@NotNull Call<Feedback> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });

    }
}