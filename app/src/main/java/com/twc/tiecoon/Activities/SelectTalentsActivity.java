package com.twc.tiecoon.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.twc.tiecoon.Adapters.TalentsAdapter;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

public class SelectTalentsActivity extends AppCompatActivity implements ItemClickListener {
    List<ProfileTalent> talents;
    private ImageView iv_back;
    private RecyclerView recycler_select_talents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_talents);
        talents=getIntent().getParcelableArrayListExtra("talentList");
        initView();
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        recycler_select_talents = findViewById(R.id.recycler_select_talents);
        recycler_select_talents.setHasFixedSize(true);
        recycler_select_talents.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        TalentsAdapter talentsAdapter = new TalentsAdapter(this,talents,this);
        recycler_select_talents.setAdapter(talentsAdapter);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    public void itemClick(int pos) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("talent",talents.get(pos));
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }
}