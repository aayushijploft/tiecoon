package com.twc.tiecoon.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Activities.views.RemoveUser;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.ProjectAccept;
import com.twc.tiecoon.network.response.ProjectUsers;
import com.twc.tiecoon.network.response.RefundList;
import com.twc.tiecoon.network.response.RefundRequest;
import com.twc.tiecoon.network.response.RemoveList;
import com.twc.tiecoon.network.response.RequestMoneyModel;
import com.twc.tiecoon.network.response.RequestedUser;
import com.twc.tiecoon.network.response.SaveUnsaveAds;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProjectInformationActivity extends AppCompatActivity {
    RecyclerView rvGroupUser;
    String group_id="",is_admin="",projectid="",group_name="",project_id="";
    ImageView iv_back;
    TextView tvnousers,tvEndProject;
    SwipeRefreshLayout swiperefresh;
    TextView tvAddProject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_information);

        group_id = getIntent().getStringExtra("group_id");
        is_admin = getIntent().getStringExtra("is_admin");
        group_name = getIntent().getStringExtra("group_name");
        project_id = getIntent().getStringExtra("project_id");


        rvGroupUser=findViewById(R.id.rvGroupUser);
        tvnousers=findViewById(R.id.tvnousers);
        tvEndProject=findViewById(R.id.tvEndProject);
        swiperefresh = findViewById(R.id.swiperefresh);
        iv_back=findViewById(R.id.iv_back);
        rvGroupUser.setHasFixedSize(true);
        rvGroupUser.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        iv_back.setOnClickListener(v -> onBackPressed());
        ProjectUserList(group_id,true);
        tvEndProject.setOnClickListener(v -> endProject(projectid));

        swiperefresh.setOnRefreshListener(() -> {
            if (Utils.isDeviceOnline(this)) {
                ProjectUserList(group_id,false);
            }
        });

        tvAddProject = findViewById(R.id.tvAddProject);
        tvAddProject.setOnClickListener(v -> showProjectDialog() );

        if(group_name.equals("")){
            tvAddProject.setVisibility(View.GONE);
        }

        if(is_admin.equals("false")){
            tvAddProject.setVisibility(View.GONE);
        }
    }


    RecyclerView rvProject;
    private void showProjectDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.project_popup);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(v -> dialog.dismiss());

        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);

        rvProject = dialog.findViewById(R.id.rvProject);
        rvProject.setHasFixedSize(true);
        rvProject.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));

        requestedUserList(dialog,tvConfirm);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void requestedUserList(Dialog dialog,TextView tvConfirm) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<RequestedUser> call = apiInterface.requestedUserList("Bearer "+accessToken,project_id);

        call.enqueue(new Callback<RequestedUser>() {
            @Override
            public void onResponse(Call<RequestedUser> call, Response<RequestedUser> response) {
                RequestedUser user = response.body();
                Log.e("user",user.toString());
                if(user.getData().size() > 0) {
                    tvConfirm.setVisibility(View.GONE);
                    rvProject.setVisibility(View.VISIBLE);
                    ProjectAdapter projectAdapter = new ProjectAdapter(ProjectInformationActivity.this, user.getData(), pos -> {
                        dialog.dismiss();
                        if(user.getData().get(pos).getPaid_status() == 1){
                            acceptRequest(projectid,user.getData().get(pos).getUser_id()+"",user.getData().get(pos).getName()+"");
                        }
                        else {
                            startActivity(new Intent(ProjectInformationActivity.this, AmountDepositActivity.class)
                                    .putExtra("user_id",user.getData().get(pos).getUser_id()+"")
                                    .putExtra("user_name",user.getData().get(pos).getName()+"")
                                    .putExtra("currency",user.getData().get(pos).getCurrency()+"")
                                    .putExtra("projectid",project_id));
                        }
                    });
                    rvProject.setAdapter(projectAdapter);
                }
                else {
                    tvConfirm.setVisibility(View.VISIBLE);
                    rvProject.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<RequestedUser> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void acceptRequest(String id,String user_id,String user_name) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProjectAccept> call = apiInterface.acceptProject("Bearer "+accessToken,id,user_id);

        call.enqueue(new Callback<ProjectAccept>() {
            @Override
            public void onResponse(Call<ProjectAccept> call, Response<ProjectAccept> response) {
                dialog.dismiss();
                ProjectAccept user = response.body();
                ArrayList<Integer> occupantIdsList = new ArrayList<Integer>();
                occupantIdsList.add(user.getData().getOccupants_id());

                QBChatDialog qbChatDialog = new QBChatDialog();
                qbChatDialog.setType(QBDialogType.PRIVATE);
                qbChatDialog.setOccupantsIds(occupantIdsList);

                // or just use DialogUtils
                //QBChatDialog dialog = DialogUtils.buildPrivateDialog(recipientId);

                QBRestChatService.createChatDialog(qbChatDialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog result, Bundle params) {
                        Log.e("result",result.toString());
                        String message = String.valueOf(Html.fromHtml(user.getMessage()));
                        ChatActivity.startForResult(ProjectInformationActivity.this,101, result,user_name,message);
                    }
                    @Override
                    public void onError(QBResponseException responseException) {
                        Toast.makeText(ProjectInformationActivity.this, "ERRROR", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onFailure(Call<ProjectAccept> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public static class ProjectAdapter extends RecyclerView.Adapter<ProjectAdapter.ViewHolder> {
        private Context context;
        private List<RequestedUser.Data> list;
        private ItemClickListener itemClickListener;

        public ProjectAdapter(Context context,List<RequestedUser.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public ProjectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.project_dropdown_list, parent, false);
            return new ProjectAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(ProjectAdapter.ViewHolder holder, final int position) {
            holder.tvReportName.setText(new StringBuilder().append(list.get(position).getName()));
            holder.tvReportName.setOnClickListener(v -> itemClickListener.itemClick(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            LinearLayout layoutReport;
            TextView tvReportName;
            public ViewHolder(View itemView) {
                super(itemView);
                layoutReport = itemView.findViewById(R.id.layoutReport);
                tvReportName = itemView.findViewById(R.id.tvReportName);
            }
        }
    }


    public void ProjectUserList(String group_id, boolean isshow) {
        final Dialog dialog = new Dialog(ProjectInformationActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if(isshow){
            dialog.show();
        }

        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProjectUsers> call = apiInterface.ProjectUserList("Bearer "+accessToken,group_id);

        call.enqueue(new Callback<ProjectUsers>() {
            @Override
            public void onResponse(Call<ProjectUsers> call, Response<ProjectUsers> response) {
                dialog.dismiss();
                swiperefresh.setRefreshing(false);
                ProjectUsers user = response.body();
                if(user.getData().size()>0){
                    tvnousers.setVisibility(View.GONE);
                    rvGroupUser.setVisibility(View.VISIBLE);
                    projectid = user.getProject_id();
                    ProjectUserAdapter projectUserAdapter = new ProjectUserAdapter(ProjectInformationActivity.this,user.getData(),(pos, type) ->  {
                        if(type.equals("release")){
                            showReleaseConfirmDialog(user.getData().get(pos).getProject_id()+"",user.getData().get(pos).getUser_id()+"",
                                    user.getData().get(pos).getProjectuser_id()+"",user.getData().get(pos).getAmount(),user.getData().get(pos).getCurrency() );
                        }
                        if(type.equals("refund")){
                            showRefundConfirmDialog(user.getData().get(pos).getProject_id()+"",user.getData().get(pos).getUser_id()+"");
                        }
                        if(type.equals("remove")){
                            showRemoveDialog(user.getData().get(pos).getProject_id()+"",user.getData().get(pos).getUser_id()+"");
                        }
                        if(type.equals("depositmore")){
                            showDepositMoreDialog(user.getData().get(pos).getProject_id()+"",user.getData().get(pos).getUser_id()+"",user.getData().get(pos).getCurrency()+"");
                        }
                        if(type.equals("requestmoney")){
                            showRequestMoneyDialog(user.getData().get(pos).getProject_id()+"",user.getData().get(pos).getUser_id()+"");
                        }
                    },is_admin);
                    rvGroupUser.setAdapter(projectUserAdapter);

                    if(user.getRelese().equals("no")){
                        tvEndProject.setVisibility(View.GONE);
                    }
                    else {
                        tvEndProject.setVisibility(View.VISIBLE);
                    }
                    if(user.getStatus().equals("1")){
                        tvEndProject.setEnabled(false);
                        tvEndProject.setText("Project Ended");
                        tvEndProject.setBackground(getResources().getDrawable(R.drawable.greybtn));
                    }
                }
                else {
                    tvnousers.setVisibility(View.VISIBLE);
                    rvGroupUser.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ProjectUsers> call, Throwable t) {
                dialog.dismiss();
                swiperefresh.setRefreshing(false);
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public class ProjectUserAdapter extends RecyclerView.Adapter<ProjectUserAdapter.ViewHolder> {
        private Context context;
        private List<ProjectUsers.Data> list;
        private ItemClickListenerExtraParam itemClickListener;
        String is_admin;

        public ProjectUserAdapter(Context context, List<ProjectUsers.Data> list, ItemClickListenerExtraParam itemClickListener,String is_admin) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
            this.is_admin = is_admin;
        }

        @Override
        public ProjectUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.projectuser_list, parent, false);
            return new ProjectUserAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(ProjectUserAdapter.ViewHolder holder, final int position) {
            holder.tvUserName.setText(list.get(position).getName());

            if(list.get(position).getAmount().equals("0")){
                holder.tvAmount.setVisibility(View.GONE);
            }
            else {
                holder.btnRemove.setBackground(context.getResources().getDrawable(R.drawable.greybtn));
                holder.btnRemove.setEnabled(false);
                holder.tvAmount.setVisibility(View.VISIBLE);
                holder.tvAmount.setText(list.get(position).getCurrency()+" "+list.get(position).getAmount());
            }
            if(list.get(position).getAdmin().equals("yes")){
                holder.layoutRequest.setVisibility(View.VISIBLE);
                holder.tvname.setText(list.get(position).getName());
                holder.tvcollab.setText("(Collab ID: "+list.get(position).getCollabId()+") ");
                holder.tvUserName.setVisibility(View.GONE);
            }
            if(list.get(position).getAdmin().equals("no")){
                holder.layoutRequest.setVisibility(View.GONE);
                holder.tvUserName.setVisibility(View.VISIBLE);
            }

            if(list.get(position).getRelesestatus().equals("0")){
                holder.btnRefund.setBackground(context.getResources().getDrawable(R.drawable.greybtn));
                holder.btnRefund.setEnabled(false);
            }

            if(is_admin.equals("false")){
                holder.btnRelease.setVisibility(View.GONE);
                holder.btnRefund.setVisibility(View.GONE);
                holder.btnRemove.setVisibility(View.GONE);
                holder.btnDepositmore.setVisibility(View.GONE);
                holder.layoutmore.setVisibility(View.GONE);
                tvEndProject.setVisibility(View.GONE);
            }

            if(list.get(position).getName().equals("You")){
                holder.btnRelease.setVisibility(View.GONE);
                holder.btnRefund.setVisibility(View.GONE);
                holder.btnRemove.setVisibility(View.GONE);
                holder.btnDepositmore.setVisibility(View.GONE);
                holder.layoutmore.setVisibility(View.GONE);
                holder.layoutRequest.setVisibility(View.GONE);
                holder.tvUserName.setVisibility(View.VISIBLE);
            }
            holder.btnRelease.setOnClickListener(v -> {
                if(list.get(position).getAmount().equals("0")){
                    Toast.makeText(context, "You have released complete amount", Toast.LENGTH_SHORT).show();
                }
                else itemClickListener.itemClick(position,"release");
            });
            holder.btnRefund.setOnClickListener(v -> itemClickListener.itemClick(position,"refund"));
            holder.btnRemove.setOnClickListener(v -> itemClickListener.itemClick(position,"remove"));
            holder.btnDepositmore.setOnClickListener(v -> itemClickListener.itemClick(position,"depositmore"));
            holder.btnRequestMoney.setOnClickListener(v -> {
                if(list.get(position).getRequestmoney().equals("1")){
                    Toast.makeText(context, "Already Request Sent", Toast.LENGTH_SHORT).show();
                }
                else itemClickListener.itemClick(position,"requestmoney");
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }
        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvUserName,btnRemove,btnRefund,tvAmount,tvname,tvcollab;
            RelativeLayout btnRelease,btnDepositmore,btnRequestMoney,layoutmore;
            LinearLayout layoutRequest;
            public ViewHolder(View itemView) {
                super(itemView);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                tvname = itemView.findViewById(R.id.tvname);
                btnRelease = itemView.findViewById(R.id.btnRelease);
                btnRefund = itemView.findViewById(R.id.btnRefund);
                btnRemove = itemView.findViewById(R.id.btnRemove);
                btnDepositmore = itemView.findViewById(R.id.btnDepositmore);
                tvAmount = itemView.findViewById(R.id.tvAmount);
                layoutmore = itemView.findViewById(R.id.layoutmore);
                layoutRequest = itemView.findViewById(R.id.layoutRequest);
                tvcollab = itemView.findViewById(R.id.tvcollab);
                btnRequestMoney = itemView.findViewById(R.id.btnRequestMoney);
            }
        }
    }

    RecyclerView rvProject1;
    private void showRefundDialog(String projectid,String userid) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.project_popup);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(v -> dialog.dismiss());
        rvProject1 = dialog.findViewById(R.id.rvProject);
        rvProject1.setHasFixedSize(true);
        rvProject1.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        getRefundReasons(dialog,projectid,userid);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    RecyclerView rvRemove;
    private void showRemoveListDialog(String projectid,String userid) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.project_popup);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(v -> dialog.dismiss());
        rvRemove = dialog.findViewById(R.id.rvProject);
        rvRemove.setHasFixedSize(true);
        rvRemove.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
        getRemoveReasons(dialog,projectid,userid);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showRemoveDialog(String Projectid,String userid) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);

        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            showRemoveListDialog(Projectid,userid);
        });

        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    private void showDepositMoreDialog(String Projectid,String userid,String currency) {

        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to add more Amount to this user ?");
        btnRemove.setText("Deposit More");

        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            startActivity(new Intent(ProjectInformationActivity.this, MoreDeposit_Activity.class)
                    .putExtra("user_id",userid)
                    .putExtra("currency",currency)
                    .putExtra("projectid",Projectid));
        });

        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showRefundConfirmDialog(String Projectid,String userid) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to initiate refund for this user?");
        btnRemove.setText("REFUND");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            showRefundDialog(Projectid,userid);
        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showRequestMoneyDialog(String projectid,String projectownerId) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);

        tvConfirm.setText("Are you sure, you want to send Request?");
        btnRemove.setText("SEND");

        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
           requestMoney(projectid,projectownerId);
        });

        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @SuppressLint("SetTextI18n")
    private void showReleaseConfirmDialog(String Projectid, String userid, String project_userid, String Amount, String Currency) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to release payment for this user?");
        btnRemove.setText("RELEASE");

        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            startActivity(new Intent(ProjectInformationActivity.this,ReleaseAmountActivity.class)
                    .putExtra("user_id",userid)
                    .putExtra("project_id",Projectid)
                    .putExtra("project_userid",project_userid)
                    .putExtra("Amount",Amount)
                    .putExtra("Currency",Currency)
            );
        });

        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @SuppressLint("SetTextI18n")
    private void endProject(String Projectid) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        tvConfirm.setText("Are you sure, you want to end this project?");
        btnRemove.setText("END");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            EndProject(Projectid);

        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public static class ProjectAdapter1 extends RecyclerView.Adapter<ProjectAdapter1.ViewHolder> {
        private Context context;
        private List<RefundList.Data> list;
        private ItemClickListener itemClickListener;

        public ProjectAdapter1(Context context,List<RefundList.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public ProjectAdapter1.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.project_dropdown_list, parent, false);
            return new ProjectAdapter1.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(ProjectAdapter1.ViewHolder holder, final int position) {
            holder.tvReportName.setText(new StringBuilder().append(list.get(position).getTitle()));
            holder.tvReportName.setOnClickListener(v -> itemClickListener.itemClick(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            LinearLayout layoutReport;
            TextView tvReportName;
            public ViewHolder(View itemView) {
                super(itemView);
                layoutReport = itemView.findViewById(R.id.layoutReport);
                tvReportName = itemView.findViewById(R.id.tvReportName);
            }
        }
    }

    public static class RemoveAdapter extends RecyclerView.Adapter<RemoveAdapter.ViewHolder> {
        private Context context;
        private List<RemoveList.Data> list;
        private ItemClickListener itemClickListener;

        public RemoveAdapter(Context context,List<RemoveList.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public RemoveAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.project_dropdown_list, parent, false);
            return new RemoveAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(RemoveAdapter.ViewHolder holder, final int position) {
            holder.tvReportName.setText(new StringBuilder().append(list.get(position).getName()));
            holder.tvReportName.setOnClickListener(v -> itemClickListener.itemClick(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            LinearLayout layoutReport;
            TextView tvReportName;
            public ViewHolder(View itemView) {
                super(itemView);
                layoutReport = itemView.findViewById(R.id.layoutReport);
                tvReportName = itemView.findViewById(R.id.tvReportName);
            }
        }
    }

    public void getRefundReasons(Dialog dialog,String projectid,String userid) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<RefundList> call = apiInterface.refundReasonList("Bearer "+accessToken);

        call.enqueue(new Callback<RefundList>() {
            @Override
            public void onResponse(Call<RefundList> call, Response<RefundList> response) {
                RefundList user = response.body();
                Log.e("user",user.toString());
                ProjectAdapter1 projectAdapter = new ProjectAdapter1(ProjectInformationActivity.this, user.getData(), pos -> {
                    dialog.dismiss();
                    refundRequest(user.getData().get(pos).getId()+"",projectid,userid);
                });
                rvProject1.setAdapter(projectAdapter);
            }

            @Override
            public void onFailure(Call<RefundList> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void getRemoveReasons(Dialog dialog,String projectid,String userid) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<RemoveList> call = apiInterface.removeReasonList("Bearer "+accessToken);

        call.enqueue(new Callback<RemoveList>() {
            @Override
            public void onResponse(Call<RemoveList> call, Response<RemoveList> response) {
                RemoveList user = response.body();
                Log.e("user",user.toString());
                RemoveAdapter removeAdapter = new RemoveAdapter(ProjectInformationActivity.this, user.getData(), pos -> {
                    dialog.dismiss();
                    removeUserFromGroup(projectid,userid,user.getData().get(pos).getId()+"");
                });
                rvRemove.setAdapter(removeAdapter);
            }

            @Override
            public void onFailure(Call<RemoveList> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void refundRequest(String refund_id,String project_id,String user_id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<RefundRequest> call = apiInterface.refundRequest("Bearer "+accessToken,refund_id,project_id,user_id);

        call.enqueue(new Callback<RefundRequest>() {
            @Override
            public void onResponse(Call<RefundRequest> call, Response<RefundRequest> response) {
                RefundRequest user = response.body();
                Log.e("user",user.toString());
                if(user.getSuccess()){
                    Toast.makeText(ProjectInformationActivity.this, user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<RefundRequest> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void removeUserFromGroup(String project_id,String user_id,String remove_id) {
        final Dialog dialog = new Dialog(ProjectInformationActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<RemoveUser> call = apiInterface.removeUserFromGroup("Bearer "+accessToken,project_id,user_id,remove_id);

        call.enqueue(new Callback<RemoveUser>() {
            @Override
            public void onResponse(Call<RemoveUser> call, Response<RemoveUser> response) {
                dialog.dismiss();
                RemoveUser user = response.body();
                Log.e("user",user.toString());
                Toast.makeText(ProjectInformationActivity.this, "User Removed from group.", Toast.LENGTH_SHORT).show();
//                startActivity(new Intent(ProjectInformationActivity.this,UserfeedbackActivity.class)
//                        .putExtra("projectid",projectid)
//                        .putExtra("userid",user_id)
//                );
                finish();
                startActivity(getIntent());
            }
            @Override
            public void onFailure(Call<RemoveUser> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void EndProject(String project_id) {
        final Dialog dialog = new Dialog(ProjectInformationActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SaveUnsaveAds> call = apiInterface.endProject("Bearer "+accessToken,project_id);
        call.enqueue(new Callback<SaveUnsaveAds>() {
            @Override
            public void onResponse(Call<SaveUnsaveAds> call, Response<SaveUnsaveAds> response) {
                dialog.dismiss();
                SaveUnsaveAds user = response.body();
                Log.e("user",user.toString());
                Toast.makeText(ProjectInformationActivity.this, "Your project has been ended.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(ProjectInformationActivity.this,UserfeedbackbyAdmin.class)
                        .putExtra("projectid",project_id));
            }
            @Override
            public void onFailure(Call<SaveUnsaveAds> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void requestMoney(String project_id,String projectownerId) {
        final Dialog dialog = new Dialog(ProjectInformationActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<RequestMoneyModel> call = apiInterface.requestMoney("Bearer "+accessToken,project_id,projectownerId);
        call.enqueue(new Callback<RequestMoneyModel>() {
            @Override
            public void onResponse(Call<RequestMoneyModel> call, Response<RequestMoneyModel> response) {
                dialog.dismiss();
                RequestMoneyModel user = response.body();
               // if(user.message.equals("Already Request sent.")){
                    Toast.makeText(ProjectInformationActivity.this, user.message, Toast.LENGTH_SHORT).show();
               // }
            }
            @Override
            public void onFailure(Call<RequestMoneyModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.isDeviceOnline(this)) {
            ProjectUserList(group_id,false);
        }
    }
}