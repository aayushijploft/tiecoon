package com.twc.tiecoon.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.Adapters.AddAssignmentAdapters;
import com.twc.tiecoon.Adapters.AddTalentAdapters;
import com.twc.tiecoon.Adapters.MyProfileTalentAdapters;
import com.twc.tiecoon.Adapters.PreferedLangAdapter;
import com.twc.tiecoon.Adapters.SelectedPreferedLanguageAdapter;
import com.twc.tiecoon.Adapters.SelectedTalentsAdapter;
import com.twc.tiecoon.Adapters.TalentsAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddProductCategoryData;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.HomeUserPreferedLanguage;
import com.twc.tiecoon.network.response.PreferredLanguage;
import com.twc.tiecoon.network.response.PreferredLanguageData;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.network.response.ProfileData;
import com.twc.tiecoon.network.response.ProfileFirstDetailsUser;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.network.response.ProfileUserTalent;
import com.twc.tiecoon.network.response.QuickbloxCheck;
import com.twc.tiecoon.network.response.UserProfileDetails;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;
import com.twc.tiecoon.utils.Utils;
import com.firebase.client.Firebase;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Observable;

public class ProfileFirst_Activity extends AbstractFragmentActivity implements View.OnClickListener, TextWatcher {
    private final String[] LOCATION_PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    private final String[] PROFILE_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int PROFILE_PERMISSION_REQUEST = 601;
    private final int LOCATION_PERMISSIONS_REQUEST = 701;
    private MultipartBody.Part multiBodyPart = null;
    private Uri photoURI;
    private static final int REQUEST_GALLERY_CODE = 605;
    private static final int REQUEST_CAMERA_CODE = 606;
    private boolean profilePermissionDeniedWithDontAskMeAgain = false;
    private boolean currentLocationPermissionDeniedWithDontAskMeAgain = false;

    private static final int CREATEASSGN = 110;
    private RelativeLayout relative_parent;
    public static final int SELECT_TALENT = 100;
    public static final int UPLOAD_TALENT = 101;
    private static final int UNAUTHORIZED = 401;
    private final ProfileModel profileModel = new ProfileModel();
    private final List<ProfileTalent> profileSelectedTalents = new ArrayList<>();
    private ImageView imgMale;
    private ImageView imgfemale;
    private ImageView imgOther;
    private ImageView imgCamera;
    private LinearLayout linearSpinnerCategory;
    private EditText et_name, et_status, et_email, et_phone, et_age, et_location, et_bio;
    private LinearLayout linear_add_talents, linear_add_assignments;
    private CardView cardViewMyTalents;
    private CircleImageView ivProfile;
    private RelativeLayout relativeImageProfile;
    private Button btnSubmit;
    private Dialog dialogTalents;
    private TalentsAdapter talentsAdapter;
    private List<ProfileTalent> addTalentDataList = new ArrayList<>();
    private List<ProfileTalent> addTalentDataList2 = new ArrayList<>();
    private SelectedTalentsAdapter selectedTalentsAdapter;
    private ArrayList<HomeUserDetailsUploadTalent> addTalentsList = new ArrayList<>();
    private ArrayList<ProfileCreateAssignment> addAssignmentList = new ArrayList<>();
    private AddTalentAdapters addTalentAdapters;
    private AddAssignmentAdapters addAssignmentAdapters;
    private RecyclerView recyclerView_add_talents, recyclerView_add_assignments, recyclerView_selected_talents, recyclerView_my_profile_add_talents;
    private String gender = "";
    private int deleteTalents;
    private int deleteAssignments;
    private boolean first = false;
    private boolean updateTalent = false;
    private boolean updateAssign = false;
    private boolean phoneNotSetFirebase = false;
    String type = "";
    String talent = "";
    ArrayAdapter arrayAdapter;
    ArrayList<String> arrayListTalentsAdded = new ArrayList();

    //myprofile talents design
    private int updatePos = 0;
    private int updateSubPos = 0;
    private List<List<HomeUserDetailsUploadTalent>> listTalent = new ArrayList<>();
    private Spinner spinnerTalents;
    private List<String> listTalentsSpinner = new ArrayList<>();
    private List<CategoryData> listCategorySpinnerData = new ArrayList<>();
    private MyProfileTalentAdapters myprofileTalentAdapters;

//    Preferred Language
    private CardView cardViewPreferredLang;
    private Dialog preferredlangDialog;
    private RecyclerView recyclerView_selected_preferedLang;
    private List<PreferredLanguageData> selectedLanguageDataList = new ArrayList<>();
    private List<PreferredLanguageData> preferredLanguageDataList = new ArrayList<>();
    private List<PreferredLanguageData> preferredLanguageDataList2 = new ArrayList<>();
    private PreferedLangAdapter preferedLangAdapter;
    private SelectedPreferedLanguageAdapter selectedPreferedLanguageAdapter;
    private ArrayList<String> languagelist = new ArrayList<>();

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_profile_first_);
        first = getIntent().getBooleanExtra("first", false);
        initView();
        getUserDetails();
    }


    private void getUserDetails() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getUserDetails(this, relative_parent);
        } else {
            Utils.warningSnackBar(this, relative_parent, getString(R.string.no_internet));
        }
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void initView() {
        relative_parent = findViewById(R.id.relative_parent);
        ImageView iv_back = findViewById(R.id.iv_back);
        ivProfile = findViewById(R.id.ivProfile);
        relativeImageProfile = findViewById(R.id.relativeImageProfile);
        imgCamera = findViewById(R.id.imgCamera);
        et_name = findViewById(R.id.et_name);
        et_status = findViewById(R.id.et_status);
        et_email = findViewById(R.id.et_email);
        et_phone = findViewById(R.id.et_phone);
        imgMale = findViewById(R.id.imgMale);
        imgfemale = findViewById(R.id.imgFemale);
        imgOther = findViewById(R.id.imgOther);
        LinearLayout ll_male = findViewById(R.id.ll_male);
        LinearLayout ll_female = findViewById(R.id.ll_female);
        LinearLayout ll_other = findViewById(R.id.ll_other);
        et_age = findViewById(R.id.et_age);
        et_location = findViewById(R.id.et_location);
        ImageView imgCurrentLocation = findViewById(R.id.imgCurrentLocation);
        et_bio = findViewById(R.id.et_bio);
        cardViewMyTalents = findViewById(R.id.cardViewMyTalents);
        cardViewPreferredLang = findViewById(R.id.cardViewPreferredLang);
        recyclerView_selected_talents = findViewById(R.id.recyclerView_selected_talents);
        recyclerView_selected_preferedLang = findViewById(R.id.recyclerView_selected_preferedLang);
        recyclerView_add_talents = findViewById(R.id.recyclerView_add_talents);
        recyclerView_add_assignments = findViewById(R.id.recyclerView_add_assignments);
        selectedTalentsAdapter = new SelectedTalentsAdapter(this, profileSelectedTalents, pos -> {
            profileSelectedTalents.remove(pos);
            selectedTalentsAdapter.notifyItemRemoved(pos);
            selectedTalentsAdapter.notifyItemRangeChanged(pos, profileSelectedTalents.size());
        });
        recyclerView_selected_talents.setAdapter(selectedTalentsAdapter);

        //myprofile talents design
        spinnerTalents = findViewById(R.id.spinnerTalents);
        linearSpinnerCategory = findViewById(R.id.linearSpinnerCategory);
        recyclerView_my_profile_add_talents = findViewById(R.id.recyclerView_my_profile_add_talents);


        linear_add_talents = findViewById(R.id.linear_add_talents);
        linear_add_assignments = findViewById(R.id.linear_add_assignments);
        btnSubmit = findViewById(R.id.btnSubmit);

        iv_back.setOnClickListener(this);
        relativeImageProfile.setOnClickListener(this);
        imgCurrentLocation.setOnClickListener(this);
        cardViewMyTalents.setOnClickListener(this);
        ll_male.setOnClickListener(this);
        ll_female.setOnClickListener(this);
        ll_other.setOnClickListener(this);
        linear_add_talents.setOnClickListener(this);
        linear_add_assignments.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        cardViewPreferredLang.setOnClickListener(this);

        addTalentAdapters = new AddTalentAdapters(this, addTalentsList, new ItemClickListenerExtraParam() {
            @Override
            public void itemClick(int pos, String type) {
                deleteTalents = pos;
                if (type.equals("update")) {
                    updateTalent = true;
                    HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = addTalentsList.get(pos);
                    ApplicationClass.uploadImageListDataArrayList.clear();
                    if (homeUserDetailsUploadTalent.getUpload_image() != null && !(homeUserDetailsUploadTalent.getUpload_image().isEmpty())) {
                        ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsUploadTalent.getUpload_image());
                    }
                    Intent i = new Intent(ProfileFirst_Activity.this, WatermarkActivity.class);
                    i.putExtra("type", "talent");
                    i.putExtra("updateItem", addTalentsList.get(pos));
                    startActivityForResult(i, UPLOAD_TALENT);
                } else {
                    removeTalent(addTalentsList.get(pos));
                }
            }
        });
        recyclerView_add_talents.setAdapter(addTalentAdapters);

        spinnerTalents.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) spinnerTalents.getSelectedView()).setTextColor(getResources().getColor(R.color.white));
                if (i == 0) {

                } else {
                    CategoryData categoryData = getCategoryData(listTalentsSpinner.get(i));
                    if (categoryData != null) {
                        if (listTalent.isEmpty()) {
                            arrayListTalentsAdded.add((String.valueOf(categoryData.getId())));
                            HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = new HomeUserDetailsUploadTalent();
                            homeUserDetailsUploadTalent.setCategoryId(categoryData.getId());
                            AddProductCategoryData addProductCategoryData = new AddProductCategoryData(categoryData.getId(), categoryData.getTitle(), categoryData.getSlug(),
                                    categoryData.getParentId(), categoryData.getDescription(), categoryData.getStatus(), categoryData.getTresh(), categoryData.getCreatedAt(), categoryData.getUpdatedAt());
                            homeUserDetailsUploadTalent.setCategory(addProductCategoryData);
                            List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = new ArrayList<>();
                            homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                            listTalent.add(homeUserDetailsUploadTalentList);
                            Log.e("talentlist", listTalent.toString());
                            myprofileTalentAdapters.notifyDataSetChanged();
                        } else {
                            boolean present = checkCategoryAdded(categoryData);
                            if (!present) {
                                arrayListTalentsAdded.add((String.valueOf(categoryData.getId())));
                                HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = new HomeUserDetailsUploadTalent();
                                homeUserDetailsUploadTalent.setCategoryId(categoryData.getId());
                                AddProductCategoryData addProductCategoryData = new AddProductCategoryData(categoryData.getId(), categoryData.getTitle(), categoryData.getSlug(),
                                        categoryData.getParentId(), categoryData.getDescription(), categoryData.getStatus(), categoryData.getTresh(), categoryData.getCreatedAt(), categoryData.getUpdatedAt());
                                homeUserDetailsUploadTalent.setCategory(addProductCategoryData);
                                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = new ArrayList<>();
                                homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                                listTalent.add(homeUserDetailsUploadTalentList);
                                Log.e("talentlist", listTalent.toString());
                                myprofileTalentAdapters.notifyDataSetChanged();
                            } else {
                                Toast.makeText(ProfileFirst_Activity.this, "Talents alreay present", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        myprofileTalentAdapters = new MyProfileTalentAdapters(this, listTalent, new ItemClickListenerExtraThreeParam() {
            @Override
            public void itemClick(int pos, String subPos, String type) {
                updatePos = pos;
                if (type.isEmpty()) {
                    updateTalent = false;
                    List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                    int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                    Intent i = new Intent(ProfileFirst_Activity.this, WatermarkActivity.class);
                    i.putExtra("type", "talent");
                    i.putExtra("categoryId", categoryId);
                    i.putExtra("categoryName", homeUserDetailsUploadTalentList.get(0).getCategory().getTitle());
                    startActivityForResult(i, UPLOAD_TALENT);
                } else if (type.equals("delete")) {

                    updateSubPos = Integer.parseInt(subPos);
                    List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                    HomeUserDetailsUploadTalent homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateSubPos);
                    removeTalent(homeUserDetailsProduct);

                } else if (type.equals("deletetalent")) {
                    if (!talent.equals("")) {
                        List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                        int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                        showDeleteDialog(categoryId);
                        myprofileTalentAdapters.notifyDataSetChanged();
                    } else {
                        listTalent.remove(pos);
                        myprofileTalentAdapters.notifyDataSetChanged();
                    }

                } else {
                    updateTalent = true;
                    updateSubPos = Integer.parseInt(subPos);
                    List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                    HomeUserDetailsUploadTalent homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateSubPos);
                    int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                    Intent i = new Intent(ProfileFirst_Activity.this, WatermarkActivity.class);
                    ApplicationClass.uploadImageListDataArrayList.clear();
                    if (homeUserDetailsProduct.getUpload_image() != null && !(homeUserDetailsProduct.getUpload_image().isEmpty())) {
                        ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsProduct.getUpload_image());
                    }
                    i.putExtra("type", "talent");
                    i.putExtra("updateItem", homeUserDetailsProduct);
                    i.putExtra("categoryId", categoryId);
                    startActivityForResult(i, UPLOAD_TALENT);
                }
            }
        });
        recyclerView_my_profile_add_talents.setAdapter(myprofileTalentAdapters);

        addAssignmentAdapters = new AddAssignmentAdapters(this, addAssignmentList, (pos, type) -> {
            deleteAssignments = pos;
            if (type.equals("update")) {
                updateAssign = true;
                Intent i = new Intent(ProfileFirst_Activity.this, CreateAssignentNew.class);
                i.putExtra("updateItem", addAssignmentList.get(pos).getId()+"");
                startActivityForResult(i, CREATEASSGN);
            } else {
                deleteAssignment(addAssignmentList.get(pos));
            }
        });
        recyclerView_add_assignments.setAdapter(addAssignmentAdapters);

    }

    private boolean checkCategoryAdded(CategoryData categoryData) {
        for (List<HomeUserDetailsUploadTalent> list : listTalent) {
            if (list.get(0).getCategoryId() == categoryData.getId()) {
                return true;
            }
        }
        return false;
    }

    private CategoryData getCategoryData(String title) {
        for (CategoryData categoryData : listCategorySpinnerData) {
            if (categoryData.getTitle() != null && title.equals(categoryData.getTitle())) {
                return categoryData;
            }
        }
        return null;
    }



    private void showDeleteDialog(int categoryId) {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_popup);

        Button button_Cancel = dialog.findViewById(R.id.button_Cancel);
        Button button_logout = dialog.findViewById(R.id.button_logout);

        button_logout.setOnClickListener(view -> {
            deleteTalent(categoryId);
            dialog.dismiss();
            listTalentsSpinner.clear();

        });

        button_Cancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void deleteTalent(int category_id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deletTalent(this, relative_parent, category_id);
        } else {
            Utils.warningSnackBar(this, relative_parent, getString(R.string.no_internet));
        }
    }


    private void deleteAssignment(ProfileCreateAssignment profileCreateAssignment) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deleteAssignment(this, relative_parent, profileCreateAssignment.getId());
        } else {
            Utils.warningSnackBar(this, relative_parent, getString(R.string.no_internet));
        }
    }

    private void removeTalent(HomeUserDetailsUploadTalent profileUploadTalent) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deleteTalents(this, relative_parent, profileUploadTalent.getId());
        } else {
            Utils.warningSnackBar(this, relative_parent, getString(R.string.no_internet));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.relativeImageProfile:
                checkPermissionForProfile();
                break;
            case R.id.imgCurrentLocation:
                checkPermissionForCurrentLocation();
                break;
            case R.id.ll_male:
                imgMale.setImageResource(R.drawable.ic_radio_checked);
                imgfemale.setImageResource(R.drawable.ic_radio_unchecked);
                imgOther.setImageResource(R.drawable.ic_radio_unchecked);
                gender = "male";
                break;
            case R.id.ll_female:
                imgMale.setImageResource(R.drawable.ic_radio_unchecked);
                imgfemale.setImageResource(R.drawable.ic_radio_checked);
                imgOther.setImageResource(R.drawable.ic_radio_unchecked);
                gender = "female";
                break;
            case R.id.ll_other:


                imgMale.setImageResource(R.drawable.ic_radio_unchecked);
                imgfemale.setImageResource(R.drawable.ic_radio_unchecked);
                imgOther.setImageResource(R.drawable.ic_radio_checked);
                gender = "other";
                break;
            case R.id.cardViewMyTalents:
                if (addTalentDataList2.isEmpty()) {
                    Toast.makeText(this, "No talents found!!", Toast.LENGTH_SHORT).show();
                } else {
                    showTalentsDialog();
                }
                break;
            case R.id.linear_add_talents:
                updateTalent = false;
                Intent i = new Intent(ProfileFirst_Activity.this, WatermarkActivity.class);
                i.putExtra("type", "talent");
                startActivityForResult(i, UPLOAD_TALENT);
                break;
            case R.id.linearSpinnerTalents:
                if (listTalentsSpinner.isEmpty()) {
                    if (Utils.isDeviceOnline(this)) {
                        Utils.showProDialog(this);
                        profileModel.getCategoryCurrency(this, relative_parent);
                    } else {
                        Utils.warningSnackBar(this, relative_parent, getString(R.string.no_internet));
                    }
                }
                break;
            case R.id.linear_add_assignments:
                updateAssign = false;
                Intent intent1 = new Intent(ProfileFirst_Activity.this, CreateAssignentNew.class);
                startActivityForResult(intent1, CREATEASSGN);
                break;
            case R.id.btnSubmit:
                checkValidationUserDetails();
                break;
            case R.id.cardViewPreferredLang:
                if (preferredLanguageDataList2.isEmpty()) {
                    if (Utils.isDeviceOnline(this)) {
                        Utils.showProDialog(this);
                        profileModel.getPreferredLang(this, relative_parent);
                    } else {
                        Utils.warningSnackBar(this, relative_parent, getString(R.string.no_internet));
                    }
                } else {
                    showPreferredLangDialog();
                }
                break;
        }
    }

    private void showPreferredLangDialog() {
        preferredlangDialog = new Dialog(this);
        preferredlangDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        preferredlangDialog.setCancelable(true);
        preferredlangDialog.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = preferredlangDialog.findViewById(R.id.editText_price_search);
        editText_price_search.setHint(getString(R.string.preferred_language));
        RecyclerView rvCurrency = preferredlangDialog.findViewById(R.id.rvCurrency);
        preferredLanguageDataList.clear();
        preferredLanguageDataList.addAll(preferredLanguageDataList2);

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);
        selectedPreferedLanguageAdapter = new SelectedPreferedLanguageAdapter(this, selectedLanguageDataList, new ItemClickListener() {
            @Override
            public void itemClick(int pos) {

                      selectedLanguageDataList.remove(pos);
                      selectedPreferedLanguageAdapter.notifyItemRemoved(pos);
                      selectedPreferedLanguageAdapter.notifyItemRangeChanged(pos, selectedLanguageDataList.size());

            }
        });
        recyclerView_selected_preferedLang.setAdapter(selectedPreferedLanguageAdapter);
        preferedLangAdapter = new PreferedLangAdapter(this, preferredLanguageDataList, new ItemClickListener() {
            @Override
            public void itemClick(int pos) {
                boolean isExits = checkPreferrdLangExits(preferredLanguageDataList.get(pos));
                if (isExits) {
                    Toast.makeText(ProfileFirst_Activity.this, "Language already added", Toast.LENGTH_SHORT).show();
                } else {
                        Log.e("list",selectedLanguageDataList.size()+"");
                         if(selectedLanguageDataList.size() > 2){
                        Toast.makeText(ProfileFirst_Activity.this, "You can Select maximum 3 Languages", Toast.LENGTH_SHORT).show();
                    }
                    else {
                             selectedLanguageDataList.add(preferredLanguageDataList.get(pos));
                             selectedPreferedLanguageAdapter.notifyDataSetChanged();
                             languagelist.add(preferredLanguageDataList.get(pos).getId() + "");
                         }


                }
                preferredlangDialog.dismiss();
            }
        });
        rvCurrency.setAdapter(preferedLangAdapter);

        editText_price_search.addTextChangedListener(this);
        preferredlangDialog.setCanceledOnTouchOutside(true);
        preferredlangDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        preferredlangDialog.show();
    }
    private boolean checkPreferrdLangExits(PreferredLanguageData preferredLanguageData) {
        for (PreferredLanguageData preferredLanguageData1 : selectedLanguageDataList) {
            if (preferredLanguageData1.getId() == preferredLanguageData.getId()) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_PERMISSIONS_REQUEST:
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    checkPermissionForCurrentLocation();
                } else if (PermissionChecker.checkCallingOrSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PermissionChecker.PERMISSION_GRANTED ||
                        PermissionChecker.checkCallingOrSelfPermission(this,
                                Manifest.permission.ACCESS_FINE_LOCATION) != PermissionChecker.PERMISSION_GRANTED) {
                    currentLocationPermissionDeniedWithDontAskMeAgain = true;
                    checkPermissionForCurrentLocation();
                } else {
                    if (!(Utils.isGPSEnabled(this))) {
                        Utils.enableGPS(this);
                    } else {
                        setUserCurrentLocation();
                    }
                }
                break;
            case PROFILE_PERMISSION_REQUEST:
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                } else if (PermissionChecker.checkCallingOrSelfPermission(this,
                        Manifest.permission.CAMERA) != PermissionChecker.PERMISSION_GRANTED ||
                        PermissionChecker.checkCallingOrSelfPermission(this
                                , Manifest.permission.READ_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED ||
                        PermissionChecker.checkCallingOrSelfPermission(this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
                    profilePermissionDeniedWithDontAskMeAgain = true;
                } else {
                    showPictureDialog();
                }
                break;
        }
    }

    private void checkPermissionForCurrentLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, LOCATION_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs location permission for current location.",
                            LOCATION_PERMISSIONS, LOCATION_PERMISSIONS_REQUEST);
                } else if (currentLocationPermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs location permission for current location.",
                            LOCATION_PERMISSIONS_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, LOCATION_PERMISSIONS, LOCATION_PERMISSIONS_REQUEST);
                }
            } else {
                if (!(Utils.isGPSEnabled(this))) {
                    Utils.enableGPS(this);
                } else {
                    setUserCurrentLocation();
                }
            }
        } else {
            if (!(Utils.isGPSEnabled(this))) {
                Utils.enableGPS(this);
            } else {
                setUserCurrentLocation();
            }

        }
    }

    private void checkPermissionForProfile() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(this, PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(this, "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(this, PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                showPictureDialog();
            }
        } else {
            showPictureDialog();
        }
    }

    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Profile photo");
        String[] pictureDialogItems = {"Gallery", "Capture photo", "Remove photo"};
        pictureDialog.setItems(pictureDialogItems, (dialog, which) -> {
            switch (which) {
                case 0:
                    Utils.choosePhotoFromGallery(this, REQUEST_GALLERY_CODE);
                    break;
                case 1:
                    photoURI = Utils.takePhotoFromCamera(this, REQUEST_CAMERA_CODE);
                    break;
                case 2:

                    deleteUserImage();
                    break;
            }
        });
        pictureDialog.setCancelable(true);
        pictureDialog.show();
    }


    private void setImage(Uri uri) {
        imgCamera.setVisibility(View.GONE);
        String filePath = Utils.getUriRealPath(this, uri);
        File file = new File(filePath);
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
//        editivProfile.setImageBitmap(bitmap);
        try {
            ivProfile.setImageBitmap(Utils.getScaledBitmap(this, uri));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Log.d("File", "Filename " + file.getName());
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpg"), file);
        //  RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        multiBodyPart = MultipartBody.Part.createFormData("profile", file.getName(), requestFile);
        //RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
    }

    private void deleteUserImage() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deleteUserImage(this, relative_parent);
        } else {
            Utils.warningSnackBar(this, relative_parent, getString(R.string.no_internet));
        }
    }


    private void checkValidationUserDetails() {
        String name = et_name.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "Name can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (name.length() <= 1) {
            Toast.makeText(this, "Enter a valid name", Toast.LENGTH_SHORT).show();
            return;
        }
        String status = et_status.getText().toString().trim();
        String email = et_email.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Email can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!(Utils.isValidEmail(email))) {
            Toast.makeText(this, "Email not valid", Toast.LENGTH_SHORT).show();
            return;
        }
        String phone = et_phone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(this, "Phone Number can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (phone.length() < 7) {
            Toast.makeText(this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
            return;
        }

        String age = et_age.getText().toString().trim();
        if (TextUtils.isEmpty(age)) {
            Toast.makeText(this, "Age can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        int numAge = Integer.parseInt(age);
        if (numAge <= 0 || numAge > 100) {
            Toast.makeText(this, "Age is not valid", Toast.LENGTH_SHORT).show();
            return;
        }
        String location = et_location.getText().toString().trim();
        if (TextUtils.isEmpty(location)) {
            Toast.makeText(this, "Location can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(gender)) {
            Toast.makeText(this, "Gender not selected", Toast.LENGTH_SHORT).show();
            return;
        }
        if (selectedLanguageDataList.isEmpty()) {
            Toast.makeText(this, "Preferred Language can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        String bio = et_bio.getText().toString().trim();

        sendUserDetails(name, status, email, phone, age, location, bio);
    }
    protected void showErrorSnackbar(@StringRes int resId, Exception e, View.OnClickListener clickListener) {
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        if (rootView != null) {
//            ErrorUtils.showSnackbar(rootView, resId, e,
//                    R.string.dlg_retry, clickListener);
        }
    }




    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof ProfileData) {
            setUserDetails((ProfileData) o);
            if (Utils.isDeviceOnline(this)) {
                Utils.showProDialog(this);
                profileModel.getCategoryCurrency(this, relative_parent);
            } else {
                Utils.warningSnackBar(this, relative_parent, getString(R.string.no_internet));
            }
        } else if (o instanceof String) {
            String data = (String) o;
            if (data.equals("assignmentDeleted")) {
                addAssignmentList.remove(deleteAssignments);
                addAssignmentAdapters.notifyItemRemoved(deleteAssignments);
            } else if (data.equals("deleted")) {
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(updatePos);
                homeUserDetailsUploadTalentList.remove(updateSubPos);
                listTalent.remove(updatePos);
                recyclerView_my_profile_add_talents.setAdapter(myprofileTalentAdapters);
                myprofileTalentAdapters.notifyDataSetChanged();
            }
            if (data.equals("deleteUserImage")) {
                ivProfile.setImageBitmap(null);
                imgCamera.setVisibility(View.VISIBLE);
            }
        }else  if (o instanceof PreferredLanguage) {
            PreferredLanguage preferredLanguage = (PreferredLanguage) o;
            List<PreferredLanguageData> preferredLanguageDataListNew = preferredLanguage.getData();
            if (preferredLanguageDataListNew != null && !(preferredLanguageDataListNew.isEmpty())) {
                preferredLanguageDataList.addAll(preferredLanguageDataListNew);
                preferredLanguageDataList2.addAll(preferredLanguageDataListNew);
                showPreferredLangDialog();
            }
        } else if (o instanceof ProfileFirstDetailsUser) {
            ApplicationClass.appPreferences.setFirstTime(true);
            ProfileFirstDetailsUser profileFirstDetailsUser = (ProfileFirstDetailsUser) o;
            if (profileFirstDetailsUser.getEmail() != null) {
                ApplicationClass.appPreferences.setEmailId(profileFirstDetailsUser.getEmail());
            }

            if (profileFirstDetailsUser.getPath() != null && profileFirstDetailsUser.getProfile() != null) {
                String imageURL = (new StringBuilder().append(Constant.ImageURL).append(profileFirstDetailsUser.getPath())
                        .append("/").append(profileFirstDetailsUser.getProfile())).toString();
                ApplicationClass.appPreferences.setUserPicUrl(imageURL);
            } else
                ApplicationClass.appPreferences.setUserPicUrl(profileFirstDetailsUser.getProfile());
            if (profileFirstDetailsUser.getUserDetils() != null) {
                if (profileFirstDetailsUser.getUserDetils().getName() != null) {
                    ApplicationClass.appPreferences.setUserName(profileFirstDetailsUser.getUserDetils().getName());
                }
            }
            if (first) {
//                setUserForChat(String.valueOf(profileFirstDetailsUser.getId()));
                ApplicationClass.appPreferences.setPhoneNumber(et_phone.getText().toString());
                prepareUser();
                checkQuickSubscription();

                Intent intent = new Intent(this, UserHomeActivity.class);
                intent.putExtra("user","first");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                Toast.makeText(this, "Profile updated successfully!", Toast.LENGTH_SHORT).show();
                finish();
            }
        } else if (o instanceof CategoryCurrency) {
            setCategoryCurrency((CategoryCurrency) o);
        }
    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1", "size:" + categoryDataList1.size() + ",  " + categoryDataList1.toString());
        if (categoryDataList1 != null && !(categoryDataList1.isEmpty())) {
            for (CategoryData categoryData : categoryDataList1) {
                if (categoryData.getTitle() != null) {
                    listCategorySpinnerData.add(categoryData);
                    listTalentsSpinner.add(categoryData.getTitle());
                }
            }
            listTalentsSpinner.add(0, "Select Talents");
            arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, listTalentsSpinner);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            //Setting the ArrayAdapter data on the Spinner
            spinnerTalents.setAdapter(arrayAdapter);
        }
    }


    private void setUserForChat(String userId) {
        Firebase.setAndroidContext(this);
        String url = "https://tiecoon-2c718.firebaseio.com/users.json";
//            final ProgressDialog pd = new ProgressDialog(OtpActivity.this);
//            pd.setMessage("Loading...");
//            pd.show();

        StringRequest request = new StringRequest(Request.Method.GET, url, s -> {
            Firebase reference = new Firebase("https://tiecoon-2c718.firebaseio.com/users");

            if (s.equals("null")) {
                reference.child(userId).child("password").setValue("1234");
                // Toast.makeText(OtpActivity.this, "registration successful", Toast.LENGTH_LONG).show();
            } else {
                try {
                    JSONObject obj = new JSONObject(s);
                    if (!obj.has(userId)) {
                        reference.child(userId).child("password").setValue("1234");
                        //  Toast.makeText(OtpActivity.this, "registration successful", Toast.LENGTH_LONG).show();
                    } else {
                        // Toast.makeText(OtpActivity.this, "username already exists", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

//                    pd.dismiss();
        }, volleyError -> {
            System.out.println("" + volleyError);
//                    pd.dismiss();
        });

        RequestQueue rQueue = Volley.newRequestQueue(this);
        rQueue.add(request);

    }

    private void getTalents() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getUserDetails(this, relative_parent);
        } else {
            Utils.warningSnackBar(this, relative_parent, getString(R.string.no_internet));
        }
    }

    public void sendUserDetails(String name, String status, String email, String phone, String age, String location, String bio) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            ArrayList<String> arrayList = new ArrayList();
            for (ProfileTalent profileTalent : profileSelectedTalents) {
                arrayList.add(String.valueOf(profileTalent.getId()));
            }
            ArrayList<Integer> arrayListLang = new ArrayList<>();
            for (PreferredLanguageData preferredLanguageData : selectedLanguageDataList) {
                arrayListLang.add(preferredLanguageData.getId());
            }
            profileModel.updateProfile(this, relative_parent, multiBodyPart, name, status, email, phone, gender,
                    Integer.parseInt(age), location, bio, arrayListTalentsAdded,arrayListLang);
        } else {
            Utils.warningSnackBar(this, relative_parent, getString(R.string.no_internet));
        }
    }

    private void setUserDetails(ProfileData profileData) {
        type = profileData.getType();
        if (profileData.getType().equals("facebook") && profileData.getEmail() != null) {
            et_email.setEnabled(false);
        }
        if (profileData.getType().equals("google") && profileData.getEmail() != null) {
            et_email.setEnabled(false);
        }

        Log.d("profile", "" + profileData.toString());
        ApplicationClass.appPreferences.setUserId(profileData.getId());
        UserProfileDetails userProfileDetails = profileData.getUserDetils();
        if (profileData.getPhone() == null || profileData.getPhone().isEmpty()) {
            phoneNotSetFirebase = true;
        }
        if (userProfileDetails != null) {
            if (userProfileDetails.getName() != null) {
                et_name.setText(String.valueOf(userProfileDetails.getName()));
            }

            if (userProfileDetails.getStatus() != null) {
                et_status.setText(userProfileDetails.getStatus());
            }

            if (userProfileDetails.getAge() != null) {
                et_age.setText(userProfileDetails.getAge());
            }

            if (userProfileDetails.getLocation() != null) {
                et_location.setText(userProfileDetails.getLocation());
            }

            if (userProfileDetails.getBio() != null) {
                et_bio.setText(userProfileDetails.getBio());
            }
//            if (userProfileDetails.getPreferredLanguage() != null && !(userProfileDetails.getPreferredLanguage().isEmpty())) {
//                for (HomeUserPreferedLanguage homeUserPreferedLanguage : userProfileDetails.getPreferredLanguage()) {
//                    if (homeUserPreferedLanguage.getLanguage() != null) {
//                        selectedLanguageDataList.add(homeUserPreferedLanguage.getLanguage());
//                    }
//                }
//                selectedPreferedLanguageAdapter.notifyDataSetChanged();
//            }
            if (userProfileDetails.getGender() != null && !(userProfileDetails.getGender().isEmpty())) {
                switch (userProfileDetails.getGender().toLowerCase()) {
                    case "male":
                        gender = "male";
                        imgMale.setImageResource(R.drawable.ic_radio_checked);
                        break;
                    case "female":
                        gender = "female";
                        imgfemale.setImageResource(R.drawable.ic_radio_checked);
                        break;
                    case "other":
                        gender = "other";
                        imgOther.setImageResource(R.drawable.ic_radio_checked);
                        break;
                    default:
                        imgMale.setImageResource(R.drawable.ic_radio_unchecked);
                        imgfemale.setImageResource(R.drawable.ic_radio_unchecked);
                        imgOther.setImageResource(R.drawable.ic_radio_unchecked);
                }
            }
        }
        if (profileData.getEmail() != null) {
            et_email.setText(profileData.getEmail());
        }
        if (profileData.getPhone() != null) {
            et_phone.setText(profileData.getPhone());
        }

        if (profileData.getTalents() != null && !(profileData.getTalents().isEmpty())) {
            if (profileData.getProfileUserTalents() != null && !(profileData.getProfileUserTalents().isEmpty())) {
                for (ProfileUserTalent profileUserTalent : profileData.getProfileUserTalents()) {
                    for (ProfileTalent profileTalent : profileData.getTalents()) {
                        if (profileUserTalent.getTalentId().equals(profileTalent.getId())) {
                            profileSelectedTalents.add(profileTalent);
                        }
                    }
                }
                selectedTalentsAdapter.notifyDataSetChanged();
            }
        }

        if (profileData.getProfile() != null && !(profileData.getProfile().isEmpty())) {
            Glide.with(this)
                    .load(profileData.getProfile())
                    .into(ivProfile);
            imgCamera.setVisibility(View.GONE);
        }
        List<ProfileTalent> profileTalentList = profileData.getTalents();
        if (profileTalentList != null) {
            addTalentDataList.addAll(profileTalentList);
            addTalentDataList2.addAll(profileTalentList);
        }
        List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents1 = profileData.getUploadTalent();
        if (homeUserDetailsUploadTalents1 != null && !(homeUserDetailsUploadTalents1.isEmpty())) {
            listTalent.clear();
            setUploadTalentData(homeUserDetailsUploadTalents1);
        }

        List<ProfileCreateAssignment> profileCreateAssignmentList = profileData.getCreateAssignments();
        if (profileCreateAssignmentList != null && !(profileCreateAssignmentList.isEmpty())) {
            addAssignmentList.clear();
            addAssignmentList.addAll(profileCreateAssignmentList);
            addAssignmentAdapters.notifyDataSetChanged();
        }
    }

    private void setUploadTalentData(List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents1) {
        List<HomeUserDetailsUploadTalent> listTalentNew = new ArrayList<>(homeUserDetailsUploadTalents1);
        Iterator<HomeUserDetailsUploadTalent> iteratormain = homeUserDetailsUploadTalents1.iterator();
        while (iteratormain.hasNext()) {
            HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = (HomeUserDetailsUploadTalent) iteratormain.next();
            List<HomeUserDetailsUploadTalent> newlist = new ArrayList<>();
            Iterator<HomeUserDetailsUploadTalent> iteratorsub = listTalentNew.iterator();
            if (iteratorsub.hasNext()) {
                while (iteratorsub.hasNext()) {
                    HomeUserDetailsUploadTalent homeUserDetailsUploadTalent1 = (HomeUserDetailsUploadTalent) iteratorsub.next();
                    if ((homeUserDetailsUploadTalent.getCategoryId().equals(homeUserDetailsUploadTalent1.getCategoryId()))) {
                        newlist.add(homeUserDetailsUploadTalent1);
                        iteratorsub.remove();
                    }
                }
            } else {
                break;
            }
            if (!(newlist.isEmpty())) {
                iteratormain.remove();
                listTalent.add(newlist);
            }
        }
        int talentTotal = listTalent.size();
        myprofileTalentAdapters.notifyDataSetChanged();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case UPLOAD_TALENT:
                //myprofile talents design
                if (resultCode == RESULT_OK) {
                    HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = (HomeUserDetailsUploadTalent) data.getParcelableExtra("UploadData");
                    talent = data.getStringExtra("Talent");
                    List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(updatePos);
                    if (updateTalent) {
                        homeUserDetailsUploadTalentList.set(updateSubPos, homeUserDetailsUploadTalent);
                    } else {
                        homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                    }
                    listTalent.set(updatePos, homeUserDetailsUploadTalentList);
                    Log.d("gsdfgdfg",listTalent.toString());
                    myprofileTalentAdapters.notifyItemChanged(updatePos);
                }
                break;
            case CREATEASSGN:
                if (resultCode == RESULT_OK) {
                    ProfileCreateAssignment profileCreateAssignment = (ProfileCreateAssignment) data.getParcelableExtra("createAssign");
                    if (updateAssign) {
                        addAssignmentList.set(deleteAssignments, profileCreateAssignment);
                        addAssignmentAdapters.notifyItemChanged(deleteAssignments);
                    } else {
                        addAssignmentList.add(profileCreateAssignment);
                        addAssignmentAdapters.notifyDataSetChanged();
                    }
                }
                break;
            case REQUEST_GALLERY_CODE:
                try {
                    Uri imageUri = data.getData();
                    Bitmap bitmap = Utils.getScaledBitmap(this, imageUri);
                    Uri uri = Utils.getImageUri(this, bitmap);
                    Utils.cropImage(this, uri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case REQUEST_CAMERA_CODE:
                Uri uri2 = photoURI;
                Utils.cropImage(this, uri2);
                break;
            case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                if (data != null) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    Uri resultUri = result.getUri();
                    setImage(resultUri);
                }
                break;
            case LOCATION_PERMISSIONS_REQUEST:
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(this,
                                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    if (!(Utils.isGPSEnabled(this))) {
                        Utils.enableGPS(this);
                    } else {
                        setUserCurrentLocation();
                    }
                }
                break;
            case 15:
                if (!(Utils.isGPSEnabled(this))) {
                    Utils.enableGPS(this);
                } else {
                    setUserCurrentLocation();
                }
                break;
        }
    }


    private void showTalentsDialog() {
        dialogTalents = new Dialog(this);
        dialogTalents.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTalents.setCancelable(true);
        dialogTalents.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = dialogTalents.findViewById(R.id.editText_price_search);
        editText_price_search.setHint(getString(R.string.talents));
        RecyclerView rvCurrency = dialogTalents.findViewById(R.id.rvCurrency);
        addTalentDataList.clear();
        addTalentDataList.addAll(addTalentDataList2);

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);

        talentsAdapter = new TalentsAdapter(this, addTalentDataList, new ItemClickListener() {
            @Override
            public void itemClick(int pos) {
                boolean isExits = checkTalentsExits(addTalentDataList.get(pos));
                if (isExits) {
                    Toast.makeText(ProfileFirst_Activity.this, "Talent already added", Toast.LENGTH_SHORT).show();
                } else {
                    profileSelectedTalents.add(addTalentDataList.get(pos));
                    selectedTalentsAdapter.notifyDataSetChanged();
                }
                dialogTalents.dismiss();
            }
        });
        rvCurrency.setAdapter(talentsAdapter);

        editText_price_search.addTextChangedListener(this);
        dialogTalents.setCanceledOnTouchOutside(true);
        dialogTalents.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogTalents.show();
    }

    private boolean checkTalentsExits(ProfileTalent profileTalent) {
        for (ProfileTalent profileTalent1 : profileSelectedTalents) {
            if (profileTalent1.getId() == profileTalent.getId()) {
                return true;
            }
        }
        return false;
    }

    @SuppressLint("SetTextI18n")
    private void setUserCurrentLocation() {
        Location location = Utils.getLocation(this);
        if (location != null) {
            Address address = Utils.getAddressUsingLatLang(this, location.getLatitude(), location.getLongitude());
            if (address != null) {
                et_location.setText(address.getLocality() + "," + address.getCountryName());
            }
        } else {
            fetchLocation();
        }
    }

    private void fetchLocation() {
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    Address address = Utils.getAddressUsingLatLang(ProfileFirst_Activity.this, location.getLatitude(), location.getLongitude());

                    if (address != null) {
                        et_location.setText(address.getLocality() + "," + address.getCountryName());
                    }
                }
            }
        });
    }
   
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        filter(s.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void filter(String charText) {
        String newCharText = charText.toLowerCase(Locale.getDefault()).trim();
        addTalentDataList.clear();
        if (charText.isEmpty()) {
            addTalentDataList.addAll(addTalentDataList2);
        } else {
            for (ProfileTalent profileTalent : addTalentDataList2) {
                if (profileTalent.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                    addTalentDataList.add(profileTalent);
                }
            }
        }
        talentsAdapter.notifyDataSetChanged();

        preferredLanguageDataList.clear();
        if (charText.isEmpty()) {
            preferredLanguageDataList.addAll(preferredLanguageDataList2);
        } else {
            for (PreferredLanguageData preferredLanguageData : preferredLanguageDataList2) {
                if (preferredLanguageData.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                    preferredLanguageDataList.add(preferredLanguageData);
                }
            }
        }
        preferedLangAdapter.notifyDataSetChanged();
    }
    public void checkQuickSubscription() {

        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<QuickbloxCheck> call = apiInterface.QuickbloxCheck("Bearer "+accessToken);
        call.enqueue(new Callback<QuickbloxCheck>() {
            @Override
            public void onResponse(Call<QuickbloxCheck> call, Response<QuickbloxCheck> response) {

                QuickbloxCheck subscriptionModel = response.body();
                prepareUser();
            }

            @Override
            public void onFailure(Call<QuickbloxCheck> call, Throwable t) {

                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    private void prepareUser() {
        QBUser qbUser = new QBUser();
        qbUser.setLogin(ApplicationClass.appPreferences.getPhoneNumber());
//         qbUser.setFullName(usernameEt.getText().toString().trim());
        qbUser.setPassword(ApplicationClass.appPreferences.getPhoneNumber());
        //qbUser.setPassword(App.USER_DEFAULT_PASSWORD);
        signIn(qbUser);
    }

    private void signIn(final QBUser user) {

        ChatHelper.getInstance().login(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser userFromRest, Bundle bundle) {
                if (userFromRest.getId().equals(user.getId())) {
                    loginToChat(user);
//                    Toast.makeText(UserHomeActivity.this, "login", Toast.LENGTH_SHORT).show();
                } else {
                    //Need to set password NULL, because server will update user only with NULL password
                    user.setPassword(null);
                    Toast.makeText(ProfileFirst_Activity.this, "register", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    private void loginToChat(final QBUser user) {
        //Need to set password, because the server will not register to chat without password
        user.setPassword(ApplicationClass.appPreferences.getPhoneNumber());
        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                AppPreferences.getInstance().saveQbUser(user);
                finish();
//                hideProgressDialog();
            }

            @Override
            public void onError(QBResponseException e) {
//                hideProgressDialog();
//                showErrorSnackbar(R.string.login_chat_login_error, e, null);
            }
        });
    }
}