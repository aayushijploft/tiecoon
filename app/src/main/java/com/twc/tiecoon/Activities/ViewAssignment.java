package com.twc.tiecoon.Activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Adapters.ViewTalentAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.Feedback;
import com.twc.tiecoon.network.response.FeedbackModel;
import com.twc.tiecoon.network.response.ProjectDetails;
import com.twc.tiecoon.network.response.ProjectRequestModel;
import com.twc.tiecoon.network.response.SaveProject;
import com.twc.tiecoon.network.response.UserFeedback;
import com.twc.tiecoon.utils.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ViewAssignment extends AbstractFragmentActivity {

    TextView textView_select_Talents, editText, et_name, tv_language, tv_country, tv_state, tv_city, tv_AgeGroup,
            tv_Gender, tvTitle,tv_Budget, tv_ReferenceLink, rfLable, tvEnquire,tv_ProjectId;

    private final ProfileModel profileModel = new ProfileModel();
    RelativeLayout relativeParent;
    RecyclerView rvSelectedTalents;
    String data = "", qbID = "", name = "", projectuser_id;
    LinearLayout linear_like,linearAgeGroup,linear_share;
    ImageView imgLike;
    EditText editTextText;
    Button btnCollab;
    RecyclerView recyclerView_Feedback;
    CardView msgCollab;
    RecyclerView rvUsers;
    CardView userfeedback;
    String userid = "",username="";
    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_assignment);
        data = getIntent().getStringExtra("project_id");

        if (getIntent().hasExtra("qbID")) {
            qbID = getIntent().getStringExtra("qbID");
        }
        if (getIntent().hasExtra("name")) {
            name = getIntent().getStringExtra("name");
        }

        relativeParent = findViewById(R.id.relativeParent);
        et_name = findViewById(R.id.et_name);
        editText = findViewById(R.id.editText);
        tv_language = findViewById(R.id.tv_language);
        tv_country = findViewById(R.id.tv_country);
        tv_state = findViewById(R.id.tv_state);
        tv_city = findViewById(R.id.tv_city);
        tv_AgeGroup = findViewById(R.id.tv_AgeGroup);
        userfeedback = findViewById(R.id.userfeedback);
        tvTitle = findViewById(R.id.tvTitle);
        tv_Budget = findViewById(R.id.tv_Budget);
        rfLable = findViewById(R.id.rfLable);
        tv_ReferenceLink = findViewById(R.id.tv_ReferenceLink);
        linearAgeGroup = findViewById(R.id.linearAgeGroup);
        tv_Gender = findViewById(R.id.tv_Gender);
        tv_ProjectId = findViewById(R.id.tv_ProjectId);
        rvSelectedTalents = findViewById(R.id.rvSelectedTalents);
        textView_select_Talents = findViewById(R.id.textView_select_Talents);
        linear_like = findViewById(R.id.linear_like);
        linear_share = findViewById(R.id.linear_share);
        imgLike = findViewById(R.id.imgLike);
        editTextText = findViewById(R.id.editTextText);
        rvUsers = findViewById(R.id.rvUsers);
        rvUsers.setHasFixedSize(true);
        rvUsers.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvSelectedTalents.setHasFixedSize(true);
        rvSelectedTalents.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView_Feedback = findViewById(R.id.recyclerView_Feedback);
        linear_like.setOnClickListener(v -> setProjectLike());
        findViewById(R.id.ivBack).setOnClickListener(v -> onBackPressed());

        linear_share.setOnClickListener(v ->{
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Tiecoon");
                String message = "https://play.google.com/store/apps/details?id=" + "com.twc.tiecoon" + "\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, message);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch (Exception e) {
                e.printStackTrace();
            }

        } );

        msgCollab = findViewById(R.id.msgCollab);
        tvEnquire = findViewById(R.id.tvEnquire);
        tvEnquire.setOnClickListener(v -> EnquireFirst());

        tv_ReferenceLink.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tv_ReferenceLink.getText().toString()));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setPackage("com.google.android.youtube");
            startActivity(intent);
        });
        tvTitle.setOnClickListener(v -> {
            Intent i = new Intent(this, OtherUserProfileActivity.class);
            i.putExtra("UserId",userid);
            startActivity(i);
        });


        btnCollab = findViewById(R.id.btnCollab);
        btnCollab.setOnClickListener(v -> {
            submitCollab();

        });
        viewProjectDetails(data);
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }


    private void setProjectLike() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);

            profileModel.saveProject(this, relativeParent, data);

        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof ProjectDetails) {
            ProjectDetails projectDetails = (ProjectDetails) o;
            if (projectDetails.getData().getUser_id() == ApplicationClass.appPreferences.getUserId()) {
                btnCollab.setVisibility(View.GONE);
                tvEnquire.setVisibility(View.GONE);
                msgCollab.setVisibility(View.GONE);
                userfeedback.setVisibility(View.VISIBLE);
                linear_like.setVisibility(View.GONE);
            } else {
                switch (projectDetails.getData().isStatus()) {
                    case "completed":
                        btnCollab.setVisibility(View.VISIBLE);
                        tvEnquire.setVisibility(View.VISIBLE);
                        msgCollab.setVisibility(View.GONE);
                        btnCollab.setBackground(getResources().getDrawable(R.drawable.greybtn));
                        btnCollab.setEnabled(false);
                        break;
                    case "worked_on":
                        btnCollab.setVisibility(View.VISIBLE);
                        tvEnquire.setVisibility(View.VISIBLE);
                        msgCollab.setVisibility(View.GONE);
                        btnCollab.setBackground(getResources().getDrawable(R.drawable.greybtn));
                        btnCollab.setEnabled(false);
                        break;
                    case "ongoing":
                        btnCollab.setVisibility(View.VISIBLE);
                        tvEnquire.setVisibility(View.VISIBLE);
                        msgCollab.setVisibility(View.VISIBLE);
                        break;
                }
            }

            projectuser_id = projectDetails.getData().getUser_id() + "";
            feedbackList(data);
            et_name.setText(projectDetails.getData().getName());
            tv_ProjectId.setText(projectDetails.getData().getDigitId());
            tvTitle.setText(projectDetails.getData().getOwnerName());
            username = projectDetails.getData().getOwnerName();
            userid = projectDetails.getData().getUser_id()+"";
            editTextText.setText("Hello , I am interested in working on your project " + "'" + projectDetails.getData().getName() + "'");
            editText.setText(projectDetails.getData().getText());

            if(projectDetails.getData().getLanguage() != null){
                tv_language.setText(projectDetails.getData().getLanguage());
            }
            else tv_language.setVisibility(View.GONE);

            if(projectDetails.getData().getCountry() != null){
                tv_country.setText(projectDetails.getData().getCountry());
            }
            else tv_country.setVisibility(View.GONE);


            tv_state.setText(projectDetails.getData().getState());
            tv_city.setText(projectDetails.getData().getCity());

            if(projectDetails.getData().getGender() != null){
                tv_Gender.setText(projectDetails.getData().getGender());
            }
            else  tv_Gender.setVisibility(View.GONE);

            if(!projectDetails.getData().getAge().equals("")){
                tv_AgeGroup.setText(projectDetails.getData().getAge());
            }
            else  linearAgeGroup.setVisibility(View.GONE);

//            tv_AgeGroup.setText(projectDetails.getData().getAge());
            tv_Budget.setText(projectDetails.getData().getCurrency() + " " + projectDetails.getData().getAmount());
            if (projectDetails.getData().isIs_save()) {
                imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_like_solid));
            } else imgLike.setImageDrawable(getResources().getDrawable(R.drawable.ic_like_));

            if (projectDetails.getData().getYoutublink() == null) {
                tv_ReferenceLink.setVisibility(View.GONE);
                rfLable.setVisibility(View.GONE);
            } else {
                tv_ReferenceLink.setVisibility(View.VISIBLE);
                rfLable.setVisibility(View.VISIBLE);
            }
            tv_ReferenceLink.setText(projectDetails.getData().getYoutublink());
            ViewTalentAdapter viewTalentAdapter = new ViewTalentAdapter(getApplicationContext(), projectDetails.getData().getTalent());
            rvSelectedTalents.setAdapter(viewTalentAdapter);
        } else if (o instanceof SaveProject) {
            finish();
            startActivity(getIntent());
        }
    }

    private void viewProjectDetails(String id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.viewProjectDetails(this, relativeParent, id);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

//    private void submitCollab() {
//        if (Utils.isDeviceOnline(this)) {
//            profileModel.submitCollab(this, relativeParent, projectuser_id, data, editTextText.getText().toString());
//        } else {
//            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
//        }
//    }

    public void submitCollab() {
        final Dialog dialog = new Dialog(ViewAssignment.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<ProjectRequestModel> call = apiInterface.submitCollab("Bearer "+accessToken ,projectuser_id, data, editTextText.getText().toString());

        call.enqueue(new Callback<ProjectRequestModel>() {
            @Override
            public void onResponse(Call<ProjectRequestModel> call, Response<ProjectRequestModel> response) {
                dialog.dismiss();
                ProjectRequestModel user = response.body();
                assert user != null;
                if(user.getMessage().equals("Already request send")) {
                    Toast.makeText(ViewAssignment.this, "You Already Sent Request for this project", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(ViewAssignment.this, "Collaboration Request sent", Toast.LENGTH_SHORT).show();
//                    ProgressDialog progressDialog;
//                    progressDialog = new ProgressDialog(ViewAssignment.this);
//                    progressDialog.setMessage("Connecting to chat, please wait...");
//                    progressDialog.setIndeterminate(false);
//                    progressDialog.setCancelable(true);
//                    progressDialog.show();
//                    ArrayList<Integer> occupantIdsList = new ArrayList<Integer>();
//                    occupantIdsList.add(Integer.valueOf(qbID));
//                    QBChatDialog dialog = new QBChatDialog();
//                    dialog.setType(QBDialogType.PRIVATE);
//                    dialog.setOccupantsIds(occupantIdsList);
//                    QBRestChatService.createChatDialog(dialog).performAsync(new QBEntityCallback<QBChatDialog>() {
//                        @Override
//                        public void onSuccess(QBChatDialog result, Bundle params) {
//
//                            Log.e("result", result.toString());
//                            Log.e("qid", qbID);
//                            if (progressDialog.isShowing()) {
//                                progressDialog.dismiss();
//                            }
//                            ChatActivity.startForResult(ViewAssignment.this, 101, result, name, editTextText.getText().toString());
//                        }
//
//                        @Override
//                        public void onError(QBResponseException responseException) {
//                            progressDialog.dismiss();
//                            Toast.makeText(ViewAssignment.this, "ERRROR", Toast.LENGTH_SHORT).show();
//                        }
//                    });
                }

            }

            @Override
            public void onFailure(Call<ProjectRequestModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });

    }

    private void EnquireFirst() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.collab_enquiry_popup);
        ImageView imgCancel = dialog.findViewById(R.id.imgCancel);
        EditText editText = dialog.findViewById(R.id.editText);
        Button btnSave = dialog.findViewById(R.id.btnSave);
        Button btnCancle = dialog.findViewById(R.id.btnCancle);

        imgCancel.setOnClickListener(v -> dialog.dismiss());
        btnCancle.setOnClickListener(v -> dialog.dismiss());
        btnSave.setOnClickListener(v -> {

            dialog.dismiss();
            ArrayList<Integer> occupantIdsList = new ArrayList<Integer>();
            occupantIdsList.add(Integer.valueOf(qbID));

            QBChatDialog qbChatDialog = new QBChatDialog();
            qbChatDialog.setType(QBDialogType.PRIVATE);
            qbChatDialog.setOccupantsIds(occupantIdsList);
            QBRestChatService.createChatDialog(qbChatDialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                @Override
                public void onSuccess(QBChatDialog result, Bundle params) {
                    Log.e("result", result.toString());
                    ChatActivity.startForResult(ViewAssignment.this, 101, result, username, editText.getText().toString());
                }
                @Override
                public void onError(QBResponseException responseException) {
                    Toast.makeText(ViewAssignment.this, "ERRROR", Toast.LENGTH_SHORT).show();
                }
            });
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void feedbackList(String id) {
        final Dialog dialog = new Dialog(ViewAssignment.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<FeedbackModel> call = apiInterface.projectratingList("Bearer " + accessToken, id);
        call.enqueue(new Callback<FeedbackModel>() {
            @Override
            public void onResponse(@NotNull Call<FeedbackModel> call, @NotNull Response<FeedbackModel> response) {
                dialog.dismiss();
                FeedbackModel user = response.body();
                assert user != null;
                if (user.getData().size() > 0) {
                    recyclerView_Feedback.setVisibility(View.VISIBLE);
                    FeedbackAdapter feedbackAdapter = new FeedbackAdapter(ViewAssignment.this, user.getData());
                    recyclerView_Feedback.setAdapter(feedbackAdapter);
                } else recyclerView_Feedback.setVisibility(View.GONE);
                userlistsforfeedback(data);
            }

            @Override
            public void onFailure(@NotNull Call<FeedbackModel> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public static class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {
        private final Context context;
        private final List<FeedbackModel.Data> list;

        public FeedbackAdapter(Context context, List<FeedbackModel.Data> list) {
            this.context = context;
            this.list = list;
        }

        @NotNull
        @Override
        public FeedbackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.buying_items_user_rating_item_layout, parent, false);
            return new FeedbackAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(FeedbackAdapter.ViewHolder holder, final int position) {
            Glide.with(context).load(list.get(position).getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.imgProfile);
            holder.tv_UserName.setText(list.get(position).getUsername());
            holder.tv_UserName.setVisibility(View.GONE);
            holder.simpleRatingBar.setText(String.valueOf(list.get(position).getRating()));
            holder.textViewDescription.setText(list.get(position).getMassage());
            holder.rating_feedback.setRating(Float.parseFloat(String.valueOf(list.get(position).getRating())));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {

            CircleImageView imgProfile;
            TextView tv_UserName, simpleRatingBar, textViewDescription;
            RatingBar rating_feedback;

            public ViewHolder(View itemView) {
                super(itemView);
                imgProfile = itemView.findViewById(R.id.imgProfile);
                tv_UserName = itemView.findViewById(R.id.tv_UserName);
                simpleRatingBar = itemView.findViewById(R.id.simpleRatingBar);
                rating_feedback = itemView.findViewById(R.id.rating_feedback);
                textViewDescription = itemView.findViewById(R.id.textViewDescription);

            }
        }
    }

    public void userlistsforfeedback(String projectid) {
        final Dialog dialog = new Dialog(ViewAssignment.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<UserFeedback> call = apiInterface.projectEndUserList("Bearer " + accessToken, projectid);

        call.enqueue(new Callback<UserFeedback>() {
            @Override
            public void onResponse(Call<UserFeedback> call, Response<UserFeedback> response) {
                dialog.dismiss();
                UserFeedback user = response.body();
                Log.e("user", user.toString());
                FeedbackUserAdapter feedbackUserAdapter = new FeedbackUserAdapter(ViewAssignment.this, user.getData(), projectid);
                rvUsers.setAdapter(feedbackUserAdapter);
//                Toast.makeText(ViewAssignment.this, "Your withdraw request has been sent to Admin", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<UserFeedback> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public static class FeedbackUserAdapter extends RecyclerView.Adapter<FeedbackUserAdapter.ViewHolder> {

        private final Context context;
        private final List<UserFeedback.Data> list;
        String projectid;

        public FeedbackUserAdapter(Context context, List<UserFeedback.Data> list, String projectid) {
            this.context = context;
            this.list = list;
            this.projectid = projectid;
        }

        @NotNull
        @Override
        public FeedbackUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.feedbackuser_layout, parent, false);
            return new FeedbackUserAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(FeedbackUserAdapter.ViewHolder holder, final int position) {
            holder.tvUserName.setText(list.get(position).getName());

            if (list.get(position).getFeedbackstatus().equals("1")) {
                holder.btnFeedback.setBackground(context.getResources().getDrawable(R.drawable.greybtn));
                holder.btnFeedback.setEnabled(false);
            } else {
                holder.btnFeedback.setBackground(context.getResources().getDrawable(R.drawable.bg2));
                holder.btnFeedback.setEnabled(true);
            }

            holder.btnFeedback.setOnClickListener(v -> {
                context.startActivity(new Intent(context, UserfeedbackActivity.class)
                        .putExtra("projectid", projectid)
                        .putExtra("userid", list.get(position).getUser_id())
                );
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {

            TextView tvUserName, btnFeedback;

            public ViewHolder(View itemView) {
                super(itemView);
                tvUserName = itemView.findViewById(R.id.tvUserName);
                btnFeedback = itemView.findViewById(R.id.btnFeedback);
            }
        }
    }
}