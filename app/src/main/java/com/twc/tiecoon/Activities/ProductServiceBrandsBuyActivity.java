package com.twc.tiecoon.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.twc.tiecoon.Activities.PaymentModule.PaymentActivity;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Adapters.EnquiryReasonAdapter;
import com.twc.tiecoon.Adapters.ImageAdapter;
import com.twc.tiecoon.Adapters.SingleItemRatingAdapter;
import com.twc.tiecoon.Adapters.TagServiceUserAdapter;
import com.twc.tiecoon.Adapters.TagUserAdapter;
import com.twc.tiecoon.Adapters.UploadImageListAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.CustomMedia.JZMediaExo;
import com.twc.tiecoon.Helper.CircleIndicator;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.models.SingleItemRatingModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.CreateFeedbackData;
import com.twc.tiecoon.network.response.CreateReorganisationData;
import com.twc.tiecoon.network.response.CreateTestimonialData;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.network.response.EnquiryModel;
import com.twc.tiecoon.network.response.FeedbackModel;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.UploadImageListData;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.UserDetailTags;
import com.twc.tiecoon.network.response.ViewService;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.Utils;
import com.jsibbold.zoomage.ZoomageView;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Observable;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;
import de.hdodenhof.circleimageview.CircleImageView;
import me.relex.circleindicator.CircleIndicator2;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductServiceBrandsBuyActivity extends AbstractFragmentActivity implements View.OnClickListener {
    HomeModel homeModel = new HomeModel();
    private RelativeLayout relativeParent, relativeWaterMark;
    private ImageView image, iv_close, imgLike;
    private TextView tv_Category, tv_price, tvWatermark, tv_SubCategory, tvDes,tvLikeCount, tv_rating, tv_total_user_rating, textView_RateItem;
    private LinearLayout linearSubcategory, linear_like, linear_Tag, linear_share;
    private RatingBar rating_bar;
    private RecyclerView recyclerView_Feedback;
    private Button btnBuy;
    private SingleItemRatingAdapter singleItemRatingAdapter;
    private List<SingleItemRatingModel> singleItemRatingModelList = new ArrayList<>();
    private HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = null;
    private HomeUserDetailsProduct homeUserDetailsProduct = null;
    private HomeUserDetailsService homeUserDetailsService = null;
    private AddBrandsData addBrandsData = null;
    private List<UploadImageListData> uploadImageListData = new ArrayList<>();
    RecyclerView recyclerViewUploadImageList;
    private UploadImageListAdapter uploadImageListAdapter;
    MediaPlayer mp = new MediaPlayer();
    FrameLayout layoutAudio, layoutVideo;
    TextView tvTextArea, tvViews,tvEnquire;
    ImageView ivThumbnail, ivPlayButton;
    CardView cardView;
    Button btnPlayAudio, btnPauseAudio, btnResumeAudio;
    int length;
    JzvdStd jz_video;
    private ScaleGestureDetector scaleGestureDetector;
    private float mScaleFactor = 1.0f;
    ZoomageView myZoomageView;
    ViewPager vpBanner;
    CircleIndicator ciBanner;
    FrameLayout layoutViewpager;
    String talent = "",qbID="",liked="",name="";
    private boolean userLike = false;
    private boolean likeUnlikeChange = false;
    TextView tvTitle,tvTalentId;
    EnquiryReasonAdapter enquiryReasonAdapter;
    RecyclerView rvReasons;
    String Reason="",userid="",username="";
    RecyclerView rvTags;
    List<UserDetailTags> tagsList = new ArrayList<>();


    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_image_preview);

        if (getIntent().hasExtra("talent")) {
            talent = getIntent().getStringExtra("talent");

        }
        if (getIntent().hasExtra("serviceData")) {
            homeUserDetailsService = getIntent().getParcelableExtra("serviceData");
        }
        if (getIntent().hasExtra("talentData")) {
            homeUserDetailsUploadTalent = getIntent().getParcelableExtra("talentData");
        }
        if(getIntent().hasExtra("qbID")){
            qbID = getIntent().getStringExtra("qbID");
        }

        if (getIntent().hasExtra("productData")) {
            homeUserDetailsProduct = getIntent().getParcelableExtra("productData");
        }
        initView();
    }

    private void initView() {
        relativeParent = findViewById(R.id.relativeParent);
        iv_close = findViewById(R.id.iv_close);
        image = findViewById(R.id.image);
        tv_Category = findViewById(R.id.tv_Category);
        tv_price = findViewById(R.id.tv_price);
        tvWatermark = findViewById(R.id.tvWatermark);
        relativeWaterMark = findViewById(R.id.relativeWaterMark);
        tv_SubCategory = findViewById(R.id.tv_SubCategory);
        tvTalentId = findViewById(R.id.tvTalentId);
        linearSubcategory = findViewById(R.id.linearSubcategory);
        tvDes = findViewById(R.id.tvDes);
        tvLikeCount = findViewById(R.id.tvLikeCount);
        tvTitle = findViewById(R.id.tvTitle);
        imgLike = findViewById(R.id.imgLike);
        linear_like = findViewById(R.id.linear_like);
        linear_Tag = findViewById(R.id.linear_Tag);
        rvTags = findViewById(R.id.rvTags);
        linear_share = findViewById(R.id.linear_share);
        btnBuy = findViewById(R.id.btnBuy);
        btnPlayAudio = findViewById(R.id.btnPlayAudio);
        btnPauseAudio = findViewById(R.id.btnPauseAudio);
        btnResumeAudio = findViewById(R.id.btnResumeAudio);
        tv_rating = findViewById(R.id.tv_rating);
        rating_bar = findViewById(R.id.rating_bar);
        tv_total_user_rating = findViewById(R.id.tv_total_user_rating);
        tvViews = findViewById(R.id.tvViews);
        textView_RateItem = findViewById(R.id.textView_RateItem);
        recyclerView_Feedback = findViewById(R.id.recyclerView_Feedback);
        recyclerViewUploadImageList = findViewById(R.id.recyclerViewUploadImageList);
        layoutAudio = findViewById(R.id.layoutAudio);
        layoutVideo = findViewById(R.id.layoutVideo);
        tvTextArea = findViewById(R.id.tvTextArea);
        tvEnquire=findViewById(R.id.tvEnquire);
        ivPlayButton = findViewById(R.id.ivPlayButton);
        ivThumbnail = findViewById(R.id.ivThumbnail);
        cardView = findViewById(R.id.cardView);
        myZoomageView = findViewById(R.id.myZoomageView);
        vpBanner = findViewById(R.id.vpBanner);
        ciBanner = findViewById(R.id.ciBanner);
        layoutViewpager = findViewById(R.id.layoutViewpager);
        jz_video = findViewById(R.id.jz_video);
        scaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());

        iv_close.setOnClickListener(this);
        linear_like.setOnClickListener(this);
        linear_Tag.setOnClickListener(this);
        linear_share.setOnClickListener(this);
        btnBuy.setOnClickListener(this);
        textView_RateItem.setOnClickListener(this);
        singleItemRatingAdapter = new SingleItemRatingAdapter(this, singleItemRatingModelList);
        recyclerView_Feedback.setAdapter(singleItemRatingAdapter);
        uploadImageListAdapter = new UploadImageListAdapter(this, uploadImageListData);
        recyclerViewUploadImageList.setAdapter(uploadImageListAdapter);
        setItemsData();
        PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
        pagerSnapHelper.attachToRecyclerView(recyclerViewUploadImageList);
        rvTags.setHasFixedSize(true);
        rvTags.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
        linear_Tag.setOnClickListener(v ->{
            if(tagsList.size()>0){
                rvTags.setVisibility(View.VISIBLE);
            } else Toast.makeText(this, "No Tagged Users", Toast.LENGTH_SHORT).show();

        });
        CircleIndicator2 indicator = findViewById(R.id.indicator);
        indicator.attachToRecyclerView(recyclerViewUploadImageList, pagerSnapHelper);
        try {
            mp.setDataSource(Constant.ImageURL + homeUserDetailsUploadTalent.getAudio());
        } catch (IOException e) {
            e.printStackTrace();
        }

        tvTitle.setOnClickListener(v -> {
            Intent i = new Intent(this, OtherUserProfileActivity.class);
            i.putExtra("UserId",userid);
            startActivity(i);
        });

        btnPlayAudio.setOnClickListener(view -> {
            btnPauseAudio.setVisibility(View.VISIBLE);
            btnResumeAudio.setVisibility(View.GONE);
            btnPlayAudio.setVisibility(View.GONE);

            mp.setOnCompletionListener(mp -> {
                mp.stop();
                btnPauseAudio.setVisibility(View.GONE);
                btnPlayAudio.setVisibility(View.VISIBLE);

            });

            try {

                mp.prepare();
                mp.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        btnPauseAudio.setOnClickListener(view -> {
            btnPauseAudio.setVisibility(View.GONE);
            btnResumeAudio.setVisibility(View.VISIBLE);
            btnPlayAudio.setVisibility(View.GONE);

            mp.pause();
            length = mp.getCurrentPosition();
        });

        btnResumeAudio.setOnClickListener(view -> {
            btnPauseAudio.setVisibility(View.VISIBLE);
            btnResumeAudio.setVisibility(View.GONE);
            btnPlayAudio.setVisibility(View.GONE);

            if (!mp.isPlaying()) {
                mp.seekTo(length);
                mp.start();
            }
        });

        ivPlayButton.setOnClickListener(view -> {
            saveVideo();
            ivThumbnail.setVisibility(View.GONE);
            ivPlayButton.setVisibility(View.GONE);
            jz_video.startVideo();
        });

        ivThumbnail.setOnClickListener(view -> {
            saveVideo();
            ivThumbnail.setVisibility(View.GONE);
            ivPlayButton.setVisibility(View.GONE);
            jz_video.startVideo();
        });

        tvEnquire.setOnClickListener(v ->EnquireFirst() );
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));
            image.setScaleX(mScaleFactor);
            image.setScaleY(mScaleFactor);
            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        scaleGestureDetector.onTouchEvent(motionEvent);
        return true;
    }

    private void EnquireFirst() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.enquire_layout);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        EditText editText =dialog.findViewById(R.id.editText);
        Button btnSave =dialog.findViewById(R.id.btnSave);
        Button btnCancle =dialog.findViewById(R.id.btnCancle);
        rvReasons = dialog.findViewById(R.id.rvReasons);
        rvReasons.setHasFixedSize(true);
        rvReasons.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));

        imgCancel.setOnClickListener(v -> dialog.dismiss());
        btnCancle.setOnClickListener(v -> dialog.dismiss());
        getEnquiryList();
        btnSave.setOnClickListener(v -> {
            dialog.dismiss();
            ArrayList<Integer> occupantIdsList = new ArrayList<Integer>();
            occupantIdsList.add(Integer.valueOf(qbID));

            QBChatDialog qbChatDialog = new QBChatDialog();
            qbChatDialog.setType(QBDialogType.PRIVATE);
            qbChatDialog.setOccupantsIds(occupantIdsList);


            QBRestChatService.createChatDialog(qbChatDialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                @Override
                public void onSuccess(QBChatDialog result, Bundle params) {
                    Log.e("result",result.toString());
                    Log.e("ReasonList",ApplicationClass.ReasonList.toString());
                    StringBuilder string = new StringBuilder();
                    for (Object str : ApplicationClass.ReasonList ) {
                        string.append(str.toString()+"\n");
                    }
                    String singleString = string.toString();
                    ChatActivity.startForResult(ProductServiceBrandsBuyActivity.this,101, result,username,editText.getText().toString(),singleString);
                }

                @Override
                public void onError(QBResponseException responseException) {
                    Toast.makeText(ProductServiceBrandsBuyActivity.this, "ERRROR", Toast.LENGTH_SHORT).show();
                }
            });
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @SuppressLint("SetTextI18n")
    private void setItemsData() {
        if (homeUserDetailsUploadTalent != null) {
            if (!(ApplicationClass.uploadImageListDataArrayList.isEmpty())) {
                uploadImageListData.addAll(ApplicationClass.uploadImageListDataArrayList);
                uploadImageListAdapter.notifyDataSetChanged();
            }
            if (homeUserDetailsUploadTalent.getUserId() == ApplicationClass.appPreferences.getUserId()) {
                btnBuy.setVisibility(View.GONE);
                tvEnquire.setVisibility(View.GONE);
                linear_like.setVisibility(View.VISIBLE);
                if (homeUserDetailsUploadTalent.getTalant_count() != null) {
                    tvLikeCount.setText(homeUserDetailsUploadTalent.getTalant_count());
                }
            }
            if (homeUserDetailsUploadTalent.getCategory() != null && homeUserDetailsUploadTalent.getCategory().getTitle() != null) {
                tv_Category.setText(homeUserDetailsUploadTalent.getCategory().getTitle());
            } else {
                tv_Category.setText("");
            }

          //  Toast.makeText(this, homeUserDetailsUploadTalent.getFontsize().toString(), Toast.LENGTH_SHORT).show();
            if (homeUserDetailsUploadTalent.getSubCategory() != null && homeUserDetailsUploadTalent.getSubCategory().getTitle() != null) {
                tv_SubCategory.setText(homeUserDetailsUploadTalent.getSubCategory().getTitle());
            } else {
                tv_SubCategory.setVisibility(View.GONE);
            }
          if (homeUserDetailsUploadTalent.getTags() != null) {
              tagsList = homeUserDetailsUploadTalent.getTags();
              TagUserAdapter tagUserAdapter = new TagUserAdapter(this, tagsList, new ItemClickListener() {
                  @Override
                  public void itemClick(int pos) {
                      Intent i = new Intent(ProductServiceBrandsBuyActivity.this, OtherUserProfileActivity.class);
                      i.putExtra("UserId", String.valueOf(tagsList.get(pos).getUser_id()));
                      startActivity(i);
                  }
              }) ;
              rvTags.setAdapter(tagUserAdapter);
            }
            if (homeUserDetailsUploadTalent.getDescription() != null) {
                tvDes.setText(homeUserDetailsUploadTalent.getDescription());
            }

            if (homeUserDetailsUploadTalent.getTalentid() != null) {
                tvTalentId.setText("Talent ID: T"+homeUserDetailsUploadTalent.getTalentid());
            }
            tvTitle.setText(homeUserDetailsUploadTalent.getOwnername());
            userid = homeUserDetailsUploadTalent.getUserId()+"";
            username = homeUserDetailsUploadTalent.getOwnername();
//            Toast.makeText(this, String.valueOf(homeUserDetailsUploadTalent.isIs_like()), Toast.LENGTH_SHORT).show();
            Log.d("islike", homeUserDetailsUploadTalent.getFontsize() + "___");
            Log.d("islike", homeUserDetailsUploadTalent.getWatermark() + "___");
            Log.d("islike", homeUserDetailsUploadTalent.getCreateimage() + "___");

//            if (talent.equals("true")) {
//                imgLike.setImageResource(R.drawable.ic_like_solid);
//            } else {
//                imgLike.setImageResource(R.drawable.ic_like);
//            }

//            if (homeUserDetailsUploadTalent.getIs_like()) {
//                imgLike.setImageResource(R.drawable.ic_like_solid);
//            } else {
//                imgLike.setImageResource(R.drawable.ic_like);
//            }

            if (homeUserDetailsUploadTalent.getFontsize().equals("true")) {
                imgLike.setImageResource(R.drawable.ic_like_solid);
                liked = "1";
            } else {
                imgLike.setImageResource(R.drawable.ic_like);
                liked = "0";
            }
            if (homeUserDetailsUploadTalent.getPrice() != null) {
                String pricecode;
                if (homeUserDetailsUploadTalent.getPricecode().equals("USD")) {
                    pricecode = "$";
                } else pricecode = "₹";
                tv_price.setText(new StringBuilder().append("").append(pricecode).append(" ").append(homeUserDetailsUploadTalent.getPrice()));
            }
            feedbackList(homeUserDetailsUploadTalent.getId()+"");
            if (homeUserDetailsUploadTalent.getTalant_type() != null) {
                switch (homeUserDetailsUploadTalent.getTalant_type()) {
                    case "image":
                        layoutAudio.setVisibility(View.GONE);
                        tvViews.setVisibility(View.GONE);
                        layoutVideo.setVisibility(View.GONE);
                        tvTextArea.setVisibility(View.GONE);
                        if (uploadImageListData.size() > 0) {
                            myZoomageView.setVisibility(View.GONE);
                            layoutViewpager.setVisibility(View.VISIBLE);
                            ImageAdapter bannerAdapter = new ImageAdapter(this, uploadImageListData);
                            vpBanner.setAdapter(bannerAdapter);
                            ciBanner.setViewPager(vpBanner);
                        } else {
                            myZoomageView.setVisibility(View.VISIBLE);
                            layoutViewpager.setVisibility(View.GONE);
                            String imageURL = Constant.ImageURL + homeUserDetailsUploadTalent.getPath() + "/" + homeUserDetailsUploadTalent.getCreateimage();
                            Glide.with(this).load(imageURL).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(myZoomageView);
                        }
                        break;
                    case "audio":
                        myZoomageView.setVisibility(View.GONE);
                        layoutViewpager.setVisibility(View.GONE);
                        layoutAudio.setVisibility(View.VISIBLE);
                        btnPlayAudio.setVisibility(View.VISIBLE);
                        layoutVideo.setVisibility(View.GONE);
                        tvViews.setVisibility(View.GONE);
                        tvTextArea.setVisibility(View.GONE);
                        break;
                    case "video":
                        myZoomageView.setVisibility(View.GONE);
                        layoutViewpager.setVisibility(View.GONE);
                        layoutAudio.setVisibility(View.GONE);
                        layoutVideo.setVisibility(View.VISIBLE);
                        tvViews.setVisibility(View.VISIBLE);
                        tvTextArea.setVisibility(View.GONE);
                        if (homeUserDetailsUploadTalent.getVideothumbnail() != null) {
                            String imageURL1 = Constant.ImageURL + "/" + homeUserDetailsUploadTalent.getVideothumbnail();
                            Glide.with(this).load(imageURL1).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(ivThumbnail);
                        }
                        jz_video.setUp(Constant.ImageURL + homeUserDetailsUploadTalent.getVideothumbnail(), "", JzvdStd.SCREEN_NORMAL, JZMediaExo.class);
                        Log.e("vdo",Constant.ImageURL + homeUserDetailsUploadTalent.getVideo());
//                        admin\/upload\/talents\/video\/1611941647vd.mp4
                        if (homeUserDetailsUploadTalent.getTalant_see() != null) {
                            tvViews.setText(homeUserDetailsUploadTalent.getTalant_see() + " Views");
                        }
                        break;
                    case "textarea":
                        myZoomageView.setVisibility(View.GONE);
                        layoutViewpager.setVisibility(View.GONE);
                        layoutAudio.setVisibility(View.GONE);
                        layoutVideo.setVisibility(View.GONE);
                        tvViews.setVisibility(View.GONE);
                        tvTextArea.setVisibility(View.VISIBLE);
                        relativeWaterMark.setVisibility(View.GONE);
                        tvWatermark.setVisibility(View.VISIBLE);
                        relativeWaterMark.setVisibility(View.VISIBLE);

                        if (homeUserDetailsUploadTalent.getTextarea() != null) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                tvWatermark.setText(Html.fromHtml(homeUserDetailsUploadTalent.getTextarea(), Html.FROM_HTML_MODE_COMPACT));
                            }
                            else tvWatermark.setText(Html.fromHtml(homeUserDetailsUploadTalent.getTextarea()));
                        }
                        Drawable drawable = relativeWaterMark.getBackground();

                        drawable.setTint(Color.parseColor(homeUserDetailsUploadTalent.getFontcolor()));
                        relativeWaterMark.setBackground(drawable);
                        break;
                }
            }


        }
        else if (homeUserDetailsProduct != null) {
            if (homeUserDetailsProduct.getCategory() != null && homeUserDetailsProduct.getCategory().getTitle() != null) {
                tv_Category.setText(homeUserDetailsProduct.getCategory().getTitle());
            } else {
                tv_Category.setVisibility(View.GONE);
            }
            linearSubcategory.setVisibility(View.GONE);

            if (homeUserDetailsProduct.getDescription() != null) {
                tvDes.setText(homeUserDetailsProduct.getDescription());

            }
//            Log.d("islike", homeUserDetailsUploadTalent.isIs_like() + "___");
//            if (homeUserDetailsUploadTalent.isIs_like()) {
//                imgLike.setImageResource(R.drawable.ic_like_solid);
//            } else {
//                imgLike.setImageResource(R.drawable.ic_like);
//            }

            if (homeUserDetailsProduct.getPrice() != null) {
                tv_price.setText(new StringBuilder().append("Price: ").append(homeUserDetailsProduct.getPricecode()).append(" ").append(homeUserDetailsProduct.getPrice()));
            }

            if (!(ApplicationClass.uploadImageListDataArrayList1.isEmpty())) {
                uploadImageListData.addAll(ApplicationClass.uploadImageListDataArrayList1);
                uploadImageListAdapter.notifyDataSetChanged();
            }
            if (homeUserDetailsProduct.getUpload_image().get(0).getCreateimage() != null) {
                switch (homeUserDetailsProduct.getUpload_image().get(0).getCreateimage()) {
                    case "image":
                        layoutAudio.setVisibility(View.GONE);
                        tvViews.setVisibility(View.GONE);
                        layoutVideo.setVisibility(View.GONE);
                        tvTextArea.setVisibility(View.GONE);
                        if (uploadImageListData.size() > 0) {
                            myZoomageView.setVisibility(View.GONE);
                            layoutViewpager.setVisibility(View.VISIBLE);
                            ImageAdapter bannerAdapter = new ImageAdapter(this, uploadImageListData);
                            vpBanner.setAdapter(bannerAdapter);
                            ciBanner.setViewPager(vpBanner);
                        } else {
                            myZoomageView.setVisibility(View.VISIBLE);
                            layoutViewpager.setVisibility(View.GONE);
                            String imageURL = Constant.ImageURL + homeUserDetailsProduct.getPath() + "/" + homeUserDetailsProduct.getCreateimage();
                            Log.e("imageee",homeUserDetailsProduct.getUpload_image().get(0).getImage());
                            Glide.with(this).load(homeUserDetailsProduct.getUpload_image().get(0).getImage()).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(myZoomageView);
                        }
                        break;
                     case "video":
                        myZoomageView.setVisibility(View.GONE);
                        layoutViewpager.setVisibility(View.GONE);
                        layoutAudio.setVisibility(View.GONE);
                        layoutVideo.setVisibility(View.VISIBLE);
                        tvViews.setVisibility(View.VISIBLE);
                        tvTextArea.setVisibility(View.GONE);
                        if (homeUserDetailsProduct.getUpload_image().get(0).getVideo_thumbnail() != null) {
                            String imageURL1 = Constant.ImageURL +  "admin/upload/product/" + homeUserDetailsProduct.getUpload_image().get(0).getVideo_thumbnail() ;
                            Glide.with(this).load(imageURL1).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(ivThumbnail);
                        }
                        break;
                }
            }
        }

        }

    public class TagUserAdapter extends RecyclerView.Adapter<TagUserAdapter.MyViewHolder> {

        private final List<UserDetailTags> tagsList;
        Context context;
        private ItemClickListener itemClickListener;
        public TagUserAdapter(Context context, List<UserDetailTags> tagsList,
                                     ItemClickListener itemClickListener) {
            this.context=context;
            this.tagsList=tagsList;
            this.itemClickListener=itemClickListener;

        }

        @NotNull
        @Override
        public TagUserAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_tag, parent, false);
            return new TagUserAdapter.MyViewHolder(itemView);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull TagUserAdapter.MyViewHolder holder, int position) {


            holder.tv_Tag.setText(tagsList.get(position).getUsername());
            holder.tv_Tag.setOnClickListener(v -> itemClickListener.itemClick(position));




        }
        @Override
        public int getItemCount() {
            return tagsList.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            TextView tv_Tag;
            MyViewHolder(View view) {
                super(view);
                tv_Tag = view.findViewById(R.id.tv_Tag);
            }

        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
//                if (Jzvd.backPress()) {
//                    return;
//                }
                onBackPressed();
//                startActivity(new Intent(this, UserHomeActivity.class));
              //  mp.stop();
                break;
            case R.id.linear_like:
                setItemLike();
                break;
            case R.id.linear_Tag:
                //to do
                break;
            case R.id.linear_share:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Tiecoon");
                    String message = "https://play.google.com/store/apps/details?id=" + "com.twc.tiecoon" + "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, message);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.textView_RateItem:
                rateItemDialog();
                break;
                case R.id.btnBuy:
                    if(Utils.isDeviceOnline(this)){
                        startActivity(new Intent(this, PaymentActivity.class).
                                putExtra("id",homeUserDetailsUploadTalent.getId()+"").
                                putExtra("qbID",qbID)
                                .putExtra("type","talent")
                                .putExtra("amount",homeUserDetailsUploadTalent.getPrice()+"")
                                .putExtra("currency",homeUserDetailsUploadTalent.getPricecode()+"")
                        );
                    }
                    else Toast.makeText(this, "No Internet", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onBackPressed() {

//        if (Jzvd.backPress()) {
//
//           // finish();
//
//            return;
//        }
        if(mp.isPlaying()){
            mp.stop();
        }
        //9653908417
        Intent data = getIntent();
        data.putExtra("key", liked);
        setResult(RESULT_OK, data);
        finish();
//        super.onBackPressed();
//        mp.stop();

    }

    @Override
    protected void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
        mp.stop();
    }


    private void setItemLike() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            if (homeUserDetailsProduct != null) {
                homeModel.saveProduct(this, relativeParent, homeUserDetailsProduct.getId());
            } else if (homeUserDetailsService != null) {
                homeModel.saveService(this, relativeParent, homeUserDetailsService.getId());
            } else if (homeUserDetailsUploadTalent != null) {
                userLike = true;
                homeModel.saveTalents(this, relativeParent, homeUserDetailsUploadTalent.getId());
            } else if (addBrandsData != null) {
                homeModel.saveBrands(this, relativeParent, addBrandsData.getId());
            }
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    private void saveVideo() {
        if (Utils.isDeviceOnline(this)) {
            homeModel.addVideo(this, relativeParent, homeUserDetailsUploadTalent.getId(), homeUserDetailsUploadTalent.getUserId());
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }




    private void rateItemDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.item_rating_dialog);

        RatingBar rating_bar = dialog.findViewById(R.id.rating_bar);
        EditText editText_Description = dialog.findViewById(R.id.editText_Description);
        TextView textView_Cancel = dialog.findViewById(R.id.textView_Cancel);
        Button buttonSave = dialog.findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(view -> {
            if (rating_bar.getRating() == 0.0) {
                Toast.makeText(this, "Rating can't be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            if (editText_Description.getText().toString().trim().isEmpty()) {
                Toast.makeText(this, "Description can't be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            dialog.dismiss();
            setRatingToItem(editText_Description.getText().toString(), rating_bar.getRating());
        });
        textView_Cancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void setRatingToItem(String des, float rating) {
        if (homeUserDetailsUploadTalent != null) {
            if (Utils.isDeviceOnline(this)) {
                Utils.showProDialog(this);
                homeModel.createFeedback(this, relativeParent, homeUserDetailsUploadTalent.getId(),
                        String.valueOf(rating), des);
            } else {
                Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
            }
        } else if (homeUserDetailsProduct != null) {
            if (Utils.isDeviceOnline(this)) {
                Utils.showProDialog(this);
                homeModel.createTestimonial(this, relativeParent, homeUserDetailsProduct.getId(),
                        String.valueOf(rating), des);
            } else {
                Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
            }
        } else if (homeUserDetailsService != null) {
            if (Utils.isDeviceOnline(this)) {
                Utils.showProDialog(this);
                homeModel.createTestimonial(this, relativeParent, homeUserDetailsService.getId(),
                        String.valueOf(rating), des);
            } else {
                Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
            }
        } else if (addBrandsData != null) {
            if (Utils.isDeviceOnline(this)) {
                Utils.showProDialog(this);
                homeModel.createReorganisation(this, relativeParent, addBrandsData.getId(),
                        String.valueOf(rating), des);
            } else {
                Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
            }
        }
    }
    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof CreateFeedbackData) {
            textView_RateItem.setVisibility(View.GONE);
            CreateFeedbackData createFeedbackData = (CreateFeedbackData) o;
            String picURL = ApplicationClass.appPreferences.getUserPicUrl();
            String name = ApplicationClass.appPreferences.getUserName();
            SingleItemRatingModel singleItemRatingModel = new SingleItemRatingModel(picURL, name, createFeedbackData.getRating(),
                    createFeedbackData.getMassage());
            singleItemRatingModelList.add(singleItemRatingModel);
            singleItemRatingAdapter.notifyDataSetChanged();
        } else if (o instanceof CreateTestimonialData) {
            textView_RateItem.setVisibility(View.GONE);
            CreateTestimonialData createFeedbackData = (CreateTestimonialData) o;
            String picURL = ApplicationClass.appPreferences.getUserPicUrl();
            String name = ApplicationClass.appPreferences.getUserName();
            SingleItemRatingModel singleItemRatingModel = new SingleItemRatingModel(picURL, name, createFeedbackData.getRating(),
                    createFeedbackData.getMassage());
            singleItemRatingModelList.add(singleItemRatingModel);
            singleItemRatingAdapter.notifyDataSetChanged();
        } else if (o instanceof DeleteTalents) {
            DeleteTalents deleteTalents = (DeleteTalents) o;
            if (deleteTalents.getMessage().equals("Talent unlike successfully")) {
                imgLike.setImageResource(R.drawable.ic_like);
                liked = "0";

            } else {
                imgLike.setImageResource(R.drawable.ic_like_solid);
                liked = "1";
            }
        } else if (o instanceof CreateReorganisationData) {
            textView_RateItem.setVisibility(View.GONE);
            CreateReorganisationData createFeedbackData = (CreateReorganisationData) o;
            String picURL = ApplicationClass.appPreferences.getUserPicUrl();
            String name = ApplicationClass.appPreferences.getUserName();
            SingleItemRatingModel singleItemRatingModel = new SingleItemRatingModel(picURL, name, createFeedbackData.getRating(),
                    createFeedbackData.getMassage());
            singleItemRatingModelList.add(singleItemRatingModel);
            singleItemRatingAdapter.notifyDataSetChanged();
        } else if (o instanceof String) {
            String data = (String) o;
            switch (data) {
                case "productLiked":
                    imgLike.setImageResource(R.drawable.ic_like_solid);
                    break;

                case "serviceLiked":
                    imgLike.setImageResource(R.drawable.ic_like_solid);
                    break;

                case "brandLiked":
                    imgLike.setImageResource(R.drawable.ic_like_solid);
                    break;
            }
        }

        else if (o instanceof EnquiryModel) {
            EnquiryModel enquiryModel = (EnquiryModel) o;
            List<EnquiryModel.Data> enquiryModelList = enquiryModel.getData();
            enquiryReasonAdapter = new EnquiryReasonAdapter(this,enquiryModelList,pos -> {
                Reason = enquiryModelList.get(pos).getName()+"\n";
                Log.e("Reason",Reason);
            }) ;
            rvReasons.setAdapter(enquiryReasonAdapter);
        }
    }

    private void getEnquiryList() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getTalentEnquiryList(this, relativeParent);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        ApplicationClass.ReasonList.clear();
    }

    public void feedbackList(String id) {
        final Dialog dialog = new Dialog(ProductServiceBrandsBuyActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<FeedbackModel> call = apiInterface.feedbackList("Bearer "+accessToken,id,"talent");
        call.enqueue(new Callback<FeedbackModel>() {
            @Override
            public void onResponse(Call<FeedbackModel> call, Response<FeedbackModel> response) {
                dialog.dismiss();
                FeedbackModel user = response.body();
                assert user != null;
                if(user.getData().size()>0){
                    recyclerView_Feedback.setVisibility(View.VISIBLE);
                    FeedbackAdapter feedbackAdapter = new FeedbackAdapter(ProductServiceBrandsBuyActivity.this,user.getData(), pos -> {
                    });
                    recyclerView_Feedback.setAdapter(feedbackAdapter);
                }
                else recyclerView_Feedback.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<FeedbackModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public static class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {
        private Context context;
        private List<FeedbackModel.Data> list;
        private ItemClickListener itemClickListener;
        public FeedbackAdapter(Context context,List<FeedbackModel.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }
        @Override
        public FeedbackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.buying_items_user_rating_item_layout, parent, false);
            return new FeedbackAdapter.ViewHolder(listItem);
        }
        @Override
        public void onBindViewHolder(FeedbackAdapter.ViewHolder holder, final int position) {
            Glide.with(context).load(list.get(position).getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.imgProfile);
            holder.tv_UserName.setText(list.get(position).getUsername());
            holder.simpleRatingBar.setText(String.valueOf(list.get(position).getRating()));
            holder.textViewDescription.setText(list.get(position).getMassage());
            holder.rating_feedback.setRating(Float.parseFloat(String.valueOf(list.get(position).getRating())));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            CircleImageView imgProfile;
            TextView tv_UserName,simpleRatingBar,textViewDescription;
            RatingBar rating_feedback;
            public ViewHolder(View itemView) {
                super(itemView);
                imgProfile = itemView.findViewById(R.id.imgProfile);
                tv_UserName = itemView.findViewById(R.id.tv_UserName);
                simpleRatingBar = itemView.findViewById(R.id.simpleRatingBar);
                rating_feedback = itemView.findViewById(R.id.rating_feedback);
                textViewDescription = itemView.findViewById(R.id.textViewDescription);

            }
        }
    }

}