package com.twc.tiecoon.Activities;


import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.twc.tiecoon.Adapters.AddBrandsAdapters;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.SavedBrands;
import com.twc.tiecoon.network.response.SavedBrandsData;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;


public class OtherUserSavedBrandsActivity extends AbstractFragmentActivity {
    private HomeModel homeModel = new HomeModel();
    private LinearLayout linearParent;
    private ArrayList<AddBrandsData> uploadBrandsList = new ArrayList<>();
    private AddBrandsAdapters addBrandsAdapters;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_other_user_saved_brands);
        ImageView ivBack = findViewById(R.id.ivBack);
        linearParent = findViewById(R.id.linearParent);
        RecyclerView recyclerViewSavedBrands = findViewById(R.id.recyclerViewSavedBrands);
        ivBack.setOnClickListener(view -> onBackPressed());
        addBrandsAdapters = new AddBrandsAdapters(this, uploadBrandsList, (pos, type) -> {
            Intent i = new Intent(OtherUserSavedBrandsActivity.this, ProductServiceBrandsBuyActivity.class);
            i.putExtra("brandData", uploadBrandsList.get(pos));
            startActivity(i);
        });
        recyclerViewSavedBrands.setAdapter(addBrandsAdapters);
        getSavedBrands();
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    @Override
    public void update(Observable observable, Object o) {
        SavedBrands savedBrands = (SavedBrands) o;
        List<SavedBrandsData> savedBrandsDataList = savedBrands.getData();
        if (savedBrandsDataList != null && !(savedBrandsDataList.isEmpty())) {
            for (SavedBrandsData savedBrandsData : savedBrandsDataList) {
                if (savedBrandsData != null && savedBrandsData.getBrand() != null) {
                    uploadBrandsList.add(savedBrandsData.getBrand());
                }
            }
            addBrandsAdapters.notifyDataSetChanged();
        } else {
            Toast.makeText(this, "No saved brands", Toast.LENGTH_SHORT).show();
        }
    }

    private void getSavedBrands() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getSavedBrandsList(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

}