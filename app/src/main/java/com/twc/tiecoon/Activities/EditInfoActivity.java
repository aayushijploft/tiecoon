package com.twc.tiecoon.Activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.Adapters.AddAssignmentAdapters;
import com.twc.tiecoon.Adapters.AddBrandsAdapters;
import com.twc.tiecoon.Adapters.AddProductAdapters;
import com.twc.tiecoon.Adapters.AddServiceAdapters;
import com.twc.tiecoon.Adapters.AddTalentAdapters;
import com.twc.tiecoon.Adapters.PreferedLangAdapter;
import com.twc.tiecoon.Adapters.SelectedPreferedLanguageAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.network.response.EditInfo;
import com.twc.tiecoon.network.response.HomeUserDetail;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.HomeUserDetailsUserDetils;
import com.twc.tiecoon.network.response.HomeUserPreferedLanguage;
import com.twc.tiecoon.network.response.PreferredLanguage;
import com.twc.tiecoon.network.response.PreferredLanguageData;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;
import com.twc.tiecoon.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Observable;

public class EditInfoActivity extends AbstractFragmentActivity implements View.OnClickListener, TextWatcher {
    private List<PreferredLanguageData> selectedLanguageDataList = new ArrayList<>();
    private List<PreferredLanguageData> preferredLanguageDataList = new ArrayList<>();
    private List<PreferredLanguageData> preferredLanguageDataList2 = new ArrayList<>();
    private ProfileModel profileModel = new ProfileModel();
    private PreferedLangAdapter preferedLangAdapter;
    private SelectedPreferedLanguageAdapter selectedPreferedLanguageAdapter;
    private LinearLayout linearParent, ll_fullTime, ll_PartTime, ll_male, ll_female, ll_other,ll_Public,ll_Private;
    private ImageView ivBack, imgFullTime, imgPartTime, imgMale, imgfemale, imgOther,imgPublic,imgPrivate;
    private EditText et_status, et_livesIn, et_phone,et_email,et_name,et_bio,et_response;
    private Button btnSubmit;
    private CardView cardViewPreferredLang;
    private Dialog preferredlangDialog;
    private RecyclerView recyclerView_selected_preferedLang;
    private String gender = "";
    private String workType = "";
    private String PhoneType = "";
    Spinner spinnerStatus;
    private String[] statustype={"Online","Away","Inactive"};

    //add talents and assignments
    LinearLayout linear_add_talents,linear_add_assignments;
    public static final int UPLOAD_TALENT = 201;
    private static final int CREATEASSGN = 202;
    private boolean updateTalent = false;
    private boolean updateAssign = false;
    private List<ProfileTalent> addTalentDataList = new ArrayList<>();
    private ArrayList<HomeUserDetailsUploadTalent> addTalentsList = new ArrayList<>();
    private ArrayList<ProfileCreateAssignment> addAssignmentList = new ArrayList<>();
    private AddTalentAdapters addTalentAdapters;
    private AddAssignmentAdapters addAssignmentAdapters;
    private int deleteAssignments;
    private int deleteTalents;
    private RecyclerView recyclerView_add_talents, recyclerView_add_assignments, recyclerView_selected_talents;

    //add products and services
    private LinearLayout linear_upload_products, linear_upload_services;
    public static final int UPLOAD_PRODUCTS = 203;
    public static final int UPLOAD_SERVICES = 204;
    private ArrayList<HomeUserDetailsProduct> uploadProductList = new ArrayList<>();
    private ArrayList<HomeUserDetailsService> uploadServiceList = new ArrayList<>();
    private AddProductAdapters addProductAdapters;
    private AddServiceAdapters addServiceAdapters;
    private RecyclerView recyclerView_upload_product;
    private int updateProductPos;
    private int updateServicePos;
    private boolean updateProduct = false;
    private boolean updateService = false;

    //add brands
    private ArrayList<AddBrandsData> uploadBrandsList = new ArrayList<>();
    private AddBrandsAdapters addBrandsAdapters;
    private LinearLayout linear_upload_brands;
    public static final int UPLOAD_BRANDS = 2052;
    private RecyclerView recyclerView_upload_brands;
    private int updateBrandPos;
    private boolean uploadsBrands = false;
    TextView et_dob;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_edit_info);
        initView();
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void initView() {
        linearParent = findViewById(R.id.linearParent);
        ivBack = findViewById(R.id.ivBack);
        et_status = findViewById(R.id.et_status);
        et_email = findViewById(R.id.et_email);
        et_name = findViewById(R.id.et_name);
        et_bio = findViewById(R.id.et_bio);
        ll_fullTime = findViewById(R.id.ll_fullTime);
        ll_PartTime = findViewById(R.id.ll_PartTime);
        imgFullTime = findViewById(R.id.imgFullTime);
        imgPartTime = findViewById(R.id.imgPartTime);
        cardViewPreferredLang = findViewById(R.id.cardViewPreferredLang);
        et_livesIn = findViewById(R.id.et_livesIn);
        recyclerView_selected_preferedLang = findViewById(R.id.recyclerView_selected_preferedLang);
        imgMale = findViewById(R.id.imgMale);
        imgfemale = findViewById(R.id.imgFemale);
        imgOther = findViewById(R.id.imgOther);
        ll_male = findViewById(R.id.ll_male);
        ll_female = findViewById(R.id.ll_female);
        ll_other = findViewById(R.id.ll_other);
        et_phone = findViewById(R.id.et_phone);
        et_response = findViewById(R.id.et_response);
        et_dob = findViewById(R.id.et_dob);
        btnSubmit = findViewById(R.id.btnSubmit);
        spinnerStatus = findViewById(R.id.spinnerStatus);
        linear_add_talents = findViewById(R.id.linear_add_talents);
        linear_add_assignments = findViewById(R.id.linear_add_assignments);
        recyclerView_add_talents = findViewById(R.id.recyclerView_add_talents);
        recyclerView_add_assignments = findViewById(R.id.recyclerView_add_assignments);
        ll_Private = findViewById(R.id.ll_Private);
        ll_Public = findViewById(R.id.ll_Public);
        imgPublic = findViewById(R.id.imgPublic);
        imgPrivate = findViewById(R.id.imgPrivate);

        //add products and services
        linear_upload_products = findViewById(R.id.linear_upload_products);
        linear_upload_services = findViewById(R.id.linear_upload_services);
        recyclerView_upload_product = findViewById(R.id.recyclerView_upload_product);
        RecyclerView recyclerView_upload_services = findViewById(R.id.recyclerView_upload_services);
        linear_upload_products.setOnClickListener(this);
        linear_upload_services.setOnClickListener(this);

        //add brands
        linear_upload_brands = findViewById(R.id.linear_upload_brands);
        recyclerView_upload_brands = findViewById(R.id.recyclerView_upload_brands);
        linear_upload_brands.setOnClickListener(this);

        ivBack.setOnClickListener(this);
        ll_fullTime.setOnClickListener(this);
        ll_PartTime.setOnClickListener(this);
        ll_Public.setOnClickListener(this);
        ll_Private.setOnClickListener(this);
        cardViewPreferredLang.setOnClickListener(this);
        ll_male.setOnClickListener(this);
        ll_female.setOnClickListener(this);
        ll_other.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        et_dob.setOnClickListener(view -> openCalender(et_dob));

        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, statustype);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinnerStatus.setAdapter(arrayAdapter);

        linear_add_talents.setOnClickListener(view -> {
            updateTalent = false;
            Intent i = new Intent(EditInfoActivity.this, WatermarkActivity.class);
            i.putExtra("type", "talent");
            startActivityForResult(i, UPLOAD_TALENT);
        });

        linear_add_assignments.setOnClickListener(view -> {
            updateAssign = false;
            Intent intent1 = new Intent(EditInfoActivity.this, CreateAssignentNew.class);
            startActivityForResult(intent1, CREATEASSGN);
        });
        selectedPreferedLanguageAdapter = new SelectedPreferedLanguageAdapter(this, selectedLanguageDataList, new ItemClickListener() {
            @Override
            public void itemClick(int pos) {
                selectedLanguageDataList.remove(pos);
                selectedPreferedLanguageAdapter.notifyItemRemoved(pos);
                selectedPreferedLanguageAdapter.notifyItemRangeChanged(pos, selectedLanguageDataList.size());
            }
        });
        recyclerView_selected_preferedLang.setAdapter(selectedPreferedLanguageAdapter);
        addTalentAdapters = new AddTalentAdapters(this, addTalentsList, new ItemClickListenerExtraParam() {
            @Override
            public void itemClick(int pos, String type) {
                deleteTalents = pos;
                if (type.equals("update")) {
                    updateTalent = true;
                    HomeUserDetailsUploadTalent homeUserDetailsUploadTalent=addTalentsList.get(pos);
                    ApplicationClass.uploadImageListDataArrayList.clear();
                    if (homeUserDetailsUploadTalent.getUpload_image() != null && !(homeUserDetailsUploadTalent.getUpload_image().isEmpty())) {
                        ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsUploadTalent.getUpload_image());
                    }
                    Intent i = new Intent(EditInfoActivity.this, WatermarkActivity.class);
                    i.putExtra("type", "talent");
                    i.putExtra("updateItem", addTalentsList.get(pos));
                    startActivityForResult(i, UPLOAD_TALENT);
                } else {
                    removeTalent(addTalentsList.get(pos));
                }
            }
        });
        recyclerView_add_talents.setAdapter(addTalentAdapters);

        addAssignmentAdapters = new AddAssignmentAdapters(this, addAssignmentList, new ItemClickListenerExtraParam() {
            @Override
            public void itemClick(int pos, String type) {
                deleteAssignments = pos;
                if (type.equals("update")) {
                    updateAssign = true;
                    Intent i = new Intent(EditInfoActivity.this, CreateAssignentNew.class);
                    i.putExtra("updateItem", addAssignmentList.get(pos).getId()+"");
                    startActivityForResult(i, CREATEASSGN);
                } else {
                    deleteAssignment(addAssignmentList.get(pos));
                }
            }
        });

        recyclerView_add_assignments.setAdapter(addAssignmentAdapters);

        addProductAdapters = new AddProductAdapters(this, uploadProductList, new ItemClickListenerExtraThreeParam() {
            @Override
            public void itemClick(int pos, String subPos, String type) {
                updateProductPos = pos;
                if (type.equals("update")) {
                    updateProduct = true;
                    updateProduct(uploadProductList.get(pos));
                } else {
                    deleteProduct(uploadProductList.get(pos));
                }
            }
        });
        recyclerView_upload_product.setAdapter(addProductAdapters);
        addServiceAdapters = new AddServiceAdapters(this, uploadServiceList, new ItemClickListenerExtraThreeParam() {
            @Override
            public void itemClick(int pos, String subPos, String type) {
                updateServicePos = pos;
                if (type.equals("update")) {
                    updateService = true;
                    updateService(uploadServiceList.get(pos));
                } else {
                    deleteService(uploadServiceList.get(pos));
                }
            }
        });

        recyclerView_upload_services.setAdapter(addServiceAdapters);

        addBrandsAdapters = new AddBrandsAdapters(this, uploadBrandsList, new ItemClickListenerExtraParam() {
            @Override
            public void itemClick(int pos, String type) {
                updateBrandPos = pos;
                if (type.equals("update")) {
                    uploadsBrands = true;
                    updateBrands(uploadBrandsList.get(pos));
                } else {
                    deleteBrands(uploadBrandsList.get(pos));
                }
            }
        });
        recyclerView_upload_brands.setAdapter(addBrandsAdapters);
        getUserDetails();
    }

    private void getUserDetails() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.getMyProfileDetails(this, linearParent);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }
    private void removeTalent(HomeUserDetailsUploadTalent profileUploadTalent) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deleteTalents(this, linearParent, profileUploadTalent.getId());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }
    private void deleteAssignment(ProfileCreateAssignment profileCreateAssignment) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deleteAssignment(this, linearParent, profileCreateAssignment.getId());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void deleteService(HomeUserDetailsService homeUserDetailsService) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deleteServices(this, linearParent, homeUserDetailsService.getId());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void deleteProduct(HomeUserDetailsProduct homeUserDetailsProduct) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deleteProduct(this, linearParent, homeUserDetailsProduct.getId());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void updateProduct(HomeUserDetailsProduct homeUserDetailsProduct) {
        ApplicationClass.uploadImageListDataArrayList.clear();
        if (homeUserDetailsProduct.getUpload_image() != null && !(homeUserDetailsProduct.getUpload_image().isEmpty())) {
            ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsProduct.getUpload_image());
        }
        Intent i = new Intent(this, CreateProductServiceActivity.class);
        i.putExtra("type", "product");
        i.putExtra("updateItem", homeUserDetailsProduct);
        startActivityForResult(i, UPLOAD_PRODUCTS);
    }

    private void updateService(HomeUserDetailsService homeUserDetailsService) {
        ApplicationClass.uploadImageListDataArrayList.clear();
        if (homeUserDetailsService.getUpload_image() != null && !(homeUserDetailsService.getUpload_image().isEmpty())) {
            ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsService.getUpload_image());
        }
        Intent i = new Intent(this, CreateProductServiceActivity.class);
        i.putExtra("type", "service");
        i.putExtra("updateItem", homeUserDetailsService);
        startActivityForResult(i, UPLOAD_SERVICES);
    }

    private void updateBrands(AddBrandsData addBrandsData) {
        Intent i = new Intent(this, UploadBrandsActivity.class);
        i.putExtra("updateItem", addBrandsData);
        startActivityForResult(i, UPLOAD_BRANDS);
    }

    private void deleteBrands(AddBrandsData addBrandsData) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            profileModel.deleteBrands(this, linearParent, addBrandsData.getId());
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void setUserData(HomeUserDetailData homeUserDetailData) {
        HomeUserDetailsUserDetils homeUserDetailsUserDetils=homeUserDetailData.getHomeUserDetailsUserDetils();
        if (homeUserDetailsUserDetils != null) {
            if (homeUserDetailsUserDetils.getStatus() != null) {
                et_status.setText(homeUserDetailsUserDetils.getStatus());
            }
            if (homeUserDetailsUserDetils.getWorks() != null) {
                if (homeUserDetailsUserDetils.getWorks().toLowerCase().equals("full time")) {
                    imgPartTime.setImageResource(R.drawable.ic_radio_unchecked);
                    imgFullTime.setImageResource(R.drawable.ic_radio_checked);
                    workType = "Full Time";
                } else if (homeUserDetailsUserDetils.getWorks().toLowerCase().equals("part time")) {
                    imgPartTime.setImageResource(R.drawable.ic_radio_checked);
                    imgFullTime.setImageResource(R.drawable.ic_radio_unchecked);
                    workType = "Part Time";
                }
            }
            if (homeUserDetailsUserDetils.getDob() != null) {
                et_dob.setText(homeUserDetailsUserDetils.getDob());
            }
            if (homeUserDetailsUserDetils.getRespondwithin() != null) {
                et_response.setText(homeUserDetailsUserDetils.getRespondwithin() );
            }
            if (homeUserDetailsUserDetils.getPhonetype() != null) {
                if (homeUserDetailsUserDetils.getPhonetype().equals("0")) {
                    imgPrivate.setImageResource(R.drawable.ic_radio_unchecked);
                    imgPublic.setImageResource(R.drawable.ic_radio_checked);
                    PhoneType = "0";
                } else if (homeUserDetailsUserDetils.getPhonetype().equals("1")) {
                    imgPrivate.setImageResource(R.drawable.ic_radio_checked);
                    imgPublic.setImageResource(R.drawable.ic_radio_unchecked);
                    PhoneType = "1";
                }
            }

            if (homeUserDetailsUserDetils.getLocation() != null) {
                et_livesIn.setText(homeUserDetailsUserDetils.getLocation());
            }
            if (homeUserDetailsUserDetils.getGender() != null && !(homeUserDetailsUserDetils.getGender().toString().isEmpty())) {
                switch (homeUserDetailsUserDetils.getGender().toString().toLowerCase()) {
                    case "male":
                        imgMale.setImageResource(R.drawable.ic_radio_checked);
                        gender = "male";
                        break;
                    case "female":
                        imgfemale.setImageResource(R.drawable.ic_radio_checked);
                        gender = "female";
                        break;
                    case "other":
                        imgOther.setImageResource(R.drawable.ic_radio_checked);
                        gender = "other";
                        break;
                    default:
                        imgMale.setImageResource(R.drawable.ic_radio_unchecked);
                        imgfemale.setImageResource(R.drawable.ic_radio_unchecked);
                        imgOther.setImageResource(R.drawable.ic_radio_unchecked);
                }
            }
            if (homeUserDetailData.getPhone() != null) {
                et_phone.setText(homeUserDetailData.getPhone());
            }
            if (homeUserDetailData.getEmail() != null) {
                et_email.setText(homeUserDetailData.getEmail());
            }
            if (homeUserDetailsUserDetils.getName() != null) {
                String des = homeUserDetailsUserDetils.getName().substring(0, 1).toUpperCase() + homeUserDetailsUserDetils.getName().substring(1);
                et_name.setText(des);
                ApplicationClass.appPreferences.setUserName(des);
            }
            if (homeUserDetailsUserDetils.getBio() != null) {
                String des = homeUserDetailsUserDetils.getBio().substring(0, 1).toUpperCase() + homeUserDetailsUserDetils.getBio().substring(1);
                et_bio.setText(des);
            }
            if (homeUserDetailData.getEmail() != null) {
                et_email.setText(homeUserDetailData.getEmail());
            }
            if (homeUserDetailData.getPreferredLanguage() != null && !(homeUserDetailData.getPreferredLanguage().isEmpty())) {
                for (HomeUserPreferedLanguage homeUserPreferedLanguage : homeUserDetailData.getPreferredLanguage()) {
                    if (homeUserPreferedLanguage.getLanguage() != null) {
                        selectedLanguageDataList.add(homeUserPreferedLanguage.getLanguage());
                    }
                }
                selectedPreferedLanguageAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.ll_fullTime:
                imgPartTime.setImageResource(R.drawable.ic_radio_unchecked);
                imgFullTime.setImageResource(R.drawable.ic_radio_checked);
                workType = "Full Time";
                break;
            case R.id.ll_PartTime:
                imgPartTime.setImageResource(R.drawable.ic_radio_checked);
                imgFullTime.setImageResource(R.drawable.ic_radio_unchecked);
                workType = "Part Time";
                break;
             case R.id.ll_Public:
                imgPrivate.setImageResource(R.drawable.ic_radio_unchecked);
                imgPublic.setImageResource(R.drawable.ic_radio_checked);
                 PhoneType = "0";
                break;
            case R.id.ll_Private:
                imgPrivate.setImageResource(R.drawable.ic_radio_checked);
                imgPublic.setImageResource(R.drawable.ic_radio_unchecked);
                PhoneType = "1";
                break;
            case R.id.cardViewPreferredLang:
                if (preferredLanguageDataList2.isEmpty()) {
                    if (Utils.isDeviceOnline(this)) {
                        Utils.showProDialog(this);
                        profileModel.getPreferredLang(this, linearParent);
                    } else {
                        Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
                    }
                } else {
                    showPreferredLangDialog();
                }
                break;
            case R.id.ll_male:
                imgMale.setImageResource(R.drawable.ic_radio_checked);
                imgfemale.setImageResource(R.drawable.ic_radio_unchecked);
                imgOther.setImageResource(R.drawable.ic_radio_unchecked);
                gender = "male";
                break;
            case R.id.ll_female:
                imgMale.setImageResource(R.drawable.ic_radio_unchecked);
                imgfemale.setImageResource(R.drawable.ic_radio_checked);
                imgOther.setImageResource(R.drawable.ic_radio_unchecked);
                gender = "female";
                break;
            case R.id.ll_other:
                imgMale.setImageResource(R.drawable.ic_radio_unchecked);
                imgfemale.setImageResource(R.drawable.ic_radio_unchecked);
                imgOther.setImageResource(R.drawable.ic_radio_checked);
                gender = "other";
                break;
            case R.id.linear_upload_products:
                updateProduct=false;
                Intent i = new Intent(this, CreateProductServiceActivity.class);
                i.putExtra("type", "product");
                startActivityForResult(i, UPLOAD_PRODUCTS);
                break;
            case R.id.linear_upload_services:
                updateService=false;
                Intent intent = new Intent(this, CreateProductServiceActivity.class);
                intent.putExtra("type", "service");
                startActivityForResult(intent, UPLOAD_SERVICES);
                break;
            case R.id.linear_upload_brands:
                uploadsBrands=false;
                Intent intent2 = new Intent(this, UploadBrandsActivity.class);
                intent2.putExtra("type", "brands");
                startActivityForResult(intent2, UPLOAD_BRANDS);
                break;
            case R.id.btnSubmit:
                checkValidationUserDetails();
                break;
        }
    }

    private void checkValidationUserDetails() {
        String status = et_status.getText().toString().trim();
        if (TextUtils.isEmpty(status)) {
            Toast.makeText(this, "Status can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(workType)) {
            Toast.makeText(this, "Work Type can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        String address = et_livesIn.getText().toString().trim();
        if (TextUtils.isEmpty(address)) {
            Toast.makeText(this, "Address can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (selectedLanguageDataList.isEmpty()) {
            Toast.makeText(this, "Preferred Language can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(gender)) {
            Toast.makeText(this, "Gender Type can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        String respondwithin = et_response.getText().toString();
        if (TextUtils.isEmpty(respondwithin)) {
            Toast.makeText(this, "Response Time can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        String phone = et_phone.getText().toString().trim();
//        if (TextUtils.isEmpty(phone)) {
//            Toast.makeText(this, "Phone number can't be empty", Toast.LENGTH_SHORT).show();
//            return;
//        }
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            ArrayList<Integer> arrayListLang = new ArrayList<>();
            for (PreferredLanguageData preferredLanguageData : selectedLanguageDataList) {
                arrayListLang.add(preferredLanguageData.getId());
            }
            profileModel.updateUserProfile(this, linearParent, status, workType, address, arrayListLang, gender,respondwithin,et_dob.getText().toString().trim(),
                    phone,et_name.getText().toString().trim(),et_bio.getText().toString().trim(),spinnerStatus.getSelectedItem().toString().trim(), PhoneType);
        } else {
            Utils.warningSnackBar(this, linearParent, getString(R.string.no_internet));
        }
    }

    private void showPreferredLangDialog() {
        preferredlangDialog = new Dialog(this);
        preferredlangDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        preferredlangDialog.setCancelable(true);
        preferredlangDialog.setContentView(R.layout.currency_dialog_list);

        EditText editText_price_search = preferredlangDialog.findViewById(R.id.editText_price_search);
        editText_price_search.setHint(getString(R.string.preferred_language));
        RecyclerView rvCurrency = preferredlangDialog.findViewById(R.id.rvCurrency);
        preferredLanguageDataList.clear();
        preferredLanguageDataList.addAll(preferredLanguageDataList2);

        rvCurrency.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rvCurrency.getContext(), linearLayoutManager.getOrientation());
        rvCurrency.addItemDecoration(dividerItemDecoration);
        rvCurrency.setLayoutManager(linearLayoutManager);

        preferedLangAdapter = new PreferedLangAdapter(this, preferredLanguageDataList, new ItemClickListener() {
            @Override
            public void itemClick(int pos) {
                boolean isExits = checkPreferrdLangExits(preferredLanguageDataList.get(pos));
                if (isExits) {
                    Toast.makeText(EditInfoActivity.this, "Language already added", Toast.LENGTH_SHORT).show();
                } else {
                    Log.e("list",selectedLanguageDataList.size()+"");
                    if(selectedLanguageDataList.size() > 2){
                        Toast.makeText(EditInfoActivity.this, "You can Select maximum 3 Languages", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        selectedLanguageDataList.add(preferredLanguageDataList.get(pos));
                        selectedPreferedLanguageAdapter.notifyDataSetChanged();
//                        languagelist.add(preferredLanguageDataList.get(pos).getId() + "");
                    }

//                    selectedLanguageDataList.add(preferredLanguageDataList.get(pos));
//                    selectedPreferedLanguageAdapter.notifyDataSetChanged();
                }
                preferredlangDialog.dismiss();
            }
        });
        rvCurrency.setAdapter(preferedLangAdapter);

        editText_price_search.addTextChangedListener(this);
        preferredlangDialog.setCanceledOnTouchOutside(true);
        preferredlangDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        preferredlangDialog.show();
    }

    private boolean checkPreferrdLangExits(PreferredLanguageData preferredLanguageData) {
        for (PreferredLanguageData preferredLanguageData1 : selectedLanguageDataList) {
            if (preferredLanguageData1.getId() == preferredLanguageData.getId()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
        filter(s.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void filter(String charText) {
        String newCharText = charText.toLowerCase(Locale.getDefault()).trim();
        preferredLanguageDataList.clear();
        if (charText.isEmpty()) {
            preferredLanguageDataList.addAll(preferredLanguageDataList2);
        } else {
            for (PreferredLanguageData preferredLanguageData : preferredLanguageDataList2) {
                if (preferredLanguageData.getName().toLowerCase(Locale.getDefault()).trim().contains(newCharText)) {
                    preferredLanguageDataList.add(preferredLanguageData);
                }
            }
        }
        preferedLangAdapter.notifyDataSetChanged();
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof HomeUserDetail) {
            HomeUserDetail homeUserDetail = (HomeUserDetail) o;
            HomeUserDetailData homeUserDetailData = homeUserDetail.getData();
            if (homeUserDetailData != null) {
                setUserData(homeUserDetailData);
                List<HomeUserDetailsUploadTalent> uploadTalentList = homeUserDetailData.getHomeUserDetailsUploadTalent();
                if (uploadTalentList != null && !(uploadTalentList.isEmpty())) {
                    addTalentsList.clear();
                    addTalentsList.addAll(uploadTalentList);
                    addTalentAdapters.notifyDataSetChanged();
                }
                List<ProfileCreateAssignment> profileCreateAssignmentList = homeUserDetailData.getHomeUserDetailCreateAssignments();
                if (profileCreateAssignmentList != null && !(profileCreateAssignmentList.isEmpty())) {
                    addAssignmentList.clear();
                    addAssignmentList.addAll(profileCreateAssignmentList);
                    addAssignmentAdapters.notifyDataSetChanged();
                }

                List<HomeUserDetailsProduct> homeUserDetailsProductList1 = homeUserDetailData.getHomeUserDetailsProducts();
                if (homeUserDetailsProductList1 != null && !(homeUserDetailsProductList1.isEmpty())) {
                    uploadProductList.addAll(homeUserDetailsProductList1);
                    addProductAdapters.notifyDataSetChanged();
                }
                List<HomeUserDetailsService> homeUserDetailsServiceList1 = homeUserDetailData.getHomeUserDetailsServices();
                if (homeUserDetailsServiceList1 != null && !(homeUserDetailsServiceList1.isEmpty())) {
                    uploadServiceList.addAll(homeUserDetailsServiceList1);
                    addServiceAdapters.notifyDataSetChanged();
                }

                List<AddBrandsData> addBrandsDataList = homeUserDetailData.getHomeUserDetailBrand();
                if (addBrandsDataList != null && !(addBrandsDataList.isEmpty())) {
                    uploadBrandsList.addAll(addBrandsDataList);
                    addBrandsAdapters.notifyDataSetChanged();
                }
            }
        }else if (o instanceof String) {
            String data = (String) o;
            if(data.equals("deleteProduct"))
            {
                uploadProductList.remove(updateProductPos);
                addProductAdapters.notifyItemRemoved(updateProductPos);
            }else if(data.equals("deleteServices"))
            {
                uploadServiceList.remove(updateServicePos);
                addServiceAdapters.notifyItemRemoved(updateServicePos);
            }else if (data.equals("assignmentDeleted")) {
                addAssignmentList.remove(deleteAssignments);
                addAssignmentAdapters.notifyItemRemoved(deleteAssignments);
            }else  if(data.equals("deleteBrands"))
            {
                uploadBrandsList.remove(updateBrandPos);
                addBrandsAdapters.notifyItemRemoved(updateBrandPos);
            }
        } else  if (o instanceof PreferredLanguage) {
            PreferredLanguage preferredLanguage = (PreferredLanguage) o;
            List<PreferredLanguageData> preferredLanguageDataListNew = preferredLanguage.getData();
            if (preferredLanguageDataListNew != null && !(preferredLanguageDataListNew.isEmpty())) {
                preferredLanguageDataList.addAll(preferredLanguageDataListNew);
                preferredLanguageDataList2.addAll(preferredLanguageDataListNew);
                showPreferredLangDialog();
            }
        } else if (o instanceof EditInfo) {
            Toast.makeText(this, "Profile updated successfully!", Toast.LENGTH_SHORT).show();
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            onBackPressed();
        } else if (o instanceof DeleteTalents) {
            addTalentsList.remove(deleteTalents);
            addTalentAdapters.notifyItemRemoved(deleteTalents);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case UPLOAD_TALENT:
                if (resultCode == RESULT_OK) {
                    HomeUserDetailsUploadTalent uploadTalentData = (HomeUserDetailsUploadTalent) data.getParcelableExtra("UploadData");
                    if (updateTalent) {
                        addTalentsList.set(deleteTalents, uploadTalentData);
                        addTalentAdapters.notifyItemChanged(deleteTalents);
                    } else {
                        addTalentsList.add(uploadTalentData);
                        addTalentAdapters.notifyDataSetChanged();
                    }
                }
                break;
            case CREATEASSGN:
                if (resultCode == RESULT_OK) {
                    ProfileCreateAssignment profileCreateAssignment = (ProfileCreateAssignment) data.getParcelableExtra("createAssign");
                    if (updateAssign) {
                        addAssignmentList.set(deleteAssignments, profileCreateAssignment);
                        addAssignmentAdapters.notifyItemChanged(deleteAssignments);
                    } else {
                        addAssignmentList.add(profileCreateAssignment);
                        addAssignmentAdapters.notifyDataSetChanged();
                    }
                }
                break;
            case UPLOAD_PRODUCTS:
                if (resultCode == RESULT_OK) {
                    HomeUserDetailsProduct addProductData = (HomeUserDetailsProduct) data.getParcelableExtra("UploadData");
                    if (updateProduct) {
                        uploadProductList.set(updateProductPos,addProductData);
                        addProductAdapters.notifyItemChanged(updateProductPos);
                    } else {
                        uploadProductList.add(addProductData);
                        addProductAdapters.notifyDataSetChanged();
                    }
                }
                break;
            case UPLOAD_SERVICES:
                if (resultCode == RESULT_OK) {
                    HomeUserDetailsService addProductData = (HomeUserDetailsService) data.getParcelableExtra("UploadData");
                    if (updateService) {
                        uploadServiceList.set(updateServicePos,addProductData);
                        addServiceAdapters.notifyItemChanged(updateServicePos);
                    } else {
                        uploadServiceList.add(addProductData);
                        addServiceAdapters.notifyDataSetChanged();
                    }
                }
                break;
            case UPLOAD_BRANDS:
                if (resultCode == RESULT_OK) {
                    AddBrandsData addBrandsData = (AddBrandsData) data.getParcelableExtra("UploadData");
                    if (uploadsBrands) {
                        uploadBrandsList.set(updateBrandPos, addBrandsData);
                        addBrandsAdapters.notifyItemChanged(updateBrandPos);
                    } else {
                        uploadBrandsList.add(addBrandsData);
                        addBrandsAdapters.notifyDataSetChanged();
                    }
                }
                break;

        }
    }

    public void openCalender (final TextView tvDate) {
        Calendar newCalendar = Calendar.getInstance();
        final SimpleDateFormat dateFormatter = new
                SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,R.style.DatePickerTheme,
                (view, year, monthOfYear, dayOfMonth) -> {
                    String select_date;
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    select_date = dateFormatter.format(newDate.getTime()) + "";
                    tvDate.setText(select_date);
                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

}