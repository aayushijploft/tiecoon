package com.twc.tiecoon.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.quickblox.auth.QBAuth;
import com.quickblox.auth.session.QBSession;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;
import com.twc.tiecoon.utils.Utils;
import com.twc.tiecoon.utils.qb.QbChatDialogMessageListenerImp;
import com.twc.tiecoon.utils.qb.VerboseQbChatConnectionListener;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smackx.muc.DiscussionHistory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

import static com.quickblox.chat.model.QBDialogType.GROUP;
import static com.quickblox.chat.model.QBDialogType.PUBLIC_GROUP;
//import com.twc.c_ony.emojiview.UI;

public class ChatActivity extends AbstractFragmentActivity implements View.OnClickListener {
    private static final String TAG = "iiiiresult";
    private RelativeLayout relativeLay;
    private final ProfileModel profileModel = new ProfileModel();
    ImageView imgSent,imgMicrophone;
    LinearLayout linearChatDesign;
    Firebase reference1, reference2;
    ScrollView scrollView;
    String user1="";
    String user2="";
    String user2name;
    String devicedToken;
    String messageText;
    EmojiconEditText emojiconEditText;
    EmojIconActions emojIcon;
    TextView tvName;
    String tag ="";
    QBChatDialog dialog = new QBChatDialog();
    public static final int MAX_MESSAGE_SYMBOLS_LENGTH = 1000;
    public static final String EXTRA_DIALOG_ID = "dialogId";
    private QBUser currentUser;
    private ChatMessageListener chatMessageListener = new ChatMessageListener();
    private ConnectionListener chatConnectionListener;
    ImageView icMenu;

    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_chat);
//        user2 = getIntent().getStringExtra("UserId");
//        user2name = getIntent().getStringExtra("name");
//        devicedToken = getIntent().getStringExtra("devicedToken");
//        if(getIntent().hasExtra("tag")){
//            tag = getIntent().getStringExtra("tag");
//
//        }
////        dialog = AppPreferences.getInstance().getQbUser();
//        if (ChatHelper.getCurrentUser() != null) {
//            currentUser = AppPreferences.getInstance().getQbUser();
//            Toast.makeText(this, currentUser.getId()+"", Toast.LENGTH_SHORT).show();
//        }
//
////        try {
////            dialog.initForChat(QBChatService.getInstance());
////        } catch (IllegalStateException e) {
////            Log.v(TAG, "The error registerCallback fro chat. Error message is : " + e.getMessage());
////            finish();
////        }
////        dialog.addMessageListener(chatMessageListener);
////        dialog.addIsTypingListener(new TypingStatusListener());
//
//        Log.d("phone",user2+"_____userID_______");
//        Log.d("phone", AppPreferences.getInstance().getQbUser().toString()+"_____name_____");
//        AppPreferences.getInstance().getQbUser();
//        Log.d("currentUser",currentUser+"_____currentUser_____");
//        user1= String.valueOf(ApplicationClass.appPreferences.getUserId());
//        ImageView iv_back=findViewById(R.id.iv_back);
//        emojiconEditText=findViewById(R.id.bottompanel);
//        relativeLay=findViewById(R.id.relativeLay);
        imgSent=findViewById(R.id.imgSent);
//        imgMicrophone=findViewById(R.id.imgMicrophone);
//        tvName=findViewById(R.id.tvName);
//        tvName.setText(user2name);
//        linearChatDesign = findViewById(R.id.linearChatDesign);
        icMenu = findViewById(R.id.icMenu);
//        scrollView = findViewById(R.id.scrollView);
//        if(emojiconEditText.requestFocus()){
//            View lastChild = scrollView.getChildAt(scrollView.getChildCount() - 1);
//            int bottom = lastChild.getBottom() + scrollView.getPaddingBottom();
//            int sy = scrollView.getScrollY();
//            int sh = scrollView.getHeight();
//            int delta = bottom - (sy + sh);
//            scrollView.smoothScrollBy(0, delta);
//        }
//        QBAuth.createSession(new QBEntityCallback<QBSession>() {
//
//            @Override
//            public void onSuccess(QBSession session, Bundle params) {
//                // You have successfully created the session
//                //
//                // Now you can use QuickBlox API!
//            }
//
//            @Override
//            public void onError(QBResponseException errors) {
//
//            }
//        });
//        QBPagedRequestBuilder pagedRequestBuilder = new QBPagedRequestBuilder();
//        pagedRequestBuilder.setPage(1);
//        pagedRequestBuilder.setPerPage(50);
//
//        QBUsers.getUsers(pagedRequestBuilder, new QBEntityCallback<ArrayList<QBUser>>() {
//            @Override
//            public void onSuccess(ArrayList<QBUser> users, Bundle params) {
//                Log.i(TAG, "Users: " + users.toString());
//                Log.i(TAG, "currentPage: " + 1);
//                Log.i(TAG, "perPage: " + 10);
//            }
//
//            @Override
//            public void onError(QBResponseException errors) {
//
//            }
//        });
        ChatHelper.getInstance().addConnectionListener(chatConnectionListener);
//        incomingMessagesManager.addDialogMessageListener(new QBChatDialogMessageListener() {
//            @Override
//            public void processMessage(String dialogID, QBChatMessage qbChatMessage, Integer senderID) {
//
//            }
//
//            @Override
//            public void processError(String dialogID, QBChatException e, QBChatMessage qbChatMessage, Integer senderID) {
//
//            }
//        });
//        emojiconEditText.setOnClickListener(v ->
//                scrollView.postDelayed(() -> {
//            View lastChild = scrollView.getChildAt(scrollView.getChildCount() - 1);
//            int bottom = lastChild.getBottom() + scrollView.getPaddingBottom();
//            int sy = scrollView.getScrollY();
//            int sh = scrollView.getHeight();
//            int delta = bottom - (sy + sh);
//            scrollView.smoothScrollBy(0, delta);
//        }, 200));
//        iv_back.setOnClickListener(this);
        imgSent.setOnClickListener(this);
        icMenu.setOnClickListener(this);
        initChatConnectionListener();
//        imgMicrophone.setOnClickListener(this);
//        Firebase.setAndroidContext(this);
//        reference1 = new Firebase("https://tiecoon-2c718.firebaseio.com/messages/" + user1 + "_" + user2);
//        reference2 = new Firebase("https://tiecoon-2c718.firebaseio.com/messages/" + user2 + "_" + user1);
//        emojIcon=new EmojIconActions(this,relativeLay,emojiconEditText,imgMicrophone,"#495C66","#DCE1E2","#E6EBEF");
//        emojIcon.ShowEmojIcon();
//        emojIcon.setKeyboardListener(new EmojIconActions.KeyboardListener() {
//            @Override
//            public void onKeyboardOpen() {
//                onKeyboardClose();
//                Log.e(TAG, "Keyboard is Opened!");
//            }
//
//            @Override
//            public void onKeyboardClose() {
//                Log.e(TAG, "Keyboard is Closed");
//            }
//        });
//        reference1.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                Map map = dataSnapshot.getValue(Map.class);
//                String message = map.get("message").toString();
//                String userName = map.get("user").toString();
//
//                if(userName.equals(user1)){
//                    addMessageBox("You:-\n" + message, 1);
//                }
//                else{
//                    addMessageBox(user2name + ":-\n" + message, 2);
//                }
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(FirebaseError firebaseError) {
//
//            }
//        });
    }

    private void initChatConnectionListener() {
        View rootView = findViewById(R.id.rv_chat_messages);
        chatConnectionListener = new VerboseQbChatConnectionListener(rootView) {
            @Override
            public void reconnectionSuccessful() {
                super.reconnectionSuccessful();
                Log.d(TAG, "Reconnection Successful");
//                skipPagination = 0;
                switch (dialog.getType()) {
                    case PUBLIC_GROUP:
                    case GROUP:
//                        checkAdapterInit = false;
                        // Join active room if we're in Group Chat
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
//                                joinGroupChat();
                            }
                        });
                        break;
                }
            }
        };
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void scrollDown() {
        scrollView.smoothScrollTo(-1, linearChatDesign.getBottom());
    }
    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                if(!tag.equals("1")){
                    Intent intent = new Intent(this,UserHomeActivity.class);
                    startActivity(intent);
                }
                else {
                    super.onBackPressed();
                }
                break;
            case R.id.imgSent:
//                if (Utils.isDeviceOnline(this)) {
//                    sendNotificationToUser();
//                    scrollDown();
//                     messageText = emojiconEditText.getText().toString();
//                    if(!messageText.equals("")){
//                        Map<String, String> map = new HashMap<>();
//                        map.put("message", messageText);
//                        map.put("user", user1);
//                        reference1.push().setValue(map);
//                        reference2.push().setValue(map);
//                        emojiconEditText.setText("");
//                    }
//
//                }else
//                {
//                    Utils.warningSnackBar(this, relativeLay, getString(R.string.no_internet));
//                }
//                try {
//                    dialog.sendStopTypingNotification();
//                } catch (XMPPException | SmackException.NotConnectedException e) {
//                    e.printStackTrace();
//                }

                break;
         case R.id.imgMicrophone:

                break;
                case R.id.icMenu:
                    popupmenudeletechat();
                break;
        }
    }

    public void popupmenudeletechat(){
            PopupMenu popup = new PopupMenu(this, icMenu);
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.delete_chat, popup.getMenu());
            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem item) {
//                    deletStory(storyViews.get(counter).getId()+"");
                    return true;
                }
            });
            popup.show();//showing popup menu
    }


    private void sendNotificationToUser() {
            profileModel.sendNotification(this, relativeLay, devicedToken);
    }

    @SuppressLint("RtlHardcoded")
    public void addMessageBox(String message, int type){
        TextView textView = new TextView(ChatActivity.this);
        textView.setText(message);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if(type == 1) {
            lp.gravity= Gravity.RIGHT;
            lp.setMargins(100, 0, 20, 10);

            textView.setLayoutParams(lp);
            textView.setBackgroundResource(R.drawable.rounded_corner1);
        }
        else{
            lp.gravity= Gravity.LEFT;
            lp.setMargins(20, 0, 100, 10);

            textView.setLayoutParams(lp);
            textView.setBackgroundResource(R.drawable.rounded_corner2);
        }

        linearChatDesign.addView(textView);
        scrollView.fullScroll(View.FOCUS_DOWN);
    }
    private class ChatMessageListener extends QbChatDialogMessageListenerImp {
        @Override
        public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
            Log.d(TAG, "Processing Received Message: " + qbChatMessage.getBody());
            showMessage(qbChatMessage);
        }
    }

    public void showMessage(QBChatMessage message) {
        Log.e("messages",message.toString());
    }


    @Override
    public void onBackPressed() {
            if(!tag.equals("1")){
                Intent intent = new Intent(this,UserHomeActivity.class);
                startActivity(intent);
            }
            else {
                super.onBackPressed();
            }
    }

    @Override
    public void update(Observable observable, Object o) {
        if(o instanceof DeleteTalents)
        {
            if (Utils.isDeviceOnline(this)) {
                profileModel.sendChat(this,relativeLay,user2,messageText);
                Log.e("datapara",user2+"----"+messageText);
            }
        }
    }
}