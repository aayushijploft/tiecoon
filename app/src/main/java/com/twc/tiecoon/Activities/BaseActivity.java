package com.twc.tiecoon.Activities;

import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;


@EActivity
public abstract class BaseActivity extends AppCompatActivity {


        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                prepareUser();
            }
        },20000);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        MainApplication.setBaseActivity(null);
    }


    @Override
    protected void onPause() {
        super.onPause();

    }

    private void prepareUser() {
        QBUser qbUser = new QBUser();
        qbUser.setLogin(ApplicationClass.appPreferences.getPhoneNumber());
//         qbUser.setFullName(usernameEt.getText().toString().trim());
        qbUser.setPassword(ApplicationClass.appPreferences.getPhoneNumber());
        //qbUser.setPassword(App.USER_DEFAULT_PASSWORD);
        signIn(qbUser);
    }

    private void signIn(final QBUser user) {

        ChatHelper.getInstance().login(user, new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser userFromRest, Bundle bundle) {
                if (userFromRest.getId().equals(user.getId())) {
                    loginToChat(user);
//                    Toast.makeText(UserHomeActivity.this, "login", Toast.LENGTH_SHORT).show();
                } else {
                    //Need to set password NULL, because server will update user only with NULL password
                    user.setPassword(null);
                    Toast.makeText(BaseActivity.this, "register", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
    }

    private void loginToChat(final QBUser user) {
        //Need to set password, because the server will not register to chat without password
        user.setPassword(ApplicationClass.appPreferences.getPhoneNumber());
        ChatHelper.getInstance().loginToChat(user, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                AppPreferences.getInstance().saveQbUser(user);
                finish();
//                hideProgressDialog();
            }

            @Override
            public void onError(QBResponseException e) {
//                hideProgressDialog();
//                showErrorSnackbar(R.string.login_chat_login_error, e, null);
            }
        });
    }

    protected void preAfterViews() {
    }

    protected void onAfterViews() {
    }

    protected void postAfterViews() {
    }


}
