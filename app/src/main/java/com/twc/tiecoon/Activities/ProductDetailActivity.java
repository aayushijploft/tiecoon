package com.twc.tiecoon.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.jsibbold.zoomage.ZoomageView;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.twc.tiecoon.Activities.PaymentModule.PaymentActivity;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Adapters.EnquiryReasonAdapter;
import com.twc.tiecoon.Adapters.ProductImageAdapter;
import com.twc.tiecoon.Adapters.TagUserAdapter;
import com.twc.tiecoon.Adapters.UploadImageListAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.CustomMedia.JZMediaExo;
import com.twc.tiecoon.Helper.CircleIndicator;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragmentActivity;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.network.response.EnquiryModel;
import com.twc.tiecoon.network.response.FeedbackModel;
import com.twc.tiecoon.network.response.UploadImageListData;
import com.twc.tiecoon.network.response.ViewProduct;
import com.twc.tiecoon.utils.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import cn.jzvd.Jzvd;
import cn.jzvd.JzvdStd;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailActivity extends AbstractFragmentActivity {
    HomeModel homeModel = new HomeModel();
    String product_id="",qbID="",video_id="",qty="",outofstock="";
    ViewPager vpBanner;
    CircleIndicator ciBanner;
    FrameLayout layoutViewpager,layoutAudio,layoutVideo;
    JzvdStd jz_video;
    TextView tvTextArea,tvTitle,tvViews,tvEnquire,tv_Category,tv_SubCategory,tvDes,tv_price;
    ImageView ivThumbnail,ivPlayButton,imgLike;
    LinearLayout linear_like,linear_Tag,linear_share;
    Button btnPlayAudio,btnPauseAudio,btnResumeAudio,btnBuy;
    private final List<UploadImageListData> uploadImageListData = new ArrayList<>();
    RecyclerView recyclerViewUploadImageList;
    private UploadImageListAdapter uploadImageListAdapter;
    RelativeLayout relativeParent;
    ZoomageView myZoomageView;
    RecyclerView rvTags;
    List<ViewProduct.Tags> tagsList = new ArrayList<>();

    EnquiryReasonAdapter enquiryReasonAdapter;
    RecyclerView rvReasons;
    String Reason="";
    RecyclerView recyclerView_Feedback;
    String username = "",userid="",amount="",currency="";
    TextView tvAvailableQuantity,tvLikeCount,tvProductId,tv_Location;
    EditText editText_quantity;
    @Override
    protected void onCreatePost(Bundle savedInstanceState) {
        setContentView(R.layout.activity_product_detail);
        product_id = getIntent().getStringExtra("product_id");
        qbID = getIntent().getStringExtra("qbID");
        init();
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    public void init() {
        relativeParent = findViewById(R.id.relativeParent);
        vpBanner = findViewById(R.id.vpBanner);
        ciBanner = findViewById(R.id.ciBanner);
        layoutViewpager = findViewById(R.id.layoutViewpager);
        layoutAudio = findViewById(R.id.layoutAudio);
        layoutVideo = findViewById(R.id.layoutVideo);
        jz_video = findViewById(R.id.jz_video);
        tvTextArea = findViewById(R.id.tvTextArea);
        tvTitle = findViewById(R.id.tvTitle);
        tvViews = findViewById(R.id.tvViews);
        tv_Category = findViewById(R.id.tv_Category);
        tv_SubCategory = findViewById(R.id.tv_SubCategory);
        tvDes = findViewById(R.id.tvDes);
        tv_price = findViewById(R.id.tv_price);
        tv_Location = findViewById(R.id.tv_Location);
        tvProductId = findViewById(R.id.tvProductId);
        ivThumbnail = findViewById(R.id.ivThumbnail);
        ivPlayButton = findViewById(R.id.ivPlayButton);
        imgLike = findViewById(R.id.imgLike);
        linear_like = findViewById(R.id.linear_like);
        linear_Tag = findViewById(R.id.linear_Tag);
        linear_share = findViewById(R.id.linear_share);
        btnPlayAudio = findViewById(R.id.btnPlayAudio);
        btnPauseAudio = findViewById(R.id.btnPauseAudio);
        btnResumeAudio = findViewById(R.id.btnResumeAudio);
        tvLikeCount = findViewById(R.id.tvLikeCount);
        tvAvailableQuantity = findViewById(R.id.tvAvailableQuantity);
        btnBuy = findViewById(R.id.btnBuy);
        editText_quantity = findViewById(R.id.editText_quantity);
        myZoomageView = findViewById(R.id.myZoomageView);
        rvTags = findViewById(R.id.rvTags);
        recyclerView_Feedback = findViewById(R.id.recyclerView_Feedback);
        rvTags.setHasFixedSize(true);
        rvTags.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewUploadImageList = findViewById(R.id.recyclerViewUploadImageList);
        uploadImageListAdapter = new UploadImageListAdapter(this, uploadImageListData);
        recyclerViewUploadImageList.setAdapter(uploadImageListAdapter);
        findViewById(R.id.iv_close).setOnClickListener(v -> {
            if (Jzvd.backPress()) {
                return;
            }
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        });

        tvTitle.setOnClickListener(v -> {
            Intent i = new Intent(this, OtherUserProfileActivity.class);
            i.putExtra("UserId", userid);
            startActivity(i);
        });

        ivPlayButton.setOnClickListener(view -> {
            saveVideo();
            ivThumbnail.setVisibility(View.GONE);
            ivPlayButton.setVisibility(View.GONE);
            jz_video.startVideo();
        });

        ivThumbnail.setOnClickListener(view -> {
            saveVideo();
            ivThumbnail.setVisibility(View.GONE);
            ivPlayButton.setVisibility(View.GONE);
            jz_video.startVideo();
        });

        linear_like.setOnClickListener(v -> setItemLike(Integer.parseInt(product_id)));
        linear_Tag.setOnClickListener(v -> {
            if (tagsList.size() > 0) {
                rvTags.setVisibility(View.VISIBLE);
            } else Toast.makeText(this, "No Tagged Users", Toast.LENGTH_SHORT).show();

        });

        linear_share.setOnClickListener(v -> {
            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Tiecoon");
                String message = "https://play.google.com/store/apps/details?id=" + "com.twc.tiecoon" + "\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, message);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        tvEnquire = findViewById(R.id.tvEnquire);
        tvEnquire.setOnClickListener(v -> EnquireFirst());

        viewProductDetails(product_id);

        btnBuy.setOnClickListener(v -> {
            if(Utils.isDeviceOnline(this)){
                String enteredqty = editText_quantity.getText().toString();
                if(TextUtils.isEmpty(enteredqty)){
                    Toast.makeText(this, "Please Enter Quantity you want to buy", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(Integer.parseInt(enteredqty) > Integer.parseInt(qty)){
                    Toast.makeText(this, "Entered quantity can not be greater than available quantity", Toast.LENGTH_SHORT).show();
                    return;
                }
                startActivity(new Intent(this, PaymentActivity.class)
                        .putExtra("id", product_id).
                                putExtra("qbID", qbID).
                                putExtra("qty", enteredqty).
                                putExtra("amount", amount).
                                putExtra("currency", currency).
                                putExtra("type", "product"));

            }
            else Toast.makeText(this, "No Internet", Toast.LENGTH_SHORT).show();
        });
    }

    private void EnquireFirst() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.enquire_layout);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        EditText editText =dialog.findViewById(R.id.editText);
        Button btnSave =dialog.findViewById(R.id.btnSave);
        Button btnCancle =dialog.findViewById(R.id.btnCancle);
         rvReasons = dialog.findViewById(R.id.rvReasons);
        rvReasons.setHasFixedSize(true);
        rvReasons.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));

        imgCancel.setOnClickListener(v -> dialog.dismiss());
        btnCancle.setOnClickListener(v -> dialog.dismiss());
        getEnquiryList();
        btnSave.setOnClickListener(v -> {
            dialog.dismiss();
            ArrayList<Integer> occupantIdsList = new ArrayList<>();
            occupantIdsList.add(Integer.valueOf(qbID));

            QBChatDialog qbChatDialog = new QBChatDialog();
            qbChatDialog.setType(QBDialogType.PRIVATE);
            qbChatDialog.setOccupantsIds(occupantIdsList);

            QBRestChatService.createChatDialog(qbChatDialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                @Override
                public void onSuccess(QBChatDialog result, Bundle params) {
                    Log.e("result",result.toString());
                    Log.e("ReasonList",ApplicationClass.ReasonList.toString());
                    StringBuilder string = new StringBuilder();
                    for (Object str : ApplicationClass.ReasonList ) {
                        string.append(str.toString()).append("\n");
                    }
                    String singleString = string.toString();
                    ChatActivity.startForResult(ProductDetailActivity.this,101, result,username,editText.getText().toString(),singleString);
                }

                @Override
                public void onError(QBResponseException responseException) {
                    Toast.makeText(ProductDetailActivity.this, "ERRROR", Toast.LENGTH_SHORT).show();
                }
            });
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }
    @Override
    protected void onPause() {
        super.onPause();
        Jzvd.releaseAllVideos();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof ViewProduct) {
            ViewProduct viewProduct = (ViewProduct) arg;
            if (!(ApplicationClass.uploadImageListDataArrayList1.isEmpty())) {
                uploadImageListData.addAll(ApplicationClass.uploadImageListDataArrayList1);
                uploadImageListAdapter.notifyDataSetChanged();
            }
            if (viewProduct.getData().getUser_id() == ApplicationClass.appPreferences.getUserId()) {
                btnBuy.setVisibility(View.GONE);
                tvEnquire.setVisibility(View.GONE);
                linear_like.setVisibility(View.VISIBLE);
                if (viewProduct.getData().getMyproductcount() != null) {
                    tvLikeCount.setText(viewProduct.getData().getMyproductcount());
                }
            }
            if (viewProduct.getData().getImage().get(0).getProduct_type()!=null) {
                switch (viewProduct.getData().getImage().get(0).getProduct_type()) {
                    case "image":
                        layoutVideo.setVisibility(View.GONE);
                        if (viewProduct.getData().getImage().size() > 0) {
                            myZoomageView.setVisibility(View.GONE);
                            layoutViewpager.setVisibility(View.VISIBLE);
                            ProductImageAdapter bannerAdapter = new ProductImageAdapter(this, viewProduct.getData().getImage());
                            vpBanner.setAdapter(bannerAdapter);
                            ciBanner.setViewPager(vpBanner);
                        } else {
                            myZoomageView.setVisibility(View.VISIBLE);
                            layoutViewpager.setVisibility(View.GONE);
                            Glide.with(this).load(viewProduct.getData().getImage().get(0).getUrl()).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(myZoomageView);
                        }
                        break;
                        case "video":
                        myZoomageView.setVisibility(View.GONE);
                        layoutViewpager.setVisibility(View.GONE);
                        layoutAudio.setVisibility(View.GONE);
                        layoutVideo.setVisibility(View.VISIBLE);
                        tvViews.setVisibility(View.VISIBLE);
                        tvTextArea.setVisibility(View.GONE);
                        if (viewProduct.getData().getImage().get(0).getUrl() != null) {
                            Glide.with(this).load(viewProduct.getData().getImage().get(0).getUrl()).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(ivThumbnail);
                        }
                        jz_video.setUp(viewProduct.getData().getImage().get(0).getUrl(), "", JzvdStd.SCREEN_NORMAL, JZMediaExo.class);
                        if (viewProduct.getData().getSeecount() != null) {
                            tvViews.setText(viewProduct.getData().getSeecount()+ " Views");
                        }
                        video_id = viewProduct.getData().getImage().get(0).getId()+"";
                        break;
                }
            }

            tagsList = viewProduct.getData().getTags();
                TagUserAdapter tagUserAdapter = new TagUserAdapter(this, viewProduct.getData().getTags(), pos -> {
                    Intent i = new Intent(ProductDetailActivity.this, OtherUserProfileActivity.class);
                    i.putExtra("UserId", String.valueOf(viewProduct.getData().getTags().get(pos).getUser_id()));
                    startActivity(i);
                }) ;
                rvTags.setAdapter(tagUserAdapter);

                if (viewProduct.getData().getCategory_name() != null) {
                tv_Category.setText(viewProduct.getData().getCategory_name());
            }
            if (viewProduct.getData().getSubcategory_name() != null) {
                tv_SubCategory.setText(viewProduct.getData().getSubcategory_name());
            }
            username = viewProduct.getData().getUser_name();
            userid = viewProduct.getData().getUser_id()+"";
            tvTitle.setText(username);
            if (viewProduct.getData().getQty() != null) {
                tvAvailableQuantity.setText("Available Quantity: "+viewProduct.getData().getQty());
                qty = viewProduct.getData().getQty();
            }
            if (viewProduct.getData().getProductid() != null) {
                tvProductId.setText("Product ID: P"+viewProduct.getData().getProductid());
            }

            if (viewProduct.getData().getOutofstock() != null) {
                outofstock = viewProduct.getData().getOutofstock();
                if(outofstock.equals("0")){
                    tvAvailableQuantity.setText("OUT OF STOCK");
                    tvAvailableQuantity.setTextColor(getResources().getColor(R.color.red));
                    btnBuy.setEnabled(false);
                    btnBuy.setBackground(getResources().getDrawable(R.drawable.greybtn));
                }
            }
            if (viewProduct.getData().getDescription() != null) {
                tvDes.setText(viewProduct.getData().getDescription());
            }
            if (viewProduct.getData().getLocation() != null) {
                tv_Location.setText(viewProduct.getData().getLocation());
            }
            if (viewProduct.getData().getPricecode() != null) {
                tv_price.setText(viewProduct.getData().getPricecode()+" "+viewProduct.getData().getPrice());
                amount = viewProduct.getData().getPrice()+"";
                currency = viewProduct.getData().getPricecode()+"";
            }
            if (viewProduct.getData().isIs_like()) {
                imgLike.setImageResource(R.drawable.ic_like_solid);
            } else imgLike.setImageResource(R.drawable.ic_like);
            feedbackList(viewProduct.getData().getId()+"");
        }

        else if (arg instanceof DeleteTalents) {
            DeleteTalents deleteTalents = (DeleteTalents) arg;
            if (deleteTalents.getMessage().equals("Product unlike successfully")) {
                imgLike.setImageResource(R.drawable.ic_like);
            } else imgLike.setImageResource(R.drawable.ic_like_solid);
        }

        else if (arg instanceof EnquiryModel) {
            EnquiryModel enquiryModel = (EnquiryModel) arg;
            List<EnquiryModel.Data> enquiryModelList = enquiryModel.getData();
            enquiryReasonAdapter = new EnquiryReasonAdapter(this,enquiryModelList,pos -> {
                Reason = enquiryModelList.get(pos).getName()+"\n";
                Log.e("Reason",Reason);
            }) ;
            rvReasons.setAdapter(enquiryReasonAdapter);
        }
    }

    private void setItemLike(int id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
                homeModel.saveProduct(this, relativeParent,id);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    private void viewProductDetails(String id) {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.viewProductDetails(this, relativeParent,id);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    private void getEnquiryList() {
        if (Utils.isDeviceOnline(this)) {
            Utils.showProDialog(this);
            homeModel.getEnquiryList(this, relativeParent);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }


    private void saveVideo() {
        if (Utils.isDeviceOnline(this)) {
            homeModel.viewProductVideo(this, relativeParent, video_id);
        } else {
            Utils.warningSnackBar(this, relativeParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Jzvd.backPress()) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ApplicationClass.ReasonList.clear();
    }

    public void feedbackList(String id) {
        final Dialog dialog = new Dialog(ProductDetailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken(  );
        Call<FeedbackModel> call = apiInterface.feedbackList("Bearer "+accessToken,id,"product");
        call.enqueue(new Callback<FeedbackModel>() {
            @Override
            public void onResponse(@NotNull Call<FeedbackModel> call, @NotNull Response<FeedbackModel> response) {
                dialog.dismiss();
                FeedbackModel user = response.body();
                assert user != null;
                if(user.getData().size()>0){
                    recyclerView_Feedback.setVisibility(View.VISIBLE);
                    FeedbackAdapter feedbackAdapter = new FeedbackAdapter(ProductDetailActivity.this,user.getData());
                    recyclerView_Feedback.setAdapter(feedbackAdapter);
                }
                else recyclerView_Feedback.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(@NotNull Call<FeedbackModel> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public static class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {
        private final Context context;
        private final List<FeedbackModel.Data> list;

        public FeedbackAdapter(Context context, List<FeedbackModel.Data> list) {
            this.context = context;
            this.list = list;
        }

        @NotNull
        @Override
        public FeedbackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.buying_items_user_rating_item_layout, parent, false);
            return new ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(FeedbackAdapter.ViewHolder holder, final int position) {
            Glide.with(context).load(list.get(position).getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.imgProfile);
            holder.tv_UserName.setText(list.get(position).getUsername());
            holder.simpleRatingBar.setText(String.valueOf(list.get(position).getRating()));
            holder.textViewDescription.setText(list.get(position).getMassage());
            holder.rating_feedback.setRating(Float.parseFloat(String.valueOf(list.get(position).getRating())));

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {

            CircleImageView imgProfile;
            TextView tv_UserName,simpleRatingBar,textViewDescription;
            RatingBar rating_feedback;
            public ViewHolder(View itemView) {
                super(itemView);
                imgProfile = itemView.findViewById(R.id.imgProfile);
                tv_UserName = itemView.findViewById(R.id.tv_UserName);
                simpleRatingBar = itemView.findViewById(R.id.simpleRatingBar);
                rating_feedback = itemView.findViewById(R.id.rating_feedback);
                textViewDescription = itemView.findViewById(R.id.textViewDescription);

            }
        }
    }

}
