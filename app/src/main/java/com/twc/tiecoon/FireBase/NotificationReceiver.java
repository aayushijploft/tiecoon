package com.twc.tiecoon.FireBase;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.legacy.content.WakefulBroadcastReceiver;

import java.util.List;

public class NotificationReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //playNotificationSound(context);


        if(intent!=null)
        {
            String typeStr = intent.getStringExtra("Type");
            String idSrt = intent.getStringExtra("Id");

            Log.e("FCM_Id_NOTI",idSrt+"");
            Log.e("FCM_Type_NOTI",typeStr+"");





        }
    }

    /*public void playNotificationSound(Context context) {
        try {
            MediaPlayer mp = MediaPlayer.create(context, R.raw.beep_beep);
            mp.setOnCompletionListener(mediaPlayer -> mp.release());
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playNotificationSound2(Context context) {
        try {
            MediaPlayer mp = MediaPlayer.create(context, R.raw.new_beep_beep);
            mp.setOnCompletionListener(mediaPlayer -> mp.release());
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    public boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager)
                context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            assert am != null;
            List<ActivityManager.RunningAppProcessInfo>
                    runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo
                    processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager
                        .RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            assert am != null;
            List<ActivityManager.RunningTaskInfo>
                    taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName()
                    .equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

}
