package com.twc.tiecoon.FireBase;

import android.util.Log;

import com.twc.tiecoon.ApplicationClass;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


public class MessagingService extends FirebaseMessagingService {
    private static final String TAG = FirebaseMessagingService.class.getSimpleName();

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.d(TAG, "NEW_TOKEN: " + token);
        ApplicationClass.appPreferences.SetFirebaseToken(token);
    }
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

//        if (remoteMessage.getNotification() != null) {
//            String title = remoteMessage.getNotification().getTitle();
//            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//            Log.d(TAG, "title: " + title);
//        }
//        Log.d(TAG, "From: " + remoteMessage.getFrom());
//        Log.d(TAG, "From: " + "qwerty");
//        Log.d(TAG, "From: " + remoteMessage);

    }



//    private void sendNotification(String messageBody, String Id, String Type,String title) {
//        intent = new Intent(this, ChatActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.putExtra("phone",Id);
//        intent.putExtra("name",Type);
//        pendingIntent = PendingIntent.getActivity(this, 0, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//        int defaults = 0;
//        defaults = defaults | Notification.DEFAULT_LIGHTS;
//        defaults = defaults | Notification.DEFAULT_VIBRATE;
//        //defaults = defaults | Notification.DEFAULT_SOUND;
//
//
//        /*AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//        assert audioManager != null;
//        audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, audioManager.
//                getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION), 0);*/
//        Bitmap bitmap = null;
////        if (!data.get("image").equals("")) {
////        try {
////            if (image != null && !image.equals(""))
////                bitmap = getBitmapFromURL(image);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//        notificationBuilder = new NotificationCompat.Builder(this, "exampleServiceChannel")
//                //.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
//                //.setSmallIcon(R.drawable.taskzon_logo1)
//                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),
//                        R.mipmap.ic_launcher))
//                //.setContentTitle(getResources().getString(R.string.app_name))
//                .setContentTitle(title)
//                //.setContentText(messageBody)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
////                .setAutoCancel(true)
//                .setLargeIcon(bitmap)
//                .setGroup("fcm")
//                .setSound(defaultSoundUri)
//                .setDefaults(defaults)
//                .setContentIntent(pendingIntent);
//
//
////        if (bitmap != null) {
////            NotificationCompat.BigPictureStyle s = new NotificationCompat.BigPictureStyle().bigPicture(bitmap);
////            notificationBuilder.setStyle(s);
////        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
//            notificationBuilder.setColor(getResources().getColor(R.color.colorPrimary));
//        } else {
//            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
//        }
//
//        notificationBuilder.setDefaults(defaults);
//
//        notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        nId++;
//        notificationManager.notify(nId, notificationBuilder.build());
//    }
}
