package com.twc.tiecoon.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.twc.tiecoon.Adapters.UserProfileImageHorizontalAdapters;
import com.twc.tiecoon.R;


public class SuccessfulConnectionFragment extends Fragment {
    private UserProfileImageHorizontalAdapters userProfileImageHorizontalAdapters;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_successful_connection, container, false);
    }
}