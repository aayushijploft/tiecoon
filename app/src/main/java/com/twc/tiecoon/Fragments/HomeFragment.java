package com.twc.tiecoon.Fragments;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.novoda.merlin.Connectable;
import com.novoda.merlin.Merlin;
import com.novoda.merlin.MerlinsBeard;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.twc.instagrampicker.InstagramPicker;
import com.twc.tiecoon.Activities.AddStoryScreen;
import com.twc.tiecoon.Activities.CollaborationActivity;
import com.twc.tiecoon.Activities.LoginActivity;
import com.twc.tiecoon.Activities.OtherUserProfileActivity;
import com.twc.tiecoon.Activities.ProfileFirst_Activity;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Activities.SettingActivity;
import com.twc.tiecoon.Activities.UserHomeActivity;
import com.twc.tiecoon.Activities.ViewStoryActivity;
import com.twc.tiecoon.Activities.WatermarkActivity;
import com.twc.tiecoon.Adapters.DashboardAdapter;
import com.twc.tiecoon.Adapters.HomeScreenAdapters;
import com.twc.tiecoon.Adapters.StoryAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.FilterUserData;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragment;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AdsModel;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.OtherProfileUserDetail;
import com.twc.tiecoon.network.response.QuickbloxCheck;
import com.twc.tiecoon.network.response.SaveUnsaveAds;
import com.twc.tiecoon.network.response.SetUserLikeUnlike;
import com.twc.tiecoon.network.response.StoryView;
import com.twc.tiecoon.network.response.StoryViewData;
import com.twc.tiecoon.network.response.SubscriptionModel;
import com.twc.tiecoon.prefrences.AppPreferences;
import com.twc.tiecoon.utils.ChatHelper;
import com.twc.tiecoon.utils.ProgressRequestBody;
import com.twc.tiecoon.utils.Utils;
import com.twc.tiecoon.utils.qb.QbDialogHolder;
import com.twc.tiecoon.utils.qb.callback.QbEntityCallbackImpl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import static android.app.Activity.RESULT_OK;


public class HomeFragment extends AbstractFragment implements View.OnClickListener, UserHomeActivity.FragmentData {
    private HomeModel homeModel = new HomeModel();
    private LinearLayout linearParent;
    private ImageView imgJoinGroupOne, imgJoinGroupTwo, imgJoinGroupCreateNew;
    private CircleImageView imageAddStory;
    private RecyclerView recyclerView_story, recyclerViewHomeFragment;
    private List<HomeUserDetailData> homeUserDetailDataList = new ArrayList<>();
    private List<StoryViewData> storyViewDataList = new ArrayList<>();
    private DashboardAdapter homeScreenAdapters;
    private StoryAdapter storyAdapter;
    private static final int USERUPDATE = 301;
    private int currentPage = 1;
    private boolean isLoading = true;
    private int pastVisibleItems, visibleItemCount, totalItemCount, previousTotal = 0;
    private int viewThreshold = 1;
    private View view;
    private int updatePosition;

    private boolean noMore = false;
    NestedScrollView mScrollview;
    private FilterUserData filterUserData=new FilterUserData();
    SwipeRefreshLayout swiperefresh;
    LinearLayout layoutNoData;
    private MerlinsBeard merlinsBeard;
    private final String[] PROFILE_PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int PROFILE_PERMISSION_REQUEST = 501;
    private boolean profilePermissionDeniedWithDontAskMeAgain = false;
    FrameLayout layoutAddStory;
    ImageView ivAddStory;
    String datatype = "1";
    EditText et_Search;
    ImageView ivSearch;
    int position = 0;

    @Override
    protected View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_testing, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        merlinsBeard = new MerlinsBeard.Builder()
                .build(getContext());
        linearParent = view.findViewById(R.id.linearParent);
        imageAddStory = view.findViewById(R.id.imageAddStory);
        mScrollview = view.findViewById(R.id.mScrollview);
        swiperefresh = view.findViewById(R.id.swiperefresh);
        layoutNoData = view.findViewById(R.id.layoutNoData);
        et_Search = view.findViewById(R.id.et_Search);
        ivSearch = view.findViewById(R.id.ivSearch);
        recyclerView_story = view.findViewById(R.id.recyclerView_story);
        recyclerViewHomeFragment = view.findViewById(R.id.recyclerViewHomeFragment);
        layoutAddStory = view.findViewById(R.id.layoutAddStory);
        ivAddStory = view.findViewById(R.id.ivAddStory);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL, false);
        recyclerViewHomeFragment.setLayoutManager(linearLayoutManager);

        ivSearch.setOnClickListener(v -> {
            String search = et_Search.getText().toString();
            if(TextUtils.isEmpty(search)){
                Toast.makeText(getContext(), "Please enter search keyword", Toast.LENGTH_SHORT).show();
                return;
            }
            filterUserData.setName(search);
            homeUserDetailDataList.clear();
            currentPage=1;
//        Toast.makeText(getContext(), datatype, Toast.LENGTH_SHORT).show();
            if (homeUserDetailDataList.isEmpty()) {
                layoutNoData.setVisibility(View.VISIBLE);
                recyclerViewHomeFragment.setVisibility(View.GONE);
            }
            getUserDetails(filterUserData,false);
            et_Search.setText("");

        });

        ivAddStory.setOnClickListener(v -> checkPermissionForProfile());
        swiperefresh.setOnRefreshListener(() -> {
            if (Utils.isDeviceOnline(view.getContext())) {
                noMore=false;
                currentPage=1;
                homeUserDetailDataList.clear();
                filterUserData.setName("");
                filterUserData.setTalantName("");
                filterUserData.setGender("");
                filterUserData.setAgeRange("");
                filterUserData.setWorkType("");
                filterUserData.setProjectName("");
                filterUserData.setServiceName("");
                filterUserData.setServiceDay("");
                filterUserData.setServiceLocation("");
                filterUserData.setLanguage("");
                filterUserData.setProjectgender("");

                filterUserData.setProductCategory("");
                filterUserData.setProductLocation("");
                filterUserData.setProductCurrency("");
                filterUserData.setProductPrice("");
                UserHomeActivity.et_Search.setText("");
                getUserDetails(filterUserData,false);
                getUserStory();
                UserHomeActivity.layoutSearch.setVisibility(View.GONE);
                UserHomeActivity.ivTiconn.setVisibility(View.VISIBLE);
            } else {
                Utils.warningSnackBar(view.getContext(), linearParent, getString(R.string.no_internet));
            }
        });

        mScrollview.getViewTreeObserver().addOnScrollChangedListener(() -> {
            View view1 = mScrollview.getChildAt(mScrollview.getChildCount() - 1);
            int diff = (view1.getBottom() - (mScrollview.getHeight() + mScrollview
                    .getScrollY()));
            if (diff == 0) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                if (!noMore) {
                    if (totalItemCount > previousTotal) {
                        previousTotal = totalItemCount;
                    }
                    if ((totalItemCount - visibleItemCount) <= pastVisibleItems + viewThreshold) {
                        currentPage++;
                        getUserDetails(filterUserData,false);
                        isLoading = true;
                    }
                }
            }
        });

        if(merlinsBeard.isConnected()){
            getUserDetails(filterUserData,true);
        }
        else Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
    }

    public  void showBadge(Context context, BottomNavigationView
            bottomNavigationView, @IdRes int itemId, String value) {
        removeBadge(bottomNavigationView, itemId);
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (getActivity()!=null){
            View badge = LayoutInflater.from(context).inflate(R.layout.layout_news_badge, bottomNavigationView, false);
            TextView text = badge.findViewById(R.id.badge_text_view);
            if(value.equals("0")){
                text.setVisibility(View.GONE);
            }
            text.setText(value);
            itemView.addView(badge);
        }
    }

    public  void showBadge1(Context context, BottomNavigationView
            bottomNavigationView, @IdRes int itemId, String value) {
        removeBadge1(bottomNavigationView, itemId);
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (getActivity()!=null){
            View badge = LayoutInflater.from(context).inflate(R.layout.layout_news_badge, bottomNavigationView, false);
            TextView text = badge.findViewById(R.id.badge_text_view);
            if(value.equals("0")){
                text.setVisibility(View.GONE);
            }
            text.setText(value);
            itemView.addView(badge);
        }
    }

    public static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (itemView.getChildCount() == 3) {
            itemView.removeViewAt(2);
        }
    }

    public static void removeBadge1(BottomNavigationView bottomNavigationView, @IdRes int itemId) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (itemView.getChildCount() == 3) {
            itemView.removeViewAt(2);
        }
    }

    private void handleItemClick(String type) {
        switch (type) {
            case "like":
                setUserLike(homeUserDetailDataList.get(updatePosition).getId());
                break;
            case "chat":
                ProgressDialog progressDialog ;
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Connecting to chat, please wait...");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(true);
                progressDialog.show();
                ArrayList<Integer> occupantIdsList = new ArrayList<Integer>();
                occupantIdsList.add(Integer.valueOf(homeUserDetailDataList.get(updatePosition).getQuickbloxuser_id()));

                QBChatDialog dialog = new QBChatDialog();
                dialog.setType(QBDialogType.PRIVATE);
                dialog.setOccupantsIds(occupantIdsList);

                QBRestChatService.createChatDialog(dialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog result, Bundle params) {
                        Log.e("result",result.toString());
                        Log.e("qid",homeUserDetailDataList.get(updatePosition).getQuickbloxuser_id());
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                        ChatActivity.startForResult(getActivity(),101,homeUserDetailDataList.get(updatePosition).getId()+"", result,homeUserDetailDataList.get(updatePosition).getHomeUserDetailsUserDetils().getName());
                    }

                    @Override
                    public void onError(QBResponseException responseException) {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), "ERRROR", Toast.LENGTH_SHORT).show();
                    }
                });
                break;

            case "share":
                String url = String.format(Constant.SHARE_URL, homeUserDetailDataList.get(updatePosition).getId()+"");
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Tiecoon");
                    String message = "Check this Profile : "+ url +"\nTo check, Download this Application : https://play.google.com/store/apps/details?id=" + "com.twc.tiecoon"+ "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, message);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case "collaborate":
                startActivity(new Intent(getContext(), CollaborationActivity.class).
                        putExtra("userid", String.valueOf(homeUserDetailDataList.get(updatePosition).getId()))
                        .putExtra("qbid",homeUserDetailDataList.get(updatePosition).getQuickbloxuser_id())
                        .putExtra("name",homeUserDetailDataList.get(updatePosition).getHomeUserDetailsUserDetils().getName())
                );
                break;

            case "AdsSave":
                saveAds(homeUserDetailDataList.get(updatePosition).getId()+"");
                break;

            default:
                Intent i = new Intent(view.getContext(), OtherUserProfileActivity.class);
                i.putExtra("UserId", String.valueOf(homeUserDetailDataList.get(updatePosition).getId()));
                startActivityForResult(i, USERUPDATE);
                break;

        }
    }


    @Override
    protected BasicModel getModel() {
        return homeModel;
    }
    HomeUserDetailData homeUserDetailData;
    @Override
    public void update(Observable observable, Object o) {
        swiperefresh.setRefreshing(false);
        if (o instanceof OtherProfileUserDetail) {
            OtherProfileUserDetail otherProfileUserDetail = (OtherProfileUserDetail) o;
            List<HomeUserDetailData> homeUserDetailDataList1 = new ArrayList<>();
            homeUserDetailDataList1 =  otherProfileUserDetail.getData();
            // homeUserDetailDataList.clear();
            if (homeUserDetailDataList1 != null && !(homeUserDetailDataList1.isEmpty())) {
                for (HomeUserDetailData homeUserDetailData : homeUserDetailDataList1) {
                    if (homeUserDetailData != null) {
                        if (!(homeUserDetailData.getId() == ApplicationClass.appPreferences.getUserId())) {
                            try {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                if (imm != null) {
                                    imm.hideSoftInputFromWindow(mScrollview.getWindowToken(), 0);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            layoutNoData.setVisibility(View.GONE);
                            recyclerViewHomeFragment.setVisibility(View.VISIBLE);
                            // homeUserDetailDataList.clear();

                            if(homeUserDetailDataList.size() > 0){
                                for (int i=0;i <homeUserDetailDataList.size();i++){
                                    if (homeUserDetailDataList.get(i).getId().equals(homeUserDetailData.getId())){
                                        return;
                                    }
                                }
//                                for(int i = 0; i<homeUserDetailDataList.size(); i++){
                                if(homeUserDetailDataList.contains(homeUserDetailData)){

                                }else {
                                    homeUserDetailDataList.add(homeUserDetailData);
                                    Log.d("qwerty",homeUserDetailDataList.size()+"qwert_");
                                }
//                                }

                            }else {
                                homeUserDetailDataList.add(homeUserDetailData);
                                Log.d("qwerty",homeUserDetailDataList.size()+"_");
                            }
                        }
                    }
                }

                homeScreenAdapters = new DashboardAdapter(view.getContext(), getFragmentManager(),datatype, homeUserDetailDataList, (pos, type) -> {
                    updatePosition = pos;
                    handleItemClick(type);

                });
                recyclerViewHomeFragment.setAdapter(homeScreenAdapters);
                homeScreenAdapters.notifyDataSetChanged();
                Parcelable recyclerViewState;
                recyclerViewState = recyclerViewHomeFragment.getLayoutManager().onSaveInstanceState();

                // Restore state
                recyclerViewHomeFragment.getLayoutManager().onRestoreInstanceState(recyclerViewState);

            } else {
                noMore = true;
                try {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(mScrollview.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            showBadge(getContext(),UserHomeActivity.bottom_navigation,R.id.notificationsFragment, String.valueOf(otherProfileUserDetail.getNotfication_count()));
            showBadge1(getContext(),UserHomeActivity.bottom_navigation,R.id.projectFragment, String.valueOf(otherProfileUserDetail.getCollaboration_count()));
            if(otherProfileUserDetail.getIs_logout() == 0){
                ApplicationClass.appPreferences.SetToken("");
                LoginManager.getInstance().logOut();
                startActivity(new Intent(getContext(), LoginActivity.class));
            }
            getUserStory();
        } else if (o instanceof HomeUserDetailData) {
            homeUserDetailData = (HomeUserDetailData) o;
            homeUserDetailDataList.set(updatePosition, homeUserDetailData);
            homeScreenAdapters.notifyItemChanged(updatePosition);
            getUserStory();
        }
        else if (o instanceof StoryView) {
            StoryView storyViewData = (StoryView) o;
            storyAdapter = new StoryAdapter(view.getContext(),storyViewData.getData(), (pos) -> {
                List<StoryView.Stores> storesList = storyViewData.getData().get(pos).getStores();
                Intent intent = new Intent(getContext(), ViewStoryActivity.class);
                intent.putExtra("profileimage",storyViewData.getData().get(pos).getProfile_image());
                intent.putExtra("username",storyViewData.getData().get(pos).getName());
                intent.putExtra("position",pos);
                intent.putParcelableArrayListExtra("storyarray", (ArrayList<? extends Parcelable>)storesList);
                startActivity(intent);
            });
            recyclerView_story.setAdapter(storyAdapter);
            storyAdapter.notifyDataSetChanged();
            List<StoryView.Stores> storesList1 = storyViewData.getData().get(0).getStores();
            if(storesList1.size()>0){
                if(getActivity() != null){
                    imageAddStory.setBorderColor(getResources().getColor(R.color.red));
                    imageAddStory.setBorderWidth(5);
                }

            }
            if (getActivity() != null) {
                Glide.with(this).load(storyViewData.getData().get(0).getProfile_image()).error(R.drawable.dummy_place)
                        .error(R.drawable.dummy_place)
                        .into(imageAddStory);
            }

            layoutAddStory.setOnClickListener(v -> {
                List<StoryView.Stores> storesList = storyViewData.getData().get(0).getStores();
                if(storesList.size()>0){
                    Intent intent = new Intent(getContext(), ViewStoryActivity.class);
                    intent.putExtra("profileimage",storyViewData.getData().get(0).getProfile_image());
                    intent.putExtra("username",storyViewData.getData().get(0).getName());
                    intent.putExtra("position",0);
                    intent.putParcelableArrayListExtra("storyarray", (ArrayList<? extends Parcelable>)storesList);
                    startActivity(intent);
                }
                else Toast.makeText(getContext(), "You have not added any story.", Toast.LENGTH_SHORT).show();
            });
        }
    }

    private void getUserDetails(FilterUserData filterUserData,boolean isLoading) {
        if (Utils.isDeviceOnline(view.getContext())) {
            if (currentPage == 1) {
                if(isLoading){
                    Utils.showProDialog(view.getContext());
                }

            }

            homeModel.getHomeUserData(view.getContext(), linearParent, currentPage,filterUserData);
        } else {
            Utils.warningSnackBar(view.getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    private void getUserStory() {
        if (Utils.isDeviceOnline(view.getContext())) {
            homeModel.viewStory(view.getContext(), linearParent);
        } else {
            Utils.warningSnackBar(view.getContext(), linearParent, getString(R.string.no_internet));
        }
    }
    private void setUserLike(int id) {
        if (Utils.isDeviceOnline(view.getContext())) {
            Utils.showProDialog(view.getContext());
            homeModel.setUserLike(view.getContext(), linearParent, id);
        } else {
            Utils.warningSnackBar(view.getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == USERUPDATE) {
            if (resultCode == RESULT_OK) {
                if (ApplicationClass.homeUserDetailData != null) {
                    homeUserDetailDataList.set(updatePosition, ApplicationClass.homeUserDetailData);
                    homeScreenAdapters.notifyItemChanged(updatePosition);
                }
            }
        }
    }

    private void openCollaborationDialog() {
        Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_groupchat);
        LinearLayout linearJoinGroupOne = dialog.findViewById(R.id.linearJoinGroupOne);
        LinearLayout linearJoinGroupTwo = dialog.findViewById(R.id.linearJoinGroupTwo);
        LinearLayout linearNewGroup = dialog.findViewById(R.id.linearNewGroup);
        imgJoinGroupOne = dialog.findViewById(R.id.imgJoinGroupOne);
        imgJoinGroupTwo = dialog.findViewById(R.id.imgJoinGroupTwo);
        imgJoinGroupCreateNew = dialog.findViewById(R.id.imgJoinGroupCreateNew);
        EditText etGroupName = dialog.findViewById(R.id.etGroupName);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(view -> dialog.dismiss());
        linearJoinGroupOne.setOnClickListener(this);
        linearJoinGroupTwo.setOnClickListener(this);
        linearNewGroup.setOnClickListener(this);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linearJoinGroupOne:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_checked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_unchecked);
                break;
            case R.id.linearJoinGroupTwo:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_checked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_unchecked);
                break;
            case R.id.linearNewGroup:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_checked);
                break;
        }
    }

    @Override
    public void sendData(FilterUserData data,String type,boolean show) {
        homeUserDetailDataList.clear();
        currentPage=1;
        filterUserData=data;
        datatype = type;
//        Toast.makeText(getContext(), datatype, Toast.LENGTH_SHORT).show();
        if (homeUserDetailDataList.isEmpty()) {
            layoutNoData.setVisibility(View.VISIBLE);
            recyclerViewHomeFragment.setVisibility(View.GONE);
        }
        getUserDetails(data, show);
    }

    private void checkPermissionForProfile() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(Utils.hasPermissions(getContext(), PROFILE_PERMISSIONS))) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAMERA) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Utils.showDialogToUserPermissionDenied(getActivity(), "This app needs camera and storage permission to select photo.",

                            PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                } else if (profilePermissionDeniedWithDontAskMeAgain) {
                    Utils.allowPermissionDeniedWithDontAskMeAgain(getActivity(), "This app needs camera and storage permission to select photo.",
                            PROFILE_PERMISSION_REQUEST);
                } else {
                    ActivityCompat.requestPermissions(getActivity(), PROFILE_PERMISSIONS, PROFILE_PERMISSION_REQUEST);
                }
            } else {
                InstagramPicker a = new InstagramPicker(getActivity());
                a.show(16, 9, 5, addresses -> {

                    Intent intent = new Intent(getContext(), AddStoryScreen.class);
                    intent.putExtra("image",addresses.get(0));
                    startActivity(intent);

                });
            }
        } else {
            InstagramPicker a = new InstagramPicker(getActivity());
            a.show(8, 8, 5, addresses -> {
            });
        }
    }

    public void saveAds(String id) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<SaveUnsaveAds> call = apiInterface.saveUnsaveAds("Bearer "+accessToken,id);
        call.enqueue(new Callback<SaveUnsaveAds>() {
            @Override
            public void onResponse(Call<SaveUnsaveAds> call, Response<SaveUnsaveAds> response) {
                dialog.dismiss();
                SaveUnsaveAds user = response.body();
                assert user != null;
                homeUserDetailDataList.clear();
                getUserDetails(filterUserData,true);

            }

            @Override
            public void onFailure(Call<SaveUnsaveAds> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void checkQuickSubscription() {

        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<QuickbloxCheck> call = apiInterface.QuickbloxCheck("Bearer "+accessToken);
        call.enqueue(new Callback<QuickbloxCheck>() {
            @Override
            public void onResponse(Call<QuickbloxCheck> call, Response<QuickbloxCheck> response) {

                QuickbloxCheck subscriptionModel = response.body();
                //   prepareUser();
            }

            @Override
            public void onFailure(Call<QuickbloxCheck> call, Throwable t) {

                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        recyclerViewHomeFragment.scrollToPosition(updatePosition);

        QBUser currentUser = AppPreferences.getInstance().getQbUser();
        if (currentUser != null && !QBChatService.getInstance().isLoggedIn()) {
            Log.d("TAG", "Resuming with Relogin");
            ChatHelper.getInstance().login(currentUser, new QBEntityCallback<QBUser>() {
                @Override
                public void onSuccess(QBUser qbUser, Bundle bundle) {
                    Log.d("TAG", "Relogin Successful");
                    reloginToChat();
                }

                @Override
                public void onError(QBResponseException e) {
                    Log.d("TAG", e.getMessage());
                }
            });
        } else {
            Log.d("TAG", "Resuming without Relogin to Chat");
            onResumeFinished();
        }
    }

    private void reloginToChat() {
        ChatHelper.getInstance().loginToChat(AppPreferences.getInstance().getQbUser(), new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                Log.d("TAG", "Relogin to Chat Successful");
                onResumeFinished();
            }

            @Override
            public void onError(QBResponseException e) {
                Log.d("TAG", "Relogin to Chat Error: " + e.getMessage());
                onResumeFinished();
            }
        });
    }

    public void onResumeFinished() {
        // Need to Override onResumeFinished() method in nested classes if we need to handle returning from background in Activity
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            homeUserDetailDataList.clear();
            getUserDetails(filterUserData,false);
            recyclerViewHomeFragment.scrollToPosition(updatePosition);
        }
    }


}