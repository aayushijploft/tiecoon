package com.twc.tiecoon.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.novoda.merlin.MerlinsBeard;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.twc.tiecoon.Activities.AddProductActivity;
import com.twc.tiecoon.Activities.AddServiceActivity;
import com.twc.tiecoon.Activities.ButtonScreen;
import com.twc.tiecoon.Activities.CreateAssignentNew;
import com.twc.tiecoon.Activities.CreateProductServiceActivity;
import com.twc.tiecoon.Activities.DropdownActivity;
import com.twc.tiecoon.Activities.EditInfoActivity;
import com.twc.tiecoon.Activities.ImageActivity;
import com.twc.tiecoon.Activities.OtherUserProfileActivity;
import com.twc.tiecoon.Activities.ProductDetailActivity;
import com.twc.tiecoon.Activities.ProductServiceBrandsBuyActivity;
import com.twc.tiecoon.Activities.ServiceDetailsActivity;
import com.twc.tiecoon.Activities.UploadBrandsActivity;
import com.twc.tiecoon.Activities.UserHomeActivity;
import com.twc.tiecoon.Activities.ViewAssignment;
import com.twc.tiecoon.Activities.WatermarkActivity;
import com.twc.tiecoon.Adapters.MyProfileBrandsHorizontalAdapters;
import com.twc.tiecoon.Adapters.MyProfileProductAdapters;
import com.twc.tiecoon.Adapters.MyProfileServiceAdapters;
import com.twc.tiecoon.Adapters.MyProfileTalentAdapters;
import com.twc.tiecoon.Adapters.MyProfileUploadedProductAdapter;
import com.twc.tiecoon.Adapters.MyProfileUploadedServiceAdapter;
import com.twc.tiecoon.Adapters.MyProfileWorkedOnProjectsAdapter;
import com.twc.tiecoon.Adapters.MyprofileCompletedAssignmentAdapters;
import com.twc.tiecoon.Adapters.MyprofileOngoingAssignmentAdapters;
import com.twc.tiecoon.Adapters.SimilarProfileAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.WorkedOnProject;
import com.twc.tiecoon.network.cmds.AbstractFragment;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.AddProductCategoryData;
import com.twc.tiecoon.network.response.CategoryCurrency;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.network.response.ConnectionModel;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.network.response.FeedbackModel;
import com.twc.tiecoon.network.response.HomeUserDetail;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.HomeUserDetailsFeedback;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsReOrganisation;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.HomeUserDetailsTestimonial;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.HomeUserDetailsUserDetils;
import com.twc.tiecoon.network.response.HomeUserPreferedLanguage;
import com.twc.tiecoon.network.response.ProductCategory;
import com.twc.tiecoon.network.response.ProfileCompleteAssignment;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.network.response.ServiceCategory;
import com.twc.tiecoon.network.response.SimilarProfile;
import com.twc.tiecoon.network.response.SubscriptionModel;
import com.twc.tiecoon.network.response.UserPdf;
import com.twc.tiecoon.utils.FileDownloader;
import com.twc.tiecoon.utils.FragmentTestimonialListListner;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.LinePagerIndicatorDecoration;
import com.twc.tiecoon.utils.Utils;
import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Observable;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileFragment extends AbstractFragment implements View.OnClickListener  {
    private HomeModel homeModel = new HomeModel();
    private LinearLayout linearParent, linearScreen, linearSpinnerCategory;
    private ImageView ivBack, imgShare, imgProfile, imgCamera;
    public static final int UPLOAD_TALENT = 101;
    public static final int UPLOAD_PRODUCTS = 102;
    public static final int UPLOAD_SERVICES = 103;
    public static final int UPLOAD_BRANDS = 104;
    private static final int CREATEASSGN = 105;
    public static final int UPLOAD_PROFILE = 106;
    private static final int EDITINFO = 107;
    private static final int UPDATEIMAGE = 108;
    private TextView tvName,tvUserName, tv_rating, tvMyProfileDes, tvTalentsTotal, tvProductAndServicesTotal, linear_add_assignments;
    private RelativeLayout rlEditInfo, relativeImageProfile;
    private RecyclerView recyclerView_profile, recyclerView_add_talents, recyclerView_upload_product,recyclerView_upload_product1
            ,recyclerView_upload_services1,
            recyclerView_upload_services, recyclerView_upload_brands, recyclerView_ongoing_assignments, recyclerView_completed_assignments;

    private LinearLayout linear_upload_brands, linearConnect, linearChat, linear3Profile, linearSpinnerTalents,linearSpinnerProducts;
    private TabLayout tabLayout, tabLayout2;
    private ViewPager viewPager, viewPager2;

    private Fragment successfulCollaborationFragment, successfulConnectionFragment, testimonialFragment, feedbackFragment,
            reorganisationFragment;

    private List<List<HomeUserDetailsUploadTalent>> listTalent = new ArrayList<>();
    private List<List<HomeUserDetailsProduct>> listProduct = new ArrayList<>();
    private List<List<HomeUserDetailsService>> listService = new ArrayList<>();
    private List<ProductCategory.Data> uploadProductList = new ArrayList<>();
    private List<List<HomeUserDetailsService>> uploadServiceList = new ArrayList<>();
    private List<ProfileCreateAssignment> homeUserDetailOngoingAssignmentList = new ArrayList<>();
    private List<ProfileCompleteAssignment> profileCompleteAssignmentArrayList = new ArrayList<>();
    private List<WorkedOnProject> workedOnProjectArrayList = new ArrayList<>();
    private List<AddBrandsData> homeUserDetailBrandList = new ArrayList<>();
    private MyProfileProductAdapters myProfileProductAdapters;
    private MyProfileUploadedProductAdapter myProfileUploadedProductAdapter;
    private MyProfileUploadedServiceAdapter myProfileUploadedServiceAdapter;
    private MyProfileServiceAdapters myProfileServiceAdapters;
    private MyProfileTalentAdapters myprofileTalentAdapters;
    private MyProfileBrandsHorizontalAdapters myProfileBrandsHorizontalAdapters;
    private MyprofileOngoingAssignmentAdapters myprofileOngoingAssignmentAdapters;
    private MyprofileCompletedAssignmentAdapters myprofileCompletedAssignmentAdapters;
    private MyProfileWorkedOnProjectsAdapter myProfileWorkedOnProjectsAdapter;
    private FragmentTestimonialListListner fragmentTestimonialListListner;
    private ArrayList<HomeUserDetailsFeedback> homeUserDetailsFeedbackArrayList = new ArrayList<>();
    private ArrayList<HomeUserDetailsReOrganisation> homeUserDetailsReOrganisationArrayList = new ArrayList<>();
    private HomeUserDetailsUserDetils homeUserDetailsUserDetils;
    private String phone = "";
    ArrayList<HomeUserPreferedLanguage> preferedLanguageList = new ArrayList<>();
    private boolean updateProduct = false;
    private int updatePos = 0;
    private int updateSubPos = 0;
    private int updateProductPos = 0;
    private int updateProductSubPos = 0;
    private int updateServicePos = 0;
    private int updateServiceSubPos = 0;
    private boolean updateService = false;
    private boolean updateTalent = false;
    private boolean updateBrand = false;
    private boolean updateAssign = false;
    CardView cardView;
    private int deleteAssignments;
    private boolean myprofilechanged = false;
    HomeUserDetailData homeUserDetailData;
    ImageView ivStatusDot;
    SwipeRefreshLayout swiperefresh;
    EditText et_notes;
    Button btnSubmit;
    String userId ="";
    TextView tvLikeCount,tvFeedback;
    LinearLayout layoutLikeCount;
    private MerlinsBeard merlinsBeard;
    private SearchableSpinner spinnerTalents;
    private SearchableSpinner spinnerProducts;
    private SearchableSpinner spinnerService;
    private List<String> listTalentsSpinner = new ArrayList<>();
    private List<String> listProductSpinner = new ArrayList<>();
    private List<String> listServiceSpinner = new ArrayList<>();
    private List<CategoryData> listCategorySpinnerData = new ArrayList<>();
    private List<ProductCategory.Data> listProductCategorySpinnerData = new ArrayList<>();
    private List<ServiceCategory.Data> listServiceSpinnerData = new ArrayList<>();
    SimilarProfileAdapter similarProfileAdapter;
    RecyclerView recycler_similarprofiles,recyclerView_worked_assignments;
    TextView tvSimilarprofile,tvCompleteProject,tvWorkedProject,tvRevenuePurchase;
    List<ProductCategory.Data> dataList = new ArrayList<>();
    List<ServiceCategory.Data> ServicedataList = new ArrayList<>();
    NestedScrollView mScrollview;
    Button btnButton1,btnButton2;
    TextView linecollab,tvcollab,lineconnection,tvconnection,tvRate;
    RecyclerView rv_connections,rv_collab,rvFeedback;
    LinearLayout linearconnection,linearCollab;

    @Override
    protected View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_profile, container, false);
        initView(view);
        return view;
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView(View view) {
        merlinsBeard = new MerlinsBeard.Builder()
                .build(getContext());
        linearParent = view.findViewById(R.id.linearParent);
        linecollab = view.findViewById(R.id.linecollab);
        tvcollab = view.findViewById(R.id.tvcollab);
        lineconnection = view.findViewById(R.id.lineconnection);
        tvconnection = view.findViewById(R.id.tvconnection);
        linearScreen = view.findViewById(R.id.linearScreen);
        rv_connections = view.findViewById(R.id.rv_connections);
        rv_collab = view.findViewById(R.id.rv_collab);
        rvFeedback = view.findViewById(R.id.rvFeedback);
        linearconnection = view.findViewById(R.id.linearconnection);
        linearCollab = view.findViewById(R.id.linearCollab);
        imgProfile = view.findViewById(R.id.imgProfile);
        tvFeedback = view.findViewById(R.id.tvFeedback);
        imgShare = view.findViewById(R.id.imgShare);
        imgCamera = view.findViewById(R.id.imgCamera);
        ivStatusDot = view.findViewById(R.id.ivStatusDot);
        relativeImageProfile = view.findViewById(R.id.relativeImageProfile);
        tvName = view.findViewById(R.id.tvName);
        tvUserName = view.findViewById(R.id.tvUserName);
        tvRate = view.findViewById(R.id.tvRate);
        et_notes = view.findViewById(R.id.et_notes);
        tv_rating = view.findViewById(R.id.tv_rating);
        tvWorkedProject = view.findViewById(R.id.tvWorkedProject);
        tvMyProfileDes = view.findViewById(R.id.tvMyProfileDes);
        rlEditInfo = view.findViewById(R.id.rlEditInfo);
        tvRevenuePurchase = view.findViewById(R.id.tvRevenuePurchase);
        cardView = view.findViewById(R.id.cardView);
        swiperefresh = view.findViewById(R.id.swiperefresh);
        btnSubmit = view.findViewById(R.id.btnSubmit);
        tvLikeCount = view.findViewById(R.id.tvLikeCount);
        recycler_similarprofiles = view.findViewById(R.id.recycler_similarprofiles);
        recyclerView_upload_services1 = view.findViewById(R.id.recyclerView_upload_services1);
        recyclerView_worked_assignments = view.findViewById(R.id.recyclerView_worked_assignments);
        tvSimilarprofile = view.findViewById(R.id.tvSimilarprofile);
        mScrollview = view.findViewById(R.id.mScrollview);
        btnButton1 = view.findViewById(R.id.btnButton1);
        btnButton2 = view.findViewById(R.id.btnButton2);
        tvCompleteProject = view.findViewById(R.id.tvCompleteProject);
        recyclerView_profile = view.findViewById(R.id.recyclerView_profile);
        linearConnect = view.findViewById(R.id.linearConnect);
        linearChat = view.findViewById(R.id.linearChat);
        linear3Profile = view.findViewById(R.id.linear3Profile);
        layoutLikeCount = view.findViewById(R.id.layoutLikeCount);
        linear_add_assignments = view.findViewById(R.id.linear_add_assignments);
        linearSpinnerTalents = view.findViewById(R.id.linearSpinnerTalents);
        linearSpinnerProducts = view.findViewById(R.id.linearSpinnerProducts);

        tabLayout = view.findViewById(R.id.tabLayout);
        viewPager = view.findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        tvTalentsTotal = view.findViewById(R.id.tvTalentsTotal);
        spinnerTalents = view.findViewById(R.id.spinnerTalents);
        spinnerProducts = view.findViewById(R.id.spinnerProducts);
        spinnerService = view.findViewById(R.id.spinnerService);
        linearSpinnerCategory = view.findViewById(R.id.linearSpinnerCategory);
        tvProductAndServicesTotal = view.findViewById(R.id.tvProductAndServicesTotal);

        recyclerView_add_talents = view.findViewById(R.id.recyclerView_add_talents);
        recyclerView_upload_product = view.findViewById(R.id.recyclerView_upload_product);
        recyclerView_upload_product1 = view.findViewById(R.id.recyclerView_upload_product1);
        recyclerView_upload_services = view.findViewById(R.id.recyclerView_upload_services);
        linear_upload_brands = view.findViewById(R.id.linear_upload_brands);
        recyclerView_upload_brands = view.findViewById(R.id.recyclerView_upload_brands);

        recyclerView_ongoing_assignments = view.findViewById(R.id.recyclerView_ongoing_assignments);
        recyclerView_completed_assignments = view.findViewById(R.id.recyclerView_completed_assignments);
        rvFeedback.setHasFixedSize(true);
        rvFeedback.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false));
        rv_connections.setHasFixedSize(true);
        rv_collab.setHasFixedSize(true);
        rv_connections.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false));
        rv_collab.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false));

        btnButton1.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ButtonScreen.class);
            intent.putExtra("value","button1");
            startActivity(intent);
        });

        btnButton2.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), ButtonScreen.class);
            intent.putExtra("value","button2");
            startActivity(intent);
        });

        view.findViewById(R.id.rlDemo).setOnClickListener(v -> {
                startActivity(new Intent(getContext(), DropdownActivity.class));
        });

        imgProfile.setOnClickListener(view1 -> {
            String imageUrl = "";
            if (homeUserDetailData.getProfile() != null) {
                if (homeUserDetailData.getPath() != null) {
                    imageUrl = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailData.getPath()).append("/").append(homeUserDetailData.getProfile())).toString();
                } else {
                    imageUrl = homeUserDetailData.getProfile();
                }
            }

            Intent intent = new Intent(getContext(), ImageActivity.class);
            intent.putExtra("image", imageUrl);
            intent.putExtra("key", "show");
            startActivityForResult(intent, UPDATEIMAGE);
        });

        imgCamera.setOnClickListener(view2 -> {
            String imageUrl = "";
            if (homeUserDetailData.getProfile() != null) {
                if (homeUserDetailData.getPath() != null) {
                    imageUrl = Constant.ImageURL + homeUserDetailData.getPath() + "/" + homeUserDetailData.getProfile();
                } else {
                    imageUrl = homeUserDetailData.getProfile();
                }
            }

            Intent intent = new Intent(getContext(), ImageActivity.class);
            intent.putExtra("image", imageUrl);
            intent.putExtra("key", "show");
            startActivityForResult(intent, UPDATEIMAGE);
        });

        imgCamera.setOnClickListener(view3 -> {
            String imageUrl = "";
            if (homeUserDetailData.getProfile() != null) {
                if (homeUserDetailData.getPath() != null) {
                    imageUrl = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailData.getPath()).append("/").append(homeUserDetailData.getProfile())).toString();
                } else {
                    imageUrl = homeUserDetailData.getProfile();
                }
            }

            Intent intent = new Intent(getContext(), ImageActivity.class);
            intent.putExtra("image", imageUrl);
            intent.putExtra("key", "show");
            startActivityForResult(intent, UPDATEIMAGE);
        });

        spinnerTalents.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {

                } else {
                    CategoryData categoryData = getCategoryData(listTalentsSpinner.get(i));
                    if (categoryData != null) {
                        if (listTalent.isEmpty()) {
                            HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = new HomeUserDetailsUploadTalent();
                            homeUserDetailsUploadTalent.setCategoryId(categoryData.getId());
                            AddProductCategoryData addProductCategoryData = new AddProductCategoryData(categoryData.getId(), categoryData.getTitle(), categoryData.getSlug(),
                                    categoryData.getParentId(), categoryData.getDescription(), categoryData.getStatus(), categoryData.getTresh(), categoryData.getCreatedAt(), categoryData.getUpdatedAt());
                            homeUserDetailsUploadTalent.setCategory(addProductCategoryData);
                            List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = new ArrayList<>();
                            homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                            listTalent.add(homeUserDetailsUploadTalentList);
                            Log.e("talentlist", listTalent.toString());
                            myprofileTalentAdapters.notifyDataSetChanged();
                        } else {
                            boolean present = checkCategoryAdded(categoryData);
                            if (!present) {
                                HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = new HomeUserDetailsUploadTalent();
                                homeUserDetailsUploadTalent.setCategoryId(categoryData.getId());
                                AddProductCategoryData addProductCategoryData = new AddProductCategoryData(categoryData.getId(), categoryData.getTitle(), categoryData.getSlug(),
                                        categoryData.getParentId(), categoryData.getDescription(), categoryData.getStatus(), categoryData.getTresh(), categoryData.getCreatedAt(), categoryData.getUpdatedAt());
                                homeUserDetailsUploadTalent.setCategory(addProductCategoryData);
                                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = new ArrayList<>();
                                homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                                listTalent.add(homeUserDetailsUploadTalentList);
                                Log.e("talentlist", listTalent.toString());
                                myprofileTalentAdapters.notifyDataSetChanged();
                            } else {
                                Toast.makeText(getContext(), "Talent already present", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerProducts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                } else {
                    ProductCategory.Data categoryData = getProductCategoryData(listProductSpinner.get(i));
                    dataList.add(categoryData);
                    if (categoryData != null) {
                        myProfileProductAdapters = new MyProfileProductAdapters(getContext(), dataList, (pos, subPos, type) -> {
                            if(type.equals("Upload")){
                                Intent intent = new Intent(getContext(), AddProductActivity.class);
                                intent.putExtra("productcat_id",dataList.get(pos).getId()+"");
                                intent.putExtra("productcat_name",dataList.get(pos).getTitle());
                                startActivityForResult(intent,UPLOAD_PRODUCTS);
                            }
                            else if(type.equals("remove")){
                                dataList.remove(pos);
                                myProfileProductAdapters.notifyDataSetChanged();
                            }

                        });
                        recyclerView_upload_product.setAdapter(myProfileProductAdapters);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {

                } else {
                    ServiceCategory.Data categoryData = getServiceCategoryData(listServiceSpinner.get(i));
                    ServicedataList.add(categoryData);
                    if (categoryData != null) {
                        myProfileServiceAdapters = new MyProfileServiceAdapters(getContext(), ServicedataList, (pos, subPos, type) -> {
                            if(type.equals("Upload")){
                                Intent intent = new Intent(getContext(), AddServiceActivity.class);
                                intent.putExtra("service_id",ServicedataList.get(pos).getId()+"");
                                intent.putExtra("service_name",ServicedataList.get(pos).getTitle());
                                startActivityForResult(intent,UPLOAD_PRODUCTS);
                            }
                            else if(type.equals("remove")){
                                ServicedataList.remove(pos);
                                myProfileServiceAdapters.notifyDataSetChanged();
                            }

                        });
                        recyclerView_upload_services.setAdapter(myProfileServiceAdapters);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        myprofileTalentAdapters = new MyProfileTalentAdapters(getContext(), listTalent, (pos, subPos, type) -> {
            updatePos = pos;
            if (type.isEmpty()) {
                updateTalent = false;
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                Intent i = new Intent(getContext(), WatermarkActivity.class);
                i.putExtra("type", "talent");
                i.putExtra("categoryId", categoryId);
                i.putExtra("categoryName", homeUserDetailsUploadTalentList.get(0).getCategory().getTitle());
                startActivityForResult(i, UPLOAD_TALENT);
            } else if (type.equals("delete")) {
                updateSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                HomeUserDetailsUploadTalent homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateSubPos);
                showConfirm(homeUserDetailsProduct);

            } else if (type.equals("deletetalent")) {
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                showDeleteDialog(categoryId);
//                    }
            }
            else if (type.equals("view")) {
                updateSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                HomeUserDetailsUploadTalent homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateSubPos);
                Intent intent = new Intent(view.getContext(), ProductServiceBrandsBuyActivity.class);
                intent.putExtra("talentData", homeUserDetailsProduct);
//                intent.putExtra("qbID", qbID);
                startActivity(intent);
            }
            else {
                updateTalent = true;
                updateSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(pos);
                HomeUserDetailsUploadTalent homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateSubPos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                Intent i = new Intent(getContext(), WatermarkActivity.class);
                ApplicationClass.uploadImageListDataArrayList.clear();
                if (homeUserDetailsProduct.getUpload_image() != null && !(homeUserDetailsProduct.getUpload_image().isEmpty())) {
                    ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsProduct.getUpload_image());
                }
                i.putExtra("type", "talent");
                i.putExtra("updateItem", homeUserDetailsProduct);
                i.putExtra("categoryId", categoryId);
                startActivityForResult(i, UPLOAD_TALENT);
            }
        });
        recyclerView_add_talents.setAdapter(myprofileTalentAdapters);

        myProfileUploadedProductAdapter = new MyProfileUploadedProductAdapter(getContext(), listProduct, (pos, subPos, type) -> {
            updateProductPos = pos;
            if (type.isEmpty()) {
                List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = listProduct.get(pos);
                HomeUserDetailsProduct homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateProductSubPos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                Intent intent = new Intent(getContext(), AddProductActivity.class);
                intent.putExtra("productcat_id",categoryId+"");
                intent.putExtra("productcat_name",homeUserDetailsUploadTalentList.get(0).getCategory().getTitle());
                startActivityForResult(intent,UPLOAD_PRODUCTS);
            }
            else if (type.equals("delete")) {
                updateProductSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = listProduct.get(pos);
                HomeUserDetailsProduct homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateProductSubPos);
                int Id = homeUserDetailsProduct.getId();
//                deleteproduct(Id);
                showProductConfirm(Id);

            }
            else if (type.equals("deleteProduct")) {
                List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = listProduct.get(pos);
                String categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId()+"";
                showProductDeleteDialog(categoryId);
            }
            else if (type.equals("view")) {
                updateProductSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = listProduct.get(pos);
                HomeUserDetailsProduct homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateProductSubPos);
                int Id = homeUserDetailsProduct.getId();
//                Toast.makeText(getContext(), String.valueOf(Id), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(), ProductDetailActivity.class);
                intent.putExtra("product_id", String.valueOf(Id));
//                intent.putExtra("qbID", qbID);
                startActivity(intent);
            }
            else {
                updateProductSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = listProduct.get(pos);
                HomeUserDetailsProduct homeUserDetailsProduct = homeUserDetailsUploadTalentList.get(updateProductSubPos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                Intent i = new Intent(getContext(), AddProductActivity.class);
                ApplicationClass.uploadImageListDataArrayList1.clear();
                if (homeUserDetailsProduct.getUpload_image() != null && !(homeUserDetailsProduct.getUpload_image().isEmpty())) {
                    ApplicationClass.uploadImageListDataArrayList1.addAll(homeUserDetailsProduct.getUpload_image());
                }
                i.putExtra("updateItem", homeUserDetailsProduct);
                i.putExtra("productcat_id", categoryId+"");
                i.putExtra("productcat_name", homeUserDetailsProduct.getCategory().getTitle());
                startActivityForResult(i, UPLOAD_PRODUCTS);
            }
        });
        recyclerView_upload_product1.setAdapter(myProfileUploadedProductAdapter);
        myProfileUploadedServiceAdapter = new MyProfileUploadedServiceAdapter(getContext(), listService, (pos, subPos, type) -> {
            updateServicePos = pos;
            if (type.isEmpty()) {
                updateTalent = false;
                List<HomeUserDetailsService> homeUserDetailsServices = listService.get(pos);
                int categoryId = homeUserDetailsServices.get(0).getCategoryId();
                Intent intent = new Intent(getContext(), AddServiceActivity.class);
                intent.putExtra("service_id",categoryId+"");
                intent.putExtra("service_name",homeUserDetailsServices.get(0).getCategory().getTitle());
                startActivityForResult(intent,UPLOAD_SERVICES);
            }
            else if (type.equals("delete")) {
                updateServiceSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsService> homeUserDetailsServices = listService.get(pos);
                HomeUserDetailsService homeUserDetailsProduct = homeUserDetailsServices.get(updateServiceSubPos);
                int Id = homeUserDetailsProduct.getId();
                showServiceConfirm(Id);

            }else if (type.equals("deleteservice")) {
                List<HomeUserDetailsService> homeUserDetailsUploadTalentList = listService.get(pos);
                String categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId()+"";
                showServiceDeleteDialog(categoryId);
            }else if (type.equals("view")) {
                updateServiceSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsService> homeUserDetailsServices = listService.get(pos);
                HomeUserDetailsService homeUserDetailsProduct = homeUserDetailsServices.get(updateServiceSubPos);
                int Id = homeUserDetailsProduct.getId();
//                Toast.makeText(getContext(), String.valueOf(Id), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(), ServiceDetailsActivity.class);
                intent.putExtra("service_id", String.valueOf(Id));
//                intent.putExtra("qbID", qbID);
                startActivity(intent);
            }
            else {
                updateServiceSubPos = Integer.parseInt(subPos);
                List<HomeUserDetailsService> homeUserDetailsUploadTalentList = listService.get(pos);
                HomeUserDetailsService homeUserDetailsService = homeUserDetailsUploadTalentList.get(updateServiceSubPos);
                int categoryId = homeUserDetailsUploadTalentList.get(0).getCategoryId();
                Intent i = new Intent(getContext(), AddServiceActivity.class);
                ApplicationClass.uploadImageListDataArrayList2.clear();
                if (homeUserDetailsService.getUpload_image() != null && !(homeUserDetailsService.getUpload_image().isEmpty())) {
                    ApplicationClass.uploadImageListDataArrayList2.addAll(homeUserDetailsService.getUpload_image());
                }
                i.putExtra("updateItem", homeUserDetailsService);
                i.putExtra("service_id", categoryId+"");
                i.putExtra("service_name", homeUserDetailsService.getCategory().getTitle());
                startActivityForResult(i, UPLOAD_SERVICES);
            }
        });
        recyclerView_upload_services1.setAdapter(myProfileUploadedServiceAdapter);

        myProfileBrandsHorizontalAdapters = new MyProfileBrandsHorizontalAdapters(getContext(), homeUserDetailBrandList, (pos, type) -> {
            updatePos = pos;
            if (type.equals("update")) {
                updateBrand = true;
                AddBrandsData homeUserDetailsProduct = homeUserDetailBrandList.get(updatePos);
                Intent i = new Intent(getContext(), UploadBrandsActivity.class);
                i.putExtra("updateItem", homeUserDetailsProduct);
                startActivityForResult(i, UPLOAD_BRANDS);
            } else {
                deleteBrands(homeUserDetailBrandList.get(pos));
            }
        });

        recyclerView_upload_brands.setAdapter(myProfileBrandsHorizontalAdapters);

        myprofileOngoingAssignmentAdapters = new MyprofileOngoingAssignmentAdapters(getContext(), homeUserDetailOngoingAssignmentList, (pos, type) -> {
            deleteAssignments = pos;
            if (type.equals("update")) {
                updateAssign = true;
                Intent i = new Intent(getContext(), CreateAssignentNew.class);
                i.putExtra("updateItem", homeUserDetailOngoingAssignmentList.get(pos).getId()+"");
                startActivityForResult(i, CREATEASSGN);
            } else if (type.equals("view")) {
                Intent i = new Intent(getContext(), ViewAssignment.class);
                i.putExtra("project_id", homeUserDetailOngoingAssignmentList.get(pos).getId()+"");
                startActivity(i);
            } else {
                showProjectDeleteDialog(homeUserDetailOngoingAssignmentList.get(pos));

            }
        });
        recyclerView_ongoing_assignments.setAdapter(myprofileOngoingAssignmentAdapters);


        myprofileCompletedAssignmentAdapters = new MyprofileCompletedAssignmentAdapters(getContext(), profileCompleteAssignmentArrayList, (pos, type) -> {
            deleteAssignments = pos;
            if (type.equals("update")) {
                updateAssign = true;
                Intent i = new Intent(getContext(), CreateAssignentNew.class);
                i.putExtra("updateItem", profileCompleteAssignmentArrayList.get(pos).getId()+"");
                startActivityForResult(i, CREATEASSGN);
            } else if (type.equals("view")) {
                Intent i = new Intent(getContext(), ViewAssignment.class);
                i.putExtra("project_id", profileCompleteAssignmentArrayList.get(pos).getId()+"");
                startActivity(i);
            } else {
                deleteCompleteAssignment(profileCompleteAssignmentArrayList.get(pos));
            }
        });
        recyclerView_completed_assignments.setAdapter(myprofileCompletedAssignmentAdapters);

        myProfileWorkedOnProjectsAdapter = new MyProfileWorkedOnProjectsAdapter(getContext(), workedOnProjectArrayList, new ItemClickListenerExtraParam() {
            @Override
            public void itemClick(int pos, String type) {
                if(type.equals("view")){
                    Intent i = new Intent(getContext(), ViewAssignment.class);
                    i.putExtra("project_id", workedOnProjectArrayList.get(pos).getProject_id()+"");
                    i.putExtra("qbID", workedOnProjectArrayList.get(pos).getQbid());

                    startActivity(i);
                }
                if(type.equals("rate")){
                    ProjectRating(workedOnProjectArrayList.get(pos).getName(),workedOnProjectArrayList.get(pos).getCurrency()+" "+
                            workedOnProjectArrayList.get(pos).getAmount(),workedOnProjectArrayList.get(pos).getProject_owner_name(),
                            workedOnProjectArrayList.get(pos).getProject_id()+"");
                }

            }
        });

        recyclerView_worked_assignments.setAdapter(myProfileWorkedOnProjectsAdapter);
        tabLayout2 = view.findViewById(R.id.tabLayout2);
        viewPager2 = view.findViewById(R.id.viewPager2);
        setupViewPager2(viewPager2);
        tabLayout2.setupWithViewPager(viewPager2);
        relativeImageProfile.setOnClickListener(this);
        imgShare.setOnClickListener(this);
        rlEditInfo.setOnClickListener(this);
        linearConnect.setOnClickListener(this);
        linearChat.setOnClickListener(this);
        linear3Profile.setOnClickListener(this);
        linearSpinnerTalents.setOnClickListener(this);
        linearSpinnerProducts.setOnClickListener(this);
        linear_upload_brands.setOnClickListener(this);
        linear_add_assignments.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        view.findViewById(R.id.imgpdf).setOnClickListener(v -> downloadPDF(String.valueOf(homeUserDetailData.getId())) );

        swiperefresh.setOnRefreshListener(() -> {
            listTalentsSpinner.clear();
            listProductSpinner.clear();
            profileCompleteAssignmentArrayList.clear();
            workedOnProjectArrayList.clear();
            homeUserDetailOngoingAssignmentList.clear();

            if(merlinsBeard.isConnected()){
                getUserDetails();
            }
            else Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        });

        linearCollab.setOnClickListener(v -> {
            linecollab.setVisibility(View.VISIBLE);
            rv_collab.setVisibility(View.VISIBLE);
            lineconnection.setVisibility(View.GONE);
            rv_connections.setVisibility(View.GONE);
        });
        linearconnection.setOnClickListener(v -> {
            linecollab.setVisibility(View.GONE);
            rv_collab.setVisibility(View.GONE);
            lineconnection.setVisibility(View.VISIBLE);
            rv_connections.setVisibility(View.VISIBLE);
        });

        if(merlinsBeard.isConnected()){
            listTalentsSpinner.clear();
            listProductSpinner.clear();
            getUserDetails();
        }
        else Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

    }

    private boolean checkCategoryAdded(CategoryData categoryData) {
        for (List<HomeUserDetailsUploadTalent> list : listTalent) {
            if (list.get(0).getCategoryId().equals(categoryData.getId())) {
                return true;
            }
        }
        return false;
    }

    private void saveNote() {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.addNote(getContext(), linearParent, et_notes.getText().toString().trim());
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    private CategoryData getCategoryData(String title) {
        for (CategoryData categoryData : listCategorySpinnerData) {
            if (categoryData.getTitle() != null && title.equals(categoryData.getTitle())) {
                return categoryData;
            }
        }
        return null;
    }

    private void downloadPDF(String  id) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.downloadUserPDF(getActivity(), linearParent, id);
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    private ProductCategory.Data getProductCategoryData(String title) {
        for (ProductCategory.Data categoryData : listProductCategorySpinnerData) {
            if (categoryData.getTitle() != null && title.equals(categoryData.getTitle())) {
                return categoryData;
            }
        }
        return null;
    }

    private ServiceCategory.Data getServiceCategoryData(String title) {
        for (ServiceCategory.Data categoryData : listServiceSpinnerData) {
            if (categoryData.getTitle() != null && title.equals(categoryData.getTitle())) {
                return categoryData;
            }
        }
        return null;
    }

    private void deleteTalents(HomeUserDetailsUploadTalent homeUserDetailsUploadTalent) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.deleteTalents(getContext(), linearParent, homeUserDetailsUploadTalent.getId());
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    private void deleteService(int id) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.deleteServices(getContext(), linearParent, id);
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    private void deleteproduct(int id) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.deleteProduct(getContext(), linearParent, id);
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    private void deleteAssignment(ProfileCreateAssignment profileCreateAssignment) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.deleteAssignment(getContext(), linearScreen, profileCreateAssignment.getId());
        } else {
            Utils.warningSnackBar(getContext(), linearScreen, getString(R.string.no_internet));
        }
    }

    private void deleteCompleteAssignment(ProfileCompleteAssignment profileCreateAssignment) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.deleteAssignment(getContext(), linearScreen, profileCreateAssignment.getId());
        } else {
            Utils.warningSnackBar(getContext(), linearScreen, getString(R.string.no_internet));
        }
    }

    private void deleteBrands(AddBrandsData addBrandsData) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.deleteBrands(getContext(), linearScreen, addBrandsData.getId());
        } else {
            Utils.warningSnackBar(getContext(), linearScreen, getString(R.string.no_internet));
        }
    }

    private void showDeleteDialog(int categoryId) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_popup);
        Button button_Cancel = dialog.findViewById(R.id.button_Cancel);
        Button button_logout = dialog.findViewById(R.id.button_logout);
        button_logout.setOnClickListener(view -> {
            deleteTalent(categoryId);
            dialog.dismiss();
            listTalentsSpinner.clear();
            listProductSpinner.clear();
            listServiceSpinner.clear();
        });
        button_Cancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showProductDeleteDialog(String categoryId) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_popup);

        Button button_Cancel = dialog.findViewById(R.id.button_Cancel);
        Button button_logout = dialog.findViewById(R.id.button_logout);

        button_logout.setOnClickListener(view -> {
            deleteprodcat(categoryId);
            dialog.dismiss();
            listTalentsSpinner.clear();
            listProductSpinner.clear();
            listServiceSpinner.clear();

        });

        button_Cancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showServiceDeleteDialog(String categoryId) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_popup);

        Button button_Cancel = dialog.findViewById(R.id.button_Cancel);
        Button button_logout = dialog.findViewById(R.id.button_logout);

        button_logout.setOnClickListener(view -> {
            deleteservicecat(categoryId);
            dialog.dismiss();
            listTalentsSpinner.clear();
            listProductSpinner.clear();
            listServiceSpinner.clear();

        });

        button_Cancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showProjectDeleteDialog(ProfileCreateAssignment profileCreateAssignment) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_popup);

        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvTitle.setText("Are you sure you want to delete ?");
        Button button_logout = dialog.findViewById(R.id.button_logout);
        button_logout.setOnClickListener(v -> {
            dialog.dismiss();
            deleteAssignment(profileCreateAssignment);


        });
        Button button_Cancel = dialog.findViewById(R.id.button_Cancel);
        button_Cancel.setOnClickListener(v -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relativeImageProfile:
//                Intent intentProfile = new Intent(this, ProfileFirst_Activity.class);
//                startActivityForResult(intentProfile, UPLOAD_PROFILE);
                break;
            case R.id.btnSubmit:
                saveNote();
                break;
            case R.id.imgShare:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Tiecoon");
                    String message = "https://play.google.com/store/apps/details?id=" + "com.twc.tiecoon" + "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, message);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.rlEditInfo:
                Intent intent2 = new Intent(getContext(), EditInfoActivity.class);
                intent2.putExtra("data", homeUserDetailsUserDetils);
                intent2.putExtra("phone", phone);
                intent2.putParcelableArrayListExtra("lang", preferedLanguageList);
                startActivityForResult(intent2, EDITINFO);
                break;
            case R.id.linearConnect:
                break;

            case R.id.linearChat:
                break;

            case R.id.linear3Profile:
                break;

            case R.id.linear_upload_brands:
                updateBrand = false;
                Intent i = new Intent(getContext(), UploadBrandsActivity.class);
                i.putExtra("type", "brands");
                startActivityForResult(i, UPLOAD_BRANDS);
                break;
            case R.id.linear_add_assignments:
                Intent intent1 = new Intent(getContext(), CreateAssignentNew.class);
                startActivityForResult(intent1, CREATEASSGN);
                break;
        }
    }

    private void getUserDetails() {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.getMyProfileDetails(getContext(), linearParent);
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    private void deleteTalent(int category_id) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.deletTalent(getContext(), linearParent,category_id);
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }
    private void deleteprodcat(String category_id) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.deleteprodcat(getContext(), linearParent,category_id);
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }
    private void deleteservicecat(String category_id) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.deleteservicecat(getContext(), linearParent,category_id);
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        successfulConnectionFragment = new SuccessfulConnectionFragment();
        successfulCollaborationFragment = new SuccessfulCollaborationFragment();

//        Bundle bundleBrand = new Bundle();
//        bundleBrand.putParcelableArrayList("brand", homeUserDetailBrandList);
//        successfulCollaborationFragment.setArguments(bundleBrand);

        adapter.addFragment(successfulConnectionFragment, "Successful Connection");
        adapter.addFragment(successfulCollaborationFragment, "Successful & Collaboration");
        viewPager.setAdapter(adapter);
    }

    @SuppressLint({"UseCompatLoadingForDrawables", "SetTextI18n"})
    @Override
    public void update(Observable observable, Object o) {
        swiperefresh.setRefreshing(false);
        if (o instanceof HomeUserDetail) {
            HomeUserDetail homeUserDetail = (HomeUserDetail) o;
            homeUserDetailData = homeUserDetail.getData();
            if (homeUserDetailData != null) {
                feedbackList(String.valueOf(homeUserDetailData.getId()));
                userId = String.valueOf(homeUserDetailData.getId());
                linearScreen.setVisibility(View.VISIBLE);
                if (homeUserDetailData.getPhone() != null) {
                    phone = homeUserDetailData.getPhone();
                }
                if (homeUserDetailData.getEmail() != null) {
                    ApplicationClass.appPreferences.setEmailId(homeUserDetailData.getEmail());
                }
                if (homeUserDetailData.getTotalearning() != null) {
                    tvRevenuePurchase.setText(homeUserDetailData.getTotalearning());
                }
                if (homeUserDetailData.getNotes() != null) {
                    et_notes.setText(homeUserDetailData.getNotes());
                }
                if (homeUserDetailData.getAvgRating() != null) {
                    tvRate.setText(homeUserDetailData.getAvgRating());
                }
                if (homeUserDetailData.getSave_profile() != null) {
                    tvLikeCount.setText(homeUserDetailData.getSave_profile());
                } else if (homeUserDetailData.getSave_profile().equals("0")) {
                    layoutLikeCount.setVisibility(View.GONE);
                }

//                Toast.makeText(this, homeUserDetailData.getUser_status(), Toast.LENGTH_SHORT).show();
                if(homeUserDetailData.getUser_status()!=null){
                    switch (homeUserDetailData.getUser_status()) {

                        case "Online":
                            if(getActivity() != null){
                                ivStatusDot.setVisibility(View.VISIBLE);
                                ivStatusDot.setBackground(getResources().getDrawable(R.drawable.ic_dot));
                                // etc ...

                            }

                            break;
                        case "Away":
                            if(getActivity() != null){
                                ivStatusDot.setVisibility(View.VISIBLE);

                                ivStatusDot.setBackground(getResources().getDrawable(R.drawable.ic_yellow_dot));
                            }

                            break;
                        case "Inactive":
                            if(getActivity() != null){
                                ivStatusDot.setVisibility(View.GONE);
                            }

                            break;
                    }
                }


                homeUserDetailsUserDetils = homeUserDetailData.getHomeUserDetailsUserDetils();
                if (homeUserDetailsUserDetils != null) {
                    if (homeUserDetailsUserDetils.getName() != null) {
                        String des = homeUserDetailsUserDetils.getName().substring(0, 1).toUpperCase() + homeUserDetailsUserDetils.getName().substring(1);
                        tvName.setText(des);
                        ApplicationClass.appPreferences.setUserName(des);
                    }
                    if (homeUserDetailsUserDetils.getBio() != null) {
                        String des = homeUserDetailsUserDetils.getBio().substring(0, 1).toUpperCase() + homeUserDetailsUserDetils.getBio().substring(1);
                        tvMyProfileDes.setText(des);
                    } else {
                        tvMyProfileDes.setVisibility(View.GONE);
                    }
                } else {
                    tvName.setVisibility(View.GONE);
                    tvMyProfileDes.setVisibility(View.GONE);
                }
                if (homeUserDetailData.getUsername() != null) {
                    tvUserName.setText("(WY"+homeUserDetailData.getUsername()+")");
                }
                if (homeUserDetailData.getPreferredLanguage() != null && !(homeUserDetailData.getPreferredLanguage().isEmpty())) {
                    preferedLanguageList.clear();
                    preferedLanguageList.addAll(homeUserDetailData.getPreferredLanguage());
                }

                if (homeUserDetailData.getProfile() != null) {
                    if (homeUserDetailData.getPath() != null) {
                        imgCamera.setVisibility(View.GONE);
                        String imageURL = Constant.ImageURL + homeUserDetailData.getPath() + "/" + homeUserDetailData.getProfile();
                        if (getActivity() != null) {
                            Glide.with(this).load(imageURL).error(R.drawable.userddummyset)
                                    .into(imgProfile);
                            ApplicationClass.appPreferences.setUserPicUrl(imageURL);
                            UserHomeActivity.setNavHeaderData(getContext());
                        }


                    } else {
                        imgCamera.setVisibility(View.VISIBLE);
                        if (getActivity() != null) {
                            Glide.with(this).load(homeUserDetailData.getProfile()).error(R.drawable.userddummyset)
                                    .into(imgProfile);
                            ApplicationClass.appPreferences.setUserPicUrl(homeUserDetailData.getProfile());
                            UserHomeActivity.setNavHeaderData(getContext());
                        }


                    }
                }
                List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents1 = homeUserDetailData.getHomeUserDetailsUploadTalent();
                listTalent.clear();
                if (homeUserDetailsUploadTalents1 != null && !(homeUserDetailsUploadTalents1.isEmpty())) {
//                    homeUserDetailsUploadTalentArrayList.addAll(homeUserDetailsUploadTalents1);
//                    myProfileTalentAdapters.notifyDataSetChanged();
                    listTalent.clear();
                    setUploadTalentData(homeUserDetailsUploadTalents1);
                }
                List<HomeUserDetailsProduct> homeUserDetailsProductList1 = homeUserDetailData.getHomeUserDetailsProducts();
                if (homeUserDetailsProductList1 != null && !(homeUserDetailsProductList1.isEmpty())) {
//                    uploadProductList.addAll(homeUserDetailsProductList1);
                    myProfileUploadedProductAdapter.notifyDataSetChanged();
                    listProduct.clear();
                    setProducttData(homeUserDetailsProductList1);
                }
                List<HomeUserDetailsService> homeUserDetailsServiceList1 = homeUserDetailData.getHomeUserDetailsServices();
                if (homeUserDetailsServiceList1 != null && !(homeUserDetailsServiceList1.isEmpty())) {
//                    uploadServiceList.addAll(homeUserDetailsServiceList1);
//                    myProfileServiceAdapters.notifyDataSetChanged();
                    myProfileUploadedServiceAdapter.notifyDataSetChanged();
                    listService.clear();
                    setServiceData(homeUserDetailsServiceList1);
                }
                List<AddBrandsData> addBrandsDataList = homeUserDetailData.getHomeUserDetailBrand();
                if (addBrandsDataList != null && !(addBrandsDataList.isEmpty())) {
                    cardView.setVisibility(View.VISIBLE);
                    homeUserDetailBrandList.clear();
                    homeUserDetailBrandList.addAll(addBrandsDataList);
                    myProfileBrandsHorizontalAdapters.notifyDataSetChanged();
                }
                List<HomeUserDetailsTestimonial> homeUserDetailsTestimonialList = homeUserDetailData.getHomeUserDetailsTestimonial();
                if (homeUserDetailsTestimonialList != null && !(homeUserDetailsTestimonialList.isEmpty())) {
                    fragmentTestimonialListListner.setList(homeUserDetailsTestimonialList);
                }

                if (homeUserDetailData.getHomeUserDetailsFeedback() != null && !(homeUserDetailData.getHomeUserDetailsFeedback().isEmpty())) {
                    homeUserDetailsFeedbackArrayList.clear();
                    homeUserDetailsFeedbackArrayList.addAll(homeUserDetailData.getHomeUserDetailsFeedback());
                }

                if (homeUserDetailData.getHomeUserDetailsReOrganisation() != null && !(homeUserDetailData.getHomeUserDetailsReOrganisation().isEmpty())) {
                    homeUserDetailsReOrganisationArrayList.clear();
                    homeUserDetailsReOrganisationArrayList.addAll(homeUserDetailData.getHomeUserDetailsReOrganisation());
                }
                if (homeUserDetailData.getHomeUserDetailCreateAssignments() != null && !(homeUserDetailData.getHomeUserDetailCreateAssignments().isEmpty())) {
                    homeUserDetailOngoingAssignmentList.clear();
                    homeUserDetailOngoingAssignmentList.addAll(homeUserDetailData.getHomeUserDetailCreateAssignments());
                    myprofileOngoingAssignmentAdapters.notifyDataSetChanged();
                }
                if (homeUserDetailData.getComplete_assignment() != null && !(homeUserDetailData.getComplete_assignment().isEmpty())) {
                    profileCompleteAssignmentArrayList.clear();
                    profileCompleteAssignmentArrayList.addAll(homeUserDetailData.getComplete_assignment());
                    myprofileCompletedAssignmentAdapters.notifyDataSetChanged();
                } else tvCompleteProject.setVisibility(View.GONE);

                if (homeUserDetailData.getComplatedprojectworkon() != null && !(homeUserDetailData.getComplatedprojectworkon().isEmpty())) {
                    workedOnProjectArrayList.clear();
                    workedOnProjectArrayList.addAll(homeUserDetailData.getComplatedprojectworkon());
                    myProfileWorkedOnProjectsAdapter.notifyDataSetChanged();
                } else tvWorkedProject.setVisibility(View.GONE);

                if (homeUserDetailData.getConnection_count() != null) {
                    tvconnection.setText("Successful Connections ("+homeUserDetailData.getConnection_count()+")");
                }
                if (homeUserDetailData.getCollaboration_count() != null) {
                    tvcollab.setText("Successful Collaborations ("+homeUserDetailData.getCollaboration_count()+")");
                }

                ConnectionAdapter connectionAdapter = new ConnectionAdapter(getContext(),homeUserDetailData.getConnection(),pos -> {
                    Intent i = new Intent(getContext(), OtherUserProfileActivity.class);
                    i.putExtra("UserId", String.valueOf(homeUserDetailData.getConnection().get(pos).getUser_id()));
                    startActivity(i);
                });
                rv_connections.setAdapter(connectionAdapter);

                ConnectionAdapter connectionAdapter1 = new ConnectionAdapter(getContext(),homeUserDetailData.getCollaboration(),pos -> {
                    Intent i = new Intent(getContext(), OtherUserProfileActivity.class);
                    i.putExtra("UserId", String.valueOf(homeUserDetailData.getCollaboration().get(pos).getUser_id()));
                    startActivity(i);
                });
                rv_collab.setAdapter(connectionAdapter1);

            }
            if (getActivity()!=null){
                if (Utils.isDeviceOnline(Objects.requireNonNull(getContext()))) {
                    Utils.showProDialog(getContext());
                    homeModel.getCategoryCurrency(getContext(), linearParent);
                } else {
                    Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
                }
            }

            if (getActivity()!=null){
                if (Utils.isDeviceOnline(getContext())) {
                    Utils.showProDialog(getContext());
                    homeModel.getProductCategory(getContext(), linearParent);
                } else {
                    Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
                }
            }
            if (getActivity()!=null){
                if (Utils.isDeviceOnline(getContext())) {
                    Utils.showProDialog(getContext());
                    homeModel.getServiceCategory(getContext(), linearParent);
                } else {
                    Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
                }
            }

        }
        else if (o instanceof String) {
            String data = (String) o;
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            switch (data) {
                case "assignmentDeleted":
                    homeUserDetailOngoingAssignmentList.remove(deleteAssignments);
                    myprofileOngoingAssignmentAdapters.notifyItemRemoved(deleteAssignments);
                    if (Build.VERSION.SDK_INT >= 26) {
                        ft.setReorderingAllowed(false);
                    }
                    ft.detach(this).attach(this).commit();
                    break;
                case "deleteBrands":
                    homeUserDetailBrandList.remove(updatePos);
                    myProfileBrandsHorizontalAdapters.notifyItemRemoved(updatePos);
                    break;
                case "deleted":
//                    if (Build.VERSION.SDK_INT >= 26) {
//                        ft.setReorderingAllowed(false);
//                    }
//                    ft.detach(this).attach(this).commit();
                    listTalent.remove(updatePos);
                    myprofileTalentAdapters.notifyItemRemoved(updatePos);
                    break;
               case "deletesingleTalent":
                    if (Build.VERSION.SDK_INT >= 26) {
                        ft.setReorderingAllowed(false);
                    }
                    ft.detach(this).attach(this).commit();
                    break;
                case "deleteProduct":
                    if (Build.VERSION.SDK_INT >= 26) {
                        ft.setReorderingAllowed(false);
                    }
                    ft.detach(this).attach(this).commit();
                    listProduct.remove(updateProductPos);
                    myProfileUploadedProductAdapter.notifyItemRemoved(updateProductPos);
                    break;
                case "deletePcat":
//                    if (Build.VERSION.SDK_INT >= 26) {
//                        ft.setReorderingAllowed(false);
//                    }
//                    linearParent.invalidate();
//                    ft.detach(this).attach(this).commit();
                    listProduct.remove(updateProductPos);
                    myProfileUploadedProductAdapter.notifyItemRemoved(updateProductPos);
                    break;
                case "Notesadded":
                    Toast.makeText(getContext(), "Notes added successfully", Toast.LENGTH_SHORT).show();
                    break;
                case "deleteServices":
                    if (Build.VERSION.SDK_INT >= 26) {
                        ft.setReorderingAllowed(false);
                    }
                    ft.detach(this).attach(this).commit();
                    listService.remove(updateServicePos);
                    myProfileUploadedServiceAdapter.notifyItemRemoved(updateServicePos);
                    break;
             case "deleteScat":
                 if (Build.VERSION.SDK_INT >= 26) {
                     ft.setReorderingAllowed(false);
                 }
                 ft.detach(this).attach(this).commit();
                 listService.remove(updateServicePos);
                 myProfileUploadedServiceAdapter.notifyItemRemoved(updateServicePos);
                    break;
            }
        } else if (o instanceof CategoryCurrency) {
            listTalentsSpinner.clear();
            setCategoryCurrency((CategoryCurrency) o);
        }else if (o instanceof ProductCategory) {
//            Toast.makeText(getContext(), "Products", Toast.LENGTH_SHORT).show();
            listProductSpinner.clear();
            setProductCategory((ProductCategory) o);

        } else if (o instanceof ServiceCategory) {
//            Toast.makeText(getContext(), "Products", Toast.LENGTH_SHORT).show();
            listServiceSpinner.clear();
            setServiceCategory((ServiceCategory) o);

        }
        if (o instanceof UserPdf) {
            UserPdf userPdf= (UserPdf) o;
            Toast.makeText(getContext(), "Downloded", Toast.LENGTH_SHORT).show();
            FileDownloader.downloadFile(userPdf.getData().getPdf(),
                    new File(Environment.getExternalStorageDirectory() + "/"+tvName.getText().toString()+".pdf"));

            displayPdf(userPdf.getData().getPdf());
        }

    }
    private void displayPdf(String fileName) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(fileName), "application/pdf");

        // FLAG_GRANT_READ_URI_PERMISSION is needed on API 24+ so the activity opening the file can read it
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_GRANT_READ_URI_PERMISSION);

        if (intent.resolveActivity(getActivity().getPackageManager()) == null) {
            // Show an error
        } else {
            startActivity(intent);
        }
    }

    private void setCategoryCurrency(CategoryCurrency categoryCurrency) {
        List<CategoryData> categoryDataList1 = categoryCurrency.getData().getCategory();
        Log.e("Categorylist1", "size:" + categoryDataList1.size() + ",  " + categoryDataList1.toString());
        if (categoryDataList1 != null && !(categoryDataList1.isEmpty())) {
            for (CategoryData categoryData : categoryDataList1) {
                if (categoryData.getTitle() != null) {
                    listCategorySpinnerData.add(categoryData);
                    listTalentsSpinner.add(categoryData.getTitle());
                }
            }
            listTalentsSpinner.add(0, "Select Talents");
            if(getActivity()!=null){
                ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), R.layout.spinner_list, listTalentsSpinner);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Setting the ArrayAdapter data on the Spinner
                spinnerTalents.setAdapter(arrayAdapter);
            }

        }
    }
    private void setProductCategory(ProductCategory productCategory) {
        List<ProductCategory.Data> productcategory = productCategory.getData();
        Log.e("Categorylist1", "size:" + productcategory.size() + ",  " + productcategory.toString());
        if (productcategory != null && !(productcategory.isEmpty())) {
            for (ProductCategory.Data productCategory1 : productcategory) {
                if (productCategory1.getTitle() != null) {
                    listProductCategorySpinnerData.add(productCategory1);
                    listProductSpinner.add(productCategory1.getTitle());
                }
            }
            listProductSpinner.add(0, "Select Product");
            if(getActivity()!=null){
                ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), R.layout.spinner_list, listProductSpinner);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Setting the ArrayAdapter data on the Spinner
                spinnerProducts.setAdapter(arrayAdapter);
            }

        }
    }
    private void setServiceCategory(ServiceCategory serviceCategory) {
        List<ServiceCategory.Data> serviceCategoryData = serviceCategory.getData();
        Log.e("Categorylist1", "size:" + serviceCategoryData.size() + ",  " + serviceCategoryData.toString());
        if (serviceCategoryData != null && !(serviceCategoryData.isEmpty())) {
            for (ServiceCategory.Data serviceCategoryData1 : serviceCategoryData) {
                if (serviceCategoryData1.getTitle() != null) {
                    listServiceSpinnerData.add(serviceCategoryData1);
                    listServiceSpinner.add(serviceCategoryData1.getTitle());
                }
            }
            listServiceSpinner.add(0, "Select Service");
            if(getActivity()!=null){
                ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), R.layout.spinner_list, listServiceSpinner);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Setting the ArrayAdapter data on the Spinner
                spinnerService.setAdapter(arrayAdapter);
            }

        }
    }

    private void setUploadTalentData(List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents1) {
        List<HomeUserDetailsUploadTalent> listTalentNew = new ArrayList<>(homeUserDetailsUploadTalents1);
        Iterator<HomeUserDetailsUploadTalent> iteratormain = homeUserDetailsUploadTalents1.iterator();
        while (iteratormain.hasNext()) {
            HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = (HomeUserDetailsUploadTalent) iteratormain.next();
            List<HomeUserDetailsUploadTalent> newlist = new ArrayList<>();
            Iterator<HomeUserDetailsUploadTalent> iteratorsub = listTalentNew.iterator();
            if (iteratorsub.hasNext()) {
                while (iteratorsub.hasNext()) {
                    HomeUserDetailsUploadTalent homeUserDetailsUploadTalent1 = (HomeUserDetailsUploadTalent) iteratorsub.next();
                    if (homeUserDetailsUploadTalent1.getCategory() != null) {
                        if ((homeUserDetailsUploadTalent.getCategoryId() == homeUserDetailsUploadTalent1.getCategoryId())) {
                            newlist.add(homeUserDetailsUploadTalent1);
                            iteratorsub.remove();
                        }
                    }
                }
            } else {
                break;
            }
            if (!(newlist.isEmpty())) {
                iteratormain.remove();
                listTalent.add(newlist);
            }
        }
        int talentTotal = listTalent.size();
        if (talentTotal > 0) {
            tvTalentsTotal.setText("Talent" + "(" + talentTotal + ")");
        }
        myprofileTalentAdapters.notifyDataSetChanged();
    }

    private void setProducttData(List<HomeUserDetailsProduct> homeUserDetailsProductList) {
        List<HomeUserDetailsProduct> listTalentNew = new ArrayList<>(homeUserDetailsProductList);
        Iterator<HomeUserDetailsProduct> iteratormain = homeUserDetailsProductList.iterator();
        while (iteratormain.hasNext()) {
            HomeUserDetailsProduct homeUserDetailsUploadTalent = (HomeUserDetailsProduct) iteratormain.next();
            List<HomeUserDetailsProduct> newlist = new ArrayList<>();
            Iterator<HomeUserDetailsProduct> iteratorsub = listTalentNew.iterator();
            if (iteratorsub.hasNext()) {
                while (iteratorsub.hasNext()) {
                    HomeUserDetailsProduct homeUserDetailsUploadTalent1 = (HomeUserDetailsProduct) iteratorsub.next();
                    if (homeUserDetailsUploadTalent1.getCategory() != null) {
                        if ((homeUserDetailsUploadTalent.getCategoryId() == homeUserDetailsUploadTalent1.getCategoryId())) {
                            newlist.add(homeUserDetailsUploadTalent1);
                            iteratorsub.remove();
                        }
                    }
                }
            } else {
                break;
            }
            if (!(newlist.isEmpty())) {
                iteratormain.remove();
                listProduct.add(newlist);
            }
        }
        myProfileUploadedProductAdapter.notifyDataSetChanged();
    }

    private void setServiceData(List<HomeUserDetailsService> homeUserDetailsProductList) {
        List<HomeUserDetailsService> listTalentNew = new ArrayList<>(homeUserDetailsProductList);
        Iterator<HomeUserDetailsService> iteratormain = homeUserDetailsProductList.iterator();
        while (iteratormain.hasNext()) {
            HomeUserDetailsService homeUserDetailsUploadTalent = (HomeUserDetailsService) iteratormain.next();
            List<HomeUserDetailsService> newlist = new ArrayList<>();
            Iterator<HomeUserDetailsService> iteratorsub = listTalentNew.iterator();
            if (iteratorsub.hasNext()) {
                while (iteratorsub.hasNext()) {
                    HomeUserDetailsService homeUserDetailsUploadTalent1 = (HomeUserDetailsService) iteratorsub.next();
                    if (homeUserDetailsUploadTalent1.getCategory() != null) {
                        if ((homeUserDetailsUploadTalent.getCategoryId() == homeUserDetailsUploadTalent1.getCategoryId())) {
                            newlist.add(homeUserDetailsUploadTalent1);
                            iteratorsub.remove();
                        }
                    }
                }
            } else {
                break;
            }
            if (!(newlist.isEmpty())) {
                iteratormain.remove();
                listService.add(newlist);
            }
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void setupViewPager2(ViewPager viewPager) {
        ViewPagerAdapter2 adapter = new ViewPagerAdapter2(getActivity().getSupportFragmentManager());
        testimonialFragment = new MyProfileTestimonialFragment();
        fragmentTestimonialListListner = (FragmentTestimonialListListner) testimonialFragment;
        feedbackFragment = new MyProfilefeedbackFragment();
        reorganisationFragment = new MyProfileReorganisationFragment();

        Bundle bundleFeeback = new Bundle();
        bundleFeeback.putParcelableArrayList("feedback", homeUserDetailsFeedbackArrayList);
        feedbackFragment.setArguments(bundleFeeback);

        Bundle bundleReorganisation = new Bundle();
        bundleReorganisation.putParcelableArrayList("reorganisation", homeUserDetailsReOrganisationArrayList);
        reorganisationFragment.setArguments(bundleReorganisation);

        adapter.addFragment(testimonialFragment, "Testimonial");
        adapter.addFragment(feedbackFragment, "Feedback");
        adapter.addFragment(reorganisationFragment, "Reorganisation");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter2 extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter2(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case UPLOAD_TALENT:
                if (resultCode == Activity.RESULT_OK) {
                    HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = (HomeUserDetailsUploadTalent) data.getParcelableExtra("UploadData");
                    List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = listTalent.get(updatePos);
                    if (updateTalent) {
                        listTalentsSpinner.clear();
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        if (Build.VERSION.SDK_INT >= 26) {
                            ft.setReorderingAllowed(false);
                        }
                        ft.detach(this).attach(this).commit();
//                        homeUserDetailsUploadTalentList.set(updateSubPos, homeUserDetailsUploadTalent);
                    } else {
                        listTalentsSpinner.clear();
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        if (Build.VERSION.SDK_INT >= 26) {
                            ft.setReorderingAllowed(false);
                        }
                        ft.detach(this).attach(this).commit();
//                        homeUserDetailsUploadTalentList.add(homeUserDetailsUploadTalent);
                    }
                    listTalent.set(updatePos, homeUserDetailsUploadTalentList);
                    myprofileTalentAdapters.notifyItemChanged(updatePos);
                }
                break;
            case UPLOAD_PRODUCTS:
                if (resultCode == Activity.RESULT_OK) {
                    listTalentsSpinner.clear();
                    dataList.clear();
                    ServicedataList.clear();
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    if (Build.VERSION.SDK_INT >= 26) {
                        ft.setReorderingAllowed(false);
                    }
                    ft.detach(this).attach(this).commit();
                }
                break;
            case UPLOAD_SERVICES:
                if (resultCode == Activity.RESULT_OK) {
                    listTalentsSpinner.clear();
                    dataList.clear();
                    ServicedataList.clear();
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    if (Build.VERSION.SDK_INT >= 26) {
                        ft.setReorderingAllowed(false);
                    }
                    ft.detach(this).attach(this).commit();
                }
                break;
            case UPLOAD_BRANDS:
                if (resultCode == Activity.RESULT_OK) {
                    AddBrandsData addBrandsData = (AddBrandsData) data.getParcelableExtra("UploadData");
                    if (updateBrand) {
                        homeUserDetailBrandList.set(updatePos, addBrandsData);
                        myProfileBrandsHorizontalAdapters.notifyItemChanged(updatePos);
                    } else {
                        homeUserDetailBrandList.add(addBrandsData);
                        myProfileBrandsHorizontalAdapters.notifyDataSetChanged();
                    }
                }
                break;
            case CREATEASSGN:
                if (resultCode == Activity.RESULT_OK) {
                    ProfileCreateAssignment profileCreateAssignment = data.getParcelableExtra("createAssign");
                    if (updateAssign) {
                        homeUserDetailOngoingAssignmentList.set(deleteAssignments, profileCreateAssignment);
                        myprofileOngoingAssignmentAdapters.notifyItemChanged(deleteAssignments);
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        if (Build.VERSION.SDK_INT >= 26) {
                            ft.setReorderingAllowed(false);
                        }
                        ft.detach(this).attach(this).commit();
                    } else {
                        listTalentsSpinner.clear();
                        listProductSpinner.clear();
                        getUserDetails();
                        homeUserDetailOngoingAssignmentList.add(profileCreateAssignment);
                        myprofileOngoingAssignmentAdapters.notifyDataSetChanged();
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        if (Build.VERSION.SDK_INT >= 26) {
                            ft.setReorderingAllowed(false);
                        }
                        ft.detach(this).attach(this).commit();
                    }
                }
                break;
            case UPLOAD_PROFILE:
                if (resultCode == Activity.RESULT_OK) {
                    myprofilechanged = true;
                    listTalentsSpinner.clear();
                    listProductSpinner.clear();
                    getUserDetails();
                }
                break;
            case UPDATEIMAGE:
                if (resultCode == Activity.RESULT_OK) {
                    myprofilechanged = true;
                    listTalentsSpinner.clear();
                    listProductSpinner.clear();
                    getUserDetails();
                }
                break;
            case EDITINFO:
                if (resultCode == Activity.RESULT_OK) {
                    listTalentsSpinner.clear();
                    listProductSpinner.clear();
                    listServiceSpinner.clear();
                    getUserDetails();
                }
                break;
        }
    }

    private void showImageDialog() {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.imagedailog);

        ImageView image = dialog.findViewById(R.id.image);
        ImageView imgCancel = dialog.findViewById(R.id.imgCancel);
        if (homeUserDetailData.getProfile() != null) {
            if (homeUserDetailData.getPath() != null) {
                imgCamera.setVisibility(View.GONE);
                String imageURL = Constant.ImageURL + homeUserDetailData.getPath() + "/" + homeUserDetailData.getProfile();
                Glide.with(this).load(imageURL).error(R.drawable.dummy_place)
                        .into(image);
                ApplicationClass.appPreferences.setUserPicUrl(imageURL);
            } else {
                imgCamera.setVisibility(View.GONE);
                Glide.with(this).load(homeUserDetailData.getProfile()).error(R.drawable.dummy_place)
                        .into(image);
                ApplicationClass.appPreferences.setUserPicUrl(homeUserDetailData.getProfile());
            }
        }
        imgCancel.setOnClickListener(view -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void showConfirm(HomeUserDetailsUploadTalent homeUserDetailsUploadTalent) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_popup);

        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvTitle.setText("Are you sure you want to delete ?");
        Button button_logout = dialog.findViewById(R.id.button_logout);

        button_logout.setOnClickListener(v -> {
            dialog.dismiss();
            deleteTalents(homeUserDetailsUploadTalent);
        });

        Button button_Cancel = dialog.findViewById(R.id.button_Cancel);
        button_Cancel.setOnClickListener(v -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    private void showProductConfirm(int Id) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_popup);

        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvTitle.setText("Are you sure you want to delete ?");
        Button button_logout = dialog.findViewById(R.id.button_logout);

        button_logout.setOnClickListener(v -> {
            dialog.dismiss();
            deleteproduct(Id);
        });

        Button button_Cancel = dialog.findViewById(R.id.button_Cancel);
        button_Cancel.setOnClickListener(v -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    private void showServiceConfirm(int Id) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_popup);

        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvTitle.setText("Are you sure you want to delete ?");
        Button button_logout = dialog.findViewById(R.id.button_logout);

        button_logout.setOnClickListener(v -> {
            dialog.dismiss();
            deleteService(Id);
        });

        Button button_Cancel = dialog.findViewById(R.id.button_Cancel);
        button_Cancel.setOnClickListener(v -> dialog.dismiss());
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void ProjectRating(String Projectname,String Amount,String Ownername,String projectid) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.projectrating_popup);
        ImageView imgCancel =dialog.findViewById(R.id.imgCancel);
        TextView tvWorkedProject = dialog.findViewById(R.id.tvWorkedProject);
        TextView tvEarned = dialog.findViewById(R.id.tvEarned);
        TextView tvOwnername = dialog.findViewById(R.id.tvOwnername);
        RatingBar rating_bar = dialog.findViewById(R.id.rating_bar);
        EditText editText1 = dialog.findViewById(R.id.editText1);
        Button btnSave = dialog.findViewById(R.id.btnSave);
        tvWorkedProject.setText(Projectname);
        tvEarned.setText(Amount);
        tvOwnername.setText(Ownername);

        imgCancel.setOnClickListener(v -> dialog.dismiss());

        btnSave.setOnClickListener(v -> {
            String Rating = String.valueOf(rating_bar.getRating());
            String Description = editText1.getText().toString();
            confirm(projectid,Rating,Description,dialog);
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void confirm(String projectid,String Rating,String Description,Dialog feedbackdialog) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("This Feedback once submitted cannot be editted or deleted. Are sure you want to submit?");
        btnRemove.setText("SUBMIT");
        tvTitle.setVisibility(View.GONE);
//        tvTitle.setText("Collaboration Request");
        btnRemove.setOnClickListener(v -> {
            feedbackdialog.dismiss();
            dialog.dismiss();
            submitRating(projectid,Rating,Description);
        });

        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void submitRating(String project_id,String rating,String description) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call = apiInterface.projectrating("Bearer "+accessToken,project_id,rating,description);
        call.enqueue(new Callback<DeleteTalents>() {
            @Override
            public void onResponse(Call<DeleteTalents> call, Response<DeleteTalents> response) {
                dialog.dismiss();
                DeleteTalents user = response.body();
                if(user!=null){
                    Toast.makeText(getContext(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DeleteTalents> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        listTalentsSpinner.clear();
        listProductSpinner.clear();
        getUserDetails();
    }

//    @Override
//    public void onDetach() {
//        super.onDetach();
//        startActivity(new Intent(getContext(),UserHomeActivity.class));
//    }

    public static class ConnectionAdapter extends RecyclerView.Adapter<ConnectionAdapter.MyViewHolder> {

        private final List<ConnectionModel> list;
        Context context;
        private ItemClickListener itemClickListener;
        public ConnectionAdapter(Context context, List<ConnectionModel> list,
                                     ItemClickListener itemClickListener) {
            this.context=context;
            this.list=list;
            this.itemClickListener=itemClickListener;

        }

        @NotNull
        @Override
        public ConnectionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.conncoll, parent, false);
            return new ConnectionAdapter.MyViewHolder(itemView);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull ConnectionAdapter.MyViewHolder holder, int position) {
            Glide.with(context).load(list.get(position).getProfile_image()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.imgProfile);


            holder.tvUsername.setVisibility(View.GONE);
            holder.imgProfile.setOnClickListener(v -> itemClickListener.itemClick(position));
        }
        @Override
        public int getItemCount() {
            return list.size();
        }

        static class MyViewHolder extends RecyclerView.ViewHolder {

            CircleImageView imgProfile;
            CardView layoutStory;
            TextView tvUsername;
            MyViewHolder(View view) {
                super(view);
                imgProfile = view.findViewById(R.id.imgProfile);
                layoutStory = view.findViewById(R.id.layoutStory);
                tvUsername = view.findViewById(R.id.tvUsername);
            }
        }
    }

    private void showFeedbackDialog() {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dailog_feedback);

        RecyclerView rvFeedback = dialog.findViewById(R.id.rvFeedback);
        rvFeedback.setHasFixedSize(true);
        rvFeedback.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false));
        rvFeedback.addItemDecoration(new LinePagerIndicatorDecoration());
        feedbackListexpand(String.valueOf(userId),rvFeedback);
        dialog.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }

    public void feedbackListexpand(String id,RecyclerView rvFeedback1) {
//        final Dialog dialog = new Dialog(getContext());
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.dialog_login);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<FeedbackModel> call = apiInterface.feedbackList("Bearer "+accessToken,id,"project");
        call.enqueue(new Callback<FeedbackModel>() {
            @Override
            public void onResponse(Call<FeedbackModel> call, Response<FeedbackModel> response) {
//                dialog.dismiss();
                FeedbackModel user = response.body();
                assert user != null;
                if(user.getData().size()>0){

                    FeedbackAdapterExpand feedbackAdapter = new FeedbackAdapterExpand(getContext(),
                            user.getData(), pos -> {
//                        showFeedbackDialog();
                    });
                    rvFeedback1.setAdapter(feedbackAdapter);
                }

            }

            @Override
            public void onFailure(Call<FeedbackModel> call, Throwable t) {
//                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public static class FeedbackAdapterExpand extends RecyclerView.Adapter<FeedbackAdapterExpand.ViewHolder> {
        private Context context;
        private List<FeedbackModel.Data> list;
        private ItemClickListener itemClickListener;
        public FeedbackAdapterExpand(Context context,List<FeedbackModel.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public FeedbackAdapterExpand.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.item_expand_feedback, parent, false);
            return new FeedbackAdapterExpand.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(FeedbackAdapterExpand.ViewHolder holder, final int position) {
            Glide.with(context).load(list.get(position).getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.imgProfile);
            holder.tv_UserName.setText(list.get(position).getProject_name());
            holder.textViewDescription.setText(list.get(position).getMassage());
            holder.rating_feedback.setRating(Float.parseFloat(String.valueOf(list.get(position).getRating())));

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            CircleImageView imgProfile;
            TextView tv_UserName,textViewDescription;
            RatingBar rating_feedback;
            CardView cardView;
            public ViewHolder(View itemView) {
                super(itemView);
                imgProfile = itemView.findViewById(R.id.imgProfile);
                tv_UserName = itemView.findViewById(R.id.tvName);
                rating_feedback = itemView.findViewById(R.id.simpleRatingBar);
                textViewDescription = itemView.findViewById(R.id.textViewDescription);
                cardView = itemView.findViewById(R.id.cardView);
            }
        }
    }

    public void feedbackList(String id) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<FeedbackModel> call = apiInterface.feedbackList("Bearer "+accessToken,id,"project");
        call.enqueue(new Callback<FeedbackModel>() {
            @Override
            public void onResponse(Call<FeedbackModel> call, Response<FeedbackModel> response) {
                dialog.dismiss();
                FeedbackModel user = response.body();
                assert user != null;
                if(user.getData().size()>0){
                    rvFeedback.setVisibility(View.VISIBLE);
                    tvFeedback.setVisibility(View.VISIBLE);
                    FeedbackAdapter feedbackAdapter = new FeedbackAdapter
                            (getContext(),
                            user.getData(), pos -> {
                                showFeedbackDialog();
                    });
                    rvFeedback.setAdapter(feedbackAdapter);
                }
                else {
                    rvFeedback.setVisibility(View.GONE);
                    tvFeedback.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<FeedbackModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public static class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {
        private Context context;
        private List<FeedbackModel.Data> list;
        private ItemClickListener itemClickListener;
        public FeedbackAdapter(Context context,List<FeedbackModel.Data> list,ItemClickListener itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public FeedbackAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.feedback_item_list, parent, false);
            return new FeedbackAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(FeedbackAdapter.ViewHolder holder, final int position) {
            Glide.with(context).load(list.get(position).getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.imgProfile);
            holder.tv_UserName.setText(list.get(position).getProject_name());
            holder.textViewDescription.setText(list.get(position).getMassage());
            holder.rating_feedback.setRating(Float.parseFloat(String.valueOf(list.get(position).getRating())));
            holder.cardView.setOnClickListener(v -> itemClickListener.itemClick(position));
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            CircleImageView imgProfile;
            TextView tv_UserName,textViewDescription;
            RatingBar rating_feedback;
            CardView cardView;
            public ViewHolder(View itemView) {
                super(itemView);
                imgProfile = itemView.findViewById(R.id.imgProfile);
                tv_UserName = itemView.findViewById(R.id.tvName);
                rating_feedback = itemView.findViewById(R.id.simpleRatingBar);
                textViewDescription = itemView.findViewById(R.id.textViewDescription);
                cardView = itemView.findViewById(R.id.cardView);
            }
        }
    }

}