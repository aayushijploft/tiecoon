package com.twc.tiecoon.Fragments;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.novoda.merlin.MerlinsBeard;
import com.twc.tiecoon.Activities.ChatActivity;
import com.twc.tiecoon.Activities.OtherUserProfileActivity;
import com.twc.tiecoon.Activities.ProductDetailActivity;
import com.twc.tiecoon.Activities.ProductServiceBrandsBuyActivity;
import com.twc.tiecoon.Activities.ProjectFeedback;
import com.twc.tiecoon.Activities.ProjectInvitationActivity;
import com.twc.tiecoon.Activities.ServiceDetailsActivity;
import com.twc.tiecoon.Activities.UserHomeActivity;
import com.twc.tiecoon.Activities.ViewAssignment;
import com.twc.tiecoon.Adapters.NotificationAdapter;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragment;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.DeleteTalents;
import com.twc.tiecoon.network.response.NotificationData;
import com.twc.tiecoon.network.response.NotificationDataDetails;
import com.twc.tiecoon.network.response.ProjectRequest;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends AbstractFragment {
    private final ProfileModel profileModel = new ProfileModel();
    private final List<NotificationDataDetails> notificationDatalist = new ArrayList<>();
    RecyclerView recycler_notifications;
    RelativeLayout relative_parent;
    RelativeLayout rLNoData;
    NotificationAdapter notificationAdapter;
    private View view;
    private MerlinsBeard merlinsBeard;
    SwipeRefreshLayout swiperefresh;


    @Override
    protected View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notification, container, false);

        merlinsBeard = new MerlinsBeard.Builder()
                .build(getContext());
        swiperefresh = view.findViewById(R.id.swiperefresh);
        recycler_notifications = view.findViewById(R.id.recycler_notifications);
        relative_parent = view.findViewById(R.id.relative_parent);
        rLNoData = view.findViewById(R.id.rLNoData);

        notificationAdapter = new NotificationAdapter(getContext(), notificationDatalist, pos -> {
            switch (notificationDatalist.get(pos).getClick_action()) {
                case "notificationTalent": {
                    Intent intent = new Intent(getContext(), OtherUserProfileActivity.class);
                    intent.putExtra("UserId", String.valueOf(notificationDatalist.get(pos).getUser_id()));
                    startActivity(intent);
                    break;
                }
                case "notificationTalant": {
                    Intent intent = new Intent(view.getContext(), ProductServiceBrandsBuyActivity.class);
                    intent.putExtra("talentData", notificationDatalist.get(pos).getTalentData());
//                intent.putExtra("qbID", qbID);
                    startActivity(intent);
                    break;
                }
                case "endproject": {
                    Intent intent = new Intent(getContext(), ProjectFeedback.class);
                    intent.putExtra("project_id", notificationDatalist.get(pos).getProject_id());
                    startActivity(intent);
                    break;
                }
                case "collaborationrequest": {
                    Intent intent = new Intent(getContext(), ProjectInvitationActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;
                }case "usercollaborationrequest": {
//                    Intent intent = new Intent(getContext(), ProjectInvitationActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(intent);
                    userCollab( notificationDatalist.get(pos).getName(), notificationDatalist.get(pos).getProject_name());
                    break;
                }
                case "refund": {
                    Intent intent = new Intent(getContext(), ViewAssignment.class);
                    intent.putExtra("project_id", notificationDatalist.get(pos).getProject_id());
                    intent.putExtra("qbID", notificationDatalist.get(pos).getQbID());
                    intent.putExtra("name", notificationDatalist.get(pos).getName());
                    startActivity(intent);
                    break;
                }
                case "notificationProject": {
                    Intent intent = new Intent(getContext(), ViewAssignment.class);
                    intent.putExtra("project_id", notificationDatalist.get(pos).getProject_id());
                    intent.putExtra("qbID", notificationDatalist.get(pos).getQbID());
                    intent.putExtra("name", notificationDatalist.get(pos).getName());
                    startActivity(intent);
                    break;
                }
                case "parchage": {
                    Intent intent = new Intent(getContext(), OtherUserProfileActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("UserId", String.valueOf(notificationDatalist.get(pos).getUser_id()));
                    intent.putExtra("tag", "1");
                    startActivity(intent);
                    break;
                }
                case "talentparchage": {
                    Intent intent = new Intent(view.getContext(), ProductServiceBrandsBuyActivity.class);
                    intent.putExtra("talentData", notificationDatalist.get(pos).getTalentData());
//                intent.putExtra("qbID", qbID);
                    startActivity(intent);
                    break;
                } case "serviceparchage": {
                    Intent intent = new Intent(view.getContext(), ServiceDetailsActivity.class);
                    intent.putExtra("service_id", notificationDatalist.get(pos).getProject_id());
//                intent.putExtra("qbID", qbID);
                    startActivity(intent);
                    break;
                } case "productparchage": {
                    Intent intent = new Intent(view.getContext(), ProductDetailActivity.class);
                    intent.putExtra("product_id", notificationDatalist.get(pos).getProject_id());
//                intent.putExtra("qbID", qbID);
                    startActivity(intent);
                    break;
                }
                case "amountrelese":
                    release();
                    break;
                case "remove":
                    remove();
                    break;
            }
        });

        recycler_notifications.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        recycler_notifications.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(view.getContext(), linearLayoutManager.getOrientation());
        recycler_notifications.addItemDecoration(dividerItemDecoration);
        recycler_notifications.setAdapter(notificationAdapter);
        swiperefresh.setOnRefreshListener(() -> {
            if (Utils.isDeviceOnline(view.getContext())) {
                if(merlinsBeard.isConnected()){
                    notificationDatalist.clear();
                    getNotification();
                }
                else Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        });
        if(merlinsBeard.isConnected()){
            getNotification();
        }
        else Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

        return view;
    }

    private void release() {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("Your amount will be credited on your wallet on My Transaction page in 7-10 days");
        btnRemove.setText("OK");
        tvTitle.setVisibility(View.GONE);
        btnCancle.setVisibility(View.GONE);
//        tvTitle.setText("Collaboration Request");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();

        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    private void userCollab(String userName,String projectName) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("Add "+userName+" to your "+projectName+" project from\nMessages > Collabs > Your Project > Add to project");
        btnRemove.setText("OK");
        tvTitle.setVisibility(View.GONE);
        btnCancle.setVisibility(View.GONE);
//        tvTitle.setText("Collaboration Request");
        btnRemove.setOnClickListener(v -> {

            dialog.dismiss();
            startActivity(new Intent(getContext(),ProjectInvitationActivity.class));

        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    private void remove() {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("You are removed from the project. If you have any concerns, Write to us on Help and Support page with description");
        btnRemove.setText("OK");
        tvTitle.setVisibility(View.GONE);
        btnCancle.setVisibility(View.GONE);
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void getNotification() {
        if (Utils.isDeviceOnline(view.getContext())) {
            Utils.showProDialog(view.getContext());
            profileModel.getNotificationList(view.getContext(), relative_parent);
        } else {
            Utils.warningSnackBar(view.getContext(), relative_parent, getString(R.string.no_internet));
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        swiperefresh.setRefreshing(false);
        NotificationData notificationData = (NotificationData) arg;
        List<NotificationDataDetails> notificationDataDetails = notificationData.getData();
        if(notificationDataDetails.size()>0){
            rLNoData.setVisibility(View.GONE);
            recycler_notifications.setVisibility(View.VISIBLE);
            if (notificationDataDetails != null && !(notificationDataDetails.isEmpty())) {
                notificationDatalist.addAll(notificationDataDetails);
                notificationAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(view.getContext(), "No Notification found", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            rLNoData.setVisibility(View.VISIBLE);
            recycler_notifications.setVisibility(View.GONE);
        }

        NotificationSee();
    }

    public void NotificationSee() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call = apiInterface.NotificationSee("Bearer "+accessToken);
        call.enqueue(new Callback<DeleteTalents>() {
            @Override
            public void onResponse(Call<DeleteTalents> call, Response<DeleteTalents> response) {
                dialog.dismiss();
                DeleteTalents user = response.body();
                assert user != null;
                if(user.getSuccess()){
                    removeBadge(UserHomeActivity.bottom_navigation,R.id.notificationsFragment);
                }


            }

            @Override
            public void onFailure(Call<DeleteTalents> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (itemView.getChildCount() == 3) {
            itemView.removeViewAt(2);
        }
    }

}