package com.twc.tiecoon.Fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.google.android.material.tabs.TabLayout;
import com.twc.tiecoon.Activities.ProjectInvitationActivity;
import com.twc.tiecoon.Activities.UserHomeActivity;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.DeleteTalents;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.twc.tiecoon.Fragments.HomeFragment.removeBadge1;


public class ProjectCollabFragment extends Fragment {
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_project_collab, container, false);
        viewPager = view.findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
//        view.findViewById(R.id.iv_back).setOnClickListener(v -> getActivity().onBackPressed());
        CollabSee();
        return view;
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0)
            {
                fragment = new ReceiveInvitationFragment();
            }
            if (position == 1)
            {
                fragment = new SentInvitationFragment();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;

            if (position == 0)
            {
                title = "On My Project";
            }
            else if (position == 1)
            {
                title = "On Other's Project";
            }
            return title;
        }
    }

    public void CollabSee() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<DeleteTalents> call = apiInterface.CollabSee("Bearer "+accessToken);
        call.enqueue(new Callback<DeleteTalents>() {
            @Override
            public void onResponse(Call<DeleteTalents> call, Response<DeleteTalents> response) {
                dialog.dismiss();
                DeleteTalents user = response.body();
                assert user != null;
                if(user.getSuccess()){
                    removeBadge1(UserHomeActivity.bottom_navigation,R.id.projectFragment);
                }


            }

            @Override
            public void onFailure(Call<DeleteTalents> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


}