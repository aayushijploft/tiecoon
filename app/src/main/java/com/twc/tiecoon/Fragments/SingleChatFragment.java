package com.twc.tiecoon.Fragments;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Adapters.SingleChatAdapter;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragment;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.OnetooneChatModel;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.Observable;


public class SingleChatFragment extends AbstractFragment {

    private final ProfileModel profileModel = new ProfileModel();
    RecyclerView rvChat;
    LinearLayout linearParent;
    TextView tvNoResult;
    SwipeRefreshLayout swiperefresh;

    @Override
    protected View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_single_chat, container, false);
        rvChat = view.findViewById(R.id.rvChat);
        linearParent = view.findViewById(R.id.linearParent);
        tvNoResult = view.findViewById(R.id.tvNoResult);
        swiperefresh = view.findViewById(R.id.swiperefresh);
        rvChat.setHasFixedSize(true);
        rvChat.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
        getChatList();
        swiperefresh.setOnRefreshListener(() -> {
            if (Utils.isDeviceOnline(view.getContext())) {
                    getChatList();
            }
        });
        return view;
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    @Override
    public void update(Observable o, Object arg) {
        swiperefresh.setRefreshing(false);
        if (arg instanceof OnetooneChatModel) {
            OnetooneChatModel onetooneChatModel = (OnetooneChatModel) arg;

            if(onetooneChatModel.getData().size()>0){
                tvNoResult.setVisibility(View.GONE);
                rvChat.setVisibility(View.VISIBLE);
                SingleChatAdapter singleChatAdapter = new SingleChatAdapter(getContext(),onetooneChatModel.getData(),pos -> {
                    ProgressDialog progressDialog ;
                    progressDialog = new ProgressDialog(getContext());
                    progressDialog.setMessage("Connecting to chat, please wait...");
                    progressDialog.setIndeterminate(false);
                    progressDialog.setCancelable(true);
                    progressDialog.show();
                    ArrayList<Integer> occupantIdsList = new ArrayList<Integer>();
                    occupantIdsList.add(onetooneChatModel.getData().get(pos).getQuickbloxuser_id());
                    QBChatDialog dialog = new QBChatDialog();
                    dialog.setType(QBDialogType.PRIVATE);
                    dialog.setOccupantsIds(occupantIdsList);

                    QBRestChatService.createChatDialog(dialog).performAsync(new QBEntityCallback<QBChatDialog>() {
                        @Override
                        public void onSuccess(QBChatDialog result, Bundle params) {
                            Log.e("result",result.toString());
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            ChatActivity.startForResult(getActivity(),101, result,onetooneChatModel.getData().get(pos).getName(),"");
                        }

                        @Override
                        public void onError(QBResponseException responseException) {
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), "ERRROR", Toast.LENGTH_SHORT).show();
                        }
                    });
                });
                rvChat.setAdapter(singleChatAdapter);

            }
            else {
                tvNoResult.setVisibility(View.VISIBLE);
                rvChat.setVisibility(View.GONE);
            }
        }
    }

    private void getChatList() {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            profileModel.getOnetooneChatList(getActivity(), linearParent);
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            Log.i("IsRefresh", "Yes");
        }
    }

}