package com.twc.tiecoon.Fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.twc.tiecoon.Activities.AddKYCActivity;
import com.twc.tiecoon.Activities.TransactionActivity;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragment;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AccountVerifyModel;
import com.twc.tiecoon.network.response.BankDetails;
import com.twc.tiecoon.network.response.GetBankDetails;
import com.twc.tiecoon.network.response.WithdrawModel;
import com.twc.tiecoon.utils.Utils;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class KycAccountFragment extends AbstractFragment {

    private final ProfileModel profileModel = new ProfileModel();
    EditText et_name,et_bankname,et_accountno,et_ifsc,et_phoneno,et_paypalEmail,et_upi,et_verify;
    Button btnSave,btnWithdraw;
    RelativeLayout relative_parent;
    TextView tvemail,tvupi,tvphoneno,tvifsc,tvaccountno,tvbankname,tvName,tvEarnings,tvVerifyAccount;
    String WithdrwaAmount = "";
    String Status = "0";


    @Override
    protected View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_kyc_account, container, false);
        et_name = view.findViewById(R.id.et_name);
        et_bankname = view.findViewById(R.id.et_bankname);
        et_accountno = view.findViewById(R.id.et_accountno);
        et_ifsc = view.findViewById(R.id.et_ifsc);
        et_phoneno = view.findViewById(R.id.et_phoneno);
        et_paypalEmail = view.findViewById(R.id.et_paypalEmail);
        relative_parent = view.findViewById(R.id.relative_parent);
        btnSave = view.findViewById(R.id.btnSave);
        tvemail = view.findViewById(R.id.tvemail);
        tvupi = view.findViewById(R.id.tvupi);
        tvphoneno = view.findViewById(R.id.tvphoneno);
        tvifsc = view.findViewById(R.id.tvifsc);
        tvaccountno = view.findViewById(R.id.tvaccountno);
        tvbankname = view.findViewById(R.id.tvbankname);
        tvName = view.findViewById(R.id.tvName);
        tvEarnings = view.findViewById(R.id.tvEarnings);
        btnWithdraw = view.findViewById(R.id.btnWithdraw);
        et_upi = view.findViewById(R.id.et_upi);
        tvVerifyAccount = view.findViewById(R.id.tvVerifyAccount);
        et_verify = view.findViewById(R.id.et_verify);
        getBankDetails();
        btnSave.setOnClickListener(v -> CheckAssignmentValidation());
        btnWithdraw.setOnClickListener(v -> {
            if(Status.equals("0")){
                Toast.makeText(getContext(), "Please Add Bank Details", Toast.LENGTH_SHORT).show();
            }
            else withdrawConfirmation();
        });
        tvVerifyAccount.setOnClickListener(v -> {
            if(TextUtils.isEmpty(et_verify.getText().toString())){
                Toast.makeText(getContext(), "Please Enter Amount", Toast.LENGTH_SHORT).show();
                return;
            }

            accountVerify(et_verify.getText().toString());
        });

        return view;
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    private void withdrawConfirmation() {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        btnRemove.setText("CONFIRM");
        tvConfirm.setText("Check your Account number/Paypal details in KYC page before pressing the Confirm.");

        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            sendWithdrawRequest();
        });

        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void sendWithdrawRequest() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<WithdrawModel> call = apiInterface.withdrawAmount("Bearer "+accessToken,WithdrwaAmount,"INR");

        call.enqueue(new Callback<WithdrawModel>() {
            @Override
            public void onResponse(@NotNull Call<WithdrawModel> call, @NotNull Response<WithdrawModel> response) {
                dialog.dismiss();
                WithdrawModel user = response.body();
                Log.e("user",user.toString());
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                if (Build.VERSION.SDK_INT >= 26) {
                    ft.setReorderingAllowed(false);
                }
                ft.detach(KycAccountFragment.this).attach(KycAccountFragment.this).commit();
                Toast.makeText(getContext(), "Your withdraw request has been sent to Admin", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<WithdrawModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
    public void accountVerify(String amount) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<AccountVerifyModel> call = apiInterface.AccountVerify("Bearer "+accessToken,amount);

        call.enqueue(new Callback<AccountVerifyModel>() {
            @Override
            public void onResponse(@NotNull Call<AccountVerifyModel> call, @NotNull Response<AccountVerifyModel> response) {
                dialog.dismiss();
                AccountVerifyModel user = response.body();
                Log.e("user",user.toString());
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                if (Build.VERSION.SDK_INT >= 26) {
                    ft.setReorderingAllowed(false);
                }
                ft.detach(KycAccountFragment.this).attach(KycAccountFragment.this).commit();
                Toast.makeText(getContext(), user.message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<AccountVerifyModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    private void CheckAssignmentValidation() {

        if(!et_upi.getText().toString().equals("")){
            if (et_upi.getText().toString().trim().isEmpty()) {
                Toast.makeText(getContext(), "Please enter upi", Toast.LENGTH_SHORT).show();
                return;
            }

        }
      else if(!et_paypalEmail.getText().toString().equals("")){
            if (et_paypalEmail.getText().toString().trim().isEmpty()) {
                Toast.makeText(getContext(), "Please enter paypal email", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!(Utils.isValidEmail(et_paypalEmail.getText().toString().trim()))) {
                Toast.makeText(getContext(), "Email not valid", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        else {
            if (et_name.getText().toString().trim().isEmpty()) {
                Toast.makeText(getContext(), "Name can't be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            if (et_bankname.getText().toString().trim().isEmpty()) {
                Toast.makeText(getContext(), "Bank name can't be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            if (et_accountno.getText().toString().trim().isEmpty()) {
                Toast.makeText(getContext(), "Account Number can't be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            if (et_ifsc.getText().toString().trim().isEmpty()) {
                Toast.makeText(getContext(), "IFSC code can't be empty", Toast.LENGTH_SHORT).show();
                return;
            }
            if (et_phoneno.getText().toString().trim().isEmpty()) {
                Toast.makeText(getContext(), "Phone Number can't be empty", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        confirm();

    }

    private void confirm() {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("Are you sure, you want to submit this details?");
        btnRemove.setText("SUBMIT");
        tvTitle.setVisibility(View.GONE);
//        tvTitle.setText("Collaboration Request");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            SubmitSupport(et_name.getText().toString(),et_bankname.getText().toString(),et_accountno.getText().toString(),
                    et_ifsc.getText().toString(),et_phoneno.getText().toString(),et_paypalEmail.getText().toString(),et_upi.getText().toString());

        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    private void SubmitSupport(String name,String bank_name,String account_no,String ifsc,String phone,String email,String et_upi) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            Map<String, String> map = new HashMap<>();
            map.put("name", name);
            map.put("bankname", bank_name);
            map.put("account_number",account_no );
            map.put("ifsc_code", ifsc);
            map.put("phone_number", phone);
            map.put("paypal_email", email);
            map.put("upi", et_upi);
            profileModel.addBankDetails(getActivity(), relative_parent,map);
        } else {
            Utils.warningSnackBar(getContext(), relative_parent, getString(R.string.no_internet));
        }
    }


    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof BankDetails) {
            Toast.makeText(getContext(),"saved successfully",Toast.LENGTH_SHORT).show();
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            if (Build.VERSION.SDK_INT >= 26) {
                ft.setReorderingAllowed(false);
            }
            ft.detach(this).attach(this).commit();
        }

        if (arg instanceof GetBankDetails) {
            GetBankDetails getBankDetails = (GetBankDetails) arg;
            if(getBankDetails.getData()!=null){
                et_name.setText(getBankDetails.getData().getName());
                et_bankname.setText(getBankDetails.getData().getBankname());
                et_accountno.setText(getBankDetails.getData().getAccount_number());
                et_ifsc.setText(getBankDetails.getData().getIfsc_code());
                et_phoneno.setText(getBankDetails.getData().getPhone_number());
                et_paypalEmail.setText(getBankDetails.getData().getPaypal_email());
                et_upi.setText(getBankDetails.getData().getUpi());
                tvName.setText(getBankDetails.getData().getName());
                tvbankname.setText(getBankDetails.getData().getBankname());
                tvaccountno.setText(getBankDetails.getData().getAccount_number());
                tvifsc.setText(getBankDetails.getData().getIfsc_code());
                tvphoneno.setText(getBankDetails.getData().getPhone_number());
                tvemail.setText(getBankDetails.getData().getPaypal_email());
                tvupi.setText(getBankDetails.getData().getUpi());
                tvEarnings.setText("Rs."+getBankDetails.getTotal_earning().getInr());
                WithdrwaAmount = getBankDetails.getTotal_earning().getInr();
                Status = getBankDetails.getData().getStatus();

                if(getBankDetails.getData().getIsverfy().equals("1")){
                    tvVerifyAccount.setText("verified");
                    tvVerifyAccount.setCompoundDrawablesWithIntrinsicBounds
                            (getResources().getDrawable(R.drawable.ic_radio_checked)
                                    ,null, null, null);
                    tvVerifyAccount.setEnabled(false);
                    et_upi.setEnabled(false);
                }

            }
        }
    }

    public void getBankDetails(){
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            profileModel.getbankdetails(getActivity(), relative_parent);
        } else {
            Utils.warningSnackBar(getContext(), relative_parent, getString(R.string.no_internet));
        }
    }
}