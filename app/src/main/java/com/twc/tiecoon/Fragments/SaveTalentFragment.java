package com.twc.tiecoon.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.twc.tiecoon.Activities.ProductServiceBrandsBuyActivity;
import com.twc.tiecoon.Adapters.OtherProfileTalentHorizontalAdapters;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.cmds.AbstractFragment;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.SaveTalent;
import com.twc.tiecoon.network.response.SaveTalentData;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class SaveTalentFragment extends AbstractFragment {
    private HomeModel homeModel = new HomeModel();
    private LinearLayout linearParent;
    private OtherProfileTalentHorizontalAdapters otherProfileTalentHorizontalAdapters;
    List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList=new ArrayList<>();


    @Override
    protected View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_save_talent, container, false);
        linearParent = view.findViewById(R.id.linearParent);
        RecyclerView recyclerViewSavedTalents = view.findViewById(R.id.recyclerViewSavedTalents);
        otherProfileTalentHorizontalAdapters = new OtherProfileTalentHorizontalAdapters(getContext(),
                homeUserDetailsUploadTalentList, pos -> {
            Intent intent=new Intent(getContext(), ProductServiceBrandsBuyActivity.class);
            intent.putExtra("talentData",homeUserDetailsUploadTalentList.get(pos));
            startActivity(intent);
        });
        recyclerViewSavedTalents.setAdapter(otherProfileTalentHorizontalAdapters);

        getSavedBrands();
        return view;
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    @Override
    public void update(Observable observable, Object o) {
        SaveTalent saveTalent = (SaveTalent) o;
        List<SaveTalentData> saveTalentDataList = saveTalent.getData();
        if (saveTalentDataList != null && !(saveTalentDataList.isEmpty())) {
            for (SaveTalentData saveTalentData : saveTalentDataList) {
                if (saveTalentData != null && saveTalentData.getTalent() != null) {
                    homeUserDetailsUploadTalentList.add(saveTalentData.getTalent());
                }
            }
            otherProfileTalentHorizontalAdapters.notifyDataSetChanged();
        } else {
            Toast.makeText(getContext(), "No saved talents", Toast.LENGTH_SHORT).show();
        }
    }

    private void getSavedBrands() {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            homeModel.getSavedTalentsList(getContext(), linearParent);
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }
}