package com.twc.tiecoon.Fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.braintreepayments.cardform.view.CardForm;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.cmds.AbstractFragment;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.AddKYC;
import com.twc.tiecoon.network.response.CardResponse;
import com.twc.tiecoon.network.response.KycDetails;
import com.twc.tiecoon.utils.Utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class KycCardFragment extends AbstractFragment {
    private final ProfileModel profileModel = new ProfileModel();
    LinearLayout linearParent;
    CardForm cardForm;
    WebView webview;

    @Override
    protected View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_kyc_card, container, false);
         cardForm = view.findViewById(R.id.card_form);
        linearParent = view.findViewById(R.id.linearParent);
        webview = view.findViewById(R.id.webview);
        cardForm.cardRequired(true)
                .expirationRequired(true)
                .cvvRequired(true)
                .cardholderName(CardForm.FIELD_REQUIRED)
                .postalCodeRequired(false)
                .actionLabel("VERIFY")
                .setup((AppCompatActivity) getActivity());

        view.findViewById(R.id.btnSave).setOnClickListener(v -> {
            CheckAssignmentValidation();
        });
        getDetails();
        return view;
    }

    private void CheckAssignmentValidation() {
        if(cardForm.getCardholderName().equals("")){
            Toast.makeText(getContext(), "Name can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if(cardForm.getCardNumber().equals("")){
            Toast.makeText(getContext(), "Card Number can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if(cardForm.getExpirationMonth().equals("")){
            Toast.makeText(getContext(), "Month can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if(cardForm.getExpirationYear().equals("")){
            Toast.makeText(getContext(), "Year can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if(cardForm.getCvv().equals("")){
            Toast.makeText(getContext(), "CVV can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }

        SubmitSupport(cardForm.getCardholderName(),cardForm.getCardNumber(),cardForm.getCvv(),cardForm.getExpirationMonth(),cardForm.getExpirationYear());
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof AddKYC) {
            AddKYC addKYC = (AddKYC) arg;
//            VerifyCard(addKYC.getData().getId()+"");
            webview.getSettings().setJavaScriptEnabled(true);
            webview.loadUrl("https://tiecoonapp.com/demo2/public/kyc/verfication/"+addKYC.getData().getId());
            Toast.makeText(getContext(),"card verified",Toast.LENGTH_SHORT).show();
//            getActivity().onBackPressed();
        }
        if (arg instanceof KycDetails) {
            KycDetails kycDetails = (KycDetails) arg;
            if(kycDetails!=null){
//                cardForm.cardholderName(Integer.parseInt(kycDetails.getData().getName()));
//                cardForm.card(kycDetails.getData().getCard_number());
//                cardForm.setExpirationError(kycDetails.getData().getExpiration_month()+"/"+kycDetails.getData().getExpiration_year());
//                cardForm.setCvvError(kycDetails.getData().getCvc());

            }

        }
    }

    public void getDetails(){
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            profileModel.KYCDetails(getActivity(), linearParent);

        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    private void SubmitSupport(String name,String card_number,String cvc,String expiration_month,String expiration_year) {
        if (Utils.isDeviceOnline(getContext())) {
            Utils.showProDialog(getContext());
            Map<String, String> map = new HashMap<>();
            map.put("nameofcard", name);
            map.put("card_number", card_number);
            map.put("cvc", cvc);
            map.put("expiration_month", expiration_month);
            map.put("expiration_year", expiration_year);
            profileModel.addKYCDetails(getActivity(), linearParent,map);

        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }

//    https://tiecoonapp.com/demo2/public/stripe/payment/1
//    https://tiecoonapp.com/demo2/public/stripe/payment/0
    public void VerifyCard(String id) {
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        Call<CardResponse> call = apiInterface.CardVerify(id);

        call.enqueue(new Callback<CardResponse>() {
            @Override
            public void onResponse(Call<CardResponse> call, Response<CardResponse> response) {
                CardResponse user = response.body();
                Log.e("user",user.toString());
            }

            @Override
            public void onFailure(Call<CardResponse> call, Throwable t) {
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

}