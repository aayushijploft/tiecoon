package com.twc.tiecoon.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.twc.tiecoon.Activities.ProductServiceBrandsBuyActivity;
import com.twc.tiecoon.Adapters.UserHomeTabTalentAdapters;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.utils.FragmentUploadTalentListListner;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class HomeUserTalentFragment extends Fragment implements FragmentUploadTalentListListner {
    private UserHomeTabTalentAdapters userHomeTabTalentAdapters;
    private List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = new ArrayList<>();
    private HomeUserDetailData homeUserDetailData;
    private int posit;
    String qbID;
    String liked="";
    RelativeLayout rLNoData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            homeUserDetailsUploadTalentList = getArguments().getParcelableArrayList("talent");
            posit=getArguments().getInt("pos");
            Log.e("telent","list"+homeUserDetailsUploadTalentList);
            Log.e("pos","list"+posit);
            qbID = getArguments().getString("qbID");
        }
        return inflater.inflate(R.layout.fragment_talent, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView_talents = view.findViewById(R.id.recyclerView_talents);
        rLNoData= view.findViewById(R.id.rLNoData);
        recyclerView_talents.setHasFixedSize(true);
        recyclerView_talents.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false));
        if(homeUserDetailsUploadTalentList.size()>0){
            userHomeTabTalentAdapters = new UserHomeTabTalentAdapters(view.getContext(), homeUserDetailsUploadTalentList, new ItemClickListener() {
                @Override
                public void itemClick(int pos) {
                    rLNoData.setVisibility(View.GONE);
                    recyclerView_talents.setVisibility(View.VISIBLE);
                    liked = homeUserDetailsUploadTalentList.get(pos).getFontsize();
                    ApplicationClass.uploadImageListDataArrayList.clear();
                    if (homeUserDetailsUploadTalentList.get(pos).getUpload_image() != null && !(homeUserDetailsUploadTalentList.get(pos).getUpload_image().isEmpty())) {
                        ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsUploadTalentList.get(pos).getUpload_image());
                    }

                    Intent intent = new Intent(view.getContext(), ProductServiceBrandsBuyActivity.class);
                    posit = pos;
                    intent.putExtra("talentData", homeUserDetailsUploadTalentList.get(pos));
//                    intent.putExtra("name", homeUserDetailData.getHomeUserDetailsUserDetils().getName());
                    intent.putExtra("qbID", qbID);

                    startActivityForResult(intent,100);
                }
            });
            recyclerView_talents.setAdapter(userHomeTabTalentAdapters);
        }
        else {
            rLNoData.setVisibility(View.VISIBLE);
            recyclerView_talents.setVisibility(View.GONE);
        }

    }

    @Override
    public void setList(List<HomeUserDetailsUploadTalent> list) {
        homeUserDetailsUploadTalentList.clear();
        homeUserDetailsUploadTalentList.addAll(list);
        if (userHomeTabTalentAdapters != null)
            userHomeTabTalentAdapters.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {

            if (resultCode == RESULT_OK) {
                Log.e("dataaa","Dattaaaaa");
                String returnedResult = data.getStringExtra("key");
                Log.e("returlike",returnedResult);
                if(returnedResult.equals("1")){
                    homeUserDetailsUploadTalentList.get(posit).setFontsize("true");
                    userHomeTabTalentAdapters.notifyDataSetChanged();
                }
                else {
                    homeUserDetailsUploadTalentList.get(posit).setFontsize("false");
                    userHomeTabTalentAdapters.notifyDataSetChanged();
                }


            }
        }
    }
}