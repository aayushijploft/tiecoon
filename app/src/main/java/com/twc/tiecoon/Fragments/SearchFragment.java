package com.twc.tiecoon.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.BuildConfig;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.twc.tiecoon.Activities.ChatActivity;
import com.twc.tiecoon.Activities.OtherUserProfileActivity;
import com.twc.tiecoon.Activities.UserHomeActivity;
import com.twc.tiecoon.Adapters.HomeScreenAdapters;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.FilterUserData;
import com.twc.tiecoon.models.HomeModel;
import com.twc.tiecoon.network.cmds.AbstractFragment;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.OtherProfileUserDetail;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import static android.app.Activity.RESULT_OK;

public class SearchFragment extends AbstractFragment implements View.OnClickListener, UserHomeActivity.FragmentData {
    private HomeModel homeModel = new HomeModel();
    private RelativeLayout linearParent;
    private ImageView imgJoinGroupOne, imgJoinGroupTwo, imgJoinGroupCreateNew;
    private RecyclerView  recyclerViewHomeFragment;
    private List<HomeUserDetailData> homeUserDetailDataList = new ArrayList<>();
    private HomeScreenAdapters homeScreenAdapters;
    private static final int USERUPDATE = 301;
    private int currentPage = 1;
    //for pagination
    private boolean isLoading = true;
    private int pastVisibleItems, visibleItemCount, totalItemCount, previousTotal = 0;
    private int viewThreshold = 1;
    private View view;
    private int updatePosition;
    private boolean noMore = false;
    private FilterUserData filterUserData=new FilterUserData();
    NestedScrollView mScrollview;
    LinearLayout layoutNoData;

    @Override
    protected View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        linearParent = view.findViewById(R.id.linearParent);
        mScrollview = view.findViewById(R.id.mScrollview);
        layoutNoData = view.findViewById(R.id.layoutNoData);
        recyclerViewHomeFragment = view.findViewById(R.id.recyclerViewHomeFragment);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL, false);
        recyclerViewHomeFragment.setLayoutManager(linearLayoutManager);
        homeScreenAdapters = new HomeScreenAdapters(view.getContext(), getFragmentManager(), homeUserDetailDataList, new ItemClickListenerExtraParam() {
            @Override
            public void itemClick(int pos, String type) {
                updatePosition = pos;
                handleItemClick(type);
            }
        });

        recyclerViewHomeFragment.setAdapter(homeScreenAdapters);

        mScrollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged()
            {
                View view = (View)mScrollview.getChildAt(mScrollview.getChildCount() - 1);

                int diff = (view.getBottom() - (mScrollview.getHeight() + mScrollview
                        .getScrollY()));

                if (diff == 0) {
                    // your pagination code
//                    recyclerViewHomeFragment.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                        @Override
//                        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                            super.onScrolled(recyclerView, dx, dy);
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
//                            if (dy > 0) {
                    if (!noMore) {
                        if (totalItemCount > previousTotal) {
                            previousTotal = totalItemCount;
                        }
                        if ((totalItemCount - visibleItemCount) <= pastVisibleItems + viewThreshold) {
                            currentPage++;
                            getUserDetails(filterUserData);
                            isLoading = true;
                        }
                    }
                }
            }
        });
        getUserDetails(filterUserData);
    }

    private void handleItemClick(String type) {
        switch (type) {
            case "like":
                setUserLike(homeUserDetailDataList.get(updatePosition).getId());
                break;
            case "chat":
                Intent intent = new Intent(view.getContext(), ChatActivity.class);
                intent.putExtra("UserId",String.valueOf( homeUserDetailDataList.get(updatePosition).getId()));
                startActivity(intent);
                break;
            case "share":
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "C_Ony");
                    String message = "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, message);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "collaborate":
                openCollaborationDialog();
                break;
            default:
                Intent i = new Intent(view.getContext(), OtherUserProfileActivity.class);
                i.putExtra("UserId", homeUserDetailDataList.get(updatePosition).getId());
                startActivityForResult(i, USERUPDATE);
                break;
        }
    }

    @Override
    protected BasicModel getModel() {
        return homeModel;
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof OtherProfileUserDetail) {
            OtherProfileUserDetail otherProfileUserDetail = (OtherProfileUserDetail) o;
            List<HomeUserDetailData> homeUserDetailDataList1 = otherProfileUserDetail.getData();
            if (homeUserDetailDataList1 != null && !(homeUserDetailDataList1.isEmpty())) {
                for (HomeUserDetailData homeUserDetailData : homeUserDetailDataList1) {
                    if (homeUserDetailData != null) {
                        if (!(homeUserDetailData.getId() == ApplicationClass.appPreferences.getUserId())) {
                            try {
                                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                if (imm != null) {
                                    imm.hideSoftInputFromWindow(mScrollview.getWindowToken(), 0);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            layoutNoData.setVisibility(View.GONE);
                            recyclerViewHomeFragment.setVisibility(View.VISIBLE);
                            homeUserDetailDataList.add(homeUserDetailData);
//                            homeUserDetailDataList.add(homeUserDetailData);
                        }
                    }
                }
                homeScreenAdapters.notifyDataSetChanged();
            } else {
                noMore = true;
                try {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(mScrollview.getWindowToken(), 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (homeUserDetailDataList.isEmpty()) {
                    layoutNoData.setVisibility(View.VISIBLE);
                    recyclerViewHomeFragment.setVisibility(View.GONE);
                }

            }
        } else if (o instanceof HomeUserDetailData) {
            HomeUserDetailData homeUserDetailData = (HomeUserDetailData) o;
            homeUserDetailDataList.set(updatePosition, homeUserDetailData);
            homeScreenAdapters.notifyItemChanged(updatePosition);
        }
    }

    private void getUserDetails(FilterUserData filterUserData) {
        if (Utils.isDeviceOnline(view.getContext())) {
            if (currentPage == 1) {
                Utils.showProDialog(view.getContext());
            }
            homeModel.getHomeUserData(view.getContext(), linearParent, currentPage,filterUserData);
        } else {
            Utils.warningSnackBar(view.getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    private void setUserLike(int id) {
        if (Utils.isDeviceOnline(view.getContext())) {
            Utils.showProDialog(view.getContext());
            homeModel.setUserLike(view.getContext(), linearParent, id);
        } else {
            Utils.warningSnackBar(view.getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == USERUPDATE) {
            if (resultCode == RESULT_OK) {
                if (ApplicationClass.homeUserDetailData != null) {
                    homeUserDetailDataList.set(updatePosition, ApplicationClass.homeUserDetailData);
                    homeScreenAdapters.notifyItemChanged(updatePosition);
                }
            }
        }
    }

    private void openCollaborationDialog() {
        Dialog dialog = new Dialog(view.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_groupchat);

        LinearLayout linearJoinGroupOne = dialog.findViewById(R.id.linearJoinGroupOne);
        LinearLayout linearJoinGroupTwo = dialog.findViewById(R.id.linearJoinGroupTwo);
        LinearLayout linearNewGroup = dialog.findViewById(R.id.linearNewGroup);
        imgJoinGroupOne = dialog.findViewById(R.id.imgJoinGroupOne);
        imgJoinGroupTwo = dialog.findViewById(R.id.imgJoinGroupTwo);
        imgJoinGroupCreateNew = dialog.findViewById(R.id.imgJoinGroupCreateNew);

        EditText etGroupName = dialog.findViewById(R.id.etGroupName);
        Button btnSubmit = dialog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(view -> dialog.dismiss());
        linearJoinGroupOne.setOnClickListener(this);
        linearJoinGroupTwo.setOnClickListener(this);
        linearNewGroup.setOnClickListener(this);

        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linearJoinGroupOne:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_checked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_unchecked);
                break;
            case R.id.linearJoinGroupTwo:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_checked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_unchecked);
                break;
            case R.id.linearNewGroup:
                imgJoinGroupOne.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupTwo.setImageResource(R.drawable.ic_radio_unchecked);
                imgJoinGroupCreateNew.setImageResource(R.drawable.ic_radio_checked);
                break;
        }
    }

    @Override
    public void sendData(FilterUserData data,String value,boolean show) {
        homeUserDetailDataList.clear();
        currentPage=1;
        filterUserData=data;
        getUserDetails(data);
    }
}