package com.twc.tiecoon.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.twc.tiecoon.Activities.ProductDetailActivity;
import com.twc.tiecoon.Activities.ProductServiceBrandsBuyActivity;
import com.twc.tiecoon.Adapters.UserHomeProductServiceAdapters;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.utils.FragmentProductServiceListListner;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class HomeProductServiceFragment extends Fragment implements FragmentProductServiceListListner {
    private List<HomeUserDetailsProduct> homeUserDetailsProductList = new ArrayList<>();
    private UserHomeProductServiceAdapters userHomeProductServiceAdapters;
    String qbID;
    RelativeLayout rLNoData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            homeUserDetailsProductList = getArguments().getParcelableArrayList("product");
            qbID = getArguments().getString("qbID");
        }
        return inflater.inflate(R.layout.fragment_product_service, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView_ProductService = view.findViewById(R.id.recyclerView_ProductService);
        rLNoData = view.findViewById(R.id.rLNoData);
        if(homeUserDetailsProductList.size()>0){
            recyclerView_ProductService.setVisibility(View.VISIBLE);
            rLNoData.setVisibility(View.GONE);
            userHomeProductServiceAdapters = new UserHomeProductServiceAdapters(view.getContext(), homeUserDetailsProductList, new ItemClickListener() {
                @Override
                public void itemClick(int pos) {
                    ApplicationClass.uploadImageListDataArrayList.clear();
                    if (homeUserDetailsProductList.get(pos).getUpload_image() != null && !(homeUserDetailsProductList.get(pos).getUpload_image().isEmpty())) {
                        ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsProductList.get(pos).getUpload_image());
                    }
                    Intent intent = new Intent(view.getContext(), ProductDetailActivity.class);
                    intent.putExtra("product_id", homeUserDetailsProductList.get(pos).getId()+"");
                    intent.putExtra("qbID", qbID);
                    startActivity(intent);
                }
            });
            recyclerView_ProductService.setAdapter(userHomeProductServiceAdapters);
        }
        else {
                recyclerView_ProductService.setVisibility(View.GONE);
            rLNoData.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void setList(List<HomeUserDetailsProduct> list) {
//        homeUserDetailsProductList.clear();
//        homeUserDetailsProductList.addAll(list);
//        if (userHomeProductServiceAdapters != null)
//            userHomeProductServiceAdapters.notifyDataSetChanged();
    }
}