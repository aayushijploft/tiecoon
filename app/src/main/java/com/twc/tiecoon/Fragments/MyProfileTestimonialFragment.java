package com.twc.tiecoon.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.twc.tiecoon.Adapters.MyprofileTestimonialAdapters;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsTestimonial;
import com.twc.tiecoon.utils.FragmentTestimonialListListner;

import java.util.ArrayList;
import java.util.List;

public class MyProfileTestimonialFragment extends Fragment implements FragmentTestimonialListListner {
    private List<HomeUserDetailsTestimonial> homeUserDetailsTestimonialList = new ArrayList<>();
    private MyprofileTestimonialAdapters myprofileTestimonialAdapters;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_profile_testimonial, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView_testimonial = view.findViewById(R.id.recyclerView_testimonial);
        myprofileTestimonialAdapters = new MyprofileTestimonialAdapters(view.getContext(), homeUserDetailsTestimonialList);
       // recyclerView_testimonial.setAdapter(myprofileTestimonialAdapters);
    }

    @Override
    public void setList(List<HomeUserDetailsTestimonial> list) {
        homeUserDetailsTestimonialList.clear();
        homeUserDetailsTestimonialList.addAll(list);
        if(myprofileTestimonialAdapters!=null)
        myprofileTestimonialAdapters.notifyDataSetChanged();
    }
}