package com.twc.tiecoon.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.twc.tiecoon.Adapters.MyprofileFeedbackAdapters;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsFeedback;
import com.twc.tiecoon.utils.FragmentFeedbackListListner;

import java.util.ArrayList;
import java.util.List;

public class MyProfilefeedbackFragment extends Fragment implements FragmentFeedbackListListner {
    private List<HomeUserDetailsFeedback> homeUserDetailsFeedbackList = new ArrayList<>();
    private MyprofileFeedbackAdapters userHomeFeedbackAdapters;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feedbacks_my_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView_Feedback = view.findViewById(R.id.recyclerView_Feedback);
        userHomeFeedbackAdapters = new MyprofileFeedbackAdapters(view.getContext(), homeUserDetailsFeedbackList);
        recyclerView_Feedback.setAdapter(userHomeFeedbackAdapters);
    }

    @Override
    public void setList(List<HomeUserDetailsFeedback> list) {
        homeUserDetailsFeedbackList.clear();
        homeUserDetailsFeedbackList.addAll(list);
//        if(userHomeFeedbackAdapters!=null)
//        userHomeFeedbackAdapters.notifyDataSetChanged();
    }
}