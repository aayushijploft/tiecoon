package com.twc.tiecoon.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Adapters.GroupChatAdapter;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragment;
import com.twc.tiecoon.network.cmds.BasicModel;
import com.twc.tiecoon.network.response.GroupChatModel;
import com.twc.tiecoon.utils.Utils;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Observable;


public class GroupChatFragment extends AbstractFragment {
    private final ProfileModel profileModel = new ProfileModel();
    RecyclerView rvChat;
    LinearLayout linearParent;
    TextView tvNoResult;
    SwipeRefreshLayout swiperefresh;
    @Override
    protected View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_group_chat, container, false);
        rvChat = view.findViewById(R.id.rvChat);
        linearParent = view.findViewById(R.id.linearParent);
        tvNoResult = view.findViewById(R.id.tvNoResult);
        swiperefresh = view.findViewById(R.id.swiperefresh);
        rvChat.setHasFixedSize(true);
        rvChat.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
        getGroupChatList();

        swiperefresh.setOnRefreshListener(() -> {
            if (Utils.isDeviceOnline(view.getContext())) {
                getGroupChatList();
            }
        });

        return  view;
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    @Override
    public void update(Observable o, Object arg) {
        swiperefresh.setRefreshing(false);
        if (arg instanceof GroupChatModel) {
            GroupChatModel groupChatModel = (GroupChatModel) arg;

            if(groupChatModel.getData().size()>0){
                tvNoResult.setVisibility(View.GONE);
                rvChat.setVisibility(View.VISIBLE);
                
                GroupChatAdapter groupChatAdapter = new GroupChatAdapter(getContext(),groupChatModel.getData(), pos -> {
                    ProgressDialog progressDialog ;
                    progressDialog = new ProgressDialog(getContext());
                    progressDialog.setMessage("Connecting to group, please wait...");
                    progressDialog.setIndeterminate(false);
                    progressDialog.setCancelable(true);
                    progressDialog.show();
                    QBRequestGetBuilder requestBuilder = new QBRequestGetBuilder();
                    requestBuilder.setLimit(100);

                    QBRestChatService.getChatDialogs(QBDialogType.PUBLIC_GROUP, requestBuilder).performAsync(
                            new QBEntityCallback<ArrayList<QBChatDialog>>() {
                                @Override
                                public void onSuccess(ArrayList<QBChatDialog> result, Bundle params) {
                                    Log.e("result1111111",result.toString());
                                    for(int i = 0; i<result.size();i++){
                                        Log.e("result",result.get(i).getDialogId()+"----------------"+groupChatModel.getData().get(pos).getDialogs_id());
                                        if(result.get(i).getDialogId().equals(groupChatModel.getData().get(pos).getDialogs_id())){
                                            if (progressDialog.isShowing()) {
                                                progressDialog.dismiss();
                                            }
                                            ChatActivity.startForResult(getActivity(),101,
                                                    result.get(i),""
                                                    ,groupChatModel.getData().get(pos).getName()
                                                    ,groupChatModel.getData().get(pos).getId()+"",
                                                    groupChatModel.getData().get(pos).getProject_id()+"",
                                                    groupChatModel.getData().get(pos).isIs_admin()+"");
                                        }
                                        Log.e("grpid",groupChatModel.getData().get(pos).getId()+"");
                                        Log.e("isadmin",groupChatModel.getData().get(pos).isIs_admin()+"");
                                        Log.e("idss",groupChatModel.getData().get(pos).getDialogs_id()+"******");
                                    }
                                }

                                @Override
                                public void onError(QBResponseException responseException) {
                                    Log.e("erroreeeee", Objects.requireNonNull(responseException.getMessage()));
                                }
                            });
                });
                rvChat.setAdapter(groupChatAdapter);
            }
            else {
                tvNoResult.setVisibility(View.VISIBLE);
                rvChat.setVisibility(View.GONE);
            }

        }

    }

    private void getGroupChatList() {
        if (Utils.isDeviceOnline(Objects.requireNonNull(getContext()))) {
            Utils.showProDialog(getContext());
            profileModel.getGroupChatList(getActivity(), linearParent);
        } else {
            Utils.warningSnackBar(getContext(), linearParent, getString(R.string.no_internet));
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            Log.i("IsRefresh", "Yes");
        }
    }

}