package com.twc.tiecoon.Fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.twc.tiecoon.Activities.OtherUserProfileActivity;
import com.twc.tiecoon.Activities.Quickblox.ChatActivity;
import com.twc.tiecoon.Activities.ViewAssignment;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.ApiClient;
import com.twc.tiecoon.network.ApiInterface;
import com.twc.tiecoon.network.response.AcceptInvite;
import com.twc.tiecoon.network.response.InivtationModel;
import com.twc.tiecoon.network.response.TransactionListModel;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SentInvitationFragment extends Fragment {
    RecyclerView rvMyProject;
    SwipeRefreshLayout swiperefresh;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_sent_invitation, container, false);
        rvMyProject = view.findViewById(R.id.rvMyProject);
        rvMyProject.setHasFixedSize(true);
        rvMyProject.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
        swiperefresh = view.findViewById(R.id.swiperefresh);
        getOtherProjectInvitation(true,"otherproject");
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOtherProjectInvitation(false,"otherproject");
            }
        });


        return view;
    }

    public void getOtherProjectInvitation(boolean isshow,String type) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if(isshow){
            dialog.show();
        }
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<InivtationModel> call = apiInterface.collabRequest("Bearer "+accessToken,type);
        call.enqueue(new Callback<InivtationModel>() {
            @Override
            public void onResponse(Call<InivtationModel> call, Response<InivtationModel> response) {
                dialog.dismiss();
                swiperefresh.setRefreshing(false);
                InivtationModel user = response.body();
                if(user.data.size() == 0){

                }
                else {
                    OtherProjectAdapter myProjectAdapter = new OtherProjectAdapter(getContext(),user.data,(pos, type) -> {
                        if(type.equals("accept")){
                            projectaccept(user.data.get(pos).request_id+"",user.data.get(pos).project_id,user.data.get(pos).user_id);

                        }
                        if(type.equals("reject")){
                            projectreject(user.data.get(pos).request_id+"",user.data.get(pos).project_id,user.data.get(pos).user_id);

                        }
                    });
                    rvMyProject.setAdapter(myProjectAdapter);
                }
            }

            @Override
            public void onFailure(Call<InivtationModel> call, Throwable t) {
                dialog.dismiss();
                swiperefresh.setRefreshing(false);
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void projectaccept(String requestid,String projectid,String userid) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("Are you sure, you want to accept this project invitation?");
        btnRemove.setText("CONFIRM");
        tvTitle.setVisibility(View.GONE);
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            acceptInvite(requestid,projectid,userid);

        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @SuppressLint("SetTextI18n")
    private void projectreject(String requestid,String projectid,String userid) {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("Are you sure, you want to reject this project invitation?");
        btnRemove.setText("REJECT");
        tvTitle.setVisibility(View.GONE);
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();
            rejectInvite(requestid,projectid,userid);

        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public static class OtherProjectAdapter extends RecyclerView.Adapter<OtherProjectAdapter.ViewHolder> {
        private final Context context;
        private final List<InivtationModel.Data> list;
        private final ItemClickListenerExtraParam itemClickListener;

        public OtherProjectAdapter(Context context,List<InivtationModel.Data> list,ItemClickListenerExtraParam itemClickListener) {
            this.context = context;
            this.list = list;
            this.itemClickListener = itemClickListener;
        }

        @NotNull
        @Override
        public OtherProjectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.invitationlayout, parent, false);
            return new OtherProjectAdapter.ViewHolder(listItem);
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(OtherProjectAdapter.ViewHolder holder, final int position) {
            SpannableString SpanString = new SpannableString(
                    list.get(position).ownername+" "+list.get(position).massage+" - "+list.get(position).name);

            int ownername = list.get(position).ownername.length();

            int count = ownername+1+list.get(position).massage.length()+3;
            Log.e("count",count+"");
            int projectname = ownername+1+list.get(position).massage.length()+3+list.get(position).name.length();

            ClickableSpan teremsAndCondition = new ClickableSpan() {
                @Override
                public void onClick(@NotNull View textView) {
                    Intent i = new Intent(context, OtherUserProfileActivity.class);
                    i.putExtra("UserId", String.valueOf(list.get(position).ownername_id));
                    context.startActivity(i);
                }
            };

            ClickableSpan privacy = new ClickableSpan() {
                @Override
                public void onClick(@NotNull View textView) {
                    Intent i = new Intent(context, ViewAssignment.class);
                    i.putExtra("project_id",  String.valueOf(list.get(position).project_id));
                    i.putExtra("qbID", list.get(position).quickbloxId);
                    i.putExtra("name", list.get(position).name);
                    context.startActivity(i);
                }
            };

            final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
            SpanString.setSpan(teremsAndCondition, 0, ownername, 0);
            SpanString.setSpan(new ForegroundColorSpan(Color.WHITE), 0, ownername, 0);
            SpanString.setSpan(new UnderlineSpan(), 0, ownername, 0);
            SpanString.setSpan(bss, 0, ownername, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            final StyleSpan bss1 = new StyleSpan(android.graphics.Typeface.BOLD);
            SpanString.setSpan(privacy, count, projectname, 0);
            SpanString.setSpan(new ForegroundColorSpan(Color.WHITE), count, projectname, 0);
            SpanString.setSpan(new UnderlineSpan(), count, projectname, 0);
            SpanString.setSpan(bss1, count, projectname, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            holder.tvReportName.setMovementMethod(LinkMovementMethod.getInstance());
            holder.tvReportName.setText(SpanString, TextView.BufferType.SPANNABLE);
            holder. tvReportName.setSelected(true);

            holder.tvAccept.setOnClickListener(v -> itemClickListener.itemClick(position,"accept"));
            holder.tvReject.setOnClickListener(v -> itemClickListener.itemClick(position,"reject"));

            if(list.get(position).accepct == 0){
                holder.tvAccept.setText("Accept");
            }
            else {
                holder.tvStatus.setText("Accepted");
                holder.tvAccept.setEnabled(false);
                holder.tvAccept.setBackgroundColor(context.getResources().getColor(R.color.border_color));
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.green));
                holder.tvReject.setVisibility(View.GONE);
                holder.tvAccept.setVisibility(View.GONE);
            }

            if(list.get(position).reject == 0){
                holder.tvReject.setText("Reject");
            }
            else {
                holder.tvStatus.setText("Rejected");
                holder.tvReject.setEnabled(false);
                holder.tvReject.setBackgroundColor(context.getResources().getColor(R.color.border_color));
                holder.tvStatus.setTextColor(context.getResources().getColor(R.color.red));
                holder.tvAccept.setVisibility(View.GONE);
                holder.tvReject.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {

            LinearLayout layoutReport;
            TextView tvReportName,tvAccept,tvReject,tvStatus;
            public ViewHolder(View itemView) {
                super(itemView);
                layoutReport = itemView.findViewById(R.id.layoutReport);
                tvReportName = itemView.findViewById(R.id.tvReportName);
                tvAccept = itemView.findViewById(R.id.tvAccept);
                tvReject = itemView.findViewById(R.id.tvReject);
                tvStatus = itemView.findViewById(R.id.tvStatus);
            }
        }
    }

    public void acceptInvite(String id,String projectid,String userid) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<AcceptInvite> call = apiInterface.acceptInvite("Bearer "+accessToken,id,"other",projectid,userid);
        call.enqueue(new Callback<AcceptInvite>() {
            @Override
            public void onResponse(@NotNull Call<AcceptInvite> call, @NotNull Response<AcceptInvite> response) {
                dialog.dismiss();
                AcceptInvite user = response.body();
//                getActivity().finish();
//                startActivity(getActivity().getIntent());
                getFragmentManager().beginTransaction().detach(SentInvitationFragment.this).attach(SentInvitationFragment.this).commit();

            }

            @Override
            public void onFailure(@NotNull Call<AcceptInvite> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void rejectInvite(String id,String projectid,String userid) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        final ApiInterface apiInterface = ApiClient.getClient(Constant.BASEURL).create(ApiInterface.class);
        String accessToken = ApplicationClass.appPreferences.GetToken();
        Call<AcceptInvite> call = apiInterface.rejectInvite("Bearer "+accessToken,id,"other",projectid,userid);
        call.enqueue(new Callback<AcceptInvite>() {
            @Override
            public void onResponse(@NotNull Call<AcceptInvite> call, @NotNull Response<AcceptInvite> response) {
                dialog.dismiss();
//                getActivity().finish();
//                startActivity(getActivity().getIntent());
                getFragmentManager().beginTransaction().detach(SentInvitationFragment.this).attach(SentInvitationFragment.this).commit();

            }

            @Override
            public void onFailure(@NotNull Call<AcceptInvite> call, @NotNull Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            assert getFragmentManager() != null;
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            Log.i("IsRefresh", "Yes");
        }
    }

}