package com.twc.tiecoon.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.ProfileModel;
import com.twc.tiecoon.network.cmds.AbstractFragment;
import com.twc.tiecoon.network.cmds.BasicModel;

import java.util.Observable;

public class MessageFragment extends AbstractFragment {
        private ProfileModel profileModel=new ProfileModel();
    private View view;
    private LinearLayout linearParent;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    @Override
    protected View onCreateViewPost(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_message, container, false);
//        merlinsBeard = new MerlinsBeard.Builder()
//                .build(getContext());
        linearParent=view.findViewById(R.id.linearParent);
        viewPager = view.findViewById(R.id.viewPager1);

        viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);


        return view;
    }

    public static class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0)
            {
                fragment = new SingleChatFragment();
            }
            else if (position == 1)
            {
                fragment = new GroupChatFragment();
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            if (position == 0)
            {
                title = "Connections";
            }
            else if (position == 1)
            {
                title = "Collabs";
            }
            return title;
        }
    }

    @Override
    protected BasicModel getModel() {
        return profileModel;
    }

    @Override
    public void update(Observable observable, Object o) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            Log.i("IsRefresh", "Yes");
        }
    }




}