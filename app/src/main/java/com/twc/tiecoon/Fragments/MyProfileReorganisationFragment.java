package com.twc.tiecoon.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.twc.tiecoon.Adapters.MyProfileReorganisationAdapters;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsReOrganisation;

import java.util.ArrayList;
import java.util.List;

public class MyProfileReorganisationFragment extends Fragment {
    private List<HomeUserDetailsReOrganisation> homeUserDetailsReOrganisationArrayList = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            homeUserDetailsReOrganisationArrayList = getArguments().getParcelableArrayList("reorganisation");
        }
        return inflater.inflate(R.layout.fragment_my_profile_reorganisation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView_reorganisation = view.findViewById(R.id.recyclerView_reorganisation);
        if (homeUserDetailsReOrganisationArrayList != null && !(homeUserDetailsReOrganisationArrayList.isEmpty())) {
            MyProfileReorganisationAdapters userHomeFeedbackAdapters = new MyProfileReorganisationAdapters(view.getContext(), homeUserDetailsReOrganisationArrayList);
            //recyclerView_reorganisation.setAdapter(userHomeFeedbackAdapters);
        }
    }
}