package com.twc.tiecoon.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.twc.tiecoon.Activities.ProductServiceBrandsBuyActivity;
import com.twc.tiecoon.Activities.ServiceDetailsActivity;
import com.twc.tiecoon.Adapters.UserHomeServiceAdapters;
import com.twc.tiecoon.Adapters.UserThirdProfileTalentAdapters;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.ArrayList;

public class HomeThirdProfileFragment extends Fragment {
    private ArrayList<HomeUserDetailsService> homeUserDetailBrandList = new ArrayList<>();
    String  qbID="";
    RelativeLayout rLNoData;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            homeUserDetailBrandList = getArguments().getParcelableArrayList("service");
            qbID = getArguments().getString("qbID");
        }
        return inflater.inflate(R.layout.fragment_third_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView_ThirdProfile = view.findViewById(R.id.recyclerView_ThirdProfile);
        rLNoData = view.findViewById(R.id.rLNoData);
        if(homeUserDetailBrandList.size()>0){
            recyclerView_ThirdProfile.setVisibility(View.VISIBLE);
            rLNoData.setVisibility(View.GONE);
            if (homeUserDetailBrandList != null && !(homeUserDetailBrandList.isEmpty())) {
                UserHomeServiceAdapters userHomeServiceAdapters = new UserHomeServiceAdapters(view.getContext(), homeUserDetailBrandList,
                        pos -> {
                            ApplicationClass.uploadImageListDataArrayList.clear();
                            Intent intent = new Intent(view.getContext(), ServiceDetailsActivity.class);
                            intent.putExtra("service_id", homeUserDetailBrandList.get(pos).getId()+"");
                            intent.putExtra("qbID", qbID);
                            startActivity(intent);
                        });
                recyclerView_ThirdProfile.setAdapter(userHomeServiceAdapters);
            }
        }
        else {
            recyclerView_ThirdProfile.setVisibility(View.GONE);
            rLNoData.setVisibility(View.VISIBLE);
        }

    }
}