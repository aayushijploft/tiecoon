package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.ProfileCompleteAssignment;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MyprofileCompletedAssignmentAdapters extends RecyclerView.Adapter<MyprofileCompletedAssignmentAdapters.MyViewHolder> {
    private final List<ProfileCompleteAssignment> talentDataList;
    private final Context context;
    private final ItemClickListenerExtraParam itemClickListener;

    public MyprofileCompletedAssignmentAdapters(Context context, List<ProfileCompleteAssignment> talentDataList,ItemClickListenerExtraParam itemClickListener) {
        this.context = context;
        this.talentDataList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public MyprofileCompletedAssignmentAdapters.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_profile_ongoing_assignment_items, parent, false);
        return new MyprofileCompletedAssignmentAdapters.MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(@NonNull MyprofileCompletedAssignmentAdapters.MyViewHolder holder, int position) {
        ProfileCompleteAssignment uploadTalentData = talentDataList.get(position);
        if (uploadTalentData != null) {
//            String imageURL = Constant.ImageURL + uploadTalentData.getPath() + "/" + uploadTalentData.getImage();
//            Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
//                    .error(R.drawable.dummy_place)
//                    .into(holder.image);
            holder.textViewName.setText(uploadTalentData.getName());

            holder.textViewText.setText(uploadTalentData.getText());


//            if(!(uploadTalentData.getUser_id()== ApplicationClass.appPreferences.getUserId()))
//            {
                holder.fabCancelItem.hide();
                holder.fabEditItem.hide();
//            }

        }

        holder.fabCancelItem.setOnClickListener(view -> itemClickListener.itemClick(position,"delete"));
        holder.fabEditItem.setOnClickListener(view -> itemClickListener.itemClick(position,"update"));
        holder.cardView.setOnClickListener(view -> itemClickListener.itemClick(position,"view"));

    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView textViewName, textViewText;
        FloatingActionButton fabCancelItem,fabEditItem;
        CardView cardView;
        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            textViewName = view.findViewById(R.id.textViewName);
            cardView = view.findViewById(R.id.cardView);
            textViewText = view.findViewById(R.id.textViewText);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            fabEditItem = view.findViewById(R.id.fabEditItem);
        }
    }
}
