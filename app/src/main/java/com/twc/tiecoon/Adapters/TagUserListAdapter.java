package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.ReportList;
import com.twc.tiecoon.network.response.TagUserList;
import com.twc.tiecoon.utils.ItemClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class TagUserListAdapter extends RecyclerView.Adapter<TagUserListAdapter.ViewHolder> {
    private Context context;
    private List<TagUserList.Data> list;
    private ItemClickListener itemClickListener;
    public TagUserListAdapter(Context context, List<TagUserList.Data> list, ItemClickListener itemClickListener) {
        this.context = context;
        this.list = list;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public TagUserListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.usertag_list_layout, parent, false);
        return new TagUserListAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(TagUserListAdapter.ViewHolder holder, final int position) {
        Glide.with(context).load(list.get(position).getUrl()).placeholder(R.drawable.ic_dummy_user)
                .error(R.drawable.ic_dummy_user)
                .into(holder.imgUserimage);

        holder.tvName.setText(list.get(position).getName());
        holder.layoutItem.setOnClickListener(v -> itemClickListener.itemClick(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView imgUserimage;
        TextView tvName;
        LinearLayout layoutItem;

        public ViewHolder(View itemView) {
            super(itemView);
            imgUserimage = itemView.findViewById(R.id.imgUserimage);
            tvName = itemView.findViewById(R.id.tvName);
            layoutItem = itemView.findViewById(R.id.layoutItem);

        }
    }
}

