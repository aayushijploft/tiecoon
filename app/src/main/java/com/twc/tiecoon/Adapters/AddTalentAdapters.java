package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class AddTalentAdapters extends RecyclerView.Adapter<AddTalentAdapters.MyViewHolder> {
    private List<HomeUserDetailsUploadTalent> talentDataList;
    private ItemClickListenerExtraParam itemClickListener;
    private Context context;

    public AddTalentAdapters(Context context, List<HomeUserDetailsUploadTalent> talentDataList, ItemClickListenerExtraParam itemClickListener) {
        this.context = context;
        this.talentDataList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_talents_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailsUploadTalent uploadTalentData = talentDataList.get(position);
        if (uploadTalentData != null) {
            if (uploadTalentData.getUploadtype() != null) {
                if (uploadTalentData.getUploadtype() == 1) {
                    holder.relativeWaterMark.setVisibility(View.GONE);
                    holder.image.setVisibility(View.VISIBLE);
                    String imageURL = (new StringBuilder().append(Constant.ImageURL).append(uploadTalentData.getPath()).append("/").append(uploadTalentData.getCreateimage())).toString();
                    Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                            .error(R.drawable.dummy_place)
                            .into(holder.image);
                } else {
                    holder.image.setVisibility(View.GONE);
                    holder.relativeWaterMark.setVisibility(View.VISIBLE);
                    if (uploadTalentData.getFontcolor() != null && !(uploadTalentData.getFontcolor().isEmpty())) {
                        try {
                            holder.relativeWaterMark.setBackgroundColor(Color.parseColor(uploadTalentData.getFontcolor()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    holder.tvWatermark.setText(uploadTalentData.getWatermark());
                }
            } else {
                holder.relativeWaterMark.setVisibility(View.GONE);
                holder.image.setVisibility(View.VISIBLE);
                String imageURL = (new StringBuilder().append(Constant.ImageURL).append(uploadTalentData.getPath()).append("/").append(uploadTalentData.getImage())).toString();
                Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                        .error(R.drawable.dummy_place)
                        .into(holder.image);
            }
        }
        holder.fabCancelItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.itemClick(position,"delete");
            }
        });
        holder.fabEditItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.itemClick(position,"update");
            }
        });
    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        RelativeLayout relativeWaterMark;
        TextView tvWatermark;
        ImageView image;
        FloatingActionButton fabCancelItem,fabEditItem;

        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            cardView = view.findViewById(R.id.cardView);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            fabEditItem = view.findViewById(R.id.fabEditItem);
            relativeWaterMark = view.findViewById(R.id.relativeWaterMark);
            tvWatermark = view.findViewById(R.id.tvWatermark);
        }
    }
}