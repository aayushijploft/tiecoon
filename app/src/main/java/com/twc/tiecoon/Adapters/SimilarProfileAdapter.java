package com.twc.tiecoon.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.SimilarProfile;
import com.twc.tiecoon.utils.ItemClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SimilarProfileAdapter extends RecyclerView.Adapter<SimilarProfileAdapter.MyViewHolder> {

    private final List<SimilarProfile.Data> similarDataList;
    Context context;
    private ItemClickListener itemClickListener;
    public SimilarProfileAdapter(Context context, List<SimilarProfile.Data> similarDataList,
                        ItemClickListener itemClickListener) {
        this.context=context;
        this.similarDataList=similarDataList;
        this.itemClickListener=itemClickListener;

    }

    @NotNull
    @Override
    public SimilarProfileAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.similar_profiles_list_item, parent, false);
        return new SimilarProfileAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull SimilarProfileAdapter.MyViewHolder holder, int position) {



            Glide.with(context).load(similarDataList.get(position).getProfile()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.imgProfile);


            holder.tvUsername.setText(similarDataList.get(position).getName());
            holder.imgProfile.setOnClickListener(v -> itemClickListener.itemClick(position));





    }
    @Override
    public int getItemCount() {
        return similarDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView imgProfile;
        CardView layoutStory;
        TextView tvUsername;
        MyViewHolder(View view) {
            super(view);
            imgProfile = view.findViewById(R.id.imgProfile);
            layoutStory = view.findViewById(R.id.layoutStory);
            tvUsername = view.findViewById(R.id.tvUsername);
        }

    }
}


