package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class MyprofileOngoingAssignmentAdapters extends RecyclerView.Adapter<MyprofileOngoingAssignmentAdapters.MyViewHolder> {
    private final List<ProfileCreateAssignment> talentDataList;
    private final Context context;
    private final ItemClickListenerExtraParam itemClickListener;

    public MyprofileOngoingAssignmentAdapters(Context context, List<ProfileCreateAssignment> talentDataList,ItemClickListenerExtraParam itemClickListener) {
        this.context = context;
        this.talentDataList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_profile_ongoing_assignment_items, parent, false);
        return new MyViewHolder(itemView);
    }



    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ProfileCreateAssignment uploadTalentData = talentDataList.get(position);
        if (uploadTalentData != null) {
                String imageURL = Constant.ImageURL + uploadTalentData.getPath() + "/" + uploadTalentData.getImage();
                Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                        .error(R.drawable.dummy_place)
                        .into(holder.image);
                holder.textViewName.setText(uploadTalentData.getName());

            holder.textViewText.setText(uploadTalentData.getText());


            if(!(uploadTalentData.getUser_id()== ApplicationClass.appPreferences.getUserId()))
            {
                holder.fabCancelItem.hide();
                holder.fabEditItem.hide();
            }

        }

        holder.fabCancelItem.setOnClickListener(view -> itemClickListener.itemClick(position,"delete"));
        holder.fabEditItem.setOnClickListener(view -> itemClickListener.itemClick(position,"update"));
        holder.cardView.setOnClickListener(view -> itemClickListener.itemClick(position,"view"));

    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView textViewName, textViewText;
        FloatingActionButton fabCancelItem,fabEditItem;
        CardView cardView;
        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            textViewName = view.findViewById(R.id.textViewName);
            cardView = view.findViewById(R.id.cardView);
            textViewText = view.findViewById(R.id.textViewText);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            fabEditItem = view.findViewById(R.id.fabEditItem);
        }
    }
}