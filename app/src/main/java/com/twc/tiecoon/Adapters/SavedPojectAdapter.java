package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.network.response.SaveProjectList;
import com.twc.tiecoon.utils.ItemClickListener;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.util.List;

public class SavedPojectAdapter extends RecyclerView.Adapter<SavedPojectAdapter.ViewHolder> {
    private Context context;
    private List<SaveProjectList.Datum> list;
    private ItemClickListener itemClickListener;

    public SavedPojectAdapter(Context context, List<SaveProjectList.Datum> list, ItemClickListener itemClickListener) {
        this.context = context;
        this.list = list;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public SavedPojectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.my_profile_ongoing_assignment_items, parent, false);
        return new SavedPojectAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(SavedPojectAdapter.ViewHolder holder, final int position) {
        holder.textViewName.setText(list.get(position).getName());
        holder.textViewText.setText(list.get(position).getText());
        holder.cardView.setOnClickListener(v -> itemClickListener.itemClick(position));
        holder.fabCancelItem.setVisibility(View.GONE);
        holder.fabEditItem.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       TextView textViewName,textViewText;
       CardView cardView;
       FloatingActionButton fabCancelItem,fabEditItem;
        public ViewHolder(View itemView) {
            super(itemView);
            textViewName=itemView.findViewById(R.id.textViewName);
            textViewText=itemView.findViewById(R.id.textViewText);
            cardView=itemView.findViewById(R.id.cardView);
            fabCancelItem=itemView.findViewById(R.id.fabCancelItem);
            fabEditItem=itemView.findViewById(R.id.fabEditItem);

        }
    }
}

