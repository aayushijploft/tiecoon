package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.TalantCat;
import com.twc.tiecoon.utils.ItemClickListener;
import org.jetbrains.annotations.NotNull;
import java.util.List;

public class SelectProjectAdapter extends RecyclerView.Adapter<SelectProjectAdapter.ViewHolder> {
    private final List<TalantCat.Data> list;
    private final ItemClickListener itemClickListener;
    Context context;

    public SelectProjectAdapter(Context context, List<TalantCat.Data> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public SelectProjectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.curreny_item_list, parent, false);
        return new SelectProjectAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(SelectProjectAdapter.ViewHolder holder, final int position) {
        holder.tv_currency.setText(list.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_currency;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_currency = itemView.findViewById(R.id.tv_currency);
            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAdapterPosition()));
        }
    }
}

