package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.Activities.ProductServiceBrandsBuyActivity;
import com.twc.tiecoon.Activities.ViewAssignment;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.Fragments.HomeProductServiceFragment;
import com.twc.tiecoon.Fragments.HomeThirdProfileFragment;
import com.twc.tiecoon.Fragments.HomeUserTalentFragment;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.HomeUserPreferedLanguage;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.utils.FragmentBrandListListner;
import com.twc.tiecoon.utils.FragmentProductServiceListListner;
import com.twc.tiecoon.utils.FragmentUploadTalentListListner;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import de.hdodenhof.circleimageview.CircleImageView;

public class HomeScreenAdapters extends RecyclerView.Adapter<HomeScreenAdapters.MyViewHolder> {
    private List<HomeUserDetailData> homeUserDetailDataList;
    private ItemClickListenerExtraParam itemClickListener;
    private Context context;
    private FragmentManager fragmentManager;

    Fragment homeUserTalentFragment, productServiceFragment, thirdProfileFragment;
    FragmentUploadTalentListListner fragmentUploadTalentListListner;
    FragmentProductServiceListListner fragmentProductServiceListListner;
    FragmentBrandListListner fragmentBrandListListner;
    //        List<ProfileCreateAssignment> homeUserDetailCreateAssignmentList = new ArrayList<>();
    OngoingAssignmentAdapters ongoingAssignmentAdapter;

    public HomeScreenAdapters(Context context, FragmentManager fragmentManager, List<HomeUserDetailData> homeUserDetailDataList,
                              ItemClickListenerExtraParam itemClickListener) {
        this.context = context;
        this.homeUserDetailDataList = homeUserDetailDataList;
        this.itemClickListener = itemClickListener;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailData homeUserDetailData = homeUserDetailDataList.get(position);
        Log.d("search",homeUserDetailData.toString());
        if (homeUserDetailData != null) {
            if (homeUserDetailData.getHomeUserDetailsUserDetils() != null) {
                if (homeUserDetailData.getHomeUserDetailsUserDetils().getName() != null) {
                    String name = homeUserDetailData.getHomeUserDetailsUserDetils().getName().substring(0, 1).toUpperCase() + homeUserDetailData.getHomeUserDetailsUserDetils().getName().substring(1);
                    holder.tvName.setText(name);
                }
                if (homeUserDetailData.getHomeUserDetailsUserDetils().getLocation() != null) {
                    String location = homeUserDetailData.getHomeUserDetailsUserDetils().getLocation().substring(0, 1).toUpperCase() + homeUserDetailData.getHomeUserDetailsUserDetils().getLocation().substring(1);
                    holder.tvLocation.setText(location);
                }
                if (homeUserDetailData.getHomeUserDetailsUserDetils().getWorks() != null) {
//                    String location = homeUserDetailData.getHomeUserDetailsUserDetils().getLocation().substring(0, 1).toUpperCase() + homeUserDetailData.getHomeUserDetailsUserDetils().getLocation().substring(1);
                    holder.tvWorks.setText(homeUserDetailData.getHomeUserDetailsUserDetils().getWorks());
                }
                else  holder.tvWorks.setText("Part Time");
                if (homeUserDetailData.getHomeUserDetailsUserDetils().getStatus() != null) {
                    String des = homeUserDetailData.getHomeUserDetailsUserDetils().getStatus().substring(0, 1).toUpperCase() + homeUserDetailData.getHomeUserDetailsUserDetils().getStatus().substring(1);
                    holder.tvProfileDes.setText(des);
                }
//                if (homeUserDetailData.getHomeUserDetailsUserDetils().getp() != null) {
//                    String des = homeUserDetailData.getHomeUserDetailsUserDetils().getStatus().substring(0, 1).toUpperCase() + homeUserDetailData.getHomeUserDetailsUserDetils().getStatus().substring(1);
//                    holder.tvProfileDes.setTex    t(des);
//                }
            }

            List<HomeUserPreferedLanguage> homeUserPreferedLanguageList = homeUserDetailData.getPreferredLanguage();
            if (homeUserPreferedLanguageList != null && !(homeUserPreferedLanguageList.isEmpty())) {
                StringBuilder stringBuilder = new StringBuilder();
                for (HomeUserPreferedLanguage homeUserPreferedLanguage : homeUserPreferedLanguageList) {
                    if (homeUserPreferedLanguage != null && homeUserPreferedLanguage.getLanguage() != null) {
                        if (homeUserPreferedLanguage.getLanguage().getName() != null) {
                            if (stringBuilder.length() == 0) {
                                stringBuilder.append(homeUserPreferedLanguage.getLanguage().getName());
                            } else {
                                stringBuilder.append("/").append(homeUserPreferedLanguage.getLanguage().getName());
                            }
                        }
                    }
                }
                holder.tvLang.setText(stringBuilder.toString());
            }
            else holder.tvLang.setVisibility(View.GONE);

            if (homeUserDetailData.getProfile() != null) {
                if (homeUserDetailData.getPath() != null) {
                    String imageURL = Constant.ImageURL + homeUserDetailData.getPath() + "/" + homeUserDetailData.getProfile();
                    Glide.with(context).load(imageURL).placeholder(R.drawable.ic_dummy_user)
                            .error(R.drawable.ic_dummy_user)
                            .into(holder.ivProfile);
                } else {
                    Glide.with(context).load(homeUserDetailData.getProfile()).placeholder(R.drawable.ic_dummy_user)
                            .error(R.drawable.ic_dummy_user)
                            .into(holder.ivProfile);
                }
            }
            else {
                Glide.with(context).load(R.drawable.ic_dummy_user).placeholder(R.drawable.ic_dummy_user)
                        .error(R.drawable.ic_dummy_user)
                        .into(holder.ivProfile);
            }


            switch (homeUserDetailData.getUser_status()) {
                case "Online":
                    holder.ivStatusDot.setColorFilter(ContextCompat.getColor(context, R.color.green));
                    break;
                case "Away":
                    holder.ivStatusDot.setColorFilter(ContextCompat.getColor(context, R.color.yellow));
                    break;
                case "Inactive":
                    holder.ivStatusDot.setColorFilter(ContextCompat.getColor(context, R.color.white));
                    break;
            }
            if (homeUserDetailData.isIs_like()) {
                holder.imgLike.setImageResource(R.drawable.ic_like_solid);
            } else {
                holder.imgLike.setImageResource(R.drawable.ic_like);
            }

            List  fragmentList = new ArrayList<>();
            List  titleList = new ArrayList<>();
            //TalentList
            List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentsNew = homeUserDetailData.getHomeUserDetailsUploadTalent();
            Log.e("talentlist",homeUserDetailsUploadTalentsNew.toString());

            if (homeUserDetailsUploadTalentsNew != null && !(homeUserDetailsUploadTalentsNew.isEmpty())) {
                HomeUserTalentFragment homeUserTalentFragment = new HomeUserTalentFragment();
                Bundle bundleProduct = new Bundle();
                bundleProduct.putInt("pos",position);
                bundleProduct.putParcelableArrayList("talent", (ArrayList<? extends Parcelable>) homeUserDetailsUploadTalentsNew);
                homeUserTalentFragment.setArguments(bundleProduct);
                fragmentList.add(homeUserTalentFragment);
                titleList.add("Talents");
            }
            //ProductList
            List<HomeUserDetailsProduct> homeUserDetailsProductList1 = homeUserDetailData.getHomeUserDetailsProducts();
            if (homeUserDetailsProductList1 != null && !(homeUserDetailsProductList1.isEmpty())) {
                HomeProductServiceFragment  productServiceFragment = new HomeProductServiceFragment();
                Bundle bundleProduct = new Bundle();
                bundleProduct.putParcelableArrayList("product", (ArrayList<? extends Parcelable>) homeUserDetailsProductList1);
                productServiceFragment.setArguments(bundleProduct);
                fragmentList.add(productServiceFragment);
                titleList.add("Products");
            }else {
                HomeProductServiceFragment  productServiceFragment = new HomeProductServiceFragment();
                fragmentList.add(productServiceFragment);
                titleList.add("Products");
            }
//            BrandList
            List<AddBrandsData> homeUserDetailBrandList1 = homeUserDetailData.getHomeUserDetailBrand();
            if (homeUserDetailBrandList1 != null && !(homeUserDetailBrandList1.isEmpty())) {
                HomeThirdProfileFragment  thirdProfileFragment = new HomeThirdProfileFragment();
                Bundle bundleThirdProfile = new Bundle();
                bundleThirdProfile.putParcelableArrayList("brand", (ArrayList<? extends Parcelable>) homeUserDetailBrandList1);
                thirdProfileFragment.setArguments(bundleThirdProfile);
                fragmentList.add(thirdProfileFragment);
                titleList.add("Brand");
            }else{
                HomeThirdProfileFragment  thirdProfileFragment = new HomeThirdProfileFragment();
                fragmentList.add(thirdProfileFragment);
                titleList.add("Brand");
            }

            final TodaysDealViewPagerAdapter adapter1 = new TodaysDealViewPagerAdapter(context, fragmentManager, fragmentList, titleList);
            holder.viewPager.setAdapter(adapter1);
            holder.viewPager.setId(position);
            holder.tabLayout.setupWithViewPager(holder.viewPager);
            setupTabIcons(holder);

            List<ProfileCreateAssignment> homeUserDetailCreateAssignmentList = new ArrayList<>();
            if (homeUserDetailData.getHomeUserDetailCreateAssignments() != null && !(homeUserDetailData.getHomeUserDetailCreateAssignments().isEmpty())) {
                for (ProfileCreateAssignment profileCreateAssignment : homeUserDetailData.getHomeUserDetailCreateAssignments()) {
//                    if (profileCreateAssignment.getPath() != null && profileCreateAssignment.getImage() != null) {
                    homeUserDetailCreateAssignmentList.add(profileCreateAssignment);
//                    }
                }
                MyprofileOngoingAssignmentAdapters ongoingAssignmentAdapter = new MyprofileOngoingAssignmentAdapters(context, homeUserDetailCreateAssignmentList, new ItemClickListenerExtraParam() {
                    @Override
                    public void itemClick(int pos, String type) {
                        if(type.equals("view")){
                            Intent i = new Intent(context, ViewAssignment.class);
                            i.putExtra("project_id", homeUserDetailCreateAssignmentList.get(pos));
                            context.startActivity(i);
                        }

                    }
                });

                holder.recyclerView_ongoing_assignments.setAdapter(ongoingAssignmentAdapter);
            }else
            {
                holder.tvOngoingAssignnmets.setVisibility(View.GONE);
            }

            holder.linearOtherUserProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "click");
                }
            });
            holder.linearLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "like");
                }
            });
            holder.linearConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "chat");
                }
            });
            holder.linearShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "share");
                }
            });
            holder.linearChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "collaborate");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivProfile;
        private RecyclerView recyclerView_ongoing_assignments;
        private TextView tvLocation, tvPrice, tvWorks,tvrating, tvName, tvLang, tvProfileDes,tvOngoingAssignnmets;
        private ImageView imgUserMenu, imgshare, imgLike, imgJoinGroupOne, imgJoinGroupTwo, imgJoinGroupCreateNew,ivStatusDot;
        private LinearLayout linearOtherUserProfile,linearLike, linearConnect, linearChat, linear3Profile, linear_add_ongoingAssignment,linearShare;
        private TabLayout tabLayout;
        private ViewPager viewPager;

        MyViewHolder(View view) {
            super(view);
            ivProfile = view.findViewById(R.id.ivProfile);
            tvPrice = view.findViewById(R.id.tvPrice);
            tvrating = view.findViewById(R.id.tvrating);
            tvWorks = view.findViewById(R.id.tvWorks);
            tvLang = view.findViewById(R.id.tvLang);
            tvLocation = view.findViewById(R.id.tvLocation);
            imgUserMenu = view.findViewById(R.id.imgUserMenu);
            tvName = view.findViewById(R.id.tvName);
            tvOngoingAssignnmets = view.findViewById(R.id.tvOngoingAssignnmets);
            linearOtherUserProfile = view.findViewById(R.id.linearOtherUserProfile);
            linearLike = view.findViewById(R.id.linearLike);
            tvProfileDes = view.findViewById(R.id.tvProfileStatus);
            linearShare = view.findViewById(R.id.linearShare);
            imgshare = view.findViewById(R.id.imgshare);
            imgLike = view.findViewById(R.id.imgLike);
            ivStatusDot = view.findViewById(R.id.ivStatusDot);

            linearOtherUserProfile = view.findViewById(R.id.linearOtherUserProfile);
            linearConnect = view.findViewById(R.id.linearConnect);
            linearChat = view.findViewById(R.id.linearChat);
            linear3Profile = view.findViewById(R.id.linear3Profile);
            linear_add_ongoingAssignment = view.findViewById(R.id.linear_add_ongoingAssignment);

            //first tablayout
            tabLayout = view.findViewById(R.id.tabLayout);
            viewPager = view.findViewById(R.id.viewPager);

            recyclerView_ongoing_assignments = view.findViewById(R.id.recyclerView_ongoing_assignments);
//            recyclerViewUploadImageList = view.findViewById(R.id.recyclerViewUploadImageList);
        }

    }

    private void setupTabIcons(MyViewHolder holder) {
        TextView tabOne = (TextView) LayoutInflater.from(context).inflate(R.layout.custom_tabs, null);
        tabOne.setText("Talents");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_star, 0, 0);
        holder.tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(context).inflate(R.layout.custom_tabs, null);
        tabTwo.setText("Products");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_hand_pick, 0, 0);
        holder.tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(context).inflate(R.layout.custom_tabs, null);
        tabThree.setText("Services");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_user, 0, 0);
        holder.tabLayout.getTabAt(2).setCustomView(tabThree);
    }
    public static class TodaysDealViewPagerAdapter extends FragmentStatePagerAdapter {

        private Context myContext;
        private List<Fragment> fragmentList;
        private List<String> titleList;

        public TodaysDealViewPagerAdapter(Context context, FragmentManager fm, List<Fragment> fragmentList, List<String> titleList) {
            super(fm);
            myContext = context;
            this.fragmentList = fragmentList;
            this.titleList = titleList;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }

        // this is for fragment tabs
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        // this counts total number of tabs
        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }

}