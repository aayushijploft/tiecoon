package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.StoryView;
import com.twc.tiecoon.network.response.UploadImageListData;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class StorySliderAdapter extends RecyclerView.Adapter<StorySliderAdapter.MyViewHolder> {
    private final List<StoryView.Stores> storyViewList;
    private final Context context;

    public StorySliderAdapter(Context context, List<StoryView.Stores> talentDataList) {
        this.context = context;
        this.storyViewList = talentDataList;
    }

    @NotNull
    @Override
    public StorySliderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.slidingimages_layout, parent, false);
        return new StorySliderAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull StorySliderAdapter.MyViewHolder holder, int position) {

            Glide.with(context).load(storyViewList.get(position).getImage()).error(R.drawable.dummy_place)
                    .error(R.drawable.dummy_place)
                    .into(holder.image);

            holder.ivNextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
//        String imageURL = Constant.ImageURL + homeUserDetailsUploadTalent.getCreateimage();

    }

    @Override
    public int getItemCount() {
        return storyViewList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        ImageView ivPreviousButton;
        ImageView ivNextButton;
        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            ivPreviousButton = view.findViewById(R.id.ivPreviousButton);
            ivNextButton = view.findViewById(R.id.ivNextButton);
        }
    }


}
