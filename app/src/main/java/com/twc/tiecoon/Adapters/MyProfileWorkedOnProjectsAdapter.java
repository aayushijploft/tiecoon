package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.WorkedOnProject;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MyProfileWorkedOnProjectsAdapter extends RecyclerView.Adapter<MyProfileWorkedOnProjectsAdapter.MyViewHolder> {
    private final List<WorkedOnProject> talentDataList;
    private final ItemClickListenerExtraParam itemClickListener;

    public MyProfileWorkedOnProjectsAdapter(Context context, List<WorkedOnProject> talentDataList,ItemClickListenerExtraParam itemClickListener) {
        this.talentDataList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public MyProfileWorkedOnProjectsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_profile_ongoing_assignment_items, parent, false);
        return new MyProfileWorkedOnProjectsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyProfileWorkedOnProjectsAdapter.MyViewHolder holder, int position) {
        WorkedOnProject uploadTalentData = talentDataList.get(position);
        if (uploadTalentData != null) {
            holder.textViewName.setText(uploadTalentData.getName());
            holder.textViewText.setText(uploadTalentData.getText());
            if(uploadTalentData.getUser_id()== ApplicationClass.appPreferences.getUserId()) {
                holder.RateUS.setVisibility(View.VISIBLE);
            }
             holder.fabCancelItem.hide();
             holder.fabEditItem.hide();
        }

        holder.fabCancelItem.setOnClickListener(view -> itemClickListener.itemClick(position,"delete"));
        holder.fabEditItem.setOnClickListener(view -> itemClickListener.itemClick(position,"update"));
        holder.linearView.setOnClickListener(view -> itemClickListener.itemClick(position,"view"));
        holder.RateUS.setOnClickListener(view -> itemClickListener.itemClick(position,"rate"));

    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView textViewName, textViewText,RateUS;
        FloatingActionButton fabCancelItem,fabEditItem;
        CardView cardView;
        LinearLayout linearView;
        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            textViewName = view.findViewById(R.id.textViewName);
            cardView = view.findViewById(R.id.cardView);
            textViewText = view.findViewById(R.id.textViewText);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            fabEditItem = view.findViewById(R.id.fabEditItem);
            RateUS = view.findViewById(R.id.RateUS);
            linearView = view.findViewById(R.id.linearView);
        }
    }
}

