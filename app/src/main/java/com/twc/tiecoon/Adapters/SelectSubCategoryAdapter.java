package com.twc.tiecoon.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.Activities.WatermarkActivity;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.SubCategoryData;
import com.twc.tiecoon.utils.ItemClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SelectSubCategoryAdapter extends RecyclerView.Adapter<SelectSubCategoryAdapter.ViewHolder> implements WatermarkActivity.UpdateSubcategory {
    private final List<SubCategoryData> subCategoryDataList;
    private final ItemClickListener itemClickListener;
    private final Context context;
    private int mSelectedItem = -1;
    private int subCategoryId;

    public SelectSubCategoryAdapter(Context context, int subCategoryId, List<SubCategoryData> subCategoryDataList, ItemClickListener itemClickListener) {
        this.subCategoryDataList = subCategoryDataList;
        this.itemClickListener = itemClickListener;
        this.context = context;
        this.subCategoryId = subCategoryId;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.category_item_list, parent, false);
        return new ViewHolder(listItem);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tvTitle.setText(subCategoryDataList.get(position).getTitle());
        if (subCategoryId > 0 && subCategoryId == subCategoryDataList.get(position).getId()) {
            holder.linearCategory.setBackground(context.getResources().getDrawable(R.drawable.rounder_border_linear_solid));
            holder.tvTitle.setTextColor(Color.WHITE);
            subCategoryId = 0;
            mSelectedItem = position;
            itemClickListener.itemClick(position);
        } else {
            if (mSelectedItem == position) {
                holder.linearCategory.setBackground(context.getResources().getDrawable(R.drawable.rounder_border_linear_solid));
                holder.tvTitle.setTextColor(Color.WHITE);
            } else {
                holder.linearCategory.setBackground(context.getResources().getDrawable(R.drawable.rounder_border_linear_stroke));
                holder.tvTitle.setTextColor(context.getResources().getColor(R.color.white));
            }
        }
        holder.linearCategory.setOnClickListener(view -> {
            if (mSelectedItem >= 0) {
                notifyItemChanged(mSelectedItem);
            }
            mSelectedItem = position;
            notifyItemChanged(position);
            itemClickListener.itemClick(position);
        });
    }

    @Override
    public int getItemCount() {
        return subCategoryDataList.size();
    }

    @Override
    public void updateId() {
        subCategoryId = 0;
        mSelectedItem=-1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;
        private final LinearLayout linearCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            linearCategory = itemView.findViewById(R.id.linearCategory);
        }
    }
}


