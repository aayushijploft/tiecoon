package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SpecialProfileAdapters extends RecyclerView.Adapter<SpecialProfileAdapters.MyViewHolder> {
    private List<HomeUserDetailData> homeUserDetailDataList;
    private ItemClickListener itemClickListener;
    private Context context;

    public SpecialProfileAdapters(Context context, List<HomeUserDetailData> homeUserDetailDataList, ItemClickListener itemClickListener) {
        this.context = context;
        this.homeUserDetailDataList = homeUserDetailDataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.specialprofile_mainlayout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailData homeUserDetailData = homeUserDetailDataList.get(position);
        List<AddBrandsData> homeUserDetailBrandList = homeUserDetailData.getHomeUserDetailBrand();
        if (homeUserDetailBrandList != null && !(homeUserDetailBrandList.isEmpty())) {
            SpecialProfileHorizontalAdapters specialProfileHorizontalAdapters = new SpecialProfileHorizontalAdapters(context, homeUserDetailBrandList);
            holder.recyclerView_SpecialProfileBrands.setAdapter(specialProfileHorizontalAdapters);
        }
        if (homeUserDetailData.getUsername() != null) {
            holder.tvName.setText(homeUserDetailData.getUsername().toString());
        }
        if (homeUserDetailData.getPath() != null && homeUserDetailData.getProfile() != null) {
            String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailData.getPath()).append("/").append(homeUserDetailData.getProfile())).toString();
            Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                    .error(R.drawable.dummy_place)
                    .into(holder.image);
        }

        holder.relativeContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.itemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeUserDetailDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView tvName, tvRevenue;
        private RelativeLayout relativeContact;
        private RecyclerView recyclerView_SpecialProfileBrands;

        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            tvName = view.findViewById(R.id.tvName);
            tvRevenue = view.findViewById(R.id.tvRevenue);
            relativeContact = view.findViewById(R.id.relativeContact);
            recyclerView_SpecialProfileBrands = view.findViewById(R.id.recyclerView_SpecialProfileBrands);
        }
    }
}