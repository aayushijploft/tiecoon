package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsFeedback;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyprofileFeedbackAdapters extends RecyclerView.Adapter<MyprofileFeedbackAdapters.ViewHolder> {
    private List<HomeUserDetailsFeedback> homeUserDetailsFeedbackList;
    private Context context;

    public MyprofileFeedbackAdapters(Context context, List<HomeUserDetailsFeedback> homeUserDetailsFeedbackList) {
        this.homeUserDetailsFeedbackList = homeUserDetailsFeedbackList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_feedbacks_myprofile, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        HomeUserDetailsFeedback homeUserDetailsFeedback=   homeUserDetailsFeedbackList.get(position);
        if(homeUserDetailsFeedback!=null)
        {
//            if (homeUserDetailsFeedback.getUserDetils()!=null &&  homeUserDetailsFeedback.getUserDetils().getName() != null) {
//                holder.tvName.setText(homeUserDetailsFeedback.getUserDetils().getName());
//            }
//            holder.simpleRatingBar.setNumStars(homeUserDetailsFeedback.getRating());
//            holder.textViewDescription.setText(homeUserDetailsFeedback.getMassage());
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailsFeedbackList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,tvRatingText,textViewDescription;
        private RatingBar simpleRatingBar;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvRatingText= itemView.findViewById(R.id.tvRatingText);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            simpleRatingBar = itemView.findViewById(R.id.rating_feedback);
        }
    }
}
