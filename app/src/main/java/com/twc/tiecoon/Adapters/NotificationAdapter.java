package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.Activities.OtherUserProfileActivity;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.NotificationDataDetails;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {
    private List<NotificationDataDetails> notificationDataDetailsList;
    private ItemClickListener itemClickListener;
    private Context context;

    public NotificationAdapter(Context context, List<NotificationDataDetails> notificationDataDetails, ItemClickListener itemClickListener) {
        this.context = context;
        this.notificationDataDetailsList = notificationDataDetails;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public NotificationAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item_layout, parent, false);
        return new NotificationAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.MyViewHolder holder, int position) {
        NotificationDataDetails notificationDataDetails=notificationDataDetailsList.get(position);
        holder.tv_notificationTitle.setText(notificationDataDetails.getMassage());
        holder.tv_notificationTime.setText(notificationDataDetails.getAgo());
        Glide.with(context).load(notificationDataDetails.getProfile()).placeholder(R.drawable.userddummyset)
                .error(R.drawable.userddummyset)
                .into(holder.ivNotification);
        holder.relativeNotification.setOnClickListener(view -> itemClickListener.itemClick(position));
        holder.relNotificationImage.setOnClickListener(view -> {
            Intent i = new Intent(context, OtherUserProfileActivity.class);
            i.putExtra("UserId",notificationDataDetails.getUser_id()+"");
            context.startActivity(i);
        });
    }

    @Override
    public int getItemCount() {
        return notificationDataDetailsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeNotification,relNotificationImage;
        TextView tv_notificationTitle,tv_notificationTime;
        ImageView ivNotification;

        MyViewHolder(View view) {
            super(view);
            ivNotification = view.findViewById(R.id.imgProfile);
            relativeNotification = view.findViewById(R.id.relativeNotification);
            tv_notificationTitle = view.findViewById(R.id.tv_notificationTitle);
            tv_notificationTime = view.findViewById(R.id.tv_notificationTime);
            relNotificationImage = view.findViewById(R.id.relNotificationImage);
        }
    }
}
