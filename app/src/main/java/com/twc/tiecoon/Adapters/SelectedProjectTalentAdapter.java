package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.PreferredLanguageData;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

public class SelectedProjectTalentAdapter extends RecyclerView.Adapter<SelectedProjectTalentAdapter.ViewHolder> {

    private List<ProfileTalent> preferredLanguageDataList;
    private ItemClickListener itemClickListener;

    public SelectedProjectTalentAdapter(Context context, List<ProfileTalent> profileTalentList, ItemClickListener itemClickListener) {
        this.preferredLanguageDataList = profileTalentList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public SelectedProjectTalentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_talent, parent, false);
        return new SelectedProjectTalentAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedProjectTalentAdapter.ViewHolder holder, final int position) {
        holder.tv_talent.setText(preferredLanguageDataList.get(position).getName());
        holder.fabCancelItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.itemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return preferredLanguageDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_talent;
        private FloatingActionButton fabCancelItem;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_talent = itemView.findViewById(R.id.tv_talent);
            fabCancelItem = itemView.findViewById(R.id.fabCancelItem);
        }
    }
}
