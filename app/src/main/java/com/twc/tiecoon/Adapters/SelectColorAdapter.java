package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.twc.tiecoon.R;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class SelectColorAdapter extends RecyclerView.Adapter<SelectColorAdapter.ViewHolder> {
    private Context context;
    private final List<String> list;
    private final ItemClickListener itemClickListener;

    public SelectColorAdapter(Context context, List<String> list, ItemClickListener itemClickListener) {
        this.context = context;
        this.list = list;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.select_color_items, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.linear_Select_Color.getBackground().setTint(Color.parseColor(list.get(position)));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linear_Select_Color;

        public ViewHolder(View itemView) {
            super(itemView);
            linear_Select_Color = itemView.findViewById(R.id.linear_Select_Color);
            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAdapterPosition()));
        }
    }
}
