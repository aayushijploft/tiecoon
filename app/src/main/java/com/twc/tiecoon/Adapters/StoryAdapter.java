package com.twc.tiecoon.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.StoryView;
import com.twc.tiecoon.network.response.StoryViewData;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.MyViewHolder> {

    private final List<StoryView.Data> storyDataList;
    Context context;
    private ItemClickListener itemClickListener;
    public StoryAdapter(Context context, List<StoryView.Data> storyDataList,
                        ItemClickListener itemClickListener) {
        this.context=context;
        this.storyDataList=storyDataList;
        this.itemClickListener=itemClickListener;

    }

    @NotNull
    @Override
    public StoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.myprofile_image_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull StoryAdapter.MyViewHolder holder, int position) {


        if(position!= 0){
            Glide.with(context).load(storyDataList.get(position).getProfile_image()).placeholder(R.drawable.userddummyset)
                    .error(R.drawable.userddummyset)
                    .into(holder.imgProfile);
            holder.imgProfile.setOnClickListener(v -> itemClickListener.itemClick(position));
        }
        else holder.layoutStory.setVisibility(View.GONE);

    }
    @Override
    public int getItemCount() {
        return storyDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView imgProfile;
        CardView layoutStory;
        MyViewHolder(View view) {
            super(view);
            imgProfile = view.findViewById(R.id.imgProfile);
            layoutStory = view.findViewById(R.id.layoutStory);
        }

    }
}
