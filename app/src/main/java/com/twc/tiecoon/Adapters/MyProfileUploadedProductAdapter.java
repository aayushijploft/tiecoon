package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;

import java.util.ArrayList;
import java.util.List;

public class MyProfileUploadedProductAdapter extends RecyclerView.Adapter<MyProfileUploadedProductAdapter.MyViewHolder> {
    private List<List<HomeUserDetailsProduct>> homeUserDetailDataList;
    private ItemClickListenerExtraThreeParam itemClickListener;
    private Context context;

    public MyProfileUploadedProductAdapter(Context context, List<List<HomeUserDetailsProduct>> homeUserDetailDataList,   ItemClickListenerExtraThreeParam  itemClickListener) {
        this.context = context;
        this.homeUserDetailDataList = homeUserDetailDataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyProfileUploadedProductAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.myprofile_talents_item, parent, false);
        return new MyProfileUploadedProductAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyProfileUploadedProductAdapter.MyViewHolder holder, int position) {
        List<HomeUserDetailsProduct> newList = new ArrayList<>();
        List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList = homeUserDetailDataList.get(position);
        if (homeUserDetailsUploadTalentList != null && !(homeUserDetailsUploadTalentList.isEmpty())) {
            if(homeUserDetailsUploadTalentList.get(0).getCategory()!=null)
            {
                if (homeUserDetailsUploadTalentList.get(0).getCategory().getTitle() != null) {
                    holder.tvTitle.setText(homeUserDetailsUploadTalentList.get(0).getCategory().getTitle());
                }
            }
            for (HomeUserDetailsProduct homeUserDetailsUploadTalent : homeUserDetailsUploadTalentList) {
                if (homeUserDetailsUploadTalent.getId() != null && homeUserDetailsUploadTalent.getUserId() != null) {
                    newList.add(homeUserDetailsUploadTalent);
                }
            }

            if (newList.size() > 0) {
                AddProductAdapters addProductAdapters = new AddProductAdapters(context, newList, (pos, subPos, type) -> itemClickListener.itemClick(position,String.valueOf(pos),type));
                holder.recyclerView_upload_product.setAdapter(addProductAdapters);
            }
        }
        holder.tvUpload.setText("Upload");
        holder.tvUpload.setOnClickListener(view -> itemClickListener.itemClick(position,"",""));
        holder.fabCancelItem.setOnClickListener(view -> itemClickListener.itemClick(position,"","deleteProduct"));


    }

    @Override
    public int getItemCount() {
        return homeUserDetailDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvUpload;
        private TextView tvTitle,tvalent;
        private RecyclerView recyclerView_upload_product;

        FloatingActionButton fabCancelItem;
        MyViewHolder(View view) {
            super(view);
            tvUpload = view.findViewById(R.id.tvUpload);
            tvalent= view.findViewById(R.id.tvalent);
            tvTitle = view.findViewById(R.id.tvTitle);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            recyclerView_upload_product = view.findViewById(R.id.recyclerView_upload_product);
        }
    }
}