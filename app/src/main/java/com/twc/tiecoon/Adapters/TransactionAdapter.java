package com.twc.tiecoon.Adapters;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.BuildConfig;
import com.twc.tiecoon.Activities.BuyServiceActivity;
import com.twc.tiecoon.Activities.FeedbackActivity;
import com.twc.tiecoon.Activities.ServiceFeedbackActivity;
import com.twc.tiecoon.Activities.ServiceRequestMoneyActivity;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.Feedback;
import com.twc.tiecoon.network.response.TransactionListModel;
import com.twc.tiecoon.network.response.TransactionModel;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static com.facebook.FacebookSdk.getApplicationContext;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyViewHolder> {

    final List<TransactionListModel.Data> list;
    Context context;

    public TransactionAdapter(Context context,List<TransactionListModel.Data> list) {
        this.context=context;
        this.list=list;
    }

    @NotNull    
    @Override
    public TransactionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_item, parent, false);
        return new TransactionAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull TransactionAdapter.MyViewHolder holder, int position) {

        holder.tv_transactionTitle.setText(list.get(position).massage);
        switch (list.get(position).type) {
            case "talent":
                holder.tv_digitid.setText("Talent Id: " + list.get(position).digitid);
                break;
            case "talantbuy":
                holder.tv_digitid.setText("Talent Id: " + list.get(position).digitid);
                break;
            case "product":
                holder.tv_digitid.setText("Product Id: " + list.get(position).digitid);
                break;
            case "service":
                holder.tv_digitid.setText("Service Id: " + list.get(position).digitid);
                break;
            case "project":
                holder.tv_digitid.setText("Project Id: " + list.get(position).digitid);
                break;
        }

        holder.tv_notificationTime.setText(list.get(position).date+" "+list.get(position).time);

        if(list.get(position).type!=null){

            if(list.get(position).type.equals("talent")){
                holder.layoutButton.setVisibility(View.VISIBLE);
                holder.tv_orderid.setVisibility(View.VISIBLE);
                holder.tv_transactionid.setVisibility(View.GONE);
                holder.tv_collabid.setVisibility(View.GONE);
                holder.tv_trackid.setVisibility(View.GONE);
                holder.tv_trakinglink.setVisibility(View.GONE);
                holder.tvTackLink.setVisibility(View.GONE);
                holder.tv_price.setText(list.get(position).currency+" "+list.get(position).price);
                holder.tv_orderid.setText("ORDER ID: "+list.get(position).order_id);
            }

            if(list.get(position).type.equals("talantbuy")){
                holder.layoutButton.setVisibility(View.GONE);
                holder.tv_orderid.setVisibility(View.VISIBLE);
                holder.tv_transactionid.setVisibility(View.GONE);
                holder.tv_collabid.setVisibility(View.GONE);
                holder.tv_trackid.setVisibility(View.GONE);
                holder.tv_trakinglink.setVisibility(View.GONE);
                holder.tvTackLink.setVisibility(View.GONE);
                holder.tv_price.setText(list.get(position).currency+" "+list.get(position).price);
                holder.tv_orderid.setText("ORDER ID: "+list.get(position).order_id);
            }

            if(list.get(position).type.equals("product")){
                holder.tv_transactionid.setVisibility(View.GONE);
                holder.tv_orderid.setVisibility(View.VISIBLE);
                holder.tv_productFeedback.setVisibility(View.VISIBLE);
                holder.tv_collabid.setVisibility(View.GONE);
                holder.tv_bookingbid.setVisibility(View.GONE);
                holder.tv_trackid.setVisibility(View.VISIBLE);
                holder.tv_trakinglink.setVisibility(View.VISIBLE);
                holder.tvTackLink.setVisibility(View.VISIBLE);

                if(list.get(position).self!=null){
                    if(list.get(position).self.equals("yes")){
                        holder.tv_productFeedback.setVisibility(View.GONE);
                        holder.tv_trackid.setVisibility(View.GONE);
                        holder.tv_trakinglink.setVisibility(View.GONE);
                        holder.tvTackLink.setVisibility(View.GONE);
                    }
                }


                holder.tv_price.setText(list.get(position).currency+" "+list.get(position).price);
                holder.tv_orderid.setText("ORDER ID: "+list.get(position).order_id);
                holder.tv_productFeedback.setText("Give Feedback - "+list.get(position).productname);
                holder.tv_trackid.setText("TRACKING ID: "+list.get(position).tracking_id);
                Log.e("ttli",list.get(position).tracking_link);
                holder.tv_trakinglink.setText(list.get(position).tracking_link);


            }
            holder.tv_trakinglink.setOnClickListener(v -> {
                Intent viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(list.get(position).tracking_link));
                context.startActivity(viewIntent);
            });

            if(list.get(position).type.equals("project")){
                if(list.get(position).is_release.equals("yes")){
                    holder.tv_transactionid.setVisibility(View.GONE);
                }
               else holder.tv_transactionid.setVisibility(View.VISIBLE);

                holder.tv_collabid.setVisibility(View.VISIBLE);
                holder.tv_bookingbid.setVisibility(View.GONE);
                holder.tv_orderid.setVisibility(View.GONE);
                holder.tv_trackid.setVisibility(View.GONE);
                holder.tv_trakinglink.setVisibility(View.GONE);
                holder.tvTackLink.setVisibility(View.GONE);
                holder.tv_transactionid.setText("TXN ID: "+list.get(position).trangctionId);
                holder.tv_collabid.setText("COLLAB ID: "+list.get(position).collabId);
                holder.tv_price.setText(list.get(position).currency+" "+list.get(position).price);
            }

            if(list.get(position).type.equals("service")){
                if(list.get(position).is_release.equals("yes")){
                    holder.tv_transactionid.setVisibility(View.GONE);
                }
                else holder.tv_transactionid.setVisibility(View.VISIBLE);

                holder.tv_bookingbid.setVisibility(View.VISIBLE);
                holder.tv_collabid.setVisibility(View.GONE);
                holder.tv_orderid.setVisibility(View.GONE);
                holder.tv_trackid.setVisibility(View.GONE);
                holder.tv_trakinglink.setVisibility(View.GONE);
                holder.tvTackLink.setVisibility(View.GONE);
                holder.tv_price.setText(list.get(position).currency+" "+list.get(position).price);
                holder.tv_transactionid.setText("TXN ID: "+list.get(position).trangctionId);
                holder.tv_bookingbid.setText("BOOKING ID: "+list.get(position).booking_id);
            }

        }

        holder.tv_productFeedback.setOnClickListener(v -> {
            context.startActivity(new Intent(context, FeedbackActivity.class).putExtra("type","product")
            .putExtra("typeid",list.get(position).type_id));
        });


        String url = list.get(position).download;
        holder.button_Download.setOnClickListener(v -> {
            Random random = new Random();
            String id = String.format("%04d", random.nextInt(10000));
            String extension = url.substring(url.lastIndexOf("."));
            String filename= id+extension;
            downloadFile(context,url,filename);
        });

        holder.button_Feedback.setOnClickListener(v -> {
            if(list.get(position).type.equals("service")){
                if(list.get(position).is_feedback.equals("0")){
                    context.startActivity(new Intent(context, ServiceFeedbackActivity.class)
                            .putExtra("userid",list.get(position).service_owner_id).
                                    putExtra("serviceid",list.get(position).service_id)
                            .putExtra("type","user"));
                }
            }
            else {
                context.startActivity(new Intent(context, FeedbackActivity.class)
                        .putExtra("type",list.get(position).type)
                        .putExtra("typeid",list.get(position).type_id)
                );
            }
        });

        if(list.get(position).type.equals("service")){
            if(list.get(position).feedback != null){
                if(list.get(position).feedback.equals("1")) {
                    holder.layoutButton.setVisibility(View.VISIBLE);
                    holder.button_Download.setVisibility(View.GONE);
                    if(list.get(position).is_feedback.equals("1")) {
                        holder.button_Feedback.setBackground(context.getResources().getDrawable(R.drawable.greybtn));
                        holder.button_Feedback.setEnabled(false);
                    }
                    else {
                        holder.button_Feedback.setBackground(context.getResources().getDrawable(R.drawable.custom_bg));
                        holder.button_Feedback.setEnabled(true);
                    }
                }
                else {
                    holder.layoutButton.setVisibility(View.GONE);
                }
            }
        }

        holder.relativeNotification.setOnClickListener(v -> {
            if(list.get(position).type.equals("service")){
                if(list.get(position).is_owner.equals("yes")){
                    context.startActivity(new Intent(context, ServiceRequestMoneyActivity.class)
                            .putExtra("userid",list.get(position).service_owner_id)
                            .putExtra("serviceid",list.get(position).service_id)
                            .putExtra("ServiceIdGen",list.get(position).service_digitid)
                            .putExtra("amount",list.get(position).price)
                            .putExtra("username",list.get(position).username)
                    );
                }
                else {
                    context.startActivity(new Intent(context, BuyServiceActivity.class)
                            .putExtra("userid",list.get(position).service_owner_id)
                            .putExtra("serviceid",list.get(position).service_id)
                            .putExtra("ServiceIdGen",list.get(position).service_digitid)
                    );
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_transactionTitle,tv_notificationTime,tv_price,
                tv_productFeedback,tv_digitid,tv_transactionid,tv_collabid,tv_orderid,
                tv_trackid,tvTackLink,tv_trakinglink,tv_bookingbid;
        LinearLayout layoutButton,relativeNotification;
        Button button_Download,button_Feedback;

        MyViewHolder(View view) {
            super(view);
            tv_transactionTitle = view.findViewById(R.id.tv_transactionTitle);
            tv_digitid = view.findViewById(R.id.tv_digitid);
            tv_notificationTime = view.findViewById(R.id.tv_notificationTime);
            layoutButton = view.findViewById(R.id.layoutButton);
            button_Download = view.findViewById(R.id.button_Download);
            button_Feedback = view.findViewById(R.id.button_Feedback);
            tv_transactionid = view.findViewById(R.id.tv_transactionid);
            tv_price = view.findViewById(R.id.tv_price);
            relativeNotification = view.findViewById(R.id.relativeNotification);
            tv_collabid = view.findViewById(R.id.tv_collabid);
            tv_orderid = view.findViewById(R.id.tv_orderid);
            tv_trackid = view.findViewById(R.id.tv_trackid);
            tv_trakinglink = view.findViewById(R.id.tv_trakinglink);
            tvTackLink = view.findViewById(R.id.tvTackLink);
            tv_productFeedback = view.findViewById(R.id.tv_productFeedback);
            tv_bookingbid = view.findViewById(R.id.tv_bookingbid);
        }
    }

    public void downloadFile(final Context activity, final String url, final String fileName) {
        try {
            if (url != null && !url.isEmpty()) {
                Uri uri = Uri.parse(url);
                activity.registerReceiver(attachmentDownloadCompleteReceive, new IntentFilter(
                        DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setMimeType(getMimeType(uri.toString()));
                request.setTitle(fileName);
                request.setDescription("Downloading attachment..");
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                DownloadManager dm = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
                dm.enqueue(request);
            }
        } catch (IllegalStateException e) {
            Toast.makeText(activity, "Please insert an SD card to download file", Toast.LENGTH_SHORT).show();
        }
    }

    private String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }

    BroadcastReceiver attachmentDownloadCompleteReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                long downloadId = intent.getLongExtra(
                        DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                openDownloadedAttachment(context, downloadId);
            }
        }
    };

    private void openDownloadedAttachment(final Context context, final long downloadId) {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(downloadId);
        Cursor cursor = downloadManager.query(query);
        if (cursor.moveToFirst()) {
            int downloadStatus = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
            String downloadLocalUri = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
            String downloadMimeType = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE));
            if ((downloadStatus == DownloadManager.STATUS_SUCCESSFUL) && downloadLocalUri != null) {
                openDownloadedAttachment(context, Uri.parse(downloadLocalUri), downloadMimeType);
            }
        }
        cursor.close();
    }

    private void openDownloadedAttachment(final Context context, Uri attachmentUri, final String attachmentMimeType) {
        if(attachmentUri!=null) {
            // Get Content Uri.
            if (ContentResolver.SCHEME_FILE.equals(attachmentUri.getScheme())) {
                // FileUri - Convert it to contentUri.
                File file = new File(attachmentUri.getPath());
                attachmentUri =   FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        BuildConfig.APPLICATION_ID + ".provider", file);
            }
            Intent openAttachmentIntent = new Intent(Intent.ACTION_VIEW);
            openAttachmentIntent.setDataAndType(attachmentUri, attachmentMimeType);
            openAttachmentIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                context.startActivity(openAttachmentIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(context, "unable_to_open_file", Toast.LENGTH_LONG).show();
            }
        }
    }

}

