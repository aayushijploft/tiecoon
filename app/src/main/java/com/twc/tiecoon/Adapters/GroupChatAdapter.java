package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.Activities.OtherUserProfileActivity;
import com.twc.tiecoon.Activities.ProjectInformationActivity;
import com.twc.tiecoon.Activities.UserHomeActivity;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.GroupChatModel;
import com.twc.tiecoon.network.response.OnetooneChatModel;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

public class GroupChatAdapter extends RecyclerView.Adapter<GroupChatAdapter.MyViewHolder> {

    private Context context;
    private List<GroupChatModel.Data> dataList;
    private ItemClickListener itemClickListener;

    public GroupChatAdapter(Context context,List<GroupChatModel.Data> dataList,ItemClickListener itemClickListener) {
        super();
        this.context = context;
        this.dataList = dataList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public GroupChatAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_chat_item, parent, false);

        return new GroupChatAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupChatAdapter.MyViewHolder holder, int position) {
        holder.tv_name.setText(dataList.get(position).getName());
        holder.tv_recent_message.setText(dataList.get(position).getMassage());
        Glide.with(context)
                .load(dataList.get(position).getImage())
                .thumbnail(Glide.with(context).load(R.drawable.userddummyset))
                .error(Glide.with(context)
                        .load(R.drawable.userddummyset))
                .into(holder.image);

        holder.image.setOnClickListener(view -> {
            if(dataList.get(position).getProjectuser_id() == ApplicationClass.appPreferences.getUserId() ){
                Intent i = new Intent(context, UserHomeActivity.class);
                i.putExtra("open","profile");
                context.startActivity(i);
            }
            else {
                Intent i = new Intent(context, OtherUserProfileActivity.class);
                i.putExtra("UserId",dataList.get(position).getProjectuser_id()+"");
                context.startActivity(i);
            }

        });
        holder.tv_recent_message.setVisibility(View.GONE);
        holder.layoutchat.setOnClickListener(v -> itemClickListener.itemClick(position));

        holder.icMenu.setOnClickListener(v -> {
            context.startActivity(new Intent(context, ProjectInformationActivity.class).
                    putExtra("group_id",dataList.get(position).getId()+"").
                    putExtra("group_name",dataList.get(position).getName()+"").
                    putExtra("project_id",dataList.get(position).getProject_id()+"").
                    putExtra("is_admin",dataList.get(position).isIs_admin()+""));

        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image,icMenu;
        TextView tv_name,tv_recent_message;
        RelativeLayout rlChat;
        LinearLayout layoutchat;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.bell1);
            icMenu = itemView.findViewById(R.id.icMenu);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_recent_message = itemView.findViewById(R.id.tv_recent_message);
            rlChat = itemView.findViewById(R.id.rlChat);
            layoutchat = itemView.findViewById(R.id.layoutchat);
        }
    }
}
