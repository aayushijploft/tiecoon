package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.CityDataList;
import com.twc.tiecoon.network.response.CountryDataList;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {
    private Context context;
    private List<CityDataList> list;
    private ItemClickListener itemClickListener;

    public CityAdapter(Context context, List<CityDataList> list, ItemClickListener itemClickListener) {
        this.context = context;
        this.list = list;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public CityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.curreny_item_list, parent, false);
        return new CityAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(CityAdapter.ViewHolder holder, final int position) {
        holder.tv_currency.setText(new StringBuilder().append(list.get(position).getName()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_currency;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_currency = itemView.findViewById(R.id.tv_currency);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(getAdapterPosition());
                }
            });
        }
    }
}


