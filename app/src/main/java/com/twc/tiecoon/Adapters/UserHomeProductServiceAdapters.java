package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class UserHomeProductServiceAdapters extends RecyclerView.Adapter<UserHomeProductServiceAdapters.MyViewHolder> {
    private List<HomeUserDetailsProduct> homeUserDetailsProductList;
    private Context context;
    private ItemClickListener itemClickListener;
    public UserHomeProductServiceAdapters(Context context, List<HomeUserDetailsProduct> talentDataList,
                                          ItemClickListener itemClickListener) {
        this.context = context;
        this.homeUserDetailsProductList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_talents_grid_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailsProduct homeUserDetailsProduct = homeUserDetailsProductList.get(position);
        if (homeUserDetailsProduct != null) {
            if(homeUserDetailsProduct.getUpload_image().get(0).getCreateimage() != null){
                switch (homeUserDetailsProduct.getUpload_image().get(0).getCreateimage()) {
                    case "image":
                        holder.image.setVisibility(View.VISIBLE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        String imageURL = homeUserDetailsProduct.getCreateimage();
                        Glide.with(context).load(homeUserDetailsProduct.getUpload_image().get(0).getImage()).error(R.drawable.dummy_place)
                                .error(R.drawable.dummy_place)
                                .into(holder.image);
                        break;
                    case "video":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.VISIBLE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        if (homeUserDetailsProduct.getUpload_image().get(0).getVideo_thumbnail() != null) {
                            String imageURL1 = Constant.ImageURL + "admin/upload/product/" + homeUserDetailsProduct.getUpload_image().get(0).getVideo_thumbnail();
                            Glide.with(context).load(imageURL1).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(holder.ivThumbnail);
                        }
                        break;
                }
            }

            holder.cardView.setOnClickListener(view -> {
                    itemClickListener.itemClick(position);
                });
            }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailsProductList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        RelativeLayout relativeWaterMark;
        TextView tvWatermark;
        ImageView image,ivThumbnail;
        FrameLayout layoutAudio,layoutVideo;
        MyViewHolder(View view) {
            super(view);
            cardView= view.findViewById(R.id.cardView);
            relativeWaterMark = view.findViewById(R.id.relativeWaterMark);
            tvWatermark = view.findViewById(R.id.tvWatermark);
            image = view.findViewById(R.id.image);
            layoutAudio = view.findViewById(R.id.layoutAudio);
            layoutVideo = view.findViewById(R.id.layoutVideo);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);
        }
    }
}