package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddProductCategoryData;
import com.twc.tiecoon.network.response.AddProductSubCategoryData;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class MyProfileTalentHorizontalAdapters extends RecyclerView.Adapter<MyProfileTalentHorizontalAdapters.MyViewHolder> {
    private final List<HomeUserDetailsUploadTalent> talentDataList;
    private final ItemClickListenerExtraThreeParam itemClickListener;
    private final Context context;

    public MyProfileTalentHorizontalAdapters(Context context, List<HomeUserDetailsUploadTalent> talentDataList, ItemClickListenerExtraThreeParam itemClickListener) {
        this.context = context;
        this.talentDataList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_product_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailsUploadTalent uploadProductData = talentDataList.get(position);
        if (uploadProductData != null) {
            AddProductCategoryData addProductCategoryData = uploadProductData.getCategory();
            if (addProductCategoryData != null && addProductCategoryData.getTitle() != null) {
                holder.tvCategory.setText(addProductCategoryData.getTitle());
            }
            AddProductSubCategoryData addProductSubCategoryData = uploadProductData.getSubCategory();
            if (addProductSubCategoryData != null) {
                if (addProductSubCategoryData.getTitle() != null)
                    holder.tvSubCategory1.setText(addProductSubCategoryData.getTitle());
                    holder.tvCategory.setText(addProductSubCategoryData.getTitle());
            }
            if(uploadProductData.getTalant_type() != null){
                switch (uploadProductData.getTalant_type()) {
                    case "image":
                        holder.image.setVisibility(View.VISIBLE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        String imageURL = Constant.ImageURL + uploadProductData.getPath() + "/" + uploadProductData.getCreateimage();
                        Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                                .error(R.drawable.dummy_place)
                                .into(holder.image);
                        break;
                    case "audio":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.VISIBLE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        break;
                    case "video":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.VISIBLE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        if (uploadProductData.getVideothumbnail() != null) {
                            String imageURL1 = Constant.ImageURL + "/" + uploadProductData.getVideothumbnail();
                            Glide.with(context).load(imageURL1).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(holder.ivThumbnail);
                        }
                        break;
                    case "textarea":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                    holder.relativeWaterMark.setVisibility(View.VISIBLE);
                    if (uploadProductData.getFontcolor() != null && !(uploadProductData.getFontcolor().isEmpty())) {
                        try {
                            holder.relativeWaterMark.setBackgroundColor(Color.parseColor(uploadProductData.getFontcolor()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else  holder.relativeWaterMark.setBackgroundColor(Color.parseColor("#3B6FFF"));
                    if (uploadProductData.getTextarea() != null)
                        holder.tvWatermark.setText(  Html.fromHtml(uploadProductData.getTextarea()));
                        break;
                }
            }

            if (uploadProductData.getDescription() != null) {
                holder.textViewDescription.setText(uploadProductData.getDescription());
            }
            if (uploadProductData.getTextarea() != null) {
                holder.tvTextArea.setText(uploadProductData.getTextarea());
            }
            if (uploadProductData.getPricecode() != null && uploadProductData.getPrice() != null) {
                String pricecode;
                if(uploadProductData.getPricecode().equals("USD")){
                    pricecode = "$";
                }
                else pricecode  = "₹";
                holder.textViewPrice.setText(new StringBuilder().append("").append(pricecode).append(" ").append(uploadProductData.getPrice()));
            }
        }
        holder.fabCancelItem.setOnClickListener(view -> itemClickListener.itemClick(position,"","delete"));
        holder.fabEditItem.setOnClickListener(view -> itemClickListener.itemClick(position,"","update"));
        holder.cardView.setOnClickListener(view -> itemClickListener.itemClick(position,"","view"));
    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeWaterMark;
        ImageView image,ivThumbnail;
        FloatingActionButton fabEditItem,fabCancelItem;
        TextView tvWatermark,tvCategory, textViewDescription, textViewPrice, tvSubCategory1, tvSubCategory2,tvTextArea;
        FrameLayout layoutAudio,layoutVideo;
        CardView cardView;
        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);
            relativeWaterMark = view.findViewById(R.id.relativeWaterMark);
            tvWatermark = view.findViewById(R.id.tvWatermark);
            fabEditItem = view.findViewById(R.id.fabEditItem);
            cardView = view.findViewById(R.id.cardView);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            tvCategory = view.findViewById(R.id.tvCategory);
            textViewDescription = view.findViewById(R.id.textViewDescription);
            textViewPrice = view.findViewById(R.id.textViewPrice);
            tvTextArea = view.findViewById(R.id.tvTextArea);
            tvSubCategory1 = view.findViewById(R.id.tvSubCategory1);
            tvSubCategory2 = view.findViewById(R.id.tvSubCategory2);
            layoutAudio = view.findViewById(R.id.layoutAudio);
            layoutVideo = view.findViewById(R.id.layoutVideo);

        }
    }
}