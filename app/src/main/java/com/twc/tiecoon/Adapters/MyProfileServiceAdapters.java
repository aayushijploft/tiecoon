package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.ProductCategory;
import com.twc.tiecoon.network.response.ServiceCategory;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyProfileServiceAdapters extends RecyclerView.Adapter<MyProfileServiceAdapters.MyViewHolder> {
    private List<ServiceCategory.Data> dataList;
    private ItemClickListenerExtraThreeParam itemClickListener;
    private Context context;

    public MyProfileServiceAdapters(Context context, List<ServiceCategory.Data> dataList, ItemClickListenerExtraThreeParam itemClickListener) {
        this.context = context;
        this.dataList = dataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyProfileServiceAdapters.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.myprofile_talents_product_services_item, parent, false);
        return new MyProfileServiceAdapters.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyProfileServiceAdapters.MyViewHolder holder, int position) {

        holder.tvTitle.setText(dataList.get(position).getTitle());

//            AddProductAdapters addProductAdapters = new AddProductAdapters(context, homeUserDetailsProductList, new ItemClickListenerExtraThreeParam() {
//                @Override
//                public void itemClick(int pos, String subPos, String type) {
//                    itemClickListener.itemClick(position,String.valueOf(pos),type);
//                }
//            });
//                    holder.recyclerView_upload_product.setAdapter(addProductAdapters);

        holder.tvUpload.setText("Upload");
        holder.tvUpload.setOnClickListener(v ->  itemClickListener.itemClick(position,String.valueOf(position),"Upload"));
        holder.fabCancelItem.setOnClickListener(v ->  itemClickListener.itemClick(position,String.valueOf(position),"remove"));
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        //        private LinearLayout;
        private TextView tvTitle;
        private RecyclerView recyclerView_upload_product;
        TextView tvUpload;
        FloatingActionButton fabCancelItem;

        MyViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tvTitle);
            recyclerView_upload_product = view.findViewById(R.id.recyclerView_upload_product);
            tvUpload = view.findViewById(R.id.tvUpload);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
        }
    }
}