package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsReOrganisation;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyProfileReorganisationAdapters extends RecyclerView.Adapter<MyProfileReorganisationAdapters.ViewHolder> {
    private List<HomeUserDetailsReOrganisation> homeUserDetailsReOrganisations;
    private Context context;

    public MyProfileReorganisationAdapters(Context context, List<HomeUserDetailsReOrganisation> homeUserDetailsReOrganisations) {
        this.homeUserDetailsReOrganisations = homeUserDetailsReOrganisations;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_feedbacks_myprofile, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        HomeUserDetailsReOrganisation homeUserDetailsFeedback=   homeUserDetailsReOrganisations.get(position);
        if(homeUserDetailsFeedback!=null)
        {
//            if (homeUserDetailsFeedback.getUserDetils()!=null &&  homeUserDetailsFeedback.getUserDetils().getName() != null) {
//                holder.tvName.setText(homeUserDetailsFeedback.getUserDetils().getName());
//            }
//            holder.simpleRatingBar.setNumStars(homeUserDetailsFeedback.getRating());
//            holder.textViewDescription.setText(homeUserDetailsFeedback.getMassage());
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailsReOrganisations.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,textViewDescription;
        private RatingBar simpleRatingBar;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            simpleRatingBar = itemView.findViewById(R.id.rating_feedback);
        }
    }
}
