package com.twc.tiecoon.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.ProjectDetails;
import com.twc.tiecoon.network.response.StoryView;
import com.twc.tiecoon.utils.ItemClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewTalentAdapter extends RecyclerView.Adapter<ViewTalentAdapter.MyViewHolder> {

    private final List<ProjectDetails.Talent> storyDataList;
    Context context;
    public ViewTalentAdapter(Context context, List<ProjectDetails.Talent> storyDataList) {
        this.context=context;
        this.storyDataList=storyDataList;

    }

    @NotNull
    @Override
    public ViewTalentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_talent, parent, false);
        return new ViewTalentAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewTalentAdapter.MyViewHolder holder, int position) {


       holder.tv_talent.setText(storyDataList.get(position).getName());
       holder.fabCancelItem.setVisibility(View.GONE);




    }
    @Override
    public int getItemCount() {
        return storyDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        FloatingActionButton fabCancelItem;
        TextView tv_talent;
        MyViewHolder(View view) {
            super(view);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            tv_talent = view.findViewById(R.id.tv_talent);
        }

    }
}

