package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.twc.tiecoon.Activities.ProductServiceBrandsBuyActivity;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class OtherProfileTalentAdapters extends RecyclerView.Adapter<OtherProfileTalentAdapters.MyViewHolder> {
    private final List<List<HomeUserDetailsUploadTalent>> homeUserDetailDataList;
    private ItemClickListener itemClickListener;
    private final Context context;
   String qbid;

    public OtherProfileTalentAdapters(Context context, List<List<HomeUserDetailsUploadTalent>> homeUserDetailDataList, String qbid,
                                      ItemClickListener itemClickListener) {
        this.context = context;
        this.homeUserDetailDataList = homeUserDetailDataList;
        this.qbid = qbid;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_profile_talents_product_services_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = homeUserDetailDataList.get(position);
        if (homeUserDetailsUploadTalentList != null && !(homeUserDetailsUploadTalentList.isEmpty())) {
            if (homeUserDetailsUploadTalentList.get(0).getCategory().getTitle() != null) {
                holder.tvTitle.setText(homeUserDetailsUploadTalentList.get(0).getCategory().getTitle());
            }
            OtherProfileTalentHorizontalAdapters myProfileTalentHorizontalAdapters = new OtherProfileTalentHorizontalAdapters(context,
                    homeUserDetailsUploadTalentList, pos -> {
                Log.d("islike", homeUserDetailsUploadTalentList.get(0).isIs_like() + "_111__");
                        Intent intent=new Intent(context, ProductServiceBrandsBuyActivity.class);
                        intent.putExtra("talentData",homeUserDetailsUploadTalentList.get(pos));
                        intent.putExtra("qbID", qbid);
                        context.startActivity(intent);
                    });
            holder.recyclerView_upload_product.setAdapter(myProfileTalentHorizontalAdapters);
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;
        private final RecyclerView recyclerView_upload_product;

        MyViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tvTitle);
            recyclerView_upload_product = view.findViewById(R.id.recyclerView_upload_product);
        }
    }
}