package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.ProductCategory;
import com.twc.tiecoon.network.response.ServiceCategory;
import com.twc.tiecoon.utils.ItemClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SelectServiceAdapter extends RecyclerView.Adapter<SelectServiceAdapter.ViewHolder> {
    private final List<ServiceCategory.Data> list;
    private final ItemClickListener itemClickListener;
    Context context;

    public SelectServiceAdapter(Context context, List<ServiceCategory.Data> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public SelectServiceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.curreny_item_list, parent, false);
        return new SelectServiceAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(SelectServiceAdapter.ViewHolder holder, final int position) {
        holder.tv_currency.setText(list.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_currency;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_currency = itemView.findViewById(R.id.tv_currency);
            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAdapterPosition()));
        }
    }
}
