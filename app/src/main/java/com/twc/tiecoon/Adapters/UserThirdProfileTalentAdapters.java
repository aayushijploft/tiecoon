package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class UserThirdProfileTalentAdapters extends RecyclerView.Adapter<UserThirdProfileTalentAdapters.MyViewHolder> {
    private List<AddBrandsData> talentDataList;
    private Context context;
    private ItemClickListener itemClickListener;

    public UserThirdProfileTalentAdapters(Context context, List<AddBrandsData> talentDataList,
                                          ItemClickListener itemClickListener) {
        this.context = context;
        this.talentDataList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_talents_grid_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AddBrandsData homeUserDetailsUploadTalent = talentDataList.get(position);
        if (homeUserDetailsUploadTalent != null) {
            if (homeUserDetailsUploadTalent.getPath() != null && homeUserDetailsUploadTalent.getImage() != null) {
                String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailsUploadTalent.getPath()).append("/").append(homeUserDetailsUploadTalent.getImage())).toString();
                Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                        .error(R.drawable.dummy_place)
                        .into(holder.image);
            }
            holder.cardView.setOnClickListener(view -> {
                itemClickListener.itemClick(position);
            });
        }
    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
         CardView cardView;
        MyViewHolder(View view) {
            super(view);
            cardView= view.findViewById(R.id.cardView);
            image = view.findViewById(R.id.image);
        }
    }
}