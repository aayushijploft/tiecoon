package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.SingleItemRatingModel;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class SingleItemRatingAdapter extends RecyclerView.Adapter<SingleItemRatingAdapter.ViewHolder> {
    private final Context context;
   private final List<SingleItemRatingModel> list;

    public SingleItemRatingAdapter(Context context, List<SingleItemRatingModel> list) {
        this.context = context;
        this.list = list;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.buying_items_user_rating_item_layout, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        SingleItemRatingModel singleItemRatingModel=list.get(position);
        Glide.with(context).load(singleItemRatingModel.getPicURL()).error(R.drawable.dummy_place)
                .error(R.drawable.dummy_place)
                .into(holder.imgProfile);
        String name  =singleItemRatingModel.getUserName().substring(0, 1).toUpperCase() +singleItemRatingModel.getUserName().substring(1);
        holder.tv_UserName.setText(name);
        holder.rating_feedback.setRating(Float.parseFloat(singleItemRatingModel.getRating()));
        holder.simpleRatingBar.setText(singleItemRatingModel.getRating());
        holder.textViewDescription.setText(singleItemRatingModel.getDes());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tv_UserName;
        private final TextView simpleRatingBar;
        private final TextView textViewDescription;
        private final ImageView imgProfile;
        private final RatingBar rating_feedback;
        public ViewHolder(View itemView) {
            super(itemView);
            imgProfile= itemView.findViewById(R.id.imgProfile);
            tv_UserName = itemView.findViewById(R.id.tv_UserName);
            rating_feedback= itemView.findViewById(R.id.rating_feedback);
            simpleRatingBar= itemView.findViewById(R.id.simpleRatingBar);
            textViewDescription= itemView.findViewById(R.id.textViewDescription);
        }
    }
}
