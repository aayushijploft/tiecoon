package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class UserHomeTabTalentAdapters extends RecyclerView.Adapter<UserHomeTabTalentAdapters.MyViewHolder> {
    private final List<HomeUserDetailsUploadTalent> talentDataList;
    private final Context context;
    private final ItemClickListener itemClickListener;
    public UserHomeTabTalentAdapters(Context context, List<HomeUserDetailsUploadTalent> talentDataList,
                                     ItemClickListener itemClickListener  ) {
        this.context = context;
        this.talentDataList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_talents_grid_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = talentDataList.get(position);
        if (homeUserDetailsUploadTalent != null) {
            if(homeUserDetailsUploadTalent.getUpload_image().isEmpty()){
                holder.ivMultipleImage.setVisibility(View.GONE);
            }
            else holder.ivMultipleImage.setVisibility(View.VISIBLE);


            if(homeUserDetailsUploadTalent.getTalant_type() != null){
                switch (homeUserDetailsUploadTalent.getTalant_type()) {
                    case "image":
                        holder.image.setVisibility(View.VISIBLE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        String imageURL = Constant.ImageURL + homeUserDetailsUploadTalent.getPath() + "/" + homeUserDetailsUploadTalent.getCreateimage();
                        Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                                .error(R.drawable.dummy_place)
                                .into(holder.image);
                        break;
                    case "audio":
                        holder.image.setVisibility(View.GONE);
                        holder.ivMultipleImage.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.VISIBLE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        break;
                    case "video":
                        holder.image.setVisibility(View.GONE);
                        holder.ivMultipleImage.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.VISIBLE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        if (homeUserDetailsUploadTalent.getVideothumbnail() != null) {
                            String imageURL1 = Constant.ImageURL + "/" + homeUserDetailsUploadTalent.getVideothumbnail();
                            Glide.with(context).load(imageURL1).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(holder.ivThumbnail);
                        }
                        break;
                    case "textarea":
                        holder.image.setVisibility(View.GONE);
                        holder.ivMultipleImage.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.VISIBLE);
                        if (homeUserDetailsUploadTalent.getFontcolor() != null && !(homeUserDetailsUploadTalent.getFontcolor().isEmpty())) {
                            try {
                                holder.relativeWaterMark.setBackgroundColor(Color.parseColor(homeUserDetailsUploadTalent.getFontcolor()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (homeUserDetailsUploadTalent.getTextarea() != null)

                            holder.tvWatermark.setText(Html.fromHtml(homeUserDetailsUploadTalent.getTextarea()).toString());
                        break;
                }
            }
            holder.cardView.setOnClickListener(view -> itemClickListener.itemClick(position));
        }
    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private final CardView cardView;
        ImageView image,ivThumbnail,ivMultipleImage;
        RelativeLayout relativeWaterMark;
        TextView tvWatermark,tvTextArea;
        FrameLayout layoutAudio,layoutVideo;
        MyViewHolder(View view) {
            super(view);
            cardView= view.findViewById(R.id.cardView);
            relativeWaterMark = view.findViewById(R.id.relativeWaterMark);
            tvWatermark = view.findViewById(R.id.tvWatermark);
            image = view.findViewById(R.id.image);
            layoutAudio = view.findViewById(R.id.layoutAudio);
            layoutVideo = view.findViewById(R.id.layoutVideo);
            tvTextArea = view.findViewById(R.id.tvTextArea);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);
            ivMultipleImage = view.findViewById(R.id.ivMultipleImage);
        }
    }
}