package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.Activities.ProductDetailActivity;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.EnquiryModel;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

public class EnquiryReasonAdapter extends RecyclerView.Adapter<EnquiryReasonAdapter.ViewHolder> {
    private Context context;
    private List<EnquiryModel.Data> list;
    private ItemClickListener itemClickListener;

    public EnquiryReasonAdapter(Context context, List<EnquiryModel.Data> list, ItemClickListener itemClickListener) {
        this.context = context;
        this.list = list;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public EnquiryReasonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.enquiry_reason_layout, parent, false);
        return new EnquiryReasonAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(EnquiryReasonAdapter.ViewHolder holder, final int position) {
        holder.cbReason.setText(list.get(position).getName());
        holder.cbReason.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(holder.cbReason.isChecked()){
                ApplicationClass.ReasonList.add(list.get(position).getName());
                Log.e("ReasonList",ApplicationClass.ReasonList.toString());
            }
            else {
                ApplicationClass.ReasonList.remove(list.get(position).getName());
            }

        });
        holder.cbReason.setOnClickListener(v -> itemClickListener.itemClick(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox cbReason;

        public ViewHolder(View itemView) {
            super(itemView);
            cbReason = itemView.findViewById(R.id.cbReason);

        }
    }
}


