package com.twc.tiecoon.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.CategoryData;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class SelectCategoryAdapter extends RecyclerView.Adapter<SelectCategoryAdapter.ViewHolder> {
    private final List<CategoryData> categoryDataList;
    private final ItemClickListenerExtraParam itemClickListener;
    private int categoryID;
    private final Context context;
    private int mSelectedItem = -1;
    private final boolean isCategory;

    public SelectCategoryAdapter(Context context, int categoryID, boolean isCategory, List<CategoryData> categoryDataList, ItemClickListenerExtraParam itemClickListener) {
        this.categoryDataList = categoryDataList;
        this.itemClickListener = itemClickListener;
        this.categoryID = categoryID;
        this.context = context;
        this.isCategory = isCategory;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.category_item_list, parent, false);
        return new ViewHolder(listItem);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        CategoryData categoryData = categoryDataList.get(position);
        if (categoryData != null) {
            if (categoryData.getTitle() != null) {
                holder.tvTitle.setText(categoryDataList.get(position).getTitle());
            }
            if (isCategory) {
                holder.linearCategory.setBackground(context.getResources().getDrawable(R.drawable.rounder_border_linear_solid));
                holder.tvTitle.setTextColor(Color.WHITE);
                itemClickListener.itemClick(position, "update");
            } else {
                if (categoryID > 0 && categoryID == categoryData.getId()) {
                    holder.linearCategory.setBackground(context.getResources().getDrawable(R.drawable.rounder_border_linear_solid));
                    holder.tvTitle.setTextColor(Color.WHITE);
                    itemClickListener.itemClick(position, "update");
                    mSelectedItem = position;
                    categoryID = 0;
                } else {
                    if (mSelectedItem == position) {
                        holder.linearCategory.setBackground(context.getResources().getDrawable(R.drawable.rounder_border_linear_solid));
                        holder.tvTitle.setTextColor(Color.WHITE);
                    } else {
                        holder.linearCategory.setBackground(context.getResources().getDrawable(R.drawable.rounder_border_linear_stroke));
                        holder.tvTitle.setTextColor(context.getResources().getColor(R.color.black));
                    }
                }
                holder.linearCategory.setOnClickListener(view -> {
                    if (mSelectedItem >= 0) {
                        notifyItemChanged(mSelectedItem);
                    }
                    mSelectedItem = position;
                    notifyItemChanged(position);
                    itemClickListener.itemClick(position, "click");
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return categoryDataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;
        private final LinearLayout linearCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            linearCategory = itemView.findViewById(R.id.linearCategory);
        }
    }
}

