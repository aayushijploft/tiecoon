package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twc.tiecoon.Activities.ProductServiceBrandsBuyActivity;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.Fragments.HomeProductServiceFragment;
import com.twc.tiecoon.Fragments.HomeThirdProfileFragment;
import com.twc.tiecoon.Fragments.HomeUserTalentFragment;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.LikeUserListData;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import de.hdodenhof.circleimageview.CircleImageView;

public class LikeUserAdapter extends RecyclerView.Adapter<LikeUserAdapter.MyViewHolder> {
    private List<LikeUserListData> homeUserDetailDataList;
    private ItemClickListenerExtraParam itemClickListener;
    private Context context;
    private FragmentManager fragmentManager;

    Fragment homeUserTalentFragment, productServiceFragment, thirdProfileFragment, testimonialFragment, feedbackFragment,
            reorganisationFragment;

    public LikeUserAdapter(Context context, FragmentManager fragmentManager, List<LikeUserListData> homeUserDetailDataList,
                           ItemClickListenerExtraParam itemClickListener) {
        this.context = context;
        this.homeUserDetailDataList = homeUserDetailDataList;
        this.itemClickListener = itemClickListener;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_screen_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LikeUserListData homeUserDetailData = homeUserDetailDataList.get(position);
        Log.d("likedusers",""+homeUserDetailData.toString());
        if (homeUserDetailData != null) {
            //new Design
            List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentsNew = homeUserDetailData.getUploadTalent();
            Log.e("likeusersize", String.valueOf(homeUserDetailsUploadTalentsNew.size()));
            if (homeUserDetailsUploadTalentsNew != null && !(homeUserDetailsUploadTalentsNew.isEmpty())) {
                UserHomeTabTalentAdapters userHomeTabTalentAdapters = new UserHomeTabTalentAdapters(context, homeUserDetailsUploadTalentsNew, new ItemClickListener() {
                    @Override
                    public void itemClick(int pos) {
                        ApplicationClass.uploadImageListDataArrayList.clear();
                        if (homeUserDetailsUploadTalentsNew.get(pos).getUpload_image() != null && !(homeUserDetailsUploadTalentsNew.get(pos).getUpload_image().isEmpty())) {
                            ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsUploadTalentsNew.get(pos).getUpload_image());
                        }
                        Intent intent = new Intent(context, ProductServiceBrandsBuyActivity.class);
                        intent.putExtra("talentData", homeUserDetailsUploadTalentsNew.get(pos));
                        context.startActivity(intent);
                    }
                });
                holder.recyclerViewUploadImageList.setAdapter(userHomeTabTalentAdapters);
            }


            holder.imgLike.setImageResource(R.drawable.ic_like_solid);

            List  fragmentList = new ArrayList<>();
            List  titleList = new ArrayList<>();
            //TalentList
            List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents1 = homeUserDetailData.getUploadTalent();
//            ArrayList<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents2 = new ArrayList<>();
//            homeUserDetailsUploadTalents2.addAll(homeUserDetailsUploadTalents1);

            if (homeUserDetailsUploadTalents1 != null && !(homeUserDetailsUploadTalents1.isEmpty())) {
                HomeUserTalentFragment homeUserTalentFragment = new HomeUserTalentFragment();
                Bundle bundleProduct = new Bundle();
                bundleProduct.putInt("pos",position);
                bundleProduct.putParcelableArrayList("talent", (ArrayList<? extends Parcelable>) homeUserDetailsUploadTalents1);
                homeUserTalentFragment.setArguments(bundleProduct);
                fragmentList.add(homeUserTalentFragment);
                titleList.add("Talent");
            }else{
                HomeUserTalentFragment homeUserTalentFragment = new HomeUserTalentFragment();
                fragmentList.add(homeUserTalentFragment);
                titleList.add("Talent");
            }
            //ProductList
            List<HomeUserDetailsProduct> homeUserDetailsProductList1 = homeUserDetailData.getProducts();
            if (homeUserDetailsProductList1 != null && !(homeUserDetailsProductList1.isEmpty())) {
                HomeProductServiceFragment  productServiceFragment = new HomeProductServiceFragment();
                Bundle bundleProduct = new Bundle();
                bundleProduct.putParcelableArrayList("product", (ArrayList<? extends Parcelable>) homeUserDetailsProductList1);
                productServiceFragment.setArguments(bundleProduct);
                fragmentList.add(productServiceFragment);
                titleList.add("Product");
            }else {
                HomeProductServiceFragment  productServiceFragment = new HomeProductServiceFragment();
                fragmentList.add(productServiceFragment);
                titleList.add("Product");
            }
            //BrandList
            List<AddBrandsData> homeUserDetailBrandList1 = homeUserDetailData.getBrand();
            if (homeUserDetailBrandList1 != null && !(homeUserDetailBrandList1.isEmpty())) {
                HomeThirdProfileFragment  thirdProfileFragment = new HomeThirdProfileFragment();
                Bundle bundleThirdProfile = new Bundle();
                bundleThirdProfile.putParcelableArrayList("brand", (ArrayList<? extends Parcelable>) homeUserDetailBrandList1);
                thirdProfileFragment.setArguments(bundleThirdProfile);
                fragmentList.add(thirdProfileFragment);
                titleList.add("Brand");
            }else{
                HomeThirdProfileFragment  thirdProfileFragment = new HomeThirdProfileFragment();
                fragmentList.add(thirdProfileFragment);
                titleList.add("Brand");
            }
            final TodaysDealViewPagerAdapter adapter1 = new TodaysDealViewPagerAdapter(context, fragmentManager, fragmentList, titleList);
            holder.viewPager.setAdapter(adapter1);
            holder.viewPager.setId(position);
            holder.tabLayout.setupWithViewPager( holder.viewPager);
            setupTabIcons(holder);

           /* ArrayList<AddBrandsData> homeUserDetailBrandList = (ArrayList<AddBrandsData>) homeUserDetailData.getHomeUserDetailBrand();
            if (homeUserDetailBrandList != null && !(homeUserDetailBrandList.isEmpty())) {
                Bundle bundleBrand = new Bundle();
                bundleBrand.putParcelableArrayList("brand", homeUserDetailBrandList);
                thirdProfileFragment.setArguments(bundleBrand);
            }*/


            List<ProfileCreateAssignment> homeUserDetailCreateAssignmentList = new ArrayList<>();
            if (homeUserDetailData.getCreateAssignments() != null && !(homeUserDetailData.getCreateAssignments().isEmpty())) {
                for (ProfileCreateAssignment profileCreateAssignment : homeUserDetailData.getCreateAssignments()) {
                    if (profileCreateAssignment.getPath() != null && profileCreateAssignment.getImage() != null) {
                        homeUserDetailCreateAssignmentList.add(profileCreateAssignment);
                    }
                }
                OngoingAssignmentAdapters ongoingAssignmentAdapter = new OngoingAssignmentAdapters(context, homeUserDetailCreateAssignmentList);
                holder.recyclerView_ongoing_assignments.setAdapter(ongoingAssignmentAdapter);
            }

            holder.linearOtherUserProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "click");
                }
            });
            holder.linearLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "like");
                }
            });
            holder.linearConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "chat");
                }
            });
            holder.linearShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "share");
                }
            });
            holder.linearChat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "collaborate");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivProfile;
        private RecyclerView recyclerView_ongoing_assignments,recyclerViewUploadImageList;
        private TextView tvLocation, tvPrice, tvrating, tvName, tvProfileDes;
        private ImageView imgUserMenu, imgshare, imgLike, imgJoinGroupOne, imgJoinGroupTwo, imgJoinGroupCreateNew;
        private static final int CREATEASSGN = 110;
        private LinearLayout linearLike,linearOtherUserProfile, linearConnect, linearChat, linear3Profile, linear_add_ongoingAssignment,linearShare;
        private TabLayout tabLayout, tabLayout2;
        private ViewPager viewPager, viewPager2;

        MyViewHolder(View view) {
            super(view);
            ivProfile = view.findViewById(R.id.ivProfile);
            linearOtherUserProfile= view.findViewById(R.id.linearOtherUserProfile);
            tvPrice = view.findViewById(R.id.tvPrice);
            tvrating = view.findViewById(R.id.tvrating);
            tvLocation = view.findViewById(R.id.tvLocation);
            imgUserMenu = view.findViewById(R.id.imgUserMenu);
            tvName = view.findViewById(R.id.tvName);
            linearLike = view.findViewById(R.id.linearLike);
            tvProfileDes = view.findViewById(R.id.tvProfileDes);
            linearShare = view.findViewById(R.id.linearShare);
            imgshare = view.findViewById(R.id.imgshare);
            imgLike = view.findViewById(R.id.imgLike);

            linearConnect = view.findViewById(R.id.linearConnect);
            linearChat = view.findViewById(R.id.linearChat);
            linear3Profile = view.findViewById(R.id.linear3Profile);
            linear_add_ongoingAssignment = view.findViewById(R.id.linear_add_ongoingAssignment);
            recyclerViewUploadImageList = view.findViewById(R.id.recyclerViewUploadImageList);

            //first tablayout
            tabLayout = view.findViewById(R.id.tabLayout);
            viewPager = view.findViewById(R.id.viewPager);

            recyclerView_ongoing_assignments = view.findViewById(R.id.recyclerView_ongoing_assignments);

            //second tablayout
            tabLayout2 = view.findViewById(R.id.tabLayout2);
            viewPager2 = view.findViewById(R.id.viewPager2);
        }

    }


    private void setupTabIcons(MyViewHolder holder) {
        TextView tabOne = (TextView) LayoutInflater.from(context).inflate(R.layout.custom_tabs, null);
        tabOne.setText("Talent");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_star, 0, 0);
        holder.tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(context).inflate(R.layout.custom_tabs, null);
        tabTwo.setText("Product & Services");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_hand_pick, 0, 0);
        holder.tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(context).inflate(R.layout.custom_tabs, null);
        tabThree.setText("3rd Profile");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_user, 0, 0);
        holder.tabLayout.getTabAt(2).setCustomView(tabThree);
    }
        ViewPagerAdapter adapter;

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(fragmentManager);

        adapter.addFragment(homeUserTalentFragment, "Talent");
        adapter.addFragment(productServiceFragment, "Product & Services");
        adapter.addFragment(thirdProfileFragment, "3rd Profile");
        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }


        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public class TodaysDealViewPagerAdapter extends FragmentStatePagerAdapter {

        private Context myContext;
        private List<Fragment> fragmentList;
        private List<String> titleList;

        public TodaysDealViewPagerAdapter(Context context, FragmentManager fm, List<Fragment> fragmentList, List<String> titleList) {
            super(fm);
            myContext = context;
            this.fragmentList = fragmentList;
            this.titleList = titleList;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }

        // this is for fragment tabs
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        // this counts total number of tabs
        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}