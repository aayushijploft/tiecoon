package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.twc.tiecoon.Activities.ProductDetailActivity;
import com.twc.tiecoon.Activities.ProductServiceBrandsBuyActivity;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class OtherProfileProductAdapters extends RecyclerView.Adapter<OtherProfileProductAdapters.MyViewHolder> {
    private List<List<HomeUserDetailsProduct>> homeUserDetailDataList;
    private ItemClickListener itemClickListener;
    private Context context;

    String qbid;

    public OtherProfileProductAdapters(Context context, List<List<HomeUserDetailsProduct>> homeUserDetailDataList, String qbid,
                                       ItemClickListener itemClickListener) {
        this.context = context;
        this.homeUserDetailDataList = homeUserDetailDataList;
        this.qbid = qbid;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_profile_talents_product_services_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        List<HomeUserDetailsProduct> homeUserDetailsProductList = homeUserDetailDataList.get(position);
        if (homeUserDetailsProductList != null && !(homeUserDetailsProductList.isEmpty())) {
            if (homeUserDetailsProductList.get(0).getCategory()!=null && homeUserDetailsProductList.get(0).getCategory().getTitle()!=null) {
                int value=position+1;
                holder.tvTitle.setText(homeUserDetailsProductList.get(0).getCategory().getTitle());
            }
            OtherProfileProductHorizontalAdapters myProfileTalentHorizontalAdapters = new OtherProfileProductHorizontalAdapters(context,
                    homeUserDetailsProductList, new ItemClickListener() {
                @Override
                public void itemClick(int pos) {
                    Intent intent=new Intent(context, ProductDetailActivity.class);
                    intent.putExtra("product_id",homeUserDetailsProductList.get(pos).getId()+"");
                    intent.putExtra("qbID", qbid);
                    context.startActivity(intent);
                }
            });
            holder.recyclerView_upload_product.setAdapter(myProfileTalentHorizontalAdapters);
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private RecyclerView recyclerView_upload_product;

        MyViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tvTitle);
            recyclerView_upload_product = view.findViewById(R.id.recyclerView_upload_product);
        }
    }
}