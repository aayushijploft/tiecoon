package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyProfileTalentAdapters extends RecyclerView.Adapter<MyProfileTalentAdapters.MyViewHolder> {
    private List<List<HomeUserDetailsUploadTalent>> homeUserDetailDataList;
    private ItemClickListenerExtraThreeParam itemClickListener;
    private Context context;

    public MyProfileTalentAdapters(Context context, List<List<HomeUserDetailsUploadTalent>> homeUserDetailDataList,   ItemClickListenerExtraThreeParam  itemClickListener) {
        this.context = context;
        this.homeUserDetailDataList = homeUserDetailDataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.myprofile_talents_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        List<HomeUserDetailsUploadTalent> newList = new ArrayList<>();
        List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList = homeUserDetailDataList.get(position);
        if (homeUserDetailsUploadTalentList != null && !(homeUserDetailsUploadTalentList.isEmpty())) {
            if(homeUserDetailsUploadTalentList.get(0).getCategory()!=null)
            {
                if (homeUserDetailsUploadTalentList.get(0).getCategory().getTitle() != null) {
                    holder.tvTitle.setText(homeUserDetailsUploadTalentList.get(0).getCategory().getTitle());
                }
            }
            for (HomeUserDetailsUploadTalent homeUserDetailsUploadTalent : homeUserDetailsUploadTalentList) {
                if (homeUserDetailsUploadTalent.getId() != null && homeUserDetailsUploadTalent.getUserId() != null) {
                    newList.add(homeUserDetailsUploadTalent);
                }
            }

            if (newList.size() > 0) {
                MyProfileTalentHorizontalAdapters myProfileTalentHorizontalAdapters = new MyProfileTalentHorizontalAdapters(context, newList, (pos, subPos, type) -> itemClickListener.itemClick(position,String.valueOf(pos),type));
                holder.recyclerView_upload_product.setAdapter(myProfileTalentHorizontalAdapters);
            }
        }
        holder.tvUpload.setText("Upload");
        holder.tvUpload.setOnClickListener(view -> itemClickListener.itemClick(position,"",""));
        holder.fabCancelItem.setOnClickListener(view -> itemClickListener.itemClick(position,"","deletetalent"));


    }

    @Override
    public int getItemCount() {
        return homeUserDetailDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvUpload;
        private TextView tvTitle,tvalent;
        private RecyclerView recyclerView_upload_product;

        FloatingActionButton fabCancelItem;
        MyViewHolder(View view) {
            super(view);
            tvUpload = view.findViewById(R.id.tvUpload);
            tvalent= view.findViewById(R.id.tvalent);
            tvTitle = view.findViewById(R.id.tvTitle);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            recyclerView_upload_product = view.findViewById(R.id.recyclerView_upload_product);
        }
    }
}