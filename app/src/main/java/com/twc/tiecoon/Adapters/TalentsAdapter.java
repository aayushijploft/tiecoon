package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class TalentsAdapter extends RecyclerView.Adapter<TalentsAdapter.ViewHolder> {
    private Context context;
    private List<ProfileTalent> list;
    private ItemClickListener itemClickListener;

    public TalentsAdapter(Context context, List<ProfileTalent> list, ItemClickListener itemClickListener) {
        this.context = context;
        this.list = list;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.curreny_item_list, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tv_currency.setText(list.get(position).getName());
        Log.d("asdfasdf", list.get(position).getIsseletcted() + "__");
       /* if (list.get(position).getIsseletcted() == null || list.get(position).getIsseletcted().equals("0")) {
            holder.llmainlayout.setBackground(null);
        } else {
            holder.llmainlayout.setBackground(context.getResources().getDrawable(R.drawable.bg5));
        }*/
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView tv_currency;
        private final LinearLayout llmainlayout;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_currency = itemView.findViewById(R.id.tv_currency);
            llmainlayout = itemView.findViewById(R.id.llmainlayout);
            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAdapterPosition()));
        }
    }
}
