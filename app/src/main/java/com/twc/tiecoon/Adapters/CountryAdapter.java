package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.CountryDataList;
import com.twc.tiecoon.network.response.PreferredLanguageData;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {
    private Context context;
    private List<CountryDataList> list;
    private ItemClickListener itemClickListener;

    public CountryAdapter(Context context, List<CountryDataList> list, ItemClickListener itemClickListener) {
        this.context = context;
        this.list = list;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public CountryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.curreny_item_list, parent, false);
        return new CountryAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(CountryAdapter.ViewHolder holder, final int position) {
        holder.tv_currency.setText(new StringBuilder().append(list.get(position).getName()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_currency;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_currency = itemView.findViewById(R.id.tv_currency);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(getAdapterPosition());
                }
            });
        }
    }
}

