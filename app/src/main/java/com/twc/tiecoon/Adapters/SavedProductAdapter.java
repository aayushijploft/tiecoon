package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.AddProductSubCategoryData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SavedProductAdapter extends RecyclerView.Adapter<SavedProductAdapter.MyViewHolder> {
    private final List<HomeUserDetailsProduct> talentDataList;
    private final ItemClickListenerExtraThreeParam itemClickListener;
    private final Context context;

    public SavedProductAdapter(Context context, List<HomeUserDetailsProduct> talentDataList, ItemClickListenerExtraThreeParam itemClickListener) {
        this.context = context;
        this.talentDataList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public SavedProductAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_product_items, parent, false);
        return new SavedProductAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SavedProductAdapter.MyViewHolder holder, int position) {
        HomeUserDetailsProduct uploadProductData = talentDataList.get(position);
        if (uploadProductData != null) {
//            AddProductCategoryData addProductCategoryData = uploadProductData.getCategory();
//            if (addProductCategoryData != null && addProductCategoryData.getTitle() != null) {
//                holder.tvCategory.setText(addProductCategoryData.getTitle());
//            }
            AddProductSubCategoryData addProductSubCategoryData = uploadProductData.getSubCategory();
            if (addProductSubCategoryData != null) {
                if (addProductSubCategoryData.getTitle() != null)
                    holder.tvCategory.setText(addProductSubCategoryData.getTitle());
            }
            if (uploadProductData.getDescription() != null) {
                holder.textViewDescription.setText(uploadProductData.getDescription());
            }
            if (uploadProductData.getPricecode() != null && uploadProductData.getPrice() != null) {
                holder.textViewPrice.setText(new StringBuilder().append("Price: ").append(uploadProductData.getPricecode()).append(" ").append(uploadProductData.getPrice()));
            }
//            Log.e("upload_image",uploadProductData.getUpload_image().get(0).getCreateimage());

            if(uploadProductData.getUpload_image().get(0).getCreateimage() != null){
                switch (uploadProductData.getUpload_image().get(0).getCreateimage()) {
                    case "image":
                        holder.image.setVisibility(View.VISIBLE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        String imageURL = uploadProductData.getCreateimage();
                        Glide.with(context).load(uploadProductData.getUpload_image().get(0).getImage()).error(R.drawable.dummy_place)
                                .error(R.drawable.dummy_place)
                                .into(holder.image);
                        break;
                    case "video":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.VISIBLE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        if (uploadProductData.getUpload_image().get(0).getVideo_thumbnail() != null) {
                            String imageURL1 = uploadProductData.getUpload_image().get(0).getImage();
                            Log.e("vdoooo",imageURL1);
                            Glide.with(context).load(imageURL1).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(holder.ivThumbnail);
                        }
                        break;

                }
            }

        }
        holder.fabCancelItem.hide();
        holder.fabEditItem.hide();
        holder.cardView.setOnClickListener(view -> itemClickListener.itemClick(position,"",""));
    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeWaterMark;
        FloatingActionButton fabEditItem,fabCancelItem;
        ImageView image,ivThumbnail;
        CardView cardView;
        TextView tvWatermark, tvCategory, textViewDescription, textViewPrice, tvSubCategory1, tvSubCategory2;
        FrameLayout layoutAudio,layoutVideo;
        MyViewHolder(View view) {
            super(view);
            fabEditItem = view.findViewById(R.id.fabEditItem);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            image = view.findViewById(R.id.image);
            cardView = view.findViewById(R.id.cardView);
            relativeWaterMark = view.findViewById(R.id.relativeWaterMark);
            tvWatermark = view.findViewById(R.id.tvWatermark);
            tvCategory = view.findViewById(R.id.tvCategory);
            textViewDescription = view.findViewById(R.id.textViewDescription);
            textViewPrice = view.findViewById(R.id.textViewPrice);
            tvSubCategory1 = view.findViewById(R.id.tvSubCategory1);
            tvSubCategory2 = view.findViewById(R.id.tvSubCategory2);
            layoutAudio = view.findViewById(R.id.layoutAudio);
            layoutVideo = view.findViewById(R.id.layoutVideo);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);

        }
    }

}
