package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class AddAssignmentAdapters extends RecyclerView.Adapter<AddAssignmentAdapters.MyViewHolder> {
    private List<ProfileCreateAssignment> profileCreateAssignmentList;
    private ItemClickListenerExtraParam itemClickListener;
    private Context context;

    public AddAssignmentAdapters(Context context, List<ProfileCreateAssignment> profileCreateAssignmentList, ItemClickListenerExtraParam itemClickListener) {
        this.context = context;
        this.profileCreateAssignmentList = profileCreateAssignmentList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_assignments_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ProfileCreateAssignment profileCreateAssignment = profileCreateAssignmentList.get(position);
        if(profileCreateAssignment!=null)
        {
            String imageURL = (new StringBuilder().append(Constant.ImageURL).append(profileCreateAssignment.getPath()).append("/").append(profileCreateAssignment.getImage())).toString();
            Glide.with(context).load(imageURL).placeholder(R.drawable.iv_assignment)
                    .error(R.drawable.iv_assignment)
                    .into(holder.image);
            holder.textViewName.setText(profileCreateAssignment.getName());
            holder.textViewText.setText(profileCreateAssignment.getText());
        }
        holder.fabCancelItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.itemClick(position,"delete");
            }
        });
        holder.fabEditItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.itemClick(position,"update");
            }
        });
    }

    @Override
    public int getItemCount() {
        return profileCreateAssignmentList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        TextView textViewName, textViewText;
        private ImageView image;
        FloatingActionButton fabCancelItem,fabEditItem;

        MyViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.cardView);
            textViewName = view.findViewById(R.id.textViewName);
            textViewText = view.findViewById(R.id.textViewText);
            image = view.findViewById(R.id.image);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            fabEditItem = view.findViewById(R.id.fabEditItem);
        }
    }
}