package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.twc.tiecoon.R;
import com.twc.tiecoon.models.tagModel;
import com.twc.tiecoon.network.response.TagUserList;
import com.twc.tiecoon.utils.ItemClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SelectedTagUserAdapter extends RecyclerView.Adapter<SelectedTagUserAdapter.ViewHolder> {
    private Context context;
    private List<tagModel> list;
    private ItemClickListener itemClickListener;
    public SelectedTagUserAdapter(Context context, List<tagModel> list, ItemClickListener itemClickListener) {
        this.context = context;
        this.list = list;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public SelectedTagUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_talent, parent, false);
        return new SelectedTagUserAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(SelectedTagUserAdapter.ViewHolder holder, final int position) {
        holder.tv_talent.setText(list.get(position).getName());

        holder.fabCancelItem.setOnClickListener(v -> itemClickListener.itemClick(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_talent;
        FloatingActionButton fabCancelItem;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_talent = itemView.findViewById(R.id.tv_talent);
            fabCancelItem = itemView.findViewById(R.id.fabCancelItem);

        }
    }
}

