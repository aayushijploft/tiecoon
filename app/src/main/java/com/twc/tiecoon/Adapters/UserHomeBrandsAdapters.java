package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.HomeUserDetailBrand;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UserHomeBrandsAdapters extends RecyclerView.Adapter<UserHomeBrandsAdapters.MyViewHolder> {
    private List<HomeUserDetailBrand> homeUserDetailBrandList;
    private Context context;

    public UserHomeBrandsAdapters(Context context, List<HomeUserDetailBrand> talentDataList) {
        this.context = context;
        this.homeUserDetailBrandList = talentDataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_brands_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailBrand homeUserDetailBrand = homeUserDetailBrandList.get(position);
        if (homeUserDetailBrand != null) {
            if (homeUserDetailBrand.getName() != null) {
                holder.tvCategory.setText(homeUserDetailBrand.getName() );
            } else {
                holder.tvCategory.setText("");
            }
            if (homeUserDetailBrand.getLink1() != null) {
                holder.tvSubCategory1.setText(homeUserDetailBrand.getLink1());
                holder.tvSubCategory2.setVisibility(View.GONE);
            } else {
                holder.tvSubCategory1.setVisibility(View.GONE);
                holder.tvSubCategory2.setVisibility(View.GONE);
            }
            holder.textViewDescription.setText(homeUserDetailBrand.getDescription());
            String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailBrand.getPath()).append("/").append(homeUserDetailBrand.getImage())).toString();
            Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                    .error(R.drawable.dummy_place)
                    .into(holder.image);

        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailBrandList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView tvCategory, textViewDescription, textViewPrice, tvSubCategory1, tvSubCategory2;

        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            tvCategory = view.findViewById(R.id.tvCategory);
            textViewDescription = view.findViewById(R.id.textViewDescription);
            textViewPrice = view.findViewById(R.id.textViewPrice);
            tvSubCategory1 = view.findViewById(R.id.tvSubCategory1);
            tvSubCategory2 = view.findViewById(R.id.tvSubCategory2);

        }
    }
}