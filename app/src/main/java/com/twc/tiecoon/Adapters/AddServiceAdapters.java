package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.AddProductSubCategoryData;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AddServiceAdapters extends RecyclerView.Adapter<AddServiceAdapters.MyViewHolder> {
    private final List<HomeUserDetailsService> talentDataList;
    private final ItemClickListenerExtraThreeParam itemClickListener;
    private final Context context;

    public AddServiceAdapters(Context context, List<HomeUserDetailsService> talentDataList, ItemClickListenerExtraThreeParam itemClickListener) {
        this.context = context;
        this.talentDataList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_product_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailsService uploadProductData = talentDataList.get(position);
        if (uploadProductData != null) {
            AddProductSubCategoryData addProductSubCategoryData = uploadProductData.getSubCategory();
            if (addProductSubCategoryData != null) {
                if (addProductSubCategoryData.getTitle() != null)
                    holder.tvCategory.setText(addProductSubCategoryData.getTitle());
            }
            if (uploadProductData.getDescription() != null) {
                holder.textViewDescription.setText(uploadProductData.getDescription());
            }
//            if (uploadProductData.getPricecode() != null && uploadProductData.getPrice() != null) {
//                holder.textViewPrice.setText(new StringBuilder().append("Price: ").append(uploadProductData.getPricecode()).append(" ").append(uploadProductData.getPrice()));
//            }
            if (uploadProductData.getPrice() != null  && uploadProductData.getPricecode()!=null) {
                String procecode;
                if(uploadProductData.getPricecode().equals("USD")){
                    procecode = "$";
                }
                else procecode = "₹";
                holder.textViewPrice.setText(new StringBuilder().append("").append(procecode).append(" ")
                        .append(uploadProductData.getPrice()));
            }


            if(uploadProductData.getUpload_image().get(0).getCreateimage() != null){
                switch (uploadProductData.getUpload_image().get(0).getCreateimage()) {
                    case "image":
                        holder.image.setVisibility(View.VISIBLE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
//                        String imageURL = Constant.ImageURL + homeUserDetailsUploadTalent.getPath() + "/" + homeUserDetailsUploadTalent.getCreateimage();
                        Glide.with(context).load(uploadProductData.getUpload_image().get(0).getImage()).error(R.drawable.dummy_place)
                                 .error(R.drawable.dummy_place)
                                .into(holder.image);
                        break;
                    case "audio":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.VISIBLE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        break;
                    case "video":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.VISIBLE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        if (uploadProductData.getUpload_image().get(0).getVideo_thumbnail() != null) {
//                            String imageURL1 = Constant.ImageURL + "/" + homeUserDetailsUploadTalent.getVideothumbnail();
                            Glide.with(context).load(uploadProductData.getUpload_image().get(0).getVideo_thumbnail()).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(holder.ivThumbnail);
                        }
                        break;
                    case "text":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.VISIBLE);
//                        Log.e("bgcolor",uploadProductData.getColor());
                        if (uploadProductData.getColor() != null && !(uploadProductData.getColor().isEmpty())) {
                            try {
                                holder.relativeWaterMark.setBackgroundColor(Color.parseColor(uploadProductData.getColor()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (uploadProductData.getUpload_image().get(0).getImage() != null)
                            holder.tvWatermark.setText(Html.fromHtml(uploadProductData.getUpload_image().get(0).getImage()) );
                        break;
                }
            }

            if(!(uploadProductData.getUserId()== ApplicationClass.appPreferences.getUserId()))
            {
                holder.fabCancelItem.hide();
                holder.fabEditItem.hide();

            }
        }
        holder.cardView.setOnClickListener(view -> itemClickListener.itemClick(position,"","view"));
//        holder.cardView.setOnClickListener(view -> itemClickListener.itemClick(position,"",""));
        holder.fabCancelItem.setOnClickListener(view -> itemClickListener.itemClick(position,"","delete"));
        holder.fabEditItem.setOnClickListener(view -> itemClickListener.itemClick(position,"","update"));
    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeWaterMark;
        FloatingActionButton fabEditItem,fabCancelItem;
        ImageView image,ivThumbnail;
        CardView cardView;
        TextView tvWatermark,tvCategory, textViewDescription, textViewPrice, tvSubCategory1, tvSubCategory2,tvTextArea;
        FrameLayout layoutAudio,layoutVideo;
        MyViewHolder(View view) {
            super(view);
            fabEditItem = view.findViewById(R.id.fabEditItem);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            image = view.findViewById(R.id.image);
            cardView = view.findViewById(R.id.cardView);
            relativeWaterMark = view.findViewById(R.id.relativeWaterMark);
            tvWatermark = view.findViewById(R.id.tvWatermark);
            tvCategory = view.findViewById(R.id.tvCategory);
            textViewDescription = view.findViewById(R.id.textViewDescription);
            textViewPrice = view.findViewById(R.id.textViewPrice);
            tvSubCategory1 = view.findViewById(R.id.tvSubCategory1);
            tvSubCategory2 = view.findViewById(R.id.tvSubCategory2);
            tvTextArea = view.findViewById(R.id.tvTextArea);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);
            layoutAudio = view.findViewById(R.id.layoutAudio);
            layoutVideo = view.findViewById(R.id.layoutVideo);
        }
    }
}