package com.twc.tiecoon.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.twc.tiecoon.Activities.ViewAssignment;
import com.twc.tiecoon.CustomMedia.JZMediaExo;
import com.twc.tiecoon.Fragments.HomeProductServiceFragment;
import com.twc.tiecoon.Fragments.HomeThirdProfileFragment;
import com.twc.tiecoon.Fragments.HomeUserTalentFragment;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.HomeUserDetailData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.HomeUserPreferedLanguage;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import cn.jzvd.JzvdStd;
import de.hdodenhof.circleimageview.CircleImageView;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {
    private final List<HomeUserDetailData> homeUserDetailDataList;
    public static final int TYPE_DASHBOARD = 1;
    public static final int TYPE_ADVERTISEMENT = 2;
    Context context;
    String type;
    FragmentManager fragmentManager;
    private final int[] tabIcons = {
            R.drawable.ic_big_star,
            R.drawable.ic_hand_pick,
            R.drawable.ic_user
    };
    private LayoutInflater inflater;
    private final ItemClickListenerExtraParam itemClickListener;
    private SparseIntArray containerLayoutRes = new SparseIntArray() {
        {
            put(TYPE_DASHBOARD, R.layout.list_item_message_right);
            put(TYPE_ADVERTISEMENT, R.layout.list_item_message_left);

        }
    };

    public DashboardAdapter(Context context, FragmentManager fragmentManager,String type, List<HomeUserDetailData> homeUserDetailDataList,
                            ItemClickListenerExtraParam itemClickListener) {
        this.context = context;
        this.type = type;
        this.homeUserDetailDataList = homeUserDetailDataList;
        this.fragmentManager = fragmentManager;
        this.itemClickListener = itemClickListener;
        this.inflater = LayoutInflater.from(context);
    }

    @NotNull
    @Override
    public DashboardAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull DashboardAdapter.MyViewHolder holder, int position) {
        HomeUserDetailData homeUserDetailData = homeUserDetailDataList.get(position);
        if(homeUserDetailData.getData_type().equals("ads")){
            holder.layoutDashboard.setVisibility(View.GONE);
            holder.layoutAdvertisement.setVisibility(View.VISIBLE);
            if(homeUserDetailData.getAds_type().equals("video")){
                holder.ivAdvertisement.setVisibility(View.GONE);
                holder.layoutVideo.setVisibility(View.VISIBLE);
            }
            else {
                holder.ivAdvertisement.setVisibility(View.VISIBLE);
                holder.layoutVideo.setVisibility(View.GONE);
            }
            holder.ivThumbnail.setOnClickListener(view -> {
                holder.ivThumbnail.setVisibility(View.GONE);
                holder.ivPlayButton.setVisibility(View.GONE);
                holder.jz_video.startVideo();
            });

            if(homeUserDetailData.getLike() == 0){
                holder.AdsLike.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_like));
            }
            else if(homeUserDetailData.getLike() == 1){
                holder.AdsLike.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_like_solid));
            }

            holder.jz_video.setUp(homeUserDetailData.getImage(), "", JzvdStd.SCREEN_NORMAL, JZMediaExo.class);
            Glide.with(context).load(homeUserDetailData.getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.ivAdvertisement);
            Glide.with(context).load(homeUserDetailData.getImage()).placeholder(R.drawable.ic_dummy_user)
                    .error(R.drawable.ic_dummy_user)
                    .into(holder.ivThumbnail);
            holder.tvAdTitle.setText(homeUserDetailData.getTitle());
            holder.tvAdDescription.setText(homeUserDetailData.getMassage());
            holder.ivAdvertisement.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(homeUserDetailData.getLink()));
                context.startActivity(browserIntent);
            });
            holder.layoutVideo.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(homeUserDetailData.getLink()));
                context.startActivity(browserIntent);
            });
            holder.layoutVisitAds.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(homeUserDetailData.getLink()));
                context.startActivity(browserIntent);
            });
            holder.AdsLike.setOnClickListener(view -> itemClickListener.itemClick(position, "AdsSave"));
        }
        else {
            holder.layoutDashboard.setVisibility(View.VISIBLE);
            holder.layoutAdvertisement.setVisibility(View.GONE);
            if (homeUserDetailData.getProfile() != null) {
                if (homeUserDetailData.getPath() != null) {
                    String imageURL = Constant.ImageURL + homeUserDetailData.getPath() + "/" + homeUserDetailData.getProfile();
                    Glide.with(context).load(imageURL).placeholder(R.drawable.userddummyset)
                            .error(R.drawable.userddummyset)
                            .into(holder.ivProfile);
                } else {
                    Glide.with(context).load(homeUserDetailData.getProfile()).placeholder(R.drawable.userddummyset)
                            .error(R.drawable.userddummyset)
                            .into(holder.ivProfile);
                }
            } else {
                Glide.with(context).load(R.drawable.userddummyset).placeholder(R.drawable.userddummyset)
                        .error(R.drawable.userddummyset)
                        .into(holder.ivProfile);
            }

            if (homeUserDetailData.getHomeUserDetailsUserDetils() != null) {
                if (homeUserDetailData.getHomeUserDetailsUserDetils().getName() != null) {
                    String name = homeUserDetailData.getHomeUserDetailsUserDetils().getName().substring(0, 1).toUpperCase() + homeUserDetailData.getHomeUserDetailsUserDetils().getName().substring(1);
                    holder.tvName.setText(name);
                }
                String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());
                if (homeUserDetailData.getHomeUserDetailsUserDetils().getLocation() != null) {
                    String location = homeUserDetailData.getHomeUserDetailsUserDetils().getLocation().substring(0, 1).toUpperCase() + homeUserDetailData.getHomeUserDetailsUserDetils().getLocation().substring(1);
                    holder.tvLocation.setText(location+", IST "+currentTime);
                }
                if (homeUserDetailData.getHomeUserDetailsUserDetils().getWorks() != null) {
//                    126234869
                    String location = homeUserDetailData.getHomeUserDetailsUserDetils().getLocation().substring(0, 1).toUpperCase() + homeUserDetailData.getHomeUserDetailsUserDetils().getLocation().substring(1);
                    holder.tvWorks.setText(homeUserDetailData.getHomeUserDetailsUserDetils().getWorks());
                } else holder.tvWorks.setText("Part Time");
                if (homeUserDetailData.getHomeUserDetailsUserDetils().getStatus() != null) {
                    String des = homeUserDetailData.getHomeUserDetailsUserDetils().getStatus().substring(0, 1).toUpperCase() + homeUserDetailData.getHomeUserDetailsUserDetils().getStatus().substring(1);
                    holder.tvProfileStatus.setText(des);
                }
            }
            List<HomeUserPreferedLanguage> homeUserPreferedLanguageList = homeUserDetailData.getPreferredLanguage();
            if (homeUserPreferedLanguageList != null && !(homeUserPreferedLanguageList.isEmpty())) {
                StringBuilder stringBuilder = new StringBuilder();
                for (HomeUserPreferedLanguage homeUserPreferedLanguage : homeUserPreferedLanguageList) {
                    if (homeUserPreferedLanguage != null && homeUserPreferedLanguage.getLanguage() != null) {
                        if (homeUserPreferedLanguage.getLanguage().getName() != null) {
                            if (stringBuilder.length() == 0) {
                                stringBuilder.append(homeUserPreferedLanguage.getLanguage().getName());
                            } else {
                                stringBuilder.append("/").append(homeUserPreferedLanguage.getLanguage().getName());
                            }
                        }
                    }
                }
                holder.tvLang.setText(stringBuilder.toString());

            } else holder.tvLang.setVisibility(View.GONE);

            switch (homeUserDetailData.getUser_status()) {
                case "Online":
                    holder.ivStatusDot.setColorFilter(ContextCompat.getColor(context, R.color.green));
                    break;
                case "Away":
                    holder.ivStatusDot.setColorFilter(ContextCompat.getColor(context, R.color.yellow));
                    break;
                case "Inactive":
                    holder.ivStatusDot.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.inactive_bg));
                    break;
            }
            if (homeUserDetailData.isIs_like()) {
                holder.imgLike.setImageResource(R.drawable.ic_like_solid);
            } else {
                holder.imgLike.setImageResource(R.drawable.ic_like);
            }

            if (homeUserDetailData.getHomeUserDetailCreateAssignments() != null && !(homeUserDetailData.getHomeUserDetailCreateAssignments().isEmpty())) {
                List<ProfileCreateAssignment> homeUserDetailCreateAssignmentList = new ArrayList<>(homeUserDetailData.getHomeUserDetailCreateAssignments());
                MyprofileOngoingAssignmentAdapters ongoingAssignmentAdapter = new MyprofileOngoingAssignmentAdapters(context, homeUserDetailCreateAssignmentList, new ItemClickListenerExtraParam() {
                    @Override
                    public void itemClick(int pos, String type) {
                        if (type.equals("view")) {
                            Intent i = new Intent(context, ViewAssignment.class);
                            i.putExtra("project_id", homeUserDetailCreateAssignmentList.get(pos).getId()+"");
                            i.putExtra("qbID", homeUserDetailData.getQuickbloxuser_id());
                            i.putExtra("name", homeUserDetailData.getHomeUserDetailsUserDetils().getName());
                            context.startActivity(i);
                        }
                    }
                });
                holder.recyclerView_ongoing_assignments.setAdapter(ongoingAssignmentAdapter);
            }

            //TalentList
            List fragmentList = new ArrayList<>();
            List titleList = new ArrayList<>();

            List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentsNew = homeUserDetailData.getHomeUserDetailsUploadTalent();
            List<HomeUserDetailsService> homeUserDetailsServices = homeUserDetailData.getHomeUserDetailsServices();
            List<HomeUserDetailsProduct> homeUserDetailsProducts = homeUserDetailData.getHomeUserDetailsProducts();
            Log.e("talentlist", homeUserDetailsUploadTalentsNew.toString());
            Log.e("servicelist", homeUserDetailsServices.toString());
            Log.e("productlist", homeUserDetailsProducts.toString());

            if (homeUserDetailsUploadTalentsNew != null && !(homeUserDetailsUploadTalentsNew.isEmpty())) {
                HomeUserTalentFragment homeUserTalentFragment = new HomeUserTalentFragment();
                Bundle bundleProduct = new Bundle();
                bundleProduct.putInt("pos", position);
                bundleProduct.putString("qbID", homeUserDetailData.getQuickbloxuser_id());
                bundleProduct.putParcelableArrayList("talent", (ArrayList<? extends Parcelable>) homeUserDetailsUploadTalentsNew);
                homeUserTalentFragment.setArguments(bundleProduct);
                fragmentList.add(homeUserTalentFragment);
                titleList.add("Talents");
            } else {
                HomeUserTalentFragment homeUserTalentFragment = new HomeUserTalentFragment();
                fragmentList.add(homeUserTalentFragment);
                titleList.add("Talents");
            }
            if (homeUserDetailsProducts != null && !(homeUserDetailsProducts.isEmpty())) {
                HomeProductServiceFragment homeUserTalentFragment = new HomeProductServiceFragment();
                Bundle bundleProduct = new Bundle();
                bundleProduct.putInt("pos", position);
                bundleProduct.putString("qbID", homeUserDetailData.getQuickbloxuser_id());
                bundleProduct.putParcelableArrayList("product", (ArrayList<? extends Parcelable>) homeUserDetailsProducts);
                homeUserTalentFragment.setArguments(bundleProduct);
                fragmentList.add(homeUserTalentFragment);
                titleList.add("Products");
            } else {
                HomeProductServiceFragment homeUserTalentFragment = new HomeProductServiceFragment();
                fragmentList.add(homeUserTalentFragment);
                titleList.add("Products");
            }
            if (homeUserDetailsServices != null && !(homeUserDetailsServices.isEmpty())) {
                HomeThirdProfileFragment homeUserTalentFragment = new HomeThirdProfileFragment();
                Bundle bundleProduct = new Bundle();
                bundleProduct.putInt("pos", position);
                bundleProduct.putString("qbID", homeUserDetailData.getQuickbloxuser_id());
                bundleProduct.putParcelableArrayList("service", (ArrayList<? extends Parcelable>) homeUserDetailsServices);
                homeUserTalentFragment.setArguments(bundleProduct);
                fragmentList.add(homeUserTalentFragment);
                titleList.add("Services");
            } else {
                HomeThirdProfileFragment homeUserTalentFragment = new HomeThirdProfileFragment();
                fragmentList.add(homeUserTalentFragment);
                titleList.add("Services");
            }

            final TodaysDealViewPagerAdapter adapter1 = new TodaysDealViewPagerAdapter(context, fragmentManager, fragmentList, titleList);
            holder.viewPager.setAdapter(adapter1);
            holder.viewPager.setId(position+1);
            holder.tabLayout.setupWithViewPager(holder.viewPager);
            setupTabIcons(holder);
            switch (type) {
                case "1":
                    Objects.requireNonNull(holder.tabLayout.getTabAt(0)).select();
                    break;
                case "2":
                    Objects.requireNonNull(holder.tabLayout.getTabAt(1)).select();
                    break;
                case "3":
                    Objects.requireNonNull(holder.tabLayout.getTabAt(2)).select();
                    break;
            }

            holder.linearOtherUserProfile.setOnClickListener(view -> itemClickListener.itemClick(position, "click"));
            holder.linearLike.setOnClickListener(view -> itemClickListener.itemClick(position, "like"));
            holder.linearConnect.setOnClickListener(view -> itemClickListener.itemClick(position, "chat"));
            holder.linearShare.setOnClickListener(view -> itemClickListener.itemClick(position, "share"));
            holder.linearCollaborate.setOnClickListener(view -> itemClickListener.itemClick(position, "collaborate"));
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailDataList.size();
    }

    private void setupTabIcons(DashboardAdapter.MyViewHolder holder) {
        Objects.requireNonNull(holder.tabLayout.getTabAt(0)).setIcon(tabIcons[0]);
        Objects.requireNonNull(holder.tabLayout.getTabAt(1)).setIcon(tabIcons[1]);
        Objects.requireNonNull(holder.tabLayout.getTabAt(2)).setIcon(tabIcons[2]);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        CircleImageView ivProfile;
        TextView tvName, tvLocation, tvLang, tvWorks, tvProfileStatus,tvAdTitle,tvAdDescription;
        ImageView ivStatusDot, imgLike, imgshare,ivAdvertisement,ivThumbnail,ivPlayButton,AdsLike;
        JzvdStd jz_video;
        TabLayout tabLayout;
        ViewPager viewPager;
        RecyclerView recyclerView_ongoing_assignments;
        LinearLayout linearConnect, linearCollaborate, linearLike,
                linearShare, linearOtherUserProfile,layoutDashboard,layoutAdvertisement,layoutVisitAds;
        FrameLayout layoutVideo;

        MyViewHolder(View view) {
            super(view);
            ivProfile = view.findViewById(R.id.ivProfile);
            tvName = view.findViewById(R.id.tvName);
            tvLocation = view.findViewById(R.id.tvLocation);
            tvLang = view.findViewById(R.id.tvLang);
            tvWorks = view.findViewById(R.id.tvWorks);
            tvProfileStatus = view.findViewById(R.id.tvProfileStatus);
            ivPlayButton = view.findViewById(R.id.ivPlayButton);
            tvAdTitle = view.findViewById(R.id.tvAdTitle);
            jz_video = view.findViewById(R.id.jz_video);
            tvAdDescription = view.findViewById(R.id.tvAdDescription);
            ivStatusDot = view.findViewById(R.id.ivStatusDot);
            imgLike = view.findViewById(R.id.imgLike);
            imgshare = view.findViewById(R.id.imgshare);
            AdsLike = view.findViewById(R.id.AdsLike);
            layoutVisitAds = view.findViewById(R.id.layoutVisitAds);
            layoutVideo = view.findViewById(R.id.layoutVideo);
            ivAdvertisement = view.findViewById(R.id.ivAdvertisement);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);
            tabLayout = view.findViewById(R.id.tabLayout);
            viewPager = view.findViewById(R.id.viewPager);
            recyclerView_ongoing_assignments = view.findViewById(R.id.recyclerView_ongoing_assignments);
            linearConnect = view.findViewById(R.id.linearConnect);
            linearCollaborate = view.findViewById(R.id.linearCollaborate);
            linearLike = view.findViewById(R.id.linearLike);
            linearShare = view.findViewById(R.id.linearShare);
            linearOtherUserProfile = view.findViewById(R.id.linearOtherUserProfile);
            layoutDashboard = view.findViewById(R.id.layoutDashboard);
            layoutAdvertisement = view.findViewById(R.id.layoutAdvertisement);
        }
    }

    public static class TodaysDealViewPagerAdapter extends FragmentStatePagerAdapter {
        private Context myContext;
        private List<Fragment> fragmentList;
        private List<String> titleList;

        public TodaysDealViewPagerAdapter(Context context, FragmentManager fm, List<Fragment> fragmentList, List<String> titleList) {
            super(fm);
            myContext = context;
            this.fragmentList = fragmentList;
            this.titleList = titleList;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }

        // this is for fragment tabs
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        // this counts total number of tabs
        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }
}
