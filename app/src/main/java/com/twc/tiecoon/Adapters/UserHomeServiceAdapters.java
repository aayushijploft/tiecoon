package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

public class UserHomeServiceAdapters extends RecyclerView.Adapter<UserHomeServiceAdapters.MyViewHolder> {
    private List<HomeUserDetailsService> homeUserDetailsServiceList;
    private Context context;
    private ItemClickListener itemClickListener;
    public UserHomeServiceAdapters(Context context, List<HomeUserDetailsService> talentDataList,
                                          ItemClickListener itemClickListener) {
        this.context = context;
        this.homeUserDetailsServiceList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public UserHomeServiceAdapters.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_talents_grid_items, parent, false);
        return new UserHomeServiceAdapters.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UserHomeServiceAdapters.MyViewHolder holder, int position) {
        HomeUserDetailsService homeUserDetailsService = homeUserDetailsServiceList.get(position);
        if (homeUserDetailsService != null) {
            if(homeUserDetailsService.getUpload_image()!=null){
                if(homeUserDetailsService.getUpload_image().get(0).getCreateimage() != null){
                    switch (homeUserDetailsService.getUpload_image().get(0).getCreateimage()) {
                        case "image":
                            holder.image.setVisibility(View.VISIBLE);
                            holder.layoutAudio.setVisibility(View.GONE);
                            holder.layoutVideo.setVisibility(View.GONE);
                            holder.relativeWaterMark.setVisibility(View.GONE);
                            String imageURL = homeUserDetailsService.getCreateimage();
                            Glide.with(context).load(homeUserDetailsService.getUpload_image().get(0).getImage())
                                    .error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(holder.image);
                            break;
                        case "video":
                            holder.image.setVisibility(View.GONE);
                            holder.layoutAudio.setVisibility(View.GONE);
                            holder.layoutVideo.setVisibility(View.VISIBLE);
                            holder.relativeWaterMark.setVisibility(View.GONE);
                            if (homeUserDetailsService.getUpload_image().get(0).getVideo_thumbnail() != null) {
                                String imageURL1 = homeUserDetailsService.getUpload_image().get(0).getVideo_thumbnail();
                                Log.e("thumbnailvdo",imageURL1);
                                Glide.with(context).load(imageURL1).error(R.drawable.dummy_place)
                                        .error(R.drawable.dummy_place)
                                        .into(holder.ivThumbnail);
                            }
                            break;
                        case "audio":
                            holder.image.setVisibility(View.GONE);
                            holder.layoutAudio.setVisibility(View.VISIBLE);
                            holder.layoutVideo.setVisibility(View.GONE);
                            break;
                        case "text":
                            holder.image.setVisibility(View.GONE);
                            holder.layoutAudio.setVisibility(View.GONE);
                            holder.layoutVideo.setVisibility(View.GONE);
                            holder.relativeWaterMark.setVisibility(View.VISIBLE);
//                            Log.e("bgcolor",homeUserDetailsService.getColor());
                            if (homeUserDetailsService.getColor() != null && !(homeUserDetailsService.getColor().isEmpty())) {
                                try {
                                    holder.relativeWaterMark.setBackgroundColor(Color.parseColor(homeUserDetailsService.getColor()));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (homeUserDetailsService.getUpload_image().get(0).getImage() != null)
                                holder.tvWatermark.setText( Html.fromHtml(homeUserDetailsService.getUpload_image().get(0).getImage()) );
                            break;

                    }
                }
            }

            holder.cardView.setOnClickListener(view -> {
                itemClickListener.itemClick(position);
            });
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailsServiceList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        RelativeLayout relativeWaterMark;
        TextView tvWatermark;
        ImageView image,ivThumbnail;
        FrameLayout layoutAudio,layoutVideo;
        MyViewHolder(View view) {
            super(view);
            cardView= view.findViewById(R.id.cardView);
            relativeWaterMark = view.findViewById(R.id.relativeWaterMark);
            tvWatermark = view.findViewById(R.id.tvWatermark);
            image = view.findViewById(R.id.image);
            layoutAudio = view.findViewById(R.id.layoutAudio);
            layoutVideo = view.findViewById(R.id.layoutVideo);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);
        }
    }

}
