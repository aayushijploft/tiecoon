package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.network.response.ReportList;
import com.twc.tiecoon.utils.ItemClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ReportReasonAdapter extends RecyclerView.Adapter<ReportReasonAdapter.ViewHolder> {
    private Context context;
    private List<ReportList.Data> list;
    private ItemClickListener itemClickListener;
    private static RadioButton lastChecked = null;
    private static int lastCheckedPos = 0;
    public ReportReasonAdapter(Context context, List<ReportList.Data> list, ItemClickListener itemClickListener) {
        this.context = context;
        this.list = list;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public ReportReasonAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.reason_layout, parent, false);
        return new ReportReasonAdapter.ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ReportReasonAdapter.ViewHolder holder, final int position) {
       holder.rbReason.setText(list.get(position).getTitle());
        holder.rbReason.setChecked(list.get(position).isIs_Selected());
        holder.rbReason.setTag(new Integer(position));

        //for default check in first item
        if(position == 0 && list.get(0).isIs_Selected() && holder.rbReason.isChecked())
        {
            lastChecked = holder.rbReason;
            lastCheckedPos = 0;
        }

        holder.rbReason.setOnClickListener(v -> {
            RadioButton cb = (RadioButton)v;
            int clickedPos = ((Integer)cb.getTag()).intValue();

            if(cb.isChecked())
            {
                if(lastChecked != null)
                {
                    lastChecked.setChecked(false);
                    list.get(lastCheckedPos).setIs_Selected(false);
                }

                lastChecked = cb;
                lastCheckedPos = clickedPos;
            }
            else
                lastChecked = null;

            list.get(clickedPos).setIs_Selected(cb.isChecked());
            itemClickListener.itemClick(position);
        });
//        holder.rbReason.setOnClickListener(view -> itemClickListener.itemClick(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       RadioButton rbReason;

        public ViewHolder(View itemView) {
            super(itemView);
            rbReason = itemView.findViewById(R.id.rbReason);

        }
    }
}

