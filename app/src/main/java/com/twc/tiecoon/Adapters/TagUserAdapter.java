package com.twc.tiecoon.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.StoryView;
import com.twc.tiecoon.network.response.ViewProduct;
import com.twc.tiecoon.utils.ItemClickListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class TagUserAdapter extends RecyclerView.Adapter<TagUserAdapter.MyViewHolder> {

    private final List<ViewProduct.Tags> tagsList;
    Context context;
    private ItemClickListener itemClickListener;
    public TagUserAdapter(Context context, List<ViewProduct.Tags> tagsList,
                        ItemClickListener itemClickListener) {
        this.context=context;
        this.tagsList=tagsList;
        this.itemClickListener=itemClickListener;

    }

    @NotNull
    @Override
    public TagUserAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_tag, parent, false);
        return new TagUserAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull TagUserAdapter.MyViewHolder holder, int position) {


        holder.tv_Tag.setText(tagsList.get(position).getUser_name());
        holder.tv_Tag.setOnClickListener(v -> itemClickListener.itemClick(position));




    }
    @Override
    public int getItemCount() {
        return tagsList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_Tag;
        MyViewHolder(View view) {
            super(view);
            tv_Tag = view.findViewById(R.id.tv_Tag);
        }

    }
}

