package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.UploadImageListData;

import java.util.ArrayList;


public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Uri> dataList;

    public BannerAdapter(Context context,  ArrayList<Uri> dataList) {
        super();
        this.context = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.add_talents_items, parent, false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

//        UploadImageListData homeUserDetailsUploadTalent = dataList.get(position);
//        BannerModel categoryModel = dataList.get(position);

//        String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailsUploadTalent.getCreateimage())).toString();

        Glide.with(context)
                .load(dataList.get(position))
                .thumbnail(Glide.with(context).load(R.drawable.talent1))
                .error(Glide.with(context)
                        .load(R.drawable.talent1))
                .into(holder.image);


    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {



        ImageView image;


        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
        }
    }
}
