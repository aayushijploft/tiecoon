package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.Activities.ProductServiceBrandsBuyActivity;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.Fragments.HomeProductServiceFragment;
import com.twc.tiecoon.Fragments.HomeThirdProfileFragment;
import com.twc.tiecoon.Fragments.HomeUserTalentFragment;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.network.response.HomeUserPreferedLanguage;
import com.twc.tiecoon.network.response.LikeUserListData;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;
import com.twc.tiecoon.utils.FragmentBrandListListner;
import com.twc.tiecoon.utils.FragmentProductServiceListListner;
import com.twc.tiecoon.utils.FragmentUploadTalentListListner;
import com.twc.tiecoon.utils.ItemClickListener;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class LikeScreenAdapters2 extends RecyclerView.Adapter<LikeScreenAdapters2.MyViewHolder> {
    private List<LikeUserListData> homeUserDetailDataList;
    private ItemClickListenerExtraParam itemClickListener;
    private Context context;
    private FragmentManager fragmentManager;

    Fragment homeUserTalentFragment, productServiceFragment, thirdProfileFragment;
    FragmentUploadTalentListListner fragmentUploadTalentListListner;
    FragmentProductServiceListListner fragmentProductServiceListListner;
    FragmentBrandListListner fragmentBrandListListner;
    //        List<ProfileCreateAssignment> homeUserDetailCreateAssignmentList = new ArrayList<>();
    OngoingAssignmentAdapters ongoingAssignmentAdapter;
    private final int[] tabIcons = {
            R.drawable.ic_star,
            R.drawable.ic_hand_pick,
            R.drawable.ic_user
    };
    public LikeScreenAdapters2(Context context, FragmentManager fragmentManager, List<LikeUserListData> homeUserDetailDataList,
                               ItemClickListenerExtraParam itemClickListener) {
        this.context = context;
        this.homeUserDetailDataList = homeUserDetailDataList;
        this.itemClickListener = itemClickListener;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dashboard_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LikeUserListData homeUserDetailData = homeUserDetailDataList.get(position);
        if (homeUserDetailData != null) {

            holder.layoutDashboard.setVisibility(View.VISIBLE);
//            setupViewPager(holder.viewPager);
//            holder.tabLayout.setupWithViewPager(holder.viewPager);
//            setupTabIcons(holder);

            if (homeUserDetailData.getUserDetils() != null)
            {
                if( homeUserDetailData.getUserDetils().getName() != null) {
                    String name = homeUserDetailData.getUserDetils().getName().substring(0, 1).toUpperCase() + homeUserDetailData.getUserDetils().getName().substring(1);
                    holder.tvName.setText(name);
                }
                if (homeUserDetailData.getUserDetils().getStatus() != null) {
                    String des = homeUserDetailData.getUserDetils().getStatus().substring(0, 1).toUpperCase() + homeUserDetailData.getUserDetils().getStatus().substring(1);
                    holder.tvProfileStatus.setText(des);
                }
                if (homeUserDetailData.getUserDetils().getLocation() != null) {
                    String location = homeUserDetailData.getUserDetils().getLocation().substring(0, 1).toUpperCase() + homeUserDetailData.getUserDetils().getLocation().substring(1);
                    holder.tvLocation.setText(location);
                }

            }
            //new Design
//            List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentsNew = homeUserDetailData.getUploadTalent();
//            Log.e("likeusersize", String.valueOf(homeUserDetailsUploadTalentsNew.size()));
//            if (homeUserDetailsUploadTalentsNew != null && !(homeUserDetailsUploadTalentsNew.isEmpty())) {
//                UserHomeTabTalentAdapters userHomeTabTalentAdapters = new UserHomeTabTalentAdapters(context, homeUserDetailsUploadTalentsNew, new ItemClickListener() {
//                    @Override
//                    public void itemClick(int pos) {
//                        ApplicationClass.uploadImageListDataArrayList.clear();
//                        if (homeUserDetailsUploadTalentsNew.get(pos).getUpload_image() != null && !(homeUserDetailsUploadTalentsNew.get(pos).getUpload_image().isEmpty())) {
//                            ApplicationClass.uploadImageListDataArrayList.addAll(homeUserDetailsUploadTalentsNew.get(pos).getUpload_image());
//                        }
//                        Intent intent = new Intent(context, ProductServiceBrandsBuyActivity.class);
//                        intent.putExtra("talentData", homeUserDetailsUploadTalentsNew.get(pos));
//                        context.startActivity(intent);
//                    }
//                });
//                holder.recyclerViewUploadImageList.setAdapter(userHomeTabTalentAdapters);
//            }
            if(homeUserDetailData.getUser()!=null)
            {
                if (homeUserDetailData.getUser().getProfile() != null) {
                    if (homeUserDetailData.getUser().getPath() != null) {
                        String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailData.getUser().getPath()).append("/").append(homeUserDetailData.getUser().getProfile())).toString();
                        Glide.with(context).load(imageURL).placeholder(R.drawable.ic_dummy_user)
                                .error(R.drawable.ic_dummy_user)
                                .into(holder.ivProfile);
                    } else {
                        Glide.with(context).load(homeUserDetailData.getUser().getProfile()).placeholder(R.drawable.ic_dummy_user)
                                .error(R.drawable.ic_dummy_user)
                                .into(holder.ivProfile);
                    }
                }

                List<HomeUserPreferedLanguage> homeUserPreferedLanguageList = homeUserDetailData.getUser().getPreferredLanguage();
                if (homeUserPreferedLanguageList != null && !(homeUserPreferedLanguageList.isEmpty())) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (HomeUserPreferedLanguage homeUserPreferedLanguage : homeUserPreferedLanguageList) {
                        if (homeUserPreferedLanguage != null && homeUserPreferedLanguage.getLanguage() != null) {
                            if (homeUserPreferedLanguage.getLanguage().getName() != null) {
                                if (stringBuilder.length() == 0) {
                                    stringBuilder.append(homeUserPreferedLanguage.getLanguage().getName());
                                } else {
                                    stringBuilder.append("/").append(homeUserPreferedLanguage.getLanguage().getName());
                                }
                            }
                        }
                    }
                    holder.tvLang.setText(stringBuilder.toString());
                }
            }

            holder.imgLike.setImageResource(R.drawable.ic_like_solid);

            List  fragmentList = new ArrayList<>();
            List  titleList = new ArrayList<>();
            //TalentList
            List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents1 = homeUserDetailData.getUploadTalent();
//            ArrayList<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalents2 = new ArrayList<>();
//            homeUserDetailsUploadTalents2.addAll(homeUserDetailsUploadTalents1);



            if (homeUserDetailsUploadTalents1 != null && !(homeUserDetailsUploadTalents1.isEmpty())) {
                HomeUserTalentFragment homeUserTalentFragment = new HomeUserTalentFragment();
                Bundle bundleProduct = new Bundle();
                bundleProduct.putInt("pos",position);
                bundleProduct.putParcelableArrayList("talent", (ArrayList<? extends Parcelable>) homeUserDetailsUploadTalents1);
                homeUserTalentFragment.setArguments(bundleProduct);
                fragmentList.add(homeUserTalentFragment);
                titleList.add("Talents");
            }else{
                HomeUserTalentFragment homeUserTalentFragment = new HomeUserTalentFragment();
                fragmentList.add(homeUserTalentFragment);
                titleList.add("Talents");
            }
            //ProductList
            List<HomeUserDetailsProduct> homeUserDetailsProductList1 = homeUserDetailData.getProducts();
            if (homeUserDetailsProductList1 != null && !(homeUserDetailsProductList1.isEmpty())) {
                HomeProductServiceFragment  productServiceFragment = new HomeProductServiceFragment();
                Bundle bundleProduct = new Bundle();
                bundleProduct.putParcelableArrayList("product", (ArrayList<? extends Parcelable>) homeUserDetailsProductList1);
                productServiceFragment.setArguments(bundleProduct);
                fragmentList.add(productServiceFragment);
                titleList.add("Products");
            }else {
                HomeProductServiceFragment  productServiceFragment = new HomeProductServiceFragment();
                fragmentList.add(productServiceFragment);
                titleList.add("Products");
            }
            //BrandList
            List<HomeUserDetailsService> homeUserDetailsServices = homeUserDetailData.getServices();
            if (homeUserDetailsServices != null && !(homeUserDetailsServices.isEmpty())) {
                HomeThirdProfileFragment  thirdProfileFragment = new HomeThirdProfileFragment();
                Bundle bundleThirdProfile = new Bundle();
                bundleThirdProfile.putParcelableArrayList("service", (ArrayList<? extends Parcelable>) homeUserDetailsServices);
                thirdProfileFragment.setArguments(bundleThirdProfile);
                fragmentList.add(thirdProfileFragment);
                titleList.add("Services");
            }else{
                HomeThirdProfileFragment  thirdProfileFragment = new HomeThirdProfileFragment();
                fragmentList.add(thirdProfileFragment);
                titleList.add("Services");
            }


            final DashboardAdapter.TodaysDealViewPagerAdapter adapter1 = new DashboardAdapter.TodaysDealViewPagerAdapter(context, fragmentManager, fragmentList, titleList);
            holder.viewPager.setAdapter(adapter1);
            holder.viewPager.setId(position+1);
            holder.tabLayout.setupWithViewPager(holder.viewPager);
            setupTabIcons(holder);

            List<ProfileCreateAssignment> homeUserDetailCreateAssignmentList = new ArrayList<>();
            if (homeUserDetailData.getCreateAssignments() != null && !(homeUserDetailData.getCreateAssignments().isEmpty())) {
                for (ProfileCreateAssignment profileCreateAssignment : homeUserDetailData.getCreateAssignments()) {
                    if (profileCreateAssignment.getPath() != null && profileCreateAssignment.getImage() != null) {
                        homeUserDetailCreateAssignmentList.add(profileCreateAssignment);
                    }
                }
                OngoingAssignmentAdapters ongoingAssignmentAdapter = new OngoingAssignmentAdapters(context, homeUserDetailCreateAssignmentList);
                holder.recyclerView_ongoing_assignments.setAdapter(ongoingAssignmentAdapter);
            }

            holder.linearOtherUserProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "click");
                }
            });
            holder.linearLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "like");
                }
            });
            holder.linearConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "chat");
                }
            });
            holder.linearShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "share");
                }
            });
            holder.linearCollaborate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position, "collaborate");
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView ivProfile;
        TextView tvName, tvLocation, tvLang, tvWorks, tvProfileStatus;
        ImageView ivStatusDot, imgLike, imgshare;
        TabLayout tabLayout;
        ViewPager viewPager;
        RecyclerView recyclerView_ongoing_assignments;
        LinearLayout linearConnect, linearCollaborate, linearLike, linearShare, linearOtherUserProfile,layoutDashboard;
        MyViewHolder(View view) {
            super(view);
            ivProfile = view.findViewById(R.id.ivProfile);
            tvLang = view.findViewById(R.id.tvLang);
            tvLocation = view.findViewById(R.id.tvLocation);
//            tvOngoingAssignnmets = view.findViewById(R.id.tvOngoingAssignnmets);
            ivStatusDot = view.findViewById(R.id.ivStatusDot);
            tvName = view.findViewById(R.id.tvName);
            linearOtherUserProfile = view.findViewById(R.id.linearOtherUserProfile);
            tvWorks = view.findViewById(R.id.tvWorks);
            linearLike = view.findViewById(R.id.linearLike);
            tvProfileStatus = view.findViewById(R.id.tvProfileStatus);
            linearShare = view.findViewById(R.id.linearShare);
            imgshare = view.findViewById(R.id.imgshare);
            imgLike = view.findViewById(R.id.imgLike);

            linearOtherUserProfile = view.findViewById(R.id.linearOtherUserProfile);
            linearConnect = view.findViewById(R.id.linearConnect);
            linearCollaborate = view.findViewById(R.id.linearCollaborate);
            layoutDashboard = view.findViewById(R.id.layoutDashboard);


            //first tablayout
            tabLayout = view.findViewById(R.id.tabLayout);
            viewPager = view.findViewById(R.id.viewPager);

            recyclerView_ongoing_assignments = view.findViewById(R.id.recyclerView_ongoing_assignments);
        }

    }

    private void setupTabIcons(LikeScreenAdapters2.MyViewHolder holder) {
        Objects.requireNonNull(holder.tabLayout.getTabAt(0)).setIcon(tabIcons[0]);
        Objects.requireNonNull(holder.tabLayout.getTabAt(1)).setIcon(tabIcons[1]);
        Objects.requireNonNull(holder.tabLayout.getTabAt(2)).setIcon(tabIcons[2]);
    }    ViewPagerAdapter adapter;

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(fragmentManager);

        adapter.addFragment(homeUserTalentFragment, "Talent");
        adapter.addFragment(productServiceFragment, "Product & Services");
        adapter.addFragment(thirdProfileFragment, "3rd Profile");
        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }


        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public class TodaysDealViewPagerAdapter extends FragmentStatePagerAdapter {

        private Context myContext;
        private List<Fragment> fragmentList;
        private List<String> titleList;

        public TodaysDealViewPagerAdapter(Context context, FragmentManager fm, List<Fragment> fragmentList, List<String> titleList) {
            super(fm);
            myContext = context;
            this.fragmentList = fragmentList;
            this.titleList = titleList;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }

        // this is for fragment tabs
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        // this counts total number of tabs
        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }

}