package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddProductCategoryData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyProfileProductHorizontalAdapters extends RecyclerView.Adapter<MyProfileProductHorizontalAdapters.MyViewHolder> {
    private List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList;
    private Context context;

    public MyProfileProductHorizontalAdapters(Context context, List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList) {
        this.context = context;
        this.homeUserDetailsUploadTalentList = homeUserDetailsUploadTalentList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_profile_talent_product_service_horizontal, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailsProduct homeUserDetailsUploadTalent = homeUserDetailsUploadTalentList.get(position);
        if (homeUserDetailsUploadTalent != null) {
            AddProductCategoryData addProductCategoryData = homeUserDetailsUploadTalent.getCategory();
            if (addProductCategoryData != null) {
                holder.tvSubCategory.setText(addProductCategoryData.getTitle());
            } else {
                holder.tvSubCategory.setText("");
            }
            if (homeUserDetailsUploadTalent.getDescription() != null) {
                holder.textViewDescription.setText(homeUserDetailsUploadTalent.getDescription());
            }
            if (homeUserDetailsUploadTalent.getPrice() != null) {
                holder.textViewPrice.setText(new StringBuilder().append("Price: ").append(homeUserDetailsUploadTalent.getPricecode()).append(" ").append(homeUserDetailsUploadTalent.getPrice()));
            }
            if (homeUserDetailsUploadTalent.getPath() != null && homeUserDetailsUploadTalent.getImage() != null) {
                String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailsUploadTalent.getPath()).append("/").append(homeUserDetailsUploadTalent.getImage())).toString();
                Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                        .error(R.drawable.dummy_place)
                        .into(holder.image);
            }
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailsUploadTalentList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView tvSubCategory, textViewDescription, textViewPrice;

        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            tvSubCategory = view.findViewById(R.id.tvSubCategory);
            textViewDescription = view.findViewById(R.id.textViewDescription);
            textViewPrice = view.findViewById(R.id.textViewPrice);
        }
    }
}