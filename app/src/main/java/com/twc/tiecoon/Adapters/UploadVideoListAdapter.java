package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.Activities.VideoViewActivity;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.UploadVideoListData;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class UploadVideoListAdapter extends RecyclerView.Adapter<UploadVideoListAdapter.MyViewHolder> {
    private final List<UploadVideoListData> talentDataList;
    private final Context context;
    public UploadVideoListAdapter(Context context, List<UploadVideoListData> talentDataList) {
        this.context = context;
        this.talentDataList = talentDataList;
    }

    @NotNull
    @Override
    public UploadVideoListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_photos, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull UploadVideoListAdapter.MyViewHolder holder, int position) {
        UploadVideoListData homeUserDetailsUploadTalent = talentDataList.get(position);
        holder.fabCancelItem.hide();
        if (homeUserDetailsUploadTalent != null) {
           holder.ivPlay.setVisibility(View.VISIBLE);
            Glide.with(context).load(R.drawable.talent1).error(R.drawable.dummy_place)
                    .error(R.drawable.dummy_place)
                    .into(holder.image);
        }

        holder.cardView.setOnClickListener(view -> {
            assert homeUserDetailsUploadTalent != null;
            String imageURL = Constant.ImageURL + homeUserDetailsUploadTalent.getVideo();
            Intent intent = new Intent(context, VideoViewActivity.class);
            intent.putExtra("videourl",imageURL);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        FloatingActionButton fabCancelItem;
        CardView cardView;
        ImageView ivPlay;
        MyViewHolder(View view) {
            super(view);
            fabCancelItem= view.findViewById(R.id.fabCancelItem);
            image = view.findViewById(R.id.image);
            ivPlay = view.findViewById(R.id.ivPlay);
            cardView = view.findViewById(R.id.cardView);
        }
    }
}
