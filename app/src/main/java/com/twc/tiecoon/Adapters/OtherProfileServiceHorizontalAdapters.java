package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddProductCategoryData;
import com.twc.tiecoon.network.response.AddProductSubCategoryData;
import com.twc.tiecoon.network.response.HomeUserDetailsService;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class OtherProfileServiceHorizontalAdapters extends RecyclerView.Adapter<OtherProfileServiceHorizontalAdapters.MyViewHolder> {
    private List<HomeUserDetailsService> homeUserDetailsUploadTalentList;
    private ItemClickListener itemClickListener;
    private Context context;
    String qbid;

    public OtherProfileServiceHorizontalAdapters(Context context, List<HomeUserDetailsService> homeUserDetailsUploadTalentList,String qbid,
                                                 ItemClickListener itemClickListener) {
        this.context = context;
        this.homeUserDetailsUploadTalentList = homeUserDetailsUploadTalentList;
        this.qbid = qbid;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_profile_talent_product_service_horizontal, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailsService homeUserDetailsUploadTalent = homeUserDetailsUploadTalentList.get(position);
        if (homeUserDetailsUploadTalent != null) {
            AddProductSubCategoryData addProductCategoryData = homeUserDetailsUploadTalent.getSubCategory();
            if (addProductCategoryData != null && addProductCategoryData.getTitle()!=null) {
                holder.tvSubCategory.setText(addProductCategoryData.getTitle());
            }
            if (homeUserDetailsUploadTalent.getDescription() != null) {
                holder.textViewDescription.setText(homeUserDetailsUploadTalent.getDescription());
            }
//            if (homeUserDetailsUploadTalent.getPrice() != null  && homeUserDetailsUploadTalent.getPricecode()!=null) {
//                holder.textViewPrice.setText(new StringBuilder().append("Price: ").append(homeUserDetailsUploadTalent.getPricecode()).append(" ").append(homeUserDetailsUploadTalent.getPrice()));
//            }
            if (homeUserDetailsUploadTalent.getPrice() != null  && homeUserDetailsUploadTalent.getPricecode()!=null) {
                String procecode;
                if(homeUserDetailsUploadTalent.getPricecode().equals("USD")){
                    procecode = "$";
                }
                else procecode = "₹";
                holder.textViewPrice.setText(new StringBuilder().append("").append(procecode).append(" ").append(homeUserDetailsUploadTalent.getPrice()));
            }

            if(homeUserDetailsUploadTalent.getUpload_image().get(0).getCreateimage() != null){
                switch (homeUserDetailsUploadTalent.getUpload_image().get(0).getCreateimage()) {
                    case "image":
                        holder.image.setVisibility(View.VISIBLE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
//                        String imageURL = Constant.ImageURL + homeUserDetailsUploadTalent.getPath() + "/" + homeUserDetailsUploadTalent.getCreateimage();
                        Glide.with(context).load(homeUserDetailsUploadTalent.getUpload_image().get(0).getImage()).error(R.drawable.dummy_place)
                                .error(R.drawable.dummy_place)
                                .into(holder.image);
                        break;
                    case "audio":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.VISIBLE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        break;
                    case "video":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.VISIBLE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        if (homeUserDetailsUploadTalent.getUpload_image().get(0).getVideo_thumbnail() != null) {
//                            String imageURL1 = Constant.ImageURL + "/" + homeUserDetailsUploadTalent.getVideothumbnail();
                            Glide.with(context).load(homeUserDetailsUploadTalent.getUpload_image().get(0).getVideo_thumbnail()).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(holder.ivThumbnail);
                        }
                        break;
                    case "text":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.VISIBLE);
                        if (homeUserDetailsUploadTalent.getColor() != null && !(homeUserDetailsUploadTalent.getColor().isEmpty())) {
                            try {
                                holder.relativeWaterMark.setBackgroundColor(Color.parseColor(homeUserDetailsUploadTalent.getColor()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (homeUserDetailsUploadTalent.getUpload_image().get(0).getImage() != null)
                            holder.tvWatermark.setText(homeUserDetailsUploadTalent.getUpload_image().get(0).getImage() );
                        break;
                }
            }

            holder.cardViewBuy.setOnClickListener(view -> itemClickListener.itemClick(position));
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailsUploadTalentList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeWaterMark;
        ImageView image,ivThumbnail;
        TextView tvWatermark,tvSubCategory, textViewDescription, textViewPrice,tvTextArea;
        CardView cardViewBuy;
        FrameLayout layoutAudio,layoutVideo;
        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            relativeWaterMark = view.findViewById(R.id.relativeWaterMark);
            tvWatermark = view.findViewById(R.id.tvWatermark);
            tvSubCategory = view.findViewById(R.id.tvSubCategory);
            textViewDescription = view.findViewById(R.id.textViewDescription);
            textViewPrice = view.findViewById(R.id.textViewPrice);
            cardViewBuy=view.findViewById(R.id.cardViewBuy);
            tvTextArea = view.findViewById(R.id.tvTextArea);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);
            layoutAudio = view.findViewById(R.id.layoutAudio);
            layoutVideo = view.findViewById(R.id.layoutVideo);
        }
    }
}