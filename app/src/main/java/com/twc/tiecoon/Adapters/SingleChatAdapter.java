package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.OnetooneChatModel;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class SingleChatAdapter extends RecyclerView.Adapter<SingleChatAdapter.MyViewHolder> {

    private Context context;
    private List<OnetooneChatModel.Data> dataList;
    private ItemClickListener itemClickListener;

    public SingleChatAdapter(Context context,List<OnetooneChatModel.Data> dataList,ItemClickListener itemClickListener) {
        super();
        this.context = context;
        this.dataList = dataList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public SingleChatAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.single_chat_item, parent, false);

        return new SingleChatAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SingleChatAdapter.MyViewHolder holder, int position) {
        holder.tv_name.setText(dataList.get(position).getName());
        holder.icMenu.setVisibility(View.GONE);
        holder.tv_recent_message.setText(dataList.get(position).getLast_message());
        Glide.with(context)
                .load(dataList.get(position).getProfile_image())
                .thumbnail(Glide.with(context).load(R.drawable.talent1))
                .error(Glide.with(context)
                        .load(R.drawable.talent1))
                .into(holder.image);
        holder.rlChat.setOnClickListener(v -> itemClickListener.itemClick(position));

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {



        ImageView image,icMenu;
        TextView tv_name,tv_recent_message;
        RelativeLayout rlChat;
        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.bell1);
            icMenu = itemView.findViewById(R.id.icMenu);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_recent_message = itemView.findViewById(R.id.tv_recent_message);
            rlChat = itemView.findViewById(R.id.rlChat);
        }
    }
}
