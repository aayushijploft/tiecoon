package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class AddBrandsAdapters extends RecyclerView.Adapter<AddBrandsAdapters.MyViewHolder> {
    private List<AddBrandsData> talentDataList;
    private ItemClickListenerExtraParam itemClickListener;
    private Context context;

    public AddBrandsAdapters(Context context, List<AddBrandsData> talentDataList, ItemClickListenerExtraParam itemClickListener) {
        this.context = context;
        this.talentDataList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_brands_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AddBrandsData addBrandsData = talentDataList.get(position);
        if (addBrandsData != null) {
            if (addBrandsData.getName() != null) {
                holder.tvCategory.setText(addBrandsData.getName());
            }
            if (addBrandsData.getLink1() != null) {
                holder.tvSubCategory1.setText(addBrandsData.getLink1());
            }
            else {
                holder.tvSubCategory1.setVisibility(View.GONE);
            }
            if (addBrandsData.getLink2() != null) {
                holder.tvSubCategory2.setText(addBrandsData.getLink2());
            } else {
                holder.tvSubCategory2.setVisibility(View.GONE);
            }
            if (addBrandsData.getLink3() != null) {
                holder.tvSubCategory3.setText(addBrandsData.getLink3());
            } else {
                holder.tvSubCategory3.setVisibility(View.GONE);
            }
            if(addBrandsData.getLink1()==null && addBrandsData.getLink2()==null && addBrandsData.getLink3()==null)
            {
                holder.viewBrand.setVisibility(View.GONE);
            }
            if (addBrandsData.getPrice() != null) {
                holder.textViewPrice.setText("Price: Rs. "+addBrandsData.getPrice());
            }
            if (addBrandsData.getDescription() != null) {
                holder.textViewDescription.setText(addBrandsData.getDescription());
            }

            String imageURL = (new StringBuilder().append(Constant.ImageURL).append(addBrandsData.getPath()).append("/").append(addBrandsData.getImage())).toString();
            Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                    .error(R.drawable.dummy_place)
                    .into(holder.image);
            if(!(addBrandsData.getUserId()== ApplicationClass.appPreferences.getUserId()))
            {
                holder.fabCancelItem.hide();
                holder.fabEditItem.hide();
                holder.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        itemClickListener.itemClick(position,"view");
                    }
                });
            }
        }
        holder.fabCancelItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.itemClick(position,"delete");
            }
        });
        holder.fabEditItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.itemClick(position,"update");
            }
        });
    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView image;
        View viewBrand;
        TextView tvCategory, textViewDescription, textViewPrice, tvSubCategory1, tvSubCategory2, tvSubCategory3;
        FloatingActionButton fabCancelItem,fabEditItem;
        MyViewHolder(View view) {
            super(view);
            viewBrand = view.findViewById(R.id.viewBrand);
            cardView = view.findViewById(R.id.cardView);
            image = view.findViewById(R.id.image);
            tvCategory = view.findViewById(R.id.tvCategory);
            textViewDescription = view.findViewById(R.id.textViewDescription);
            textViewPrice = view.findViewById(R.id.textViewPrice);
            tvSubCategory1 = view.findViewById(R.id.tvSubCategory1);
            tvSubCategory2 = view.findViewById(R.id.tvSubCategory2);
            tvSubCategory3 = view.findViewById(R.id.tvSubCategory3);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            fabEditItem = view.findViewById(R.id.fabEditItem);
        }
    }
}