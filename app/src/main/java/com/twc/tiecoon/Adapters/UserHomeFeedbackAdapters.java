package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.HomeUserDetailsFeedback;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserHomeFeedbackAdapters extends RecyclerView.Adapter<UserHomeFeedbackAdapters.ViewHolder> {
    private List<HomeUserDetailsFeedback> homeUserDetailsFeedbackList;
    private Context context;

    public UserHomeFeedbackAdapters(Context context, List<HomeUserDetailsFeedback> homeUserDetailsFeedbackList) {
        this.homeUserDetailsFeedbackList = homeUserDetailsFeedbackList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.feedback_item_list, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        HomeUserDetailsFeedback homeUserDetailsFeedback=   homeUserDetailsFeedbackList.get(position);
        if(homeUserDetailsFeedback!=null)
        {
            if (homeUserDetailsFeedback.getUser() != null) {
                String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailsFeedback.getUser().getPath()).append("/").append(homeUserDetailsFeedback.getUser().getProfile())).toString();
                Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                        .error(R.drawable.dummy_place)
                        .into(holder.imgUserimage);
                if (homeUserDetailsFeedback.getUserDetils()!=null &&  homeUserDetailsFeedback.getUserDetils().getName() != null) {
                    holder.tvName.setText(homeUserDetailsFeedback.getUserDetils().getName());
                }
            }
            if (homeUserDetailsFeedback.getMassage() != null)
                holder.simpleRatingBar.setNumStars(homeUserDetailsFeedback.getRating());
            if (homeUserDetailsFeedback.getMassage() != null)
                holder.textViewDescription.setText(homeUserDetailsFeedback.getMassage());
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailsFeedbackList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView imgUserimage;
        private TextView tvName,textViewDescription;
        private RatingBar simpleRatingBar;

        public ViewHolder(View itemView) {
            super(itemView);
            imgUserimage=itemView.findViewById(R.id.imgUserimage);
            tvName = itemView.findViewById(R.id.tvName);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            simpleRatingBar = itemView.findViewById(R.id.simpleRatingBar);
        }
    }
}
