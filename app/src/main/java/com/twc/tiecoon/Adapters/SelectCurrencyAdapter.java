package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.CurrencyData;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class SelectCurrencyAdapter extends RecyclerView.Adapter<SelectCurrencyAdapter.ViewHolder> {
    private final List<CurrencyData> list;
    private final ItemClickListener itemClickListener;
    Context context;

    public SelectCurrencyAdapter(Context context, List<CurrencyData> list, ItemClickListener itemClickListener) {
        this.list = list;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.curreny_item_list, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tv_currency.setText(new StringBuilder().append(list.get(position).getCountry()).append("(").append(list.get(position).getSymbol()).append(")"));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_currency;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_currency = itemView.findViewById(R.id.tv_currency);
            itemView.setOnClickListener(view -> itemClickListener.itemClick(getAdapterPosition()));
        }
    }
}
