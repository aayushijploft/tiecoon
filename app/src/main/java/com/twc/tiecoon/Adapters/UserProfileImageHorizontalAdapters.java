package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.HomeUserDetailBrand;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class UserProfileImageHorizontalAdapters extends RecyclerView.Adapter<UserProfileImageHorizontalAdapters.MyViewHolder> {
    private List<HomeUserDetailBrand> talentDataList;
    private Context context;

    public UserProfileImageHorizontalAdapters(Context context, List<HomeUserDetailBrand> talentDataList) {
        this.context = context;
        this.talentDataList = talentDataList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_talents_grid_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailBrand homeUserDetailsUploadTalent = talentDataList.get(position);
        if (homeUserDetailsUploadTalent != null) {
            String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailsUploadTalent.getPath()).append("/").append(homeUserDetailsUploadTalent.getImage())).toString();
            Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                    .error(R.drawable.dummy_place)
                    .into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;

        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
        }
    }
}