package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.network.response.ProductCategory;
import com.twc.tiecoon.utils.ItemClickListenerExtraThreeParam;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyProfileProductAdapters extends RecyclerView.Adapter<MyProfileProductAdapters.MyViewHolder> {
    private List<ProductCategory.Data> dataList;
    private ItemClickListenerExtraThreeParam itemClickListener;
    private Context context;

    public MyProfileProductAdapters(Context context, List<ProductCategory.Data> dataList, ItemClickListenerExtraThreeParam itemClickListener) {
        this.context = context;
        this.dataList = dataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.myprofile_talents_product_services_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvTitle.setText(dataList.get(position).getTitle());
        holder.tvUpload.setText("Upload");
        holder.tvUpload.setOnClickListener(v ->  itemClickListener.itemClick(position,String.valueOf(position),"Upload"));
        holder.fabCancelItem.setOnClickListener(v ->  itemClickListener.itemClick(position,String.valueOf(position),"remove"));
        }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
//        private LinearLayout;
        private TextView tvTitle;
        private RecyclerView recyclerView_upload_product;
        TextView tvUpload;
        FloatingActionButton fabCancelItem;

        MyViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tvTitle);
            recyclerView_upload_product = view.findViewById(R.id.recyclerView_upload_product);
            tvUpload = view.findViewById(R.id.tvUpload);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
        }
    }
}