package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddProductCategoryData;
import com.twc.tiecoon.network.response.AddProductSubCategoryData;
import com.twc.tiecoon.network.response.HomeUserDetailsProduct;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class OtherProfileProductHorizontalAdapters extends RecyclerView.Adapter<OtherProfileProductHorizontalAdapters.MyViewHolder> {
    private List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList;
    private Context context;
    private ItemClickListener itemClickListener;

    public OtherProfileProductHorizontalAdapters(Context context, List<HomeUserDetailsProduct> homeUserDetailsUploadTalentList, ItemClickListener itemClickListener) {
        this.context = context;
        this.homeUserDetailsUploadTalentList = homeUserDetailsUploadTalentList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_profile_talent_product_service_horizontal, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailsProduct homeUserDetailsUploadTalent = homeUserDetailsUploadTalentList.get(position);
        if (homeUserDetailsUploadTalent != null) {
            AddProductSubCategoryData addProductCategoryData = homeUserDetailsUploadTalent.getSubCategory();
            if (addProductCategoryData != null && addProductCategoryData.getTitle()!=null) {
                holder.tvSubCategory.setText(addProductCategoryData.getTitle());
            }
            if (homeUserDetailsUploadTalent.getDescription() != null) {
                holder.textViewDescription.setText(homeUserDetailsUploadTalent.getDescription());
            }
            if (homeUserDetailsUploadTalent.getPrice() != null  && homeUserDetailsUploadTalent.getPricecode()!=null) {
                String procecode;
                if(homeUserDetailsUploadTalent.getPricecode().equals("USD")){
                    procecode = "$";
                }
                else procecode = "₹";
                holder.textViewPrice.setText(new StringBuilder().append("").append(procecode).append(" ").append(homeUserDetailsUploadTalent.getPrice()));
            }
            if(homeUserDetailsUploadTalent.getUpload_image().get(0).getCreateimage() != null){
                switch (homeUserDetailsUploadTalent.getUpload_image().get(0).getCreateimage()) {
                    case "image":
                        holder.image.setVisibility(View.VISIBLE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        String imageURL = homeUserDetailsUploadTalent.getCreateimage();
                        Glide.with(context).load(homeUserDetailsUploadTalent.getUpload_image().get(0).getImage()).error(R.drawable.dummy_place)
                                .error(R.drawable.dummy_place)
                                .into(holder.image);
                        break;
                    case "video":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.VISIBLE);
                        holder.relativeWaterMark.setVisibility(View.GONE);
                        if (homeUserDetailsUploadTalent.getUpload_image().get(0).getVideo_thumbnail() != null) {
                            String imageURL1 = homeUserDetailsUploadTalent.getUpload_image().get(0).getImage();
                            Log.e("vdoooo",imageURL1);
                            Glide.with(context).load(imageURL1).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(holder.ivThumbnail);
                        }
                        break;

                }
            }

            holder.cardViewBuy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.itemClick(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailsUploadTalentList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeWaterMark;
        ImageView image,ivThumbnail;
        TextView tvWatermark,tvSubCategory, textViewDescription, textViewPrice;
        CardView cardViewBuy;
        FrameLayout layoutAudio,layoutVideo;
        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            relativeWaterMark = view.findViewById(R.id.relativeWaterMark);
            tvWatermark = view.findViewById(R.id.tvWatermark);
            tvSubCategory = view.findViewById(R.id.tvSubCategory);
            textViewDescription = view.findViewById(R.id.textViewDescription);
            textViewPrice = view.findViewById(R.id.textViewPrice);
            cardViewBuy=view.findViewById(R.id.cardViewBuy);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);
            layoutAudio = view.findViewById(R.id.layoutAudio);
            layoutVideo = view.findViewById(R.id.layoutVideo);
        }
    }
}