package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.UploadImageListData;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class UploadImageListAdapter extends RecyclerView.Adapter<UploadImageListAdapter.MyViewHolder> {
    private final List<UploadImageListData> talentDataList;
    private final Context context;
    public UploadImageListAdapter(Context context, List<UploadImageListData> talentDataList) {
        this.context = context;
        this.talentDataList = talentDataList;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.slidingimages_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        UploadImageListData homeUserDetailsUploadTalent = talentDataList.get(position);
        if (homeUserDetailsUploadTalent != null) {
            String imageURL = Constant.ImageURL  + homeUserDetailsUploadTalent.getPath() + "/" + homeUserDetailsUploadTalent.getCreateimage();
            Log.e("talent_image",imageURL);
            Glide.with(context).load(homeUserDetailsUploadTalent.getWatermark()).error(R.drawable.dummy_place)
                    .error(R.drawable.dummy_place)
                    .into(holder.image);
        }
        assert homeUserDetailsUploadTalent != null;
//        String imageURL = Constant.ImageURL + homeUserDetailsUploadTalent.getCreateimage();

    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
        }
    }

}