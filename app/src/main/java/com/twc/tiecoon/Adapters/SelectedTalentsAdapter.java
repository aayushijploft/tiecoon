package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.ProfileTalent;
import com.twc.tiecoon.utils.ItemClickListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SelectedTalentsAdapter extends RecyclerView.Adapter<SelectedTalentsAdapter.ViewHolder> {

    private List<ProfileTalent> profileTalentList;
    private ItemClickListener itemClickListener;

    public SelectedTalentsAdapter(Context context, List<ProfileTalent> profileTalentList, ItemClickListener itemClickListener) {
        this.profileTalentList = profileTalentList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_talent, parent, false);
        return new ViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.tv_talent.setText(profileTalentList.get(position).getName());
        holder.fabCancelItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.itemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return profileTalentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_talent;
        private FloatingActionButton fabCancelItem;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_talent = itemView.findViewById(R.id.tv_talent);
            fabCancelItem = itemView.findViewById(R.id.fabCancelItem);
        }
    }
}
