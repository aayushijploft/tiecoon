package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.TalentCurrencyUserTalentsData;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SelectProductServiceTalentsAdapter extends RecyclerView.Adapter<SelectProductServiceTalentsAdapter.ViewHolder> {
    private List<TalentCurrencyUserTalentsData> categoryDataList;
    private ItemClickListenerExtraParam itemClickListener;
    private int categoryID;
    private Context context;
    private int mSelectedItem = -1;
    private boolean isCategory;

    public SelectProductServiceTalentsAdapter(Context context, int categoryID, boolean isCategory, List<TalentCurrencyUserTalentsData> categoryDataList, ItemClickListenerExtraParam itemClickListener) {
        this.categoryDataList = categoryDataList;
        this.itemClickListener = itemClickListener;
        this.categoryID = categoryID;
        this.context = context;
        this.isCategory = isCategory;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.category_item_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        TalentCurrencyUserTalentsData talentCurrencyUserTalentsData = categoryDataList.get(position);
        if (talentCurrencyUserTalentsData != null) {
            if (talentCurrencyUserTalentsData.getName() != null) {
                holder.tvTitle.setText(categoryDataList.get(position).getName());
            }
            if (isCategory) {
                holder.linearCategory.setBackground(context.getResources().getDrawable(R.drawable.rounder_border_linear_solid));
                holder.tvTitle.setTextColor(Color.WHITE);
                itemClickListener.itemClick(position, "update");
            } else {
                if (categoryID > 0 && categoryID == talentCurrencyUserTalentsData.getId()) {
                    holder.linearCategory.setBackground(context.getResources().getDrawable(R.drawable.rounder_border_linear_solid));
                    holder.tvTitle.setTextColor(Color.WHITE);
                    itemClickListener.itemClick(position, "update");
                    mSelectedItem = position;
                    categoryID = 0;
                } else {
                    if (mSelectedItem == position) {
                        holder.linearCategory.setBackground(context.getResources().getDrawable(R.drawable.rounder_border_linear_solid));
                        holder.tvTitle.setTextColor(Color.WHITE);
                    } else {
                        holder.linearCategory.setBackground(context.getResources().getDrawable(R.drawable.rounder_border_linear_stroke));
                        holder.tvTitle.setTextColor(context.getResources().getColor(R.color.black));
                    }
                }
                holder.linearCategory.setOnClickListener(view -> {
                    if (mSelectedItem >= 0) {
                        notifyItemChanged(mSelectedItem);
                    }
                    mSelectedItem = position;
                    notifyItemChanged(position);
                    itemClickListener.itemClick(position, "click");
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return categoryDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private LinearLayout linearCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            linearCategory = itemView.findViewById(R.id.linearCategory);
        }
    }
}

