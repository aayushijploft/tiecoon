package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.ProfileCreateAssignment;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class OngoingAssignmentAdapters extends RecyclerView.Adapter<OngoingAssignmentAdapters.MyViewHolder> {
    private List<ProfileCreateAssignment> homeUserDetailCreateAssignmentList;
    private Context context;

    public OngoingAssignmentAdapters(Context context, List<ProfileCreateAssignment> homeUserDetailCreateAssignmentList) {
        this.context = context;
        this.homeUserDetailCreateAssignmentList = homeUserDetailCreateAssignmentList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ongoing_assignment_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ProfileCreateAssignment homeUserDetailCreateAssignment = homeUserDetailCreateAssignmentList.get(position);
        if (homeUserDetailCreateAssignment != null) {
            if (homeUserDetailCreateAssignment.getPath() != null && homeUserDetailCreateAssignment.getImage() != null) {
                String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailCreateAssignment.getPath()).append("/").append(homeUserDetailCreateAssignment.getImage())).toString();
                Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                        .error(R.drawable.dummy_place)
                        .into(holder.image);
            }
            holder.textViewText.setText(homeUserDetailCreateAssignment.getText());
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailCreateAssignmentList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewText;
        private ImageView image;

        MyViewHolder(View view) {
            super(view);
            textViewText = view.findViewById(R.id.tvText);
            image = view.findViewById(R.id.image);
        }
    }
}