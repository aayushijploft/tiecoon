package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddBrandsData;
import com.twc.tiecoon.utils.ItemClickListenerExtraParam;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyProfileBrandsHorizontalAdapters extends RecyclerView.Adapter<MyProfileBrandsHorizontalAdapters.MyViewHolder> {
    private List<AddBrandsData> talentDataList;
    private ItemClickListenerExtraParam itemClickListener;
    private Context context;

    public MyProfileBrandsHorizontalAdapters(Context context, List<AddBrandsData> talentDataList, ItemClickListenerExtraParam itemClickListener) {
        this.context = context;
        this.talentDataList = talentDataList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.upload_product_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AddBrandsData addBrandsData = talentDataList.get(position);
        if (addBrandsData != null) {
            if (addBrandsData.getName() != null) {
                holder.tvCategory.setText(addBrandsData.getName());
            }
            if (addBrandsData.getLink1() != null) {
                holder.tvSubCategory1.setText(addBrandsData.getLink1());
            }
            if (addBrandsData.getLink2() != null) {
                holder.tvSubCategory2.setVisibility(View.VISIBLE);
                holder.tvSubCategory2.setText(addBrandsData.getLink2());
            } else {
                holder.tvSubCategory2.setVisibility(View.GONE);
            }
            if (addBrandsData.getDescription() != null) {
                holder.textViewDescription.setText(addBrandsData.getDescription());
            }
            if (addBrandsData.getPrice() != null) {
                holder.textViewPrice.setText(new StringBuilder().append("Price: Rs. ").append(addBrandsData.getPrice()));
            }
            String imageURL = (new StringBuilder().append(Constant.ImageURL).append(addBrandsData.getPath()).append("/").append(addBrandsData.getImage())).toString();
            Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                    .error(R.drawable.dummy_place)
                    .into(holder.image);

        }
        holder.fabCancelItem.setOnClickListener(view -> itemClickListener.itemClick(position,"delete"));
        holder.fabEditItem.setOnClickListener(view -> itemClickListener.itemClick(position,"update"));
    }

    @Override
    public int getItemCount() {
        return talentDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        FloatingActionButton fabCancelItem,fabEditItem;
        TextView tvCategory, textViewDescription, textViewPrice, tvSubCategory1, tvSubCategory2;

        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            fabCancelItem = view.findViewById(R.id.fabCancelItem);
            fabEditItem = view.findViewById(R.id.fabEditItem);
            tvCategory = view.findViewById(R.id.tvCategory);
            textViewDescription = view.findViewById(R.id.textViewDescription);
            textViewPrice = view.findViewById(R.id.textViewPrice);
            tvSubCategory1 = view.findViewById(R.id.tvSubCategory1);
            tvSubCategory2 = view.findViewById(R.id.tvSubCategory2);
        }
    }
}