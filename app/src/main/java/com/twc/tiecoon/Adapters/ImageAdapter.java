package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.UploadImageListData;

import java.util.List;

public class ImageAdapter extends PagerAdapter {

    private final Context mContext;
    private final List<UploadImageListData> pagerList;
    private final LayoutInflater mLayoutInflater;

    public ImageAdapter(Context mContext, List<UploadImageListData> pagerList) {

        this.mContext = mContext;
        this.pagerList = pagerList;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return pagerList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (o);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.slidingimages_layout, container, false);
        ImageView ivBanner = itemView.findViewById(R.id.image);
        String imageURL = Constant.ImageURL + pagerList.get(position).getCreateimage();
        Glide.with(mContext)
                .load(pagerList.get(position).getImage())
                .thumbnail(Glide.with(mContext).load(R.drawable.talent1))
                .error(Glide.with(mContext)
                        .load(R.drawable.tiecoon_logo))
                .into(ivBanner);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((FrameLayout) object);
    }

}
