package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddBrandsData;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SpecialProfileHorizontalAdapters extends RecyclerView.Adapter<SpecialProfileHorizontalAdapters.MyViewHolder> {
    private List<AddBrandsData> homeUserDetailBrandList;
    private Context context;

    public SpecialProfileHorizontalAdapters(Context context, List<AddBrandsData> homeUserDetailBrandList) {
        this.context = context;
        this.homeUserDetailBrandList = homeUserDetailBrandList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.special_profile_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AddBrandsData homeUserDetailBrand = homeUserDetailBrandList.get(position);
        if (homeUserDetailBrand != null) {
            if (homeUserDetailBrand.getName() != null) {
                holder.tvCategory.setText(homeUserDetailBrand.getName());
            } else {
                holder.tvCategory.setText("");
            }
            if (homeUserDetailBrand.getLink1() != null) {
                holder.tvSubCategory1.setText(homeUserDetailBrand.getLink1());
            }
            if (homeUserDetailBrand.getDescription() != null) {
                holder.textViewDescription.setText(homeUserDetailBrand.getDescription());
            }
            if (homeUserDetailBrand.getPrice() != null) {
                holder.textViewPrice.setText(new StringBuilder().append("Price: ").append(homeUserDetailBrand.getPrice()));
            }
            if (homeUserDetailBrand.getPath() != null && homeUserDetailBrand.getImage() != null) {
                String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailBrand.getPath()).append("/").append(homeUserDetailBrand.getImage())).toString();
                Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                        .error(R.drawable.dummy_place)
                        .into(holder.image);
            }
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailBrandList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView tvCategory, textViewDescription, textViewPrice, tvSubCategory1;

        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            tvCategory = view.findViewById(R.id.tvCategory);
            textViewDescription = view.findViewById(R.id.textViewDescription);
            textViewPrice = view.findViewById(R.id.textViewPrice);
            tvSubCategory1 = view.findViewById(R.id.tvSubCategory1);
        }
    }
}