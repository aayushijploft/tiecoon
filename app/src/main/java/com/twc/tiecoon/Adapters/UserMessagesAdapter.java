package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.network.response.ChatDataDetails;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

public class UserMessagesAdapter extends RecyclerView.Adapter<UserMessagesAdapter.MyViewHolder> {
    private List<ChatDataDetails> chatDataDetailsList;
    private ItemClickListener itemClickListener;
    private Context context;

    public UserMessagesAdapter(Context context, List<ChatDataDetails> chatDataDetailsList, ItemClickListener itemClickListener) {
        this.context = context;
        this.chatDataDetailsList = chatDataDetailsList;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.messages_item_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ChatDataDetails chatDataDetails=chatDataDetailsList.get(position);
        holder.tv_UserName.setText(chatDataDetails.getName());
        holder.tv_UserMsg.setText(chatDataDetails.getMassage());
        Glide.with(context).load(chatDataDetails.getProfile()).placeholder(R.drawable.ic_dummy_user)
                .error(R.drawable.ic_dummy_user)
                .into(holder.imgProfile);
        holder.linearMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.itemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return chatDataDetailsList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout linearMessage;
        TextView tv_UserName,tv_UserMsg;
        ImageView imgProfile;

        MyViewHolder(View view) {
            super(view);
            imgProfile = view.findViewById(R.id.imgProfile);
            linearMessage = view.findViewById(R.id.linearMessage);
            tv_UserName = view.findViewById(R.id.tv_UserName);
            tv_UserMsg = view.findViewById(R.id.tv_UserMsg);
        }
    }
}