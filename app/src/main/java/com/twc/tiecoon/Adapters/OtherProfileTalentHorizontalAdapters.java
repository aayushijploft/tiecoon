package com.twc.tiecoon.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.twc.tiecoon.R;
import com.twc.tiecoon.Services.Constant;
import com.twc.tiecoon.network.response.AddProductSubCategoryData;
import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;
import com.twc.tiecoon.utils.ItemClickListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

public class OtherProfileTalentHorizontalAdapters extends RecyclerView.Adapter<OtherProfileTalentHorizontalAdapters.MyViewHolder> {
    private final List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList;
    private final ItemClickListener itemClickListener;
    private final Context context;

    public OtherProfileTalentHorizontalAdapters(Context context, List<HomeUserDetailsUploadTalent> homeUserDetailsUploadTalentList,
             ItemClickListener itemClickListener) {
        this.context = context;
        this.homeUserDetailsUploadTalentList = homeUserDetailsUploadTalentList;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_profile_talent_product_service_horizontal, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        HomeUserDetailsUploadTalent homeUserDetailsUploadTalent = homeUserDetailsUploadTalentList.get(position);
        if (homeUserDetailsUploadTalent != null) {
             AddProductSubCategoryData addProductSubCategoryData = homeUserDetailsUploadTalent.getSubCategory();
            if (addProductSubCategoryData != null && addProductSubCategoryData.getTitle()!=null) {
                holder.tvSubCategory.setText(addProductSubCategoryData.getTitle());
            }
            if (homeUserDetailsUploadTalent.getDescription() != null) {
                holder.textViewDescription.setText(homeUserDetailsUploadTalent.getDescription());
            }
            if (homeUserDetailsUploadTalent.getPrice() != null  && homeUserDetailsUploadTalent.getPricecode()!=null) {
                String procecode;
                if(homeUserDetailsUploadTalent.getPricecode().equals("USD")){
                    procecode = "$";
                }
                else procecode = "₹";
                holder.textViewPrice.setText(new StringBuilder().append("").append(procecode).append(" ").append(homeUserDetailsUploadTalent.getPrice()));
            }
//            if (homeUserDetailsUploadTalent.getUploadtype() != null) {
//                if (homeUserDetailsUploadTalent.getUploadtype() == 1) {
//                    holder.relativeWaterMark.setVisibility(View.GONE);
//                    holder.image.setVisibility(View.VISIBLE);
//                    String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailsUploadTalent.getPath()).append("/").append(homeUserDetailsUploadTalent.getCreateimage())).toString();
//                    Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
//                            .error(R.drawable.dummy_place)
//                            .into(holder.image);
//                } else {
//                    holder.image.setVisibility(View.GONE);
//                    holder.relativeWaterMark.setVisibility(View.VISIBLE);
//                    if (homeUserDetailsUploadTalent.getFontcolor() != null && !(homeUserDetailsUploadTalent.getFontcolor().isEmpty())) {
//                        try {
//                            holder.relativeWaterMark.setBackgroundColor(Color.parseColor(homeUserDetailsUploadTalent.getFontcolor()));
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    if(homeUserDetailsUploadTalent.getWatermark()!=null)
//                    holder.tvWatermark.setText(homeUserDetailsUploadTalent.getWatermark());
//                }
//            } else {
//                holder.relativeWaterMark.setVisibility(View.GONE);
//                holder.image.setVisibility(View.VISIBLE);
//                String imageURL = (new StringBuilder().append(Constant.ImageURL).append(homeUserDetailsUploadTalent.getPath()).append("/").append(homeUserDetailsUploadTalent.getImage())).toString();
//                Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
//                        .error(R.drawable.dummy_place)
//                        .into(holder.image);
//            }
            if(homeUserDetailsUploadTalent.getTalant_type() != null){
                switch (homeUserDetailsUploadTalent.getTalant_type()) {
                    case "image":
                        holder.image.setVisibility(View.VISIBLE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        String imageURL = Constant.ImageURL + homeUserDetailsUploadTalent.getPath() + "/" + homeUserDetailsUploadTalent.getCreateimage();
                        Glide.with(context).load(imageURL).error(R.drawable.dummy_place)
                                .error(R.drawable.dummy_place)
                                .into(holder.image);
                        break;
                    case "audio":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.VISIBLE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        break;
                    case "video":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.VISIBLE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        if (homeUserDetailsUploadTalent.getVideothumbnail() != null) {
                            String imageURL1 = Constant.ImageURL + "/" + homeUserDetailsUploadTalent.getVideothumbnail();
                            Glide.with(context).load(imageURL1).error(R.drawable.dummy_place)
                                    .error(R.drawable.dummy_place)
                                    .into(holder.ivThumbnail);
                        }
                        break;
                    case "textarea":
                        holder.image.setVisibility(View.GONE);
                        holder.layoutAudio.setVisibility(View.GONE);
                        holder.layoutVideo.setVisibility(View.GONE);
                        holder.tvTextArea.setVisibility(View.GONE);
                        holder.relativeWaterMark.setVisibility(View.VISIBLE);
                        if (homeUserDetailsUploadTalent.getFontcolor() != null && !(homeUserDetailsUploadTalent.getFontcolor().isEmpty())) {
                            try {
                                holder.relativeWaterMark.setBackgroundColor(Color.parseColor(homeUserDetailsUploadTalent.getFontcolor()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (homeUserDetailsUploadTalent.getTextarea() != null)
                            holder.tvWatermark.setText(homeUserDetailsUploadTalent.getTextarea());
                        break;
                }
            }

            holder.cardViewBuy.setOnClickListener(view -> itemClickListener.itemClick(position));
        }
    }

    @Override
    public int getItemCount() {
        return homeUserDetailsUploadTalentList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout relativeWaterMark;
        ImageView image,ivThumbnail;
        TextView tvTextArea;
        FrameLayout layoutAudio,layoutVideo;
        TextView tvWatermark,tvSubCategory, textViewDescription, textViewPrice;
        CardView cardViewBuy;
        MyViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            relativeWaterMark = view.findViewById(R.id.relativeWaterMark);
            tvWatermark = view.findViewById(R.id.tvWatermark);
            tvSubCategory = view.findViewById(R.id.tvSubCategory);
            textViewDescription = view.findViewById(R.id.textViewDescription);
            textViewPrice = view.findViewById(R.id.textViewPrice);
            cardViewBuy=view.findViewById(R.id.cardViewBuy);

            layoutAudio = view.findViewById(R.id.layoutAudio);
            layoutVideo = view.findViewById(R.id.layoutVideo);
            tvTextArea = view.findViewById(R.id.tvTextArea);
            ivThumbnail = view.findViewById(R.id.ivThumbnail);
        }
    }
}