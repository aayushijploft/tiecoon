package com.twc.tiecoon.Notification_new;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.twc.tiecoon.Activities.ChatActivity;
import com.twc.tiecoon.Activities.OtherUserProfileActivity;
import com.twc.tiecoon.Activities.ProjectFeedback;
import com.twc.tiecoon.Activities.ProjectInvitationActivity;
import com.twc.tiecoon.Activities.TransactionActivity;
import com.twc.tiecoon.Activities.ViewAssignment;
import com.twc.tiecoon.ApplicationClass;
import com.twc.tiecoon.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Random;

public class FirebaseMessageReceive extends FirebaseMessagingService {
    private static final String TAG ="vishal_data";
    public static int NOTIFICATION_ID;
    NotificationCompat.Builder builder;
    Intent intent;
    String body = "",title ="",userID ="",userType="", TicketId ="",project_id="";
    private NotificationUtils notificationUtils;
    private NotificationManager mNotificationManager;
    String user_id = "";
    String name = "";
    String qbID = "";
    String click_action = "";

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.d(TAG, "NEW_TOKEN: " + token);
        ApplicationClass.appPreferences.SetFirebaseToken(token);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Log.d(TAG, "Message data payload123242: " + remoteMessage.getNotification());
            Map<String, String> data = remoteMessage.getData();
            if (remoteMessage.getData() != null || !remoteMessage.getData().isEmpty()) {
                Log.d(TAG, "Message data payload123242: " + remoteMessage.getData());

                if (remoteMessage.getData() != null || !remoteMessage.getData().isEmpty()) {
                    body = data.get("body");
                    title =  remoteMessage.getData().get("title");
                    user_id = remoteMessage.getData().get("UserId");
                    if(remoteMessage.getData().containsKey("project_id")){
                        project_id = remoteMessage.getData().get("project_id");
                    }
                    if(remoteMessage.getData().containsKey("qbID")){
                        qbID = remoteMessage.getData().get("qbID");
                    }

                    name = remoteMessage.getData().get("name");
                    click_action = remoteMessage.getData().get("click_action");
                    Log.d(TAG, "From: " + user_id + "_" + name);
                }
                handleNotification(body, title, user_id,name,click_action);
            } else {
                if (remoteMessage.getData() == null || remoteMessage.getData().isEmpty()) {
                    String messageNotification = remoteMessage.getNotification().getBody();
                    if (messageNotification != null) {
                        handleNotification(messageNotification, remoteMessage.getNotification().getTitle(),"","",click_action);
                    }
                }
                else
                {
                    if (remoteMessage.getData() != null || !remoteMessage.getData().isEmpty()) {
                            body = data.get("body");
                            title =  remoteMessage.getData().get("title");
                            user_id = remoteMessage.getData().get("UserId");
                            name = remoteMessage.getData().get("name");
                        if(remoteMessage.getData().containsKey("qbID")){
                            qbID = remoteMessage.getData().get("qbID");
                        }
                        click_action = remoteMessage.getData().get("click_action");
                            Log.d(TAG, "From: " + user_id + "_" + name);
                        }
                        handleNotification(body, title,  user_id,name,click_action);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleNotification(String message, String title, String User_Id,String Name,String click_action) {

        Random rand = new Random();
        int n = rand.nextInt(5000);

        if (!NotificationUtils.isAppIsInBackground(this)) {
            try {

                PendingIntent contentIntent = null;
                Intent intent;
                switch (click_action) {
                    case "notificationTalent":
                        intent = new Intent(this, OtherUserProfileActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("UserId", User_Id);
                        intent.putExtra("tag", "1");
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                    case "notificationTalant":
                        intent = new Intent(this, OtherUserProfileActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("UserId", User_Id);
                        intent.putExtra("tag", "1");
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                    case "notificationProject":
                        intent = new Intent(this, ViewAssignment.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("project_id", project_id);
                        intent.putExtra("qbID", qbID);
                        intent.putExtra("name", name);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                    case "refund":
                        intent = new Intent(this, ViewAssignment.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("project_id", project_id);
                        intent.putExtra("qbID", qbID);
                        intent.putExtra("name", name);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                    case "collaborationrequest":
                        intent = new Intent(this, ProjectInvitationActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                   case "endproject":
                        intent = new Intent(this, ProjectFeedback.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("project_id",project_id);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                    case "parchage":
                        intent = new Intent(this, OtherUserProfileActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("UserId", User_Id);
                        intent.putExtra("tag", "1");
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                    case "amountrelese":
                        release();
                        break;
                }


                AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                manager.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0);
                Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.notification_sound);
               /* MediaPlayer player = MediaPlayer.create(getApplicationContext(), uri);
                player.start();*/

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        if (Build.VERSION.SDK_INT < 26) {
                            return;
                        }
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        NotificationChannel channel = new NotificationChannel("default", "Channel name", NotificationManager.IMPORTANCE_DEFAULT);
                        channel.setDescription("Channel description");
                        notificationManager.createNotificationChannel(channel);
                    }
                    builder = new NotificationCompat.Builder(this, "default")
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.tiecoon_logo))
                            .setSmallIcon(R.drawable.tiecoon_logo)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setColor(Color.TRANSPARENT)
                            .setContentText(message)
                            .setSound(uri)
                            .setContentTitle(title)
                            .setAutoCancel(true)
                            .setPriority(Notification.PRIORITY_MAX)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(message));

                /*NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();*/
                } else {
                    builder= new NotificationCompat.Builder(this)
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.tiecoon_logo))
                            .setSmallIcon(R.drawable.tiecoon_logo)
                            .setContentText(message)
                            .setSound(uri)
                            .setContentTitle(title)
                            .setAutoCancel(true)
                            .setPriority(Notification.PRIORITY_MAX)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(message));

                    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                    notificationUtils.playNotificationSound();
                }

                builder.setContentIntent(contentIntent);
                mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

//                builder.setDefaults(NotificationCompat.DEFAULT_ALL);
                mNotificationManager.notify(n, builder.build());

            } catch (Exception e) {
                e.printStackTrace();
            }

            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("isNotification", true);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

        /*NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.playNotificationSound();*/
        } else {
            try {
                PendingIntent contentIntent = null;
                Intent intent;
                switch (click_action) {
                    case "notificationLike":
                        intent = new Intent(this, OtherUserProfileActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("UserId", User_Id);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                    case "notificationTalant":
                        intent = new Intent(this, OtherUserProfileActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("UserId", User_Id);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                   case "userlike":
                        intent = new Intent(this, OtherUserProfileActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("UserId", User_Id);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                    case "usercollaborationrequest":
                        intent = new Intent(this, ProjectInvitationActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                    case "relese":
                        intent = new Intent(this, TransactionActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                    case "servicerelese":
                        intent = new Intent(this, TransactionActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                     case "servicedeposit":
                        intent = new Intent(this, TransactionActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;
                    case "notificationProject":
                        intent = new Intent(this, ViewAssignment.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("project_id", project_id);
                        contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                        break;

                }
                AudioManager manager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                manager.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0);
                Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.notification_sound);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        if (Build.VERSION.SDK_INT < 26) {
                            return;
                        }
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        NotificationChannel channel = new NotificationChannel("default", "Channel name", NotificationManager.IMPORTANCE_DEFAULT);
                        channel.setDescription("Channel description");
                        notificationManager.createNotificationChannel(channel);
                    }
                    builder = new NotificationCompat.Builder(this, "default")
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.tiecoon_logo))
                            .setSmallIcon(R.drawable.tiecoon_logo)
                            .setColor(getResources().getColor(R.color.colorPrimary))
                            .setColor(Color.TRANSPARENT)
                            .setContentText(message)
                            .setSound(uri)
                            .setContentTitle(title)
                            .setAutoCancel(true)
                            .setPriority(Notification.PRIORITY_MAX)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(message));


                }
                else {
                    builder = new NotificationCompat.Builder(this)
                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.tiecoon_logo))
                            .setSmallIcon(R.drawable.tiecoon_logo)
                            .setContentText(message)
                            .setSound(uri)
                            .setContentTitle(title)
                            .setAutoCancel(true)
                            .setPriority(Notification.PRIORITY_MAX)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(message));

                    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                    notificationUtils.playNotificationSound();
                }

                builder.setContentIntent(contentIntent);

                mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

                mNotificationManager.notify(n, builder.build());

            } catch (Exception e) {
                e.printStackTrace();
            }

            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("isNotification", true);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    private void release() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.remove_confirm);

        TextView btnRemove = dialog.findViewById(R.id.btnRemove);
        TextView btnCancle = dialog.findViewById(R.id.btnCancle);
        TextView tvConfirm = dialog.findViewById(R.id.tvConfirm);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);
        tvConfirm.setText("Your amount will be credited on your wallet on My Transaction page in 7-10 days");
        btnRemove.setText("OK");
        tvTitle.setVisibility(View.GONE);
        btnCancle.setVisibility(View.GONE);
//        tvTitle.setText("Collaboration Request");
        btnRemove.setOnClickListener(v -> {
            dialog.dismiss();

        });
        btnCancle.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

}
