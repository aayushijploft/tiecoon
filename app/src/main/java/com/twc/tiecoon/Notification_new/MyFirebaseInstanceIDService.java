package com.twc.tiecoon.Notification_new;

/*
 * Created by vishal on 09-may-18.
 */
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;




public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";


    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);


        getSharedPreferences("Refreshed_token",MODE_PRIVATE).edit().putString("token",refreshedToken).apply();
        getSharedPreferences("Notification",MODE_PRIVATE).edit().putString("status","1").apply();
    }
    // [END refresh_token]
}