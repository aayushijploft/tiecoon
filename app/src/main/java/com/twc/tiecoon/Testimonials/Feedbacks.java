package com.twc.tiecoon.Testimonials;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.twc.tiecoon.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class Feedbacks extends Fragment {


    private RecyclerView recycler_testimonials;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feedbacks, container, false);
        recycler_testimonials = view.findViewById(R.id.recycler_testimonials);
        recycler_testimonials.setAdapter(new FeedbackAdapter());
        return view;
    }

    public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(getActivity()).inflate(R.layout.list_feedbacks, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        }

        @Override
        public int getItemCount() {
            return 10;
        }

        public long getItemId(int position) {
            return position;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private CircleImageView feedbackuser_image;
            private TextView feedbackuser_name;
            private RatingBar rating_feedback;
            private TextView feedbackuser_text;
            public MyViewHolder(View v) {
                super(v);
                this.feedbackuser_image = v.findViewById(R.id.feedbackuser_image);
                this.feedbackuser_name = v.findViewById(R.id.feedbackuser_name);
                this.rating_feedback = v.findViewById(R.id.rating_feedback);
                this.feedbackuser_text = v.findViewById(R.id.feedbackuser_text);

            }
        }

    }

}