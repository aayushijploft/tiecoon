package com.twc.tiecoon.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.twc.tiecoon.Fonts_Utils.FontCache;


public class CustomTextViewBold extends TextView {


    public CustomTextViewBold(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public CustomTextViewBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context)
    {
        Typeface customFont = FontCache.getTypeface("TCB.TTF", context);
        setTypeface(customFont);
    }
}
