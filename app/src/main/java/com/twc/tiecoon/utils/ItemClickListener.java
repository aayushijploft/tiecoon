package com.twc.tiecoon.utils;

public interface ItemClickListener {
    void itemClick(int pos);
}
