package com.twc.tiecoon.utils;

public interface ItemClickListenerExtraParam {
    void itemClick(int pos,String type);
}
