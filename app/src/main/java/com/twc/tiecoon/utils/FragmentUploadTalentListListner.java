package com.twc.tiecoon.utils;

import com.twc.tiecoon.network.response.HomeUserDetailsUploadTalent;

import java.util.List;

public interface FragmentUploadTalentListListner {
    void setList(List<HomeUserDetailsUploadTalent> list);
}
