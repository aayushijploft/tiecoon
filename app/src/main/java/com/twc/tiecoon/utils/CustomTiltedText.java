package com.twc.tiecoon.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.twc.tiecoon.Fonts_Utils.FontCache;

public class CustomTiltedText extends TextView {
    

        public CustomTiltedText(Context context) {
            super(context);

            applyCustomFont(context);
        }

        public CustomTiltedText(Context context, AttributeSet attrs) {
            super(context, attrs);

            applyCustomFont(context);
        }

        public CustomTiltedText(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);

            applyCustomFont(context);
        }

        private void applyCustomFont(Context context)
        {
            Typeface customFont = FontCache.getTypeface("Vladimir Script Regular.ttf", context);
            setTypeface(customFont);
        }

    }


