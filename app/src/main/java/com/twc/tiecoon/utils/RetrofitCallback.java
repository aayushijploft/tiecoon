package com.twc.tiecoon.utils;


import android.content.Context;

import java.io.IOException;

import androidx.annotation.NonNull;
import okhttp3.RequestBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitCallback<T> implements Callback<T> {

        private Context mContext;
        private final Callback<T> mCallback;

        public RetrofitCallback(Context context, Callback<T> callback) {
            mContext = context;
            this.mCallback = callback;
        }


        @Override
        public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
            String log = "Request==>" + call.request().url();
            String body = bodyToString(call.request().body());
            if (body != null) {
                log = log + "?" + body;
            }
            log = log + ", Response ===> Type" + response.isSuccessful() + "";
            if (response.isSuccessful()) {
                log = log + "<==>" + response.body();
            }
            LoggerUtils.writeLogU(log);
            mCallback.onResponse(call, response);
        }

        @Override
        public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
            LoggerUtils.writeLogU("Request==>" + call.request().url() + ", Error ===>" + t.getMessage());
            mCallback.onFailure(call, t);
        }

        private String bodyToString(final RequestBody request) {
            try {
                if (request == null) {
                    return null;
                }
                final Buffer buffer = new Buffer();
                request.writeTo(buffer);
                return buffer.readUtf8();
            } catch (final IOException e) {
                return null;
            }
        }

    }
