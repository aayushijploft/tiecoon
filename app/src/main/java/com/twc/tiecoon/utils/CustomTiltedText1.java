package com.twc.tiecoon.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.twc.tiecoon.Fonts_Utils.FontCache;

public class CustomTiltedText1 extends TextView {

    public CustomTiltedText1(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public CustomTiltedText1(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public CustomTiltedText1(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    private void applyCustomFont(Context context)
    {
        Typeface customFont = FontCache.getTypeface("Spantaran.ttf", context);
        setTypeface(customFont);
    }
}
