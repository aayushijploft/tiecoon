package com.twc.tiecoon.utils;

import android.os.Environment;
import android.util.Log;

import com.twc.tiecoon.Services.Constant;

import java.io.File;

public class LoggerUtils {

    public static void createLogFile() {
        Logger.getInstance(getLogFilePath());
    }

    private static String getLogFilePath() {
        return Environment.getExternalStorageDirectory() + File.separator + Constant.LOG_FILE_NAME;
    }

    public static File getLogFile() {
        try {
            return new File(getLogFilePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void writeLogU(String msg) {
        createLogFile();
        Log.d("retrofit Url", msg);
        Logger.info("Url", msg);
    }
}
