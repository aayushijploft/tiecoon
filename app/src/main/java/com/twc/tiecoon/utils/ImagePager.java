package com.twc.tiecoon.utils;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.twc.tiecoon.R;

import androidx.fragment.app.Fragment;

public class ImagePager extends Fragment {


    private TextView tv_fresh_food;
    private TextView tv_flat;
    private TextView tv_offValue;
    private TextView tv_off;
    private ImageView iv_offer_banner;
    private LinearLayout ll_banner_texts;

    public ImagePager() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_pager, container, false);
        initView(view);
        return view;
    }

    public void initView(View view) {

        iv_offer_banner = view.findViewById(R.id.iv_offer_banner);
        ll_banner_texts = view.findViewById(R.id.ll_banner_texts);
        tv_fresh_food = view.findViewById(R.id.tv_fresh_food);
        tv_flat = view.findViewById(R.id.tv_flat);
        tv_offValue = view.findViewById(R.id.tv_offValue);
        tv_off = view.findViewById(R.id.tv_off);

//        try {
//            if (!getArguments().getString("image").isEmpty()) {
//                Picasso.get()
//                        .load( getArguments().getString("image"))
//                        .noFade()
//                        .error(R.drawable.no_image_available)
//                        .into(iv_offer_banner);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        iv_offer_banner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //gsidgfjhasokli
            }
        });


        tv_fresh_food.setText("Fresh Food");
        tv_flat.setText("Flat");
        tv_offValue.setText("40" + "% ");
        tv_off.setText("OFF");
    }


}
