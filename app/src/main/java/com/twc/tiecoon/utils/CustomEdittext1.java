package com.twc.tiecoon.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.twc.tiecoon.Fonts_Utils.FontCache;

public class CustomEdittext1 extends EditText {


    public CustomEdittext1(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public CustomEdittext1(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public CustomEdittext1(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    public void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("TCM.TTF", context);
        setTypeface(customFont);
    }
}

