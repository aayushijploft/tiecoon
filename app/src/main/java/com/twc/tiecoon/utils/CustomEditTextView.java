package com.twc.tiecoon.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.twc.tiecoon.Fonts_Utils.FontCache;


/**
 * Created by logicspice on 15/10/16.
 */

public class CustomEditTextView extends EditText {


    public CustomEditTextView(Context context) {
        super(context);

        applyCustomFont(context);
    }

    public CustomEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        applyCustomFont(context);
    }

    public CustomEditTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        applyCustomFont(context);
    }

    public void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("Typo_Round_Regular_Demo.otf", context);
        setTypeface(customFont);
    }
}
