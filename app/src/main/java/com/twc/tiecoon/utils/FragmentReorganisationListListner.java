package com.twc.tiecoon.utils;

import com.twc.tiecoon.network.response.HomeUserDetailsReOrganisation;

import java.util.List;

public interface FragmentReorganisationListListner {
    void setList(List<HomeUserDetailsReOrganisation> list);
}
