package com.twc.tiecoon.utils;

import com.twc.tiecoon.network.response.HomeUserDetailsFeedback;

import java.util.List;

public interface FragmentFeedbackListListner {
    void setList(List<HomeUserDetailsFeedback> list);
}
