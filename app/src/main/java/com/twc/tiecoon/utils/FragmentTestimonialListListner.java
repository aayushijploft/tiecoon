package com.twc.tiecoon.utils;

import com.twc.tiecoon.network.response.HomeUserDetailsTestimonial;

import java.util.List;

public interface FragmentTestimonialListListner {
    void setList(List<HomeUserDetailsTestimonial> list);
}
