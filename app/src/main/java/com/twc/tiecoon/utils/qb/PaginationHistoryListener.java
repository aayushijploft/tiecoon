package com.twc.tiecoon.utils.qb;

public interface PaginationHistoryListener {
    void downloadMore();
}