package com.twc.tiecoon.utils;

import com.twc.tiecoon.network.response.HomeUserDetailsProduct;

import java.util.List;

public interface FragmentProductServiceListListner {
    void setList(List<HomeUserDetailsProduct> list);
}
