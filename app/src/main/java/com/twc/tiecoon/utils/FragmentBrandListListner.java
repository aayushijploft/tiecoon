package com.twc.tiecoon.utils;

import com.twc.tiecoon.network.response.AddBrandsData;

import java.util.List;

public interface FragmentBrandListListner {
    void setList(List<AddBrandsData> list);
}
