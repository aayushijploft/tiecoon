package com.twc.tiecoon.utils;

public interface ItemClickListenerExtraThreeParam {
    void itemClick(int pos, String subPos,String type);
}
