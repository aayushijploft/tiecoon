package com.twc.instagrampicker.gallery;

public interface SelectListener {

    void onClick(String address, int position);
}
